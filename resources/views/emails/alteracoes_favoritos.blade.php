<table width="500" cellspacing="0" cellpadding="30" border="0" style="margin: 0 auto;">
	<tr col>
		<td width="500">
			<img src="{{ asset('/images/backend/emkt_red_logo.png') }}" border="0" style="display:block; margin: 0 auto;">
		</td>
	</tr>
	<tr>
		<td width="500" style="font-size:15px; color:#777777; font-family: Arial, Verdana, Tahoma;">
			Ol&aacute; {{ $nome }}, <br /><br />
			Segue lista com as alterações realizadas em seus produtos favoritos:
            <br /><br />
            <table cellpadding="5" cellspacing="10">
                <thead>
                    <th>Módulo</th>
                    <th>Descrição</th>
                    <th>Produto</th>
                    <th>Data</th>
                </thead>
                <tbody>
                    @foreach($news as $new)
                        <tr style="height: 40px;">
                            <td>
                                <div class="image-border-right">
                                    <img src="{{ $new->image_mod }}">
                                </div>
                            </td>
                            <td> {{ Html::link($new->url, $new->descricao, array('style' => 'text-decoration: none;')) }} </td>
                            <td> {{ Html::link($new->url, $new->produtos->nome, array('style' => 'text-decoration: none;')) }} </td>
                            <td> {{ $new->data }} </td>
                        </tr>
                    @endforeach
                </tbody>
            </table><br /><br /><br />
			Att,<br /><br />
			Equipe PaintPack
		</td>
	</tr>
	<tr>
		<td width="500" style="font-size:10px; color:#777777; font-family: Arial, Verdana, Tahoma; text-align: center;">
			Fornecido pela PaintPack &copy;2016
		</td>
	</tr>
</table>
