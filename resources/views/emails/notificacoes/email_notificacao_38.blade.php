<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <table style="margin:0 auto;padding:40px 100px 0px 100px;background:#F4F4F4;" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td style="font-family:Arial, Helvetica, sans-serif;border:1px solid #ACACAC;background:#FFFFFF;">
                    <table style="width:800px;" cellpadding="0" cellspacing="0">
                        <thead>	
                            <tr>
                                <th style="padding:70px 100px 50px 100px; border-bottom:2px dotted #ACACAC;">
                                    <a style="float:left;display:inline-block;" href="{{ url('/') }}"><img src="{{ asset('/images/layout/cliente_'. config('app.costummer_id') .'/logo-topo-email.png?v=31072024') }}" title="" alt=""></a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="padding:80px 100px 90px 100px;">
                                    <table style="width:100%;" cellpadding="0" cellspacing="0"> 
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <h3 style="font-size:20px;font-weight:600;margin:0px 0px 50px 0px; padding-bottom:15px;border-bottom:1px solid #ACACAC;">Arte-final &bull; {{ $ticket->numero }} &bull; Versão {{ $arte->versao }}</h3>
                                                    <p style="margin:0px 0px 5px 0px;">
                                                        {{ Auth::user()->nome_abreviado }} disponibilizou a arte-final para ser baixada, clique no link abaixo para acessar a página com os arquivos.
                                                    </p>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td style="height:30px;"></td>
                                            </tr>
                                            
                                            @if ($mensagem)
                                            <tr>
                                                <td>
                                                    <div>
                                                        <h3 style="font-size:16px;font-weight:600;margin:0px 0px 10px 0px;">Mensagem</h3>
                                                        <!-- TO-DO mensagem do envio da arte para o fornecedor -->
                                                        <p>{!! nl2br($mensagem) !!}</p>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endif
                                            
                                            <tr>
                                                <td style="height:30px;"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div style="border:1px solid #ACACAC;background:#F4F4F4;">
                                                        <a style="display:block;color:#000000;line-height:26px;text-decoration:none;padding:15px;overflow:auto;" href="{{ route('download') }}?token={{ $token }}" target="_blank">
                                                            <img style="float:left;display:inline-block;" src="{{ asset('images/backend/ponteiro.png') }}" title="" alt="" />
                                                            <span style="display:block;font-weight:600;vertical-align:middle;text-decoration:underline;word-break:break-all;margin-left:33px;">{{ $ticket->sku_nome_completo }}</span>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height:30px;"></td>
                                            </tr>
                                            
                                            @if ($compartilhadas)
                                            <tr>
                                                <td>
                                                    <h3 style="font-size:16px;font-weight:600;margin:0px;">Item Compartilhado</h3>
                                                    <p style="margin:0px;">
                                                        Esta mesma arte-final serve para as seguintes embalagens:  
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height:10px;"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width:100%;border-collapse:collapse;" border="0" cellpading="0" cellspacing="0">
                                                        
                                                        @foreach($compartilhadas as $compartilhada)
                                                        <tr style="border-bottom:1px solid #ACACAC">
                                                            <td style="height:50px;">
                                                                <span style="display:flex;align-items:center;font-weight:300;line-height:18px;">
                                                                    <img style="float:left;display:inline-block;vertical-align:middle;" src="{{ asset('images/backend/cubo.png') }}" title="" alt="" />
                                                                    <strong style="display:inline-block;font-weight:300;vertical-align:middle;margin-left:10px;">{{ $compartilhada['nome_original'] }}</strong>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                        
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height:30px;"></td>
                                            </tr>
                                            @endif
                                            
                                            <tr>
                                                <td>
                                                    <p style="display:block;font-weight:200;line-height:normal;vertical-align:middle;text-decoration:none;word-break:break-all;">
                                                        A página ficará disponível por <strong style="font-weight:600;">30 dias</strong> e depois será expirada. Baixe o quanto antes.
                                                    </p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="font-family:Arial, Helvetica, sans-serif;font-size:12px;text-align:center;width:800px;margin:0 auto;padding:40px 0px;">
                        <a style="display:inline-block;margin-bottom:20px;" href="http://www.rubrum.com.br" target="_blank"><img src="{{ asset('/images/backend/rubrum-logo.png') }}" title="" alt="" /></a>
                        <p style="margin:0px 0px 3px 0px;">
                            E-mail enviado automaticamente por Rubrum Software.
                        </p>
                        <p style="margin:0px 0px 3px 0px;">
                            Seu cadastro no Rubrum implica em envios obrigatórios de e-mails pelo sistema.
                        </p>
                        <p style="margin:0px 0px 30px 0px;">
                            Defina suas preferências de e-mail em <a style="color:#000000;" href="{{ url('/site/user') }}">Configurar Conta</a>, ou solicite a exclusão 
                            do seu cadastro para não receber mais e-mails.
                        </p>
                        <p style="text-transform:uppercase;margin:0px;">
                            Rubrum Tecnologia LTDA.
                        </p>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>