<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <table style="margin:0 auto;padding:40px 100px 0px 100px;background:#F4F4F4;" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td style="font-family:Arial, Helvetica, sans-serif;border:1px solid #ACACAC;background:#FFFFFF;">
                    <table style="width:800px;" cellpadding="0" cellspacing="0">
                        <thead>	
                            <tr>
                                <th style="padding:70px 100px 50px 100px; border-bottom:2px dotted #ACACAC;">
                                    <a style="float:left;display:inline-block;" href="{{ url('/') }}"><img src="{{ asset('/images/layout/cliente_'. config('app.costummer_id') .'/logo-topo-email.png?v=31072024') }}" title="" alt=""></a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="padding:80px 100px 90px 100px;">
                                    <table style="width:100%;" cellpadding="0" cellspacing="0"> 
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <h3 style="font-size:20px;font-weight:600;margin:0px 0px 30px 0px; padding-bottom:15px;border-bottom:1px solid #ACACAC;">Ticket Cancelado &bull; {{ $ticket->numero }}</h3>
                                                    <div style="margin-bottom:40px;">
                                                        <p style="margin:0px;">{{ $notificacao->to->nome_abreviado }}, o ticket foi cancelado por {{ $notificacao->from->nome_abreviado }} às {{ ($ticket->cancelado_em) ? $ticket->cancelado_em->format('H\hi \d\e d/m/Y') : '' }}.</p>
                                                        <div style="margin-top:30px;">
                                                            <h4 style=" font-weight:600;color:#000000;margin:0px 0px 5px 0px;">Motivo do Cancelamento</h4>
                                                            <p style="font-weight:200;color:#000000;margin:0px;">
                                                                {!! nl2br($ticket->cancelado_motivo) !!}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="border:1px solid #ACACAC;background:#F4F4F4;">
                                                    <a style="display:block;color:#000000;line-height:26px;text-decoration:none;padding:15px;overflow:auto;" href="{{ route('site.artwork.ticket.edit.step1.get', [$ticket->id]) }}">
                                                        <img style="float:left;display:inline-block;" src="{{ asset('/images/backend/ticket.png') }}" title="" alt="" />
                                                        <span style="display:block;font-weight:600;vertical-align:middle;text-decoration:underline;word-break:break-all;margin-left:33px;">{{ $ticket->sku_nome_completo }}</span>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height:30px;"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <h4 style="font-size:16px;font-weight:600;margin:0px 0px 5px 0px;">Resumo do Ticket</h4>
                                                    <p style="margin:0px 0px 30px 0px;">{!! nl2br($ticket->descricao) !!}</p>
                                                    <ul style="font-weight:200;color:#000000;margin:0px;padding:0px;list-style:none;">
                                                        <li>&bull; Início: {{ $ticket->dt_ini->format('d/m/Y') }}</li>
                                                        <li>&bull; Aprovação: {{ $ticket->dt_envio->format('d/m/Y') }}</li>
                                                        <li>&bull; Encerramento: {{ $ticket->dt_fim->format('d/m/Y') }}</li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="font-family:Arial, Helvetica, sans-serif;font-size:12px;text-align:center;width:800px;margin:0 auto;padding:40px 0px;">
                        <a style="display:inline-block;margin-bottom:20px;" href="http://www.rubrum.com.br" target="_blank"><img src="{{ asset('/images/backend/rubrum-logo.png') }}" title="" alt="" /></a>
                        <p style="margin:0px 0px 3px 0px;">
                            E-mail enviado automaticamente por Rubrum Software.
                        </p>
                        <p style="margin:0px 0px 3px 0px;">
                            Seu cadastro no Rubrum implica em envios obrigatórios de e-mails pelo sistema.
                        </p>
                        <p style="margin:0px 0px 30px 0px;">
                            Defina suas preferências de e-mail em <a style="color:#000000;" href="{{ url('/site/user') }}">Configurar Conta</a>, ou solicite a exclusão 
                            do seu cadastro para não receber mais e-mails.
                        </p>
                        <p style="text-transform:uppercase;margin:0px;">
                            Rubrum Tecnologia LTDA.
                        </p>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>