<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    {{--<table style="margin:0 auto;padding:40px 100px 0px 100px;background:#F4F4F4;" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td style="font-family:Arial, Helvetica, sans-serif;border:1px solid #ACACAC;background:#FFFFFF;">
                    <table style="width:800px;" cellpadding="0" cellspacing="0">
                        <thead>	
                            <tr>
                                <th style="padding:70px 100px 50px 100px; border-bottom:2px dotted #ACACAC;">
                                    <a style="float:left;display:inline-block;" href="{{ url('/') }}"><img src="{{ asset('/images/backend/logo-email-login.png') }}" title="" alt=""></a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="padding:80px 100px 90px 100px;">
                                    <table style="width:100%;" cellpadding="0" cellspacing="0"> 
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <h3 style="font-size:20px;font-weight:600;margin:0px 0px 50px 0px; padding-bottom:15px;border-bottom:1px solid #ACACAC;">Aprovado</h3>
                                                    <div style="margin-bottom:40px;">
                                                        <p style="margin:0px 0px 5px 0px;">
                                                            {{ trim(Auth::user()->nome . ' ' . Auth::user()->sobrenome) }} te enviou as seguintes artes-finais, clique nos links para baixá-las:
                                                        </p>
                                                        <div style="border:1px solid #ACACAC;background:#F4F4F4;">
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            @foreach ($artes as $arte)
                                                                
                                                                
                                                                <a href="{{ asset($arte->path) }}" style="display:block;color:#000000;line-height:26px;text-decoration:none;padding:15px;overflow:auto;border-bottom:2px dotted #ACACAC;" href="javascript:;">
                                                                    <img style="float:left;display:inline-block;" src="images/baixar.png" title="" alt="" />
                                                                    <span style="display:block;font-weight:600;vertical-align:middle;text-decoration:underline;word-break:break-all;margin-left:33px;">{{ $arte->nome }}</span>
                                                                    <small style="display:block;font-size:12px;font-weight:200;line-height:normal;vertical-align:middle;text-decoration:none;word-break:break-all;margin-left:33px;">{{ $arte->nome_arquivo }}</small>
                                                                </a>
                                                                
                                                                
                                                                <div>
                                                                    <img src="{{ asset('/images/notificacoes/icon-12.png') }}" style="float:left;width:28px;height:28px;" />
                                                                    <div style="margin:0px 0px 0px 32px;">
                                                                        <strong style="display:block;font-weight:600;color:#004392;line-height:28px;"><span style="display:inline-block;line-height:normal;vertical-align:middle;">{{ $arte->nome }}</span></strong>
                                                                        <a href="{{ asset($arte->path) }}" style="color:#004392;">{{ $arte->nome_arquivo }}</a>
                                                                    </div>
                                                                </div>
                                                                @if ($arte->id != $artes->last()->id)<hr style="border:none;margin:30px 0px;border-bottom:1px dashed #676767;background:none;" />@endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="border:1px solid #ACACAC;background:#F4F4F4;">
                                                    <a style="display:block;color:#000000;line-height:26px;text-decoration:none;padding:15px;overflow:auto;" href="{{ route('site.artwork.ticket.edit.step1.get', [$ticket->id]) }}">
                                                        <img style="float:left;display:inline-block;" src="{{ asset('/images/backend/ticket.png') }}" title="" alt="" />
                                                        <span style="display:block;font-weight:600;vertical-align:middle;text-decoration:underline;word-break:break-all;margin-left:33px;">{{ $ticket->sku_nome_completo }}</span>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height:30px;"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <h4 style="font-size:20px;font-weight:600;margin:0px 0px 5px 0px;">Resumo do Ticket</h4>
                                                    <p style="margin:0px 0px 30px 0px;">{{ $ticket->descricao }}</p>
                                                    <ul style="font-weight:200;color:#000000;margin:0px;padding:0px;list-style:none;">
                                                        <li>&bull; Data de Início: {{ $ticket->dt_ini->format('d/m/Y') }}</li>
                                                        <li>&bull; Envio para Aprovação: {{ $ticket->dt_envio->format('d/m/Y') }}</li>
                                                        <li>&bull; Entrega no Fornecedor: {{ $ticket->dt_fim->format('d/m/Y') }}</li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="font-family:Arial, Helvetica, sans-serif;font-size:12px;text-align:center;width:800px;margin:0 auto;padding:40px 0px;">
                        <a style="display:inline-block;margin-bottom:20px;" href="http://www.rubrum.com.br" target="_blank"><img src="{{ asset('/images/backend/rubrum-logo.png') }}" title="" alt="" /></a>
                        <p style="margin:0px 0px 3px 0px;">
                            E-mail enviado automaticamente por Rubrum Software.
                        </p>
                        <p style="margin:0px 0px 3px 0px;">
                            Seu cadastro no Rubrum implica em envios obrigatórios de e-mails pelo sistema.
                        </p>
                        <p style="margin:0px 0px 30px 0px;">
                            Defina suas preferências de e-mail em <a style="color:#000000;" href="{{ url('/site/user') }}">Configurar Conta</a>, ou solicite a exclusão 
                            do seu cadastro para não receber mais e-mails.
                        </p>
                        <p style="text-transform:uppercase;margin:0px;">
                            Rubrum Tecnologia LTDA.
                        </p>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>--}}
    <table width="550" border="0" cellpadding="0" cellspacing="0" style="margin:0 auto;">
        <tbody>
            <tr>
                <td align="right"><img src="{{ asset('/images/layout/cliente_'. config('app.costummer_id') .'/logo-topo-email.png?v=31072024') }}" title="" alt=""></td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="550" border="0" cellpadding="0" cellspacing="0" style="margin:30px 0px;padding:25px;border:1px solid #676767;">
                        <tbody>
                            <tr>
                                <td align="left" width="400"><span style="display:inline-block;font-size:16px;color:#004392;font-weight:500;letter-spacing:1px;text-transform:uppercase;vertical-align:middle;padding:25px 25px 25px 0px;">{{ $ticket->sku_nome_completo }}</span></td>
                                <td align="right" width="250"><span style="display:inline-block;font-size:16px;color:#004392;font-weight:500;line-height:28px;vertical-align:middle;padding:25px 0px 25px 25px;border-left:1px dashed #676767;"><img src="{{ asset('/images/notificacoes/icon-versao-small.png') }}" title="" alt="" align="middle" style="float:left;margin-right:5px;width:28px;height:28px;" />Versão {{ ($ticket->artwItem->artwVersoes->last()) ? $ticket->artwItem->artwVersoes->last()->numero+1 : 1 }}</span></td>
                            </tr>
                        </tbody>
                    </table>                    
                </td>                
            </tr>
            <tr>
                <td colspan="2">
                    <table width="550" border="0" cellpadding="0" cellspacing="0" style="margin:5px 0px 30px 0px;">
                        <tbody>
                            <tr>
                                <td align="left">
                                    <h4 style="font-size:16px;font-weight:500;color:#676767;margin:0px 0px 10px 0px;">Arte-Final</h4>
                                    <p style="font-size:16px;font-weight:200;color:#676767;margin:0px 0px 30px 0px;">
                                        {{ trim(Auth::user()->nome . ' ' . Auth::user()->sobrenome) }} te enviou as seguintes artes-finais, clique nos links para baixá-las:        
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                            
                                    @foreach ($artes as $arte)
                                        <div>
                                            <img src="{{ asset('/images/notificacoes/icon-12.png') }}" style="float:left;width:28px;height:28px;" />
                                            <div style="margin:0px 0px 0px 32px;">
                                                <strong style="display:block;font-weight:600;color:#004392;line-height:28px;"><span style="display:inline-block;line-height:normal;vertical-align:middle;">{{ $arte->nome }}</span></strong>
                                                <a href="{{ asset($arte->path) }}" style="color:#004392;">{{ $arte->nome_arquivo }}</a>
                                            </div>
                                        </div>
                                        @if ($arte->id != $artes->last()->id)<hr style="border:none;margin:30px 0px;border-bottom:1px dashed #676767;background:none;" />@endif
                                    @endforeach
                                                                        
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr style="border:none;margin:0px 0px 30px 0px;border-bottom:1px solid #676767;background:none;" />
                    <h4 style="font-size:16px;font-weight:500;color:#676767;margin:0px 0px 10px 0px;">Layout em Baixa Resolução</h4>
                    <p style="font-size:16px;font-weight:200;color:#676767;margin:0px 0px 10px 0px;">
                        Segue anexo também um PDF de visualização da arte-final completa para conferência.
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>