<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <table style="margin:0 auto;padding:40px 100px 0px 100px;background:#F4F4F4;" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td style="font-family:Arial, Helvetica, sans-serif;border:1px solid #ACACAC;background:#FFFFFF;">
                    <table style="width:800px;" cellpadding="0" cellspacing="0">
                        <thead>	
                            <tr>
                                <th style="padding:70px 100px 50px 100px; border-bottom:2px dotted #ACACAC;">
                                    <a style="float:left;display:inline-block;" href="{{ url('/') }}"><img src="{{ asset('/images/layout/cliente_'. config('app.costummer_id') .'/logo-topo-email.png?v=31072024') }}" title="" alt=""></a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="padding:80px 100px 90px 100px;">
                                    <table style="width:100%;" cellpadding="0" cellspacing="0"> 
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <h3 style="font-size:20px;font-weight:600;margin:0px 0px 50px 0px; padding-bottom:15px;border-bottom:1px solid #ACACAC;">Aprovado</h3>
                                                    <div style="margin-bottom:40px;">
                                                        <p style="margin:0px 0px 5px 0px;">{{ $notificacao->to->nome_abreviado }} possui a seguinte tarefa:</p>
                                                        <ul style="font-weight:200;color:#000000;margin:0px;padding:0px;list-style:none;">
                                                            <li>&bull; Autorizar o envio da arte-final para os fornecedores.</li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="border:1px solid #ACACAC;background:#F4F4F4;">
                                                    <a style="display:block;color:#000000;line-height:26px;text-decoration:none;padding:15px;overflow:auto;" href="{{ route('site.artwork.ticket.edit.step1.get', [$ticket->id]) }}">
                                                        <img style="float:left;display:inline-block;" src="{{ asset('/images/backend/ticket.png') }}" title="" alt="" />
                                                        <span style="display:block;font-weight:600;vertical-align:middle;text-decoration:underline;word-break:break-all;margin-left:33px;">{{ $ticket->sku_nome_completo }}</span>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height:30px;"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <h4 style="font-size:20px;font-weight:600;margin:0px 0px 5px 0px;">Resumo do Ticket</h4>
                                                    <p style="margin:0px 0px 30px 0px;">{{ $ticket->descricao }}</p>
                                                    <ul style="font-weight:200;color:#000000;margin:0px;padding:0px;list-style:none;">
                                                        <li>&bull; Data de Início: {{ $ticket->dt_ini->format('d/m/Y') }}</li>
                                                        <li>&bull; Envio para Aprovação: {{ $ticket->dt_envio->format('d/m/Y') }}</li>
                                                        <li>&bull; Entrega no Fornecedor: {{ $ticket->dt_fim->format('d/m/Y') }}</li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="font-family:Arial, Helvetica, sans-serif;font-size:12px;text-align:center;width:800px;margin:0 auto;padding:40px 0px;">
                        <a style="display:inline-block;margin-bottom:20px;" href="http://www.rubrum.com.br" target="_blank"><img src="{{ asset('/images/backend/rubrum-logo.png') }}" title="" alt="" /></a>
                        <p style="margin:0px 0px 3px 0px;">
                            E-mail enviado automaticamente por Rubrum Software.
                        </p>
                        <p style="margin:0px 0px 3px 0px;">
                            Seu cadastro no Rubrum implica em envios obrigatórios de e-mails pelo sistema.
                        </p>
                        <p style="margin:0px 0px 30px 0px;">
                            Defina suas preferências de e-mail em <a style="color:#000000;" href="{{ url('/site/user') }}">Configurar Conta</a>, ou solicite a exclusão 
                            do seu cadastro para não receber mais e-mails.
                        </p>
                        <p style="text-transform:uppercase;margin:0px;">
                            Rubrum Tecnologia LTDA.
                        </p>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    {{--<table width="550" border="0" cellpadding="0" cellspacing="0" style="margin:0 auto;">
        <tbody>
            <tr>
                <td align="right"><img src="{{ asset('/images/notificacoes/logo-rubrum-alt.png') }}" title="" alt="" align="middle" /></td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="550" border="0" cellpadding="0" cellspacing="0" style="margin:30px 0px;padding:25px;border:1px solid #676767;">
                        <tbody>
                            <tr>
                                <td align="left" width="400"><span style="display:inline-block;font-size:16px;color:#004392;font-weight:500;letter-spacing:1px;text-transform:uppercase;vertical-align:middle;padding:25px 25px 25px 0px;">{{ $ticket->sku_nome_completo }}</span></td>
                                <td align="right" width="250"><span style="display:inline-block;font-size:16px;color:#004392;font-weight:500;line-height:28px;vertical-align:middle;padding:25px 0px 25px 25px;border-left:1px dashed #676767;"><img src="{{ asset('/images/notificacoes/icon-ticket-small.png') }}" title="" alt="" align="middle" style="float:left;margin-right:5px;" />Ticket {{ $ticket->numero }}</span></td>
                            </tr>
                        </tbody>
                    </table>                    
                </td>                
            </tr>
            <tr>
                <td colspan="2">
                    <table width="550" border="0" cellpadding="0" cellspacing="0" style="margin:5px 0px 30px 0px;">
                        <tbody>
                            <tr>
                                <td align="left" width="160">
                                    <span style="display:inline-block;font-size:16px;color:#004392;font-weight:500;line-height:30px;vertical-align:middle;padding:25px 25px 25px 0px;">
                                        <img src="{{ asset('/images/notificacoes/icon-7.png') }}" title="" alt="" align="middle" style="float:left;margin-right:5px;" />
                                        <strong style="display:inline-block;font-weight:500;line-height:1em;width:93px;">Aprovado</strong>
                                    </span>
                                </td>
                                <td align="left" width="400">
                                    <h4 style="font-weight:500;color:#676767;margin:0px 0px 10px 0px;">{{ $notificacao->to->nome_abreviado }} possui a seguinte tarefa:</h4>
                                    <ul style="font-weight:200;color:#676767;margin:0px;padding:0px;list-style:none;">
                                        <li>&bull; Autorizar o envio da arte-final para os fornecedores.</li>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="550" border="0" cellpadding="0" cellspacing="0" style="margin-bottom:30px;padding:30px 0px 30px 0px;border-top:1px solid #676767;border-bottom:1px solid #676767;">
                        <tbody>
                            <tr>
                                <td>
                                    <h4 style="font-size:16px;font-weight:500;color:#676767;margin:0px 0px 10px 0px;">Descrição do Ticket</h4>
                                    <p style="font-size:16px;font-weight:200;color:#676767;margin:0px 0px 0px 0px;">
                                        {{ $ticket->descricao }}
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table width="400" border="0" cellpadding="0" cellspacing="0" style="margin:0 auto;padding:30px 0px 0px 0px;">
                                        <tbody>
                                            <tr>
                                                <td align="center">
                                                   <h4 style="font-size:16px;font-weight:500;color:#676767;margin:0px 0px 5px 0px;">Data de Início</h4>
                                                   <span style="font-size:16px;font-weight:200;color:#676767;">{{ $ticket->dt_ini->format('d/m/Y') }}</span> 
                                                </td>
                                                <td align="center">
                                                   <h4 style="font-size:16px;font-weight:500;color:#676767;margin:0px 0px 5px 0px;">Envio para Aprovação</h4>
                                                   <span style="font-size:16px;font-weight:200;color:#676767;">{{ $ticket->dt_envio->format('d/m/Y') }}</span> 
                                                </td>
                                                <td align="center">
                                                   <h4 style="font-size:16px;font-weight:500;color:#676767;margin:0px 0px 5px 0px;">Entrega no Fornecedor</h4>
                                                   <span style="font-size:16px;font-weight:200;color:#676767;">{{ $ticket->dt_fim->format('d/m/Y') }}</span> 
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>        
                </td>    
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <a href="{{ route('site.artwork.ticket.edit.step1.get', [$ticket->id]) }}"><img src="{{ asset('images/notificacoes/btn-acessar-ticket.png') }}" align="middle" title="" alt=""></a>
                </td>
            </tr>
            @if ($notificacao->publico == 'seguidores')
            <tr>
                <td colspan="2">
                    <p style="font-size:14px;font-weight:200;color:#676767;text-align:center;margin:30px 0px;">
                        Você recebeu este e-mail porque optou por Seguir este ticket. 
                        Caso não queira mais receber, desmarque esta opção em Editar no cabeçalho do ticket.
                    </p>
                </td>
            </tr>
            @endif
        </tbody>
    </table>--}}
</body>
</html>