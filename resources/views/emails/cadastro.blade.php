<table style="margin:0 auto;padding:40px 100px 0px 100px;background:#F4F4F4;" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="font-family:Arial, Helvetica, sans-serif;border:1px solid #ACACAC;background:#FFFFFF;">
				<table style="width:800px;" cellpadding="0" cellspacing="0">
					<thead>	
						<tr>
							<th style="padding:70px 100px 50px 100px; border-bottom:2px dotted #ACACAC;">
								<a style="float:left;display:inline-block;" href="{{ url('/') }}"><img src="{{ asset('/images/layout/cliente_'. config('app.costummer_id') .'/logo-topo-email.png?v=31072024') }}" title="" alt=""></a>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="padding:80px 100px 90px 100px;">
								<table style="width:100%;" cellpadding="0" cellspacing="0"> 
									<tbody>
										<tr>
											<td>
												<h3 style="font-size:20px;font-weight:600;margin:0px 0px 30px 0px; padding-bottom:15px;border-bottom:1px solid #ACACAC;">Cadastro no Sistema</h3>
												<div style="margin-bottom:40px;">
													<p style="margin:0px 0px 5px 0px;">
														Ol&aacute; {{ $nome }},
													</p>
													<p style="margin:0px 0px 5px 0px;">
														Seu cadastro no sistema foi realizado com sucesso.
													</p>
													@if (str_contains($email, config('app.sso_dominio')) && config('app.sso_name'))
														<p style="margin:0px 0px 0px 0px;">
															Acesse o link abaixo e selecione a opção Acesso com Conta {{ config('app.sso_name') }}.
														</p>
														{{--<p style="margin:0px 0px 0px 0px;">
															Utilize as mesmas credenciais que usa internamente na empresa para acessar.
														</p>--}}
													@else
														<p style="margin:0px 0px 0px 0px;">
															Acesse o link abaixo para criar uma senha.
														</p>
													@endif
												</div>
											</td>
										</tr>
										@if (str_contains($email, config('app.sso_dominio')) && config('app.sso_name'))


										@else
										<tr>
											<td style="border:1px solid #ACACAC;background:#F4F4F4;">
												<a style="display:block;color:#000000;line-height:26px;text-decoration:none;padding:15px;overflow:auto;" href="{{ url('/login/token/' . $token) }}">
													<img style="float:left;display:inline-block;" src="{{ asset('/images/backend/ponteiro.png') }}" title="" alt="" />
													<span style="display:block;font-weight:600;vertical-align:middle;text-decoration:underline;word-break:break-all;margin-left:33px;">{{ url('/login/token/' . $token) }}</span>
												</a>
											</td>
										</tr>
										@endif
										<tr>
											<td style="height:30px;"></td>
										</tr>
										<tr>
											<td style="border:1px solid #ACACAC;background:#F4F4F4;">
												<div style="display:block;color:#000000;line-height:26px;text-decoration:none;padding:15px;overflow:auto;">
													@if (str_contains($email, config('app.sso_dominio')) && config('app.sso_name'))
														<a style="display:inline-block;text-decoration:none;color:#000000;border:none;" href="{{ url('/') }}">
															<img style="float:left;display:inline-block;border:none;text-decoration:none;" src="{{ asset('/images/backend/nuvem.png') }}" title="" alt="" />
															<strong style="display:block;font-size:16px;font-weight:600;vertical-align:middle;word-break:break-all;margin-left:43px;">Acesso com Conta {{ config('app.sso_name') }}</span></strong>
															<strong style="display:block;font-size:14px;font-weight:600;vertical-align:middle;word-break:break-all;margin-left:43px;"><span style="font-weight:100;">Utilize as mesmas credenciais que usa internamente na empresa para acessar.</span></strong>
														</a>
													@else
														<img style="float:left;display:inline-block;" src="{{ asset('/images/backend/usuario_2.png') }}" title="" alt="" />
														<strong style="display:block;font-weight:600;vertical-align:middle;word-break:break-all;margin-left:33px;">Usuário: <span style="font-weight:100;">{{ $email }}</span></strong>
														{{--<strong style="display:block;font-weight:600;vertical-align:middle;word-break:break-all;margin-left:33px;">Token: <span style="font-weight:100;">{{ $token }}</span></strong>--}}
													@endif
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<div style="font-family:Arial, Helvetica, sans-serif;font-size:12px;text-align:center;width:800px;margin:0 auto;padding:40px 0px;">
					<a style="display:inline-block;margin-bottom:20px;" href="http://www.rubrum.com.br" target="_blank"><img src="{{ asset('/images/backend/rubrum-logo.png') }}" title="" alt="" /></a>
					<p style="margin:0px 0px 3px 0px;">
						E-mail enviado automaticamente por Rubrum Software.
					</p>
					<p style="margin:0px 0px 3px 0px;">
						Seu cadastro no Rubrum implica em envios obrigatórios de e-mails pelo sistema.
					</p>
					<p style="margin:0px 0px 30px 0px;">
						Defina suas preferências de e-mail em <a style="color:#000000;" href="{{ url('/site/user') }}">Configurar Conta</a>, ou solicite a exclusão 
						do seu cadastro para não receber mais e-mails.
					</p>
					<p style="text-transform:uppercase;margin:0px;">
						Rubrum Tecnologia LTDA.
					</p>
				</div>
			</td>
		</tr>
	</tbody>
</table>
{{--<table width="500" cellspacing="0" cellpadding="30" border="0" style="margin: 0 auto;">
	<tr>
		<td width="600">
			<div style="padding:72px;border-radius:5px;border:1px solid #ACACAC;">
				<table width="600" cellspacing="0" cellpadding="0" border="0" style="margin: 0 auto;">
					<tr>
						<td width="600" style="font-size:18px;font-family: Arial, Verdana, Tahoma;font-weight:700;color:#000000;">
                            <h3>Cadastro</h3>
                            <br />
						</td>
					</tr>
					<tr>
						<td width="600" style="font-size:14px;font-family: Arial, Verdana, Tahoma;font-weight:300;color:#000000;">
							Ol&aacute; {{ $nome }}, <br /><br />
							Seu cadastro no sistema foi realizado com sucesso! <br />
							Utilize os dados de acesso abaixo para entrar no sistema através do link:<br /><br />
                            <a href="{{ url('/') }}" style="text-decoration:underline;color:#000000" target="_blank">{{ url('/') }}</a><br /><br />
							Login: <strong>{{ $email }}</strong><br />
							Senha Provisória: <strong>{{ $senha }}</strong><br /><br />
                            <span>No primeiro acesso, será necessário definir sua senha definitiva.</span>
                            <br />
                            <br />
                            <br />
                            <br />
						</td>
					</tr>
					<tr>
						<td width="600" style="font-size:14px;font-family: Arial, Verdana, Tahoma;font-weight:300;color:#000000;">
							<img src="{{ asset('/images/backend/emkt_rubrum_logo.png') }}" border="0" style="display:block; margin:0px;" />
						    <br />
                            <br />
						</td>
					</tr>
					<tr>
						<td width="600" style="font-family: Arial, Verdana, Tahoma;text-align:left;font-size:14px;font-weight:300;color:#ACACAC;">
							<span>E-mail enviado por Rubrum Software</span>
						</td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
</table>--}}

