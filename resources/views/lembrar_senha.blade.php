<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Rubrum Software - Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais</title>
    <script type="text/javascript" src="{{ asset('/js/site/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset2('/js/site/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset2('/js/site/jquery-confirm-config.js') }}"></script>
    <script type="text/javascript" src="{{ asset2('/js/site/OverlayScrollbars.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset2('/js/site/my-alert.js') }}"></script>
    <script type="text/javascript" src="{{ asset2('/js/site/login.js') }}"></script>
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/fontawesome/4.7.0/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/OverlayScrollbars.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/jquery-confirm.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/cliente_'. config('app.costummer_id') .'/layout.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/cliente_'. config('app.costummer_id') .'/home-v2.css') }}" />
    <!-- Favico -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=RyQmEa60ez">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=RyQmEa60ez">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=RyQmEa60ez">
    <link rel="manifest" href="/site.webmanifest?v=RyQmEa60ez">
    <link rel="mask-icon" href="/safari-pinned-tab.svg?v=RyQmEa60ez" color="#c41425">
    <link rel="shortcut icon" href="/favicon.ico?v=RyQmEa60ez">
    <meta name="apple-mobile-web-app-title" content="Rubrum">
    <meta name="application-name" content="Rubrum">
    <meta name="msapplication-TileColor" content="#c41425">
    <meta name="theme-color" content="#ffffff">
    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" />
</head>
<body>
    <div class="master">
        <div class="wrapped">
            <div class="home-container txt-center">
                <div class="home-container-box">
                    <span class="brand"></span>
                    <section>
                        <p class="txt-regular">
                            Digite o e-mail cadastrado para recuperar seu acesso.
                        </p>
                        {!! Form::open(array('route' => 'site.post.forgot', 'class' => 'home-container-form form-reminder')) !!}
                            <div class="block margin-top-30">    
                                <div class="flexbox-container margin-bottom-20">
                                    <div class="input-group animate">
                                        {!! Form::text('email', '', array('class' => 'input-control input-email-only', 'id' => 'inputEmail', 'placeholder' => 'E-mail', 'autofocus')) !!}
                                        <span class="addon fa fa-user-circle"></span>
                                    </div>
                                </div>
                                <div class="btn-group margin-bottom-10">
                                    {!! Form::button('<span><i class="icon pull-left margin-right-10 fa fa-key"></i><i class="icon pull-left no-margin fa fa-spin fa-circle-o-notch"></i><strong>Recuperar Acesso e Senha</strong></span>', array('type' => 'submit', 'class' => 'btn-control btn-positive btn-enviar btn-recuperar-acesso')) !!}
                                </div>
                                <div class="btn-group no-margin">
                                    {!! Html::decode(Html::link('/', '<span><i class="icon pull-left margin-right-10 fa fa-arrow-left"></i>Voltar</span>', array('class' => 'btn btn-control btn-hollow btn-link animate'))) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </section>
                </div>
                <footer class="footer clearfix">
                    <div class="copyright pull-left">
                        <span class="txt-uppercase">Rubrum Software &copy; {{ date("Y") }}</span>
                    </div>
                    <a href="http://www.rubrum.com.br" target="_blank" class="footer-brand pull-right"></a>
                </footer>
            </div>
        </div>
    </div>
    @if (session('status'))
        <script>
            myAlert('{{ status }}', 'error');
        </script>
    @endif
    @if (count($errors) > 0)
        <script>
            @foreach ($errors->all() as $error)
                myAlert('{{ $error }}', 'error');
            @endforeach
        </script>
    @endif
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-101521196-1', 'auto');
        ga('send', 'pageview');
    </script>
</body>
</html>