<div class="sidebar">

	@if(isset($categorias) || isset($tiposEmbalagens))
		<div class="sidebar-title">
			<h4>FILTRO</h4>
		</div>
		<div class="sidebar-form">
			{!! Form::open(array('class' => '', 'id' => 'form-category', 'method' => 'get', 'url' => Request::url())) !!}
				@if(isset($categorias))
					{!! Form::select('categorias', $categorias, null, array('id' => 'categoria', 'placeholder' => 'Categorias',)) !!}
				@endif
				@if(isset($tiposEmbalagens))
					{!! Form::select('tiposEmbalagens', $tiposEmbalagens, null, array('id' => 'embalagem', 'placeholder' => 'Tipos de Embalagens',)) !!}
				@endif
				<br />
				{!! Form::submit('Filtrar', array('id' => 'submit-filtro')) !!}
			{!! Form::close() !!}
		</div>
		<div class="separate-box-filter"></div>
	@endif

	@if(!Auth::guest())
		<div class="sidebar-favorites">
			<h4>FAVORITOS</h4>
		</div>
		<div class="sidebar-favorites-list">
			<ul>
				@if(count($favorites) > 0)
					@foreach($favorites as $favorite)
						<li>
							{{ Html::linkRoute('site.search.product', $favorite['nome'], array($favorite['family'], $favorite['product'])) }}
						</li>
					@endforeach
					<li class="last">
						{{ Html::linkRoute('site.more.favorites', 'Ver Mais') }}
					</li>
				@else
					<li>Adicione favoritos clicando no ícone de estrela ao lado do nome dos produtos.</li>
				@endif
			</ul>
		</div>
		<div class="separate-box-filter"></div>
	@endif

	<div class="sidebar-lasts">
		<h4>RECENTES</h4>
	</div>
	<div class="sidebar-favorites-list">
		<ul>
			@foreach($recentSku as $sku)
				<li>
					{{ Html::linkRoute('site.sku', $sku->nome, array($sku->produtos->id_familia, $sku->id_produto, 'sku' => $sku->id)) }}
				</li>
			@endforeach
			<li class="last">
				{{ Html::linkRoute('site.more.lasts', 'Ver Mais') }}
			</li>
		</ul>
	</div>

</div>