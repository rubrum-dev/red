<div id="token" class="modal-user">

	<h3 id="title">COMPARTILHAR LINK</h3>

	<div class="user-box">

		<div class="input-box" style="margin: 0 auto 25px; float: none; width: 90%">
			<center>
				{!! Form::text('nome', '', array('class' => 'input-text', 'id' => 'show-token', 'style' => 'margin-bottom: 15px; width: 90%;', 'disabled')) !!}
				Selecione e copie o link acima para compartilha-lo. Ele poderá ser acessado por 15 dias,
				após esse prazo será expirado. Caso queira renová-lo, basta acessar a
				página de gerenciamento de tokens.
			</center>
		</div>

	</div>

</div>
