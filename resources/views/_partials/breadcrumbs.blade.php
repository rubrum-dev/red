@if ($breadcrumbs)
	<div class="breadcumbs-out clearfix">		
		<div class="breadcrumbs-inner content-fluid clearfix">	
		    <ul>
		        @foreach ($breadcrumbs as $breadcrumb)
		            @if (!$breadcrumb->last)
		                <li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
					@else
		                <li class="active">{{ $breadcrumb->title }}</li>
		            @endif
		        @endforeach
				{{--<li class="breadcrumbs-back"><a href="{{ url()->previous() }}"><span class="icon icon-arrow-left-light"></span> Voltar</a></li>--}}
		    </ul>
		</div>
	</div>
@endif