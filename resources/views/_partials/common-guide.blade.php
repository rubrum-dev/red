<script src="{{ asset('/js/site/guide.js') }}"></script>

<!-- Script e CSS de Modal -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.7.0/jquery.modal.js"></script>
<script src='//cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js'></script>
<link href='//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.7.0/jquery.modal.css' rel='stylesheet' type='text/css'>

@include('_partials._modal-token')

<input id="family" type="hidden" value="{{ $produto->id_familia }}">
<input id="product" type="hidden" value="{{ $produto->id }}">
<input id="guide" type="hidden" value="{{ $guide }}">

<div class="top-head">
	<div class="top-inline top-packing-logo">
		<a href="javascript:;" class="logo-down"><span class="logo-down-ico">Baixar Logo</span></a>
		<img src="{{asset('/images/layout/logo-brahma-small.png')}}" alt="" width="90" height="90" />
		<!--
		{{ Html::image(asset($produto->logotipo), 'Produto') }}
		-->
		<!--
		@if(!Auth::guest())
			<center>
				<div class="star-favorite">
					<a href="javascript:void(0)">
						@if(in_array($produto->id, $user->favorites))
							{{ Html::image(asset('/images/icons/frontend/favorite-set-yes.png'), 'favorite', array('id' => 'imgSkuTitle')) }}
						@else
							{{ Html::image(asset('/images/icons/frontend/favorite-set-no.png'), 'favorite', array('id' => 'imgSkuTitle')) }}
						@endif
						<a href="javascript:void(0)" class="texto-favorito-top">
							Favorito
						</a>
					</a>
				</div>
			</center>
		@endif
		@if(!Auth::guest() && (in_array(Auth::user()->id_adm_perfil, [1, 2]) || Auth::user()->token_guide))
			<center style="margin-top: 5px">
				<a href="#token" title="Compartilhar Link" class="btn btn-primary btn-sm" id="share-link" rel="modal:open"
					data-id-prod="{{ $produto->id }}" data-toggle="modal" data-url-token="{{ Request::url() }}">
					Compartilhar Link
				</a>
			</center>
		@endif
		-->
	</div>
	<div class="top-inline top-head-title top-head-title-center">
		<h2><span>{{ mb_strtoupper($produto->nome) }}</span></h2>
		<!--
		<div style="width: 100%;">
			<h3>{{ mb_strtoupper($produto->nome) }}</h3>
			<div class="links-paginas-top">
			@if($token)
				{{ HTML::linkRoute('site.guide', 'Versões', array($produto->id_familia, $produto->id, 'versoes', 'token' => $token), array('id' => 'versoes')) }}
				@if($guide !== 'versoes' && $guide !== 'cores')
					:
				@endif
				{{ HTML::linkRoute('site.guide', 'Cores', array($produto->id_familia, $produto->id, 'cores', 'token' => $token), array('id' => 'cores')) }}
				@if($guide !== 'cores' && $guide !== 'proporcoes')
					:
				@endif
				{{ HTML::linkRoute('site.guide', 'Proporções', array($produto->id_familia, $produto->id, 'proporcoes', 'token' => $token), array('id' => 'proporcoes')) }}
				@if($guide !== 'proporcoes' && $guide !== 'usos-aplicacoes')
					:
				@endif
				{{ HTML::linkRoute('site.guide', 'Aplicações', array($produto->id_familia, $produto->id, 'usos-aplicacoes', 'token' => $token), array('id' => 'usos-aplicacoes')) }}<br />
				{{ HTML::linkRoute('site.guide', 'Usos Incorretos', array($produto->id_familia, $produto->id, 'usos-incorretos', 'token' => $token), array('id' => 'usos-incorretos')) }}
				@if($guide !== 'usos-incorretos' && $guide !== 'tipografias')
					:
				@endif
				{{ HTML::linkRoute('site.guide', 'Tipografias', array($produto->id_familia, $produto->id, 'tipografias', 'token' => $token), array('id' => 'tipografias')) }}
				@if($guide !== 'tipografias' && $guide !== 'icones')
					:
				@endif
				{{ HTML::linkRoute('site.guide', 'Ícones', array($produto->id_familia, $produto->id, 'icones', 'token' => $token), array('id' => 'icones')) }}
			@else
				{{ HTML::linkRoute('site.guide', 'Versões', array($produto->id_familia, $produto->id, 'versoes'), array('id' => 'versoes')) }}
				@if($guide !== 'versoes' && $guide !== 'cores')
					:
				@endif
				{{ HTML::linkRoute('site.guide', 'Cores', array($produto->id_familia, $produto->id, 'cores'), array('id' => 'cores')) }}
				@if($guide !== 'cores' && $guide !== 'proporcoes')
					:
				@endif
				{{ HTML::linkRoute('site.guide', 'Proporções', array($produto->id_familia, $produto->id, 'proporcoes'), array('id' => 'proporcoes')) }}
				@if($guide !== 'proporcoes' && $guide !== 'usos-aplicacoes')
					:
				@endif
				{{ HTML::linkRoute('site.guide', 'Aplicações', array($produto->id_familia, $produto->id, 'usos-aplicacoes'), array('id' => 'usos-aplicacoes')) }}<br />
				{{ HTML::linkRoute('site.guide', 'Usos Incorretos', array($produto->id_familia, $produto->id, 'usos-incorretos'), array('id' => 'usos-incorretos')) }}
				@if($guide !== 'usos-incorretos' && $guide !== 'tipografias')
					:
				@endif
				{{ HTML::linkRoute('site.guide', 'Tipografias', array($produto->id_familia, $produto->id, 'tipografias'), array('id' => 'tipografias')) }}
				@if($guide !== 'tipografias' && $guide !== 'icones')
					:
				@endif
				{{ HTML::linkRoute('site.guide', 'Ícones', array($produto->id_familia, $produto->id, 'icones'), array('id' => 'icones')) }}
			@endif
			</div>
		</div>
		-->
	</div>
	<div class="top-inline top-nav-right">
		<ul class="top-inline">
			@if(!Auth::guest())
				@if(in_array($produto->id, $user->favorites))
					<li class="favorite"><a href="javascript:;" class="favorite-set-yes"><span class="icon icon-star"></span> Favorito</a></li>
				@else
					<li class="favorite"><a href="javascript:;"><span class="icon icon-star"></span> Adicionar Favorito</a></li>
				@endif
			@endif
			@if($produto->logoDown)
				<li class="download"><a href="javascript:;" class="down-manual"><span class="icon icon-shield"></span> Manual da Marca</a></li>
			@endif
		</ul>
	</div>
</div>
