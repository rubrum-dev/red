@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- DataTables -->
<script src="{{ asset('/js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/datatables/accent-neutralise.js') }}"></script>

{!! Breadcrumbs::renderIfExists('notificacoes.index') !!}

<div class="content content-fluid clearfix">
    <div class="manager-container margin-top-40 margin-bottom-40">
        <section>
            <h3 class="master-title">Notificações</h3>
            <form>
                <table cellpading="0" cellspacing="0" id="myTable4" class="manager-table table-border table-hover">
                    <thead>
                        <tr>
                            <th width="65"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">ID</small></span></th>
                            <th width=""><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Remetente</small></span></th>
                            <th width=""><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Destinatário</small></span></th>
                            <th width="300"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Assunto</small></span></th>
                            <th width="120"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Público</small></span></th>
                            <th width="100"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Data</small></span></th>
                            <th width="140"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Status de envio</small></span></th>
                            <th width="130"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Data de envio</small></span></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($notificacoes as $notificacao)
                            <tr>
                                <td width=""><span class="txt-small">{{ $notificacao['id'] }}</span></td>
                                <td width=""><span class="txt-small"><strong class="txt-small txt-semibold">{{ $notificacao['remetente'] }}</strong><br>{{ $notificacao['remetente_email'] }}</span></td>
                                <td width=""><span class="txt-small"><strong class="txt-small txt-semibold">{{ $notificacao['destinatario'] }}</strong><br>{{ $notificacao['destinatario_email'] }}</span></td>
                                <td width=""><span class="txt-small"><a href="{{ $notificacao['link'] }}" target="_blank" class="txt-small">{{ $notificacao['assunto'] }}</a></span></td>
                                <td width=""><span class="txt-small">{{ $notificacao['publico'] }}</span></td>
                                <td width=""><span class="txt-small">{{ $notificacao['data'] }}</span></td>
                                <td width=""><span class="txt-small">{{ $notificacao['envio_status'] }}</span></td>
                                <td width=""><span class="txt-small">{{ $notificacao['envio_data'] }}</span></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </form>
        </section>
        <script>
            $(function() {
                var dataTableToolbarBtnDOM = '';
                var table = $('#myTable4').DataTable({
                    dom: '<"datatable-toolbar flexbox-container clearfix"' +
                            '<"datatable-filter-container flex flex-3-large"f>' +
                            '<"datatable-filter-container filter-by-status flex flex-3-large">' +
                        '>' +
                        '<"clearfix"' +
                            '<tr>' +
                        '>' +
                        '<"datatable-pagination flexbox-container flex-align-center clearfix"' +
                            '<"flex"<"select-control"l>><"flex flex-auto"p><"flex"i>' +
                        '>',
                    bAutoWidth: false,
                    processing: true,
                    serverSide: false,
                    stateSave: false,
                    pageLength: 25,
                    lengthMenu: [
                        [10, 25, 50, -1], 
                        ['Exibir 10 por página', 'Exibir 25 por página', 'Exibir 50 por página', 'Exibir todos']
                    ],
                    columnDefs: [
                        //{ orderable: false, targets: 5 }
                    ],
                    order: [[0, 'desc']],
                    language: {
                        "url": "/js/datatables/language/Portuguese-Brasil.json"
                    },
                    initComplete: function () {
                        this.api().columns().every( function () {
                            var column = this;
                            var select = $('<select><option value=""></option></select>')
                                .appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );
                                    column
                                        .search( val ? '^'+val+'$' : '', true, false )
                                        .draw();
                                });
                            column.data().unique().sort().each( function ( d, j ) {
                                select.append( '<option value="'+d+'">'+d+'</option>' )
                            });
                        });

                        $('.datatable-filter-container .dataTables_filter input[type="search"]').attr('placeholder', 'Busca');

                        var dataTables_filter_cancel = '<i></i>';
                        var dataTables_filter_search = '<i></i>';
                        
                        $('.datatable-filter-container .dataTables_filter').append(dataTables_filter_search);
                        $('.datatable-filter-container .dataTables_filter').append(dataTables_filter_cancel);
                        $('.datatable-filter-container .dataTables_filter i').addClass('dataTables_filter_search');
                        $('.datatable-filter-container .dataTables_filter i + i').addClass('dataTables_filter_cancel');
                        
                        function dataTablesFilterCancelShowHide() {
                            if ($('.datatable-filter-container .dataTables_filter input[type="search"]').val() !== '') {
                                $('.dataTables_filter_search').hide();
                                $('.dataTables_filter_cancel').show();
                            
                            } else {
                                $('.dataTables_filter_search').show();
                                $('.dataTables_filter_cancel').hide();
                                
                                table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
                            }
                        }
                        
                        $(document).on('click', '.datatable-filter-container .dataTables_filter_cancel', function () {
                            $('#myTable3_filter input[type="search"]').val('');
                            
                            dataTablesFilterCancelShowHide();
                        });

                        // Remove accented character from search input as well
                        $(document).on('keyup', '#myTable3_filter input[type="search"]', function () {
                            table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
                            
                            dataTablesFilterCancelShowHide();
                        });
                        
                        $('.dataTable thead').show();
                        $('.dataTables_filter').show();
                        $('.datatable-toolbar').append(dataTableToolbarBtnDOM);
                    },
                    fnDrawCallback: function( oSettings ) {
                        if(oSettings.aiDisplay.length <= 0) {
                            $('.dataTable thead').hide();
                            $('.datatable-pagination').hide();
                            $('.dataTables_empty').addClass('fa-user');
                            $('.dataTables_empty').text('Nenhuma notificação encontrada.');
                        } else {
                            $('.dataTable thead').show();
                            $('.datatable-pagination').show();
                            $('.dataTables_empty').removeClass('fa-user');
                            $('.dataTables_empty').text('');
                        }
                    }
                });
            });
        </script>
    </div>
</div>
@stop