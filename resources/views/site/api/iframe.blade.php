<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/fontawesome/4.7.0/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/three-dots/dist/three-dots.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/jquery-confirm.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/OverlayScrollbars.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/cliente_'. config('app.costummer_id') .'/compare.css') }}" />
    <script src="{{ asset('/js/angular/angular.min.js') }}"></script>
    <script src="{{ asset('/js/angular/angular-sanitize.min.js') }}"></script>
    <script src="{{ asset('/js/site/jquery.min.js') }}"></script>
    <script src="{{ asset2('/js/site/jquery-confirm.min.js') }}"></script>
    <script src="{{ asset2('/js/site/OverlayScrollbars.min.js') }}"></script>
    <script src="{{ asset2('/js/site/api/compare.js') }}"></script>
    {{--<script src="{{ asset('/js/site/site.js') }}"></script>--}}
    <title>Rubrum Software - Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais</title>

    <script>
    //document.domain = 'api-testes.rubrum.com.br';
    </script>

</head>
<body>
    <header>
		<div class="header-in center">
            <span class="header-brand"></span>
            <div class="header-right">
                <!-- TODO - Botões de Aprovação -->
                @if (isset($voltar) && $voltar)
                    <a href="{{ $voltar }}" class="btn btn-back"><span><i class="icon fa fa-arrow-left"></i> Voltar</span></a>
                @endif
                <div class="ticket-col d-inline-flex flex-align-center">
                    <!-- TODO - Comparação - Ciclo Atual -->
                    @if (isset($ticketLabel1) && $ticketLabel1)
                        <span @if (isset($ticketBadge1) && $ticketBadge1) class="cycle-badge comparing" @else class="cycle-badge" @endif>
                            @if (isset($ticketBadge1) && $ticketBadge1)
                                <i class="cycle-badge-addon left">{{ $ticketBadge1 }}</i>
                            @endif
                            <strong>{{ $ticketLabel1 }}</strong></span>
                    @endif
                    <!-- TODO - Inspector com 1 PDF ou Reprovação -->
                    <!--span class="cycle-badge"><strong>Ciclo 3</strong></span-->
                    <!-- TODO - Nome do Ticket -->
                    <h4 class="txt-ticket-info">{{ $ticketNumero }} • {{ $ticketTitulo }}</h4>
                    <!-- TODO - Comparação - Ciclo Reprovado ou Versão de Mercado -->
                    @if (isset($ticketLabel2) && $ticketLabel2)
                        <span class="cycle-badge comparing">
                            <strong>{{ $ticketLabel2 }}</strong>
                            @if (isset($ticketBadge1) && $ticketBadge1)
                                <i class="cycle-badge-addon right">{{ $ticketBadge2 }}</i>
                            @endif
                        </span>
                    @endif
                </div>
                <!-- TODO - Botões de Aprovação -->
                @if (isset($prosseguir) && $prosseguir)
                    <a id="inspector-proceed" class="btn btn-proceed" href="{{ $prosseguir }}"><span><i class="icon fa fa-arrow-right"></i> Prosseguir</span></a>
                @endif
            </div>
        </div>
	</header>
	<div class="contents">
        <div class="loading-big lds-css ng-scope">
		    <div class="line-wobble"></div>
	    </div>	
        <iframe frameborder="0" src="{{ $url }}?{{ time() }}" id="myFrame" class="frame"></iframe>
    </div>
</body>
</html>