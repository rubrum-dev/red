<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" />
	<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/fontawesome/4.7.0/css/font-awesome.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset2('/css/three-dots/dist/three-dots.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/cliente_'. config('app.costummer_id') .'/compare.css') }}" />
    <script src="{{ asset('/js/angular/angular.min.js') }}"></script>
    <script src="{{ asset('/js/angular/angular-sanitize.min.js') }}"></script>
	<script src="{{ asset('/js/site/jquery.min.js') }}"></script>
	<title>Rubrum Software - Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais</title>
</head>
<body>
	<header>
		<div class="header-in center">
			{{--<img src="{{ asset('images/layout/logo-header.png') }}" alt="Logo">--}}
			<span class="header-brand"></span>
			@if (isset($file))
            	<div class="txt-files-basename">
					{{ $file }}
				</div>
            @else
				<div class="txt-files-basename">
					<small class="files-old"><i class="fa fa-level-down file-1"></i><span>{{ $file1 }}</span></small>
					<small class="files-current"><span>{{ $file2 }}</span><i class="fa fa-level-down file-2"></i></small>
				</div>
            @endif
			<a href="javascript:;" onclick="window.close();" class="close">
				<i class="btn-icon fa fa-times-circle"></i>
			</a>
		</div>
	</header>
	<div class="contents">
		<div class="lds-css">
			<div style="width:100%;height:100%" class="lds-rolling">
				<div class="lds-in">
					<div class="dot-overtaking"></div>
				</div>
			</div>
		</div>
		<iframe frameborder="0" src="{{ $url }}?{{ time() }}" id="myFrame" class="frame"></iframe>
	</div>
</body>
</html>