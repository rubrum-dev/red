@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('familias') !!}

<div class="content content-fluid">
	<div class="packshelf-container margin-top-10 margin-bottom-30">
		<section class="brand-list">
			<h3 class="master-title">Marcas</h3>
			<ul class="tabs clearfix">
				<li><a href="javascript:;" class="txt-color-default active">Marcas de Mercado</a></li>
			</ul>
			<div class="block margin-top-20 margin-bottom-20">
				<div class="flexbox-container">
					@if(isset($categorias) || isset($tiposEmbalagens))
						<div class="flexbox-column flex flex-fluid">
							{!! Form::open(array('class' => '', 'id' => 'form-category', 'method' => 'get', 'url' => Request::url())) !!}
								<div class="flexbox-group flex flex-4-large">
									<div class="select-control">
										@if(isset($categorias))
											{!! Form::select('categorias', $categorias, app('request')->input('categorias'), array('id' => 'categoria', 'placeholder' => 'Categorias', 'onchange' => 'this.form.submit()')) !!}
										@endif
										@if(isset($tiposEmbalagens))
											{!! Form::select('tiposEmbalagens', $tiposEmbalagens, null, array('id' => 'embalagem', 'placeholder' => 'Tipos de Embalagens', 'onchange' => 'this.form.submit()')) !!}
										@endif
									</div>
								</div>
							{!! Form::close() !!}
						</div>
					@endif
				</div>
			</div>
			@if(count($familias) > 0)
				<div class="listing">
					<ul>
						@foreach($familias as $familia)
							@foreach($familia->logotipos as $logotipo)
								<li>
									@if($logotipo->id_tipo_logotipo == 1)
										<div style="background-color: {{ $familia->cor_hexa }}" class="box-image animate">
											<a href="{{ route('site.index') . '/' . $familia->id . '/produtos/' . Request::route('modulo') }}" title="{{ $familia->nome }}"></a>
											<div class="box-image-thumb-redim">	
												{{--<img src="{{ asset($logotipo->thumb) }}" title="{{$familia->nome}}" alt="{{ $familia->nome }}" data-adaptive-background class="box-image-logo" />--}}
												<img src="{{ asset($logotipo->thumb) }}" title="{{$familia->nome}}" alt="{{ $familia->nome }}" class="box-image-logo" />
											</div>
										</div>
									@endif
								</li>
							@endforeach
						@endforeach
					</ul>
				</div>
			@else
				<div class="is-empty-big">
					<span class="fa fa-cubes">Nenhum item localizado</span>
				</div>
			@endif
		</section>
	</div>
</div>
@stop