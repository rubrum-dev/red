<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Rubrum Software - Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais</title>
	<!-- Favico -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=RyQmEa60ez">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=RyQmEa60ez">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=RyQmEa60ez">
    <link rel="manifest" href="/site.webmanifest?v=RyQmEa60ez">
    <link rel="mask-icon" href="/safari-pinned-tab.svg?v=RyQmEa60ez" color="#c41425">
    <link rel="shortcut icon" href="/favicon.ico?v=RyQmEa60ez">
    <meta name="apple-mobile-web-app-title" content="Rubrum">
    <meta name="application-name" content="Rubrum">
    <meta name="msapplication-TileColor" content="#c41425">
    <meta name="theme-color" content="#ffffff">
	<!-- Fonts -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" />
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/fontawesome/4.7.0/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/politica-privacidade.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/cliente_2/layout.css') }}" />
	<!-- Scripts Locais -->
	<script src="{{ asset2('/js/site/jquery.min.js') }}"></script>
    <script src="{{ asset2('/js/site/jquery.nicescroll.js') }}"></script>
    <script src="{{ asset2('/js/site/jquery.visualnav.min.js') }}"></script>
    <script src="{{ asset2('/js/site/privacy.js') }}"></script>
</head>
<body>
    <div class="privacidade-container">
		<!-- Header -->
		<header class="header">
			<div class="header-in content-fluid clearfix">
				<span class="header-brand"></span>
			</div>
		</header>
		<!-- END Header -->
		<!-- DIV Wrapped  -->
		<div class="wrapped">
			<div class="container clearfix">
				<div class="breadcumbs-out clearfix">		
                    <div class="breadcrumbs-inner content-fluid clearfix">	
                        <ul>
                            <li class="active">Termos de Uso</li>
                        </ul>
                    </div>
                </div>
                <div class="min-height-container">
                    <div class="content content-fluid">
                        <div class="privacidade">
                            <div class="privacidade-wp">
                                <div class="sidebar">
                                    <ul class="sidebar-privacidade-ul">
                                        <li><a href="#intro" class="privacidade-link">Introdução</a></li>
                                        <li><a href="#clausula_1" class="privacidade-link">Da Atualizalçao e Disponibilização</a></li>
                                        <li><a href="#clausula_2" class="privacidade-link">Do Objeto</a></li>
                                        <li><a href="#clausula_3" class="privacidade-link">Do Serviço</a></li>
                                        <li><a href="#clausula_4" class="privacidade-link">Do Consentimento</a></li>
                                        <li><a href="#clausula_5" class="privacidade-link">Do Cadastro</a></li>
                                        <li><a href="#clausula_6" class="privacidade-link">Das Responsabilidades dos Usuários</a></li>
                                        <li><a href="#clausula_7" class="privacidade-link">Das Permissões e Direitos dos Usuários</a></li>
                                        <li><a href="#clausula_8" class="privacidade-link">Das Responsabilidades da Plataforma</a></li>
                                        <li><a href="#clausula_9" class="privacidade-link">Dos Direitos Autorais</a></li>
                                        <li><a href="#clausula_10" class="privacidade-link">Da Política de Confidencialidade e Sigilo</a></li>
                                        <li><a href="#clausula_11" class="privacidade-link">Das Sanções</a></li>
                                        <li><a href="#clausula_12" class="privacidade-link">Do Prazo e da Rescisão</a></li>
                                        <li><a href="#clausula_13" class="privacidade-link">Da Vigência e das Alterações deste Instrumento</a></li>
                                        <li><a href="#clausula_14" class="privacidade-link">Do Foro</a></li>
                                    </ul>
                                    <a href="/" class="link-voltar">Retornar a página inicial &laquo;</a>
                                </div>
                                <div class="content-right">
                                    <h1 class="txt-x-bold margin-bottom-30">Termos e Condições de Uso</h1>
                                    <section id="intro" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">INTRODUÇÃO</h3>
                                        <p>
                                            Este instrumento contém os termos gerais e condições de uso dos serviços de SaaS (Software como Serviço – Software as a Service) disponibilizados pela RUBRUM TECNOLOGIA LTDA., pessoa jurídica de direito privado, inscrita no CNPJ sob nº. 33.824.009/0001-76, com sede na Avenida Onze de Junho, 884, Vila Clementino, CEP 04041-003, na cidade de São Paulo, doravante denominada RUBRUM.
                                        </p>
                                    </section>
                                    <section id="clausula_1" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">CLÁUSULA 1ª: DA ATUALIZAÇÃO E DISPONIBILIZAÇÃO</h3>
                                        <p>
                                            <strong class="txt-bold">1.1.</strong> O presente instrumento foi atualizado e disponibilizado em 01/05/2023, incluindo cláusulas relacionadas à confidencialidade e sigilo de informações, e em conformidade com a Lei nº 13.709/18 – Lei Geral de Proteção de Dados (LGPD).
                                        </p>
                                    </section>
                                    <section id="clausula_2" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">CLÁUSULA 2ª: DO OBJETO</h3>
                                        <p>
                                            <strong class="txt-bold">2.1.</strong> O objeto deste documento é delimitar as condições de uso do software da RUBRUM, bem como de suas ferramentas, condições, direitos e obrigações das partes envolvidas.
                                        </p>
                                        <p>
                                            <strong class="txt-bold">2.2.</strong> Qualquer usuário que pretenda utilizar os serviços do sistema deverá, indispensavelmente, aceitar estes Termos e todas as demais políticas eventualmente disponibilizadas pela plataforma.
                                        </p>
                                    </section>
                                    <section id="clausula_3" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">CLÁUSULA 3ª: DO SERVIÇO</h3>
                                        <p>
                                            <strong class="txt-bold">3.1.</strong> O sistema caracteriza-se pela prestação dos seguintes serviços:
                                        </p>
                                        <ul class="list-terms">
                                            <li>
                                                <p>
                                                    Disponibilização dos módulos ArtWork, WorkFlow, TechDraw e PackShelf do software, realizada de forma irrevogável, não exclusiva e intransferível por meio da internet, na modalidade de Software as a Service (SaaS), incluindo os serviços de hospedagem do software e banco de dados;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Relacionados ao uso do software, tais como implementação, parametrização, treinamento de usuários e atualização do sistema, com fins de arquivamento, gerenciamento, aprovação e distribuição de artes-finais, arquivos e imagens em alta resolução;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Prestação de suporte técnico e resolução de problemas, se necessário e em conformidade com o Contrato de Prestação de Serviços;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Manutenção de backups com viés de segurança, assegurando a salvaguarda dos dados carregados pelo usuário no software.
                                                </p>
                                            </li>
                                        </ul>
                                        <p>
                                            <strong class="txt-bold">3.2.</strong> O serviço contratado contempla a versão atual e vigente do software. Fica a critério da RUBRUM realizar melhorias e atualizações no software, assumindo como responsabilidade a notificação do usuário a respeito das novas diretrizes a entrar em vigor.
                                        </p>
                                    </section>
                                    <section id="clausula_4" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">CLÁUSULA 4ª: DO CONSENTIMENTO</h3>
                                        <p>
                                            <strong class="txt-bold">4.1.</strong> Ao utilizar a plataforma, o usuário confirma que leu, entendeu e aceitou integralmente os presentes Termos, comprometendo-se a cumprí-los, sob o risco de aplicação das penalidades cabíveis.
                                        </p>
                                        <p>
                                            <strong class="txt-bold">4.2.</strong> O usuário, ao aceitar os Termos e a Política de Privacidade, autoriza expressamente a plataforma a operar com o tratamento de seus dados previsto no art. 5º, X da Lei 13.709/18 (Lei Geral de Proteção de Dados), incluindo todas as informações preenchidas pelo usuário no momento em que realizar ou atualizar seu cadastro, além de outras expressamente descritas na Política de Privacidade que deverá ser autorizada pelo usuário.
                                        </p>
                                        <p>
                                            <strong class="txt-bold">4.3.</strong> Em caso de persistência no uso da plataforma, entende-se como implícita a anuência do usuário aos novos Termos já em vigor.
                                        </p>                                            
                                    </section>
                                    <section id="clausula_5" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">CLÁUSULA 5ª: DO CADASTRO</h3>
                                        <p>
                                            <strong class="txt-bold">5.1.</strong> O acesso às funcionalidades da plataforma exigirá a realização de cadastro prévio, em que o usuário deve informar obrigatoriamente: e-mail válido, nome, sobrenome, empresa, departamento e, de maneira opcional, telefone e cargo na empresa.
                                        </p>
                                        <p>
                                            <strong class="txt-bold">5.2.</strong> Após a confirmação do cadastro, o usuário adquire login e senha pessoal, conferindo-o conta particular e intransferível.
                                        </p>
                                        <p>
                                            <strong class="txt-bold">5.3.</strong> Com a efetivação do cadastro, o usuário declara que é o responsável legal e possui autorização para realizar o gerenciamento de embalagens pelo software, tendo tal atribuição atrelada ao seu posto como funcionário das empresas vinculadas a RUBRUM ou seu posto como funcionário da RUBRUM.
                                        </p>
                                        <p>
                                            <strong class="txt-bold">5.4.</strong> Nesse sentido, o usuário obriga-se a:
                                        </p>
                                        <ul class="list-terms">
                                            <li>
                                                <p>
                                                    Fornecer dados cadastrais verdadeiros, recentes e válidos, sob pena de responsabilização nos termos da legislação aplicável;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Garantir a confidencialidade e segurança dos dados cadastrais e de acesso, tal qual a criação de uma senha forte e o cuidado para com o seu sigilo;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Responder por todas as atividades realizadas com o uso da senha;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Informar prontamente a plataforma em caso de uso indevido e não autorizado dos dados de acesso, incluindo aqueles realizados anteriormente ao consentimento dos presentes Termos.
                                                </p>
                                            </li>
                                        </ul>
                                        <p>
                                            <strong class="txt-bold">5.4.1.</strong> A RUBRUM não é responsável por qualquer perda ou dano decorrente da falha do usuário em cumprir com o estabelecido nesta seção e não se responsabiliza pelas ações e danos decorrentes do uso irregular da conta de acesso pelo cliente ou terceiros, exceto se comprovada a falha por parte da plataforma.
                                        </p>
                                    </section>
                                    <section id="clausula_6" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">CLÁUSULA 6ª: DAS RESPONSABILIDADES DOS USUÁRIOS</h3>
                                        <p>
                                            <strong class="txt-bold">6.1.</strong> É de responsabilidade geral dos Usuários, que se aplica a todo e qualquer cargo específico:
                                        </p>
                                        <ul class="list-terms">
                                            <li>
                                                <p>
                                                    As informações e dados inseridos, produzidos e disponibilizados através de sua conta de acesso, sem que haja qualquer tipo de adulteração ou criação por parte do software;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    As obrigações perante terceiros decorrentes da utilização do software, incluindo contratuais, penais, trabalhistas, tributários e passivos regulatórios;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Garantir que seus arquivos estejam livres de quaisquer malware, vírus, cavalos de Troia, spyware, vermes, ou outro código malicioso ou danoso;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Defeitos, falhas ou vícios técnicos originados nos computadores, dispositivos móveis ou na rede utilizada pelo usuário e que possam vir a prejudicar o bom funcionamento do software;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Pela veracidade, proteção e atualização dos dados de acesso à sua conta (login e senha), nos termos da cláusula 5.4;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    O cumprimento de todas as leis aplicáveis ao seu negócio, incluindo leis e regulamentos, e quaisquer licenças ou contratos a que estiver obrigado;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Utilizar o software exclusivamente para fins legais, não veiculando conteúdos: 
                                                </p>
                                                <p>
                                                    (I) ilícitos; (II) informações sobre atividades ilegais; (III) incitação ao crime; (IV) divulgação de informação relativa à pirataria do software ou (V) pornografia;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Apresentação pessoal verídica em nome da empresa e não fraudulenta, afastando a hipótese de apresentar-se como outra pessoa, empresa ou entidade;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Trabalhar apenas com conteúdos lícitos na manipulação da plataforma, de modo que a responsabilidade por qualquer uso do software em discordância com a legislação recai integralmente sobre o usuário responsável por promover tal conteúdo;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Notificar os Gerentes, Administradores ou Suporte da RUBRUM acerca de eventuais falhas nocivas ao sistema e à empresa;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Pelo cumprimento e respeito ao conjunto de regras disposto neste Termo de Condições Gerais de Uso, na respectiva Política de Privacidade e na legislação nacional e internacional.
                                                </p>
                                            </li>
                                        </ul>
                                        <p>
                                            <strong class="txt-bold">6.1.1.</strong> É de responsabilidade específica dos Gerentes:
                                        </p>
                                        <ul class="list-terms">
                                            <li>
                                                <p>
                                                    Exigir que todos os usuários mantenham seus IDs (identidade do usuário) e senhas estritamente confidenciais às pessoas não autorizadas;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Monitorar todas as ações da plataforma e notificar, imediatamente, o Administrador sobre a suspeita ou confirmação de qualquer uso não autorizado de IDs;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    O sigilo para com as informações pessoais dos usuários gerenciados;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Excluir usuários do sistema cujo perfil remeta a funcionários desligados da empresa contratante.
                                                </p>
                                            </li>
                                        </ul>
                                        <p>
                                            <strong>6.1.2.</strong> É de responsabilidade específica do Administrador, para além de todas as previstas aos Gerentes:
                                        </p>
                                        <ul class="list-terms no-margin">
                                            <li>
                                                <p>
                                                    A permissão para que outros usuários tornem-se Administradores;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    As ações tomadas por usuários promovidos a Administradores por conta da deliberação do gestor do cargo;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Notificar a RUBRUM sobre o desligamento de usuários e organização do software.
                                                </p>
                                            </li>
                                        </ul>
                                    </section>
                                    <section id="clausula_7" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">CLÁUSULA 7ª: DAS PERMISSÕES E DIREITOS DOS USUÁRIOS</h3>
                                        <p>
                                            <strong class="txt-bold">7.1.</strong> É permitido categoricamente aos usuários, permissão esta que se aplica a todo e qualquer cargo específico:
                                        </p>
                                        <ul class="list-terms">
                                            <li>
                                                <p>
                                                    O acesso, quando conferido, a todas as funções presentes no sistema;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    O acesso, quando conferido, a todos os projetos aprovados, em aprovação e seu histórico;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Baixar e anexar arquivos de artes-final de embalagens e demais documentos anexados nos projetos aos quais possui acesso.
                                                </p>
                                            </li>
                                        </ul>
                                        <p>
                                            <strong class="txt-bold">7.1.1.</strong> Para além das permissões gerais, é permitido, especificamente, aos Gerentes:
                                        </p>
                                        <ul>
                                            <li>
                                                <p>
                                                    O cadastro de novos Gerentes;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    O gerenciamento e a efetivação do cadastro de usuários padrão;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Acesso a dados cadastrais, como nome, e-mail, telefone, cargo e departamento de todos os usuários do sistema.
                                                </p>
                                            </li>
                                        </ul>
                                        <p>
                                            <strong>7.1.2.</strong> Para além das permissões gerais e as permissões aos Gerentes, é permitido, ao Administrador:
                                        </p>
                                        <ul class="list-terms">
                                            <li>
                                                <p>
                                                    O cadastro de novos Administradores;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    O gerenciamento e a efetuação do cadastro de Gerente e de usuários padrão;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    O cadastro e gerenciamento dos parâmetros do sistema na área administrativa da RUBRUM, de suma importância para a manutenção da organização dos arquivos e versões de embalagens.
                                                </p>
                                            </li>
                                        </ul>
                                        <p>
                                            <strong>7.2.</strong> É de direito de todo e qualquer usuário da plataforma:
                                        </p>
                                        <ul class="list-terms">
                                            <li>
                                                <p>
                                                    A disponibilização integral do sistema, salvo em eventos de casos fortuitos e força maior respaldados na legislação;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Serviço de suporte técnico em conformidade com a Política de Suporte da RUBRUM;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Manutenção do sigilo de todas informações e dados pessoais.
                                                </p>
                                            </li>
                                        </ul>
                                        <p>
                                            <strong class="txt-bold">7.2.2</strong> O acesso dos usuários à plataforma 24 (vinte e quatro) horas por dia, 7 (sete) dias por semana será permitido a partir da utilização de todas as soluções técnicas à disposição do responsável pela plataforma. No entanto, a navegação na plataforma ou em alguma de suas páginas poderá ser interrompida, limitada ou suspensa para atualizações, modificações ou qualquer ação necessária ao seu bom funcionamento.
                                        </p>
                                    </section>
                                    <section id="clausula_8" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">CLÁUSULA 8ª: DAS RESPONSABILIDADES DA PLATAFORMA</h3>
                                        <p>
                                            <strong class="txt-bold">8.1.</strong> É de responsabilidade da RUBRUM:
                                        </p>
                                        <ul class="list-terms">
                                            <li>
                                                <p>
                                                    Manter as informações do usuário e seus registros de acesso em sigilo e armazenados em ambiente seguro, sendo respeitadas a intimidade, a vida privada, a honra e a imagem do usuário, em conformidade com as disposições da Lei nº 12.965/2014 e com a Política de Privacidade;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Realizar os serviços conforme as condições acordadas entre as partes, responsabilizando-se pelo funcionamento do software, pelas correções que eventualmente sejam necessárias e disponibilizando suporte técnico para esclarecimento de dúvidas em relação ao uso do software;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Adotar as medidas de segurança adequadas ao padrão de mercado para a proteção das informações do usuário. Contudo, o usuário, desde já, reconhece que nenhum sistema, servidor ou software está absolutamente imune a ataques, não sendo o software responsável por qualquer exclusão, obtenção, utilização ou divulgação não autorizada de informações que sejam resultante de ataques;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Garantir que o software funcionará regularmente, se respeitadas as condições de uso definidas nestes Termos.
                                                </p>
                                            </li>
                                        </ul>
                                        <p>
                                            <strong class="txt-bold">8.2.</strong> Nesse sentido, não configuram responsabilidades da RUBRUM:
                                        </p>
                                        <ul>
                                            <li>
                                                <p>
                                                    Eventuais erros e/ou falhas apresentadas pelo software que tenham por causa problemas nos computadores, dispositivos móveis, rede utilizada pelo usuário ou  qualquer causa além do razoável controle;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Problemas advindos de casos fortuitos ou de força maior, nos termos da legislação;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Eventuais problemas oriundos de ações de terceiros, que possam interferir no funcionamento da plataforma;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Danos causados a terceiros em razão de culpa ou dolo do usuário;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Revisar as informações de conta fornecidas pelo usuário, seja no que tange à precisão dos dados quanto à legalidade, licitude ou ameaça de violação.
                                                </p>
                                            </li>
                                        </ul>
                                    </section>
                                    <section id="clausula_9" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">CLÁUSULA 9ª: DOS DIREITOS AUTORAIS</h3>
                                        <p>
                                            <strong class="txt-bold">9.1.</strong> A estrutura do software, site ou aplicativo, as marcas, logotipos, nomes comerciais, layouts, gráficos e design de interface, imagens, ilustrações, fotografias, apresentações, vídeos, conteúdos escritos e de som e áudio, programas de computador, banco de dados, arquivos de transmissão e quaisquer outras informações e direitos de propriedade intelectual da razão social RUBRUM, observados os termos da Lei da Propriedade Industrial (Lei nº 9.279/96), Lei de Direitos Autorais (Lei nº 9.610/98) e Lei do Software (Lei nº 9.609/98), estão devidamente reservados.
                                        </p>
                                        <p>
                                            <strong class="txt-bold">9.2.</strong> São proibidas a exploração, cessão, imitação, cópia, plágio, aplicação de engenharia reversa, armazenamento, alteração, modificação de características, ampliação, sub licenciamento, venda, locação, doação, alienação, transferência, reprodução, integral ou parcial, de qualquer conteúdo do site ou do software.
                                        </p>
                                        <p>
                                            <strong class="txt-bold">9.3.</strong> O usuário que violar as proibições contidas na legislação sobre propriedade intelectual e nestes Termos Gerais e Condições de Uso será responsabilizado, civil e criminalmente, pelas infrações cometidas, além de ser excluído da base de dados do software.
                                        </p>
                                    </section>
                                    <section id="clausula_10" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">CLÁUSULA 10ª: DA POLÍTICA DE CONFIDENCIALIDADE E SIGILO </h3>
                                        <p>
                                            <strong class="txt-bold">10.1.</strong> O usuário sujeita-se, neste contrato, a manter a confidencialidade das informações que sejam fornecidas ou obtidas durante o período de vigência na RUBRUM, consequentemente de acesso ao software da plataforma, reconhecendo que as Informações Confidenciais em questão pertencem, não somente à RUBRUM, mas aos clientes cujos dados encontram-se armazenados na rede da empresa.
                                        </p>
                                        <p>
                                            <strong class="txt-bold">10.2.</strong> A confidencialidade das informações inclui a manutenção do sigilo e privação de divulgá-las, por qualquer meio pertinente, a pessoas não autorizadas a receberem-nas.
                                        </p>
                                        <p>
                                            <strong class="txt-bold">10.3.</strong> O usuário tem ciência de que manipula arquivos com direitos autorais e tem acesso a informações privilegiadas que não devem ser compartilhadas fora do escopo do projeto para o qual se destina.
                                        </p>
                                    </section>
                                    <section id="clausula_11" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">CLÁUSULA 11ª: DAS SANÇÕES </h3>
                                        <p>
                                            <strong class="txt-bold">11.1.</strong> Sem prejuízo das demais medidas legais cabíveis, fica a critério exclusivo da RUBRUM, a qualquer momento, advertir, suspender ou cancelar a conta do usuário:
                                        </p>
                                        <ul class="list-terms">
                                            <li>
                                                <p>
                                                    Que violar qualquer dispositivo do presente Termo;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Que descumprir com os seus deveres de usuário;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Que tenha qualquer comportamento fraudulento, doloso ou que ofenda a terceiros.
                                                </p>
                                            </li>
                                        </ul>
                                    </section>
                                    <section id="clausula_12" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">CLÁUSULA 12ª: DO PRAZO E DA RESCISÃO</h3>
                                        <p>
                                            <strong class="txt-bold">12.1.</strong> Os Termos Gerais e Condições de Uso e a respectiva Política de Privacidade permanecerão em vigor por tempo indeterminado.
                                        </p>
                                        <p>
                                            <strong class="txt-bold">12.2.</strong> A não observância das obrigações pactuadas nestes Termos de Uso ou da legislação aplicável poderá, sem prévio aviso, ensejar a imediata rescisão unilateral por parte da RUBRUM e o bloqueio de todos os serviços prestados ao Usuário.
                                        </p>
                                    </section>
                                    <section id="clausula_13" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">CLÁUSULA 13ª: DA VIGÊNCIA E DAS ALTERAÇÕES DESTE INSTRUMENTO</h3>
                                        <p>
                                            <strong class="txt-bold">13.1.</strong> O presente contrato vigerá enquanto perdurar o Contrato de Prestação de Serviços firmado entre as partes em separado.
                                        </p>
                                        <p>
                                            <strong class="txt-bold">13.2.</strong> Os itens descritos no presente instrumento poderão sofrer alterações, unilateralmente e a qualquer tempo, por parte da RUBRUM, para adequar, aprimorar ou modificar os serviços, bem como para atender novas exigências legais. As alterações serão veiculadas na conta do usuário e este, em comportamento inerte, opta por aceitar o novo conteúdo ou ativamente cancelar o uso dos serviços.
                                        </p>
                                        <p>
                                            <strong class="txt-bold">13.3</strong> O usuário que optar por usufruir dos serviços após as atualizações entrarem em vigor, demonstra que está ciente e concorda com os Termos revisados.
                                        </p>
                                    </section>
                                    <section id="clausula_14" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">CLÁUSULA 14ª: DO FORO</h3>
                                        <p>
                                            <strong class="txt-bold">13.1.</strong> O presente contrato será regido pela legislação brasileira, elegendo as partes o Foro da comarca de São Paulo, como único competente para conhecer e dirimir quaisquer questões oriundas do presente contrato.
                                        </p>
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="push"></div>
			</div>
			<div class="clear-footer"></div>
			<div class="footer clearfix">
				<div class="footer-inner content-fluid clearfix">
					<span class="copyright">Rumbrum Software &copy; {{ date("Y") }}</span>
					<ul>
						<li><a href="javascript:;" class="no-click no-hover">Versão 1.6.2</a></li>
					</ul>
					<div class="footer-right">
						<a target="_blank" href="http://www.rubrum.com.br" class="animate"></a>
					</div>
				</div>
			</div>
		</div>
		<!-- END DIV Wrapped -->
	</div>
</body>
</html>