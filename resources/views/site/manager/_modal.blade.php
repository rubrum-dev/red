<div id="user" class="modal-user">
	<div class="mask-load">
		<div id='load'>
			{{ Html::image(asset('/images/icons/frontend/load.gif'), 'Load') }}
		</div>
	</div>

	<h3 id="title">CADASTRO DE USUÁRIO</h3>

	<div class="user-box">
		<div class="tipo-nome">
			<span>Dados do Usuário</span>
			<hr>
		</div>

		{!! Form::open(array('class' => 'form-horizontal')) !!}
			{{ Form::hidden('id', '', array('id' => 'editId')) }}
			<div class="input-box">
			  Nome
			  {!! Form::text('nome', '', array('class' => 'input-text', 'id' => 'editNome')) !!}
			</div>
			<div class="input-box">
			  Sobrenome
			  {!! Form::text('sobrenome', '', array('class' => 'input-text', 'id' => 'editSobrenome')) !!}
			</div>
			<div class="input-box">
				Email
				{!! Form::text('email', '', array('class' => 'input-text', 'id' => 'editEmail')) !!}
			</div>
			<div class="input-box">
			  Telefone
			  {!! Form::text('fone', '', array('class' => 'input-text', 'id' => 'editFone')) !!}
			</div>
			<div class="input-box">
				Cargo
				{!! Form::text('cargo', '', array('class' => 'input-text', 'id' => 'editCargo')) !!}
			</div>
			<div class="input-box">
			  Departamento
			  {!! Form::text('depto', '', array('class' => 'input-text', 'id' => 'editDepto')) !!}
			</div>
			<div class="input-box select">
			  Perfil de Acesso<br />
			  <small>Define as permissões às famílias de produtos.</small>
			  {!! Form::select('id_adm_perfil', $profiles, null, array('placeholder' => 'Selecione o Perfil...', 'class' => 'input-text', 'id' => 'editPerfil')) !!}
			</div>
			<div class="input-box status-box">
				Status
				<div class="status-user">
					<span>{{ Form::radio('status', '1', true) }}&nbsp; Ativo</span>
					<span>{{ Form::radio('status', '0') }}&nbsp; Inativo</span>
				</div>
			</div>

			<div class="tipo-nome tipo-nome-password">
				<span>Módulos do Sistema</span>
				<hr>
			</div>

			<p class="p-modulos">Selecione os módulos que o usuário poderá ter acesso.</p>

			<div class="input-box-modulos">
				{{ Html::image(asset('/images/layout/logo_guide.png'), 'Guide') }}<br />
				{!! Form::checkbox('modulos[]', 1, null, array('id' => 'editModulo1')) !!}<br />
				Guia de Marcas
			</div>
			{{-- <div class="input-box-modulos modulos-center">
				{{ Html::image(asset('/images/layout/logo_sku.png'), 'File') }}<br />
				<span>
					{{ Form::checkbox('modulos[]', 3, null, array('id' => 'editModulo3')) }}<br />
					Imagem
				</span>
				<span>
					{{ Form::checkbox('modulos[]', 2, null, array('id' => 'editModulo2')) }}<br />
					Arte-Final
				</span>
			</div> --}}
			<div class="input-box-modulos modulos-center">
				{{ Html::image(asset('/images/layout/logo_sku.png'), 'File') }}<br />
				{{ Form::checkbox('modulos[]', 3, null, array('id' => 'editModulo3')) }}<br />
				Imagens
			</div>
			<div class="input-box-modulos">
				{{ Html::image(asset('/images/layout/logo_enxoval.png'), 'Enxoval') }}<br />
				{!! Form::checkbox('modulos[]', 4, null, array('id' => 'editModulo4')) !!}<br />
				Enxovais de Campanha
			</div>
			{!! Form::submit('Cadastrar', array('class' => 'btn-save btn-modal-user', 'id' => 'cadastro-save')) !!}
			<a href="javascript:void(0)" class="btn-save btn-modal-user" id="cancel-user">
				Cancelar
			</a>
		{!! Form::close() !!}

	</div>

</div>
