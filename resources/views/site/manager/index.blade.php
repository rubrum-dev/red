@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

<script src="{{ asset('/js/site/manager.js') }}"></script>

<!-- Script e CSS de Modal -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.7.0/jquery.modal.js"></script>
<script src='//cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js'></script>
<link href='//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.7.0/jquery.modal.css' rel='stylesheet' type='text/css'>

@include('site.manager._modal')

{!! Breadcrumbs::renderIfExists('user') !!}

<div class="principal">
	<div class="listagem listagem-users">
        <div class="top-title top-manager">
        	<div class="clearfix">
                <h3>Gerenciar Usuários</h3>
                <div class="links-paginas-top links-paginas-top-manager">
                    <a href="#user" class="link-user" rel="modal:open" data-toggle="modal">Novo Usuário</a>
                </div>
            </div>
        </div>
        <div class="list-more">
            @if(count($users) > 0)
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <th>Nome</th>
                        <th>Perfil de Acesso</th>
                        <th>Departamento</th>
                        <th>Status</th>
                        <th>Ações</th>
                    </thead>
            		<tbody>
            			@foreach($users as $usr)
            				<tr>
            					<td class="name-bold">{{ $usr->nome . ' ' . $usr->sobrenome }}</td>
                                <td>{{ $usr->admPerfis->nome }}</td>
            					<td>{{ $usr->depto }}</td>
            					<td>{{ $usr->status ? 'Ativo' : 'Inativo' }}</td>
            					<td>
            						@if($usr->id_adm_perfil != 1)
                                        <a href="#user" class="edit-user" id="edit-user-{{ $usr->id }}" title="Editar Usuário" rel="modal:open" data-toggle="modal" data-id-user="{{ $usr->id }}">
            								{{ Html::image(asset('/images/layout/edit-user.png'), 'Editar Usuário') }}
            							</a>
                                        <a href="javascript:void(0)" class="remove-user" id="remove-user-{{ $usr->id }}" data-id-user="{{ $usr->id }}" title="Remover Usuário">
                                            {{ Html::image(asset('/images/layout/remove-user.png'), 'Remover Usuário') }}
            							</a>
            						@endif
            					</td>
            				</tr>
            			@endforeach
            		</tbody>
            	</table>
                <center>
            		{!! $users->render() !!}
            	</center>
            @else
            	<div>
            		<h4>Nenhum Usuário Localizado</h4>
            	</div>
            @endif

            @if (session('status'))
                <div class="alert-success">
                    <ul>
                        <li>{{ session('status') }}</li>
                    </ul>
                </div>
            @endif

    	</div>
    </div>

    <!-- Importação do Sidebar -->
	@include('_partials.sidebar')

</div>

@stop
