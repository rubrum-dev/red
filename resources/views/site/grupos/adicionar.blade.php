@extends('site.site_template')
@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop
@section('content')
<div class="content">
    <div class="top-head">
        <div class="top-tickets-manager clearfix">
            <div class="top-inline top-head-title  manage-groups-title">
                <h2>Novo Tipo de Alteração e Grupo de Aprovação</h2>
            </div>
        </div>
    </div>
    <div class="container-grupos">
        <form name="addGroupsForm" method="POST" action="" class="form-grupos ticket-alt-request-form">
            <div style="margin:0px 0px 30px 0px;" class="clearfix">
                <div style="width:458px;" class="form-inline">
                    <label>Tipo de Alteração</label>
                    <input type="text" name="tipoAlteracao" id="tipoAlteracao" class="input-control" />
                </div>
                <div style="width:160px;" class="form-inline">
                    <label>Status</label>
                    <div class="select-control">
                        <select name="tipoAlteracaoStatus">
                            <option value="">Selecione</option>
                            <option value="1">Ativo</option>
                            <option value="2">Inativo</option>
                        </select>
                    </div>
                </div>
            </div>
            <hr />
            <h4>Grupos de Aprovação</h4>
            <div class="text-block">
                <p>
                    Adicione os usuários que necessariamente devem participar do Ciclo de Aprovação desse Tipo de Alteração.
                </p>
            </div>
            <fieldset>
                <div id="membershipWrap">
                    <div class="input-fields-group clearfix" ng-repeat="(k, participante) in participantes">
                        <div class="clearfix">
                            <div class="form-inline column-access-type">
                                <div class="form-inline column-membership">
                                    <label ng-show="k === 0">Participantes</label>
                                    <div class="select-control">
                                        <select ng-show="!participante.id || participante.id_usuario !== participante.id_usuario_logado || k === 0" ng-disabled="(participante.id_usuario !== participante.id_usuario_logado) || (k === 0)" name="cbMembershipName[]">
                                            <option value="" selected>Selecionar participante</option>
                                            <option ng-selected="participante.id_usuario == membro.id" ng-repeat="(k, membro) in membros" value="@{{membro.id}}">@{{membro.nome}}</option>
                                        </select>
                                        <!--    
                                        <select ng-show="participante.id && participante.id_usuario === participante.id_usuario_logado && k > 0" name="cbMembershipAltName[@{{k}}]">
                                            <option value="" selected>Selecionar participante</option>
                                            <option ng-selected="participante.id_usuario == membro.id" ng-repeat="(k, membro) in membros" value="@{{membro.id}}">@{{membro.nome}}</option>
                                        </select>
                                        <input ng-disabled="participante.id_usuario === participante.id_usuario_logado && k > 0" name="cbMembershipName[]" type="hidden" ng-value="participante.id_usuario">
                                        -->
                                    </div>
                                </div>
                                <div class="form-inline column-membership-profile">
                                    <label ng-show="k === 0">Perfil de Aprovação</label>
                                    <div class="select-control">
                                        <select ng-disabled="(participante.id) || (!participante.id && k > 0)" ng-model="cbMembershipProfile[k]" ng-init='cbMembershipProfile[k] = participante.id_perfil.toString()' name="cbMembershipProfile[@{{k}}]" class="members-profile-validate">
                                            <option value="" selected>Perfil</option>
                                            <option ng-selected="participante.id_perfil == 1" value="1">Aprovador</option>
                                            <option ng-selected="participante.id_perfil == 2" value="2">Marketing</option>
                                            @if (Auth::user()->id_adm_empresa != 2)
                                                <option ng-selected="participante.id_perfil == 3" value="3">Revisor</option>
                                            @endif
                                            <option ng-selected="participante.id_perfil == 4" value="4">Participante</option>
                                            @if (Auth::user()->id_adm_empresa != 2)
                                                <option ng-selected="participante.id_perfil == 5" value="5">Arte-finalista</option>
                                            @endif
                                        </select>
                                        <input ng-model="cbMembershipProfile[k]" name="cbMembershipProfile[@{{k}}]" type="hidden" ng-value="cbMembershipProfile[k]">
                                    </div>
                                </div>
                            </div>
                            <div class="form-inline column-membership-notify">
                                <label ng-show="k === 0">Seguir</label>
                                <div class="checkbox-control">
                                    <input ng-disabled="participante.id" ng-checked="participante.recebe_email == 1" value='1' type="checkbox" name="chkNotificationsSwitch[@{{k}}]" id="chkNotificationsSwitch_@{{k}}" />
                                    <input ng-disabled="!participante.id" name="chkNotificationsSwitch[]" type="hidden" ng-value="participante.recebe_email">
                                    <label for="chkNotificationsSwitch_@{{k}}"></label>
                                </div>
                            </div>
                            <div class="form-inline column-membership-team">
                                <label ng-show="k === 0">Equipe</label>
                                <a href="javascript:;" class="icon icon-male-symbol"></a>
                            </div>
                            <div class="form-inline btn-add column-membership-remove">
                                <label ng-show="k === 0">Excluir</label>
                                <button ng-show='!participante.id && k > 0' ng-click="remover(k)" type="button" id="btnAddArtworkAlt" class="btn-remove-members">
                                    <span class="icon-cross"></span>
                                </button>
                            </div>
                        </div>
                        <div style="border-top:1px dashed #676767;border-bottom:1px dashed #676767;margin-top:10px;padding:10px 0px;" class="clearfix">
                            <div class="input-fields-group clearfix">
                                <div class="form-inline">
                                    <div class="form-inline column-access-type">
                                        <span class="icon-bullet-tree icon-arrow-curve-right"></span>
                                        <div class="form-inline select-control" style="width:272px;">
                                            <select name="cbMembershipTeamName[]">
                                                <option value="" selected>Membros da equipe</option>
                                            </select>
                                        </div>
                                        <div class="form-inline">
                                            <div class="select-control column-membership-profile">
                                                <select name="cbMembershipTeamProfile[]" disabled>
                                                    <option value="" selected>Perfil</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-inline column-membership-notify">
                                        <div class="checkbox-control">
                                            <input ng-disabled="participante.id" ng-checked="participante.recebe_email == 1" value='1' type="checkbox" name="chkNotificationsSwitch[@{{k}}]" id="chkNotificationsSwitch_@{{k}}" />
                                            <input ng-disabled="!participante.id" name="chkNotificationsSwitch[]" type="hidden" ng-value="participante.recebe_email">
                                            <label for="chkNotificationsSwitch_@{{k}}"></label>
                                        </div>
                                    </div>
                                    <div class="form-inline btn-add column-membership-remove push-offset">
                                        <button type="button" id="btnAddArtworkAlt" class="btn-remove-team-members btn-remove-members">
                                            <span class="icon-cross"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="input-fields-group clearfix">
                                <div class="form-inline membership-team-col">
                                    <div class="form-inline column-access-type">
                                        <span class="icon-bullet-tree icon-arrow-curve-right"></span>
                                        <div class="form-inline select-control" style="width:272px;">
                                            <select name="cbMembershipTeamName[]">
                                                <option value="" selected>Membros da equipe</option>
                                            </select>
                                        </div>
                                        <div class="form-inline">
                                            <div class="select-control column-membership-profile">
                                                <select name="cbMembershipTeamProfile[]" disabled>
                                                    <option value="" selected>Perfil</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-inline btn-add">
                                            <button type="button" id="btnAddMembership" class="btn-add-team-members btn-add-fields">
                                                <span class="icon-plus"></span> Adcionar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                                        
                    <div class="input-fields-group clearfix">
                        <div class="form-inline column-membership">
                            <div class="select-control">
                                <select ng-model='add_participante'>
                                    <option value="" selected>Selecionar participante</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-inline column-membership-profile">
                            <div class="select-control">
                                <select class="members-profile-validate">
                                    <option value="" selected>Perfil</option>
                                    <option value="1">Aprovador</option>
                                    <option value="2">Marketing</option>
                                    <option value="3">Revisor</option>
                                    <option value="4">Participante</option>
                                    <option value="5">Arte-finalista</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-inline btn-add">
                            <button type="button" id="btnAddMembership" class="btn-add-membership btn-add-fields">
                                <span class="icon-plus"></span> Adicionar
                            </button>
                        </div>
                    </div>
                                        
                    <!--
                    <div class="input-fields-group clearfix">
                        <div class="form-inline column-membership">
                            <label ng-show="participantes.length === 0">Participantes</label>
                            <div class="select-control">
                                <select ng-model='add_participante'>
                                    <option value="" selected>Selecionar participante</option>
                                    <option ng-repeat="(k, membro) in membros" ng-if="!membro.ignore" value="@{{membro.id}}">@{{membro.nome}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-inline column-membership-profile">
                            <label ng-show="participantes.length === 0">Perfil de Aprovação</label>
                            <div class="select-control">
                                <select ng-model='add_perfil' class="members-profile-validate">
                                    <option value="" selected>Perfil</option>
                                    <option value="1">Aprovador</option>
                                    <option value="2">Marketing</option>
                                    @if (Auth::user()->id_adm_empresa != 2)
                                        <option value="3">Revisor</option>
                                    @endif
                                    <option value="4">Participante</option>
                                    @if (Auth::user()->id_adm_empresa != 2)
                                        <option value="5">Arte-finalista</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-inline btn-add">
                            <button ng-click="adicionar()" type="button" id="btnAddMembership" class="btn-add-membership btn-add-fields">
                                <span class="icon-plus"></span> Adicionar
                            </button>
                        </div>
                    </div>
                    -->
                </div>
            </fieldset>
            <div class="form-btn-group clearfix">
                <hr />
                <button type="button" class="call-to-action-btn">
					<span class="btn-icon icon-check"></span> Salvar
				</button>
            </div>
        </form>
    </div>
</div>
@stop