@extends('site.site_template')
@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop
@section('content')
<script type="text/javascript" src="{{ asset('/js/site/manage-groups-sorter.js') }}"></script>
<div class="content" ng-app="app" ng-controller="FornecedorAdicionarCtrl">
    <div class="top-head">
        <div class="top-tickets-manager clearfix">
            <div class="top-inline top-head-title  manage-groups-title">
                <h2>Tipos de Alteração e Grupos de Aprovação</h2>
            </div>
            <div class="top-inline manage-groups-nav">
				<ul>
				    <li><a href="javascript:;"><span class="icon-plus"></span> Novo Tipo de Alteração</a></li>
				</ul>
			</div>
        </div>
    </div>
    <div class="container-grupos">
        <form name="addGroupsForm" method="POST" action="" class="form-grupos ticket-alt-request-form">
            <table border="0" cellpadding="0" cellspacing="0" id="myTable2" class="table-gerenciar-grupos">
                <thead>
                    <tr>
                        <th width="700" onclick="sortTable(0)"><a href="javascript:;" class="sortable">Tipo de Alteração <span class="icon icon-arrow-down"></span></a></th>
                        <th width="100" onclick="sortTable(1)"><a href="javascript:;" class="sortable">Status <span class="icon icon-arrow-down"></span></a></th>
                        <th width="100">Editar</th>
                        <th width="100">Excluir</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="700">&bull; Texto Legal</td>
                        <td width="100">Ativo</td>
                        <td width="100"><a href="javascript:;" class="icon icon-options-settings"></a></td>
                        <td width="100"><a href="javascript:;" class="icon icon-cross"></a></td>
                    </tr>
                    <tr>
                        <td width="700">&bull; Elementos Gráficos</td>
                        <td width="100">Ativo</td>
                        <td width="100"><a href="javascript:;" class="icon icon-options-settings"></a></td>
                        <td width="100"><a href="javascript:;" class="icon icon-cross"></a></td>
                    </tr>
                    <tr>
                        <td width="700">&bull; Elementos Textuais</td>
                        <td width="100">Ativo</td>
                        <td width="100"><a href="javascript:;" class="icon icon-options-settings"></a></td>
                        <td width="100"><a href="javascript:;" class="icon icon-cross"></a></td>
                    </tr>
                    <tr>
                        <td width="700">&bull; Tabela Nutricional</td>
                        <td width="100">Ativo</td>
                        <td width="100"><a href="javascript:;" class="icon icon-options-settings"></a></td>
                        <td width="100"><a href="javascript:;" class="icon icon-cross"></a></td>
                    </tr>
                    <tr>
                        <td width="700">&bull; Composição do Produto</td>
                        <td width="100">Ativo</td>
                        <td width="100"><a href="javascript:;" class="icon icon-options-settings"></a></td>
                        <td width="100"><a href="javascript:;" class="icon icon-cross"></a></td>
                    </tr>
                    <tr>
                        <td width="700">&bull; Nova Identidade Visual (VBI)</td>
                        <td width="100">Ativo</td>
                        <td width="100"><a href="javascript:;" class="icon icon-options-settings"></a></td>
                        <td width="100"><a href="javascript:;" class="icon icon-cross"></a></td>
                    </tr>
                    <tr>
                        <td width="700">&bull; Outros</td>
                        <td width="100">Ativo</td>
                        <td width="100"><a href="javascript:;" class="icon icon-options-settings"></a></td>
                        <td width="100"><a href="javascript:;" class="icon icon-cross"></a></td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</div>
@stop