<div class="custom-alert-heading heading-news flex-align-center clearfix">
    <h4 class="flexbox-container flex-fluid flex-align-center no-margin">
        <strong class="">Resultados da Pesquisa de Satisfação</strong>
        <label class="flex-4-large dont-show-again switch-control switch-control-alt pull-right margin-left-auto"> 
            <input onclick="exibir_news()" value="1" type="checkbox" name="dontShowAgain" id="dontShowAgain" /> 
            <span class="switch-toggle pull-right margin-left-10"></span>
            <span class="abs-left-offset dont-show-again-txt switch-txt txt-small txt-bold margin-left-10">Não Exibir Novamente</span>
        </label>
    </h4> 
</div>
<p class="txt-regular txt-default">
    Um grande obrigado a todos que contribuíram para a nossa pesquisa! As respostas de vocês foram enriquecedoras e estamos ansiosos para 
    compartilhá-las. Para ter acesso antecipado aos resultados, siga-nos nas nossas redes sociais.
</p>
<div class="social-flex flexbox-container flex-align-center flexbox-group margin-top-40 animate">
    <div class="flex"><a href="https://www.linkedin.com/company/rubrumsoftware/" target="_blank" data-title="LinkedIn" class="ui-tooltip top fa fa-linkedin"></a></div>
    <div class="flex"><a href="https://www.instagram.com/rubrumsoftware/" target="_blank" data-title="Instagram" class="ui-tooltip top fa fa-instagram"></a></div>
    <div class="flex"><a href="https://web.facebook.com/rubrumsoftware" target="_blank" data-title="Facebook" class="ui-tooltip top fa fa-facebook"></a></div>
    <div class="flex"><a href="https://www.youtube.com/channel/UCXeE9f3g7wJLFdnHro12aSQ" target="_blank" data-title="YouTube" class="ui-tooltip top fa fa-youtube-play"></a></div>
    <div class="flex margin-left-15">
        <span class="d-inline-flex txt-color-alt">Siga nos nas redes: /rubrumsoftware</span>
    </div>
</div>
<p class="text-regular txt-default margin-top-40">
    Junte-se à nossa comunidade online! Lá, não só compartilhamos os resultados da pesquisa, mas também exploramos notícias e curiosidades do 
    universo das embalagens, além de oferecer dicas valiosas para impulsionar sua eficiência.
</p>