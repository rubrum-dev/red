@extends('site.site_template')
@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />

<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/fornecedor_gerenciar.js') }}"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- DataTables -->
<script src="{{ asset('/js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/datatables/accent-neutralise.js') }}"></script>
<script defer type="text/javascript" src="{{ asset2('/js/site/dataTables-resolve-loader.js') }}"></script>

{!! Breadcrumbs::renderIfExists('fornecedor.gerenciar') !!}

<div ng-app="app" ng-controller="FornecedorGerenciarCtrl" ng-cloak class="content content-fluid clearfix">
    <div class="manager-container margin-top-10 margin-bottom-40">
        <section>
            <h3 class="master-title margin-bottom-30">Gerenciar Fornecedores</h3>
            <div class="dt-content-container box-relative">            
                <form>
                    <table style="display:none;" cellpading="0" cellspacing="0" id="myTable4" class="table manager-table table-border table-hover">
                        <thead>
                            <tr>
                                <th width=""><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Fornecedores</small></span></th>
                                <th width="140"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Status</small></span></th>
                                <th width="50"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($fornecedores as $fornecedor)
                                <tr>
                                    <td width="">
                                        <a href="{{ route('site.fornecedores.editar.get', [$fornecedor->id]) }}">
                                            <i class="icon fa fa-industry"></i><span class="text-side">{{ $fornecedor->nome }}</span>
                                        </a>
                                    </td>
                                    <td width="140">{{ ($fornecedor->status == 1) ? 'Ativo' : 'Inativo' }}</td>
                                    <td width="50">
                                        <div class="dropdown drop-to-left">
                                            <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                            <div class="dropdown-list">
                                                <ul> 
                                                    <li><a href="{{ route('site.fornecedores.editar.get', [$fornecedor->id]) }}" class="txt-bold"><span class="icon fa fa-edit"></span>Alterar</a></li>
                                                    <li><a href="javascript:excluir('Excluir {{ $fornecedor->nome }}?', '{{ route('site.fornecedores.excluir.get', [$fornecedor->id]) }}')" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </form>
            </div>
        </section>
        <script>
            $(function() {
                $('.table').on( 'init.dt', function (e, settings) {
				    $('.datatable-pagination').hide();
                    setTimeout(function () {
                        if(settings.aiDisplay.length > 0) {
                            $('.datatable-pagination').show();
                        }		
                    }, new Date().getMilliseconds());
                });

                var dataTableToolbarBtnDOM = '<div class="flex flex-auto">' +
                    '<a href="{{ route('site.fornecedores.adicionar.get') }}" class="btn-function"><i class="icon fa fa-industry"></i> Adicionar Fornecedor</a>' +
                '</div>';
                var table = $('#myTable4').DataTable({
                    dom: '<"datatable-toolbar flexbox-container clearfix"' +
                            '<"datatable-filter-container flex flex-3-large"f>' +
                            '<"datatable-filter-container filter-by-status flex flex-3-large">' +
                        '>' +
                        '<"clearfix"' +
                            '<tr>' +
                        '>' +
                        '<"datatable-pagination flexbox-container flex-align-center clearfix"' +
                            '<"flex"<"select-control"l>><"flex flex-auto"p><"flex"i>' +
                        '>',
                    bAutoWidth: false,
                    processing: false,
                    serverSide: false,
                    stateSave: true,
                    pageLength: 25,
                    lengthMenu: [
                        [10, 25, 50, -1], 
                        ['Exibir 10 por página', 'Exibir 25 por página', 'Exibir 50 por página', 'Exibir todos']
                    ],
                    columnDefs: [
                        { orderable: false, targets: 2 }
                    ],
                    language: {
                        "url": "/js/datatables/language/Portuguese-Brasil.json"
                    },
                    initComplete: function () {
                        $('.datatable-filter-container .dataTables_filter input[type="search"]').attr('placeholder', 'Busca');

                        var dataTables_filter_cancel = '<i></i>';
                        var dataTables_filter_search = '<i></i>';
                        
                        $('.datatable-filter-container .dataTables_filter').append(dataTables_filter_search);
                        $('.datatable-filter-container .dataTables_filter').append(dataTables_filter_cancel);
                        $('.datatable-filter-container .dataTables_filter i').addClass('dataTables_filter_search');
                        $('.datatable-filter-container .dataTables_filter i + i').addClass('dataTables_filter_cancel');
                        
                        function dataTablesFilterCancelShowHide() {
                            if ($('.datatable-filter-container .dataTables_filter input[type="search"]').val() !== '') {
                                $('.dataTables_filter_search').hide();
                                $('.dataTables_filter_cancel').show();
                            
                            } else {
                                $('.dataTables_filter_search').show();
                                $('.dataTables_filter_cancel').hide();
                                
                                table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
                            }
                        }
                        
                        $(document).on('click', '.datatable-filter-container .dataTables_filter_cancel', function () {
                            $('#myTable4_filter input[type="search"]').val('');
                            
                            dataTablesFilterCancelShowHide();
                        });

                        $(document).on('keyup', '#myTable4_filter input[type="search"]', function () {
                            dataTablesFilterCancelShowHide();
                        });

                        $('.dataTable').show();
                        $('.dataTable thead').show();
                        $('.dataTables_filter').show();
                        $('.datatable-toolbar').append(dataTableToolbarBtnDOM);
                    },
                    fnDrawCallback: function( oSettings ) {
                        if(oSettings.aiDisplay.length <= 0) {
                            $('.dataTable thead').hide();
                            $('.dataTable thead tr').hide();
                            $('.datatable-pagination').hide();
                            $('.dataTables_empty').addClass('fa-industry');
                            $('.dataTables_empty').text('Nenhum fornecedor encontrado.');
                        } else {
                            $('.dataTable thead').show();
                            $('.dataTable thead tr').show();
                            $('.datatable-pagination').show();
                            $('.dataTables_empty').removeClass('fa-industry');
                            $('.dataTables_empty').text('');
                        }
                    }
                });
            });
        </script>
    </div>
</div>
@stop
