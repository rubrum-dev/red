@extends('site.site_template')
@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />

<script>
    var excluidos = [];

    @if (old('excluidos'))
        @foreach (old('excluidos') as $excluido)
            @if ($excluido)
                excluidos.push('{{ $id }}')
            @endif
        @endforeach
    @endif

    var campo_ids = [];

    @if (old('ids', $ids))
        @foreach (old('ids', $ids) as $id)
            @if ($id)
                campo_ids.push('{{ $id }}')
            @endif
        @endforeach
    @endif

    var campo_emails = [];

    @if (old('emails', $emails))
        @foreach (old('emails', $emails) as $email)
            @if ($email)
                campo_emails.push('{{ $email }}')
            @endif
        @endforeach
    @endif

    var campo_nomes = [];

    @if (old('nomes', $nomes))
        @foreach (old('nomes', $nomes) as $nome)
            @if ($nome)
                campo_nomes.push('{{ html_entity_decode($nome, ENT_COMPAT, 'ISO-8859-1') }}')
            @endif
        @endforeach
    @endif

    var template = {};

    if (campo_emails && campo_nomes)
    {
        var emails = [];

        for (index = 0; index < campo_emails.length; ++index) {

            template = {'id': campo_ids[index], 'email': campo_emails[index], 'nome': campo_nomes[index]};
            emails.push(template);
        }
    }
    else
    {
        var emails = [
            //{'id': '1', 'descricao': 'Teste 1'},
            //{'id': '2', 'descricao': 'Teste 2'}
        ]
    }
</script>
<script src="{{ asset('/js/angular/angular.min.js')}}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js')}}"></script>
<script src="{{ asset('/js/angular/angular-sanitize.min.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/fornecedor_editar.js')}}"></script>
<script src="{{ asset('/js/site/fornecedores.js?v=2.1.5')}}"></script>

{!! Breadcrumbs::renderIfExists('fornecedor.editar') !!}

<div class="content content-fluid clearfix" ng-app="app" ng-controller="FornecedorEditarCtrl" ng-cloak>
    @if (count($errors) > 0)
        <script>
            @foreach ($errors->all() as $error)
                myAlert('{{ $error }}', 'warning');
            @endforeach
        </script>
    @endif

    <div class="manager-container margin-top-10 margin-bottom-40">
        <section>
            <h3 class="master-title margin-bottom-30">Editar Fornecedor</h3>
            <form name="addSupplierForm" method="POST" action="" id="addSupplierForm" class="">
                <input ng-repeat="(k, excluido) in excluidos" type="hidden" name="excluidos[]" ng-value="excluido">
                <input type="hidden" name="_token" value="{{ csrf_token()}}">
                <div class="block margin-bottom-30">
                    <div class="flexbox-container flexbox-group">
                        <div class="flex flex-8-large">
                            <label class="label-control txt-small txt-uppercase margin-bottom-10">Razão Social *</label>
                            <input value="{{ old('nome', $fornecedor->nome) }}" type="text" autocomplete="off" name="nome" id="tfRazaoSocial" class="input-control" />
                        </div>
                        <div class="flex flex-2-large">
                            <label class="label-control txt-small txt-uppercase margin-bottom-10">Telefone</label>
                            <input value="{{ old('telefone', $fornecedor->telefone) }}" type="text" autocomplete="off" name="telefone" id="tfTelefone" class="phone-mask input-control" />
                        </div>
                        <div class="flex flex-2-large">
                            <label class="label-control txt-small txt-uppercase margin-bottom-10">Status *</label>
                            <div class="select-control">
                                <select name="status" id="sbSupplierStatus">
                                    <option value="">Selecione</option>
                                    <option {{ (old('status', $fornecedor->status) == '1') ? 'selected' : '' }} value="1">Ativo</option>
                                    <option {{ (old('status', $fornecedor->status) == '0') ? 'selected' : '' }} value="0">Inativo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block margin-bottom-30">
                    <table ng-show="emails.length" cellpading="0" cellspacing="0" class="profiles-table table-border table-hover margin-bottom-15">
                        <thead>
                            <th class="no-hover" width=""><span class="txt-small txt-uppercase">Destinatários</span></th>
                            <th class="no-hover" width="540"><span class="txt-small txt-uppercase">E-mail para receber Arte-final</span></th>
                            <th class="no-hover" width="50"></th>
                        </thead>
                        <tbody>
                            <tr ng-repeat="(k, email) in emails">
                                <td width="">
                                    <div class="d-flex flex-align-center">
                                        <i class="icon fa fa-envelope"></i>
                                        <span class="text-side" ng-bind-html="email.nome"></span> 
                                        <input type="hidden" name="nomes[]" ng-value="email.nome">
                                    </div>
                                </td>
                                <td width="540">
                                    <span class="txt-small" ng-bind-html="email.email"></span>
                                    <input type="hidden" name="emails[]" ng-value="email.email">
                                </td>
                                <td width="50">
                                    <div class="dropdown drop-to-left">
                                        <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                        <div class="dropdown-list">
                                            <ul> 
                                                <li><a ng-click="remover(k)" href="javascript:;" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                            </ul>
                                        </div>
                                    </div> 
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <input ng-disabled="emails.length > 0" type="hidden" name="nomes[]" value="">
                    <input ng-disabled="emails.length > 0" type="hidden" name="emails[]" value="">
                    <div ng-class="{'margin-top-15': emails.length}" class="flexbox-container flexbox-group">
                        <div class="col-responsavel flex flex-6-large">
                            <label ng-hide="emails.length" class="label-control txt-small txt-uppercase margin-bottom-10">Destinatário *</label>
                            <input ng-model='add_nome' type="text" autocomplete="off" placeholder="Nome" class="input-control" />
                        </div>
                        <div class="col-email-artwork flex flex-fluid">
                            <label ng-hide="emails.length" class="label-control txt-small txt-uppercase margin-bottom-10">E-mail para receber Arte-final *</label>
                            <div class="flexbox-container flexbox-group">
                                <div class="flex flex-fluid">
                                    <input ng-model='add_email' type="text" autocomplete="off" placeholder="E-mail" class="input-control input-email-only" />
                                </div>
                                <div class="flex col-destinatarios-add">
                                    <button ng-click="adicionar()" type="button" class="btn btn-control btn-positive">Adicionar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-group txt-center margin-top-30 no-margin-bottom">
                    <div class="bs border-color-primary padding-top-40">
                        <a href="{{ route('site.fornecedores.get') }}" class="btn-control btn-call-to-action call-to-action-negative btn-icon btn-icon-left">
                            <span><i class="icon fa fa-times"></i> Cancelar</span>
                        </a>
                        <button type="submit" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i> Salvar</span>
                        </button>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>
@stop
