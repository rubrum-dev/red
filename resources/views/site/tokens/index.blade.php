@extends('site.site_template')

@section('title')
    Gerenciamento de Tokens
@stop

@section('content')

<script src="{{ asset('/js/site/tokens.js') }}"></script>

<!-- Script e CSS de Modal -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.7.0/jquery.modal.js"></script>
<script src='//cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js'></script>
<link href='//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.7.0/jquery.modal.css' rel='stylesheet' type='text/css'>

@include('site.tokens._modal')

{!! Breadcrumbs::renderIfExists('token') !!}

<div class="principal">
	<div class="listagem listagem-tokens">
        <div class="top-title top-manager">
			<div class="clearfix">
				<h3>Gerenciar Tokens</h3>			
			</div>	
		</div>
		<div class="list-more" id="list-tokens">
            @if(count($tokens) > 0)
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <th>Produto</th>
        				<th>Token</th>
        				<th>Validade</th>
        				<th>Ações</th>
                    </thead>
                    <tbody>
            			@foreach($tokens as $token)
            				<tr>
            					<td>{{ $token->produtos->nome }}</td>
            					<td>{{ $token->token }}</td>
                                @if($token->dt_expiracao < date('d/m/Y'))
                                    <td style="color: #B20000">{{ $token->dt_expiracao }}</td>
                                @else
                                    <td>{{ $token->dt_expiracao }}</td>
                                @endif
            					<td>
                                    <a href="javascript:void(0)" title="Renovar Token" id="btn-edit-{{$token->id}}" class="btn btn-success btn-sm btn-extend" data-token-id="{{ $token->id }}">
            							{{ Html::image(asset('/images/layout/img-renew.png'), 'Renovar Token', array('style' => 'height:23px !important')) }}
            						</a>
            						<a href="javascript:void(0)" title="Remover Token" id="btn-remove-{{$token->id}}" class="btn btn-danger btn-sm btn-remove" data-token-id="{{ $token->id }}">
                                        {{ Html::image(asset('/images/layout/img-trash.png'), 'Remover Token', array('style' => 'height:23px !important')) }}
            						</a>
                                    <a href="#token" title="Copiar Link" class="btn btn-primary btn-sm copy-link" rel="modal:open" data-token-id="{{ $token->id }}" data-token-url="{{ $token->url }}" data-toggle="modal">
            							{{ Html::image(asset('/images/layout/img-link.png'), 'Copiar Link', array('style' => 'height:23px !important')) }}
            						</a>
            					</td>
            				</tr>
            			@endforeach
            		</tbody>
            	</table>
            @else
            	<div>
            		<h4>Nenhum Token Localizado</h4>
            	</div>
            @endif

    	</div>
    </div>

    <!-- Importação do Sidebar -->
	@include('_partials.sidebar')

</div>

@stop
