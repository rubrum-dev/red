@extends('site.site_template')
@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop
@section('content')
<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset('/js/angular/angular-ui/ui-mask/mask.min.js') }}"></script>

<script src="{{ asset2('/js/site/passwordCheckStrength.js') }}"></script>

<script src="{{ asset2('/js/angular/controllers/usuarios_config.js') }}"></script>

<div class="content content-fluid clearfix" ng-app="app" ng-controller="UsuariosConfigCtrl" ng-cloak>
    @if (count($errors) > 0)
        <div class="alert alert-danger" role="alert">
            <div class="alert-error">
                @foreach ($errors->all() as $error) {{ $error}}
                <br /> @endforeach
            </div>
        </div>
    @endif
    <div class="margin-top-40 margin-bottom-40">
        <section>
            <h3 class="master-title margin-bottom-30">Configurações da Conta</h3>
            <form autocomplete="off" ng-submit="add_usuario_submit($event)" name="formUserConfig" action="" method="POST" id="formUserConfig" class="form-usuarios">
                <input type="hidden" name="_token" value="{{ csrf_token()}}">
                <div class="block">
                    <div class="flexbox-container flexbox-group">
                        <div class="flex flex-3-large">
                            <label class="label-control txt-small txt-uppercase margin-bottom-10">Nome *</label>
                            <input type="text" autocomplete="off" ng-init="nome='{{ $user->nome }}'" ng-model="nome" name="nome" id="editNome" class="input-control" />
                        </div>
                        <div class="flex flex-3-large">
                            <label class="label-control txt-small txt-uppercase margin-bottom-10">Sobrenome *</label>
                            <input type="text" autocomplete="off" ng-init="sobrenome='{{ $user->sobrenome }}'" ng-model="sobrenome" name="sobrenome" id="editSobrenome" class="input-control" />
                        </div>
                        <div class="flex flex-4-large">
                            <label class="label-control txt-small txt-uppercase margin-bottom-10">E-mail *</label>
                            <input type="text" autocomplete="off" ng-init="email='{{ $user->email }}'" ng-model="email" name="email" disabled id="editEmail" class="input-control disabled" />
                        </div>
                        <div class="flex flex-2-large">
                            <label class="label-control txt-small txt-uppercase margin-bottom-10">Telefone</label>
                            <input ui-mask="@{{ phoneMask }}" ui-mask-placeholder mask-change="phoneMask" ng-init="telefone='{{ $user->fone }}'" ng-model="telefone" type="text" autocomplete="off" name="fone" id="editFone" class="input-control" />
                        </div>
                    </div>
                </div>
                <div class="block">
                    <div class="flexbox-container flexbox-group">
                        <div class="flex flex-fluid">
                            <label class="label-control txt-small txt-uppercase margin-bottom-10">Empresa *</label>
                            <div class="flexbox-container flexbox-group">
                                <div class="flex flex-3-large">
                                    <div class="select-control disabled">
                                        <!-- To-do -->
                                        <select disabled name="id_empresa" ng-model="id_empresa" ng-init="id_empresa = '{{ old('id_empresa', Auth::user()->admEmpresa->id) }}'" id="id_empresa">
                                            <option value="">Selecione...</option>
                                            @foreach ($empresas as $row)
                                                <option value="{{ $row->id }}">{{ $row->nome }}</option>
                                            @endforeach
                                        </select>
                                        <!-- -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block">
                    <div class="flexbox-container flexbox-group">
                        <div class="flex flex-fluid">
                            <label class="label-control txt-small txt-uppercase margin-bottom-10">Departamento *</label>
                            <div class="flexbox-container flexbox-group">
                                <div class="flex flex-3-large">
                                    <div id="select_id_departamento" class="select-control">
                                        <select name="id_departamento" ng-model="id_departamento" ng-init="id_departamento = '{{ old('id_departamento', $user->id_departamento) }}'" id="id_departamento">
                                            <option value="">Selecione...</option>
                                            <option value="0">Novo Departamento</option>
                                            @foreach ($departamentos as $row)
                                                <option value="{{ $row->id }}">{{ $row->nome }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="flex flex-3-large">
                                    <input value="{{ old('departamento_nome', $user->departamento_nome) }}" ng-show="id_departamento === '0'" ng-model="departamento_nome" type="text" placeholder="Nome do departamento" name="departamento_nome" id="departamento_nome" class="input-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block">
                    <div class="flexbox-container flexbox-group">
                        <div class="flex flex-fluid">
                            <label class="label-control txt-small txt-uppercase margin-bottom-10">Cargo</label>
                            <div class="flexbox-container flexbox-group">
                                <div class="flex flex-3-large">
                                    <div id="select_id_cargo" class="select-control">
                                        <select name="id_cargo" ng-model="id_cargo" ng-init="id_cargo = '{{ old('id_cargo', $user->id_cargo) }}'" id="id_cargo">
                                            <option value="">Selecione...</option>
                                            <option value="0">Novo Cargo</option>
                                            @foreach ($cargos as $row)
                                                <option value="{{ $row->id }}">{{ $row->nome }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="flex flex-3-large">
                                    <input value="{{ old('cargo_nome', $user->cargo_nome) }}" ng-show="id_cargo === '0'" type="text" ng-model="cargo_nome" placeholder="Nome do cargo" name="cargo_nome" id="cargo_nome" class="input-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block">
                    <div class="flexbox-container flexbox-group margin-bottom-30">
                        <div class="flex flex-fluid">
                            <label class="label-control txt-small txt-uppercase margin-bottom-10">Alterar a Senha</label>
                            <p class="txt-regular margin-bottom-15">
                                Para garantir uma senha segura utilize ao menos um de cada: letra maiúscula, letra minúscula e número.
                            </p>
                            <div class="flexbox-container flexbox-group">
                                <div class="flex flex-4-large">
                                    <input type="password" autocomplete="off" placeholder="Digite a senha atual" ng-model="old_password" name="old_password" id="inputOldPwd" class="input-control no-space" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flexbox-container flexbox-group">
                        <div class="flex flex-fluid">
                            <label class="label-control txt-small txt-uppercase margin-bottom-10">Nova Senha</label>
                            <div class="flexbox-container flexbox-group">
                                <div class="flex flex-4-large">
                                    <input type="password" autocomplete="off" placeholder="Digite a nova senha" ng-model="password" name="password" id="inputNewPwd" class="input-control no-space" />
                                    <div id="popover-password">
                                        <div class="progress margin-top-15 margin-bottom-10">
                                            <div id="password-strength" 
                                                class="progress-bar" 
                                                role="progressbar" 
                                                aria-valuenow="40" 
                                                aria-valuemin="0" 
                                                aria-valuemax="100" 
                                                style="width:0%">
                                            </div>
                                        </div>
                                        <ul class="list-unstyled">
                                            <li><span class="upper-case"><i class="fa fa-circle" aria-hidden="true"></i>Deve conter letra maiúscula</span></li>
                                            <li><span class="lower-case"><i class="fa fa-circle" aria-hidden="true"></i>Deve conter letra minúscula</span></li>
                                            <li><span class="one-number"><i class="fa fa-circle" aria-hidden="true"></i>Deve conter número (0 - 9)</span></li>
                                            <li><span class="six-character"><i class="fa fa-circle" aria-hidden="true"></i>Deve conter no mínimo 6 caracteres</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="flex flex-4-large">
                                    <input type="password" autocomplete="off" placeholder="Repetir nova senha" ng-model="password_confirmation" name="password_confirmation" id="inputConfNewPwd" class="input-control no-space" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-group txt-center margin-top-30 margin-bottom-20">
                    <div class="bs border-color-primary padding-top-40">
                        <a href="{{'/'}}" class="btn-control btn-call-to-action call-to-action-negative btn-icon btn-icon-left">
                            <span><i class="icon fa fa-times"></i> Cancelar</span>
                        </a>
                        <button type="submit" ng-click="" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i> Salvar</span>
                        </button>
                    </div>
                </div>
            </form>
        </section>
    </div>    
</div>
@stop