@extends('site.site_template') 
@section('title') 
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop 
@section('content')
<script src="{{ asset('/js/site/user.js') }}"></script>
<script src='//cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js'></script>

{!! Breadcrumbs::renderIfExists('user') !!}

<div class="principal">
	<div class="listagem listagem-config">
		<div class="top-title top-others">
			<h3>Configurações da Conta</h3>
		</div>
		{!! Form::open(array('class' => 'form-settings form-horizontal')) !!}
		<div class="user-box">
			<div class="clearfix">
				<div class="tipo-nome">
					<span>Dados do Usuário</span>
					<hr>
				</div>
				<div class="input-box">
					Nome {!! Form::text('nome', $user->nome, array('class' => 'input-text', 'id' => 'editNome')) !!}
				</div>
				<div class="input-box">
					Sobrenome {!! Form::text('sobrenome', $user->sobrenome, array('class' => 'input-text', 'id' => 'editSobrenome')) !!}
				</div>
				<div class="input-box">
					Email {!! Form::text('email', $user->email, array('class' => 'input-text disabled', 'id' => 'editEmail', 'disabled')) !!}
				</div>
				<div class="input-box">
					Telefone {!! Form::text('fone', $user->fone, array('class' => 'input-text', 'id' => 'editFone')) !!}
				</div>
				<div class="input-box check">
					{!! Form::checkbox('notificacao_favoritos', $user->notificacao_favoritos, $user->notificacao_favoritos) !!} Desejo receber
					alertas dos produtos favoritos por email.
				</div>
				<div class="tipo-nome tipo-nome-password">
					<span>Alterar a Senha</span>
					<hr>
				</div>
				<p>A senha deve conter no mínimo 6 caracteres.</p>

				<div class="input-box input-box-password">
					Senha Atual {!! Form::password('old_password', array('class' => 'input-text', 'id' => 'inputPass')) !!}
				</div>
				<div class="input-box input-box-password">
					Nova Senha {!! Form::password('password', array('class' => 'input-text', 'id' => 'inputNewPass')) !!}
				</div>
				<div class="input-box input-box-password">
					Repetir Nova Senha {!! Form::password('password_confirmation', array('class' => 'input-text', 'id' => 'inputConfNewPass'))
					!!}
				</div>
			</div>
			<div class="btn-box clearfix">
				{!! Form::submit('Salvar', array('class' => 'btn-save')) !!}
			</div>
			@if (count($errors) > 0)
			<div class="alert-error">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif @if (session('status'))
			<div class="alert-success">
				<ul>
					<li>{{ session('status') }}</li>
				</ul>
			</div>
			@endif
		</div>
		{!! Form::close() !!}
	</div>

	<!-- Importação do Sidebar -->
	@include('_partials.sidebar')

</div>

@stop