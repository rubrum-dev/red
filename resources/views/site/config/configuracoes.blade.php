@extends('site.site_template') 
@section('title') 
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop 
@section('content')
<script src="{{ asset('/js/site/user.js') }}"></script>
<script src='//cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js'></script>
{!! Breadcrumbs::renderIfExists('user') !!}
<div class="content">
    <div class="top-head">
        <div class="top-inline top-head-title">
            <h2>Configurações da Conta</h2>
        </div>
    </div>
    <div class="container-user-settings">
        {!! Form::open(array('class' => 'user-settings-form ticket-alt-request-form')) !!}
        <fieldset>
            <div class="form-group clearfix">
                <div class="form-inline user-settings-input-box">
                    <label>Nome</label>
                    <!--<input type="text" name="" class="input-control">-->
                    {!! Form::text('nome', $user->nome, array('class' => 'input-control', 'id' => 'editNome')) !!}
                </div>
                <div class="form-inline user-settings-input-box">
                    <label>Sobrenome</label>
                    <!--<input type="text" name="" class="input-control">-->
                    {!! Form::text('sobrenome', $user->sobrenome, array('class' => 'input-control', 'id' => 'editSobrenome')) !!}
                </div>
                <div class="form-inline user-settings-input-box">
                    <label>Departamento</label>
                    <div class="select-control">
                        <select name="" id="departamento">
                            <option value="">Selecione...</option>
                            <option value="Informática">Informática / TI</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="form-inline user-settings-input-box">
                    <label>Email</label>
                    <!--<input type="text" name="" class="input-control">-->
                    {!! Form::text('email', $user->email, array('class' => 'input-control disabled', 'id' => 'editEmail', 'disabled')) !!}
                </div>
                <div class="form-inline user-settings-input-box">
                    <label>Telefone</label>
                    <!--<input type="text" name="" class="input-control">-->
                    {!! Form::text('fone', $user->fone, array('class' => 'input-control', 'id' => 'editFone')) !!}
                </div>
                <div class="form-inline user-settings-input-box">
                    <label>Cargo</label>
                    <div class="select-control">
                        <select name="" id="cargo">
                            <option value="">Selecione...</option>
                            <option value="Analista de Sistemas">Analista de Sistemas</option>
                        </select>
                    </div>
                </div>
            </div>
        </fieldset>
        <hr />
        <fieldset>
            <div class="text-block">
                <h3>Alterar a senha</h3>
                <p>
                    A senha deve conter pelo menos 6 caracteres.
                </p>
            </div>
            <div class="form-group clearfix">
                <div class="form-inline user-settings-input-box">
                    <label>Senha Atual</label>
                    <!--<input type="password" name="" class="input-control">-->
                    {!! Form::password('old_password', array('class' => 'input-control', 'id' => 'inputPass')) !!}
                </div>
                <div class="form-inline user-settings-input-box">
                    <label>Nova Senha</label>
                    <!--<input type="password" name="" class="input-control">-->
                    {!! Form::password('password', array('class' => 'input-control', 'id' => 'inputNewPass')) !!}
                </div>
                <div class="form-inline user-settings-input-box">
                    <label>Repetir Nova Senha</label>
                    <!--<input type="password" name="" class="input-control">-->
                    {!! Form::password('password_confirmation', array('class' => 'input-control', 'id' => 'inputConfNewPass')) !!}
                </div>
            </div>
        </fieldset>
        <hr />
        <fieldset>
            <div class="text-block">
                <h3>Favoritos</h3>
                <p>
                    Deseja receber as alertas dos favoritos por e-mail? Quando houver uma atualização de produto 
                    no SKU, GUIDE ou ADS, um e-mail será enviado. 
                </p>
            </div>
            <div class="form-group clearfix">
                <div class="alert-notifier-col form-inline form-inline-spacer radio-btn-control">
                    <input type="radio" id="radioNotifyMeChooser_Y" name="notificacao_favoritos" />
                    <label for="radioNotifyMeChooser_Y">Sim</label>
                </div>
                <div class="alert-notifier-col form-inline radio-btn-control">
                    <input type="radio" id="radioNotifyMeChooser_N" name="notificacao_favoritos" />
                    <label for="radioNotifyMeChooser_N">Não</label>
                </div>
            </div>
        </fieldset>
        @if (count($errors) > 0)
        <div class="alert-error">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif @if (session('status'))
        <div class="alert-success">
            <ul>
                <li>{{ session('status') }}</li>
            </ul>
        </div>
        @endif
        <div class="form-btn-group clearfix">
            <hr />
            <button type="submit" class="btn-salvar call-to-action-btn">
                <span class="btn-icon btn-icon-left icon-check"></span> Salvar
            </button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@stop