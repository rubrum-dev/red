<div class="custom-alert-heading heading-news flex-align-center clearfix">
    <h4 class="flexbox-container flex-fluid flex-align-center no-margin">
        <strong class="txt-bold">Atualizações - Versão 1.8.1</strong>
        <label class="flex-4-large dont-show-again switch-control switch-control-alt pull-right margin-left-auto"> 
            <input onclick="exibir_news()" value="1" type="checkbox" name="dontShowAgain" id="dontShowAgain" /> 
            <span class="switch-toggle pull-right margin-left-10"></span>
            <span class="abs-left-offset dont-show-again-txt switch-txt txt-small txt-bold margin-left-10">Não Exibir Novamente</span>
        </label>
    </h4> 
</div>
<div class="text-block">
    <h4 class="master-title-sub txt-light txt-small txt-uppercase margin-bottom-15">Veja as implementações e correções dessa versão:</h4>
    <ul class="list-news no-margin-bottom">
        <li>
            <p class="no-margin-bottom">
                Agora dentro do ticket os campos "Projeto" e "Código” são editáveis. Basta ir em Opções > Editar ticket para fazer a alteração.
            </p>
        </li>
    </ul>
</div>
{{--<div class="video-container">
    <video onloadeddata="this.play();" controls="" autoplay="autoplay">
        <source src="{{ asset2('/images/novidades/pdf-fornecedor.mp4') }}" type="video/mp4">
    </video>
</div>--}}