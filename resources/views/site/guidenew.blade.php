@extends('site.site_template')
@section('title')
    Guide
@stop
@section('content')
<div class="principal">
	<div class="breadcumbs-out clearfix">		
		<div class="breadcrumbs-inner clearfix">	
		    <ul>
		        <li class="active">Guide</li>
		        <li class="breadcrumbs-back"><a href="{{ url('/site/guide') }}"><span class="icon icon-arrow-left-light"></span> Voltar</a></li>
		    </ul>
		</div>
	</div>
	<div class="top-head">
		<div class="top-inline top-head-title">
			<h2 class="text-left"><span>Famílias de Produtos</span></h2>
		</div>
		<div class="top-inline top-nav-right">
			<ul class="top-inline">
				<li><a href="{{ url('/site/more/favorites_new') }}"><span class="icon icon-star"></span> Gerenciar Favoritos</a></li>
			</ul>
		</div>
	</div>
	<div class="listagem">
		<div class="box-imagens clearfix">
			<div class="box-zoom">
				<img src="{{ asset('/images/uploads/logotipos/2ab034877e76cdcc5bcf9abef98923db.png') }}" class="img-logo" alt="Brahma">
				<div class="camada-link">
					<a href="{{ url('/site/guide/brahma') }}" title="Brahma">
						<div class="box-img-link">
							<h4>Brahma</h4>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
@stop