@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

<script>
    var csrf_token = '{{ csrf_token() }}';

    @if((in_array('dashboard_indicadores_gerais', Auth::user()->modulos)))
        var aba_indicadores_gerais = true;
    @else
        var aba_indicadores_gerais = false;
    @endif

    @if((in_array('dashboard_relatorios', Auth::user()->modulos)))
        var aba_relatorios = true;
    @else
        var aba_relatorios = false;
    @endif

</script>

<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm.css') }}" />

<script src="{{ asset('/js/site/ui.datepicker-pt-BR.js') }}"></script>
<script src="{{ asset2('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-route.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset('/js/angular/dashboard-resolve-loader.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/dashboard/dashboard-controller.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/dashboard/relatorio1-controller.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/dashboard/relatorio2-controller.js') }}"></script>

<!-- TO-DO - Breadcrumb -->
<div class="breadcumbs-out clearfix">		
    <div class="breadcrumbs-inner content-fluid clearfix">	
        <ul>
            <li class="active">Dashboard</li>
        </ul>
    </div>
</div>
<!-- END TO-DO - Breadcrumb -->

<div ng-app="app" ng-controller="DashboardController" class="content content-fluid">
    <div ng-cloak class="dashboard-container margin-top-10">
        <h3 class="master-title">Olá {{ Auth::user()->nome_abreviado }}</h3>
		<div class="block margin-bottom-20">
            <p>Aqui mostramos algumas informações relevantes para você.</p>
        </div>
        <ul class="tabs clearfix">

            <li><a ng-click="$root.change_tab($event)" ng-class="{'active' : activetab == '/' && !$root.activenav}" ng-href="#/">Meus Indicadores</a></li>

            @if(!(in_array('dashboard_indicadores_gerais', Auth::user()->modulos)))
            <li class="hidden"><a ng-click="$root.change_tab($event)" class="disabled" ng-href="#/indicadores-gerais">Indicadores Gerais</a></li>
            @endif

            @if((in_array('dashboard_indicadores_gerais', Auth::user()->modulos)))
            <li><a ng-click="$root.change_tab($event)" ng-class="{'active' : activetab == '/indicadores-gerais' && !$root.activenav}" ng-href="#/indicadores-gerais">Indicadores Gerais</a></li>
            @endif

            @if(!(in_array('dashboard_relatorios', Auth::user()->modulos)))
            <li class="hidden"><a ng-click="$root.change_tab($event)" class="disabled" ng-href="#/relatorios">Relatórios</a></li>
            @endif

            @if((in_array('dashboard_relatorios', Auth::user()->modulos)))
            <li><a ng-click="$root.change_tab($event)" ng-class="{'active' : activetab == '/relatorios' || $root.activenav}" ng-href="#/relatorios">Relatórios</a></li>
            @endif

        </ul>
        <div resolve-loader></div>
        <div class="dashboard-content margin-bottom-40" ng-view></div>	
    </div>
</div>
@stop