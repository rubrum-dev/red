@extends('site.site_template')

@section('title')
    Cores
@stop

@section('content')

{!! Breadcrumbs::render('guides', ['guide' => 'Cores', 'id_prod' => $product]) !!}

<div class="principal">
	<div class="listagem listagem-guide">

		@include('_partials.common-guide')

		@if(count($produto->cores) > 0)
			@foreach($produto->tiposCores as $tipo)
				<div class="guide-tipos">
					<div class="tipo-nome">
						<span>{{ $tipo->nome }}</span>
						<hr>
					</div>
					@foreach($produto->niveisCores as $nivel)
						<?php $count = 1; $checkNivel = 0; ?>
						<h4 class="nivel-cor-{{$tipo->id}}-{{$nivel->id}}">{{ mb_strtoupper($nivel->nome, 'UTF-8') }}</h4>
						@foreach($produto->cores as $cor)
							<?php
								$cmyk = explode(' ', $cor->cmyk);
								$rgb = explode(' ', $cor->rgb);
							?>
							@if($cor->id_tipo_cor == $tipo->id && $cor->id_nivel_cor == $nivel->id)
                                <?php $checkNivel = 1; ?>
								<div class="cor-unidade">
                                @if(!empty($cor->descricao))
                                    <div class="tooltip">
                                        <span class="tooltiptext">{{ $cor->descricao }}</span>
                                @else
                                    <div class="box-color-name">
                                @endif
                                        <div class="box-color" style="background-color: #{{ $cor->hex }};"></div>
    									<div class="box-name">
                                            @if(!empty($cor->descricao))
                                                {{ Html::image(asset('/images/icons/frontend/more.png'), 'Descrição') }}
                                            @endif
                                            <center><span class="nome">{{ mb_strtoupper($cor->nome, 'UTF-8') }}</span></center>
                                        </div>
                                    </div>
                                    <div class="clear-margin"></div>
									<div class="valores-colors">
										<div class="hex-spot">
											<span class="hex-spot-title">WEB</span>
											<br />
											<span class="hex-spot-value">{{ $cor->hex }}</span>
										</div>
										<div class="hex-spot">
											<span class="hex-spot-title">SPOT</span>
											<br />
											<span class="hex-spot-value">{{ $cor->spot }}</span>
										</div>
										<div class="clear-margin"></div>
										<div class="cmyk-rgb">
											<span class="cmyk-rgb-title">CMYK</span>
											<br />
											<span class="cmyk-rgb-value">{{ $cmyk[0] }}</span><br />
											<span class="cmyk-rgb-value">{{ $cmyk[1] }}</span><br />
											<span class="cmyk-rgb-value">{{ $cmyk[2] }}</span><br />
											<span class="cmyk-rgb-value">{{ $cmyk[3] }}</span>
										</div>
										<div class="cmyk-rgb">
											<span class="cmyk-rgb-title">RGB</span>
											<br />
											<span class="cmyk-rgb-value">{{ $rgb[0] }}</span><br />
											<span class="cmyk-rgb-value">{{ $rgb[1] }}</span><br />
											<span class="cmyk-rgb-value">{{ $rgb[2] }}</span><br />
										</div>
									</div>
								</div>
								@if($count % 4 == 0)
									<div class="clear-margin"></div>
								@endif
								<?php $count++ ?>
							@endif
						@endforeach
                        @if($checkNivel == 0)
                            <script>
                            $('.nivel-cor-{{$tipo->id}}-{{$nivel->id}}').hide();
                            </script>
                        @endif
					@endforeach
				</div>
			@endforeach
		@else
			<h3>Nenhuma cor vinculada a esse produto</h3>
		@endif

	</div>

	<!-- Importação do Sidebar -->
	@include('_partials.sidebar')


</div>

@stop
