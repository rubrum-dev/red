@extends('site.site_template')

@section('title')
    Tipografias
@stop

@section('content')

{!! Breadcrumbs::render('guides', ['guide' => 'Tipografias', 'id_prod' => $product]) !!}

<div class="principal">
	<div class="listagem listagem-guide">

		@include('_partials.common-guide')

		@if(count($produto->tipografias) > 0)
			@foreach($produto->tiposTipografias as $tipo)
				<div class="guide-tipos">
					<div class="tipo-nome">
						<span>{{ $tipo->nome }}</span>
						<hr>
					</div>
					<?php $count = 1; ?>
					@foreach($produto->tipografias as $tipografia)
						@if($tipografia->id_tipo_tipografia == $tipo->id)
							<div class="unidade">
                                <div class="horizontal" style="width: 100%; font-size: 16px;">
                                    <div>
                                        <p>{!!html_entity_decode($tipografia->descricao)!!}</p>
                                    </div>
                                    <center>
                                        {{ Html::image(asset($tipografia->thumb), '', array('style' => 'width: 100%;')) }}
                                        <br>
                                        @if(!empty($tipografia->link))
                                            {{ Html::link($tipografia->link, 'Link Download', array('target' => '_blank')) }}
                                        @endif
                                    </center>
                                </div>
							</div>
							@if($count % 2 == 0)
								<div class="clear-margin"></div>
							@endif
							<?php $count++; ?>
						@endif
					@endforeach
				</div>

			@endforeach
		@else
			<h3>Nenhuma tipografia vinculada a esse produto</h3>
		@endif

	</div>

	<!-- Importação do Sidebar -->
	@include('_partials.sidebar')


</div>

@stop
