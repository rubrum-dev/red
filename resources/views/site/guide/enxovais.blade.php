@extends('site.site_template')

@section('title')
    Enxovais de Campanha
@stop

@section('content')

<script src="{{ asset('/js/site/guide.js') }}"></script>

<input id="family" type="hidden" value="{{ $produto->id_familia }}">
<input id="product" type="hidden" value="{{ $produto->id }}">

{!! Breadcrumbs::render('guides', ['guide' => 'Enxovais de Campanha', 'id_prod' => $product]) !!}

<div class="principal">
	<div class="listagem listagem-guide">

        <div class="top-title">
            <div class="guide-top clearfix">
                <div class="top-logo-down">
        			{{ Html::image(asset($produto->logotipo), 'Produto') }}
                    <div class="download-logo-top" style="height:auto;">
                        @if(!Auth::guest())
                            <center>
                                <div class="star-favorite">
                                    <a href="javascript:void(0)">
                                        @if(in_array($produto->id, $user->favorites))
                                            {{ Html::image(asset('/images/icons/frontend/favorite-set-yes.png'), 'favorite', array('id' => 'imgSkuTitle')) }}
                                        @else
                                            {{ Html::image(asset('/images/icons/frontend/favorite-set-no.png'), 'favorite', array('id' => 'imgSkuTitle')) }}
                                        @endif
                                        <a href="javascript:void(0)" class="texto-favorito-top">
                                            Favorito
                                        </a>
                                    </a>
                                </div>
                            </center>
                        @endif
    				</div>
        		</div>
                <div class="top-bottom">
                    <div class="nome-produto-favorito">
                        <h3 style="width:260px;margin:0px 30px 0px 0px;line-height: 1em;">{{ mb_strtoupper($produto->nome) }}</h3>
						<div class="links-paginas-top links-paginas-top-enxoval" style="padding:0px;">
                        	<a href="javascript:void(0)">Enxovais de Campanha</a>
                    	</div>
					</div>
                </div>
            </div>
		</div>

		@if(count($produto->enxovais) > 0)
			<div class="guide-tipos">
				<div class="tipo-nome">
					<span>Arquivos</span>
					<hr>
				</div>
				<?php $count = 1; ?>
				@foreach($produto->enxovais as $enxoval)
					<div class="unidade">
						<div class="enxoval">
							{{ Html::image(asset($enxoval->thumb), 'File Enxoval') }}
                            {{ $enxoval->nome }}
							@if(!empty($enxoval->path_down))
								<div class="down">
									<a href="{{ asset($enxoval->path_down) }}" target="_blank">
										{{ Html::image(asset('/images/icons/frontend/short-down.png'), 'down') }}
                                        ZIP
									</a>
								</div>
								<br />
							@endif
						</div>
					</div>
					@if($count % 3 == 0)
						<div class="clear-margin"></div>
					@endif
					<?php $count++; ?>
				@endforeach
			</div>
		@else
			<h3>Nenhum enxoval vinculado a esse produto</h3>
		@endif

	</div>

	<!-- Importação do Sidebar -->
	@include('_partials.sidebar')


</div>

@stop
