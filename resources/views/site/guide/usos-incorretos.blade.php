@extends('site.site_template')

@section('title')
    Usos Incorretos
@stop

@section('content')

{!! Breadcrumbs::render('guides', ['guide' => 'Usos Incorretos', 'id_prod' => $product]) !!}

<div class="principal">
	<div class="listagem listagem-guide">

		@include('_partials.common-guide')

		@if(count($produto->usosIncorretos) > 0)
			@foreach($produto->tiposUsosIncorretos as $tipo)
				<div class="guide-tipos">
					<div class="tipo-nome">
						<span>{{ $tipo->nome }}</span>
						<hr>
					</div>
					<?php $count = 1; ?>
					@foreach($produto->usosIncorretos as $incorreto)
						@if($tipo->id == $incorreto->id_tipo_uso_incorreto)
							<div class="unidade">
								<div class="horizontal" style="font-size: 16px;">
                                    <p>{!!html_entity_decode($incorreto->descricao)!!}</p>
                                    <center>
                                        {{ Html::image(asset($incorreto->thumb), '') }}
                                    </center>
								</div>
							</div>
							@if($count % 2 == 0)
								<div class="clear-margin"></div>
							@endif
							<?php $count++; ?>
						@endif
					@endforeach

				</div>
			@endforeach
		@else
        	<h3>Nenhum uso incorreto vinculado a esse produto</h3>
        @endif

	</div>

	<!-- Importação do Sidebar -->
	@include('_partials.sidebar')


</div>

@stop
