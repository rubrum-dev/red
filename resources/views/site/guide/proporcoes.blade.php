@extends('site.site_template')

@section('title')
    Proporções
@stop

@section('content')

{!! Breadcrumbs::render('guides', ['guide' => 'Proporções', 'id_prod' => $product]) !!}

<div class="principal">
	<div class="listagem listagem-guide">

		@include('_partials.common-guide')

		@if(count($produto->proporcoes) > 0)
			@foreach($produto->categoriasProporcoes as $categoria)
				<div class="guide-tipos">
					<div class="tipo-nome">
						<span>{{ $categoria->nome }}</span>
						<hr>
					</div>
					@foreach($produto->proporcoes as $proporcao)
						<?php $count = 1; ?>
						@if($proporcao->id_categoria_proporcao == $categoria->id)
							@foreach($produto->tiposProporcoes as $tipo)
								@if($tipo->id == $proporcao->id_tipo_proporcao_versao)
									<div class="unidade">
										<div class="horizontal" style="font-size: 16px;">
                                            <p>{!!html_entity_decode($proporcao->descricao)!!}</p>
											<center>
                                                {{ Html::image(asset($proporcao->thumb), '') }}
                                            </center>
										</div>
									</div>
									@if($count % 2 == 0)
										<div class="clear-margin"></div>
									@endif
								@endif
								<?php $count++; ?>
							@endforeach
						@endif
					@endforeach
				</div>
			@endforeach
		@else
        	<h3>Nenhuma proporção vinculada a esse produto</h3>
        @endif

	</div>

	<!-- Importação do Sidebar -->
	@include('_partials.sidebar')


</div>

@stop
