@extends('site.site_template')

@section('title')
    Versões
@stop

@section('content')

{!! Breadcrumbs::render('guides', ['guide' => 'Versões', 'id_prod' => $product]) !!}

<script src="{{ asset('/js/site/guide.js')}}"></script>

<div class="principal">
	<div class="listagem common-guide">
		@include('_partials.common-guide')
		<section class="guide-main-section">
			<h4>Brahma e sua Nova Marca</h4>
			<div class="row clearfix">
				<div class="col-2-guide">
					<img src="{{asset('/images/layout/logo-brahma-front.png')}}" alt="" width="480" height="215" class="guide-img-big" />
				</div>
				<div class="col-2-guide">
					<p>
						Brahma é uma das marcas mais icônicas do Brasil, tanto no seguimento de cervejas como no hall de 
						marcas formadoras de nossa cultura. Com mais de 128 anos de história não é à toa que hoje é 
						considerada uma das marcas mais valiosas do país. A eterna número 1 em sabor e tradição.
					</p>
					<p>
						Toda nova identidade visual foi inspirada nas embalagens antigas da nossa Brahma Chopp. 
						A presença da tipografia forte e branca, a fonte manual para escrever “chopp” e até mesmo a 
						presença da caldereta marcam um período no tempo: momentos dourados da Brahma relidos de forma 
						a permanecerem atuais e pertinentes. A nova marca é composta por dois elementos principais, 
						a faixa e o Brasão.
					</p>
				</div>
			</div>
		</section>
		<hr />
		<section class="guide-main-section">
			<h4>Escala de cores</h4>
			<p>
				Uma escala de cores física, impressa em offset e sem aplicação de verniz, foi desenvolvida para orientar as 
				agências e os fornecedores sobre a paleta de cores da marca. As tintas dessa escala foram desenvolvidas pela 
				ACTEGA Coatings & Sealants utilizando dois substratos como exemplo:
			</p>
			<ul>
				<li>- Papel Couché Suzano Press Gloss 170g para cores opacas.</li>
				<li>- Papel Metalizado Málaga 155g para o Dourado Metalizado Brahma.</li>
			</ul>
			<p>
				Escala de cores impressa por: PaintPack Serviços Gráficos Ltda. - Avenida Onze de Junho, 886 - Cep 04041-003 - São Paulo/SP
			</p>
			<p>
				Para obeter informações basta clicar sobre a cor desejada.
			</p>
			<!-- Guide Color Info -->
			<div class="guide-color-info clearfix">
				<div class="guide-color-unit">
					<div style="background:#BE8720" class="guide-color-box">
						<a href="javascript:;" class="color-scale-info-1">
							<strong><span class="icon icon-info"></span>Mais Informações</strong>
						</a>
					</div>
					<span class="guide-color-name"><strong>Dourado Brahma</strong></span>
					<div class="guide-color-values">
						<span class="guide-text-piece"><strong>Web</strong> BE8720</span>
						<span class="guide-text-piece"><strong>Spot</strong> Actega AM4330/10</span>
						<span class="guide-text-piece"><strong>Rgb</strong></span>
						<span class="guide-text-piece">201 &bull; 151 &bull; 000</span>
						<span class="guide-text-piece"><strong>CMYK</strong></span>
						<span class="guide-text-piece">006 &bull; 027 &bull; 100 &bull; 012</span>
					</div>
				</div>
				<div class="guide-color-unit">
					<div style="background:#AB0010" class="guide-color-box">
						<a href="javascript:;" class="color-scale-info-2">
							<strong><span class="icon icon-info"></span>Mais Informações</strong>
						</a>
					</div>
					<span class="guide-color-name"><strong>Vermelho Brahma</strong></span>
					<div class="guide-color-values">
						<span class="guide-text-piece"><strong>Web</strong> AB0010</span>
						<span class="guide-text-piece"><strong>Spot</strong> Actega VM5538/10</span>
						<span class="guide-text-piece"><strong>Rgb</strong></span>
						<span class="guide-text-piece">171 &bull; 000 &bull; 016</span>
						<span class="guide-text-piece"><strong>CMYK</strong></span>
						<span class="guide-text-piece">000 &bull; 100 &bull; 100 &bull; 005 </span>
					</div>
				</div>
				<div class="guide-color-unit">
					<div style="background:#78151B" class="guide-color-box">
						<a href="javascript:;" class="color-scale-info-3">
							<strong><span class="icon icon-info"></span>Mais Informações</strong>
						</a>
					</div>
					<span class="guide-color-name"><strong>Vermelho Retícula Brahma</strong></span>
					<div class="guide-color-values">
						<span class="guide-text-piece"><strong>Web</strong> 78151B</span>
						<span class="guide-text-piece"><strong>Spot</strong> Actega VM7535/10</span>
						<span class="guide-text-piece"><strong>Rgb</strong></span>
						<span class="guide-text-piece">120 &bull; 021 &bull; 027</span>
						<span class="guide-text-piece"><strong>CMYK</strong></span>
						<span class="guide-text-piece">000 &bull; 100 &bull; 100 &bull; 040</span>
					</div>
				</div>
				
				<div class="guide-color-unit">
					<div style="background:#2D0A0B" class="guide-color-box">
						<a href="javascript:;" class="color-scale-info-4">
							<strong><span class="icon icon-info"></span>Mais Informações</strong>
						</a>
					</div>
					<span class="guide-color-name"><strong>Marrom Brahma</strong></span>
					<div class="guide-color-values">
						<span class="guide-text-piece"><strong>Web</strong> 2D0A0B</span>
						<span class="guide-text-piece"><strong>Spot</strong> Actega MR7250/10</span>
						<span class="guide-text-piece"><strong>Rgb</strong></span>
						<span class="guide-text-piece">045 &bull; 010 &bull; 011</span>
						<span class="guide-text-piece"><strong>CMYK</strong></span>
						<span class="guide-text-piece">000 &bull; 100 &bull; 100 &bull; 075</span>
					</div>
				</div>
				<div class="guide-color-unit">
					<div style="background:#FFFFFF" class="guide-color-box">
						<a href="javascript:;" class="color-scale-info-5">
							<strong><span class="icon icon-info"></span>Mais Informações</strong>
						</a>
					</div>
					<span class="guide-color-name"><strong>Branco</strong></span>
					<div class="guide-color-values">
						<span class="guide-text-piece"><strong>Web</strong> FFFFFF</span>
						<span class="guide-text-piece"><strong>Spot</strong> - </span>
						<span class="guide-text-piece"><strong>Rgb</strong></span>
						<span class="guide-text-piece">255 &bull; 255 &bull; 255</span>
						<span class="guide-text-piece"><strong>CMYK</strong></span>
						<span class="guide-text-piece">000 &bull; 000 &bull; 000 &bull; 000</span>
					</div>
				</div>
			</div>
			<!-- END Guide Color Info -->
		</section>
		<hr />
		<section class="guide-main-section">
			<h4>Versão 1 - A Faixa</h4>
			<div class="row clearfix">
				<div class="col-2-guide">
					<img src="{{asset('/images/layout/logo-brahma-faixa.png')}}" alt="" class="guide-img-big" />
				</div>
				<div class="col-2-guide">
					<p>
						Esta é a nossa marca principal, e portanto, a versão que deve ser utilizada sempre que possível. 
						Os exemplos desta página mostram algumas aplicações indicadas para seu uso, restrições relativas 
						e sua cor e ao fundo de sua aplicação. É importante destacar que deve-se assegurar que a palavra 
						“Brahma” esteja sempre em um tom mais claro que seu fundo, nesse caso em branco. Isso deve valer 
						para qualquer outra aplicação da marca.
					</p>
					<div class="download-archive">
						<a href="{{asset('/files/ftp/Logo Brahma - Todas as Versões.zip')}}"><span class="icon-download"></span> Baixar Arquivo</a>
					</div>
				</div>
			</div>
		</section>
		<hr />
		<section class="guide-main-section">
			<h4>Versão 2 - Faixa com Brasão</h4>
			<div class="row clearfix">
				<div class="col-2-guide">
					<img src="{{asset('/images/layout/logo-brahma-brasao.png')}}" alt="" class="guide-img-big" />
				</div>
				<div class="col-2-guide">
					<p>
						A Faixa + Brasão é nossa assinatura para momentos especiais. Para que sua aplicação seja considerada, 
						deve-se garantir uma boa área de respiro. Também é muito importante certificar-se de aplicá-la em uma 
						escala que garanta sua legibilidade, já que, devido às ilustrações, é uma versão de logo mais detalhada 
						e rebuscada. A Faixa + Brasão é também a única versão da marca Brahma que apresenta três cores, o que 
						especifica ainda mais sua aplicação, como é exemplificado nesta página.
					</p>
					<div class="download-archive">
						<a href="{{asset('/files/ftp/Logo Brahma - Todas as Versões.zip')}}"><span class="icon-download"></span> Baixar Arquivo</a>
					</div>
				</div>
			</div>
		</section>
		<hr />
		<section class="guide-main-section">
			<h4>Versão 3 - Faixa com Brasão e Fundo Reticulado</h4>
			<div class="row clearfix">
				<div class="col-2-guide">
					<img src="{{asset('/files/ftp/layout/logo-brahma-brasao-reticulado.png')}}" class="guide-img-big" />
				</div>
				<div class="col-2-guide">
					<p>
						Foi desenvolvido um fundo composto por retículas que formam um degradê em meio-tom para dar mais 
						destaque à marca em casos específicos. Sempre que for aplicado deve-se atentar para a posição, 
						tamanho e formato dessas retículas, utilizando sempre o arquivo disponível para download como 
						referência. 
					</p>
					<div class="download-archive">
						<a href="{{asset('/files/ftp/Logo Brahma - Todas as Versões.zip')}}"><span class="icon-download"></span> Baixar Arquivo</a>
					</div>
				</div>
			</div>
		</section>
		<hr />
		<section class="guide-main-section">
			<h4>Ícone da Marca - Paixão por Cerveja</h4>
			<div class="row clearfix">
				<div class="col-2-guide">
					<img src="{{asset('/images/layout/logo-brahma-icone.png')}}" alt="" class="guide-img-big" />
				</div>
				<div class="col-2-guide">
					<p>
						Foi criado o ícone “Paixão por Cerveja” para ilustrar a eterna cerveja “Número 1”.  
					</p>
					<div class="download-archive">
						<a href="{{asset('/files/ftp/Logo Brahma - Todas as Versões.zip')}}"><span class="icon-download"></span> Baixar Arquivo</a>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
@stop
