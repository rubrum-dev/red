@extends('site.site_template')

@section('title')
    Aplicações
@stop

@section('content')

{!! Breadcrumbs::render('guides', ['guide' => 'Aplicações', 'id_prod' => $product]) !!}

<div class="principal">
	<div class="listagem listagem-guide">

		@include('_partials.common-guide')

		@if(count($produto->usosAplicacoes) > 0)
			@foreach($produto->tiposAplicacoes as $tipo)
				<div class="guide-tipos">
					<div class="tipo-nome">
						<span>{{ $tipo->nome }}</span>
						<hr>
					</div>
					<?php $count = 1; ?>
					@foreach($produto->usosAplicacoes as $aplicacao)
						@if($tipo->id == $aplicacao->id_tipo_uso_aplicacao)
							<div class="unidade">
								<div class="horizontal" style="font-size: 16px;">
                                    <p>{!!html_entity_decode($aplicacao->descricao)!!}</p>
                                    <center>
                                        {{ Html::image(asset($aplicacao->thumb), '') }}
                                    </center>
								</div>
							</div>
							@if($count % 2 == 0)
								<div class="clear-margin"></div>
							@endif
							<?php $count++; ?>
						@endif
					@endforeach

				</div>
			@endforeach
		@else
        	<h3>Nenhuma aplicação vinculada a esse produto</h3>
        @endif

	</div>

	<!-- Importação do Sidebar -->
	@include('_partials.sidebar')


</div>

@stop
