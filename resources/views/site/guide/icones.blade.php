@extends('site.site_template')

@section('title')
    Ícones
@stop

@section('content')

{!! Breadcrumbs::render('guides', ['guide' => 'Ícones', 'id_prod' => $product]) !!}

<div class="principal">
	<div class="listagem listagem-guide">

		@include('_partials.common-guide')

		@if(count($produto->icones) > 0)
			@foreach($produto->categoriasIcones as $categoria)
				<div class="guide-tipos">
					<div class="tipo-nome">
						<span>{{ $categoria->nome }}</span>
						<hr>
					</div>
					<?php $count = 1; ?>
					@foreach($produto->icones as $icone)
						@if($icone->id_categoria_icone == $categoria->id)
							<div class="unidade">
								<div class="icone">
                                    <p>{!!html_entity_decode($icone->descricao)!!}</p>
									{{ Html::image(asset($icone->thumb), '') }}
									<br />
									@if(!empty($icone->path_down))
										<div class="down">
											<a href="{{ asset($icone->path_down) }}" target="_blank">
												{{ Html::image(asset('/images/icons/frontend/short-down.png'), 'down') }}
                                                ZIP
											</a>
										</div>
										<br />
									@endif
								</div>
							</div>
							@if($count % 3 == 0)
								<div class="clear-margin"></div>
							@endif
							<?php $count++; ?>
						@endif
					@endforeach
				</div>

			@endforeach
		@else
			<h3>Nenhum icone vinculado a esse produto</h3>
		@endif



	</div>

	<!-- Importação do Sidebar -->
	@include('_partials.sidebar')


</div>

@stop
