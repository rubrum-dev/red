@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/usuarios_gerenciar.js') }}"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- DataTables -->
<script src="{{ asset('/js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/datatables/accent-neutralise.js') }}"></script>
<script defer type="text/javascript" src="{{ asset2('/js/site/dataTables-resolve-loader.js') }}"></script>

<script>
    var usuariosValidos = {{ $usuariosValidos }};
    var limiteUsuarios = {{ $limiteUsuarios }};
</script>

{!! Breadcrumbs::renderIfExists('usuarios.gerenciar') !!}

<div ng-app="app" ng-controller="UsuariosGerenciarCtrl" ng-cloak class="content content-fluid clearfix">
    <div class="manager-container margin-top-40 margin-bottom-40">
        <section>
            <h3 class="master-title">Gerenciar Usuários ({{ $usuariosValidos }} de {{ $limiteUsuarios }})</h3>
            <div class="dt-content-container box-relative">            
                <form>
                    <table style="display:none;" cellpading="0" cellspacing="0" id="myTable3" class="table manager-table table-border table-hover">
                        <thead>
                            <tr>
                                <th width=""><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Usuário</small></span></th>
                                <th width="250"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Email</small></span></th>
                                <th width="195"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Empresa / Departamento</small></span></th>
                                <th width="135" class="txt-center"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Último acesso</small></span></th>
                                <th width="67"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Link</small></span></th>
                                <th width="83"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Status</small></span></th>
                                <th width="50"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($usuarios as $usuario)
                                <tr>
                                    <td width="">
                                        <a href="{{ route('site.usuarios.editar', $usuario->id) }}">
                                            <i class="icon fa fa-user-circle-o"></i><span class="text-side">{{ trim($usuario->nome .' '. $usuario->sobrenome) }}</span>
                                        </a>
                                    </td>
                                    <td width=""><small class="txt-small">{{ $usuario->email }}</small></td>
                                    <td width="">
                                        @if(@$usuario->admDepartamento->nome) 
                                            <small class="txt-small">{{ trim($usuario->admEmpresas->nome .' / '. @$usuario->admDepartamento->nome ) }}</small>
                                        @else
                                            <small class="txt-small">{{ $usuario->admEmpresas->nome }}</small>
                                        @endif
                                    </td>
                                    <td width="" class="txt-center">
                                        <small class="txt-small">{!! ($usuario->logged_at) ? $usuario->logged_at->format('d/m/Y H:i:s') : '<span class="d-flex flex-justify-center">--</span>' !!}</small>
                                    </td>
                                    <td width="">
                                        <div class="flexbox-container flex-justify-center">
                                            @if($usuario->tem_token == 1)
                                                <a href="javascript:;" ng-click="enviar_novo_link('{{ $usuario->id }}','cadastro')" class="ui-icon-badge ui-icon-badge-primary"><i data-title="Cadastro" class="ui-tooltip top fa fa-user-plus"></i></a>
                                            @elseif($usuario->tem_token == 2)
                                                <a href="javascript:;" ng-click="enviar_novo_link('{{ $usuario->id }}','recuperar')" class="ui-icon-badge ui-icon-badge-primary"><i data-title="Recuperar Acesso" class="ui-tooltip top fa fa-key"></i></a>
                                            @else
                                                --
                                            @endif
                                        </div>
                                    </td>
                                    <td width="">
                                        @if($usuario->expirado_em && $usuario->statusNome === 'Ativo')
                                            <span data-title="Expirado em {{ $usuario->expirado_em->format('d/m/Y H:i:s') }}" class="ui-tooltip top txt-small">Expirado</span>
                                        @elseif($usuario->arquivado_em && $usuario->statusNome === 'Inativo')
                                            <span data-title="Arquivado em {{ $usuario->arquivado_em->format('d/m/Y H:i:s') }}" class="ui-tooltip top txt-small">Arquivado</span>
                                        @else
                                            <small class="txt-small">{{ $usuario->statusNome }}</span>
                                        @endif
                                        {{-- ($usuario->expirado_em) ? 'Expirado em ' . $usuario->expirado_em->format('d/m/Y H:i:s') : $usuario->statusNome --}}
                                    </td>
                                    <td width="">
                                        <div class="dropdown drop-to-left">
                                            <a data-title="Opções" href="javascript:;" class="dropdown-toggle ui-tooltip top fa fa-ellipsis-h"></a>
                                            <div class="dropdown-list">
                                                <ul> 
                                                    <li><a href="{{ route('site.usuarios.editar', $usuario->id) }}" class="txt-bold"><span class="icon fa fa-edit"></span>Alterar</a></li>
                                                    <li><a href="javascript:;" onclick="excluir('Após excluído, o usuário não poderá ser recuperado. Deseja excluir esse usuário?', '{{ route('site.usuarios.excluir', $usuario->id) }}')" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </form>
            </div>
        </section>
        <script>
            $(function() {
                $('.table').on( 'init.dt', function (e, settings) {
                    $('.datatable-pagination').hide();
                    setTimeout(function () {
                        if(settings.aiDisplay.length > 0) {
                            $('.datatable-pagination').show();
                        }		
                    }, new Date().getMilliseconds());
                });
                
                var dataTableToolbarBtnDOM = '<div class="flex flex-auto">' +
                    @if($usuariosValidos >= $limiteUsuarios)
                        '<a href="javascript:;" onclick="myAlert(\'O limite de usuários da sua conta foi atingido. Libere o limite de usuários excluindo usuários pouco usados ou contrate mais usuários no seu plano.\')" class="btn-function btn-function-alt"><i class="icon fa fa-user-plus"></i> Adicionar Usuário</a>' +
                    @else
                        '<a href="{{ Route('site.usuarios.adicionar') }}" class="btn-function"><i class="icon fa fa-user-plus"></i> Adicionar Usuário</a>' +
                    @endif
                '</div>';
                var table = $('#myTable3').DataTable({
                    dom: '<"datatable-toolbar flexbox-container clearfix"' +
                            '<"datatable-filter-container flex flex-3-large"f>' +
                        '>' +
                        '<"clearfix"' +
                            '<tr>' +
                        '>' +
                        '<"datatable-pagination flexbox-container flex-align-center clearfix"' +
                            '<"flex"<"select-control"l>><"flex flex-auto"p><"flex"i>' +
                        '>',
                    bAutoWidth: false,
                    processing: false,
                    serverSide: false,
                    stateSave: true,
                    pageLength: 25,
                    lengthMenu: [
                        [10, 25, 50, -1], 
                        ['Exibir 10 por página', 'Exibir 25 por página', 'Exibir 50 por página', 'Exibir todos']
                    ],
                    columnDefs: [
                        { orderable: false, targets: 6 }
                    ],
                    language: {
                        "url": "/js/datatables/language/Portuguese-Brasil.json"
                    },
                    initComplete: function () {
                        $('.datatable-filter-container .dataTables_filter input[type="search"]').attr('placeholder', 'Busca');

                        var dataTables_filter_cancel = '<i></i>';
                        var dataTables_filter_search = '<i></i>';
                        
                        $('.datatable-filter-container .dataTables_filter').append(dataTables_filter_search);
                        $('.datatable-filter-container .dataTables_filter').append(dataTables_filter_cancel);
                        $('.datatable-filter-container .dataTables_filter i').addClass('dataTables_filter_search');
                        $('.datatable-filter-container .dataTables_filter i + i').addClass('dataTables_filter_cancel');
                        
                        function dataTablesFilterCancelShowHide() {
                            if ($('.datatable-filter-container .dataTables_filter input[type="search"]').val() !== '') {
                                $('.dataTables_filter_search').hide();
                                $('.dataTables_filter_cancel').show();
                            
                            } else {
                                $('.dataTables_filter_search').show();
                                $('.dataTables_filter_cancel').hide();
                                
                                table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
                            }
                        }
                        
                        $(document).on('click', '.datatable-filter-container .dataTables_filter_cancel', function () {
                            $('#myTable3_filter input[type="search"]').val('');
                            
                            dataTablesFilterCancelShowHide();
                        });

                        $(document).on('keyup', '#myTable3_filter input[type="search"]', function () {
                            dataTablesFilterCancelShowHide();
                        });

                        $('.dataTable').show();
                        $('.dataTable thead').show();
                        $('.dataTables_filter').show();
                        $('.datatable-toolbar').append(dataTableToolbarBtnDOM);
                    },
                    fnDrawCallback: function( oSettings ) {
                        if(oSettings.aiDisplay.length <= 0) {
                            $('.dataTable thead').hide();
                            $('.dataTable thead tr').hide();
							$('.datatable-pagination').hide();
                            $('.dataTables_empty').addClass('fa-user');
                            $('.dataTables_empty').text('Nenhum usuário encontrado.');
                        } else {
                            $('.dataTable thead').show();
                            $('.dataTable thead tr').show();
							$('.datatable-pagination').show();
                            $('.dataTables_empty').removeClass('fa-user');
                            $('.dataTables_empty').text('');
                        }
                    }
                });
            });
        </script>
    </div>
</div>
@stop