@extends('site.site_template')
@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop
@section('content')
<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset('/js/angular/angular-ui/ui-mask/mask.min.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/usuarios_adicionar.js') }}"></script>
<script>
    var cadastrar = false; 
    var usuariosValidos = {{ $usuariosValidos }};
    var limiteUsuarios = {{ $limiteUsuarios }};
</script>

<div ng-app="app" ng-controller="UsuariosAdicionarCtrl" ng-cloak class="content content-fluid clearfix">
    @if (count($errors) > 0)
        <script>
            @foreach ($errors->all() as $error)
                myAlert('{{ $error }}', 'warning');
            @endforeach
        </script>
    @endif
    <div class="manager-container margin-top-40 margin-bottom-40">
        <section>
            <h3 class="master-title margin-bottom-30">Editar Usuário</h3>
            <form ng-submit="add_usuario_submit($event);" name="formUserManagerAdd" action="{{ route('site.usuarios.editar.post', $usuario->id) }}" method="POST" class="">
                <input type="hidden" name="_token" value="{{ csrf_token()}}">
                <div class="block">
                    <div class="flexbox-container flexbox-group">
                        <div class="flex flex-3-large">
                            <label class="label-control txt-small txt-uppercase margin-bottom-10">Nome *</label>
                            <input ng-model="nome" ng-init="nome = '{{ old('nome', $usuario->nome) }}'" type="text" autocomplete="off" name="nome" id="nome" class="input-control">            
                        </div>
                        <div class="flex flex-3-large">
                            <label class="label-control txt-small txt-uppercase margin-bottom-10">Sobrenome *</label>
                            <input ng-model="sobrenome" ng-init="sobrenome = '{{ old('sobrenome', $usuario->sobrenome) }}'" type="text" autocomplete="off" name="sobrenome" id="sobrenome" class="input-control">
                        </div>
                        <div class="flex flex-4-large">
                            <label class="label-control txt-small txt-uppercase margin-bottom-10">Email *</label>
                            <input ng-model="email" ng-init="email = '{{ old('email', $usuario->email) }}'" type="text" autocomplete="off" name="email" id="email" class="input-control input-email-only">
                        </div>
                        <div class="flex flex-2-large">
                            <label class="label-control txt-small txt-uppercase margin-bottom-10">Telefone</label>
                            <input ui-mask="@{{ phoneMask }}" ui-mask-placeholder mask-change="phoneMask" ng-model="telefone" ng-init="telefone = '{{ old('telefone', $usuario->fone) }}'" type="text" autocomplete="off" name="telefone" id="telefone" class="input-control">
                        </div>
                    </div>
                </div>
                <div class="block">
                    <label class="label-control txt-small txt-uppercase margin-bottom-10">Empresa *</label>
                    <div class="flexbox-container flexbox-group">
                        <div class="flex flex-6-large">
                            <div id="select_adm_empresa" class="select-control">
                                <select name="id_adm_empresa" ng-model="id_adm_empresa" ng-init="id_adm_empresa = '{{ old('id_adm_empresa', $usuario->id_adm_empresa) }}'" id="id_adm_empresa">
                                    @foreach ($empresas as $row)
                                        <option value="{{ $row->id }}">{{ $row->nome }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block">
                    <label class="label-control txt-small txt-uppercase margin-bottom-10">Departamento *</label>
                    <div class="flexbox-container flexbox-group">
                        <div class="flex flex-6-large">
                            <div id="select_id_departamento" class="select-control">
                                <select name="id_departamento" ng-model="id_departamento" ng-init="id_departamento = '{{ old('id_departamento', $usuario->id_departamento) }}'" id="id_departamento">
                                    <option value="">Selecione...</option>
                                    <option value="0">Novo Departamento</option>
                                    @foreach ($departamentos as $row)
                                        <option value="{{ $row->id }}">{{ $row->nome }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="flex flex-6-large">
                            <input value="{{ old('departamento_nome', $usuario->departamento_nome) }}" ng-show="id_departamento === '0'" ng-model="departamento_nome" type="text" placeholder="Nome do departamento" name="departamento_nome" id="departamento_nome" class="input-control" />
                        </div>
                    </div>
                </div>
                <div class="block">
                    <label class="label-control txt-small txt-uppercase margin-bottom-10">Cargo</label>
                    <div class="flexbox-container flexbox-group">
                        <div class="flex flex-6-large">
                            <div id="select_id_cargo" class="select-control">
                                <select name="id_cargo" ng-model="id_cargo" ng-init="id_cargo = '{{ old('id_cargo', $usuario->id_cargo) }}'" id="id_cargo">
                                    <option value="">Selecione...</option>
                                    <option value="0">Novo Cargo</option>
                                    @foreach ($cargos as $row)
                                        <option value="{{ $row->id }}">{{ $row->nome }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="flex flex-6-large">
                            <input value="{{ old('cargo_nome', $usuario->cargo_nome) }}" ng-show="id_cargo === '0'" type="text" ng-model="cargo_nome" placeholder="Nome do cargo" name="cargo_nome" id="cargo_nome" class="input-control" />
                        </div>
                    </div>
                </div>
                <div ng-init="perfisAdicionados = {{ $perfisSelecionados }}" class="block">
                    <label class="label-control txt-small txt-uppercase margin-bottom-10">Perfis de Acesso *</label>
                    <p class="no-margin-top margin-bottom-15">
                        Os perfis de acesso permitem que o usuário visualize apenas as Marcas ou Grupos de Marcas atribuídos.
                    </p>
                    <table ng-show="perfisAdicionados.length" cellpading="0" cellspacing="0" class="profiles-table table-border table-hover margin-bottom-15">
                        <tbody>
                            <tr ng-repeat="(p, perfil) in perfisAdicionados">
                                <td width="">
                                    <a href="javascript:;" class="d-flex flex-align-center txt-no-link">     
                                        <i class="icon fa fa-low-vision"></i>
                                        <span class="text-side">@{{perfil.nome}} {{--<small class="txt-small txt-color-alt txt-uppercase margin-left-15">To-do</small>--}}</span>
                                        <input type="hidden" name="perfis[]" ng-value="perfil.id_perfil">
                                    </a>
                                </td>
                                <td width="50">
                                    <div class="dropdown drop-to-left">
                                        <a data-title="Opções" href="javascript:;" class="dropdown-toggle ui-tooltip top fa fa-ellipsis-h"></a>
                                        <div class="dropdown-list">
                                            <ul> 
                                                <li><a ng-click="remover_perfil(p)" href="javascript:;" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="flexbox-container">
                        <div class="flex flex-fluid">
                            <div class="select-control">
                                <select ng-model="addPerfil" id="addPerfil">
                                    <option value="">Selecione os perfis de acesso</option>
                                    <option ng-show="!(perfisAdicionados | filter:{nome:p.nome}:true).length" ng-repeat="(x, p) in perfis" ng-value="x">@{{ p.nome }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="flex">
                            <button type="button" ng-click="adicionar_perfil()" class="btn btn-control btn-positive">Adicionar</button>
                        </div>
                    </div>
                </div>
                <div class="block">
                    <label class="label-control txt-small txt-uppercase margin-bottom-10">Perfil do Usuário</label>
                    <ul class="list-group-indent">
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-user-circle-o"></i>
                                    <span class="text txt-bold">Usuário Ativo <small class="txt-small txt-regular margin-left-20">Permite acesso ao sistema e aos módulos.</small></span>
                                    <label class="switch-control pull-right no-margin">
                                        @if($usuariosValidos >= $limiteUsuarios && $usuario->status != 1)
                                            <input ng-checked="{{ old('status', $usuario->status) }}" ng-init="perfil_adm_sistema_3 = {{ ($usuario->status == 1) ? 'true' : 'false' }}" ng-model="perfil_adm_sistema_3" ng-click="uncheck_ativar_usuario()" type="checkbox" name="status" value='1' id="perfil_adm_sistema_3" />
                                        @else
                                            <input ng-checked="{{ old('status', $usuario->status) }}" ng-init="perfil_adm_sistema_3 = {{ ($usuario->status == 1) ? 'true' : 'false' }}" ng-model="perfil_adm_sistema_3" type="checkbox" name="status" value='1' id="perfil_adm_sistema_3" />
                                        @endif
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>

                        <!-- Gerente de Embalagens -->
                        @if (Auth::user()->admin || Auth::user()->gerente || Auth::user()->gerencia_embalagem)
                            <li class="list-item-horizontal animate">
                                <div class="indent">
                                    <div class="pull">
                                        <i class="icon fa fa-cube"></i>
                                        <span class="text txt-bold">Gerente de Embalagens <small class="txt-small txt-regular margin-left-20">Pode gerenciar os parâmetros de embalagens e cadastrar outros Gerentes de Embalagens.</small></span>
                                        <label class="switch-control pull-right no-margin">
                                            <input ng-click="superAdmToggle()" ng-disabled="!perfil_adm_sistema_3" ng-init="perfil_adm_sistema_6 = {{ ($usuario->gerencia_embalagem == 1) ? 'true' : 'false' }}" ng-checked="{{ old('gerencia_embalagem', $usuario->gerencia_embalagem) }}" ng-model="perfil_adm_sistema_6" type="checkbox" name="gerencia_embalagem" value='1' id="perfil_adm_sistema_6" />
                                            <span class="switch-toggle abs-right-offset no-margin"></span>
                                        </label>
                                    </div>
                                </div>
                            </li>
                        @endif

                        <!-- Gerente de Fotos -->
                        @if (Auth::user()->admin || Auth::user()->gerente || Auth::user()->gerencia_foto)
                            <li class="list-item-horizontal animate">
                                <div class="indent">
                                    <div class="pull">
                                        <i class="icon fa fa-camera"></i>
                                        <span class="text txt-bold">Gerente de Fotos <small class="txt-small txt-regular margin-left-20">Pode gerenciar os parâmetros de fotos e cadastrar outros Gerentes de Fotos.</small></span>
                                        <label class="switch-control pull-right no-margin">
                                            <input ng-click="superAdmToggle()" ng-disabled="!perfil_adm_sistema_3" ng-init="perfil_adm_sistema_7 = {{ ($usuario->gerencia_foto == 1) ? 'true' : 'false' }}" ng-checked="{{ old('gerencia_foto', $usuario->gerencia_foto) }}" ng-model="perfil_adm_sistema_7" type="checkbox" name="gerencia_foto" value='1' id="perfil_adm_sistema_7" />
                                            <span class="switch-toggle abs-right-offset no-margin"></span>
                                        </label>
                                    </div>
                                </div>
                            </li>
                        @endif

                        <!-- Gerente de Mídia -->
                        @if ( (Auth::user()->admin || Auth::user()->gerente || Auth::user()->gerencia_midia) && (config('app.costummer_id') == 1 || config('app.costummer_id') == 'dev' || config('app.costummer_id') == 'stg' || config('app.costummer_id') == 2) )
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-newspaper-o"></i>
                                    <span class="text txt-bold">Gerente de Mídias <small class="txt-small txt-regular margin-left-20">Pode gerenciar os parâmetros de mídias e cadastrar outros outros Gerentes de Mídias.</small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-click="superAdmToggle()" ng-disabled="!perfil_adm_sistema_3" ng-init="perfil_adm_sistema_4 = {{ ($usuario->gerencia_midia == 1) ? 'true' : 'false' }}" ng-checked="{{ old('gerente_midias', $usuario->gerencia_midia) }}" ng-model="perfil_adm_sistema_4" type="checkbox" name="gerencia_midia" value='1' id="perfil_adm_sistema_4" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        @endif

                        <!-- Gerente de Usuário -->
                        @if (Auth::user()->admin || Auth::user()->gerente || Auth::user()->gerencia_usuario)
                            <li class="list-item-horizontal animate">
                                <div class="indent">
                                    <div class="pull">
                                        <i class="icon fa fa-user-circle"></i>
                                        <span class="text txt-bold">Gerente de Usuários <small class="txt-small txt-regular margin-left-20">Pode gerenciar os parâmetros de usuários e cadastrar outros Gerentes de Usuários.</small></span>
                                        <label class="switch-control pull-right no-margin">
                                            <input ng-click="superAdmToggle()" ng-disabled="!perfil_adm_sistema_3" ng-init="perfil_adm_sistema_2 = {{ ($usuario->gerencia_usuario == 1) ? 'true' : 'false' }}" ng-checked="{{ old('gerencia_usuario', $usuario->gerencia_usuario) }}" ng-model="perfil_adm_sistema_2" type="checkbox" name="gerencia_usuario" value='1' id="perfil_adm_sistema_2" />
                                            <span class="switch-toggle abs-right-offset no-margin"></span>
                                        </label>
                                    </div>
                                </div>
                            </li>
                        @endif
                        
                        <!-- Gerente de Fornecedores -->
                        @if (Auth::user()->admin || Auth::user()->gerente || Auth::user()->gerencia_fornecedor)
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-industry"></i>
                                    <span class="text txt-bold">Gerente de Fornecedores <small class="txt-small txt-regular margin-left-20">Pode gerenciar os parâmetros de fornecedores e cadastrar outros Gerentes de Fornecedores.</small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-click="superAdmToggle()" ng-disabled="!perfil_adm_sistema_3" ng-init="perfil_adm_sistema_5 = {{ ($usuario->gerencia_fornecedor == 1) ? 'true' : 'false' }}" ng-checked="{{ old('gerencia_fornecedor', $usuario->gerencia_fornecedor) }}" ng-model="perfil_adm_sistema_5" type="checkbox" name="gerencia_fornecedor" value='1' id="perfil_adm_sistema_5" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        @endif

                        <!-- Gerente de Administrador -->
                        @if (Auth::user()->admin || Auth::user()->gerente)
                            <li class="list-item-horizontal animate">
                                <div class="indent">
                                    <div class="pull">
                                        <i class="icon fa fa-cogs"></i>
                                        <span class="text txt-bold">Administrador <small class="txt-small txt-regular margin-left-20">Pode gerenciar todos os parâmetros e configurações do sistema, ver todos os tickets e cadastrar outros Administradores.</small></span>
                                        <label class="switch-control pull-right no-margin">
                                            <input ng-disabled="!perfil_adm_sistema_3" ng-init="perfil_adm_sistema_1 = {{ ($usuario->gerente == 1) ? 'true' : 'false' }}" ng-click="admToggleAll()" ng-checked="{{ old('gerente', $usuario->gerente) }}" ng-model="perfil_adm_sistema_1" type="checkbox" name="gerente" value='1' id="perfil_adm_sistema_1" />
                                            <span class="switch-toggle abs-right-offset no-margin"></span>
                                        </label>
                                    </div>
                                </div>
                            </li>
                        @endif
                    
                    </ul>
                </div>
                <div class="block">
                    <label class="label-control txt-small txt-uppercase margin-bottom-10">Opções do Artwork</label>
                    <ul class="list-group-indent">
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-paint-brush"></i>
                                    <span class="text txt-bold">Acesso Permitido <small class="txt-small txt-regular margin-left-20">Permite apenas visualizar e baixar os PDFs de aprovação das embalagens.</small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-checked="{{ old('permissao', in_array(3, $modulos)) }}" ng-init="art_permissao_1 = {{ (in_array(3, $modulos)) ? 'true' : 'false' }}" value='3' ng-model="art_permissao_1" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-cube"></i>
                                    <span class="text txt-bold">Criar Nova Embalagem <small class="txt-small txt-regular margin-left-20">Permite também criar e alterar Itens de Embalagens e subir arquivos de Versão 0.</small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-disabled="!art_permissao_1" ng-init="art_permissao_6 = {{ (old('permissao', in_array(8, $modulos))) ? 'true' : 'false' }}" value='8' ng-model="art_permissao_6" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-ticket"></i>
                                    <span class="text txt-bold">Abrir Ticket <small class="txt-small txt-regular margin-left-20">Condicionado ao acesso do módulo WorkFlow.</small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-disabled="!art_permissao_1" ng-init="art_permissao_4 = {{ (old('permissao', in_array(6, $modulos))) ? 'true' : 'false' }}" value='6' ng-model="art_permissao_4" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-history"></i>
                                    <span class="text txt-bold">Histórico <small class="txt-small txt-regular margin-left-20"></small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-disabled="!art_permissao_1" ng-init="art_permissao_9 = {{ (old('permissao', in_array(16, $modulos))) ? 'true' : 'false' }}" value='16' ng-model="art_permissao_9" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-paper-plane"></i>
                                    <span class="text txt-bold">Enviar Arte-final <small class="txt-small txt-regular margin-left-20"></small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-disabled="!art_permissao_1" ng-init="art_permissao_3 = {{ (old('permissao', in_array(5, $modulos))) ? 'true' : 'false' }}" value='5' ng-model="art_permissao_3" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-share-alt"></i>
                                    <span class="text txt-bold">Compartilhar Item <small class="txt-small txt-regular margin-left-20"></small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-disabled="!art_permissao_1" ng-init="art_permissao_10 = {{ (old('permissao', in_array(17, $modulos))) ? 'true' : 'false' }}" value='17' ng-model="art_permissao_10" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-industry"></i>
                                    <span class="text txt-bold">Anexar PDF do Fornecedor <small class="txt-small txt-regular margin-left-20"></small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-disabled="!art_permissao_1" ng-init="art_permissao_11 = {{ (old('permissao', in_array(18, $modulos))) ? 'true' : 'false' }}" value='18' ng-model="art_permissao_11" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-download"></i>
                                    <span class="text txt-bold">Baixar Arte-final <small class="txt-small txt-regular margin-left-20"></small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-disabled="!art_permissao_1" ng-init="art_permissao_2 = {{ (old('permissao', in_array(4, $modulos))) ? 'true' : 'false' }}" value='4' ng-model="art_permissao_2" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-crosshairs"></i>
                                    <span class="text txt-bold">Abrir no Inspector <small class="txt-small txt-regular margin-left-20"></small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-disabled="!art_permissao_1" ng-init="art_permissao_8 = {{ (old('permissao', in_array(15, $modulos))) ? 'true' : 'false' }}" value='15' ng-model="art_permissao_8" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-trash"></i>
                                    <span class="text txt-bold">Excluir e Descontinuar Embalagem <small class="txt-small txt-regular margin-left-20"></small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-disabled="!art_permissao_1" ng-init="art_permissao_12 = {{ (old('permissao', in_array(14, $modulos))) ? 'true' : 'false' }}" value='14' ng-model="art_permissao_12" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-paint-brush"></i>
                                    <span class="text txt-bold">Permitir Tudo <small class="txt-small txt-regular margin-left-20"></small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input type="checkbox" ng-click="artwToggleAll()" ng-checked="art_permissao_1 && art_permissao_6 && art_permissao_4 && art_permissao_9 && art_permissao_3 && art_permissao_10 && art_permissao_11 && art_permissao_2 && art_permissao_8 && art_permissao_12" ng-model="artwIsAllSelected" name="artwIsAllSelected" value="" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="block">
                    <label class="label-control txt-small txt-uppercase margin-bottom-10">Opções do WorkFlow</label>
                    <ul class="list-group-indent">
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-list-alt"></i>
                                    <span class="text txt-bold">Acesso Permitido <small class="txt-small txt-regular margin-left-20">Permite acesso ao módulo WorkFlow.</small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-click="(!work_permissao_1) ? art_permissao_4 = false : false" ng-checked="{{ old('permissao', in_array(9, $modulos)) }} || art_permissao_4" value='9' ng-model="work_permissao_1" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="block">
                    <label class="label-control txt-small txt-uppercase margin-bottom-10">Opções do Techdraw</label>
                    <ul class="list-group-indent">
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-pencil"></i>
                                    <span class="text txt-bold">Acesso Permitido <small class="txt-small txt-regular margin-left-20">Permite apenas visualizar e baixar os PDFs das plantas técnicas.</small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-checked="{{ old('permissao', in_array(11, $modulos)) }}" ng-init="tech_permissao_1 = {{ (in_array(11, $modulos)) ? 'true' : 'false' }}" value='11' ng-model="tech_permissao_1" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-codepen"></i>
                                    <span class="text txt-bold">Criar Nova Planta Técnica <small class="txt-small txt-regular margin-left-20">Permite também subir arquivos de plantas técnicas.</small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-disabled="!tech_permissao_1" ng-init="tech_permissao_2 = {{ (old('permissao', in_array(12, $modulos))) ? 'true' : 'false' }}" value='12' ng-model="tech_permissao_2" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-trash"></i>
                                    <span class="text txt-bold">Excluir Planta Técnica <small class="txt-small txt-regular margin-left-20"></small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-disabled="!tech_permissao_1" ng-init="tech_permissao_3 = {{ (old('permissao', in_array(13, $modulos))) ? 'true' : 'false' }}" value='13' ng-model="tech_permissao_3" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-pencil"></i>
                                    <span class="text txt-bold">Permitir Tudo <small class="txt-small txt-regular margin-left-20"></small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input type="checkbox" ng-click="techDrawToggleAll()" ng-checked="tech_permissao_1 && tech_permissao_2 && tech_permissao_3" ng-model="tcdrwIsAllSelected" name="tcdrwIsAllSelected" value="" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>

                @if (config('app.costummer_id') == 1 || config('app.costummer_id') == 'dev' || config('app.costummer_id') == 'stg' || config('app.costummer_id') == 2)
                <!-- TO-DO -->
                <div class="block">
                    <label class="label-control txt-small txt-uppercase margin-bottom-10">Opções do AdSmart</label>
                    <ul class="list-group-indent">
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-bullhorn"></i>
                                    <span class="text txt-bold">Acesso Permitido <small class="txt-small txt-regular margin-left-20">Permite apenas visualizar e baixar os arquivos de mídia.</small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-init="ads_permissao_1 = {{ (old('permissao', in_array(19, $modulos))) ? 'true' : 'false' }}" value='19' ng-model="ads_permissao_1" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-bullhorn"></i>
                                    <span class="text txt-bold">Criar Nova Campanha <small class="txt-small txt-regular margin-left-20"></small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-init="ads_permissao_2 = {{ (old('permissao', in_array(20, $modulos))) ? 'true' : 'false' }}" ng-disabled="!ads_permissao_1" value='20' ng-model="ads_permissao_2" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-newspaper-o"></i>
                                    <span class="text txt-bold">Criar Nova Mídia <small class="txt-small txt-regular margin-left-20">Permite também adicionar e alterar Peças de Mídia e abrir tickets.</small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-init="ads_permissao_3 = {{ (old('permissao', in_array(21, $modulos))) ? 'true' : 'false' }}" ng-disabled="!ads_permissao_1" value='21' ng-model="ads_permissao_3" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-clock-o"></i>
                                    <span class="text txt-bold">Histórico <small class="txt-small txt-regular margin-left-20"></small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-init="ads_permissao_4 = {{ (old('permissao', in_array(22, $modulos))) ? 'true' : 'false' }}" ng-disabled="!ads_permissao_1" value='22' ng-model="ads_permissao_4" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        <!-- TO-DO -->
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-trash"></i>
                                    <span class="text txt-bold">Excluir Mídia  <small class="txt-small txt-regular margin-left-20"></small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-init="ads_permissao_5 = {{ (old('permissao', in_array(25, $modulos))) ? 'true' : 'false' }}" ng-disabled="!ads_permissao_1" value='25' ng-model="ads_permissao_5" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>

                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-trash"></i>
                                    <span class="text txt-bold">Excluir Campanha  <small class="txt-small txt-regular margin-left-20"></small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-init="ads_permissao_6 = {{ (old('permissao', in_array(26, $modulos))) ? 'true' : 'false' }}" ng-disabled="!ads_permissao_1" value='26' ng-model="ads_permissao_6" type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        <!-- END TO-DO -->
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-bullhorn"></i>
                                    <span class="text txt-bold">Permitir Tudo <small class="txt-small txt-regular margin-left-20"></small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input type="checkbox" ng-click="adSmartToggleAll()" ng-checked="ads_permissao_1 && ads_permissao_2 && ads_permissao_3 && ads_permissao_4 && ads_permissao_5 && ads_permissao_6" ng-model="adsIsAllSelected" name="adsIsAllSelected" value="" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- END TO-DO -->
                @endif

                <!-- TO-DO -->
                <div class="block">
                    <label class="label-control txt-small txt-uppercase margin-bottom-10">Opções do Dashboard</label>
                    <ul class="list-group-indent">
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-tachometer"></i>
                                    <span class="text txt-bold">Indicadores Gerais <small class="txt-small txt-regular margin-left-20">Exibe os indicadores gerais com uma visão macro da performance dos usuários.</small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-init="dashboard_permissao_1 = {{ (old('permissao', in_array(23, $modulos))) ? 'true' : 'false' }}" ng-model="dashboard_permissao_1" value='23' type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        <li class="list-item-horizontal animate">
                            <div class="indent">
                                <div class="pull">
                                    <i class="icon fa fa-table"></i>
                                    <span class="text txt-bold">Relatórios <small class="txt-small txt-regular margin-left-20">Permite extrair relatórios gerenciais com uma visão detalhada da performance dos usuários.</small></span>
                                    <label class="switch-control pull-right no-margin">
                                        <input ng-init="dashboard_permissao_2 = {{ (old('permissao', in_array(24, $modulos))) ? 'true' : 'false' }}" ng-model="dashboard_permissao_2" value='24' type="checkbox" name="permissao[]" />
                                        <span class="switch-toggle abs-right-offset no-margin"></span>
                                    </label>
                                </div>
                            </div>
                        </li>
                        
                    </ul>
                </div>
                <!-- END TO-DO -->

                <div class="btn-group txt-center margin-top-30 no-margin-bottom">
                    <div class="bs border-color-primary padding-top-40">
                        <a href="{{ route('site.usuarios.gerenciar_v2') }}" class="btn-control btn-call-to-action call-to-action-negative btn-icon btn-icon-left">
                            <span><i class="icon fa fa-times"></i> Cancelar</span>
                        </a>
                        <button type="submit" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i> Salvar</span>
                        </button>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>
@stop