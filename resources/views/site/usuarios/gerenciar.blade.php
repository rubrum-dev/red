@extends('site.site_template')
@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop
@section('content')
<div class="content">
    <div class="top-head">
        <div class="top-tickets-manager clearfix">
            <div style="padding: 0px;" class="top-inline top-head-title manage-suppliers-title text-left top-head-no-float">
                <h2 style="padding: 0px;"><span>Gerenciador de Usuários</span></h2>
            </div>
            <div class="top-nav-right manage-suppliers-nav">
                <ul>
                    <li><a href="{{ Route('site.usuarios.adicionar') }}"><span class="icon-plus"></span> Adicionar Usuário</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fornecedores">
        <table border="0" cellpading="0" cellspacing="0" class="table-usuarios">
            <thead>
                <tr>
                    <th width="500"><a href="javascript;">Usuários <span class="icon icon-arrow-down"></span></a></th>
                    <th width="200"><a href="javascript;">Cargo <span class="icon icon-arrow-down"></span></a></th>
                    <th width="200"><a href="javascript;">Departamento <span class="icon icon-arrow-down"></span></a></th>
                    <th width="120"><a href="javascript;">Status <span class="icon icon-arrow-down"></span></a></th>
                    <th width="100">Editar</th>
                    <th width="100">Excluir</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($usuarios as $usuario)
                <tr>
                    <td width="500">&bull; {{ $usuario->nome_completo }}</td>
                    <td width="200">{{ $usuario->admCargo->nome }}</td>
                    <td width="200">{{ $usuario->admDepartamento->nome }}</td>
                    <td width="120">@if ($usuario->status == 1) Ativo @else Inativo @endif</td>
                    <td width="100"><a href="{{ Route('site.usuarios.editar', $usuario->id) }}" class="icon icon-options-settings"></a></td>
                    <td width="100"><a href="javascript:;" class="icon icon-cross"></a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop