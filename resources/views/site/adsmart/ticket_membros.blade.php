@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('adsmart.ticket.criar', $ticket->id_familia, $ticket->adsmartPeca->adsmartCampanhaMidia->id_campanha) !!}

<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />

<script>
    var csrf_token = '{{ csrf_token() }}';
    var id_usuario_logado = '{{ Auth::user()->id }}';
    var id_tipo = '{{ $ticket->adsmartPeca->adsmartCampanhaMidia->adsmartMidiaTipo->id }}';
</script>

<script src="{{ asset('/js/angular/angular.min.js')}}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/ticket-membros-controller.js') }}"></script>

<div class="content content-fluid" ng-app="app" ng-controller="TicketMembrosCtrl">
    @if (count($errors) > 0)
        <div class="alert alert-danger" role="alert">
            <div class="alert-error">
                @foreach ($errors->all() as $error)
                    {{ $error }}<br />
                @endforeach
            </div>
        </div>
    @endif
    
    @include('site.adsmart.ticket_topo_inc')
    
    <form name="formMembershipInclude" action="" method="POST" id="formMembershipInclude" ng-submit="enviar_formulario($event);">
        <input type="hidden" name="_token" value="{{ csrf_token()}}">
        <section class="bs border-color-primary margin-top-45 margin-bottom-40 padding-bottom-30">
            <h4 class="master-title-alt txt-bold margin-bottom-20">Participantes</h4>

            <div ng-cloak class="block">
                <h4 class="subtitle txt-small txt-uppercase txt-lighter margin-bottom-15">Condições</h4>
                <div class="flexbox-container">
                    <!-- TO-DO Condições -->
                    <div class="flex-fluid">
                        <ul class="list-group-indent">
                            <li class="list-item-horizontal">
                                <label class="indent clickable no-underline">
                                    <div class="pull">
                                        <i class="icon fa fa-rocket"></i>
                                        <span class="text txt-bold">Auto-aprovação <small class="txt-small txt-regular margin-left-20">Você irá executar todas as etapas do ticket sem envolver ninguém no processo de aprovação.</small></span>
                                        <span class="switch-control pull-right no-margin">
                                            <input {{ ($bloquearAutoAprovacao) ? 'disabled' : '' }} ng-model="chkAutoReprovacao" value='1' ng-change="auto_aprovacao_check()" type="checkbox" name="chkTicketAutoReprovacao" id="chkTicketAutoReprovacao" />
                                            <span class="switch-toggle abs-right-offset no-margin"></span>
                                        </span>
                                    </div>
                                </label>
                            </li>
                            <li class="list-item-horizontal">
                                <label class="indent clickable no-underline">
                                    <div class="pull">
                                        <i class="icon fa fa-thumbs-o-up"></i>
                                        <span class="text txt-bold">Aprovadores de Mídia <small class="txt-small txt-regular margin-left-20">Você irá selecionar participantes para cada etapa e o grupo de aprovadores de mídia.</small></span>
                                        <span class="switch-control pull-right no-margin">
                                            <input ng-model="chkAprovadoresMidia" value='1' ng-change="aprovadores_midia_check()" type="checkbox" name="chkTicketAprovadoresMidia" id="chkTicketAprovadoresMidia" />
                                            <span class="switch-toggle abs-right-offset no-margin"></span>
                                        </span>
                                    </div>
                                </label>
                            </li>
                        </ul>
                    </div>
                    <!-- END TO-DO Condições -->
                </div>
            </div>
            <!-- Tarefas e Participantes -->
            <div ng-cloak class="block">
                <h4 class="subtitle txt-small txt-uppercase txt-lighter margin-bottom-15">Tarefas e Participantes</h4>
                <!-- Timeline vertical -->
                <div class="timeline-vertical no-animate">
                    <div class="event-box">
                        <div ng-class="{'colored-primary': chkAutoReprovacao || chkAprovadoresMidia}" class="event">
                            <i class="icon fa fa-flag"></i>
                            <div class="body">
                                <h3 class="event-title bb-event-title"><span><strong class="txt-bold">Aberto</strong>&nbsp;/&nbsp;Preencher informações iniciais</span></h3>
                                <ul class="event-tasks">
                                    <li class="no-border">
                                        <div class="col-select-member">
                                            <selectize config="participantesConfig" options="participantes" name="cbParticipantes[]" ng-model="participante_inicio" class="disabled"></selectize>
                                            <input type='hidden' ng-value='participante_inicio' name='participante_inicio' />
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="event-box">
                        <div ng-class="{'colored-primary': chkAutoReprovacao || chkAprovadoresMidia}" class="event">
                            <i class="icon fa fa-upload"></i>
                            <div class="body">
                                <h3 class="event-title bb-event-title"><span><strong class="txt-bold">Anexação</strong>&nbsp;/&nbsp;Anexar arquivo de mídia</span></h3>
                                <div class="col-select-member flexbox-container flexbox-group">
                                    <div class="flex flex-fluid">    
                                        <selectize config="participantesConfig" options="participantes" name="participantes_anexacao[]" ng-model="participantes_anexacao" ng-disabled="!chkAutoReprovacao && !chkAprovadoresMidia"></selectize>
                                    </div>
                                    <div ng-show="chkAutoReprovacao || chkAprovadoresMidia" class="flex">
                                        <button ng-disabled="!participantes_anexacao" ng-click="add_participante_anexacao()" type="button" class="btn-control btn-positive">Adicionar</button>
                                    </div>
                                </div>
                                <table ng-show="usuarios_anexacao_selecionados.length" class="table table-border table-hover margin-top-20">
                                    <tbody>
                                        <tr ng-repeat="(u, usuario) in usuarios_anexacao_selecionados">
                                            <td class="table-cell-h">
                                                <div class="d-flex flex-align-center">
                                                    <i class="icon fa fa-user txt-color-primary"></i>
                                                    <div class="text-side">@{{usuario.nome}}</div>
                                                    <input type="hidden" name="participantes_anexacao[@{{u}}]" ng-value="usuario.id" />
                                                </div>
                                            </td>
                                            <td class="table-cell-h">
                                                <span class="txt-regular">@{{usuario.empresa}} / @{{usuario.departamento ? usuario.departamento : 'Departamento'}}</span>
                                            </td>
                                            <td width="50" class="table-cell-h">
                                                <div data-title="Opções" class="dropdown drop-to-left ui-tooltip top no-margin">
                                                    <a href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                                    <div class="dropdown-list">
                                                        <ul>    
                                                            <li><a ng-click="excluir_participante_anexacao(u)" href="javascript:;" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="event-box">
                        <div ng-class="{'colored-primary': chkAutoReprovacao || chkAprovadoresMidia}" class="event">
                            <i class="icon fa fa-eye"></i>
                            <div class="body">
                                <h3 class="event-title bb-event-title"><span><strong class="txt-bold">Revisão</strong>&nbsp;/&nbsp;Revisar arquivo de mídia seguindo critérios de avaliação</span></h3>
                                <div class="col-select-member flexbox-container flexbox-group">
                                    <div class="flex flex-fluid">
                                        <selectize config="participantesConfig" options="participantes" name="participantes_revisao[]" ng-model="participantes_revisao" ng-disabled="!chkAutoReprovacao && !chkAprovadoresMidia"></selectize>
                                    </div>
                                    <div ng-show="chkAutoReprovacao || chkAprovadoresMidia" class="flex">
                                        <button ng-disabled="!participantes_revisao" ng-click="add_participante_revisao()" type="button" class="btn-control btn-positive">Adicionar</button>
                                    </div>
                                </div>
                                <table ng-show="usuarios_revisao_selecionados.length" class="table table-border table-hover margin-top-20">
                                    <tbody>
                                        <tr ng-repeat="(u, usuario) in usuarios_revisao_selecionados">
                                            <td class="table-cell-h">
                                                <div class="d-flex flex-align-center">
                                                    <i class="icon fa fa-user txt-color-primary"></i>
                                                    <div class="text-side">@{{usuario.nome}}</div>
                                                    <input type="hidden" name="participantes_revisao[@{{u}}]" ng-value="usuario.id" />
                                                </div>
                                            </td>
                                            <td class="table-cell-h">
                                                <span class="txt-regular">@{{usuario.empresa}} / @{{usuario.departamento ? usuario.departamento : 'Departamento'}}</span>
                                            </td>
                                            <td width="50" class="table-cell-h">
                                                <div data-title="Opções" class="dropdown drop-to-left ui-tooltip top no-margin">
                                                    <a href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                                    <div class="dropdown-list">
                                                        <ul>    
                                                            <li><a ng-click="excluir_participante_revisao(u)" href="javascript:;" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="event-box no-margin-bottom">
                        <div ng-class="{'colored-primary': chkAutoReprovacao || chkAprovadoresMidia}" class="event">
                            <i class="icon fa fa-gavel"></i>
                            <div class="body">
                                <h3 class="event-title bb-event-title"><span><strong class="txt-bold">Aprovação</strong>&nbsp;/&nbsp;Aprovar arquivo de mídia seguindo critérios de avaliação</span></h3>
                                <div ng-show="chkAutoReprovacao" class="col-select-member flexbox-container flexbox-group">
                                    <div class="flex flex-fluid">
                                        <selectize config="participantesConfig" options="participantes" name="participantes_aprovacao[]" ng-model="participantes_aprovacao" ng-disabled="!chkAutoReprovacao && !chkAprovadoresMidia"></selectize>
                                    </div>
                                    <div class="flex">
                                        <button ng-disabled="!participantes_aprovacao" ng-click="add_participante_aprovacao()" type="button" class="btn-control btn-positive">Adicionar</button>
                                    </div>
                                </div>
                                <div ng-show="chkAprovadoresMidia || (!chkAutoReprovacao && !chkAprovadoresMidia)" class="col-select-member flexbox-container flexbox-group">
                                    <div class="flex flex-fluid">     
                                        <selectize ng-change="carregar_aprovadores_midia()" config="aprovadoresMidiaConfig" options="aprovadores_midia" name="aprovador_midia" ng-model="aprovador_midia" ng-disabled="!chkAutoReprovacao && !chkAprovadoresMidia"></selectize>
                                        <input type="hidden" name="aprovador_midia" ng-value="aprovador_midia_id" />
                                    </div>
                                    <div ng-show="chkAprovadoresMidia && !grupo_aprovacao_has_selected" class="flex">
                                        <button ng-disabled="!aprovador_midia" ng-click="add_aprovadores_midia()" type="button" class="btn-control btn-positive">Adicionar</button>
                                    </div>
                                    <div ng-show="chkAprovadoresMidia && usuarios_selecionados.length && grupo_aprovacao_has_selected" class="flex">
                                        <button ng-click="excluir_aprovadores_midia()" type="button" class="btn-control btn-alt">Excluir</button>
                                    </div>
                                </div>
                                <!-- TO-DO Aprovadores de Mídia -->
                                <table ng-if="usuarios_selecionados.length" class="table table-border table-hover margin-top-20">
                                    <tbody>
                                        <tr ng-repeat="(u, usuario_selecionado) in usuarios_selecionados track by $index">
                                            <td class="table-cell-h">
                                                <div class="d-flex flex-align-center">
                                                    <i ng-class="{'txt-color-primary': usuario_selecionado && grupo_aprovacao_has_selected || usuario_selecionado && participante_has_selected}" class="icon fa fa-user"></i>
                                                    <div class="text-side">@{{ usuario_selecionado.nome }}</div>
                                                    <input type="hidden" name="participantes_aprovacao[@{{u}}]" ng-value="usuario_selecionado.id" />
                                                </div>
                                            </td>
                                            <td class="table-cell-h">
                                                <span class="txt-regular">@{{ usuario_selecionado.empresa }} / @{{ usuario_selecionado.departamento }}</span>
                                            </td>
                                            <td width="50" class="table-cell-h">
                                                <div data-title="Opções" ng-show="chkAutoReprovacao" class="dropdown drop-to-left ui-tooltip top no-margin">
                                                    <a href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                                    <div class="dropdown-list">
                                                        <ul>    
                                                            <li><a ng-click="excluir_participante_aprovacao($index)" href="javascript:;" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!-- END TO-DO Aprovadores de Mídia -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Tarefas e Participantes -->
        </section>
        <div class="btn-group txt-center">
            <button onclick="window.location.href='{{ route('site.adsmart.ticket.edicao', $ticket->numero) }}'" type="button" class="btn-control btn-call-to-action call-to-action-negative btn-icon btn-icon-left">
                <span><i class="icon fa fa-arrow-left"></i> Etapa Anterior</span>
            </button>
            <button type="submit" id="sendBtn" ng-disabled="(!usuarios_anexacao_selecionados.length || !usuarios_revisao_selecionados.length || !usuarios_selecionados.length) || !participante_has_selected && !grupo_aprovacao_has_selected" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-right">
                <span><i class="icon fa fa-arrow-right"></i> Confirmar e Enviar</span>
            </button>
        </div>
    </form>
    <!-- END Participantes -->
</div>
@stop
