@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('adsmart.condicoes_midia_alterar') !!}

<script>
    var csrf_token = '{{ csrf_token() }}';

    var id = '{{ $condicao->id }}';

    var condicaoMidiaNome = '{!! $condicao->nome !!}';

    var txtCondicaoMidia = "{!! str_replace( array( "\n", "\r" ), array( "\\n", "\\r" ), addslashes($condicao->texto) ) !!}";

    var chkCondicaoMidiaTipo_1 = {{ ($condicao->status) ? 'true' : 'false' }};

    var chkCondicaoMidiaTipo_2 = {{ ($condicao->abertura_ticket) ? 'true' : 'false' }};

    var chkCondicaoMidiaTipo_3 = {{ ($condicao->reprovacao) ? 'true' : 'false' }};

    var chkCondicaoMidiaTipo_4 = {{ ($condicao->obrigatoria) ? 'true' : 'false' }};

    var chkCondicaoMidiaTipo_5 = {{ ($condicao->bloquear_auto_aprovacao) ? 'true' : 'false' }};
</script>

<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />

<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/condicao-midia-editar-controller.js') }}"></script>
<script src="{{ asset('/js/site/autosize.js') }}"></script>

<script>
    $(function() {
        autosize(document.querySelectorAll('textarea'));
    });
</script>

<div ng-app="app" ng-controller="CondicaoMidiaCriarController" ng-cloak class="content content-fluid clearfix">
    <section class="artw-package-edit-container margin-top-10 margin-bottom-40">
        <form name="condicoesMidiaCriarForm" action="" method="POST" novalidate>
            <input type="hidden" name="_token" value="{{ csrf_token()}}" />
            <h3 class="master-title txt-bold margin-bottom-20">Editar Critério de Avaliação</h3>
            <div class="block">
                <div class="flexbox-container flexbox-group margin-top-20 margin-bottom-15 margin-left-10 margin-right-10">
                    <h4 class="txt-lighter txt-small txt-uppercase">Critério de Avaliação *</h4>
                </div>
                <div class="bs no-border no-padding margin-bottom-30">
                    <div class="flexbox-container">
                        <div class="flex flex-fluid">
                            <input type="text" placeholder="Título do critério de avaliação" ng-model="condicaoMidiaNome" name="condicaoMidiaNome" id="condicaoMidiaNome" class="input-control only-alpha-numeric" />
                        </div>
                    </div>
                </div>
                <div class="bs border-color-primary padding-bottom-30">
                    <div class="block margin-bottom-15 margin-left-10 margin-right-10">
                        <h4 class="txt-lighter txt-small txt-uppercase margin-bottom-15">Texto *</h4>
                    </div>
                    <div class="flexbox-container flex-align-center">
                        <div class="flex flex-fluid">
                            <textarea placeholder="Texto do critério de avaliação" rows="5" ng-model="txtCondicaoMidia" name="txtCondicaoMidia" id="txtCondicaoMidia" class="input-control textbox-control textbox-fake-auto-resizable"></textarea>
                        </div>
                    </div>
                    <div class="flexbox-container flex-align-center margin-top-30">
                        <div class="flex flex-fluid">
                            <div class="block margin-bottom-15 margin-left-10 margin-right-10">
                                <h4 class="txt-lighter txt-small txt-uppercase margin-bottom-15">Opções *</h4>
                            </div>
                            <ul class="list-group-indent">
                                <li class="list-item-horizontal">
                                    <label class="indent clickable no-underline">
                                        <strong class="pull">
                                            <i class="icon fa fa-quote-right"></i>
                                            <span class="text txt-bold no-underline">Critério de Avaliação Ativo <small class="txt-small txt-regular margin-left-20">Exibe nas listas de critérios de avaliação.</small></span>
                                            <span class="switch-control pull-right no-margin">
                                                <input ng-model="chkCondicaoMidiaTipo_1" name="chkOpcaoCriterioAvaliacaoAtivo" id="chkOpcaoCriterioAvaliacaoAtivo" type="checkbox" />
                                                <span class="switch-toggle abs-right-offset no-margin"></span>
                                            </span>
                                        </strong>
                                    </label>
                                </li>
                                <li class="list-item-horizontal">
                                    <label class="indent clickable no-underline">
                                        <strong class="pull">
                                            <i class="icon fa fa-flag"></i>
                                            <span class="text txt-bold no-underline">Abertura de Ticket <small class="txt-small txt-regular margin-left-20">Exibe na lista de critérios de avaliação na abertura de tickets.</small></span>
                                            <span class="switch-control pull-right no-margin">
                                                <input ng-disabled="!chkCondicaoMidiaTipo_1" ng-model="chkCondicaoMidiaTipo_2" ng-change="aberturaTicketUncheck()" name="chkOpcaoExibirAberturaTicket" id="chkOpcaoExibirAberturaTicket" type="checkbox" />
                                                <span class="switch-toggle abs-right-offset no-margin"></span>
                                            </span>
                                        </strong>
                                    </label>
                                </li>
                                <li class="list-item-horizontal">
                                    <label class="indent clickable no-underline">
                                        <strong class="pull">
                                            <i class="icon fa fa-ban"></i>
                                            <span class="text txt-bold no-underline">Reprovação de Ticket <small class="txt-small txt-regular margin-left-20">Exibe na lista de critérios de avaliação na reprovação de tickets.</small></span>
                                            <span class="switch-control pull-right no-margin">
                                                <input ng-disabled="!chkCondicaoMidiaTipo_1" ng-model="chkCondicaoMidiaTipo_3" ng-change="reprovacaoTicketUncheck()" name="chkOpcaoExibirReprovacaoTicket" id="chkOpcaoExibirReprovacaoTicket" type="checkbox" />
                                                <span class="switch-toggle abs-right-offset no-margin"></span>
                                            </span>
                                        </strong>
                                    </label>
                                </li>
                                <li class="list-item-horizontal">
                                    <label class="indent clickable no-underline">
                                        <strong class="pull">
                                            <i class="icon fa fa-balance-scale"></i>
                                            <span class="text txt-bold no-underline">Obrigatório <small class="txt-small txt-regular margin-left-20">Adiciona este critério de avaliação automaticamente na abertura de tickets.</small></span>
                                            <span class="switch-control pull-right no-margin">
                                                <input ng-model="chkCondicaoMidiaTipo_4" type="checkbox" name="chkOpcaoObrigatoria" id="chkOpcaoObrigatoria" />
                                                <span class="switch-toggle abs-right-offset no-margin"></span>
                                            </span>
                                        </strong>
                                    </label>
                                </li>
                                <li class="list-item-horizontal">
                                    <label class="indent clickable no-underline">
                                        <strong class="pull">
                                            <i class="icon fa fa-hand-stop-o"></i>
                                            <span class="text txt-bold no-underline">Bloquear a Auto-aprovação <small class="txt-small txt-regular margin-left-20">Quando utilizado não permite o uso da auto-aprovação na abertura de tickets.</small></span>
                                            <span class="switch-control pull-right no-margin">
                                                <input ng-model="chkCondicaoMidiaTipo_5" type="checkbox" name="chkOpcaoBloquearAutoAprovacao" id="chkOpcaoBloquearAutoAprovacao" />
                                                <span class="switch-toggle abs-right-offset no-margin"></span>
                                            </span>
                                        </strong>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>    
                </div>
                <div class="btn-group txt-center margin-top-40" ng-if="condicaoMidiaNome && txtCondicaoMidia" class="form-section clearfix">
                    <a href="{{ route('site.adsmart.condicoes_midia') }}" class="btn-control btn-call-to-action call-to-action-negative btn-icon btn-icon-left">
                        <span><i class="icon fa fa-times"></i> Cancelar</span>
                    </a>
                    <button type="button" ng-disabled="!chkCondicaoMidiaTipo_2 && !chkCondicaoMidiaTipo_3" ng-click="enviarFormulario()" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                        <span><i class="icon fa fa-check"></i> Salvar</span>
                    </button>
                </div>
            </div>
        </form>
    </section>
</div>
@stop