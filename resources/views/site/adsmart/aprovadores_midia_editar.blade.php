@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('adsmart.aprovadores_midia_alterar') !!}

<script>
    var csrf_token = '{{ csrf_token() }}';

    var id = '{{ $aprovadorMidia->id }}'

    var aprovadoresMidiaNome = '{!! $aprovadorMidia->nome !!}'

    var usuarios_selecionados = [];

    @foreach ($aprovadorMidia->usuarios as $usuarios)
    
    usuarios_selecionados.push( {!! '{id: ' . $usuarios->id . ',nome:"'.$usuarios->nome_abreviado.'"' . ',empresa:"'.$usuarios->admEmpresa->nome.'"' . ',departamento:"'.$usuarios->admDepartamento->nome.'"}' !!} );
    
    @endforeach



</script>

<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />

<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/aprovadores-midia-editar-controller.js') }}"></script>

<div ng-app="app" ng-controller="AprovadoresMidiaCriarController" ng-cloak class="content content-fluid clearfix">
    <section class="artw-package-edit-container margin-top-10 margin-bottom-40">
        <form name="packageCreateForm" action="" method="POST" novalidate>
            <input type="hidden" name="_token" value="{{ csrf_token()}}" />
            <h3 class="master-title txt-bold margin-bottom-20">Editar Aprovadores de Mídia</h3>
            <div class="block">
                <div class="flexbox-container flexbox-group margin-top-20 margin-bottom-15 margin-left-10 margin-right-10">
                    <h4 class="txt-lighter txt-small txt-uppercase">Aprovadores de Mídia *</h4>
                </div>
                <div class="bs no-border no-padding margin-bottom-30">
                    <div class="flexbox-container">
                        <div class="flex flex-fluid">
                            <input type="text" placeholder="Nome dos aprovadores de mídia" ng-model="aprovadoresMidiaNome" name="aprovadoresMidiaNome" id="aprovadoresMidiaNome" class="input-control only-alpha-numeric" />
                        </div>
                    </div>
                </div>
                <div class="bs border-color-primary padding-bottom-30">
                    <div class="block margin-bottom-15 margin-left-10 margin-right-10">
                        <h4 class="txt-lighter txt-small txt-uppercase margin-bottom-15">Usuários *</h4>
                        <p class="txt-regular">
                            Adicione os usuários que farão parte destes Aprovadores de Mídia.
                        </p>
                    </div>
                    <div class="flexbox-container flex-align-center">
                        <div class="flex flex-fluid">
                            <selectize placeholder="Selecione o usuário" ng-model="cbUsuario" config="usuarioConfig" options="usuarios" name="cbUsuario" id="cbUsuario"></selectize>            
                        </div>

                        <div class="flex">
                            <button type="button" ng-click="add_usuario_selecionado()" class="btn-control btn-positive">Adicionar</button>
                        </div>
                    </div>
                    <div class="flexbox-container flex-align-center margin-top-15">
                        <table class="artw-shared-item-table table-border table-hover no-animate">
                            <tbody>
                                <tr ng-repeat="usuario_selecionado in usuarios_selecionados track by $index">
                                    <td width="">
                                        <div class="d-flex flex-align-center">
                                            <i class="icon fa fa-user"></i>
                                            <span class="text-side margin-left-10">@{{ usuario_selecionado.nome }}</span>
                                        </div>
                                    </td>
                                    <td width="400"><span class="txt-regular">@{{ usuario_selecionado.empresa }} / @{{ usuario_selecionado.departamento }}</span></td>
                                    <td width="50">
                                        <div class="dropdown drop-to-left">
                                            <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                            <div class="dropdown-list">
                                                <ul> 
                                                    <li><a href="javascript:;" ng-click="excluir_usuario_selecionado($index)" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>    
                </div>
                <div class="btn-group txt-center margin-top-40" ng-if="aprovadoresMidiaNome && usuarios_selecionados.length" class="form-section clearfix">
                    <a href="{{ route('site.adsmart.aprovadores_midia') }}" class="btn-control btn-call-to-action call-to-action-negative btn-icon btn-icon-left">
                        <span><i class="icon fa fa-times"></i> Cancelar</span>
                    </a>
                    <button type="button" ng-click="enviarFormulario()" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                        <span><i class="icon fa fa-check"></i> Salvar</span>
                    </button>
                </div>
            </div>
        </form>
    </section>
</div>
@stop