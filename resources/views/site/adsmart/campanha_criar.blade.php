@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
{!! Breadcrumbs::renderIfExists('adsmart.campanha_criar') !!}
<script>
    var csrf_token = '{{ csrf_token() }}';
</script>

<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />

<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/campanha-criar-controller.js') }}"></script>
<script src="{{ asset2('/js/site/adsmart/campanha-criar.js') }}"></script>

<div class="content content-fluid clearfix" ng-app="app" ng-controller="CampanhaCriarCtrl" ng-cloak>
    <section class="artw-package-edit-container margin-top-10 margin-bottom-40">
        <form name="packageCreateForm" action="" method="POST" novalidate>
            <input type="hidden" name="_token" value="{{ csrf_token()}}" />
            <h3 class="master-title txt-bold margin-bottom-20">Nova / Alterar Campanha</h3>
            
            <div class="block">
                <div class="flexbox-container flexbox-group margin-top-20 margin-bottom-15 margin-left-10 margin-right-10">
                    <h4 class="txt-lighter txt-small txt-uppercase">Campanha *</h4>
                </div>
                <div class="bs no-border no-padding margin-bottom-30">
                    <div class="flexbox-container">
                        <div class="flex flex-fluid">
                            <input type="text" placeholder="Nome da campanha" ng-model="campanhaNome" name="campanhaNome" id="campanhaNome" class="input-control only-alpha-numeric" />
                        </div>
                    </div>
                </div>
                <div ng-class="{'bs border-color-primary padding-bottom-30': campanhaNome && familias_selecionadas.length}">
                    <div class="block margin-bottom-15 margin-left-10 margin-right-10">
                        <h4 class="txt-lighter txt-small txt-uppercase margin-bottom-15">A campanha é para quais marcas</h4>
                        <p class="txt-regular">
                            Adicione uma ou mais Marcas para a Campanha.
                        </p>
                    </div>
                    <div class="flexbox-container flex-align-center">
                        <div class="col-add-marca flex flex-fluid">
                            <selectize placeholder="Selecione a marca" name="cbFamilias" id="cbFamilias" config="familiasConfig" options="familias" ng-model="cbFamilias"></selectize>            
                        </div>

                        <div class="flex">
                            <button type="button" ng-click="add_familia_selecionada()" class="btn-control btn-positive">Adicionar</button>
                        </div>
                    </div>
                    <div class="flexbox-container flex-align-center margin-top-20">
                        <table class="artw-shared-item-table table-border table-hover no-animate">
                            <tbody>
                                <tr ng-repeat="familia in familias_selecionadas track by $index">
                                    <td width="">
                                        <div class="d-flex flex-align-baseline">
                                            <i class="icon fa fa-diamond"></i>
                                            <span class="text-side margin-left-10">@{{familia.nome}}</span>
                                        </div>
                                    </td>
                                    <td width="50">
                                        <div class="dropdown drop-to-left">
                                            <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                            <div class="dropdown-list">
                                                <ul> 
                                                    <li><a href="javascript:;" ng-click="excluir_familia_selecionada($index)" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>    
                </div>
                <div class="btn-group txt-center margin-top-40" ng-if="campanhaNome && familias_selecionadas.length" class="form-section clearfix">
                    <a href="{{ route('site.adsmart.familias') }}" class="btn-control btn-call-to-action call-to-action-negative btn-icon btn-icon-left">
                        <span><i class="icon fa fa-times"></i> Cancelar</span>
                    </a>
                    <button type="button" ng-click="enviarFormulario()" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                        <span><i class="icon fa fa-check"></i> Criar Campanha</span>
                    </button>
                </div>
            </div>
        </form>
    </section>
</div>
@stop