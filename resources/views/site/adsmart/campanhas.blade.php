@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
{{--<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/artwork.css') }}" />--}}

{!! Breadcrumbs::renderIfExists('adsmart.campanhas', $id_familia) !!}

<div class="content content-fluid">
	<div class="artwork-container">
		<section class="brand-list padding-bottom-40 no-margin-bottom">
			<h3 class="master-title">Campanhas</h3>
			<ul class="tabs clearfix">
				<li><a href="javascript:;" class="txt-color-default active">Campanhas</a></li>
				{{--<li><a href="javascript:;" class="disabled">Marcas Descontinuadas</a></li>--}}
			</ul>
			{!! Form::open(array('class' => '', 'id' => 'form-category', 'method' => 'get', 'url' => Request::url())) !!}
				<div class="block margin-top-20">
					<div class="flexbox-container">
						{{--<div class="flexbox-column flex flex-3-large">
							<div class="select-control">
								@if(isset($categorias))
									{!! Form::select('categorias', $categorias, app('request')->input('categorias'), array('id' => 'categoria', 'placeholder' => 'Categorias', 'onchange' => 'this.form.submit()')) !!}
								@endif
							</div>
						</div>--}}
						@if((in_array('adsmart_criar_campanha', Auth::user()->modulos)))
							<div class="artw-create-package-btn flex">
								<a href="{{ route('site.adsmart.campanha.criar.get') }}" class="btn-function"><i class="icon fa fa-bullhorn"></i> Nova Campanha</a>
							</div>
						@endif
					</div>
				</div>
			{!! Form::close() !!}
			@if(count($campanhas) > 0)
				<table cellpading="0" cellspacing="0" class="artw-brand-table table-border table-hover no-animate">
					<tbody>
						@foreach($campanhas as $key => $campanha)
							<tr class="table-group">
								<td colspan="2">
									<table cellpading="0" cellspacing="0">
										<tbody>
											<tr>
												<td width="" class="table-cell-h"><a href="{{ URL::route('site.adsmart.midias', [$id_familia, $campanha->id]) }}" class="table-link d-flex flex-align-center txt-color-default"><i class="icon fa fa-folder txt-color-alt"></i><span class="text-side txt-regular">{{ $campanha->nome }}</span></a></td>
												<td width="50">
													@if (!count($campanha->campanhaMidias))
													@if((in_array('adsmart_excluir_campanha', Auth::user()->modulos)))
													<div class="dropdown drop-to-left">
														<a class="dropdown-toggle fa fa-ellipsis-h"></a>
														<div class="dropdown-list">
															<ul>    
																<li><a href="javascript:;" onclick="excluir('Deseja excluir essa campanha?', '{{ route('site.adsmart.campanha.excluir', $campanha->id) }}')" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir Campanha</a></li>
															</ul>
														</div>														
													</div>
													@endif
													@endif
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			@else
				<div class="artw-no-item fa fa-bullhorn">
					Nenhuma campanha localizada
				</div>
			@endif
		</section>
	</div>
</div>
@stop
