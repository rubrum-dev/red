@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('artwork.ticket', $ticket->id) !!}

<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('/css/angular/v-accordion.min.css')}}" />

<script>
    var csrf_token = '{{ csrf_token() }}';
    var id_ticket = {{ $ticket->id }};
</script>

<script src="{{ asset('/js/site/snap.svg-min.js') }}"></script>
<script src="{{ asset('/js/site/artwork/ticket-edit.js') }}"></script>
<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-route.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset('/js/angular/v-accordion.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload-shim.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload.min.js') }}"></script>
<script src="{{ asset('/js/angular/adsmart-ticket-resolve-loader.js') }}"></script>

<script src="{{ asset2('/js/angular/controllers/adsmart/ticket-aba-visao-geral-controller.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/ticket-aba-anexos-controller.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/ticket-aba-tarefas-controller.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/ticket-aba-mensagens-controller.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/ticket-aba-opcoes-controller.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/ticket-controller.js') }}"></script>

<div class="content content-fluid" ng-app="app" ng-controller="TicketCtrl" ng-cloak>
    
    @include('site.adsmart.ticket_topo_inc')
    
    <!-- Timeline -->
    <div class="timeline" ng-cloak>
        
        <div ng-class="(ticket.adsmartEtapas[1] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.adsmartEtapas[0] && !ticket.adsmartEtapas[1]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.adsmartEtapas[0] && !ticket.adsmartEtapas[1]) && ticket.cancelado_em}" class="event-icon fa fa-flag"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.adsmartEtapas[0] && !ticket.adsmartEtapas[1]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.adsmartEtapas[0] && !ticket.adsmartEtapas[1]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Aberto</span>
            </div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[2] === 1) ? 'event-box is-done' : 'event-box'" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[2] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.adsmartEtapas[2] && !ticket.adsmartEtapas[3]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.adsmartEtapas[2] && !ticket.adsmartEtapas[3]) && ticket.cancelado_em}" class="event-icon fa fa-upload"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.adsmartEtapas[2] && !ticket.adsmartEtapas[3]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.adsmartEtapas[2] && !ticket.adsmartEtapas[3]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Anexação</span>
            </div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[3] === 1 && ticket.ciclo_atual.layout.length) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[3] === 1 && ticket.ciclo_atual.layout.length) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <span ng-if="(ticket.adsmartEtapas[3] && !ticket.adsmartEtapas[4]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.adsmartEtapas[3] && !ticket.adsmartEtapas[4]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <div ng-class="{'sub paused-at paused-at-btn clickable fa fa-pause-circle': (ticket.adsmartEtapas[3] && !ticket.adsmartEtapas[4]) && ticket.ciclo_atual.layout.length && ticket.pausado_em, 'sub canceled-at fa fa-times-circle': (ticket.adsmartEtapas[3] && !ticket.adsmartEtapas[4]) && ticket.ciclo_atual.layout.length && ticket.cancelado_em}" class="dot"></div>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>

            </div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[4] === 1 && ticket.ciclo_atual.layout.length) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[4] === 1 && ticket.ciclo_atual.layout.length) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.adsmartEtapas[4] && !ticket.adsmartEtapas[5]) && ticket.ciclo_atual.layout.length && ticket.pausado_em, 'canceled-at fa-times': (ticket.adsmartEtapas[4] && !ticket.adsmartEtapas[5]) && ticket.ciclo_atual.layout.length && ticket.cancelado_em}" class="event-icon fa fa-eye"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.adsmartEtapas[4] && !ticket.adsmartEtapas[5]) && ticket.ciclo_atual.layout.length && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.adsmartEtapas[4] && !ticket.adsmartEtapas[5]) && ticket.ciclo_atual.layout.length && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Revisão</span>
            </div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[5] === 1 && ticket.ciclo_atual.layout.length) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[5] === 1 && ticket.ciclo_atual.layout.length) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <span ng-if="(ticket.adsmartEtapas[5] && !ticket.adsmartEtapas[6]) && ticket.ciclo_atual.layout.length && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.adsmartEtapas[5] && !ticket.adsmartEtapas[6]) && ticket.ciclo_atual.layout.length && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <div ng-class="{'sub paused-at paused-at-btn clickable fa fa-pause-circle': (ticket.adsmartEtapas[5] && !ticket.adsmartEtapas[6]) && ticket.ciclo_atual.layout.length && ticket.pausado_em, 'sub canceled-at fa fa-times-circle': (ticket.adsmartEtapas[5] && !ticket.adsmartEtapas[6]) && ticket.ciclo_atual.layout.length && ticket.cancelado_em}" class="dot"></div>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[6] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[6] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.adsmartEtapas[6] && !ticket.adsmartEtapas[7]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.adsmartEtapas[6] && !ticket.adsmartEtapas[7]) && ticket.cancelado_em}" class="event-icon fa fa-thumbs-up"></i>
                <div class="event-dropdown-list to-left">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.adsmartEtapas[6] && !ticket.adsmartEtapas[7]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.adsmartEtapas[6] && !ticket.adsmartEtapas[7]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Aprovação</span>
            </div>
        </div>

    </div>
    <!-- END Timeline -->
    <!-- Content -->
    <div class="workflow-container">
        <form name="formTicketEdicao" action="" method="POST" onsubmit="return false;" class="form-workflow-ctrl">
            <!-- Tabs -->
            <ul class="tabs clearfix" ng-cloak>
                <li><a ng-click="$root.change_tab($event)" ng-class="{'active' : activetab == '/visao-geral'}" ng-href="#/visao-geral">Visão Geral</a></li>
                <li><a ng-click="$root.change_tab($event)" ng-class="{'active' : activetab == '/anexos'}" ng-href="#/anexos">Anexos</a></li>
                <li><a ng-click="$root.change_tab($event)" ng-class="{'active' : activetab == '/tarefas'}" ng-href="#/tarefas">Tarefas</a></li>
                <li><a ng-click="$root.change_tab($event)" ng-class="{'active' : activetab == '/mensagens'}" ng-href="#/mensagens">Mensagens</a></li>
                <li><a ng-click="$root.change_tab($event)" ng-class="{'active' : activetab == '/opcoes', 'disabled': ticket.cancelado_em || ticket.adsmartEtapas[7] || ticket.proximo_ciclo}" ng-href="#/opcoes">Opções</a></li>
            </ul>
            <!-- END Tabs -->
            
            <!-- ng-view -->
            <div ng-view class="fadein"></div>
            <!-- END ng-view -->

            <div resolve-loader></div>

            <div ng-show="statechange">
            
                <div ng-if="ticket.libera_ticket" class="btn-group txt-center no-animate" ng-cloak>

                    <div ng-show="ticket.adsmartEtapas[3] && !ticket.adsmartEtapas[4] && ticket.ciclo_atual.layout.length && activetab == '/visao-geral' && ticket.libera_status == 1" class="bs border-color-primary padding-top-40 no-padding-bottom">
                        <button mw-confirm-click="acao_ticket('revisao');" mw-confirm-click-message="Deseja submeter à revisão?" type="button" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i>Submeter à Revisão</span>
                        </button>
                    </div>

                    <div ng-show="ticket.adsmartEtapas[4] && !ticket.adsmartEtapas[6] && ticket.ciclo_atual.layout.length && activetab == '/visao-geral' && ticket.libera_status == 1" class="bs border-color-primary padding-top-40 no-padding-bottom">
                        
                        <button ng-show="ticket.adsmartEtapas[5]" mw-confirm-click="acao_ticket('aprovacao');" mw-confirm-click-message="Deseja submeter à aprovação?" type="button" ng-show="ticket.adsmart_qtde_aprovadores" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i>Submeter à Aprovação</span>
                        </button>

                        <button ng-show="!ticket.adsmartEtapas[5]" onclick="myAlert('Marque todos os Critério de Avaliação / Instruções como revisados antes de submeter à aprovação.', 'warning')" type="button" ng-show="!ticket.adsmart_qtde_aprovadores" class="btn-control btn-call-to-action call-to-action-default btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i>Submeter à Aprovação</span>
                        </button>

                        <button onclick="myAlert('Para prosseguir para os ciclos de aprovação, adicione pelo menos um participante como aprovador nesse ticket.', 'warning')" type="button" ng-show="!ticket.adsmart_qtde_aprovadores" class="btn-control btn-call-to-action call-to-action-default btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i>Submeter à Aprovação</span>
                        </button>

                    </div>

                    <div ng-show="ticket.adsmartEtapas[6] && !ticket.adsmartEtapas[7] && activetab == '/visao-geral' && ticket.libera_status == 1" class="bs border-color-primary padding-top-40 no-padding-bottom">
                        <button onClick="location='{{Route('site.adsmart.ticket.deny', [$ticket->numero])}}'" type="button" class="btn-control btn-call-to-action call-to-action-negative btn-icon btn-icon-left">
                            <span><i class="icon fa fa-times"></i>Reprovar</span>
                        </button>
        
                        {{--<button mw-confirm-click="acao_ticket('aprovar');" mw-confirm-click-message="Deseja aprovar?" type="button" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i>Aprovar</span>
                        </button>--}}
                        <button mw-confirm-click="modal_consideracao_aprovacao()" mw-confirm-click-message="Deseja aprovar?" type="button" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i>Aprovar</span>
                        </button>
                    </div>

                </div>

            </div>
        </form>
    </div>
    <!-- END Content -->
</div>
@stop