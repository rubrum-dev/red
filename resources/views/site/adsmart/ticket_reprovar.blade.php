@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />

<script>
    var csrf_token = '{{ csrf_token() }}';
    var id_ticket = {{$ticket->id}};
    var id_usuario = {{Auth::user()->id}};
</script>

<script>
    var tipos = [];
    
    @if (old('cbAlterationType'))
        @foreach (old('cbAlterationType') as $tipo)
            tipos.push('{{ $tipo }}')
        @endforeach
    @endif

    var descricoes = [];
    
    @if (old('txtDescriptionSpec'))
        @foreach (old('txtDescriptionSpec') as $desc)
            descricoes.push('{{ html_entity_decode($desc, ENT_COMPAT, 'ISO - 8859 - 1') }}')
        @endforeach
    @endif

    var template = {};
    
    if (tipos && descricoes) {
        var solicitacoes = [];
        for (index = 0; index < tipos.length; ++index) {
            template = {'id': tipos[0], 'descricao': descricoes[0]};
            solicitacoes.push(template);
        }
    } else {
        var solicitacoes = []
    }
    
    var edicao = false;
</script>

<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/ticket-reprovar-controller.js') }}"></script>

<!-- Breadcrumbs TO-DO -->
{!! Breadcrumbs::renderIfExists('artwork.ticket_reprovar', $ticket->id) !!}
<!-- END Breadcrumbs TO-DO -->

<div class="content content-fluid clearfix" ng-app="app" ng-controller="TicketReprovarCtrl" ng-cloak>
    @if (count($errors) > 0)
        <div class="alert alert-danger" role="alert">
            <div class="alert-error">
                @foreach ($errors->all() as $error)
                    {{ $error}}<br />
                @endforeach
            </div>
        </div>
    @endif
    
    @include('site.adsmart.ticket_topo_inc')
    
    <!-- Timeline -->
    <div class="timeline" ng-cloak>
        
        <div ng-class="(ticket.adsmartEtapas[1] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.adsmartEtapas[0] && !ticket.adsmartEtapas[1]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.adsmartEtapas[0] && !ticket.adsmartEtapas[1]) && ticket.cancelado_em}" class="event-icon fa fa-flag"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.adsmartEtapas[0] && !ticket.adsmartEtapas[1]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.adsmartEtapas[0] && !ticket.adsmartEtapas[1]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Aberto</span>
            </div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[2] === 1) ? 'event-box is-done' : 'event-box'" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[2] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.adsmartEtapas[2] && !ticket.adsmartEtapas[3]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.adsmartEtapas[2] && !ticket.adsmartEtapas[3]) && ticket.cancelado_em}" class="event-icon fa fa-upload"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.adsmartEtapas[2] && !ticket.adsmartEtapas[3]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.adsmartEtapas[2] && !ticket.adsmartEtapas[3]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Anexação</span>
            </div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[3] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[3] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <span ng-if="(ticket.adsmartEtapas[3] && !ticket.adsmartEtapas[4]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.adsmartEtapas[3] && !ticket.adsmartEtapas[4]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <div ng-class="{'sub paused-at paused-at-btn clickable fa fa-pause-circle': (ticket.adsmartEtapas[3] && !ticket.adsmartEtapas[4]) && ticket.pausado_em, 'sub canceled-at fa fa-times-circle': (ticket.adsmartEtapas[3] && !ticket.adsmartEtapas[4]) && ticket.cancelado_em}" class="dot"></div>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>

            </div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[4] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[4] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.adsmartEtapas[4] && !ticket.adsmartEtapas[5]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.adsmartEtapas[4] && !ticket.adsmartEtapas[5]) && ticket.cancelado_em}" class="event-icon fa fa-eye"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.adsmartEtapas[4] && !ticket.adsmartEtapas[5]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.adsmartEtapas[4] && !ticket.adsmartEtapas[5]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Revisão</span>
            </div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[5] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[5] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <span ng-if="(ticket.adsmartEtapas[5] && !ticket.adsmartEtapas[6]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.adsmartEtapas[5] && !ticket.adsmartEtapas[6]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <div ng-class="{'sub paused-at paused-at-btn clickable fa fa-pause-circle': (ticket.adsmartEtapas[5] && !ticket.adsmartEtapas[6]) && ticket.pausado_em, 'sub canceled-at fa fa-times-circle': (ticket.adsmartEtapas[5] && !ticket.adsmartEtapas[6]) && ticket.cancelado_em}" class="dot"></div>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[6] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <div ng-class="(ticket.adsmartEtapas[6] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.adsmartEtapas[6] && !ticket.adsmartEtapas[7]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.adsmartEtapas[6] && !ticket.adsmartEtapas[7]) && ticket.cancelado_em}" class="event-icon fa fa-thumbs-up"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.adsmartEtapas[6] && !ticket.adsmartEtapas[7]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.adsmartEtapas[6] && !ticket.adsmartEtapas[7]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Aprovação</span>
            </div>
        </div>

    </div>
    <!-- END Timeline -->
    
    <!-- Form Ticket Reprovado -->
    {!! Form::open(array('name' => 'formTicketReprovado', 'id' => 'formTicketReprovado', 'class' => 'artw-ticket-create', 'ng-submit' => 'validar($event)')) !!}
        <section class="bs border-color-primary margin-top-30 margin-bottom-40 no-padding">
            <div class="block margin-bottom-30">
                <h4 class="master-title-alt txt-bold margin-bottom-20">Reprovação</h4>
                <h5 class="master-title-sub label-control txt-small txt-uppercase margin-left-10 margin-right-10 margin-bottom-10">Instruções de Reprovação</h5>
                <div class="text-block margin-left-10 margin-right-10 no-margin-top margin-bottom-20">
                    <p class="txt-regular no-margin">
                        Descreva os motivos que levaram à reprovação divididos em critérios de avaliação específicos.
                    </p>
                </div>
                <div id="alterationTypeWrap" class="input-fields-wrapper request-type">
                    <div ng-repeat="(k, solicitacao) in solicitacoes" class="input-fields-group flexbox-container margin-bottom-10">
                        <div class="artw-instructions-col flex flex-auto">
                            <div class="flexbox-container">
                                <div class="flex-12-large flex-auto margin-bottom-10 clearfix">
                                    <div ng-class="{'disabled': solicitacao.id !== ''}" class="select-control">	
                                        <select ng-disabled="solicitacao.id !== ''">
                                            <option value="">Selecione o critério de avaliação</option>
                                            <option ng-selected="solicitacao.id == tipo.id" ng-repeat="(k, tipo) in condicoesMidia" ng-value="@{{ tipo.id }}">@{{ tipo.nome }}</option>
                                            <option ng-selected="solicitacao.id" value="99">Outros</option>
                                        </select>
                                        <input ng-value="solicitacao.id" type="hidden" name="cbAlterationType[]">
                                        <input ng-value="solicitacao.nome" type="hidden" name="cbAlterationName[]">
                                    </div>
                                </div>
                                <div class="flex-12-large flex-auto margin-bottom-10 clearfix">
                                    <textarea auto-height ng-readonly="solicitacao.id !== '99'" ng-model="solicitacao.descricao" placeholder="Descreva de forma específica" rows="3" class="input-control textbox-control textbox-auto-resizable">@{{ solicitacao.descricao }}</textarea>
                                    <input ng-value='solicitacao.descricao' type='hidden' name='txtDescriptionSpec[]'>
                                </div>
                            </div>
                        </div>
                        <div class="remove-instruction-col flex flex-auto">
                            <button ng-disabled="solicitacao.obrigatoria" ng-click="remover(k)" class="btn-control btn-alt">Excluir</button>
                        </div>
                    </div>
                    <div class="add-instruction-group input-fields-group flexbox-container margin-bottom-10">
                        <div class="artw-instructions-col flex flex-auto">
                            <div class="flexbox-container">
                                <div class="flex-12-large flex-auto margin-bottom-10 clearfix">
                                    <div class="select-control">	
                                        <select ng-change="addTipo()" ng-model="add_tipo">
                                            <option value="">Selecione o critério de avaliação</option>
                                            <option ng-repeat="(k, tipo) in condicoesMidia" value="@{{ tipo.id }}">@{{ tipo.nome }}</option>
                                            <option value="99">Outros</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="flex-12-large flex-auto margin-bottom-10 clearfix">
                                    <textarea auto-height ng-readonly="add_tipo != 99 && add_tipo != undefined" ng-model="add_descricao" placeholder="O texto do critério de avaliação será exibido aqui." rows="3" class="input-control textbox-control textbox-auto-resizable"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="add-instruction-col flex flex-auto">
                            <button ng-click="adicionar()" type="button" class="btn-control btn-positive">Adicionar</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Button group -->
        <div class="btn-group txt-center">
            <button type="submit" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-right">
                <span><i class="icon fa fa-arrow-right"></i> Concluir Reprovação</span>
            </button>
        </div>
        <!-- END Button group -->
    {!! Form::close() !!}
    <!-- END Form Ticket Reprovado -->
</div>
@stop