@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('artwork.ticket_editar', $ticket->id) !!}

<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />
<script>
    var id_ticket = {{$ticket->id}};
    var id_usuario = {{Auth::user()->id}};
    var csrf_token = '{{ csrf_token() }}';
    var edicao = true;
</script>
<script>
    $(function () {
        if ($("#tfStartDate").val()){
            var tfStartDate = $("#tfStartDate").val().split("/");
            var minDate = new Date(tfStartDate[2], tfStartDate[1] - 1, tfStartDate[0]);
            //minDate.setDate(minDate.getDate() + 1);
            var minDate2 = new Date(tfStartDate[2], tfStartDate[1] - 1, tfStartDate[0]);
            minDate2.setDate(minDate2.getDate() + 1);

            var dateFormat = "dd/mm/yy",
                from = $("#tfArtSendDate")
                .datepicker({
                    //defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1,
                    minDate: minDate,
                    beforeShow:function(textbox, instance){
                        var rect = textbox.getBoundingClientRect();
                        
                        setTimeout(function () {
                            $('.artw-approval-date-col').append($('#ui-datepicker-div'));
                        }, 0);
                    }
                });
            //.on("change", function () {
            //    from.datepicker("option", "maxDate", getDate(this, -1));
            //});
        }

        function getDate(element, qtd) {
            var date;
            try {
                var date2 = $.datepicker.parseDate(dateFormat, element.value);
                date2.setDate(date2.getDate() + qtd);
                date = date2;
            } catch (error) {
                date = null;
            }
            return date;
        }
    });
</script>
<script src="{{ asset('/js/site/jquery-ui.min.js')}}"></script>
<script src="{{ asset('/js/site/ui.datepicker-pt-BR.js')}}"></script>
<script src="{{ asset('/js/angular/angular.min.js')}}"></script>
<script src="{{ asset('/js/angular/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.min.js')}}"></script>
<script src="{{ asset2('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js')}}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/ticket-edicao-controller.js')}}"></script>

<div class="content content-fluid" ng-app="app" ng-controller="TicketEdicaoCtrl">
    @if (count($errors) > 0)
        <div class="alert alert-danger" role="alert">
            <div class="alert-error">
                @foreach ($errors->all() as $error)
                    {{ $error}}<br />
                @endforeach
            </div>
        </div>
    @endif
    @include('site.adsmart.ticket_topo_inc')
    <!-- Timeline -->
    <div class="timeline" ng-cloak>
        <div ng-class="(ticket.adsmartEtapas[1] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.adsmartEtapas[0] && !ticket.adsmartEtapas[1]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.adsmartEtapas[0] && !ticket.adsmartEtapas[1]) && ticket.cancelado_em}" class="event-icon fa fa-flag"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.adsmartEtapas[0] && !ticket.adsmartEtapas[1]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.adsmartEtapas[0] && !ticket.adsmartEtapas[1]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Aberto</span>
            </div>
        </div>
        <div ng-class="(ticket.adsmartEtapas[2] === 1) ? 'event-box is-done' : 'event-box'" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.adsmartEtapas[2] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.adsmartEtapas[2] && !ticket.adsmartEtapas[3]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.adsmartEtapas[2] && !ticket.adsmartEtapas[3]) && ticket.cancelado_em}" class="event-icon fa fa-upload"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.adsmartEtapas[2] && !ticket.adsmartEtapas[3]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.adsmartEtapas[2] && !ticket.adsmartEtapas[3]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Anexação</span>
            </div>
        </div>
        <div ng-class="(ticket.adsmartEtapas[3] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.adsmartEtapas[3] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <span ng-if="(ticket.adsmartEtapas[3] && !ticket.adsmartEtapas[4]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.adsmartEtapas[3] && !ticket.adsmartEtapas[4]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <div ng-class="{'sub paused-at paused-at-btn clickable fa fa-pause-circle': (ticket.adsmartEtapas[3] && !ticket.adsmartEtapas[4]) && ticket.pausado_em, 'sub canceled-at fa fa-times-circle': (ticket.adsmartEtapas[3] && !ticket.adsmartEtapas[4]) && ticket.cancelado_em}" class="dot"></div>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>

            </div>
        </div>
        <div ng-class="(ticket.adsmartEtapas[4] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.adsmartEtapas[4] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.adsmartEtapas[4] && !ticket.adsmartEtapas[5]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.adsmartEtapas[4] && !ticket.adsmartEtapas[5]) && ticket.cancelado_em}" class="event-icon fa fa-eye"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.adsmartEtapas[4] && !ticket.adsmartEtapas[5]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.adsmartEtapas[4] && !ticket.adsmartEtapas[5]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Revisão</span>
            </div>
        </div>
        <div ng-class="(ticket.adsmartEtapas[5] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.adsmartEtapas[5] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <span ng-if="(ticket.adsmartEtapas[5] && !ticket.adsmartEtapas[6]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.adsmartEtapas[5] && !ticket.adsmartEtapas[6]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <div ng-class="{'sub paused-at paused-at-btn clickable fa fa-pause-circle': (ticket.adsmartEtapas[5] && !ticket.adsmartEtapas[6]) && ticket.pausado_em, 'sub canceled-at fa fa-times-circle': (ticket.adsmartEtapas[5] && !ticket.adsmartEtapas[6]) && ticket.cancelado_em}" class="dot"></div>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div ng-class="(ticket.adsmartEtapas[6] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.adsmartEtapas[6] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.adsmartEtapas[6] && !ticket.adsmartEtapas[7]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.adsmartEtapas[6] && !ticket.adsmartEtapas[7]) && ticket.cancelado_em}" class="event-icon fa fa-thumbs-up"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.adsmartEtapas[6] && !ticket.adsmartEtapas[7]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.adsmartEtapas[6] && !ticket.adsmartEtapas[7]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Aprovação</span>
            </div>
        </div>
    </div>
    <!-- END Timeline -->
    <!-- Form Editar Ticket -->
    {!! Form::open(array('name' => 'formEditarTicket', 'id' => 'formEditarTicket', 'class' => 'artw-ticket-create', 'files' => 'true', 'ng-submit' => 'validar($event)')) !!}
        <section class="margin-top-30">
            @if ($ticket->id_status >= 0)
                <div class="block">
                    <h4 class="master-title-alt txt-bold margin-bottom-20">Informações</h4>
                    <div class="flexbox-container">
                        <div class="artw-initial-info-col flex flex-auto">
                            <label class="master-title-sub label-control txt-small txt-uppercase">Resumo</label>
                            <textarea @if ($ticket->id_status > 2 && $ticket->id_status < 9) disabled @endif name="txtMainDescription" placeholder="Descrição geral" rows="5" id="txtMainDescription" class="input-control textbox-control">{{ $ticket->descricao }}</textarea>
                        </div>
                        <div class="artw-start-date-col flex flex-auto">
                            <label class="master-title-sub label-control txt-small txt-uppercase">Início</label>
                            <div class="date-control readonly">	
                                <input value="{{ $ticket->dt_ini->format('d/m/Y') }}" disabled autocomplete="off" type="text" name="tfStartDate" placeholder="dd/mm/aaaa" id="tfStartDate" class="input-control valid-tdate" />
                                <span class="addon fa fa-calendar"></span>
                            </div>
                        </div>
                        <div class="artw-approval-date-col flex flex-auto">
                            <label class="master-title-sub label-control txt-small txt-uppercase">Data de Veiculação</label>
                            <div class="date-control">	
                                <input onkeydown="return false;" autocomplete="off" @if ($ticket->id_status > 2 && $ticket->id_status < 9) disabled @endif value="{{ $ticket->dt_fim->format('d/m/Y') }}" type="text" name="tfArtSendDate" placeholder="dd/mm/aaaa" id="tfArtSendDate" class="date-input-readonly input-control" />
                                <span class="addon fa fa-calendar"></span>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($ticket->id_status <= 14 || $ticket->id_status == 15)
                    <div ng-cloak class="block">
                        <label class="master-title-sub label-control txt-small txt-uppercase margin-left-10 margin-right-10">Critérios de Avaliação</label>
                        <div id="alterationTypeWrap" class="input-fields-wrapper request-type">
                            <div ng-repeat="(k, solicitacao) in solicitacoes" class="input-fields-group flexbox-container margin-bottom-20">
                                <div class="artw-instructions-col flex flex-auto">
                                    <div class="flexbox-container">
                                        <div class="flex-12-large flex-auto margin-bottom-10 clearfix">
                                            <div ng-class="{ 'disabled': solicitacao.id }" class="select-control">
                                                <select ng-disabled="solicitacao.id !== ''">
                                                    <option value="">Selecione o critério de avaliação</option>
                                                    <option ng-selected="solicitacao.id_tipo == tipo.id" ng-repeat="(k, tipo) in condicoesMidia" ng-value="@{{ tipo.id }}">@{{ tipo.nome }}</option>
                                                    <option ng-selected="solicitacao.id_tipo == 99" value="99">Outros</option>
                                                </select>
                                                <input ng-disabled="!solicitacao.id" ng-value='solicitacao.id_tipo' type='hidden'>
                                                <input ng-disabled="solicitacao.id" ng-value='solicitacao.id_tipo' type='hidden' name='cbAlterationType[]'>

                                                <input ng-disabled="!solicitacao.id" ng-value="solicitacao.nome" type="hidden">
                                                <input ng-disabled="solicitacao.id" ng-value="solicitacao.nome" type="hidden" name="cbAlterationName[]">

                                            </div>
                                        </div>
                                        <div class="flex-12-large flex-auto no-margin-bottom clearfix">
                                            <textarea auto-height ng-readonly="solicitacao.id_tipo" ng-model="solicitacao.descricao" placeholder="O texto do critério de avaliação será exibido aqui." rows="4" class="input-control textbox-control textbox-auto-resizable">@{{ solicitacao.descricao_texto }}</textarea>
                                            <input ng-disabled="!solicitacao.id" ng-value='solicitacao.descricao' type='hidden'>
                                            <input ng-disabled="solicitacao.id" ng-value='solicitacao.descricao' type='hidden' name='txtDescriptionSpec[]'>
                                        </div>
                                    </div>
                                </div>
                                <div class="remove-instruction-col flex flex-auto">
                                    <button ng-show="solicitacao.id" ng-click="remover(k)" disabled class="btn-control btn-alt no-margin">Excluir</button>
                                    <button ng-show="!solicitacao.id" ng-click="remover(k)" class="btn-control btn-alt no-margin">Excluir</button>
                                </div>
                            </div>
                            <div class="add-instruction-group input-fields-group flexbox-container margin-bottom-10">
                                <div class="artw-instructions-col flex flex-auto">
                                    <div class="flexbox-container">
                                        <div class="flex-12-large flex-auto margin-bottom-10 clearfix">
                                            <div class="select-control">	
                                                <select ng-change="addTipo()" ng-model="add_tipo">
                                                    <option value="">Selecione o critério de avaliação</option>
                                                    <option ng-if="!tipo.obrigatoria" ng-repeat="(k, tipo) in condicoesMidia" value="@{{ tipo.id }}">@{{ tipo.nome }}</option>
                                                    <option value="99">Outros</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="flex-12-large flex-auto no-margin-bottom clearfix">
                                            <textarea auto-height ng-model='add_descricao' placeholder="O texto do critério de avaliação será exibido aqui." rows="3" class="input-control textbox-control textbox-auto-resizable"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="add-instruction-col flex flex-auto">
                                    <button ng-click="adicionar()" type="button" class="btn-control btn-positive">Adicionar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endif    
        </section>
        <!-- Button group -->
        <div class="btn-group txt-center margin-top-30">
            <div class="bs border-color-primary padding-top-40">
                <a href="{{ route('site.adsmart.ticket', [$ticket->numero]) }}" class="btn-control btn-call-to-action call-to-action-negative btn-icon btn-icon-left">
                    <span><i class="icon fa fa-times txt-light"></i> Cancelar</span> 
                </a>
                <button type="submit" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                    <span><i class="icon fa fa-check txt-light"></i> Salvar</span>
                </button>
            </div>
        </div>
        <!-- END Button group -->
    {!! Form::close() !!}
    <!-- END Form Criar Ticket -->
</div>
@stop
