@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('adsmart.aprovadores_midia') !!}

<script>
    var csrf_token = '{{ csrf_token() }}';
</script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- DataTables -->
<script src="{{ asset('/js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/datatables/accent-neutralise.js') }}"></script>
<script defer type="text/javascript" src="{{ asset2('/js/site/dataTables-resolve-loader.js') }}"></script>

<div class="content content-fluid clearfix">
    <section class="artwork-container manager-container margin-top-10 margin-bottom-40">
        <h3 class="master-title">Gerenciar Aprovadores de Mídia</h3>
        <form name="createMediaForm" action="" method="POST" novalidate>
            <input type="hidden" name="_token" value="{{ csrf_token()}}" />
            <section>
                <div class="dt-content-container box-relative">            
                    <form>
                        <table style="display:none;" cellpading="0" cellspacing="0" id="myTable5" class="table manager-table table-border table-hover">
                            <thead>
                                <tr>
                                    <th width=""><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Aprovadores de Mídia</small></span></th>
                                    <th width="50"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($aprovadoresMidia) > 0)
                                    @foreach($aprovadoresMidia as $key => $aprovador)
                                        <tr>
                                            <td>
                                                <a href="javascript:;" class="txt-no-link flex-wrap">
                                                    <i class="icon fa fa-thumbs-o-up"></i>
                                                    <span class="text-side">{{ $aprovador->nome }}</span>
                                                </a>
                                            </td>
                                            <td>
                                                <div class="dropdown drop-to-left">
                                                    <a href="javascript:;" class="tooltip top dropdown-toggle fa fa-ellipsis-h"><span class="tooltip-box">Opções</span></a>
                                                    <div class="dropdown-list">
                                                        <ul> 
                                                            <li><a href="{{ route("site.adsmart.aprovadores_midia.editar", $aprovador->id) }}" class="txt-bold"><span class="icon fa fa-edit"></span>Alterar</a></li>
                                                            <li><a href="javascript:;" onclick="excluir('Após excluído não poderá ser recuperado. Deseja prosseguir?', '{{ route('site.adsmart.aprovadores_midia.excluir', $aprovador->id) }}')" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </form>
                </div>
            </section>
        </form>
    </section>
    <script>
        $(function() {
            $('.table').on( 'init.dt', function (e, settings) {
                $('.datatable-pagination').hide();
                setTimeout(function () {
                    if(settings.aiDisplay.length > 0) {
                        $('.datatable-pagination').show();
                    }		
                }, new Date().getMilliseconds());
            });
            var dataTableToolbarBtnDOM = '<div class="flex flex-auto">' +
                '<a href="{{ route('site.adsmart.aprovadores_midia.criar') }}" class="btn-function"><i class="icon fa fa-thumbs-o-up"></i> Criar Aprovadores de Mídia</a>' +
            '</div>';
            var table = $('#myTable5').DataTable({
                dom: '<"datatable-toolbar flexbox-container clearfix"' +
                        '<"datatable-filter-container flex flex-3-large"f>' +
                        '<"datatable-filter-container filter-by-status flex flex-3-large">' +
                    '>' +
                    '<"clearfix"' +
                        '<tr>' +
                    '>' +
                    '<"datatable-pagination flexbox-container flex-align-center clearfix"' +
                        '<"flex"<"select-control"l>><"flex flex-auto"p><"flex"i>' +
                    '>',
                bAutoWidth: false,
                processing: false,
                serverSide: false,
                stateSave: true,
                pageLength: 25,
                lengthMenu: [
                    [10, 25, 50, -1], 
                    ['Exibir 10 por página', 'Exibir 25 por página', 'Exibir 50 por página', 'Exibir todos']
                ],
                columnDefs: [
                    { orderable: false, targets: 1 }
                ],
                language: {
                    "url": "/js/datatables/language/Portuguese-Brasil.json"
                },
                initComplete: function () {
                    $('.datatable-filter-container .dataTables_filter input[type="search"]').attr('placeholder', 'Busca');

                    var dataTables_filter_cancel = '<i></i>';
                    var dataTables_filter_search = '<i></i>';
                    
                    $('.datatable-filter-container .dataTables_filter').append(dataTables_filter_search);
                    $('.datatable-filter-container .dataTables_filter').append(dataTables_filter_cancel);
                    $('.datatable-filter-container .dataTables_filter i').addClass('dataTables_filter_search');
                    $('.datatable-filter-container .dataTables_filter i + i').addClass('dataTables_filter_cancel');
                    
                    function dataTablesFilterCancelShowHide() {
                        if ($('.datatable-filter-container .dataTables_filter input[type="search"]').val() !== '') {
                            $('.dataTables_filter_search').hide();
                            $('.dataTables_filter_cancel').show();
                        } else {
                            $('.dataTables_filter_search').show();
                            $('.dataTables_filter_cancel').hide();
                            
                            table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
                        }
                    }
                    
                    $(document).on('click', '.datatable-filter-container .dataTables_filter_cancel', function () {
                        $('#myTable5_filter input[type="search"]').val('');
                        
                        dataTablesFilterCancelShowHide();
                    });

                    $(document).on('keyup', '#myTable5_filter input[type="search"]', function () {
                        dataTablesFilterCancelShowHide();
                    });
                    
                    $('.dataTable').show();
                    $('.dataTable thead').show();
                    $('.dataTables_filter').show();
                    $('.datatable-toolbar').append(dataTableToolbarBtnDOM);
                },
                fnDrawCallback: function( oSettings ) {
                    if(oSettings.aiDisplay.length <= 0) {
                        $('.dataTable thead th').hide();
                        $('.datatable-pagination').hide();
                        $('.dataTables_empty').addClass('fa-thumbs-o-up');
                        $('.dataTables_empty').text('Nenhum aprovador de mídia encontrado.');
                    } else {
                        $('.dataTable thead th').show();
                        $('.datatable-pagination').show();
                        $('.dataTables_empty').removeClass('fa-thumbs-o-up');
                        $('.dataTables_empty').text('');
                    }
                }
            });
        });
    </script>
</div>
@stop