    <!-- Showcase panel -->
	<div class="showcase-panel flexbox-container flex-align-center clearfix">
        <div class="showcase-left">
            <span class="helper"></span>
            @if (isset($familia) && $familia && $familia->logotipos->first())
                <span style="background-color: {{ $familia->cor_hexa }}" class="showcase-brand-bg"><img src="{{ $familia->logotipos->first()->thumb }}" title="" alt="" class="showcase-brand-img" /></span>
            @else
                <span class="showcase-brand-no-img"></span>
            @endif
        </div>
        <div class="showcase-body">
			<div class="showcase-container">
				<div class="flexbox-container flex-no-wrap clearfix">
                    <!-- TO-DO -->
				    <div class="flex-auto showcase-info showcase-item-detail text-inline col-100-max">
						<h3 class="txt-bold">{{ isset($ticket) ? $ticket->adsmartPeca->adsmartCampanhaMidia->adsmartCampanha->nome : $peca->adsmartCampanhaMidia->adsmartCampanha->nome }}</h3>
						<small class="text-small txt-color-alt margin-top-5">campanha</small>
					</div>
                    <div class="flex-auto showcase-info showcase-item-detail text-inline col-15-max">
                        <h3 class="txt-bold">{{ isset($ticket) ? $ticket->adsmartPeca->adsmartCampanhaMidia->adsmartMidiaTipo->nome : $peca->adsmartCampanhaMidia->adsmartMidiaTipo->nome }}</h3>
                        <small class="text-small txt-color-alt margin-top-5">mídia</small>
                    </div>
                    <div class="flex-auto showcase-info showcase-item-detail text-inline col-15-max">
                        <h3 class="txt-light">{{ isset($ticket) ? $ticket->adsmartPeca->nome : $peca->nome }}</h3>
                        <small class="text-small txt-color-alt margin-top-5">peça</small>
                    </div>
                    <!-- END TO-DO -->
                </div>
				<div class="flexbox-container clearfix">
					<!-- TO-DO -->
                    <div class="flex-auto showcase-info showcase-feature text-inline no-margin-bottom">
						<small class="text-small txt-bold txt-color-primary">Ticket: {{ isset($ticket) ? $ticket->numero : 'Novo' }}</small>
                        <small class="text-small txt-bold txt-color-primary">Ciclo: {{ isset($ticket) ? $ticket->ciclo_atual()->numero : 'Novo' }}</small>
                    </div>
                    
                    <div class="flex-auto showcase-info showcase-feature text-inline no-margin-bottom">
						<small class="text-small txt-regular">Marca: <strong class="txt-semibold">{{ isset($ticket) ? $ticket->adsmartPeca->adsmartCampanhaMidia->adsmartCampanha->familias()->lists('nome')->implode(', ') : $peca->adsmartCampanhaMidia->adsmartCampanha->familias()->lists('nome')->implode(', ') }}</strong></small>
					</div>
					<!-- END TO-DO -->
                </div>
			</div>
		</div>
	</div>
	<!-- END Showcase panel -->