@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('adsmart.midias', $id_familia, $id_campanha) !!}

<script>
    var id_campanha = '{{ $id_campanha }}'
    var id_familia = '{{ $id_familia }}'
</script>

<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />

<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset('/js/angular/midias-resolve-loader.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/midias-controller.js') }}"></script>

<div ng-app="app" ng-controller="MidiasController" class="content content-fluid" ng-cloak>
    <!-- Showcase panel -->
    <div class="showcase-panel flexbox-container flex-align-center clearfix auto-height">
        <div class="showcase-body no-margin">
            <div class="showcase-container">
                <div class="flexbox-container clearfix">
                    <div class="flex-auto showcase-icon text-inline no-margin-bottom">
                        <i class="icon fa fa-folder-open"></i>
                    </div>
                    <div class="flex-auto showcase-info showcase-item-detail text-inline grid-1-3 no-margin-bottom">
                        <h3 class="txt-bold">{{ $campanha->nome }}</h3>
                        <small class="text-small txt-color-alt margin-top-5">campanha</small>
                    </div>
                    <div class="flex-auto showcase-info showcase-item-detail text-inline grid-2-3 no-margin-bottom">
                        <h3 class="txt-light">{{ $familia->nome }}</h3>
                        <small class="text-small txt-color-alt margin-top-5">marca</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Showcase panel -->
    <div class="artwork-container margin-bottom-40">
        <!-- Tabs -->
        <ul class="tabs clearfix">
            <li><a href="javascript:;" class="txt-color-default active">Mídias</a></li>
        </ul>
        <!-- END Tabs -->
        <div ng-if="loaded" class="block margin-top-20 margin-bottom-20">
            <div class="flexbox-container">
                @if((in_array('adsmart_criar_midia', Auth::user()->modulos)))
                <div class="artw-create-package-btn flex">
                    <a href="{{ route('site.adsmart.midias.criar', [$id_familia, $id_campanha]) }}" class="btn-function"><i class="icon fa fa-newspaper-o"></i> Nova Mídia</a>
                </div>
                @endif
            </div>
        </div>
        <div resolve-loader></div>
        <div class="tab-content">
            <div ng-show="midias.length && loaded" class="adsmart-media-list-container fadein">
                <table cellpading="0" cellspacing="0" class="artw-package-table table-border table-hover no-margin">
                    <tbody>
                        <tr ng-repeat="midia in midias" class="table-group no-animate">
                            <td colspan="2">
                                <table cellpading="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td width="">
                                                <label class="txt-package-name">
                                                    <input ng-show="midia.exibir_formulario" type="checkbox" ng-model="midia.stateJS" ng-click="exibeNovaPeca(midia, false)" class="ng-toggle" />
                                                    <input ng-show="!midia.exibir_formulario" type="checkbox" ng-model="midia.stateJS" ng-click="exibeNovaPeca(midia, true)" class="ng-toggle" />
                                                    <span class="txt-color-default">
                                                        <i ng-class="{'icon fa fa-folder-open txt-color-alt': midia.stateJS, 'icon fa fa-folder txt-color-alt': !midia.stateJS }" class="icon fa fa-folder txt-color-alt"></i>
                                                        <div class="text-side txt-regular">
                                                            <span class="text-side-col txt-regular margin-right-20">@{{midia.nome}}</span>
                                                        </div>
                                                    </span>
                                                </label>
                                            </td>
                                            <td width="50">
                                                @if((in_array('adsmart_criar_midia', Auth::user()->modulos)))
                                                <div class="dropdown drop-to-left">
                                                    <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                                    <div class="dropdown-list dropdown-list-auto">
                                                        <ul> 
                                                            <li><a href="javascript:;" ng-click="midia.stateJS = true; exibeNovaPeca(midia, true)" class="txt-bold"><span class="icon fa fa-file"></span>Adicionar Peça</a></li>
                                                            <!-- TO-DO - Alterar Mídia -->
                                                            {{--<li><a href="javascript:;" class="txt-bold"><span class="icon fa fa-edit"></span>Alterar Mídia</a></li>--}}
                                                            <!-- TO-DO - Excluir Mídia -->
                                                            @if((in_array('adsmart_excluir_midia', Auth::user()->modulos)))
                                                            <li><a ng-click="excluir_midia(midia)" ng-class="{'disabled': midia.pecas.length > 0}" href="javascript:;" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir Mídia</a></li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                                @endif
                                            </td>
                                        </tr>
                                        <!-- ngRepeat: item in midia.itens -->
                                        <tr ng-repeat="peca in midia.pecas" ng-if="midia.stateJS" class="table-sub">
                                            <td colspan="2">
                                                <div class="table-sub-content slide-toggle-js">
                                                    <table cellpading="0" cellspacing="0" class="no-margin-bottom">
                                                        <tbody>
                                                            <!-- Table sub-group -->
                                                            <tr>
                                                                <td width="">
                                                                    <div class="txt-package-item-name no-animate" ng-show="!peca.exibir_editar_peca">
                                                                        {{--
                                                                        <a ng-href="@{{peca.arte}}" target="_blank" class="txt-item-name">
                                                                            <input type="checkbox" ng-model="peca_arquivos.stateJS" class="ng-toggle" />
                                                                            <span class="text-side no-margin txt-regular">
                                                                                <i ng-class="{'icon fa fa-folder': peca_arquivos.length > 1, 'icon fa fa-file': peca_arquivos.length < 2}"></i>
                                                                                <strong class="flex-inline txt-regular txt-color-default margin-left-10">@{{ peca.nome }}</strong>
                                                                            </span>
                                                                        </a>
                                                                        --}}
                                                                        <label class="txt-item-name flex-fluid clickable">
                                                                            <input type="checkbox" ng-model="peca.stateJS" class="ng-toggle" />
                                                                            <span class="text-side no-margin txt-regular">
                                                                                <i ng-class="{'icon fa fa-folder': peca.artes.length > 1, 'icon fa fa-folder-open': peca.artes.length > 1 && peca.stateJS, 'icon fa fa-file': peca.artes.length < 2}"></i>
                                                                                <strong class="flex-inline txt-regular txt-color-default margin-left-10">@{{ peca.nome }}</strong>
                                                                            </span>
                                                                        </label>
                                                                    </div>
                                                                    <div ng-show="peca.exibir_editar_peca" class="flexbox-container flexbox-group flex-align-center artw-add-item-sub no-animate">
                                                                        <i class="icon fa fa-file"></i>
                                                                        <div class="flex col-selecione-item flex-5-large margin-left-5 animate">
                                                                            <input type="text" placeholder="Nome da peça" ng-init="peca.nome_original = peca.nome" ng-model="peca.nome_original" class="input-control input-required" />
                                                                        </div>
                                                                        <div class="flexbox-container flexbox-group flex col-btn-group animate">
                                                                            <div class="flex col-member-edit-cancel">
                                                                                <button type="button" ng-click="peca.exibir_editar_peca = false; peca.nome_original = peca.nome" class="btn-control btn-alt">Cancelar</button>
                                                                            </div>
                                                                            <div class="flex col-member-edit-save">
                                                                                <button type="button" ng-click="alterar_peca(peca)" class="btn-control btn-positive">Salvar</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td align="center" width="100">
                                                                    <div tooltips tooltip-side="top" tooltip-size="small" tooltip-smart="true" tooltip-template="Data da última aprovação" ng-show="peca.ultima_aprovacao">
                                                                        @{{peca.ultima_aprovacao}}
                                                                    </div>
                                                                </td>
                                                                <td width="50">
                                                                    <!-- TO-DO -->
                                                                    <div ng-show="peca.numero_ticket_aberto && peca.ticket_status != 9" tooltips tooltip-side="top" tooltip-size="small" tooltip-smart="true" tooltip-template="Ticket em Andamento: @{{ peca.numero_ticket_aberto }}">
                                                                        <a ng-if="peca.link_ticket_aberto" target="_blank" ng-href="@{{ peca.link_ticket_aberto }}" class="btn-function active"><i class="icon fa fa-external-link"></i></a>
                                                                        <a ng-if="!peca.link_ticket_aberto" ng-href="@{{ peca.link_ticket_aberto }}" ng-click="$event.preventDefault()" class="btn-function btn-in-progress"><i class="icon fa fa-ticket"></i></a>
                                                                    </div>

                                                                    <div ng-show="peca.numero_ticket_aberto && peca.ticket_status == 9" tooltips tooltip-side="top" tooltip-size="small" tooltip-smart="true" tooltip-template="Ticket em Digitação: @{{ peca.numero_ticket_aberto }}">
                                                                        <a ng-if="peca.link_ticket_aberto" target="_blank" ng-href="@{{ peca.link_ticket_aberto }}" class="btn-function active"><i class="icon fa fa-external-link"></i></a>
                                                                        <a ng-if="!peca.link_ticket_aberto" ng-href="@{{ peca.link_ticket_aberto }}" ng-click="$event.preventDefault()" class="btn-function btn-in-progress"><i class="icon fa fa-ticket"></i></a>
                                                                    </div>
                                                                    {{--<div tooltips tooltip-side="top" tooltip-size="small" tooltip-smart="true" tooltip-template="Abrir Ticket">    
                                                                        <a ng-href="@{{ peca.link_abrir }}" class="btn-function"><i class="icon fa fa-ticket"></i></a>
                                                                    </div>--}}
                                                                    <!-- END TO-DO -->
                                                                </td>
                                                                <td width="50">
                                                                    <div ng-show="(peca.link_historico && peca.permite_historico) || peca.permite_criar_midia" class="dropdown drop-to-left">
                                                                        <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                                                        <div class="dropdown-list dropdown-list-auto">
                                                                            <ul>
                                                                                @if((in_array('adsmart_historico', Auth::user()->modulos)))<li ng-show="peca.link_historico"><a ng-href="@{{ peca.link_historico }}" class="txt-bold"><span class="icon fa fa-history"></span>Histórico</a></li>@endif
                                                                                {{--<li><a href="javascript:;" class="txt-bold"><span class="icon fa fa-calendar"></span>Alterar Data de Lançamento</a></li>--}}
                                                                                @if((in_array('adsmart_criar_midia', Auth::user()->modulos)))<li><a ng-click="exibeEditarPeca(midia, peca)" href="javascript:;" class="txt-bold"><span class="icon fa fa-pencil-square-o"></span>Alterar Peça</a></li>@endif
                                                                                <!-- TO-DO -->
                                                                                {{--<li><a href="javascript:;" class="txt-bold"><span class="icon fa fa-download"></span>Baixar Peça</a></li>--}}
                                                                                <!-- TO-DO -->
                                                                                @if((in_array('adsmart_excluir_midia', Auth::user()->modulos)))<li><a ng-class="{'disabled': peca.numero_ticket_aberto}" ng-click="excluir_peca(peca)" href="javascript:;" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir Peça</a></li>@endif
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <!-- END Table sub-group -->
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div ng-if="peca.artes.length > 1 && peca.stateJS" class="table-low-sub-container">
                                                    <!-- Loop Arquivos -->
                                                    <div ng-repeat="arte in peca.artes" class="table-sub-content table-low-sub-content slide-toggle-js">
                                                        <table cellpading="0" cellspacing="0" class="no-margin-bottom">
                                                            <tbody>
                                                                <!-- Table sub-group -->
                                                                <tr>
                                                                    <td>
                                                                        <div class="txt-package-item-name no-animate">
                                                                            <a ng-href="@{{arte.linkBaixar}}" target="_blank" class="txt-item-name flex-fluid">
                                                                                <span class="text-side no-margin txt-regular"><i class="icon fa fa-file"></i>
                                                                                    <strong class="flex-inline txt-regular txt-color-default margin-left-10">@{{arte.numero}}</strong>
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                    </td>
                                                                    <td width="50">
                                                                        <div class="dropdown drop-to-left">
                                                                            <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                                                            <div class="dropdown-list dropdown-list-auto">
                                                                                <ul>
                                                                                    <li><a ng-href="@{{arte.linkBaixar}}" class="txt-bold"><span class="icon fa fa-download"></span>Baixar Arquivo</a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <!-- END table sub-group -->
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <!-- END Loop Arquivos -->
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- END ngRepeat: item in embalagem.itens -->
                                        <!-- ngIf: !midia.itens.length && !embalagem.exibir_formulario && embalagem.stateJS  -->
                                        <tr ng-if="!midia.pecas.length && !midia.exibir_formulario && midia.stateJS" class="table-sub no-animate">
                                            <td colspan="2">
                                                <div class="table-sub-content no-item slide-toggle-js">
                                                    <table cellpading="0" cellspacing="0" class="no-margin-bottom">
                                                        <tbody>
                                                            <tr>
                                                                <td width="">
                                                                    <div class="txt-regular">
                                                                        <span class="txt-regular txt-color-alt">Essa mídia não possui nenhuma peça cadastrada</span>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- END ngIf: !midia.itens.length && !midia.exibir_formulario && midia.stateJS -->
                                        <!-- Incluir item -->
                                        <tr ng-show="midia.exibir_formulario" class="table-sub no-animate">
                                            <td colspan="2">
                                                <div class="table-sub-content slide-toggle-js">
                                                    <table cellpading="0" cellspacing="0" class="no-margin-bottom">
                                                        <tbody>
                                                            <!-- Table sub-group -->
                                                            <tr>
                                                                <td colspan="4">
                                                                    <div class="flexbox-container flexbox-group flex-align-center artw-add-item-sub animate">
                                                                        <i class="icon fa fa-file"></i>
                                                                        <div class="flex flex-4-large col-selecione-item margin-left-5">
                                                                            <input type="text" placeholder="Nome da peça" ng-model="midia.nome_nova_peca" class="input-control input-required" />
                                                                        </div>
                                                                        <div class="flexbox-container flexbox-group flex col-btn-group">
                                                                            <div class="flex col-member-edit-cancel">
                                                                                <button type="button" ng-click="midia.exibir_formulario = false; midia.nome_nova_peca = ''" class="btn-control btn-alt">Cancelar</button>
                                                                            </div>
                                                                            <div class="flex col-member-edit-save">
                                                                                <button type="button" ng-click="criar_peca(midia)" class="btn-control btn-positive">Salvar</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <!-- END Table sub-group -->
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- END Incluir item -->
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <!-- END ngRepeat: midia in midias -->
                    </tbody>
                </table>
            </div>
            <div ng-if="!midias.length && loaded" class="artw-no-item fas fa-newspaper-o">
                Nenhuma mídia
            </div>
        </div>
    </div>
</div>
@stop
