@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

<!-- TO-DO - Breadcumbs -->
<div class="breadcumbs-out clearfix">		
    <div class="breadcrumbs-inner content-fluid clearfix">	
        <ul>
            <li><a href="{{ route('site.adsmart.familias') }}">AdSmart</a></li>
        </ul>
    </div>
</div>
<!-- END TO-DO - Breadcumbs -->

<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('/css/angular/v-accordion.min.css')}}" />

<script>
    var csrf_token = '{{ csrf_token() }}';
</script>

<script src="{{ asset('/js/site/snap.svg-min.js') }}"></script>
<script src="{{ asset('/js/site/artwork/ticket-edit.js') }}"></script>
<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-route.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset('/js/angular/v-accordion.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload-shim.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload.min.js') }}"></script>

<script src="{{ asset2('/js/angular/controllers/adsmart/ticket-aba-visao-geral-controller.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/ticket-aba-anexos-controller.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/ticket-aba-tarefas-controller.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/ticket-aba-mensagens-controller.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/ticket-aba-opcoes-controller.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/ticket-controller.js') }}"></script>

<div class="content content-fluid" ng-app="app" ng-controller="TicketCtrl" ng-cloak>
    
    @include('site.adsmart.ticket_topo_inc')
    
    <!-- Timeline -->
    <div class="timeline" ng-cloak>
        
        <div ng-class="(ticket.etapas[0] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.etapas[0] && !ticket.etapas[1]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.etapas[0] && !ticket.etapas[1]) && ticket.cancelado_em}" class="event-icon fa fa-flag"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.etapas[0] && !ticket.etapas[1]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.etapas[0] && !ticket.etapas[1]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Aberto</span>
            </div>
        </div>
        
        <div ng-class="(ticket.etapas[2] === 1) ? 'event-box is-done' : 'event-box'" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <div ng-class="(ticket.etapas[2] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.etapas[2] && !ticket.etapas[3]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.etapas[2] && !ticket.etapas[3]) && ticket.cancelado_em}" class="event-icon fa fa-upload"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.etapas[2] && !ticket.etapas[3]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.etapas[2] && !ticket.etapas[3]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Anexação</span>
            </div>
        </div>
        
        <div ng-class="(ticket.etapas[3] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <div ng-class="(ticket.etapas[3] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <span ng-if="(ticket.etapas[3] && !ticket.etapas[4]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.etapas[3] && !ticket.etapas[4]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <div ng-class="{'sub paused-at paused-at-btn clickable fa fa-pause-circle': (ticket.etapas[3] && !ticket.etapas[4]) && ticket.pausado_em, 'sub canceled-at fa fa-times-circle': (ticket.etapas[3] && !ticket.etapas[4]) && ticket.cancelado_em}" class="dot"></div>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>

            </div>
        </div>
        
        <div ng-class="(ticket.etapas[4] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <div ng-class="(ticket.etapas[4] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.etapas[4] && !ticket.etapas[5]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.etapas[4] && !ticket.etapas[5]) && ticket.cancelado_em}" class="event-icon fa fa-eye"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.etapas[4] && !ticket.etapas[5]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.etapas[4] && !ticket.etapas[5]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Revisão</span>
            </div>
        </div>
        
        <div ng-class="(ticket.etapas[5] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <div ng-class="(ticket.etapas[5] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <span ng-if="(ticket.etapas[5] && !ticket.etapas[6]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.etapas[5] && !ticket.etapas[6]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <div ng-class="{'sub paused-at paused-at-btn clickable fa fa-pause-circle': (ticket.etapas[5] && !ticket.etapas[6]) && ticket.pausado_em, 'sub canceled-at fa fa-times-circle': (ticket.etapas[5] && !ticket.etapas[6]) && ticket.cancelado_em}" class="dot"></div>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div ng-class="(ticket.etapas[6] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <div ng-class="(ticket.etapas[6] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.etapas[6] && !ticket.etapas[7]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.etapas[6] && !ticket.etapas[7]) && ticket.cancelado_em}" class="event-icon fa fa-thumbs-up"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.etapas[6] && !ticket.etapas[7]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.etapas[6] && !ticket.etapas[7]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Aprovação</span>
            </div>
        </div>

    </div>
    <!-- END Timeline -->
    <!-- Content -->
    <div class="workflow-container">
        <form name="formTicketEdicao" action="" method="POST" class="form-workflow-ctrl">
            <!-- Tabs -->
            <ul class="tabs clearfix" ng-cloak>
                <li><a ng-click="$root.change_tab($event)" ng-class="{'active' : activetab == '/visao_geral'}" ng-href="#/visao_geral">Visão Geral</a></li>
                <li><a ng-click="$root.change_tab($event)" ng-class="{'active' : activetab == '/anexos'}" ng-href="#/anexos">Anexos</a></li>
                <li><a ng-click="$root.change_tab($event)" ng-class="{'active' : activetab == '/tarefas'}" ng-href="#/tarefas">Tarefas</a></li>
                <li><a ng-click="$root.change_tab($event)" ng-class="{'active' : activetab == '/mensagens'}" ng-href="#/mensagens">Mensagens</a></li>
                <li><a ng-click="$root.change_tab($event)" ng-class="{'active' : activetab == '/opcoes', 'disabled': ticket.cancelado_em || ticket.id_status == 6}" ng-href="#/opcoes">Opções</a></li>
            </ul>
            <!-- END Tabs -->
            
            <!-- ng-view -->
            <div ng-view></div>
            <!-- END ng-view -->
            
            <!-- Button group -->
            {{--<div ng-show="!loading && ticket.libera_ticket && (activetab == '/visao_geral')" class="btn-group txt-center" ng-cloak>
                <div ng-show="ticket.libera_status && ticket.ciclo_atual.layout && ticket.id_status === 1 && ticket.qtde_revisores" class="bs border-color-primary padding-top-40 no-padding-bottom">
                    <button mw-confirm-click="acao_ticket('2');" mw-confirm-click-message="Deseja submeter à revisão?" ng-show="ticket.libera_status && ticket.ciclo_atual.layout && ticket.id_status === 1 && ticket.qtde_revisores" type="button" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                        <span><i class="icon fa fa-check"></i>Submeter à Revisão</span>
                    </button>
                </div>
                <div ng-show="ticket.libera_status && ticket.ciclo_atual.layout && ticket.id_status === 1 && !ticket.qtde_revisores" class="bs border-color-primary padding-top-40 no-padding-bottom">
                    <button onclick="myAlert('Não há revisores adicionados a esse ticket.<br />Para proseguir, adicione pelo menos um revisor.', 'warning')" ng-show="ticket.libera_status && ticket.ciclo_atual.layout && ticket.id_status === 1 && !ticket.qtde_revisores" type="button" class="btn-control btn-call-to-action call-to-action-default btn-icon btn-icon-left">
                        <span><i class="icon fa fa-check"></i>Submeter à Revisão</span>
                    </button>
                </div>
                <div ng-show="(!ticket.em_reprovacao || (ticket.em_reprovacao && ticket.em_reprovacao_by == ticket.id_usuario_logado)) && (ticket.id_status === 3 && ticket.libera_status || ticket.id_status === 3 && ticket.libera_status && ticket.qtde_aprovadores)" class="bs border-color-primary padding-top-40 no-padding-bottom">
                    <button onClick="location='{{Route('site.artwork.ticket.edit.deny.get', [$ticket->id])}}'" type="button" ng-show="ticket.id_status === 3 && ticket.libera_status" class="btn-control btn-call-to-action call-to-action-negative btn-icon btn-icon-left">
                        <span><i class="icon fa fa-times"></i>Reprovar</span>
                    </button>
     
                    <button mw-confirm-click="acao_ticket('4');" mw-confirm-click-message="Deseja aprovar?" type="button" ng-show="ticket.id_status === 3 && ticket.libera_status && ticket.qtde_aprovadores && !ticket.em_reprovacao" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                        <span><i class="icon fa fa-check"></i>Aprovar</span>
                    </button>
                    <button onclick="myAlert('Para prosseguir para os ciclos de aprovação, adicione pelo menos um participante como aprovador nesse ticket.', 'warning')" type="button" ng-show="ticket.id_status === 3 && ticket.libera_status && !ticket.qtde_aprovadores && !ticket.em_reprovacao" class="btn-control btn-call-to-action call-to-action-default btn-icon btn-icon-left">
                        <span><i class="icon fa fa-check"></i>Aprovar</span>
                    </button>
                </div>
            
                <div class="flexbox-container flex-align-center" ng-show="ticket.id_status === 3 && ticket.em_reprovacao && ticket.em_reprovacao_by != ticket.id_usuario_logado">
                    <div class="flex flex-fluid">
                        <div class="panel alternative">
                            <i class="icon fa fa-hourglass-half"></i>
                            <div class="panel-body flex-inline flex-align-center txt-left no-padding-right">
                                <h4 class="panel-heading txt-semibold">@{{ticket.em_reprovacao_nome}}, que é membro da sua equipe, já iniciou a reprovação. Quem inicia responde por toda a equipe.</h4>
                            </div>
                        </div>
                    </div>
                </div>
            
                <div ng-show="(!ticket.em_reprovacao || (ticket.em_reprovacao && ticket.em_reprovacao_by == ticket.id_usuario_logado)) && (ticket.id_status === 3 && ticket.libera_status && !ticket.qtde_aprovadores || ticket.id_status === 4 && ticket.libera_status)" ng-class="{'no-border no-padding': ticket.id_status === 3 && ticket.libera_status && !ticket.qtde_aprovadores}" class="bs border-color-primary padding-top-40 no-padding-bottom">
                    <button onClick="location='{{Route('site.artwork.ticket.edit.deny.get', [$ticket->id])}}'" type="button" ng-show="ticket.id_status === 4 && ticket.libera_status" class="btn-control btn-call-to-action call-to-action-negative btn-icon btn-icon-left">
                        <span><i class="icon fa fa-times"></i>Reprovar</span>
                    </button>
                    <button mw-confirm-click="acao_ticket('5');" mw-confirm-click-message="Deseja aprovar?" type="button" ng-show="ticket.id_status === 4 && ticket.libera_status && !ticket.em_reprovacao" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                        <span><i class="icon fa fa-check"></i>Aprovar</span>
                    </button>
                </div>
                
                <div class="flexbox-container flex-align-center" ng-show="ticket.id_status === 4 && ticket.em_reprovacao && ticket.em_reprovacao_by != ticket.id_usuario_logado">
                    <div class="flex flex-fluid">    
                        <div class="panel alternative">    
                            <i class="icon fa fa-hourglass-half"></i>    
                            <div class="panel-body flex-inline flex-align-center txt-left no-padding-right">
                                <h4 class="panel-heading txt-semibold">@{{ticket.em_reprovacao_nome}}, que é membro da sua equipe, já iniciou a reprovação. Quem inicia responde por toda a equipe.</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--}}
            <!-- END Button group -->
        </form>
    </div>
    <!-- END Content -->
</div>
@stop