@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('adsmart.historico', $ticket->id_familia, $ticket->adsmartPeca->adsmartCampanhaMidia->id_campanha) !!}

<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/v-accordion.min.css') }}" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<script>
	var id_ticket = {{ $ticket->id }};
	var csrf_token = '{{ csrf_token() }}';
</script>

<script src="{{ asset('/js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/datatables/accent-neutralise.js') }}"></script>
<script src="{{ asset('/js/site/snap.svg-min.js') }}"></script>
<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-route.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset('/js/angular/v-accordion.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload-shim.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload.min.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/ticket-aba-visao-geral-controller.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/ticket-aba-anexos-controller.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/ticket-aba-tarefas-controller.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/ticket-aba-mensagens-controller.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/historico-controller.js') }}"></script>

<div class="content content-fluid" ng-app="app" ng-controller="HistoricoCtrl" ng-cloak>
	<h3 class="master-title margin-top-10 margin-bottom-15">Histórico</h3>
	
    <div class="artwork-container margin-top-30 margin-bottom-40">
		
        @include('site.adsmart.ticket_topo_inc')
        
		<!-- Timeline -->
        <div class="timeline" ng-cloak>
        
            <div ng-class="(ticket.adsmartEtapas[1] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
                <div class="event">
                    <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.adsmartEtapas[0] && !ticket.adsmartEtapas[1]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.adsmartEtapas[0] && !ticket.adsmartEtapas[1]) && ticket.cancelado_em}" class="event-icon fa fa-flag"></i>
                    <div class="event-dropdown-list to-right">
                        <ul>
                            <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                        </ul>
                    </div>
                    <span ng-if="(ticket.adsmartEtapas[0] && !ticket.adsmartEtapas[1]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                    <span ng-if="(ticket.adsmartEtapas[0] && !ticket.adsmartEtapas[1]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                    <span class="event-txt">Aberto</span>
                </div>
            </div>
            
            <div ng-class="(ticket.adsmartEtapas[2] === 1) ? 'event-box is-done' : 'event-box'" class="event-box">
                <div class="filling-bar"></div>
            </div>
            
            <div ng-class="(ticket.adsmartEtapas[2] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
                <div class="event">
                    <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.adsmartEtapas[2] && !ticket.adsmartEtapas[3]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.adsmartEtapas[2] && !ticket.adsmartEtapas[3]) && ticket.cancelado_em}" class="event-icon fa fa-upload"></i>
                    <div class="event-dropdown-list to-right">
                        <ul>
                            <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                        </ul>
                    </div>
                    <span ng-if="(ticket.adsmartEtapas[2] && !ticket.adsmartEtapas[3]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                    <span ng-if="(ticket.adsmartEtapas[2] && !ticket.adsmartEtapas[3]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                    <span class="event-txt">Anexação</span>
                </div>
            </div>
            
            <div ng-class="(ticket.adsmartEtapas[3] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
                <div class="filling-bar"></div>
            </div>
            
            <div ng-class="(ticket.adsmartEtapas[3] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
                <div class="event"> 
                    <span ng-if="(ticket.adsmartEtapas[3] && !ticket.adsmartEtapas[4]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                    <span ng-if="(ticket.adsmartEtapas[3] && !ticket.adsmartEtapas[4]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                    <div ng-class="{'sub paused-at paused-at-btn clickable fa fa-pause-circle': (ticket.adsmartEtapas[3] && !ticket.adsmartEtapas[4]) && ticket.pausado_em, 'sub canceled-at fa fa-times-circle': (ticket.adsmartEtapas[3] && !ticket.adsmartEtapas[4]) && ticket.cancelado_em}" class="dot"></div>
                    <div class="event-dropdown-list to-right">
                        <ul>
                            <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                        </ul>
                    </div>
    
                </div>
            </div>
            
            <div ng-class="(ticket.adsmartEtapas[4] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
                <div class="filling-bar"></div>
            </div>
            
            <div ng-class="(ticket.adsmartEtapas[4] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
                <div class="event">
                    <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.adsmartEtapas[4] && !ticket.adsmartEtapas[5]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.adsmartEtapas[4] && !ticket.adsmartEtapas[5]) && ticket.cancelado_em}" class="event-icon fa fa-eye"></i>
                    <div class="event-dropdown-list to-right">
                        <ul>
                            <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                        </ul>
                    </div>
                    <span ng-if="(ticket.adsmartEtapas[4] && !ticket.adsmartEtapas[5]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                    <span ng-if="(ticket.adsmartEtapas[4] && !ticket.adsmartEtapas[5]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                    <span class="event-txt">Revisão</span>
                </div>
            </div>
            
            <div ng-class="(ticket.adsmartEtapas[5] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
                <div class="filling-bar"></div>
            </div>
            
            <div ng-class="(ticket.adsmartEtapas[5] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
                <div class="event"> 
                    <span ng-if="(ticket.adsmartEtapas[5] && !ticket.adsmartEtapas[6]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                    <span ng-if="(ticket.adsmartEtapas[5] && !ticket.adsmartEtapas[6]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                    <div ng-class="{'sub paused-at paused-at-btn clickable fa fa-pause-circle': (ticket.adsmartEtapas[5] && !ticket.adsmartEtapas[6]) && ticket.pausado_em, 'sub canceled-at fa fa-times-circle': (ticket.adsmartEtapas[5] && !ticket.adsmartEtapas[6]) && ticket.cancelado_em}" class="dot"></div>
                    <div class="event-dropdown-list to-right">
                        <ul>
                            <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div ng-class="(ticket.adsmartEtapas[6] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
                <div class="filling-bar"></div>
            </div>
            
            <div ng-class="(ticket.adsmartEtapas[6] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
                <div class="event">
                    <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.adsmartEtapas[6] && !ticket.adsmartEtapas[7]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.adsmartEtapas[6] && !ticket.adsmartEtapas[7]) && ticket.cancelado_em}" class="event-icon fa fa-thumbs-up"></i>
                    <div class="event-dropdown-list to-right">
                        <ul>
                            <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                        </ul>
                    </div>
                    <span ng-if="(ticket.adsmartEtapas[6] && !ticket.adsmartEtapas[7]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                    <span ng-if="(ticket.adsmartEtapas[6] && !ticket.adsmartEtapas[7]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                    <span class="event-txt">Aprovação</span>
                </div>
            </div>
    
        </div>
        <!-- END Timeline -->
		<form name="formTicketHistory" action="" method="POST">
			<ul class="tabs clearfix" ng-cloak>
				<li><a ng-class="{'active' : activetab == '/visao-geral'}" ng-href="#/visao-geral">Visão Geral</a></li>
				<li><a ng-class="{'active' : activetab == '/anexos', 'disabled' : ticket.versao.numero === 0}" ng-href="#/anexos">Anexos</a></li>
				<li><a ng-class="{'active' : activetab == '/tarefas', 'disabled' : ticket.versao.numero === 0}" ng-href="#/tarefas">Tarefas</a></li>
				<li><a ng-class="{'active' : activetab == '/mensagens', 'disabled' : ticket.versao.numero === 0}" ng-href="#/mensagens">Mensagens</a></li>
            </ul>
			<div ng-view></div>
        </form>
	</div>
</div>
@stop
