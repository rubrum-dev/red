@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('adsmart.tipos_midia_criar') !!}

<script>
    var csrf_token = '{{ csrf_token() }}';
</script>

<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />

<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/tipo-midia-criar-controller.js') }}"></script>

<div ng-app="app" ng-controller="TipoMidiaCriarController" ng-cloak class="content content-fluid clearfix">
    <section class="artw-package-edit-container margin-top-10 margin-bottom-40">
        <form name="packageCreateForm" action="" method="POST" novalidate>
            <input type="hidden" name="_token" value="{{ csrf_token()}}" />
            <h3 class="master-title txt-bold margin-bottom-20">Novo Tipo de Mídia</h3>
            <div class="block">
                <div class="flexbox-container flexbox-group margin-top-20 margin-bottom-15 margin-left-10 margin-right-10">
                    <h4 class="txt-lighter txt-small txt-uppercase">Tipo de Mídia *</h4>
                </div>
                <div class="bs no-border no-padding margin-bottom-30">
                    <div class="flexbox-container">
                        <div class="flex flex-fluid">
                            <input type="text" placeholder="Nome do tipo de mídia" ng-model="tipoMidiaNome" name="tipoMidiaNome" id="tipoMidiaNome" class="input-control only-alpha-numeric" />
                        </div>
                    </div>
                </div>
                <!-- TO-DO Opções -->
                <div class="bs no-border no-padding margin-bottom-30">
                    <h4 class="subtitle txt-small txt-uppercase txt-lighter margin-bottom-15">Opções *</h4>
                    <div class="flexbox-container">
                        <div class="flex-fluid">
                            <ul class="list-group-indent">
                                <li class="list-item-horizontal">
                                    <label class="indent clickable no-underline">
                                        <div class="pull">
                                            <i class="icon fa fa-newspaper-o"></i>
                                            <span class="text txt-bold">Tipo de Mídia Ativo <small class="txt-small txt-regular margin-left-20">Exibe nas listas de tipos de mídia.</small></span>
                                            <span class="switch-control pull-right no-margin">
                                                <input ng-model="chkTipoMidiaAtivo" type="checkbox" name="chkTipoMidiaAtivo" id="chkTipoMidiaAtivo" />
                                                <span class="switch-toggle abs-right-offset no-margin"></span>
                                            </span>
                                        </div>
                                    </label>
                                </li>
                                <li class="list-item-horizontal">
                                    <label class="indent clickable no-underline">
                                        <div class="pull">
                                            <i class="icon fa fa-hand-stop-o"></i>
                                            <span class="text txt-bold">Bloquear a Auto-aprovação <small class="txt-small txt-regular margin-left-20">Quando ativado não permite o uso da auto-aprovação na abertura de tickets.</small></span>
                                            <span class="switch-control pull-right no-margin">
                                                <input ng-model="chkBloquarAutoAprovacao" type="checkbox" name="chkBloquarAutoAprovacao" id="chkBloquarAutoAprovacao" />
                                                <span class="switch-toggle abs-right-offset no-margin"></span>
                                            </span>
                                        </div>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- END TO-DO Condições -->
                <div class="bs border-color-primary padding-bottom-30">
                    <div class="block margin-bottom-15 margin-left-10 margin-right-10">
                        <h4 class="txt-lighter txt-small txt-uppercase margin-bottom-15">Aprovadores de Mídia *</h4>
                        <p class="txt-regular">
                            Adicione os Aprovadores de Mídia que ficarão disponíveis na abertura do ticket.
                        </p>
                    </div>
                    <div class="flexbox-container flex-align-center">
                        <div class="flex flex-fluid">
                            <selectize placeholder="Selecione os aprovadores de mídia" ng-model="cbAprovadoresMidia" config="aprovadoresMidiaConfig" options="aprovadores_midia" name="cbAprovadoresMidia" id="cbAprovadoresMidia"></selectize>            
                        </div>

                        <div class="flex">
                            <button type="button" ng-click="add_aprovador_midia_selecionado()" class="btn-control btn-positive">Adicionar</button>
                        </div>
                    </div>
                    <div class="flexbox-container flex-align-center margin-top-20">
                        <table class="artw-shared-item-table table-border table-hover no-animate">
                            <tbody>
                                <tr ng-repeat="aprovador_midia in aprovadores_midia_selecionados track by $index">
                                    <td width="">
                                        <div class="d-flex flex-align-center">
                                            <i class="icon fa fa-thumbs-o-up"></i>
                                            <span class="text-side margin-left-10">@{{ aprovador_midia.nome }}</span>
                                        </div>
                                    </td>
                                    <td width="50">
                                        <div class="dropdown drop-to-left">
                                            <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                            <div class="dropdown-list">
                                                <ul> 
                                                    <li><a href="javascript:;" ng-click="excluir_aprovador_midia_selecionado($index)" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>    
                </div>
                <div class="btn-group txt-center margin-top-40" ng-if="tipoMidiaNome && aprovadores_midia_selecionados.length" class="form-section clearfix">
                    <a href="{{ route('site.adsmart.tipos_midia') }}" class="btn-control btn-call-to-action call-to-action-negative btn-icon btn-icon-left">
                        <span><i class="icon fa fa-times"></i> Cancelar</span>
                    </a>
                    <button type="button" ng-click="enviarFormulario()" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                        <span><i class="icon fa fa-check"></i> Criar Tipo de Mídia</span>
                    </button>
                </div>
            </div>
        </form>
    </section>
</div>
@stop