@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('adsmart.midias.criar', $id_familia, $id_campanha) !!}

<script>
    var csrf_token = '{{ csrf_token() }}';
    var id_familia = '{{ $id_familia }}'
    var id_campanha = '{{ $id_campanha }}'
</script>

<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/adsmart/midia-criar-controller.js') }}"></script>

<div ng-app="app" ng-controller="MidiaCriarController" ng-cloak class="content content-fluid clearfix">
    <section class="artw-package-edit-container margin-top-10 margin-bottom-40">
        <form name="createMediaForm" action="" method="POST" novalidate>
            <input type="hidden" name="_token" value="{{ csrf_token()}}" />
            <h3 class="master-title txt-bold margin-bottom-20">Nova Mídia</h3>
            <div class="block">
                <div class="flexbox-container flexbox-group margin-top-20 margin-bottom-15 margin-left-10 margin-right-10">
                    <h4 class="txt-lighter txt-small txt-uppercase">Mídia *</h4>
                </div>
                <div class="bs no-border no-padding margin-bottom-30" ng-class="{'no-margin-bottom': !aprovadores_midia.length}">
                    <div class="flexbox-container">
                        <div class="flex flex-fluid">
                            <selectize ng-change="carregar_aprovadores()" placeholder="Selecione o tipo de mídia" name="cbTipoMidia" id="cbTipoMidia" config="tiposMidiaConfig" options="tipos_midia" ng-model="cbTipoMidia"></selectize>
                        </div>
                    </div>
                </div>
                <div ng-class="{'bs border-color-primary padding-bottom-30': cbTipoMidia !== '' && aprovadores_midia.length > 0 }">
                    <div ng-if="aprovadores_midia.length > 0" class="block margin-bottom-15 margin-left-10 margin-right-10 margin-bottom-20">
                        <h4 class="txt-lighter txt-small txt-uppercase margin-bottom-15">Aprovadores de mídia</h4>
                        <p class="txt-regular">
                            Seguem abaixo os Aprovadores de Mídia que estão disponíveis para a abertura do ticket.
                        </p>
                    </div>
                    <div class="flexbox-container flex-align-center">
                        <table class="artw-shared-item-table table-border table-hover no-animate">
                            <tbody>
                                <tr ng-repeat="aprovador_midia in aprovadores_midia">
                                    <td width="" class="table-cell-h">
                                        <div class="d-flex flex-align-center">
                                            <i class="icon fa fa-thumbs-o-up"></i>
                                            <span class="text-side">@{{ aprovador_midia.nome }}</span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>    
                </div>
                <div ng-show="cbTipoMidia" class="btn-group txt-center margin-top-40" class="form-section clearfix">
                    <a href="{{ route('site.adsmart.midias', [$id_familia, $id_campanha]) }}" class="btn-control btn-call-to-action call-to-action-negative btn-icon btn-icon-left">
                        <span><i class="icon fa fa-times"></i> Cancelar</span>
                    </a>
                    <button ng-click="enviarFormulario()" type="button" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                        <span><i class="icon fa fa-check"></i> Criar Mídia</span>
                    </button>
                </div>
            </div>
        </form>
    </section>
</div>
@stop