<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        html,
        body {
            text-align: center;
            height: 100%;
            margin: 0px;
            padding: 0px;
        }
        body {
            overflow: hidden;
        }
        img {
            position: absolute;
            top: 0px;
            left: 50%;
            height: 100%;
            transform: translateX(-50%); 
        }
    </style>
</head>
<body>
    <div>
        <img src="{{ $image }}?view=1">
    </div>
</body>
</html>
