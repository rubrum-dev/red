@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

{!! Breadcrumbs::render('skus', $produto->id) !!}

<input id="family" type="hidden" value="{{ $produto->id_familia }}">
<input id="product" type="hidden" value="{{ $produto->id }}">
<input id="checkPromo" type="hidden" value="0">

<div class="content content-fluid">
    <!-- Showcase panel -->
    <div class="showcase-panel flexbox-container flex-align-center clearfix">
        <div class="showcase-left">
            <span class="helper"></span>
            @if($produto->logotipo)
                @if($produto->replicar_logo_marca)
                    <div style="width: 100%; height: 100%; background-color: {{ $produto->cor_hexa }}">
                        <img src="{{ asset($produto->logotipo) }}" title="Produto" alt="Produto" />
                    </div>
                @else
                    <img src="{{ asset($produto->logotipo) }}" title="Produto" alt="Produto" />
                @endif
            @else
                {{ Html::image(asset('/images/layout/cliente_'. config('app.costummer_id') .'/logo-home.png'), 'Logo') }}
            @endif
        </div>
        <div class="showcase-body">
            <div class="showcase-container">
                <div class="flexbox-container clearfix">
                    <div class="flex-auto showcase-info showcase-item-detail no-margin-left margin-right-10 no-margin-bottom">
                        <h3 class="txt-bold">{{ $produto->nome }}</h3>
                        <small class="text-small txt-color-alt margin-top-5">produto</small>
                    </div>
                    <div class="flex-auto showcase-info showcase-item-detail no-margin-left margin-right-10 no-margin-bottom">
                        <h3 class="txt-light">{{ $produto->familias->nome }}</h3>
                        <small class="text-small txt-color-alt margin-top-5">marca</small>
                    </div>
                    <div class="flex-auto showcase-info showcase-item-detail no-margin-left no-margin-bottom">
                        <h3 class="txt-light">{{ $produto->categorias->nome }}</h3>
                        <small class="text-small txt-color-alt margin-top-5">categoria</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Showcase panel -->
    <div class="packshelf-container margin-top-20 margin-bottom-10">
        <section class="package-list">
            <ul class="tabs clearfix">
                <li><a href="javascript:;" class="txt-color-default active">Embalagens de Mercado</a></li>
                {{--<li><a href="javascript:;" class="disabled">Embalagens Descontinuadas</a></li>--}}
            </ul>
            <div class="block margin-top-20 margin-bottom-30">
                <div class="flexbox-container">
                    <div class="flex margin-left-auto">
                        @if($produto->logoDown)
                            <a target="_blank" href="{{ asset($produto->logoDown) }}" class="btn-function download"><i class="icon fa fa-download"></i> Baixar Logo</a>
                        @endif
                        {{--@if(!Auth::guest())
                            @if(in_array($produto->id, $user->favorites))
                                <a href="javascript:;" id="imgSkuTitle" class="btn-function favorite active"><i class="icon fa fa-star"></i> Favorito</a>
                            @else
                                <a href="javascript:;" id="imgSkuTitle" class="btn-function favorite"><i class="icon fa fa-star"></i> Adicionar Favorito</a>
                            @endif
                        @endif--}}
                    </div>
                </div>
            </div>
            <div class="listing">
                @if(count($skus) > 0)
                    <?php $first = true; ?>
                        <?php $cont = 0;?>
                        <div class="skus">
                            {{--<div class="flexbox-container flex-align-center margin-bottom-10">
                                <!--h4 class="master-title-sub txt-small txt-light txt-uppercase no-margin clearfix">to-do Natureza</h4-->
                                <div class="flexbox-group flex margin-left-auto">
                                    @if(!Auth::guest() && $first)
                                        <!-- Verifica se é usuário Admin. Caso não seja, verifica se o usuário tem permissão na família e no Módulo SKU (id_modulo = 3) -->
                                        @if($promo['check'] && $promo['check_expirados'] && (in_array($user->id_adm_perfil, [1,2]) || (in_array($produto->id_familia, $user->familias) && in_array(3, $user->modulos))))
                                            <a href="javascript:void(0)" class="btn-function btn-expired btn-show-expired txt-capitalize margin-left-auto"><i class="icon fa fa-bomb"></i> Mostrar Itens Expirados</a>
                                        @endif
                                    @endif
                                </div>
                            </div>--}}
                            <ul>
                                @foreach($skus as $sku)
                                    <?php $check = array(); ?>
                                        @if($promo['check'])
                                            @if($sku->dt_expiracao < $sku->now)
                                                <li class="unitario-sku-promo hide-show hidden">
                                            @else
                                                <li class="unitario-sku-promo">
                                            @endif
                                        @else
                                            <li class="unitario-sku sku-box-interno">
                                        @endif
                                            <div class="box-image animate">
                                                <a href="#sku" class="sku-modal" rel="modal:open" data-toggle="modal" data-id-sku="{{ $sku->id }}"></a>
                                                <?php $cont++; ?>
                                                @if(count($sku->artesFinais) > 0)
                                                    @foreach($sku->artesFinais as $arteFinal)
                                                        <?php $check[] = $arteFinal->arte_exibicao; ?>
                                                        @if($arteFinal->arte_exibicao == 1 && $arteFinal->status == 1)
                                                            {{-- Html::image(asset($arteFinal->thumb), $arteFinal->nome, array('class' => 'sku-thumb')) --}}
                                                            {{ Html::image(asset($arteFinal->thumb), $arteFinal->nome, array('class' => 'sku-thumb')) }}
                                                        @endif
                                                    @endforeach
                                                @endif
                                                @if(!in_array(1, $check))
                                                    {{ Html::image(asset($sku->dimensoes->thumb), $sku->nome, array('class' => 'sku-thumb')) }}
                                                @endif
                                            </div>
                                            <span class="package-name txt-regular">
                                                <a href="#sku" class="sku-modal" rel="modal:open" data-toggle="modal" data-id-sku="{{ $sku->id }}" class="no-padding-bottom">
                                                    {{ $sku->embalagemVolume }}
                                                    {{--@if($promo['check'])
                                                        @if($sku->dt_expiracao < $sku->now)
                                                            <small class="txt-regular txt-color-alt txt-primary">Expirado em {{ $sku->expiracao }}</small>
                                                        @else
                                                            <small class="txt-regular txt-color-alt txt-expired">Expira em {{ $sku->expiracao }}</small>
                                                        @endif
                                                    @endif--}}
                                                </a>
                                            </span>
                                        </li>
                                @endforeach
                                @if($cont == 0)
                                    <li>
                                        <div class="is-empty-big">
                                            <span class="fa fa-cubes">Nenhum item cadastrado</span>
                                        </div>
                                    </li>
                                @endif
                            </ul>
                        </div>
                        <?php $first = false; ?>
                @endif
            </div>
        </section>
    </div>
    <script>
        $(function () {
            window.path = window.location.origin;
            
            var now = new Date;

            $(function () {
                if (GetURLParameter('sku')) {
                    var sPageURL = window.location.pathname;
                    var sURLVariables = sPageURL.split('/');
                    var family = sURLVariables[2];
                    var product = sURLVariables[4];
                    
                    window.id = GetURLParameter('sku');
                    
                    $('.sku-modal').trigger('click');
                    
                    clickSkuModal();
                } else {
                    var family = $('#family').val();
                    var product = $('#product').val();
                }

                $('.sku-modal').click(function () {
                    window.id = $(this).data('id-sku');
                    clickSkuModal();
                });
                
                /*$('#imgSkuTitle').click(function () {
                    $.ajax({
                        type: 'get',
                        url: window.path + '/site/' + family + '/produtos/' + product + '/favorite',
                        context: document.body
                    }).done(function (data) {
                        checkFavorite(data);
                    });
                });*/

                /*$('#show-hide-itens-expirados').click(function () {
                    if ($('.hide-show').is(':visible')) {
                        $('.hide-show').hide();
                        $('#text-show-hide-expired').text('Mostrar Itens Expirados');
                    } else {
                        $('.hide-show').show();
                        $('#text-show-hide-expired').text('Ocultar Itens Expirados');
                    }
                });*/

                /*$('.favorite').each(function () {
                    var el = '<span></span>';

                    $(this).click(function (e) {
                        if (!$(this).parents('.top-inline').find('a:first').hasClass('favorite-set-yes')) {
                            $(this).parents('.top-inline').find('a:first').addClass('favorite-set-yes');
                            $(this).parents('.top-inline').find('a:first').text('Favorito');
                            $(this).parents('.top-inline').find('a:first').prepend($(el).addClass('icon icon-star'));
                        } else {
                            $(this).parents('.top-inline').find('a:first').removeClass('favorite-set-yes');
                            $(this).parents('.top-inline').find('a:first').text('Adicionar Favorito');
                            $(this).parents('.top-inline').find('a:first').prepend($(el).addClass('icon icon-star'));
                        }
                    });
                });*/

                $('.top-packing-logo .logo-down, .top-nav-right .download > a').click(function (e) {
                    setTimeout(removeLoader, 500);
                });
            });

            function GetURLParameter(sParam) {
                var sPageURL = window.location.search.substring(1);
                var sURLVariables = sPageURL.split('&');
                
                for (var i = 0; i < sURLVariables.length; i++) {
                    var sParameterName = sURLVariables[i].split('=');
                    if (sParameterName[0] == sParam) {
                        return sParameterName[1];
                    }
                }
            }

            function clickSkuModal() {
                var customLoadingHTML = '<div class="line-wobble"></div>';

                $('.modal-sku-title h1 span').empty();
                $('.artes-finais').empty();
                $('.imagens-sku-down').empty();

                urlServico = window.path + '/site/' + family + '/produtos/' + product + '/sku/modal/' + window.id;

                $.dialog({
                    title: '',
                    content: '',
                    animateFromElement: false,
                    backgroundDismiss: true,
                    closeIcon: true,
                    closeIconClass: 'jconfirm-btn-close fa fa-times',
                    boxWidth: '910px',
                    useBootstrap: false,
                    onContentReady: function () {
                        var self = this;

                        return $.ajax({
                            type: 'get',
                            url: urlServico,
                            context: document.body
                        }).done(function (data) {
                            $('.jconfirm-content').css('visibility', 'visible');
                            $('.jconfirm-buttons').hide();
                            $('.jconfirm-box .line-wobble').remove();

                            var modalHTML = '<div id="sku" class="modal-sku">' +
                                '	<div class="modal-sku-title margin-bottom-30 clearfix">' +
                                '		<h1><span></span></h1>' +
                                '	</div>' +
                                '	<div class="modal-sku-hide">' +
                                '		@if(!Auth::guest())' +
                                '			<div id="check-zip-pdf">' +
                                '				@if(in_array($user->id_adm_perfil, [1,2]) || (in_array($produto->id_familia, $user->familias) && in_array(2, $user->modulos)))' +
                                '					<div class="artes-finais"></div>' +
                                '				@endif' +
                                '				<br />' +
                                '			</div>' +
                                '		@endif' +
                                '		<div class="imagens-sku-down clearfix"></div>' +
                                '	</div>' +
                                '</div>';

                            self.setContentAppend(modalHTML);

                            $('.modal-sku-title h1 span').append((data.modalSku.nome));
                            $('.modal-sku-title .modal-sku-img-redim').attr("src", window.path + data.modalSku.dimensoes.thumb);

                            if (data.modalSku.artesFinais.length == 0)
                                $('.modal-sku-hide').hide();
                            else
                                $('.modal-sku-hide').show();

                            $(data.modalSku.artesFinais).each(function (index, value) {
                                if (value.arte_exibicao == 1) {
                                    $('.modal-sku-title .modal-sku-img-redim').attr("src", window.path + value.thumb);
                                }
                                //getFinalArts(value, data.user);
                                getImagesSku(value, data.user);
                            });
                            
                            if (!data.modalSku.zip_pdf)
                                $('#check-zip-pdf').hide();
                            else
                                $('#check-zip-pdf').show();
                        }).fail(function () {
                            myAlert('Ocorreu um erro.', 'error');
                            return false;
                        });
                    },
                    onOpenBefore: function () {
                        $('.jconfirm-content').css('visibility', 'hidden');
                        $('.jconfirm-box').append(customLoadingHTML);                    
                        $('.jconfirm-buttons').hide();
                        $('.jconfirm-bg').addClass('jconfirm-bg-alt');
                        $('body').addClass('no-scroll');
                        $('.jconfirm-content').css('marginLeft', '0px');
                        $('.jconfirm-box').addClass('jconfirm-box-alt');
                        $('.jconfirm').css('overflow', 'auto');

                        $('.jconfirm').overlayScrollbars({
                            scrollbars: {
                                autoHide: 'scroll'
                            }
                        });
                    },
                    onClose: function () {
                        $('.jconfirm-bg').addClass('jconfirm-bg-alt');
                        $('body').removeClass('no-scroll');
                        $('.jconfirm').css('overflow', 'hidden');
                    }
                });
            }

            /*function checkFavoriteSuccess(message) {
                if ($.alert) {
                    $.alert({
                        icon: 'icon fa fa-check',
                        title: 'Sucesso',
                        content: message,
                        buttons: {
                            ok: {
                                text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                                btnClass: 'btn-confirm',
                                keys: ['enter', 'esc', 'space'],
                                action: function () {
                                    location.reload();
                                }
                            }
                        },
                        onClose: function () {
                            location.reload();
                        }
                    });
                } else {
                    alert(message);
                }
            }*/

            /*function checkFavorite(data) {
                if (data.favorite) {
                    checkFavoriteSuccess(data.prod + ' Adicionado aos Favoritos', 'success');
                } else {
                    checkFavoriteSuccess(data.prod + ' Removido dos Favoritos', 'success');
                }
            }*/

            /*function getFinalArts(data, user) {
                var val = '';
                $(data).each(function (index, value) {
                    if (value.link_pdf != null) {
                        // Verifica se é usuário Admin. Caso não seja, verifica se o usuário tem permissão no Módulo SKU (id_modulo = 3)
                        if (user && (($.inArray(user.id_adm_perfil, [1, 2]) !== -1) || (($.inArray(3, user.modulos) !== -1) && ($.inArray(parseInt(family.value, 10), user.familias) !== -1)))) {
                            val += '<div class="modal-sku-artes-finais">';
                            if (value.nome_arte)
                                val += '<p>' + value.nome_arte + '<br /><small>Adicionado em ' + value.data + ' às ' + value.hora + '</small></p>';
                            else
                                val += '<p>' + value.sku_nome + '<br /><small>Adicionado em ' + value.data + ' às ' + value.hora + '</small></p>';
                            
                            val += '<span>';
                            
                            if (value.link_pdf != null) {
                                if (value.link_pdf != '') {
                                    val += '<a href="' + value.link_pdf + '" target="_blank">';
                                    val += '	<img src="' + window.path + '/images/icons/frontend/short-down.png" alt="down"> PDF';
                                    val += '</a>';
                                }
                            }
                            val += '</span>';
                            val += '</div>';
                        }
                    }
                });
                $('.artes-finais').append(val);
            }*/

            function getImagesSku(data, user) {
                var val = '';

                $(data).each(function (index, value) {
                    if (value.thumb) {
                        val += '<div class="unitaria-img-sku">';
                        val += '<div class="unitaria-img-sku-interno">';
                        val += '<a href="' + window.path + value.path_cmyk + '?view=1" target="_blank" class="animate">'
                        val += '<img src="' + window.path + value.thumb + '" alt="arquivo" class="modal-sku-img-redim" />';
                        val += '</a>'
                        val += '<div class="nome-sku-box">';
                        
                        if (value.nome)
                            val += '<p><a href="' + window.path + value.path_cmyk + '?view=1" class="txt-regular">' + value.nome + '</a></p>';
                        else
                            val += '<p><a href="' + window.path + value.path_cmyk + '?view=1" class="txt-regular">' + value.sku_nome + '</a></p>';
                        
                        val += '</div>';
                        val += '</div>';
                        val += '<div class="flexbox-container flex-justify-center">'
                        
                        if (value.path_cmyk != null) {
                            if (value.path_cmyk != '') {
                                val += '<span class="unitaria-btn-sku btn-view">';
                                val += '<a href="' + window.path + value.path_cmyk + '?view=1" target="_blank" data-title="Visualizar" class="btn-function ui-tooltip top">';
                                val += '<i class="icon fa fa-eye no-margin"></i>';
                                val += '</a>';
                                val += '</span>';
                            }
                        }
                                                
                        if (value.path_rgb != null) {
                            if (value.path_rgb != '') {
                                val += '<span class="unitaria-btn-sku btn-high-res">';
                                val += '<a href="' + window.path + value.path_rgb + '" target="_blank" data-title="Alta Resolução" class="btn-function ui-tooltip top">';
                                val += '<i class="icon fa fa-download no-margin"></i>';
                                val += '</a>';
                                val += '</span>';
                            }
                        }
                        
                        val += '</div>';
                        val += '</div>';
                    }
                });

                $('.imagens-sku-down').append(val);
            }
        });  
    </script>
</div>
@stop