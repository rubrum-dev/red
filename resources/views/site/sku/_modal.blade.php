<div id="sku" class="modal-sku">
	<div class="modal-sku-title clearfix">
		{{ Html::image(asset($produto->logotipo), 'Produto') }}
		<h1><span>Nome</span></h1>
	</div>
	<div class="modal-sku-hide">
		@if(!Auth::guest())
            <div id="check-zip-pdf">
    			<!-- Verifica se é usuário Admin. Caso não seja, verifica se o usuário tem permissão na família e no Módulo File (id_modulo = 2) -->
    			@if(in_array($user->id_adm_perfil, [1,2]) || (in_array($produto->id_familia, $user->familias) && in_array(2, $user->modulos)))
    				<h4>Artes-Finais</h4>
    				<div class="artes-finais"></div>
    			@endif
    			<br />
            </div>
		@endif
		<div class="imagens-sku-down clearfix"></div>
	</div>
</div>
