@extends('site.site_template')
@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm.css') }}" />
{{--<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm-custom.css') }}" />--}}
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />
{{--<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/techdraw.css') }}" />--}}

{!! Breadcrumbs::renderIfExists('techdraw.plantas', $id_tipo) !!}

<script>
	var id_tipo = '{{ $id_tipo }}';
    var csrf_token = '{{ csrf_token() }}';
</script>

<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload-shim.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload.min.js') }}"></script>
<script src="{{ asset('/js/angular/tipo-embalagem-resolve-loader.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/techdraw/tipo_embalagem.js') }}"></script>
<script src="{{ asset2('/js/site/techdraw/embalagem.js') }}"></script>

<div class="content content-fluid" ng-app="app" ng-controller="tipoEmbalagemCtrl" ng-cloak>
    <div class="techdraw-container">
        <section class="bs margin-top-10 margin-bottom-20 margin-bottom-40">
            <h3 class="flexbox-container flex-vertical-center master-title margin-bottom-20"><i class="icon fa fa-folder-open"></i>@{{ itens.nome_tipo }}</h3>
			
            @if((in_array('techdraw_criar_planta', Auth::user()->modulos)))
                <div ng-if="loaded" class="flexbox-container flex-align-center fadein margin-bottom-20">
                    <div class="techdraw-create-btn flex">
                        <a href="{{ route('site.techdraw.technicalPlan.create.get') }}" class="btn-function"><i class="icon fa fa-codepen"></i> Nova Planta Técnica</a>
                    </div>
                </div>
            @endif
            
            <div resolve-loader></div>        
            
            <div ng-show="itens.plantas.length && loaded" class="fadein">
                <table cellpading="0" cellspacing="0" class="techdraw-package-table table-border table-hover no-margin no-animate">
                    <tbody>
                        <tr ng-repeat="planta in itens.plantas" class="table-group no-animate">
                            <td colspan="2">
                                <table cellpading="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td width="">
                                                <label class="txt-package-name">
                                                    <input ng-show="planta.exibir_formulario" type="checkbox" ng-model="planta.stateJS" ng-click="exibeNovoItem(planta, false)" class="ng-toggle" />
                                                    <input ng-show="!planta.exibir_formulario" type="checkbox" ng-model="planta.stateJS" ng-click="exibeNovoItem(planta, true)" class="ng-toggle" />
                                                    <span class="txt-color-default d-flex flex-align-center">
                                                        <i ng-class="{'icon fa fa-folder-open txt-color-alt': planta.stateJS, 'icon fa fa-folder txt-color-alt': !planta.stateJS }" class="icon fa fa-folder txt-color-alt"></i>
                                                        <div class="text-side txt-regular">
                                                            <span class="txt-regular">@{{ planta.nome_completo }}</span>
                                                        </div>
                                                    </span>
                                                </label>
                                            </td>
                                            {{--<td width="50">
                                                @if( (in_array('techdraw_criar_planta', Auth::user()->modulos)) )
                                                    <span tooltips tooltip-side="top" tooltip-size="small" tooltip-smart="true" tooltip-template="Adicionar Item">    
                                                        <a href="javascript:;" ng-if="planta.exibir_formulario" ng-click="planta.stateJS = !planta.stateJS;exibeNovoItem(planta, false)" ng-class="{'active': planta.stateJS}" class="btn-function no-animate"><i class="icon no-float no-margin fa fa-clone"></i></a>
                                                        <a href="javascript:;" ng-if="!planta.exibir_formulario" ng-click="planta.stateJS = true;exibeNovoItem(planta, true)" class="btn-function no-animate"><i class="icon no-float no-margin fa fa-clone"></i></a>
                                                    </span>
                                                @endif
                                            </td>--}}
                                            <td width="50">
                                                @if( (in_array('techdraw_criar_planta', Auth::user()->modulos)) || (in_array('techdraw_excluir_planta', Auth::user()->modulos)) )
                                                    <div class="dropdown drop-to-left animate">
                                                        <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                                        <div class="dropdown-list">
                                                            <ul>
                                                                @if( (in_array('techdraw_criar_planta', Auth::user()->modulos)) )
                                                                    <li><a ng-click="planta.stateJS = true;exibeNovoItem(planta, true)" href="javascript:;" class="txt-bold"><span class="icon fa fa-file"></span>Adicionar Item</a></li>
                                                                @endif

                                                                @if((in_array('techdraw_criar_planta', Auth::user()->modulos)))
                                                                    <li><a ng-href="@{{ planta.link_edicao }}" class="txt-bold"><span class="icon fa fa-edit"></span>Alterar Planta Técnica</a></li>
                                                                @endif
                                                                
                                                                @if((in_array('techdraw_excluir_planta', Auth::user()->modulos)))
                                                                    <li><a ng-click="excluirPlanta(planta)" href="javascript:;" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir Planta Técnica</a></li>
                                                                @endif    
                                                            </ul>
                                                        </div>
                                                    </div>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr ng-repeat="item in planta.itens" ng-if="planta.stateJS" class="table-sub">
                                            <td colspan="2">
                                                <div class="table-sub-content slide-toggle-js">
                                                    <table cellpading="0" cellspacing="0" class="no-margin-bottom">
                                                        <tbody>
                                                            <!-- Table sub-group -->
                                                            <tr>
                                                                <td width="">
                                                                    <div class="txt-package-item-name txt-item-name no-animate" ng-show="!item.exibir_editar_item">
                                                                        <a ng-href="@{{ item.versao.path }}" target="_blank"><span class="text-side no-margin txt-regular"><i class="icon fa fa-file"></i><strong class="flex-inline txt-regular txt-color-default margin-left-10">@{{ item.nome }}</strong></span></a>
                                                                    </div>
                                                                    <div ng-show="item.exibir_editar_item" class="flexbox-container flexbox-group techdraw-add-item-sub no-animate">
                                                                        <i class="icon fa fa-file"></i>
                                                                        <div class="flex col-selecione-item animate">
                                                                            <div class="select-control">
                                                                                <select ng-init="item.id_tipo_original = item.id_tipo" ng-model="item.id_tipo" ng-options="item_tipo.id as item_tipo.nome for item_tipo in itens.item_tipos" class="input-required"></select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="flexbox-container flexbox-group flex col-btn-group animate">
                                                                            <div class="flex col-member-edit-cancel">
                                                                                <button type="button" ng-click="item.exibir_editar_item = false" class="btn-control btn-alt">Cancelar</button>
                                                                            </div>
                                                                            <div class="flex col-member-edit-save">
                                                                                <button ng-click="alterar_item(item)" type="button" class="btn-control btn-positive">Salvar</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td width="50px">
                                                                    @if((in_array('techdraw_criar_planta', Auth::user()->modulos)))
                                                                        <span tooltips tooltip-side="top" tooltip-size="small" tooltip-smart="true" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Carregar Planta Técnica">
                                                                            <a ng-click="techDrawUpload(item)" href="javascript:;" class="btn-function"><i class="icon fa fa-upload"></i></a>
                                                                        </span>
                                                                    @endif
                                                                </td>
                                                                <td width="50px">
                                                                    @if( (in_array('techdraw_criar_planta', Auth::user()->modulos)) || (in_array('techdraw_excluir_planta', Auth::user()->modulos)) )
                                                                        <div class="dropdown drop-to-left animate">
                                                                            <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                                                            <div class="dropdown-list">
                                                                                <ul>    
                                                                                    @if((in_array('techdraw_criar_planta', Auth::user()->modulos)))
                                                                                        <li><a ng-click="exibeEditarItem(planta, item)" href="javascript:;" class="txt-bold"><span class="icon fa fa-edit"></span>Alterar Item</a></li>
                                                                                    @endif
                                                                                    
                                                                                    @if((in_array('techdraw_excluir_planta', Auth::user()->modulos)))
                                                                                        <li><a ng-click="excluirItem(item)" href="javascript:;" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                                                    @endif
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            <!-- END Table sub-group -->
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr ng-if="!planta.itens.length && !planta.exibir_formulario && planta.stateJS" class="table-sub no-animate">
                                            <td colspan="2">
                                                <div class="table-sub-content no-item slide-toggle-js">
                                                    <table cellpading="0" cellspacing="0" class="no-margin-bottom">
                                                        <tbody>
                                                            <tr>
                                                                <td width="">
                                                                    <div class="txt-regular">
                                                                        <span class="txt-regular txt-color-alt">Essa embalagem não possui nenhum item cadastrado</span>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- Incluir item -->
                                        <tr ng-show="planta.exibir_formulario" class="table-sub no-animate">
                                            <td colspan="2">
                                                <div class="table-sub-content slide-toggle-js">
                                                    <table cellpading="0" cellspacing="0" class="no-margin-bottom">
                                                        <tbody>
                                                            <!-- Table sub-group -->
                                                            <tr>
                                                                <td colspan="4">
                                                                    <div class="flexbox-container flexbox-group flex-align-center techdraw-add-item-sub animate">
                                                                        <i class="icon fa fa-file"></i>
                                                                        <div class="flex col-selecione-item margin-left-5">
                                                                            <div class="select-control">
                                                                                <select ng-model="planta.id_item_tipo_novo" class="input-required">
                                                                                    <option value="">Selecione o Item</option>
                                                                                    <option ng-repeat="item_planta in itens.item_tipos" value="@{{ item_planta.id }}">@{{ item_planta.nome }}</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="flexbox-container flexbox-group flex col-btn-group">
                                                                            <div class="flex col-member-edit-cancel">
                                                                                <button type="button" ng-click="planta.exibir_formulario = false" class="btn-control btn-alt">Cancelar</button>
                                                                            </div>
                                                                            <div class="flex col-member-edit-save">
                                                                                <button type="button" ng-click="criar_item(planta)" class="btn-control btn-positive">Salvar</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <!-- END Table sub-group -->
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- END Incluir item -->
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div ng-show="!itens.plantas.length && loaded" class="techdraw-no-item fa fa-codepen">
                Nenhuma planta técnica localizada
            </div>
        </section>
    </div>
</div>
@stop