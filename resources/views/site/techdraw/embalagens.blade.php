@extends('site.site_template')
@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop
@section('content')
{!! Breadcrumbs::renderIfExists('artwork.categorias') !!}
<script src="{{ asset('/js/site/artwork/embalagens.js?v=2') }}"></script>
<div class="content">
	<div class="top-head">
		<div class="top-inline top-packing-logo">
			<a href="javascript:;" class="logo-down"><span class="logo-down-ico">Baixar Logo</span></a>
			{{ Html::image(asset($thumb), 'Logo', array('title' => '', 'alt' => 'Logo')) }}
		</div>
		<div class="top-inline top-head-title top-head-title-center">
			<h2><span>Embalagens</span></h2>
		</div>
		<div class="top-inline top-nav-right">
			<ul>
				<li><a href="#"><span class="icon icon-rec"></span> Ativas</a></li>
				<li><a href="#"><span class="icon icon-ban-circle"></span> Descontinuadas</a></li>
			</ul>
		</div>
	</div>
	<div class="table-pack-container" id="embalagens-regulares">
		<table class="table-pack table-hover" border="0" cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<th width="500">Embalagens Regulares</th>
					<th width="100">Ver PDF</th>
					<th width="80">Histórico</th>
					<th width="100">Enviar Arte</th>
					<th width="120">Solicitar Alteração</th>
				</tr>
			</thead>
			<tbody>
				@php $i = 0; @endphp
                @foreach ($skus as $key => $sku)
					@if(!$sku['promo'])
						<tr @if($i>0)class="border-top-tr"@endif>
							@php $i++; @endphp
                        	<td width="500">
								<span class="icon-item icon-play"></span> 
								<div style="margin-left:23px;">
									<a target="_blank" href="{{ asset('images/uploads/' . $sku->path) }}">{{ $sku->tipo }} {{ $sku->embalagem_nome }}</a>
								</div>		
							</td>
							<td width="100"><a href="{{ asset('images/uploads/' . $sku->path) }}" target="_blank">Versão {{ $sku->numero }}</a></td>
							<td width="80"><a target="_blank" class="table-icon-td icon-compass-2 icon-disabled"></a></td>
							<td width="100"><a target="_blank" class="table-icon-td icon-mail icon-disabled"></a></td>
							<td width="120"><a class="table-icon-td icon-options-settings icon-disabled"></a></td>
						</tr>
					@endif
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@stop
