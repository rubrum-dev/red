@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('techdraw.nova') !!}

{{--<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/techdraw.css') }}" />--}}

<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/techdraw/technical_plan_create.js') }}"></script>
<script src="{{ asset2('js/site/artwork/package-create.js') }}"></script>

@if (count($errors) > 0)
    <div class="alert alert-danger" role="alert">
        <div class="alert-error">
            @foreach ($errors->all() as $error)
                {{ $error}}<br />
            @endforeach
        </div>
    </div>
@endif

@if (session('sucesso'))
    <div class="alert alert-info">
        {{ session('sucesso')}}
    </div>
@endif

<div class="content content-fluid" ng-app="app" ng-controller="technicalPlanCreateCtrl">
    {{--
    <div class="top-head">
		<div class="top-inline top-head-title text-left no-padding">
			<h2><span>Nova Planta Técnica</span></h2>
		</div>
    </div>
    --}}
    <div class="techdraw-container">
        <section class="bs margin-top-10 margin-bottom-40">
            <h3 class="master-title txt-bold margin-bottom-20">Nova Planta Técnica</h3>
            <div class="box-package-info panel showcase-panel auto-height no-margin clearfix">
                <div ng-cloak class="showcase-body no-margin">
                    <div class="showcase-container">
                        <div class="flexbox-container flex-no-wrap clearfix">
                            <div class="flex-auto showcase-info showcase-item-detail text-inline col-100-max no-margin-bottom">
                                <h3 class="txt-bold">
                                    <strong ng-repeat="t in tipos" ng-show="t.id == cbPckType"><span>@{{ t.nome }}</span></strong>
                                    <strong ng-show="!cbPckType || cbPckType === 0"><span>@{{ cbPckType ? cbPckType : 'Tipo de Embalagem' }}</span></strong>
                                </h3>
                                <small class="txt-small txt-color-alt margin-top-5">tipo</small>
                            </div>
                            <div class="flex-auto showcase-info showcase-item-detail text-inline col-30-max no-margin-bottom">
                                <h3 class="txt-bold">
                                    <strong ng-repeat="t in tamanhos" ng-show="t.id == cbPckContentSize"><span>@{{ t.nome }}</span></strong>
                                    <strong ng-show="!cbPckContentSize || cbPckContentSize === 0"><span>@{{ (cbPckContentSize) ? cbPckContentSize : 'Volume' }}</span></strong>
                                </h3>
                                <small class="txt-small txt-color-alt margin-top-5">volume</small>
                            </div>
                            <div class="flex-auto showcase-info showcase-item-detail text-inline col-30-max no-margin-bottom">
                                <h3 class="txt-light">
                                    <strong ng-show="!packageHasVariation || packageHasVariation === 'Sim'"><span class="txt-light">@{{ (variacaoName) ? variacaoName : 'Variação' }}</span></strong>
                                </h3>
                                <small ng-show="!packageHasVariation || packageHasVariation === 'Sim'" class="txt-small txt-color-alt margin-top-5">variação</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <form name="newPackageForm" action="" method="POST" ng-cloak>
                <input type="hidden" name="_token" value="{{ csrf_token()}}" />
                <div class="block no-margin">
                    <div class="flexbox-container flexbox-group margin-top-20 margin-bottom-20 margin-left-10 margin-right-10">
                        <h4 class="txt-lighter txt-small txt-uppercase margin-bottom-15">Nome da nova planta técnica</h4>
                        <p class="txt-regular">
                            A nomenclatura correta da planta técnica é fundamental para que tudo funcione de forma correta. A medida que 
                            as perguntas abaixo são respondidas o sistema irá concatenar o nome da planta técnica, preste atenção se está 
                            correto. Sinta-se à vontade em alterar suas respostas até que o nome construído faça sentido.
                        </p>
                    </div>
                    <div class="bs">
                        <div class="flexbox-container">
                            <div class="flex flex-fluid">
                                <selectize ng-change="carregar_tamanhos()" ng-model="cbPckType" config="pkgTypesConfig" options="tipos"></selectize>
                                <input name="cbPckType" value="@{{ cbPckType }}" type="hidden">
                            </div>
                            <div class="flex flex-fluid">
                                <selectize ng-change="carregar_variacoes()" ng-model="cbPckContentSize" config="pkgContentSizeConfig" options="tamanhos"></selectize>
                                <input name="cbPckContentSize" value="@{{ cbPckContentSize }}" type="hidden">
                            </div>
                        </div>
                    </div>
                    <div ng-show="variacoes.length" class="bs margin-bottom-40 no-padding no-border">
                        <div class="margin-left-10 margin-right-10 margin-bottom-15">
                            <h4 class="txt-lighter txt-small txt-uppercase margin-bottom-15">Planta técnica já cadastrada no sistema!</h4>
                            <p class="txt-regular">
                                O sistema permite cadastrar várias plantas técnicas iguais, desde que possuam nomes de variação diferentes. 
                                Identifique o que essa planta técnica possui de diferente da(s) outra(s) e utilize isso como nome para essa 
                                variação.
                            </p>
                        </div>
                        <div class="flexbox-container flex-align-center">
                            <div class="flex flex-fluid">
                                <input type="text" placeholder="Nome da variação" ng-model="variacaoName" name="variacaoName" id="variacaoName" class="input-control only-alpha-numeric" />
                                <p ng-if="variacaoName && (variacoes | filter:{nome:variacaoName}:true).length" class="txt-small txt-regular txt-color-primary margin-top-10 margin-left-10 margin-right-10">
                                    Essa variação já está em uso, dê um nome diferente.
                                </p>
                                <p class="margin-top-10 margin-left-10 margin-right-10">
                                    <strong class="txt-small txt-bold">Dica:</strong> <span class="txt-small txt-regular">Cores, plantas técnicas ou fornecedores diferentes funcionam bem como nomes de variação.</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div ng-show="!variacoes.length" class="bs margin-bottom-40">
                        <div class="margin-left-10 margin-right-10 margin-bottom-15">
                            <h4 class="txt-lighter txt-small txt-uppercase margin-bottom-15">A embalagem possui variações?</h4>
                            <p class="txt-regular">
                                Embalagens iguais precisam de um nome de variação para coexistirem no sistema. Só utilize este recurso se as embalagens forem coexistir no 
                                mercado, caso contrário abra um ticket na embalagem já existente para assim criar uma nova versão. Se não tiver certeza clique em “Não”.
                            </p>
                        </div>
                        <div class="margin-left-50">
                            <div class="flexbox-container">
                                <div class="flex margin-right-50">
                                    <div class="input-group flex-align-center flexbox-container flexbox-group">
                                        <div class="flex margin-right-30">
                                            <label class="radio-btn-control">    
                                                <input type="radio" ng-change="cadastrar_variacao()" ng-model="packageHasVariation" name="packageHasVariation" value="Nao" />    
                                                <span class="checkmark"></span>
                                                <span class="radio-btn-txt txt-semibold">Não</span>
                                            </label>
                                        </div>
                                        <div class="flex">
                                            <label class="radio-btn-control">    
                                                <input type="radio" ng-change="cadastrar_variacao()" ng-model="packageHasVariation" name="packageHasVariation" value="Sim" />
                                                <span class="checkmark"></span>
                                                <span class="radio-btn-txt txt-semibold">Sim</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div ng-show="packageHasVariation === 'Sim'" class="flex flex-fluid">
                                    <input type="text" placeholder="Nome da variação" ng-model="variacaoName" name="variacaoName" id="variacaoName" class="input-control only-alpha-numeric" />
                                    <p class="margin-top-10 margin-left-10 margin-right-10">
                                        <strong class="txt-small txt-bold">Dica:</strong> <span class="txt-small txt-regular">Identifique o que essa embalagem possui de diferente da(s) outra(s) e utilize isso como nome para essa variação. Cores, plantas técnicas ou fornecedores diferentes funcionam bem.</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-group txt-center margin-top-30" ng-if="(cbPckType !== '' && cbPckContentSize !== '' && cbPckContentSize.length) && (packageHasVariation === 'Nao' || (packageHasVariation !== 'Nao') && (variacaoName && !(variacoes | filter:{nome:variacaoName}:true).length ))" class="form-section clearfix">
                    <div class="bs border-color-primary padding-top-40 no-padding-bottom">
                        <button type="submit" ng-click="enviarFormulario()" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i> Cadastrar</span>
                        </button>
                    </div>
                </div>
                {{--
                <div class="text-block">
                    <h4>Nome da nova planta técnica</h4>
                    <p>
                        A nomenclatura correta da planta técnica é fundamental para que tudo funcione de forma correta. A medida que 
                        as perguntas abaixo são respondidas o sistema irá concatenar o nome da planta técnica, preste atenção se está 
                        correto. Sinta-se à vontade em alterar suas respostas até que o nome construído faça sentido.
                    </p>
                </div>
                <div class="box-package-info">
                    <div class="clearfix">
                        <div class="box-package-col">
                            <h4>
                                <strong ng-repeat="t in tipos" ng-show="t.id == cbPckType"><span class="cor-azul">@{{ t.nome | uppercase }}</span></strong>
                                <strong ng-show="!cbPckType || cbPckType === 0"><span class="cor-azul">@{{ cbPckType ? cbPckType : 'TIPO DE EMBALAGEM' | uppercase }}</span></strong>
                            </h4>
                            <small class="text-xs">tipo</small>
                        </div>
                        <div class="box-package-col">
                            <h4>
                                <strong ng-repeat="t in tamanhos" ng-show="t.id == cbPckContentSize"><span class="cor-azul">@{{ t.nome | uppercase }}</span></strong>
                                <strong ng-show="!cbPckContentSize || cbPckContentSize === 0"><span class="cor-azul">@{{ (cbPckContentSize) ? cbPckContentSize : 'VOLUME' | uppercase }}</span></strong>
                            </h4>
                            <small class="text-xs">volume</small>
                        </div>
                        <div class="box-package-col">
                            <h4>
                                <strong ng-show="!packageHasVariation || packageHasVariation === 'Sim'" class="txt-variation"><span class="cor-azul">@{{ (variacaoName) ? variacaoName : 'VARIAÇÃO' | uppercase }}</span></strong>
                            </h4>
                            <small ng-show="!packageHasVariation || packageHasVariation === 'Sim'" class="text-xs">variação</small>
                        </div>
                    </div>
                </div>
                <section class="form-section clearfix">
                    <hr class="form-section-spacer">
                    <div class="input-inline">
                        <p>
                            <label>Qual o tipo de embalagem?</label>
                        </p>
                        <div class="input-inline form-control-280px">
                            <selectize ng-change="carregar_tamanhos()" ng-model="cbPckType" config="pkgTypesConfig" options="tipos"></selectize>
                            <input name="cbPckType" value="@{{ cbPckType }}" type="hidden">
                        </div>
                    </div>
                    <div class="input-inline">
                        <p>
                            <label>Qual o volume?</label>
                        </p>
                        <div class="input-inline form-control-280px">
                            <selectize ng-change="carregar_variacoes()" ng-model="cbPckContentSize" config="pkgContentSizeConfig" options="tamanhos"></selectize>
                            <input name="cbPckContentSize" value="@{{ cbPckContentSize }}" type="hidden">
                        </div>
                    </div>
                </section>
                <section ng-show="!variacoes.length" class="form-section clearfix">
                    <hr class="form-section-spacer">
                    <div class="text-block">
                        <h4>A embalagem possui variações?</h4>
                        <p>
                            É possível atribuir um nome de variação para a emabalegm. Caso queira cadastrar uma segunda ou 
                            terceira embalagem igual a esta, esse nome de variação será usado para diferenciar uma embalagem 
                            da outra. Se não tem certeza clique em “Não”.
                        </p>
                    </div>
                    <div class="input-inline form-control-296px">
                        <div class="form-inline-spacer form-inline">
                            <label class="radio-btn-control radio-btn-icon">    
                                <input type="radio" ng-change="cadastrar_variacao()" ng-model="packageHasVariation" name="packageHasVariation" value="Nao" class="radio-btn-hidden" />
                                <span class="icon-select-circle" ng-class="{'icon-button-close': packageHasVariation === 'Nao'}">Não</label>
                            </label>
                        </div>
                        <div class="form-inline">
                            <label class="radio-btn-control radio-btn-icon">    
                                <input type="radio" ng-change="cadastrar_variacao()" ng-model="packageHasVariation" name="packageHasVariation" value="Sim" class="radio-btn-hidden" />
                                <span class="icon-select-circle" ng-class="{'icon-button-check': packageHasVariation === 'Sim'}">Sim</span>
                            </label>
                        </div>
                    </div>
                    <div ng-show="packageHasVariation === 'Sim'" class="input-inline no-margin no-padding">
                        <div class="form-control form-control-280px margin-bottom-20">
                            <input type="text" placeholder="Nome da variação" ng-model="variacaoName" name="variacaoName" id="variacaoName" class="only-alpha-numeric" />
                        </div>
                        <div class="hint form-control-630px">
                            <p>
                                <strong>Dica:</strong> Identifique o que essa embalagem possui de diferente da(s) outra(s) e 
                                utilize isso como nome para essa variação. Cores, plantas técnicas ou fornecedores diferentes 
                                funcionam bem.
                            </p>
                        </div>
                    </div>
                </section>
                <section ng-show="variacoes.length" class="form-section clearfix">
                    <hr class="form-section-spacer">
                    <div class="text-block">
                        <h4>Essa planta técnica já foi cadastrada no sistema, atribua um nome de variação para diferenciá-la</h4>
                        <p>
                            O sistema permite cadastrar várias plantas técnicas iguais, desde que possuam nomes de variação diferentes. 
                            Identifique o que essa planta técnica possui de diferente da(s) outra(s) e utilize isso como nome para essa 
                            variação.
                        </p>
                    </div>
                    <div class="clearfix">
                        <div class="input-inline form-control form-control-480px">
                            <input type="text" placeholder="Nome da variação" ng-model="variacaoName" name="variacaoName" id="variacaoName" class="only-alpha-numeric" />
                        </div>
                    </div>
                    <div style="margin-top: 10px" class="clearfix">
                        <p style="color:red" ng-if="variacaoName && (variacoes | filter:{nome:variacaoName}:true).length">Essa variação já está em uso, dê um nome diferente.</p>
                    </div>
                    <div class="input-inline hint form-control-480px">
                        <p>
                            <strong>Dica:</strong> Cores, plantas técnicas ou fornecedores diferentes funcionam bem como nomes 
                            de variação.
                        </p>
                    </div>
                </section>
                <section ng-if="
                    (cbPckType !== '' && cbPckContentSize !== '')
                            && (packageHasVariation === 'Nao' || (packageHasVariation === 'Sim' && (variacaoName)) || !(variacoes | filter:{nome:variacaoName}:true).length )" class="form-section clearfix">
                    <div class="form-btn-group clearfix">
                        <div class="form-btn-set">
                            <hr class="form-section-spacer">
                            <button type="submit" class="btn btn-positive">
                                <span><i class="icon icon-left icon-magic-wand"></i> <strong>Cadastrar</strong></span>
                            </button>
                        </div>
                    </div>
                </section>
                --}}
            </form>
        </section>
    </div>
</div>
@stop