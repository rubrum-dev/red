@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
{{--<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/techdraw.css') }}" />--}}

{!! Breadcrumbs::renderIfExists('techdraw.tipos') !!}

<div class="content content-fluid">
	<div class="techdraw-container">
		<section class="package-list padding-bottom-40 no-margin-bottom">
			<h3 class="master-title">Tipos de Embalagens</h3>
			<div class="block margin-top-25 margin-bottom-20">
				@if((in_array('techdraw_criar_planta', Auth::user()->modulos)))
					<div class="flexbox-container flex-align-center">
						<div class="techdraw-create-btn flex">
							<a href="{{ route('site.techdraw.technicalPlan.create.get') }}" class="btn-function"><i class="icon fa fa-codepen"></i> Nova Planta Técnica</a>
						</div>
					</div>
				@endif
			</div>
			@if(count($tipos) > 0)
				<table cellpading="0" cellspacing="0" class="techdraw-table table-border table-hover no-margin no-animate">
					<tbody>
						@foreach ($tipos as $tipo)
							<tr class="table-group">
								<td colspan="2">
									<table cellpading="0" cellspacing="0">
										<tbody>
											<tr>
												<td width="" class="table-cell-h"><a href="{{ route('site.techdraw.technicalPlan.package', [$tipo->id]) }}" class="d-flex flex-align-center table-link txt-color-default"><i class="icon fa fa-folder txt-color-alt"></i><span class="text-side txt-regular">{{ $tipo->nome }}</span></a></td>
												<!--td width="50">
													<div class="dropdown drop-to-left">
														<a class="dropdown-toggle fa fa-ellipsis-h disabled"></a>
														<div class="dropdown-list">
															<ul>    
																<li><a href="javascript:;" class="txt-bold"><span class="icon fa fa-edit"></span>Alterar Tipo da Embalagem</a></li>
																<li><a href="javascript:;" class="txt-bold"><span class="icon fa fa-truck"></span>Descontinuar Embalagem</a></li>
																<li><a href="javascript:;" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir Embalagem</a></li>
															</ul>
														</div>
													</div>
												</td-->
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			@else
				<div class="techdraw-no-item fa fa-codepen">
					Nenhuma planta técnica localizada
				</div>
			@endif
		</section>
	</div>
</div>
@stop
