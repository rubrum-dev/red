<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Rubrum Software - Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais</title>
	<!-- Favico -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=RyQmEa60ez">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=RyQmEa60ez">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=RyQmEa60ez">
    <link rel="manifest" href="/site.webmanifest?v=RyQmEa60ez">
    <link rel="mask-icon" href="/safari-pinned-tab.svg?v=RyQmEa60ez" color="#c41425">
    <link rel="shortcut icon" href="/favicon.ico?v=RyQmEa60ez">
    <meta name="apple-mobile-web-app-title" content="Rubrum">
    <meta name="application-name" content="Rubrum">
    <meta name="msapplication-TileColor" content="#c41425">
    <meta name="theme-color" content="#ffffff">
	<!-- Fonts -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" />
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/fontawesome/4.7.0/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/politica-privacidade.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/cliente_2/layout.css') }}" />
	<!-- Scripts Locais -->
	<script src="{{ asset2('/js/site/jquery.min.js') }}"></script>
    <script src="{{ asset2('/js/site/jquery.nicescroll.js') }}"></script>
    <script src="{{ asset2('/js/site/jquery.visualnav.min.js') }}"></script>
    <script src="{{ asset2('/js/site/privacy.js') }}"></script>
</head>
<body>
    <div class="privacidade-container">
		<!-- Header -->
		<header class="header">
			<div class="header-in content-fluid clearfix">
				<span class="header-brand"></span>
			</div>
		</header>
		<!-- END Header -->
		<!-- DIV Wrapped  -->
		<div class="wrapped">
			<div class="container clearfix">
				<div class="breadcumbs-out clearfix">		
                    <div class="breadcrumbs-inner content-fluid clearfix">	
                        <ul>
                            <li class="active">Política de Privacidade</li>
                        </ul>
                    </div>
                </div>
                <div class="min-height-container">
                    <div class="content content-fluid">
                        <div class="privacidade">
                            <div class="privacidade-wp">
                                <div class="sidebar">
                                    <ul class="sidebar-privacidade-ul">
                                        <li><a href="#secao_1" class="privacidade-link">Informações Gerais</a></li>
                                        <li><a href="#secao_2" class="privacidade-link">Dados Pessoais</a></li>
                                        <li><a href="#secao_3" class="privacidade-link">Finalidade dos Dados</a></li>
                                        <li><a href="#secao_4" class="privacidade-link">Do Tempo que os Dados Ficarão Armazenados</a></li>
                                        <li><a href="#secao_5" class="privacidade-link">Da Segurança dos Dados Pessoais Armazenados</a></li>
                                        <li><a href="#secao_6" class="privacidade-link">Do Compartilhamento de Dados</a></li>
                                        <li><a href="#secao_7" class="privacidade-link">Dos Cookies ou Dados de Navegação</a></li>
                                        <li><a href="#secao_8" class="privacidade-link">Do Consentimento</a></li>
                                        <li><a href="#secao_9" class="privacidade-link">Da Alteração para essa Política de Privacidade</a></li>
                                        <li><a href="#secao_10" class="privacidade-link">Do Foro</a></li>
                                    </ul>
                                    <a href="/" class="link-voltar">Retornar a página inicial &laquo;</a>
                                </div>
                                <div class="content-right">
                                    <h1 class="txt-x-bold margin-bottom-30">Política de Privacidade</h1>
                                    <section id="secao_1" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">Informações Gerais</h3>
                                        <p>
                                            A presente Política de Privacidade contém informações sobre coleta, uso, armazenamento, tratamento e proteção dos dados pessoais dos usuários e visitantes do site da empresa <strong class="txt-bold">RUBRUM</strong>, com a finalidade de demonstrar absoluta transparência e esclarecer a todos interessados sobre os tipos de dados que são coletados, os motivos da coleta e a forma como os usuários podem gerenciar ou excluir as suas informações pessoais.
                                        </p>
                                        <p>
                                            Esta Política de Privacidade aplica-se a todos os usuários e visitantes do site da <a href="http://www.rubrum.com.br">RUBRUM</a> e integra os Termos e Condições Gerais de Uso do site da empresa.
                                        </p>
                                        <p>
                                            O presente documento foi elaborado em conformidade com a Lei Geral de Proteção de Dados Pessoais (Lei 13.709/18), o Marco Civil da Internet (Lei 12.965/14) e o Regulamento da UE n. 2016/6790. Ainda, o documento poderá ser alterado em decorrência de eventual atualização normativa, razão pela qual se convida o usuário a consultar periodicamente esta seção.
                                        </p>
                                    </section>
                                    <section id="secao_2" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">Dados Pessoais</h3>
                                        <p>
                                            Os dados pessoais do usuário e visitante recolhidos são os seguintes, segue exemplos:
                                        </p>
                                        <ul class="list-terms no-margin-bottom">
                                            <li>
                                                <p>
                                                    Dados para estabelecimento de contato na plataforma: Nome, Sobrenome, Email, Telefone, empresa que trabalha e cargo/departamento;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Dados para otimização da navegação: acesso a páginas, data de entrada e saída da plataforma, palavras-chave utilizadas na busca, recomendações, comentários, endereço de IP;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Dados relacionados a contratos: diante da formalização do contrato de prestação de serviços entre a plataforma, o usuário e o visitante, poderão ser coletados e armazenados dados relativos à execução contratual, inclusive as comunicações realizadas entre a empresa e o usuário
                                                </p>
                                            </li>
                                        </ul>
                                    </section>
                                    <section id="secao_3" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">Finalidade dos Dados</h3>
                                        <p>
                                            Os dados pessoais do usuário e do visitante coletados e armazenados pela plataforma da <strong class="txt-bold">RUBRUM</strong> têm por finalidade:
                                        </p>
                                        <ul class="list-terms no-margin-bottom">
                                            <li>
                                                <p>
                                                    Bem-estar do usuário e visitante: garantir a maior qualidade no serviço oferecido, facilitar, agilizar e cumprir os compromissos estabelecidos entre o usuário e a empresa, melhorar a experiência dos usuários e fornecer funcionalidades específicas a depender das características básicas do usuário;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Acesso do usuário ao sistema: permite que sejam armazenadas informações relacionadas ao fluxo de aprovação de uma embalagem, às alterações e aos documentos inseridos (nome, horário e data são gravados);
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Previsão do perfil do usuário: tratamento automatizado de dados pessoais para avaliar o uso na plataforma;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Dados de cadastro: para permitir o acesso do usuário aos conteúdos da plataforma (exclusivo para usuários cadastrados);
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Dados de contrato: conferir às partes segurança jurídica e facilitar a conclusão do negócio;
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Garantir a completude dos dados em consultas futuras: aprovação de aspectos jurídicos, layout ou técnico e para a responsabilização em casos excepcionais.
                                                </p>
                                            </li>
                                        </ul>
                                    </section>
                                    <section id="secao_4" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">Do Tempo que os Dados Ficarão Armazenados</h3>
                                        <p>
                                            Nos termos do art. 9, §3° da Lei 13.709/18, o tratamento de dados pessoais é condição para o fornecimento dos serviços da plataforma. Nesse intuito, os dados pessoais do usuário e visitante são armazenados pela plataforma durante o período necessário para o cumprimento das finalidades previstas nesta Política, conforme o disposto no inciso I do artigo 15 da Lei 13.709/18. Nesse sentido, os dados serão armazenados até o momento que a empresa contratante da <strong>RUBRUM</strong> pare de usar seu sistema, ou seja, até que o contrato chegue ao fim.
                                        </p>
                                        <p>
                                            Os dados de ex-funcionários, se solicitados, podem ser removidos ou anonimizados mediante solicitação por meio da plataforma, geralmente por e-mail. No entanto, é importante destacar que a lei prevê exceções em que é permitida a conservação desses dados mesmo após o término do tratamento (Lei 13.709/18, art. 16), a saber: I - cumprimento de obrigações legais ou regulatórias pelo controlador, por exemplo, para fins de compliance; II - uso para pesquisa, garantindo a anonimização sempre que possível; III - transferência a terceiros, desde que respeitados os requisitos legais de tratamento de dados; e IV - uso exclusivo do controlador, com acesso vedado a terceiros e desde que os dados sejam anonimizados.
                                        </p>
                                    </section>
                                    <section id="secao_5" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">Da Segurança dos Dados Pessoais Armazenados</h3>
                                        <p>
                                            A plataforma se compromete a aplicar as medidas técnicas e organizativas aptas a proteger os dados pessoais de acesso não autorizado e de situações de destruição, perda, alteração, comunicação ou difusão de tais dados.
                                        </p>
                                        <p>
                                            É válido ressaltar que apenas pessoas autorizadas têm acesso a seus dados pessoais. Sendo assim, o acesso a tais dados é feito somente após o compromisso de confidencialidade.
                                        </p>
                                        <p>
                                            Além disso, a <strong class="txt-bold">RUBRUM</strong> se compromete a adotar as melhores posturas para evitar incidentes de segurança. Nesse sentido, a plataforma garante segurança no armazenamento dos dados, estando estes mantidos em ambiente seguro e idôneo. Todavia, na eventualidade de falhas ou danos por culpa exclusiva de terceiros, como em caso de ataque de hackers ou crackers, vírus ou por parte do próprio usuário, como no caso em que ele mesmo transfere seus dados a terceiros, a plataforma buscará, a partir das medidas cabíveis, zelar para restaurar a segurança dos dados. O site compromete-se em comunicar o usuário em caso de alguma violação de segurança dos seus dados pessoais.
                                        </p>                                            
                                        <p>
                                            Os dados pessoais armazenados são tratados com confidencialidade, dentro dos limites legais. No entanto, as informações pessoais do usuário ou visitante estão sujeitas à divulgação caso a empresa seja obrigada, mediante lei, ou caso o mesmo viole os Termos Gerais de Uso da Empresa.
                                        </p>
                                    </section>
                                    <section id="secao_6" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">Do Compartilhamento de Dados</h3>
                                        <p>
                                            Os dados pessoais podem ser compartilhados com terceiros na medida do necessário para permitir que ele realize os serviços contratados.
                                        </p>
                                        <p>
                                            Com relação aos fornecedores de serviços terceirizados, como processadores de transação de pagamento, informamos que cada qual tem sua própria política de privacidade. Desse modo, recomenda-se a leitura das suas políticas de privacidade para compreensão de quais informações pessoais serão usadas por esses fornecedores.
                                        </p>
                                    </section>
                                    <section id="secao_7" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">Dos Cookies ou Dados de Navegação</h3>
                                        <p>
                                            Os cookies referem-se a arquivos de texto enviados pela plataforma ao computador do usuário e visitante e que nele ficam armazenados, informações relacionadas à navegação no site. Tais informações são relacionadas aos dados de acesso como local e horário de acesso e são armazenados pelo navegador do usuário e visitante para que o servidor da plataforma possa lê-las posteriormente a fim de personalizar os serviços da plataforma.
                                        </p>
                                        <p>
                                            O usuário e o visitante da plataforma manifestam conhecer e aceitar que pode ser utilizado um sistema de coleta de dados de navegação mediante a utilização de cookies.
                                        </p>
                                        <p>
                                            Nosso site utiliza cookies próprios e cookies de terceiros, sendo eles os de sessão (excluídos automaticamente do computador do usuário assim que ele encerra a sessão). Utilizamos cookies a fim de garantir a utilização do site pelos usuários, que estão vinculados diretamente aos dados necessários para os fins previstos neste documento.
                                        </p>
                                    </section>
                                    <section id="secao_8" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">Do Consentimento</h3>
                                        <p>
                                            Ao utilizar os serviços e fornecer as informações pessoais na plataforma, o usuário está consentindo com a presente Política de Privacidade.
                                        </p>
                                        <p>
                                            Somente com o seu consentimento tratamos os seus dados pessoais, sendo esse consentimento caracterizado por ser a manifestação livre, informada e inequívoca pela qual você autoriza a <strong class="txt-bold">RUBRUM</strong> a tratar seus dados. Assim, em consonância com a Lei Geral de Proteção de Dados, seus dados só serão coletados, tratados e armazenados mediante prévio e expresso consentimento, evidenciando o compromisso de transparência e boa-fé da <strong class="txt-bold">RUBRUM</strong> para com seus usuários/clientes, já que segue estritamente as legislações.
                                        </p>
                                        <p>
                                            O usuário, ao cadastrar-se, manifesta conhecer e pode exercitar seus direitos de cancelar seu cadastro, acessar e utilizar seus dados pessoais e garante a veracidade das informações por ele disponibilizadas.
                                        </p>
                                        <p>
                                            O usuário tem o direito de poder, a qualquer momento, sem nenhum custo e sem fornecer nenhum motivo, solicitar informações sobre os dados relativos que são salvos em nossos sistemas. Vale ressaltar que durante esse procedimento é possível que seja solicitado alguns documentos para comprovar a sua identidade.
                                        </p>
                                        <p>
                                            Além dos direitos mencionados acima, também é considerado um direito do usuário: confirmar a existência de tratamento de dados, de maneira simplificada ou em formato claro e completo e eliminar seus dados tratados segundo seu consentimento, exceto nos casos estabelecidos em lei.
                                        </p>
                                    </section>
                                    <section id="secao_9" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">Da Alteração para essa Política de Privacidade</h3>
                                        <p>
                                            Reservamos o direito de modificar essa Política de Privacidade a qualquer momento, então, é recomendável que o usuário e visitante revisem-na ao serem notificados acerca de sua mudança.
                                        </p>
                                        <p>
                                            As alterações e esclarecimentos vão surtir efeito imediatamente após sua publicação na plataforma. Quando realizadas as alterações, os usuários serão notificados mediante as ferramentas disponíveis. Ao utilizar o serviço ou fornecer informações pessoais após eventuais modificações, o usuário e visitante demonstra sua concordância com as novas normas.
                                        </p>
                                        <p>
                                            Diante da fusão ou venda da plataforma à outra empresa, os dados dos usuários podem ser transferidos para os novos proprietários para a permanência dos serviços.
                                        </p>
                                    </section>
                                    <section id="secao_10" class="section">
                                        <h3 class="txt-x-bold margin-bottom-10">Do Foro</h3>
                                        <p>
                                            Para a solução de controvérsias decorrentes do presente instrumento, será aplicado integralmente o Direito brasileiro.
                                        </p>
                                        <p>
                                            Os eventuais litígios deverão ser apresentados no foro da comarca em que se encontra a sede da empresa, no presente caso, no foro da comarca de São Paulo.
                                        </p>
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="push"></div>
			</div>
			<div class="clear-footer"></div>
			<div class="footer clearfix">
				<div class="footer-inner content-fluid clearfix">
					<span class="copyright">Rumbrum Software &copy; {{ date("Y") }}</span>
					<ul>
						<li><a href="javascript:;" class="no-click no-hover">Versão 1.6.2</a></li>
					</ul>
					<div class="footer-right">
						<a target="_blank" href="http://www.rubrum.com.br" class="animate"></a>
					</div>
				</div>
			</div>
		</div>
		<!-- END DIV Wrapped -->
	</div>
</body>
</html>