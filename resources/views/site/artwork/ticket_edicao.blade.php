@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('artwork.ticket', $ticket->id) !!}

<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm.css')}}" />
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm-custom.css')}}" />
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('/css/angular/v-accordion.min.css')}}" />

<script>
	var id_ticket = {{ $ticket->id }};
	var csrf_token = '{{ csrf_token() }}';
</script>

<script src="{{ asset('/js/site/snap.svg-min.js') }}"></script>
<script src="{{ asset('/js/site/artwork/ticket-edit.js') }}"></script>
<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset('/js/angular/v-accordion.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload-shim.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload.min.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edicao.js') }}"></script>

<div class="content " ng-app="app" ng-controller="TicketEdicaoCtrl" ng-cloak>
	<div class="top-head" ng-cloak>
		<div class="top-inline top-packing-logo">
			{{ Html::image(asset($thumb), 'Logo', array('id' => 'imgLogo')) }}
		</div>
		<div class="top-inline top-head-title top-head-title-center top-head-title-sku-name padding-left-30 padding-right-30">
            <h2><span>{{ ($ticket->cancelado_em && $ticket->cancelado_nome_original) ? $ticket->cancelado_nome_original : $ticket->sku_nome_completo }}</span></h2>
		</div>
		<div class="top-inline top-nav-right">
			<ul>
				<li><strong class="ticket-info"><span class="icon icon-options-settings"></span> Ticket {{ $ticket->numero }}</strong></li>
				<li><strong class="ticket-info"><span class="icon icon-arrow-curve-recycle"></span> Ciclo @{{ticket.ciclo_atual.numero}}</strong></li>
				
			</ul>
		</div>
	</div>
	<div class="ticket-timeline" ng-cloak>
    	<ul class="clearfix">
			<li ng-class="{'current': ticket.id_status >= '0' && ticket.id_status < '9', 'in-progress': ticket.id_status > '0' && ticket.id_status < '9'}">Em Aberto
				<span ng-if="!((ticket.id_status == 0 || ticket.id_status == 9) && (ticket.cancelado_em || ticket.pausado_em))" class="icon-quote"></span>
				<span ng-if="(ticket.id_status == 0 || ticket.id_status == 9) && ticket.cancelado_em" class="icon-cross">
					Cancelado
					<i class="paused-canceled-by">
						<small>@{{ ticket.cancelado_por_nome }}</small>
						<small>@{{ ticket.cancelado_em }}</small>
					</i>
				</span>
				<div ng-if="(ticket.id_status == 0 || ticket.id_status == 9) && ticket.pausado_em" class="dropdown">
					<a href="javascript:;" class="status-pausado dropdown-btn icon-pause"></a>
					Pausado
					<div class="dropdown-menu">
						<ul>
							<li><a ng-click="despausar_ticket()" href="javascript:;"><span class="dropdown-item-ico icon-play"></span> Reativar Ticket</a></li>
						</ul>
					</div>
					<i class="paused-canceled-by">
						<small>@{{ ticket.pausado_por_nome }}</small>
						<small>@{{ ticket.pausado_em }}</small>
					</i>
				</div>
			</li>
			<li ng-class="{'current': ticket.id_status >= '1' && ticket.id_status < '9', 'in-progress': ticket.id_status > '1' && ticket.id_status < '9'}">Em Edição
				<span ng-if="!(ticket.id_status == 1 && (ticket.cancelado_em || ticket.pausado_em))" class="icon-three-points"></span>
				<span ng-if="ticket.id_status == 1 && ticket.cancelado_em" class="icon-cross">
					Cancelado
					<i class="paused-canceled-by">
						<small>@{{ ticket.cancelado_por_nome }}</small>
						<small>@{{ ticket.cancelado_em }}</small>
					</i>
				</span>
				<div ng-if="ticket.id_status == 1 && ticket.pausado_em" class="dropdown">
					<a href="javascript:;" class="status-pausado dropdown-btn icon-pause"></a>
					Pausado
					<div class="dropdown-menu">
						<ul>
							<li><a ng-click="despausar_ticket()" href="javascript:;"><span class="dropdown-item-ico icon-play"></span> Reativar Ticket</a></li>
						</ul>
					</div>
					<i class="paused-canceled-by">
						<small>@{{ ticket.pausado_por_nome }}</small>
						<small>@{{ ticket.pausado_em }}</small>
					</i>
				</div>
			</li>
			<li ng-class="{'current': ticket.id_status >= '2' && ticket.id_status < '9', 'in-progress': ticket.id_status > '2' && ticket.id_status < '9'}">Em Revisão
				<span ng-if="!(ticket.id_status == 2 && (ticket.cancelado_em || ticket.pausado_em))" class="icon-list-square"></span>
				<span ng-if="ticket.id_status == 2 && ticket.cancelado_em" class="icon-cross">
					Cancelado
					<i class="paused-canceled-by">
						<small>@{{ ticket.cancelado_por_nome }}</small>
						<small>@{{ ticket.cancelado_em }}</small>
					</i>
				</span>
				<div ng-if="ticket.id_status == 2 && ticket.pausado_em" class="dropdown">
					<a href="javascript:;" class="status-pausado dropdown-btn icon-pause"></a>
					Pausado
					<div class="dropdown-menu">
						<ul>
							<li><a ng-click="despausar_ticket()" href="javascript:;"><span class="dropdown-item-ico icon-play"></span> Reativar Ticket</a></li>
						</ul>
					</div>
					<i class="paused-canceled-by">
						<small>@{{ ticket.pausado_por_nome }}</small>
						<small>@{{ ticket.pausado_em }}</small>
					</i>
				</div>
			</li>
			<li ng-show='ticket.count_marketing > 0' ng-class="{'current': ticket.id_status >= '3' && ticket.id_status < '9', 'in-progress': ticket.id_status > '3' && ticket.id_status < '9'}">Marketing
				<span ng-if="!(ticket.id_status == 3 && (ticket.cancelado_em || ticket.pausado_em))" class="icon-cube"></span>
				<span ng-if="ticket.id_status == 3 && ticket.cancelado_em" class="icon-cross">
					Cancelado
					<i class="paused-canceled-by">
						<small>@{{ ticket.cancelado_por_nome }}</small>
						<small>@{{ ticket.cancelado_em }}</small>
					</i>
				</span>
				<div ng-if="ticket.id_status == 3 && ticket.pausado_em" class="dropdown">
					<a href="javascript:;" class="status-pausado dropdown-btn icon-pause"></a>
					Pausado
					<div class="dropdown-menu">
						<ul>
							<li><a ng-click="despausar_ticket()" href="javascript:;"><span class="dropdown-item-ico icon-play"></span> Reativar Ticket</a></li>
						</ul>
					</div>
					<i class="paused-canceled-by">
						<small>@{{ ticket.pausado_por_nome }}</small>
						<small>@{{ ticket.pausado_em }}</small>
					</i>
				</div>
			</li>
			<li ng-class="{'current': ticket.id_status >= '4' && ticket.id_status < '9', 'in-progress': ticket.id_status > '4' && ticket.id_status < '9'}">Em Aprovação
				<span ng-if="!(ticket.id_status == 4 && (ticket.cancelado_em || ticket.pausado_em))" class="icon-text-justify-center"></span>
				<span ng-if="ticket.id_status == 4 && ticket.cancelado_em" class="icon-cross">
					Cancelado
					<i class="paused-canceled-by">
						<small>@{{ ticket.cancelado_por_nome }}</small>
						<small>@{{ ticket.cancelado_em }}</small>
					</i>
				</span>
				<div ng-if="ticket.id_status == 4 && ticket.pausado_em" class="dropdown">
					<a href="javascript:;" class="status-pausado dropdown-btn icon-pause"></a>
					Pausado
					<div class="dropdown-menu">
						<ul>
							<li><a ng-click="despausar_ticket()" href="javascript:;"><span class="dropdown-item-ico icon-play"></span> Reativar Ticket</a></li>
						</ul>
					</div>
					<i class="paused-canceled-by">
						<small>@{{ ticket.pausado_por_nome }}</small>
						<small>@{{ ticket.pausado_em }}</small>
					</i>
				</div>
			</li>
            <li ng-class="{'current': ticket.id_status >= '5' && ticket.id_status < '9', 'in-progress': (ticket.qtdEnvioArtes && ticket.envio_finalizado) || (ticket.id_status > '5' && ticket.id_status < '9') }">Aprovado
				<span ng-if="!(ticket.id_status == 5 && (ticket.cancelado_em || ticket.pausado_em))" class="icon-bag"></span>
				<span ng-if="ticket.id_status == 5 && ticket.cancelado_em" class="icon-cross">
					Cancelado
					<i class="paused-canceled-by">
						<small>@{{ ticket.cancelado_por_nome }}</small>
						<small>@{{ ticket.cancelado_em }}</small>
					</i>
				</span>
				<div ng-if="ticket.id_status == 5 && ticket.pausado_em" class="dropdown">
					<a href="javascript:;" class="status-pausado dropdown-btn icon-pause"></a>
					Pausado
					<div class="dropdown-menu">
						<ul>
							<li><a ng-click="despausar_ticket()" href="javascript:;"><span class="dropdown-item-ico icon-play"></span> Reativar Ticket</a></li>
						</ul>
					</div>
					<i class="paused-canceled-by">
						<small>@{{ ticket.pausado_por_nome }}</small>
						<small>@{{ ticket.pausado_em }}</small>
					</i>
				</div>
			</li>
			<li ng-show="ticket.qtdEnvioArtes" ng-class="{'current': ticket.envio_finalizado, 'in-progress': ticket.id_status == '6'}">Enviado
				<span ng-if="!(ticket.id_status == 6 && (ticket.cancelado_em || ticket.pausado_em))" class="icon-air-plane"></span>
				<span ng-if="ticket.id_status == 6 && ticket.cancelado_em" class="icon-cross">
					Cancelado
					<i class="paused-canceled-by">
						<small>@{{ ticket.cancelado_por_nome }}</small>
						<small>@{{ ticket.cancelado_em }}</small>
					</i>
				</span>
				<div ng-if="ticket.id_status == 6 && ticket.pausado_em" class="dropdown">
					<a href="javascript:;" class="status-pausado dropdown-btn icon-pause"></a>
					Pausado
					<div class="dropdown-menu">
						<ul>
							<li><a ng-click="despausar_ticket()" href="javascript:;"><span class="dropdown-item-ico icon-play"></span> Reativar Ticket</a></li>
						</ul>
					</div>
					<i class="paused-canceled-by">
						<small>@{{ ticket.pausado_por_nome }}</small>
						<small>@{{ ticket.pausado_em }}</small>
					</i>
				</div>
			</li>
			<li ng-class="{'current': ticket.id_status >= '6' && ticket.id_status < '9', 'in-progress': ticket.id_status > '6' && ticket.id_status < '9'}">Encerrado
				<span ng-if="!(ticket.id_status == 6 && (ticket.cancelado_em || ticket.pausado_em))" class="icon-check"></span>
				<span ng-if="ticket.id_status == 6 && ticket.cancelado_em" class="icon-cross">
					Cancelado
					<i class="paused-canceled-by">
						<small>@{{ ticket.cancelado_por_nome }}</small>
						<small>@{{ ticket.cancelado_em }}</small>
					</i>
				</span>
				<div ng-if="ticket.id_status == 6 && ticket.pausado_em" class="dropdown">
					<a href="javascript:;" class="status-pausado dropdown-btn icon-pause"></a>
					Pausado
					<div class="dropdown-menu">
						<ul>
							<li><a ng-click="despausar_ticket()" href="javascript:;"><span class="dropdown-item-ico icon-play"></span> Reativar Ticket</a></li>
						</ul>
					</div>
					<i class="paused-canceled-by">
						<small>@{{ ticket.pausado_por_nome }}</small>
						<small>@{{ ticket.pausado_em }}</small>
					</i>
				</div>
			</li>
		</ul>
	</div>
	<div class="ticket-container" ng-cloak>
		<form name="formTicketEdit" action="" method="POST" class="ticket-alt-request-form">
			<ul class="ticket-editor-tabs clearfix">
				<li><a ng-click="tab=1" ng-class="{'current' : tab==1}" href="javascript:;">Visão Geral</a></li>
				<li><a ng-click="tab=2" ng-class="{'current' : tab==2}" href="javascript:;">Anexos</a></li>
				<li><a ng-click="tab=3" ng-class="{'current' : tab==3}" href="javascript:;">Mensagens Gerais</a></li>
				<li><a ng-click="tab=6" ng-class="{'current' : tab==6, 'disabled': ticket.cancelado_em}" href="javascript:;">Opções</a></li>
			</ul>
			<div class="tabs-container">
				<div class="tab-content" ng-show="tab == 1">
					<section class="initial-info-section clearfix">
						<table border="0" cellpadding="0" cellspacing="0">
							<thead>
								<tr>
									<th width="420">Informações Iniciais</th>
									<th width="80">Data de Início</th>
									<th width="130">Envio para Aprovação</th>
									<th width="130">Entrega no Fornecedor </th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td width="420" >{!! $ticket->descricao_html !!}</td>
									<td width="80">{{ ($ticket->dt_ini) ? $ticket->dt_ini->format('d/m/Y') : '' }}</td>
									<td width="130">{{ ($ticket->dt_envio) ? $ticket->dt_envio->format('d/m/Y') : '' }}</td>
									<td width="130">{{ ($ticket->dt_fim) ? $ticket->dt_fim->format('d/m/Y') : '' }}</td>
								</tr>
							</tbody>
						</table>
						<div ng-if="ticket.cancelado_motivo" style="margin-bottom:30px;" class="text-block">
							<h4 style="margin-bottom:10px;">Motivo do Cancelamento</h4>
                            <p ng-bind-html="ticket.cancelado_motivo"></p>
						</div>
					</section>
					<section class="membership-info-section clearfix">
						<hr />
						<table border="0" cellpadding="0" cellspacing="0" class="membership-table">
							<thead>
								<tr>
									<th width="340">Participantes</th>
									<th width="720">Aprovação</th>
									<th width="172">Enviar Lembrete</th>
									<th width="172">Renovar Aprovação</th>
								</tr>
							</thead>
							<tbody ng-repeat="(p, participante) in ticket.participantes" class="members">
								<tr>
									<td width="340">
										<span ng-show="ticket.id_usuario === participante.id_usuario" class="icon-is-owner icon-star"></span>
										<span ng-show="ticket.id_usuario !== participante.id_usuario" class="icon-is-member icon-play"></span>
										@{{participante.nome_usuario}}
									</td>
									<td width="720">
										<div ng-show="!participante.aprovado &&  !participante.participante_renovacao_nome">
											<span ng-show="(ticket.id_status > 1 && participante.id_perfil === 5) || (ticket.id_status > 2 && participante.id_perfil === 3)" class="icon-status icon-is-ok"></span>
											<span ng-show="participante.id_perfil === 4" class="icon-watcher icon-social-flickr"></span>
											@{{participante.nome_perfil}}
										</div>
										<div ng-show="participante.id_perfil === 5 && participante.aprovado">
											<span class="icon-status icon-is-ok"></span>Layout enviado às @{{participante.data_atualizacao}}<span ng-show="ticket.envio_finalizado">, carregou a arte-final às @{{ticket.finalizador.data}}</span>
										</div>
										<div ng-show="participante.id_perfil === 3 && participante.aprovado">
											<span class="icon-status icon-is-ok"></span>Revisado às @{{participante.data_atualizacao}}
										</div>
										<div ng-show="(participante.id_perfil === 2) && participante.aprovado === 1">
											<div ng-show="!participante.membro_aprovador">
												<span class="icon-status icon-is-ok"></span>Aprovado no marketing às @{{participante.data_atualizacao}}
											</div>
											<div ng-show="participante.membro_aprovador">
												<span class="icon-status icon-is-ok"></span>Aprovado
											</div>
										</div>
										<div ng-show="(participante.id_perfil === 2) && participante.aprovado === 2">
											<div ng-show="!participante.membro_aprovador">
												<span class="icon-status icon-not-ok"></span>Reprovado no marketing às @{{participante.data_atualizacao}}
											</div>
											<div ng-show="participante.membro_aprovador">
												<span class="icon-status icon-not-ok"></span>Reprovado
											</div>
										</div>
										<div ng-show="(participante.id_perfil === 1) && participante.aprovado === 1">
											<div ng-show="!participante.membro_aprovador">
												<span class="icon-status icon-is-ok"></span>Aprovado às @{{participante.data_atualizacao}}
											</div>
											<div ng-show="participante.membro_aprovador">
												<span class="icon-status icon-is-ok"></span>Aprovado
											</div>
										</div>
										<div ng-show="(participante.id_perfil === 1) && participante.aprovado === 2">
											<div ng-show="!participante.membro_aprovador">
												<span class="icon-status icon-not-ok"></span>Reprovado às @{{participante.data_atualizacao}}
											</div>
											<div ng-show="participante.membro_aprovador">
												<span class="icon-status icon-not-ok"></span>Reprovado
											</div>
										</div>
                                        <div ng-show="participante.aprovado === 0 && participante.participante_renovacao_nome">
											<div>
												<span class="icon-status icon-arrow-curve-recycle icon-status-renewed"></span>Aprovação renovada por @{{ participante.participante_renovacao_nome }} às @{{ participante.data_atualizacao }}
											</div>
										</div>
                                        <div ng-show="(participante.id_perfil === 6) && participante.aprovado === 1">
											<div>
												<span class="icon-status icon-is-ok"></span>Enviado ao fornecedor às @{{ticket.finalizador.data}}
											</div>
										</div>
                                        <div ng-show="participante.id_perfil === null">
											<div>
												<span class="icon-status icon-is-ok"></span>Encerrado às @{{ participante.finalizado_em }}
											</div>
										</div>
                                    </td>
									<td width="172">
										<a href="javascript:;" ng-click="enviarLembrete(participante.id_participante, participante.id_perfil, participante.enviadas);" href="#" class="icon icon-mail" ng-class="{'send-notification': participante.envia_notificacao && !participante.enviadas, 'send-notification-ok': participante.envia_notificacao && participante.enviadas, 'send-notification-disabled': !participante.envia_notificacao}"></a>
									</td>
									<td width="170">
                                        <a
                                            ng-click="renovarAprovacao(participante)"
                                            ng-class="{'send-notification': participante.aprovado == 1 && ticket.id_status < 4 && ticket.ciclo_atual.numero > 1, 'send-notification-disabled': participante.aprovado != 1 || ticket.id_status >= 4}"
                                            ng-if="participante.id_perfil == 1 && ticket.id_usuario === {{ Auth::user()->id }}"
                                            class="icon icon-arrow-curve-recycle"
                                            href="javascript:;"></a>
                                    </td>
								</tr>
								<tr ng-repeat="(m, membro) in participante.equipe" class="team-members-sub">
									<td width="340"><span class="icon-status icon-arrow-curve-right"></span>@{{membro.nome_usuario}}</td>
									<td width="720">
										<span ng-show="!participante.aprovado">@{{participante.nome_perfil}}</span>
										<div ng-show="participante.id_perfil === 2 && participante.aprovado === 1 && participante.membro_aprovador === membro.id_membro">
											<span class="icon-status icon-is-ok zero-padding"></span>Aprovado no marketing às @{{participante.data_atualizacao}}
										</div>
										<div ng-show="participante.id_perfil === 2 && participante.aprovado === 1 && participante.membro_aprovador !== membro.id_membro">
											<span class="icon-status icon-is-ok zero-padding"></span>Aprovado
										</div>
										<div ng-show="participante.id_perfil === 2 && participante.aprovado === 2 && participante.membro_aprovador === membro.id_membro">
											<span class="icon-status icon-not-ok zero-padding"></span>Reprovado no marketing às @{{participante.data_atualizacao}}
										</div>
										<div ng-show="participante.id_perfil === 2 && participante.aprovado === 2 && participante.membro_aprovador !== membro.id_membro">
											<span class="icon-status icon-not-ok zero-padding"></span>Reprovado
										</div>
										<div ng-show="participante.id_perfil === 1 && participante.aprovado === 1 && participante.membro_aprovador === membro.id_membro">
											<span class="icon-status icon-is-ok zero-padding"></span>Aprovado às @{{participante.data_atualizacao}}
										</div>
										<div ng-show="participante.id_perfil === 1 && participante.aprovado === 1 && participante.membro_aprovador !== membro.id_membro">
											<span class="icon-status icon-is-ok zero-padding"></span>Aprovado
										</div>
										<div ng-show="participante.id_perfil === 1 && participante.aprovado === 2 && participante.membro_aprovador === membro.id_membro">
											<span class="icon-status icon-not-ok zero-padding"></span>Reprovado às @{{participante.data_atualizacao}}
										</div>
										<div ng-show="participante.id_perfil === 1 && participante.aprovado === 2 && participante.membro_aprovador !== membro.id_membro">
											<span class="icon-status icon-not-ok zero-padding"></span>Reprovado
										</div>
									</td>
									<td width="172">
										<a href="javascript:;" ng-click="enviarLembrete(membro.id_membro, participante.id_perfil, participante.enviadas);" class="icon icon-mail" ng-class="{'send-notification-ok': participante.envia_notificacao && participante.enviadas, 'send-notification': participante.envia_notificacao && !participante.enviadas, 'send-notification-disabled': !participante.envia_notificacao}"></a>
									</td>
									<td width="172"></td>
								</tr>
							</tbody>
						</table>
					</section>
					<section class="alteration-cicle-section clearfix">
						<hr />
						<h4>Alterações do Ciclo @{{ticket.ciclo_atual.numero}}</h4>
						<div class="alter-cicle-wrap">
							<div class="alteration-cicle-row clearfix" ng-repeat="(s, solicitacao) in ticket.ciclo_atual.solicitacoes">
								<div class="box-inline col-member-description">
									<span ng-class='solicitacao.icone' class="icon"></span>
									<div class="description-txt clearfix">
										<h4>@{{solicitacao.tipo}}</h4>
										<span>@{{solicitacao.nome_usuario}}</span>
										<br/>
										<span>@{{solicitacao.data_criacao}}</span>
										<br/>
									</div>
								</div>
								<div class="box-inline col-general-description">
									<p ng-bind-html="solicitacao.descricao"></p>
									<div class="chat-comment-container">
										<button ng-show="ticket.id_status < 3 && solicitacao.id_status === 1 && ticket.libera_ticket" ng-click='exibe_comentar(s)' type="button" class="comments-btn">
											<span class="btn-icon icon-marker-points"></span> Comentar
										</button>
										<div ng-show='exibe_comentar[s]' class="chat-comment-box clearfix">
											<div contenteditable="true" ng-model="comentarios[s]" ng-init="comentarios[s] = ''" ng-paste="msg_copy_paste_handler($event, 'comentarios', s)" class="textbox-fake msgBox"></div>
											<div class="chat-comment-box-ctrl">
												<div class="tooltip attach-file">
													<span class="icon-paperclip-oblique"></span>
													<input data-id="@{{s}}" onchange="angular.element(this).scope().msg_attach_file(this, 'comentarios');" class="msgAttachment" type="file" />
													<span class="tooltip-box">Anexar imagem no texto</span>
												</div>
												<a href="javascript:;" ng-click="enviar_msg_email(solicitacao.id_usuario);" class="tooltip send-by-mail">
													<span class="icon-mail"></span>
													<span class="tooltip-box">Enviar também por email</span>
												</a>
												<button ng-click="comentar(solicitacao.id, s)" type="button" class="tooltip add-comments-btn">
													<span class="icon-arrow-right-light"></span>
													<span class="tooltip-box">Enviar comentário</span>
												</button>
											</div>
										</div>
										<ul class="chat-comment-list">
											<li ng-repeat="(c, comentario) in solicitacao.comentarios">
												<hr />
												<div class="comment">
													<h4 style="color: @{{comentario.cor}}">@{{comentario.nome_usuario}} às @{{comentario.data_criacao}}</h4>
													<p ng-bind-html="comentario.texto"></p>
												</div>
												<button ng-show="ticket.id_status < 3 && solicitacao.id_status === 1 && ticket.libera_ticket" ng-click='exibe_responder($id)' type="button" class="reply-btn">
													<span class="icon-arrow-curve-right"></span> Responder
												</button>
												<div ng-show='exibe_responder[$id]' class="chat-comment-box clearfix">
													<div contenteditable="true" ng-focus="interval_tickets_stop()" ng-model="respostas[$id]" ng-init="respostas[$id] = ''" ng-paste="msg_copy_paste_handler($event, 'respostas', $id)" class="textbox-fake"></div>
													<div class="chat-comment-box-ctrl">
														<div class="tooltip attach-file">
															<span class="icon-paperclip-oblique"></span>
															<input data-id="@{{$id}}" onchange="angular.element(this).scope().msg_attach_file(this, 'respostas');" name="msgAttachment" class="msgAttachment" type="file" />
															<span class="tooltip-box">Anexar imagem no texto</span>
														</div>
														<a href="javascript:;" ng-click="enviar_msg_email(comentario.id_usuario);" class="tooltip send-by-mail">
															<span class="icon-mail"></span>
															<span class="tooltip-box">Enviar também por email</span>
														</a>
														<button ng-click="responder(comentario.id, $id)" type="button" class="tooltip add-comments-btn">
															<span class="icon-arrow-right-light"></span>
															<span class="tooltip-box">Enviar resposta</span>
														</button>
													</div>
												</div>
												<ul>
													<li ng-repeat="(c, resposta) in comentario.respostas">
														<div class="comment">
															<h4 style="color: @{{resposta.cor}}">@{{resposta.nome_usuario}} às @{{resposta.data_criacao}}</h4>
															<p ng-bind-html="resposta.texto"></p>
														</div>
													</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
								<div ng-show="ticket.id_status != 2 || ticket.libera_edicao === 0" class="box-inline col-edition-status">
									<div class="chk-edition-control clearfix disabled">
										<input type="checkbox" disabled name="chkEditionIsOk[]" id="chkEditionIsOk_1" value="" class="check-edit-status" />
										<label for="chkEditionIsOk_1" class="chk-edition-fake" ng-class="{'not-ok': solicitacao.id_status === 1, 'is-ok': solicitacao.id_status === 2}"></label>
										<label for="chkEditionIsOk_1" class="chk-edition-txt">@{{solicitacao.status}}</label>
									</div>
									<div ng-show="solicitacao.status_usuario" class="checked-info">
										<span>@{{solicitacao.status_usuario}}</span>
										<span>@{{solicitacao.status_data}}</span>
									</div>
								</div>
								<div ng-show="ticket.id_status === 2 && ticket.libera_edicao === 1" class="box-inline col-edition-status">
									<div ng-mouseover="interval_tickets_stop()" ng-mouseleave="interval_tickets_start()" class="chk-edition-control clearfix">
										<input type="checkbox" name="chkEditionIsOk[]" id="chkEditionIsOk_1" value="" class="check-edit-status">
										<label for="chkEditionIsOk_1" class="chk-edition-fake" ng-class="{'not-ok': solicitacao.id_status === 1, 'is-ok': solicitacao.id_status === 2}"></label>
										<label for="chkEditionIsOk_1" class="chk-edition-txt">@{{solicitacao.status}}</label>
										<div class="chk-edition-submenu">
											<h4>Alterar status</h4>
											<ul>
												<li>
													<a mw-confirm-click="acao_solicitacao(solicitacao.id, 2)" mw-confirm-click-message="Deseja alterar o status para Revisado?" class="status-revisado" ng-show="solicitacao.id_status === 1" href=""><span class="icon-check"></span> Revisado</a>
													<a mw-confirm-click="acao_solicitacao(solicitacao.id, 1)" mw-confirm-click-message="Deseja alterar o status para Em revisão?" class="status-edicao" ng-show="solicitacao.id_status === 2" href=""><span class="icon-three-points"></span> Em Edição</a>
												</li>
											</ul>
										</div>
									</div>
									<div ng-show="solicitacao.status_usuario" class="checked-info">
										<span>@{{solicitacao.status_usuario}}</span>
										<span>@{{solicitacao.status_data}}</span>
									</div>
								</div>
							</div>
						</div>
						<v-accordion>
							<v-pane ng-repeat="(c, ciclo) in ticket.ciclos_anteriores" expanded="pane.isExpanded" class="vpane-hspacer">
								<v-pane-header>
									<div class="cicle-accordion">
										<h4 ng-click="interval_tickets_stop()" ng-if="!pane.isExpanded">
											<a class='teste-clique'><span class="icon-plus"></span><i class="accordion-state-txt">Exibir</i> alterações do Ciclo @{{ciclo.numero}}</a>
										</h4>
										<h4 ng-click="interval_tickets_start()" ng-if="pane.isExpanded">
											<a class='teste-clique'><span class="icon-minus"></span><i class="accordion-state-txt">Ocultar</i> alterações do Ciclo @{{ciclo.numero}}</a>
										</h4>
									</div>
								</v-pane-header>
								<v-pane-content>
									<div class="cicle-accordion">
										<section class="cicle-accordion-section">
											<div class="alteration-cicle-row clearfix" ng-repeat="(s, solicitacao) in ciclo.solicitacoes">
												<div class="box-inline col-member-description">
													<span ng-class='solicitacao.icone' class="icon"></span>
													<div class="description-txt clearfix">
														<h4>@{{solicitacao.tipo}}</h4>
														<span>@{{solicitacao.nome_usuario}}</span>
														<br/>
														<span>@{{solicitacao.data_criacao}}</span>
														<br/>
													</div>
												</div>
												<div class="box-inline col-general-description">
													<p ng-bind-html="solicitacao.descricao"></p>
													<div class="chat-comment-container">
														<ul class="chat-comment-list">
															<li ng-repeat="(c, comentario) in solicitacao.comentarios">
																<hr />
																<div class="comment">
																	<h4 style="color: @{{comentario.cor}}">@{{comentario.nome_usuario}} às @{{comentario.data_criacao}}</h4>
																	<p ng-bind-html="comentario.texto"></p>
																</div>
																<ul>
																	<li ng-repeat="(c, resposta) in comentario.respostas">
																		<div class="comment">
																			<h4 style="color: @{{resposta.cor}}">@{{resposta.nome_usuario}} às @{{resposta.data_criacao}}</h4>
																			<p ng-bind-html="resposta.texto"></p>
																		</div>
																	</li>
																</ul>
															</li>
														</ul>
													</div>
												</div>
												<div class="box-inline col-edition-status">
													<div class="chk-edition-control disabled clearfix">
														<input type="checkbox" disabled name="chkEditionIsOk[]" id="chkEditionIsOk_1" checked class="check-edit-status" />
														<label for="chkEditionIsOk_1" class="chk-edition-fake" ng-class="{'not-ok': solicitacao.id_status === 1, 'is-ok': solicitacao.id_status === 2}"></label>
														<label for="chkEditionIsOk_1" class="chk-edition-txt">@{{solicitacao.status}}</label>
													</div>
													<div class="checked-info is-visible">
														<span>@{{solicitacao.status_usuario}}</span>
														<span>@{{solicitacao.status_data}}</span>
													</div>
												</div>
											</div>
										</section>
									</div>
								</v-pane-content>
							</v-pane>
						</v-accordion>
					</section>
					<section ng-show="((ticket.ciclo_atual.layout && ticket.libera_edicao) || (ticket.ciclo_atual.layout && ticket.id_status > 1)) || (ticket.arteFinalista && !step3) || (ticket.ciclo_anterior.layout) || (ticket.versao_atual)"
						class="update-attachment-section clearfix">
						<hr />
						<div class="update-attachment-column">
							<h4 ng-if="!ticket.ciclo_atual.layout && !ticket.libera_edicao || ticket.arteFinalista && !step3 || (ticket.ciclo_atual.layout && ticket.libera_edicao) || (ticket.ciclo_atual.layout && ticket.id_status > 1)">Layout Atualizado</h4>
							<div class="updated-version-container">
								<div ng-show="ticket.arteFinalista && !step3" style="margin-bottom: 20px;" class="box-inline file-attachment-update file-attachment-control clearfix">
									<span class="icon-paperclip-oblique"></span>
									<input type="file" ngf-select ng-model="layout" name="file" ngf-model-invalid="errorFile">
									<span class="file-placeholder"></span>
								</div>
								<div ng-show="layout.progress > 0" class="progress-circle">
									<svg id="svg" viewbox="0 0 100 100">
										<circle cx="50" cy="50" r="45" fill="#004192"/>
										<path fill="none" stroke-linecap="round" stroke-width="5" stroke="#fff" stroke-dasharray="@{{layout.progress*2.5}},250.2" d="M50 10 a 40 40 0 0 1 0 80 a 40 40 0 0 1 0 -80"/>
										<text id="count" class="progress" x="50" y="50" text-anchor="middle" dy="7" font-size="20" fill="#fff" ng-bind="layout.progress + '%'"></text>
									</svg>
								</div>
								<button ng-show="ticket.arteFinalista && !step3" ng-click="uploadLayout(ticket.ciclo_atual.id)" type="button" style="margin-bottom: 20px;" class="btn-upload">
									<span class="btn-icon icon-upload"></span> Carregar
								</button>
								<div ng-if="!ticket.ciclo_atual.layout && !ticket.libera_edicao" class="panel-wait-msg">
									<span><i class="icon-axis-rules"></i>Aguardando novo layout...</span>
								</div>
								<div ng-if="(ticket.ciclo_atual.layout && ticket.libera_edicao) || (ticket.ciclo_atual.layout && ticket.id_status > 1)" ng-class="{'updated-version-wrap': ticket.ciclo_anterior.layout}">
									<ul	class="box-layout-atual previous-version-list">
										<li>
											<div tooltips tooltips tooltip-template="Opções" tooltip-side="top" tooltip-size="small" class="dropdown pull-right">
												<a href="javascript:;" class="dropdown-btn table-icon-td icon-three-points"></a>
												<div class="dropdown-menu">
													<ul>
														<li ng-if="ticket.comparacao.length >= 2"><a ng-click="comparar_artes()" href="javascript:;"><span class="dropdown-item-ico icon-copy-document"></span>Comparar PDFs</a></li>
														<li><a target="_blank" ng-href="@{{ticket.ciclo_atual.layout.baixarPdf}}"><span class="dropdown-item-ico icon-download"></span>Baixar PDF</a></li>
													</ul>
												</div>
											</div>
											<a target="_blank" ng-href="@{{ ticket.ciclo_atual.layout.path }}"><span class="icon-axis-rules"></span></a>
											<div class="previous-version-txt">
												<h4><a target="_blank" ng-href="@{{ticket.ciclo_atual.layout.path}}">@{{ticket.ciclo_atual.layout.nome}}</a></h4>
												<small>Enviado por @{{ticket.ciclo_atual.layout.nome_usuario}} às @{{ticket.ciclo_atual.layout.data_criacao}}</small>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="update-attachment-column">
							<div class="previous-version-container">
								<div ng-show="ticket.ciclo_anterior.layout" id="reprovado" class="previous-version-wrap clearfix">
									<h4>Ciclo Reprovado</h4>
									<ul class="box-reprovado previous-version-list clearfix">
										<li>
											<div tooltips tooltips tooltip-template="Opções" tooltip-side="top" tooltip-size="small" class="dropdown pull-right">
												<a href="javascript:;" class="dropdown-btn table-icon-td icon-three-points"></a>
												<div class="dropdown-menu">
													<ul>
														<li><a target="_blank" ng-href="@{{ticket.ciclo_anterior.layout.baixarPdf}}"><span style="color:#004192" class="dropdown-item-ico icon-download"></span>Baixar PDF</a></li>
													</ul>
												</div>
											</div>
											<a ng-href="@{{ticket.ciclo_anterior.layout.path}}"><span class="icon-axis-rules"></span></a>
											<div class="previous-version-txt">
												<h4><a target="_blank" ng-href="@{{ticket.ciclo_anterior.layout.path}}">@{{ticket.ciclo_anterior.layout.nome}}</a></h4>
												<small>Enviado por @{{ticket.ciclo_anterior.layout.nome_usuario}} às @{{ticket.ciclo_anterior.layout.data_criacao}}</small>
											</div>
										</li>
									</ul>
								</div>
								<div ng-show="ticket.versao_atual" id="versao-mercado" class="previous-version-wrap clearfix">
									<h4>Versão de Mercado</h4>
									<ul class="box-versao-mercado previous-version-list clearfix">
										<li>
											<div tooltips tooltips tooltip-template="Opções" tooltip-side="top" tooltip-size="small" class="dropdown pull-right">
												<a href="javascript:;" class="dropdown-btn table-icon-td icon-three-points"></a>
												<div class="dropdown-menu">
													<ul>
														<li><a target="_blank" ng-href="@{{ticket.versao_atual.baixarPdf}}"><span class="dropdown-item-ico icon-download"></span>Baixar PDF</a></li>
													</ul>
												</div>
											</div>
											<a target="_blank" ng-href="@{{ticket.versao_atual.path}}"><span class="icon-axis-rules"></span></a>
											<div class="previous-version-txt">
												<h4><a target="_blank" ng-href="@{{ticket.versao_atual.path}}">@{{ticket.versao_atual.nome}}</a></h4>
												<small>Enviado por @{{ticket.versao_atual.nome_usuario}} às @{{ticket.versao_atual.data_criacao}}</small>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</section>
					<section class="artwork-upload-section margin-bottom-30 clearfix" ng-show="ticket.id_status === 5 && ticket.arteFinalista">
						<hr />
						<div class="file-attachment-container clearfix" ng-show="ticket.libera_edicao && ticket.arteFinalista">
							<h4 style="margin: 0px 0px 15px 0px; padding: 0px;">Carregar Arte-final</h4>
							<div class="box-inline file-attachment-control file-attachment-col clearfix" style="width:868px;margin-right:15px;">
								<span class="icon-paperclip-oblique"></span></span>
								<input type="file" ngf-select ng-model="arte" name="file" ngf-model-invalid="errorFile">
                            	<span class="file-placeholder">Procurar...</span>
							</div>
                            <div ng-show="arte.progress > 0" class="progress-circle">
                                <svg id="svg" viewbox="0 0 100 100">
                                    <circle cx="50" cy="50" r="45" fill="#004192"/>
                                    <path fill="none" stroke-linecap="round" stroke-width="5" stroke="#fff" stroke-dasharray="@{{arte.progress*2.5}},250.2" d="M50 10 a 40 40 0 0 1 0 80 a 40 40 0 0 1 0 -80"/>
                                    <text id="count" class="progress" x="50" y="50" text-anchor="middle" dy="7" font-size="20" fill="#fff" ng-bind="arte.progress + '%'"></text>
                                </svg>
                            </div>
							<button ng-click="uploadArte(ticket.id)" type="button" class="btn-upload">
								<span class="btn-icon icon-upload"></span> Carregar
							</button>
						</div>
					</section>
					<section class="artwork-upload-section clearfix" ng-show="ticket.id_status === 5 && (ticket.arteFinalista || ((ticket.aprovador || ticket.revisor || ticket.participanteEnvio) && ticket.envio_autorizado)) && ticket.artes.length">
						<hr />
						<div class="send-artwork-container margin-bottom-30 clearfix">
							<h4 ng-show="!ticket.envio_autorizado" style="margin: 0px 0px 15px 0px; padding: 0px;">Arte-final</h4>
							<h4 ng-show="ticket.envio_autorizado" style="margin: 0px 0px 15px 0px; padding: 0px;">Enviar Arte-final</h4>
							<div  ng-show="ticket.envio_autorizado && ticket.participanteEnvio" class="send-artwork-txt-info">
								<p ng-show="ticket.envio_autorizado && ticket.participanteEnvio">
									Selecione o fornecedor ou digite o e-mail para enviar a arte-final. Uma cópia do PDF de aprovação também será enviado automaticamente.
								</p>
							</div>
							<div ng-repeat="(a, arte) in ticket.artes" class="send-artwork-group no-border no-padding no-bgcolor no-animate clearfix" style="margin-bottom:24px;">
								<div class="box-arte-final send-artwork-col margin-bottom-30">
									<ul class="send-artwork-list">
										<li>
											<div tooltips tooltips tooltip-template="Opções" tooltip-side="top" tooltip-size="small" ng-show="ticket.arteFinalista" class="dropdown pull-right">
												<a href="javascript:;" class="dropdown-btn table-icon-td icon-three-points"></a>
												<div class="dropdown-menu">
													<ul>
                                                        <li><a target="_blank" href="@{{ arte.path }}"><span class="dropdown-item-ico icon-download"></span>Baixar Arte-final</a></li>
														<li><a href="javascript:;" mw-confirm-click="deleteArte(arte.id)" mw-confirm-click-message="Deseja excluir essa arte?"><span class="dropdown-item-ico icon-cross"></span>Excluir</a></li>
													</ul>
												</div>
											</div>
											<span class="icon-axis-rules"></span>
											<div class="send-artwork-txt send-artwork-body">
												<h4><span>@{{arte.nome}}</span></h4>
												<small class="no-margin">Enviado por @{{arte.nome_usuario}} às @{{arte.data_criacao}}</small>
												<small class="no-margin">Arquivo original: @{{arte.path_info.basename}}</small>
											</div>
										</li>
									</ul>
								</div>
								<div ng-show="ticket.qtdEnvioArtes && ticket.envio_autorizado && ticket.participanteEnvio" class="send-artwork-input-group">
									<div class="box-inline select-control col-supplier-chooser">
										<select ng-focus="interval_tickets_stop()" ng-blur="interval_tickets_start()" ng-change="setar_fornecedor(a);" ng-model="arte.fornecedorSelecionado" name="cbSupplierChooser" id="cbSupplierChooser" class="suppliers">
											<option value="">Selecionar o fornecedor</option>
											<option ng-repeat="(f, fornecedor) in fornecedores" value="@{{ f }}" ng-value="@{{ f }}">@{{ fornecedor.nome }}</option>
										</select>
									</div>
									<div class="box-inline col-supplier input-control">
										<input ng-focus="interval_tickets_stop()" ng-blur="interval_tickets_start()" ng-model="enviar_emails[a]" type="text" placeholder="Email" name="tfEmailSupplier" id="tfEmailSupplier" />
									</div>
									<button ng-click="enviar_emails(a, arte.id)" type="button"><span class="icon icon-arrow-right-light"></span></button>
								</div>
								<br />
							</div>
						</div>
					</section>
					<section class="artwork-send-history-section margin-bottom-30" ng-show="ticket.id_status >= 5 && ticket.artes_envios.length">
						<hr />
						<div class="send-artwork-container clearfix">
							<h4 class="send-artwork-title no-margin-left no-padding">Registro de Envios da Arte-final</h4>
							<div id="registro-envio-arte" class="mail-recipients-col box-inline">
								<ul class="send-artwork-list was-sent">
									<li ng-repeat="(a, envio) in ticket.artes_envios">
										<div tooltips tooltips tooltip-template="Opções" tooltip-side="top" tooltip-size="small" ng-class="{'disabled': !envio.dt_expiracao }" class="dropdown pull-right">
											<a href="javascript:;" class="dropdown-btn table-icon-td icon-three-points"></a>
											<div class="dropdown-menu">
												<ul>
													<li>
														<div ng-if="envio.dt_expiracao" ng-class="{'disabled no-hover': envio.expirado }" class="dropdown-switch-ctrl">
															<label class="switch-control no-margin">
																<input ng-click="setStatusEnvio(envio.id)" ng-checked="envio.status" type="checkbox" ng-disabled="envio.expirado" name="link_arte[]" />
																<span class="switch-toggle pull-right"></span>
																<strong ng-if="envio.status && !envio.expirado" class="switch-txt no-margin">Link Ativo</strong>
																<strong ng-if="!envio.status && !envio.expirado" class="switch-txt no-margin">Link Inativo</strong>
																<strong ng-if="envio.expirado" class="switch-txt no-margin"><span style="color:#ACACAC !important;">Link Expirado</span></strong>
																<span class="dropdown-item-ico icon-link-url"></span>
															</label>
														</div>
													</li>
													<li><a ng-click="renovarLink(envio.id)" ng-class="{'disabled no-hover': !envio.status }" href="javascript:;"><span class="dropdown-item-ico icon-arrow-curve-recycle"></span>Renovar Link</a></li>
													<li><a ng-click="reenviarEmail(envio.id)" ng-class="{'disabled no-hover': !envio.status || envio.expirado }" href="javascript:;"><span class="dropdown-item-ico icon-air-plane"></span>Reenviar E-mail</a></li>
												</ul>
											</div>
										</div>
										<span class="icon-air-plane"></span>
                                        <div class="send-artwork-txt send-artwork-body">
											<h4>
                                                <span ng-show="envio.fornecedor">@{{envio.fornecedor}}:&nbsp;</span>
                                                <span ng-show="!envio.fornecedor">@{{envio.emails}}</span>
                                            </h4>
											<small ng-show="envio.fornecedor" class="no-margin">@{{envio.emails}}</small>
                                            <small class="no-margin">Enviado por @{{envio.nome_usuario}} às @{{envio.data_criacao}}</small>
                                            <small class="no-margin"><strong class="txt-book">Arquivo original:</strong> @{{envio.nome_arte}}</small>
                                            <small ng-if="envio.dt_expiracao" class="download-counter margin-top-10 no-margin-left no-margin-bottom">
                                                    <strong ng-if="envio.downloads === 0" class="txt-bold">
                                                        <i class="icon icon-download"></i>Arte-final ainda não foi baixada
                                                    </strong>
                                                    <strong ng-if="envio.downloads === 1" class="txt-bold">
                                                        <i class="icon icon-download"></i>Arte-final foi baixada @{{ envio.downloads }} vez
                                                    </strong>
                                                    <strong ng-if="envio.downloads !== 1 && envio.downloads !== 0" class="txt-bold">
                                                        <i class="icon icon-download"></i>Arte-final foi baixada @{{ envio.downloads }} vezes
                                                    </strong>
                                            </small>
											<small ng-if="envio.dt_expiracao && envio.status && !envio.expirado" class="no-margin">Link de acesso às artes está ativo expira em @{{ envio.dt_expiracao }}</small>
											<small ng-if="envio.dt_expiracao && !envio.status && !envio.expirado" class="no-margin">Link de acesso às artes está inativo expira em @{{ envio.dt_expiracao }}</small>
											<small ng-if="envio.dt_expiracao && envio.expirado" class="no-margin">Link de acesso às artes expirou em @{{ envio.dt_expiracao }}</small>
										</div>
                                    </li>
								</ul>
							</div>
						</div>
					</section>
					<div class="clearfix">
						<div ng-show="ticket.libera_status && ticket.ciclo_atual.layout && ticket.id_status === 1 && ticket.qtde_revisores" class="form-btn-set">
							<hr />
							<button mw-confirm-click="acao_ticket('2');" mw-confirm-click-message="Deseja submeter à revisão?" ng-show="ticket.libera_status && ticket.ciclo_atual.layout && ticket.id_status === 1 && ticket.qtde_revisores" type="button" class="btn btn-alt btn-positive">
								<span><strong>Submeter à Revisão</strong><i class="icon icon-right icon-arrow-right-light"></i></span>
							</button>
						</div>
						<div ng-show="ticket.libera_status && ticket.ciclo_atual.layout && ticket.id_status === 1 && !ticket.qtde_revisores" class="form-btn-set">
							<hr />
							<button onclick="myAlert('Não há revisores adicionados a esse ticket.<br />Para proseguir, adicione pelo menos um revisor.', 'warning')" ng-show="ticket.libera_status && ticket.ciclo_atual.layout && ticket.id_status === 1 && !ticket.qtde_revisores" type="button" class="btn btn-alt btn-default">
								<span><strong>Submeter à Revisão</strong><i class="icon icon-right icon-arrow-right-light"></i></span>
							</button>
						</div>
						<div ng-show="step3 && ticket.id_status === 2 && ticket.libera_status && (ticket.qtde_marketings || ticket.qtde_aprovadores)" class="form-btn-set">
							<hr />
							<button mw-confirm-click="acao_ticket('3');" mw-confirm-click-message="Deseja submeter à aprovação?" type="button" ng-show="step3 && ticket.id_status === 2 && ticket.libera_status && (ticket.qtde_marketings || ticket.qtde_aprovadores)" class="btn btn-alt btn-positive">
								<span><strong>Submeter à Aprovação</strong><i class="icon icon-right icon-arrow-right-light"></i></span>
							</button>
						</div>
						<div ng-show="step3 && ticket.id_status === 2 && ticket.libera_status && !ticket.qtde_marketings && !ticket.qtde_aprovadores" class="form-btn-set">
							<hr />
							<button onclick="myAlert('Não há aprovadores adicionados a esse ticket.<br>Para proseguir, adicione pelo menos um marketing ou um aprovador.', 'warning')" type="button" ng-show="step3 && ticket.id_status === 2 && ticket.libera_status && !ticket.qtde_marketings && !ticket.qtde_aprovadores" class="btn btn-alt btn-default">
								<span><strong>Submeter à Aprovação</strong><i class="icon icon-right icon-arrow-right-light"></i></span>
							</button>
						</div>
						<div ng-show="ticket.id_status === 3 && ticket.libera_status || ticket.id_status === 3 && ticket.libera_status && ticket.qtde_aprovadores" class="form-btn-set">
							<hr />
							<button onClick="location='{{Route('site.artwork.ticket.edit.deny.get', [$ticket->id])}}'" type="button" ng-show="ticket.id_status === 3 && ticket.libera_status" class="btn btn-negative">
								<span><i class="icon icon-ban-circle"></i> <strong>Reprovar</strong></span>
							</button>
							<button mw-confirm-click="acao_ticket('4');" mw-confirm-click-message="Deseja aprovar?" type="button" ng-show="ticket.id_status === 3 && ticket.libera_status && ticket.qtde_aprovadores" class="btn btn-positive">
								<span><i class="icon icon-check"></i> <strong>Aprovar</strong></span>
							</button>
						</div>
						<div ng-show="ticket.id_status === 3 && ticket.libera_status && !ticket.qtde_aprovadores || ticket.id_status === 4 && ticket.libera_status" class="form-btn-set">
							<hr />
							<button onClick="location='{{Route('site.artwork.ticket.edit.deny.get', [$ticket->id])}}'" type="button" ng-show="ticket.id_status === 4 && ticket.libera_status" class="btn btn-negative">
								<span><i class="icon icon-ban-circle"></i> <strong>Reprovar</strong></span>
							</button>
							<button onclick="myAlert('Não há aprovadores adicionados a esse ticket.<br>Para proseguir, adicione pelo menos um aprovador.', 'warning')" type="button" ng-show="ticket.id_status === 3 && ticket.libera_status && !ticket.qtde_aprovadores" class="btn btn-default">
								<span><i class="icon icon-check"></i> <strong>Aprovar</strong></span>
							</button>
							<button mw-confirm-click="acao_ticket('5');" mw-confirm-click-message="Deseja aprovar?" type="button" ng-show="ticket.id_status === 4 && ticket.libera_status" class="btn btn-positive">
								<span><i class="icon icon-check"></i> <strong>Aprovar</strong></span>
							</button>
						</div>
						<div ng-show="ticket.id_status === 5 && ticket.envio_finalizado && !ticket.envio_autorizado && (ticket.revisor)" class="form-btn-set">
							<hr />
							<button mw-confirm-click="acao_ticket('autorizar_envio');" mw-confirm-click-message="Deseja autorizar envio de arte-final?" ng-show="ticket.id_status === 5 && ticket.envio_finalizado && !ticket.envio_autorizado && (ticket.revisor)" type="button" class="btn btn-positive">
								<span><i class="icon icon-check"></i> <strong>Autorizar Envio de Arte-final</strong></span>
							</button>
						</div>
						<div ng-show="ticket.id_status === 5 && ticket.arteFinalista && ticket.artes.length && !ticket.envio_autorizado" class="form-btn-set">
							<hr />
							<button mw-confirm-click="acao_ticket('finalizar_envio');" mw-confirm-click-message="Deseja finalizar envio de arte-final?" ng-show="ticket.id_status === 5 && ticket.arteFinalista && ticket.artes.length && !ticket.envio_finalizado" type="button" class="btn btn-positive">
								<span><i class="icon icon-check"></i> <strong>Finalizar Envio de Arte-final</strong></span>
							</button>
						</div>
                        
                        <div ng-show="ticket.id_status === 5 && ticket.participanteEnvio && ticket.artes_envios.length && !ticket.envio_finalizado" class="form-btn-set">
							<hr />
							<button mw-confirm-click="acao_ticket('finalizar_envio_fornecedor');" mw-confirm-click-message="Deseja finalizar envio de arte-final?" ng-show="ticket.id_status === 5 && ticket.participanteEnvio && ticket.artes_envios.length" type="button" class="btn btn-positive">
								<span><i class="icon icon-left icon-check"></i><strong>Declarar Arte-final Enviada</strong></span>
							</button>
						</div>
                        
						<div ng-show="ticket.id_status === 5 && ticket.libera_status && ( (ticket.artes_envios.length && ticket.envio_finalizado) || (ticket.qtdEnvioArtes == 0 && ticket.envio_finalizado ) )" class="form-btn-set">
							<hr />
							<button mw-confirm-click="acao_ticket('6');" mw-confirm-click-message="Deseja encerrar o ticket?" type="button" class="btn btn-positive">
								<span><i class="icon icon-left icon-check"></i><strong>Encerrar Ticket</strong></span>
							</button>
						</div>
					</div>
				</div>
				<div class="tab-content" ng-show="tab == 2">
					<section ng-hide="ticket.id_status >= 6 && !ticket.arquivos.length" class="file-attachment-section clearfix">
						<h4>Anexos</h4>
						<div ng-show="ticket.id_status < 6 && ticket.libera_ticket" class="file-attachment-container clearfix">
							<div style="margin-right: 15px; width: 89.38%;" class="box-inline col-861-wide file-attachment-control clearfix">
								<span class="icon-paperclip-oblique"></span>
							    <input ng-focus="interval_tickets_stop()" type="file" ngf-select ng-model="arquivos" name="file" ngf-model-invalid="errorFile">
                            	<span class="file-placeholder">Procurar...</span>
                            </div>
							<button ng-click="uploadArquivo(ticket.id)" type="button" class="btn-upload">
								<span class="btn-icon icon-upload"></span> Carregar
							</button>
							<div ng-show="arquivos.progress >= 0" id="progress-circle" class="progress-circle">
								<svg id="svg" viewbox="0 0 100 100">
									<circle cx="50" cy="50" r="45" fill="#004192"/>
									<path fill="none" stroke-linecap="round" stroke-width="5" stroke="#fff" stroke-dasharray="@{{arquivos.progress*2.5}},250.2" d="M50 10 a 40 40 0 0 1 0 80 a 40 40 0 0 1 0 -80"/>
									<text id="count" class="progress" x="50" y="50" text-anchor="middle" dy="7" font-size="20" fill="#fff" ng-bind="arquivos.progress + '%'"></text>
								</svg>
							</div>
						</div>
						<ul class="file-attachment-list file-attachment-list-inline clearfix">
							<li ng-repeat="(a, arquivo) in ticket.arquivos track by $index">
								<div tooltips tooltip-smart="true" tooltip-template="Opções" class="dropdown pull-right">
									<a href="javascript:;" class="dropdown-btn table-icon-td icon-three-points icon-disabled"></a>
									<div class="dropdown-menu">
										<ul>
											<li><a href="javascript:;"><span class="dropdown-item-ico icon-cross"></span>Excluir</a></li>
										</ul>
									</div>
								</div>
								<a target="_blank" ng-href="@{{ arquivo.path }}">
									<span class="icon icon-document-fill"></span>
								</a>
								<div class="file-attachment-txt">
									<h4><a target="_blank" ng-href="@{{ arquivo.path }}">@{{ arquivo.descricao }}</a></h4>
									<small>Anexado por @{{ arquivo.nome_usuario }} às @{{ arquivo.data_criacao }}</small>
								</div>
							</li>
						</ul>
						<div ng-if="ticket.arquivos.length === 0" style="margin:0px;" class="text-block">
							<p>
								Nenhum arquivo.
							</p>
						</div>
						<div ng-show="ticket.arquivos.length > 0" class="btn-download-all-files btn-group">
							<a href="{{ route('site.artwork.ticket.baixarArquivos', $ticket->numero) }}">
								<span class="btn-icon btn-icon-right icon-download"></span> Baixar Todos os Arquivos
							</a>
						</div>
					</section>
				</div>
				<div class="tab-content" ng-show="tab == 3">
					<section ng-hide="ticket.id_status >= 6" class="msgbox-general-section">
						<h4>Mensagens</h4>
						<div ng-show="ticket.libera_ticket" class="msgbox-container textbox-mensagens">
							<div contenteditable="true" ng-focus="interval_tickets_stop()" ng-model="msgTicket" ng-paste="msg_copy_paste_handler($event, 'geral')" class="textbox-fake"></div>
							<div class="msgbox-controls">
								<div class="tooltip attach-file">
									<span class="icon-paperclip-oblique"></span>
									<input type="file" data-id="@{{$id}}" onchange="angular.element(this).scope().msg_attach_file(this, 'geral');" class="msgAttachment" />
									<span class="tooltip-box">Anexar imagem no texto</span>
								</div>
								<a href="javascript:;" ng-click="enviar_msg_email();" class="tooltip send-by-mail">
									<span class="icon-mail"></span>
									<span class="tooltip-box">Enviar também por email</span>
								</a>
								<button type="button" ng-click="adicionar_mensagem();" class="tooltip">
									<span class="icon-arrow-right-light"></span>
									<span class="tooltip-box">Enviar mensagem</span>
								</button>
							</div>
						</div>
						<div ng-repeat="mensagem in ticket.comentarios" class="msgbox-comments clearfix">
							<div class="comment">
								<h4 style="color:@{{mensagem.cor}}">@{{mensagem.nome_usuario}} às @{{mensagem.data_criacao}}</h4>
								<p ng-bind-html="mensagem.texto"></p>
							</div>
							<div ng-show="ticket.libera_ticket" class="btn-group">
								<button type="button" ng-click="exibir_responder_msg($id)" class="btn-reply">
									<span class="btn-icon btn-icon-left icon-arrow-curve-right"></span>Responder
								</button>
							</div>
							<div ng-show="exibir_responder_msg[$id]">
								<div class="msgbox-container chat-comment-box msgbox-reply-to">
									<div contenteditable="true" ng-focus="interval_tickets_stop()" ng-model="respostas[$id]" ng-init="respostas[$id] = ''" ng-paste="msg_copy_paste_handler($event, 'respostas', $id)" class="textbox-fake"></div>
									<div class="msgbox-controls">
										<div class="tooltip attach-file">
											<span class="icon-paperclip-oblique"></span>
											<input type="file" name="msgAttachment" class="msgAttachment" data-id="@{{$id}}" onchange="angular.element(this).scope().msg_attach_file(this, 'respostas');" />
											<span class="tooltip-box">Anexar imagem no texto</span>
										</div>
										<a href="javascript:;" ng-click="enviar_msg_email(mensagem.id_usuario);" class="tooltip send-by-mail">
											<span class="icon-mail"></span>
											<span class="tooltip-box">Enviar também por email</span>
										</a>
										<button ng-click="responder(mensagem.id, $id, 'geral')" type="button" class="tooltip">
											<span class="icon-arrow-right-light"></span>
											<span class="tooltip-box">Enviar mensagem</span>
										</button>
									</div>
								</div>
							</div>
							<ul>
								<li ng-repeat="(c, resposta) in mensagem.respostas">
									<div class="reply-to">
										<h4 style="color: @{{resposta.cor}}">@{{resposta.nome_usuario}} às @{{resposta.data_criacao}}</h4>
										<p ng-bind-html="resposta.texto"></p>
									</div>
								</li>
							</ul>
						</div>
						<div ng-if="ticket.id_status >= 6 || ticket.comentarios.length === 0" style="margin:10px 0px 0px 0px;" class="text-block">
							<p>
								Nenhuma conversa iniciada.
							</p>
						</div>
					</section>
				</div>
				<div class="tab-content" ng-show="tab == 6">
					<section ng-if="ticket.libera_ticket" style="padding-bottom: 30px;">
						<div class="text-block">
							<h4>Seguir o Ticket</h4>
							<p>
								Ao seguir o ticket você será notificado por e-mail toda vez que houver alteração de status, mesmo que não seja relacionado à sua função ou 
								sua tarefa
							</p>
						</div>
						<label class="custom-checkbox-control no-margin"> 
							<input ng-click="seguir({{ Auth::user()->id }}, {{ $ticket->id }})" ng-model="ticket.recebe_email" type="checkbox" /> 
							<span class="checkmark"></span>Seguir 
						</label>
					</section>
					<section ng-if="ticket.libera_ticket && ({{ (Auth::user()->gerente) ? 1 : 0 }} == 1 || (ticket.id_status <= 4 && !ticket.participante))" style="padding-bottom: 30px;">
						<hr />
						<div class="text-block">
							<h4>Editar o Ticket</h4>
							<p>
								É possível editar as informações do ticket, como datas e instruções ou também incluir ou excluir participantes.
							</p>
						</div>
						<a href="{{ route('site.artwork.ticket.editNew.get', [$ticket->id]) }}" class="btn-function"><span class="icon icon-officine"></span> Editar</a>
					</section>
					<section style="padding-bottom: 30px;">
							<hr ng-if="ticket.libera_ticket && ({{ (Auth::user()->gerente) ? 1 : 0 }} == 1 || (ticket.id_status <= 4 && !ticket.participante))" />
							<div class="text-block">
								<h4>Suspender o Ticket</h4>
								<p>
									Ao pausar, o ticket fica suspenso e não é possível fazer interações até que ele seja reativado. Ao cancelar, o ticket perde o vínculo
									com a embalagem e se torna apenas um registro. Não é possível trazer um ticket cancelado de volta ao fluxo de trabalho, mas é possível
									consultá-lo no Gerenciador de Tickets.
								</p>
							</div>
							<p>
								<a ng-if="!ticket.pausado_em && !ticket.cancelado_em" ng-class="{'active':ticket.cancelado_em}" ng-click="pausar_ticket()" href="javascript:;" class="btn-function btn-funcao-pausar"><span class="icon icon-pause"></span> Pausar</a>
                                <a ng-if="ticket.cancelado_em" ng-class="{'active':ticket.cancelado_em}" href="javascript:;" class="btn-function btn-funcao-pausar"><span class="icon icon-pause"></span> Pausar</a>
                                <a ng-if="ticket.pausado_em" href="javascript:;" class="btn-function" ng-click="despausar_ticket()"><span class="icon icon-play"></span> Reativar</a>
							</p>
                            <p ng-if="ticket.id_usuario === {{ Auth::user()->id }} || {{ (Auth::user()->admin) ? 1 : 0 }} == 1">
                            	<a ng-if="!ticket.cancelado_em" ng-click="cancelar_ticket_confirm()" href="javascript:;" class="btn-function"><span class="icon icon-cross"></span> Cancelar</a>
                                <a ng-if="ticket.cancelado_em" href="javascript:;" class="btn-function active"><span class="icon icon-cross"></span> Cancelado</a>
                            </p>
					</section>
				</div>
			</div>
		</div>
	</form>
</div>
@stop