@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
{!! Breadcrumbs::renderIfExists('artwork.ticket_reprovar', $ticket->id) !!}

<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />

<script>
    var id_ticket = {{$ticket->id}};
    var id_usuario = {{Auth::user()->id}};
    var csrf_token = '{{ csrf_token() }}';
</script>
<script>
    var tipos = [];
    
    @if (old('cbAlterationType'))
        @foreach (old('cbAlterationType') as $tipo)
            tipos.push('{{ $tipo }}')
        @endforeach
    @endif

    var descricoes = [];
    
    @if (old('txtDescriptionSpec'))
        @foreach (old('txtDescriptionSpec') as $desc)
            descricoes.push('{{ html_entity_decode($desc, ENT_COMPAT, 'ISO - 8859 - 1') }}')
        @endforeach
    @endif

    var template = {};
    
    if (tipos && descricoes) {
        var solicitacoes = [];
        for (index = 0; index < tipos.length; ++index) {
            template = {'id': tipos[0], 'descricao': descricoes[0]};
            solicitacoes.push(template);
        }
    } else {
        var solicitacoes = [
            //{'id': '1', 'descricao': 'Teste 1'},
            //{'id': '2', 'descricao': 'Teste 2'}
        ]
    }
    
    var edicao = false;
</script>

<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload-shim.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload.min.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/embalagens_modal_compartilhar.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_denied.js') }}"></script>
<script src="{{ asset2('/js/site/artwork/package-alteration-request.js') }}"></script>

<div class="content content-fluid clearfix" ng-app="app" ng-controller="TicketDeniedCtrl">
    @if (count($errors) > 0)
        <div class="alert alert-danger" role="alert">
            <div class="alert-error">
                @foreach ($errors->all() as $error)
                    {{ $error}}<br />
                @endforeach
            </div>
        </div>
    @endif
    
    @include('site.artwork.ticket_topo_inc')
    
    <!-- Timeline -->
    <div class="timeline" ng-cloak>
        <div ng-class="(ticket.etapas[1] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-flag"></i>
                <span class="event-txt">Aberto</span>
            </div>
        </div>
        <div ng-class="(ticket.etapas[2] === 1) ? 'event-box is-done' : 'event-box'" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[2] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-paint-brush"></i>
                <span class="event-txt">Edição</span>
            </div>
        </div>
        <!--div ng-class="(ticket.etapas[3] === 1) ? 'event-box is-done' : 'event-box'" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[3] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <div class="dot"></div>
            </div>
        </div-->
        <div ng-class="(ticket.etapas[3] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[3] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <div class="dot"></div>
            </div>
        </div>
        <div ng-class="(ticket.etapas[4] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[4] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-eye"></i>
                <span class="event-txt">Revisão</span>
            </div>
        </div>
        <div ng-class="(ticket.etapas[5] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[5] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <div class="dot"></div>
            </div>
        </div>
        <div ng-class="(ticket.etapas[6] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <!-- V1 - Opcional -->
        <div ng-show="ticket.perfis[2]" ng-class="(ticket.etapas[6] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-thumbs-up"></i>
                <span class="event-txt">Primeira <br /> Aprovação</span>
            </div>
        </div>
        <div ng-show="ticket.perfis[2]" ng-class="(ticket.etapas[7] === 1) ? 'event-box is-done' : 'event-box'" style="flex: 1;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <!-- END V1 - Opcional -->
        <div ng-class="(ticket.etapas[7] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-gavel"></i>
                <span class="event-txt">Demais <br /> Aprovações</span>
            </div>
        </div>
        <div ng-class="(ticket.etapas[8] === 1) ? 'event-box is-done' : 'event-box'" style="flex: 1;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[8] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-paint-brush"></i>
                <span class="event-txt">Finalização</span>
            </div>
        </div>
        <div ng-class="(ticket.etapas[9] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[9] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <div class="dot"></div>
            </div>
        </div>
        <div ng-class="(ticket.etapas[10] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.44;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <!-- V1 - Opcional -->
        <div ng-show="ticket.perfis[6]" ng-class="(ticket.etapas[10] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-paper-plane"></i>
                <span class="event-txt">Envio da <br /> Arte</span>
            </div>
        </div>
        <div ng-show="ticket.perfis[6]" ng-class="(ticket.etapas[11] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <!-- END V1 - Opcional -->
        <div ng-show="ticket.perfis[6]" ng-class="(ticket.etapas[11] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <div class="dot"></div>
            </div>
        </div>
        <div ng-show="ticket.perfis[6]" ng-class="(ticket.etapas[12] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[13] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-flag-checkered"></i>
                <span class="event-txt">Encerrado</span>
            </div>
        </div>
    </div>
    <!-- END Timeline -->
    
    <!-- Form Criar Ticket -->
    {!! Form::open(array('name' => 'formTicketDenied', 'id' => 'formAltRequest', 'class' => 'artw-ticket-create', 'files' => 'true', 'ng-submit' => 'validar($event)')) !!}
        <section class="bs border-color-primary margin-top-30 margin-bottom-40 no-padding">
            <div ng-cloak class="block">
                <h4 class="master-title-alt txt-bold margin-bottom-20">Reprovação</h4>
                <h5 class="master-title-sub label-control txt-small txt-uppercase margin-left-10 margin-right-10 margin-bottom-10">Instruções de Reprovação</h5>
                <div class="text-block margin-left-10 margin-right-10 no-margin-top margin-bottom-20">
                    <p class="txt-regular no-margin">
                        Descreva os motivos que levaram à reprovação divididos em instruções específicas.
                    </p>
                </div>
                <div id="alterationTypeWrap" class="input-fields-wrapper request-type">
                    <div ng-repeat="(k, solicitacao) in solicitacoes" class="input-fields-group flexbox-container margin-bottom-10">
                        <div class="artw-instructions-col flex flex-auto">
                            <div class="flexbox-container">
                                <div class="flex-12-large flex-auto margin-bottom-10 clearfix">
                                    <div class="select-control">	
                                        <select>
                                            <option value="">Escolha o tipo de alteração</option>
                                            <option ng-selected="solicitacao.id == tipo.id" ng-repeat="(k, tipo) in tipos" value="@{{ tipo.id }}">@{{ tipo.nome }}</option>
                                        </select>
                                        <input ng-value="solicitacao.id" type="hidden" name="cbAlterationType[]">
                                    </div>
                                </div>
                                <div class="flex-12-large flex-auto margin-bottom-10 clearfix">
                                    <textarea auto-height ng-model="solicitacao.descricao" placeholder="Descreva de forma específica" rows="3" class="input-control textbox-control textbox-auto-resizable">@{{ solicitacao.descricao }}</textarea>
                                    <input ng-value='solicitacao.descricao' type='hidden' name='txtDescriptionSpec[]'>
                                </div>
                            </div>
                        </div>
                        <div class="remove-instruction-col flex flex-auto">
                            <button ng-click="remover(k)" class="btn-control btn-alt">Excluir</button>
                        </div>
                    </div>
                    <div class="add-instruction-group input-fields-group flexbox-container margin-bottom-10">
                        <div class="artw-instructions-col flex flex-auto">
                            <div class="flexbox-container">
                                <div class="flex-12-large flex-auto margin-bottom-10 clearfix">
                                    <div class="select-control">	
                                        <select ng-model="add_tipo">
                                            <option value="">Escolha o tipo de alteração</option>
                                            <option ng-repeat="(k, tipo) in tipos" value="@{{ tipo.id }}">@{{ tipo.nome }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="flex-12-large flex-auto margin-bottom-10 clearfix">
                                    <textarea auto-height ng-model="add_descricao" placeholder="Descreva de forma específica" rows="3" class="input-control textbox-control textbox-auto-resizable"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="add-instruction-col flex flex-auto">
                            <button ng-click="adicionar()" type="button" class="btn-control btn-positive">Adicionar</button>
                        </div>
                    </div>
                </div>
            </div>
            <div ng-cloak class="block margin-bottom-30">
                <h4 class="master-title-alt txt-bold margin-bottom-20">Anexos</h4>
                <div id="fileAttachWrap" class="fileAttachWrap block margin-bottom-20">
                    <div class="artw-file-upload input-fields-wrapper">
                        <div class="input-fields-group artw-attach-files-group artw-attach-files">
                            <div class="files-control-multi flexbox-container margin-bottom-10">
                                <div class="flex artw-attach-files-col">
                                    <input type="file" accept=".docx,.doc,.pdf,.xml,.jpg,.jpeg,.png,.bmp,.gif,.tif,.tiff,.psd,.eps,.ai,.svg,.ppt,.pptx,.xls,.xlsx,.zip,.rar,.7z,.txt" valid-file=".docx,.doc,.pdf,.xml,.jpg,.jpeg,.png,.bmp,.gif,.tif,.tiff,.psd,.eps,.ai,.svg,.ppt,.pptx,.xls,.xlsx,.zip,.rar,.7z,.txt" name="artworkFileUpload[]" class="valid-file-ext" />
                                    <span class="files-control-fake"><strong></strong><i class="addon fa fa-paperclip"></i></span>
                                </div>
                                <div class="btn-add flex add-files-submit-col">
                                    <button type="button" id="btnAddFileAttach" class="files-btn btn-add-fields">Adicionar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Button group -->
        <div class="btn-group txt-center">
            <button type="submit" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-right">
                <span><i class="icon fa fa-arrow-right"></i> Concluir Reprovação</span>
            </button>
        </div>
        <!-- END Button group -->
    {!! Form::close() !!}
    <!-- END Form Criar Ticket -->
</div>
@stop