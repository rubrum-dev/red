@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

	{!! Breadcrumbs::renderIfExists('artwork.embalagens', $cat, $family, $product) !!}

	<script src="{{ asset('/js/site/artwork/embalagens.js') }}"></script>

	<input name="regular" type="hidden" value="{{ $tipos['regular'] }}">
	<input name="promo" type="hidden" value="{{ $tipos['promo'] }}">

	<div class="content">
		<div class="top-head">
			<div class="top-inline top-packing-logo">
				<!--<a href="javascript:;" class="logo-down"><span class="logo-down-ico">Baixar Logo</span></a>-->
				{{ Html::image(asset($thumb), 'Logo', array('title' => '', 'alt' => 'Logo')) }}
			</div>
			<div class="top-inline top-head-title top-head-title-center">
				<h2><span>{{ $produto->nome }}</span></h2>
			</div>
			<div class="top-inline top-nav-right">
				<ul>
					<li><a href="#"><span class="icon icon-rec"></span> Ativas</a></li>
					<li><a href="#"><span class="icon icon-ban-circle"></span> Descontinuadas</a></li>
				</ul>
			</div>
		</div>
		<div class="table-pack-container" id="embalagens-regulares">
			<table class="table-pack" border="0" cellpadding="0" cellspacing="0">
				<thead>
					<tr>
						<th width="50">Embalagens Regulares</th>
						<th width="10">Ver PDF</th>
						<th width="10">Histórico</th>
						<th width="10">Enviar Arte</th>
						<th width="12">Solicitar Alteração</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($skus as $key => $sku)
						@if(!$sku['promo'])
							<tr>
								<td width="50"><a target="_blank" @if($sku['variacao']->artwVersoes->last()) href="{{ asset($sku['variacao']->artwVersoes->last()->path) }}" @endif>&bull; {{ $sku['nome'] }}</a></td>
								@if($sku['variacao']->artwVersoes->last())
									<td width="10"><a href="{{ asset($sku['variacao']->artwVersoes->last()->path) }}" target="_blank">Versão {{ $sku['variacao']->artwVersoes->last()->numero }}</a></td>
								@else
									<td width="10"><a style="color: #ff0000">Versão 0</a></td>
								@endif
								<td width="10"><a @if($sku['variacao']->artwVersoes->last()) href="{{ route('site.artwork.history', [$sku['variacao']->artwVersoes->last()->id]) }}" @endif class="table-icon-td icon-compass-2 {{ !$sku['variacao']->artwVersoes->last() ? 'icon-disabled' : '' }}"></a></td>
								<td width="10"><a @if($sku['variacao']->artwVersoes->last()) href="{{ route('site.artwork.send', [$sku['id_variacao']]) }}" @endif class="table-icon-td icon-mail {{ !$sku['variacao']->artwVersoes->last() ? 'icon-disabled' : '' }}"></a></td>
								<td width="12"><!--a href="{{ '/site/artwork/' . 'alter' . '/' }}" class="table-icon-td icon-options-settings"></a--> {{ HTML::linkRoute('site.artwork.ticket.create.get', '', array($sku['id_variacao']), array('class' => 'table-icon-td icon-options-settings')) }} </td>
							</tr>
						@endif
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="table-pack-container" id="embalagens-promocionais">
			<hr />
			<table class="table-pack">
				<thead>
					<tr>
						<th width="50">Embalagens Promocionais</th>
						<th width="10">Ver PDF</th>
						<th width="10">Histórico</th>
						<th width="10">Enviar Arte</th>
						<th width="12">Alterar Solicitação</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($skus as $key => $sku)
						@if($sku['promo'])
							<tr>
								<td width="50"><a target="_blank" @if($sku['variacao']->artwVersoes->last()) href="{{ asset($sku['variacao']->artwVersoes->last()->path) }}" @endif>&bull; {{ $sku['nome'] }}</a></td>
								@if($sku['variacao']->artwVersoes->last())
									<td width="10"><a href="{{ asset($sku['variacao']->artwVersoes->last()->path) }}" target="_blank">Versão {{ $sku['variacao']->artwVersoes->last()->numero }}</a></td>
								@else
									<td width="10"><a style="color: #ff0000">Versão 0</a></td>
								@endif
								<td width="10"><a @if($sku['variacao']->artwVersoes->last()) href="{{ route('site.artwork.history', [$sku['variacao']->artwVersoes->last()->id]) }}" @endif class="table-icon-td icon-compass-2"></a></td>
								<td width="10"><a @if($sku['variacao']->artwVersoes->last()) href="{{ route('site.artwork.send', [$sku['id_variacao']]) }}" @endif class="table-icon-td icon-mail"></a></td>
								<td width="12"><!--a href="{{ '/site/artwork/' . 'alter' . '/' }}" class="table-icon-td icon-options-settings"></a--> {{ HTML::linkRoute('site.artwork.ticket.create.get', '', array($sku['id_variacao']), array('class' => 'table-icon-td icon-options-settings')) }} </td>
							</tr>
						@endif
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@stop
