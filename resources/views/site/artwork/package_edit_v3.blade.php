@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
{!! Breadcrumbs::renderIfExists('artwork.nova_embalagem') !!}

<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />

<script>
    var packageType = '{{ ($variacao->id_campanha) ? 'Sim' : 'Nao' }}';
    var cbNewCategory = '{{ $variacao->sku->produtos->categorias->id }}';
    var cbPckFamily = '{{ $variacao->sku->produtos->familias->id }}';
    var cbPckProduct = '{{ $variacao->sku->produtos->id }}';
    var cbPckContentSize = '{{ $variacao->sku->dimensoes->id }}';
    var cbPckType = '{{ $variacao->sku->dimensoes->tiposEmbalagens->id }}';
    var cbPckCampaign = '{{ $variacao->id_campanha }}';
    var cbPckNatureza = '{{ $variacao->id_natureza_embalagem }}';

    @if ($variacao->tem_codigo_ean === 0)
    var pckEANCode = '1';
    @elseif ($variacao->tem_codigo_ean === 1 && !$variacao->codigo_ean)
    var pckEANCode = '2';
    @elseif ($variacao->tem_codigo_ean === 1 && $variacao->codigo_ean)
    var pckEANCode = '3';
    @else
    var pckEANCode = '';
    @endif

    var txtPckEANCode = '{{ $variacao->codigo_ean }}';

    var cbPckTypeItem = '0';
    var variacao_edicao = '{!! (!$variacao->principal) ? addslashes($variacao->nome) : "" !!}';
    var mercados = [];
    
    @foreach ($variacao->mercados as $mercado)
    
        mercados.push({{ $mercado->id }});
    
    @endforeach

    var embalagens_selecionadas = [];

    @foreach ($variacao->relacionadas as $relacionada)
    
        embalagens_selecionadas.push( {!! '{id: ' . $relacionada->variacao->id . ',nome:"'.$relacionada->variacao->sku_nome_completo.'"}' !!} );
    
    @endforeach
    
    var csrf_token = '{{ csrf_token() }}';
    var id_variacao = '{{ $variacao->id }}';
</script>
<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/package_edit_v3.js') }}"></script>
<script src="{{ asset2('js/site/artwork/package-create.js') }}"></script>

<div class="content content-fluid clearfix" ng-app="app" ng-controller="PackageCreateCtrl">
    <section class="artw-package-edit-container margin-top-10 margin-bottom-40">
        <form name="packageCreateForm" action="" method="POST" novalidate ng-cloak>
            <input type="hidden" name="_token" value="{{ csrf_token()}}" />
            <h3 class="master-title txt-bold margin-bottom-20">Alterar Embalagem</h3>
            <div class="box-package-info panel showcase-panel no-margin clearfix">
                <div ng-cloak class="showcase-body no-margin">
                    <div class="showcase-container">
                        <div class="flexbox-container flex-no-wrap clearfix">
                            <div class="flex-auto showcase-info showcase-item-detail text-inline col-100-max">
                                <h3 class="txt-bold">
                                    <strong ng-repeat="p in produtos" ng-show="p.id == cbPckProduct"><span>@{{ p.nome }}</span></strong>
                                    <strong ng-show="!cbPckProduct || cbPckProduct === 0"><span>@{{ cbPckProduct ? cbPckProduct : 'Nome do Produto' }}</span></strong>
                                </h3>
                                <small class="txt-small txt-color-alt margin-top-5">produto</small>
                            </div>
                            <div class="flex-auto showcase-info showcase-item-detail text-inline col-20-max">
                                <h3 class="txt-bold">
                                    <strong ng-repeat="t in tipos" ng-show="t.id == cbPckType"><span>@{{ t.nome }}</span></strong>
                                    <strong ng-show="!cbPckType || cbPckType === 0"><span>@{{ cbPckType ? cbPckType : 'Tipo de Embalagem' }}</span></strong>
                                </h3>
                                <small class="txt-small txt-color-alt margin-top-5">tipo de embalagem</small>
                            </div>
                            <div class="flex-auto showcase-info showcase-item-detail text-inline col-20-max">
                                <h3 class="txt-bold">
                                    <strong ng-repeat="t in tamanhos" ng-show="t.id == cbPckContentSize"><span>@{{ t.nome }}</span></strong>
                                    <strong ng-show="!cbPckContentSize || cbPckContentSize === 0"><span>@{{ (cbPckContentSize) ? cbPckContentSize : 'Volume' }}</span></strong>
                                </h3>
                                <small class="txt-small txt-color-alt margin-top-5">volume</small>
                            </div>
                            <div class="flex-auto showcase-info showcase-item-detail text-inline col-20-max">
                                <h3 class="txt-light">
                                    <strong ng-repeat="c in campanhas" ng-show="c.id == cbPckCampaign"><span ng-hide="cbPckCampaign === '0'" class="txt-light">@{{ c.nome }}</span></strong>
                                    <strong ng-show="!packageType || (packageType == 'Sim' && cbPckCampaign === '0') || (packageType == 'Sim' && !cbPckCampaign)"><span class="txt-light">@{{ (packageCampaignName) ? packageCampaignName : 'Campanha Promocional' }}</span></strong>
                                </h3>
                                <small ng-show="!packageType || packageType == 'Sim'" class="txt-small txt-color-alt margin-top-5">campanha</small>
                            </div>
                            <div class="flex-auto showcase-info showcase-item-detail text-inline col-20-max">
                                <h3 class="txt-light">
                                    <strong ng-show="packageHasVariation === 'Sim'"><span class="txt-light">@{{ (variacaoName) ? variacaoName : 'Variação' }}</span></strong>
                                </h3>
                                <small ng-show="packageHasVariation === 'Sim'" class="txt-small txt-color-alt margin-top-5">variação</small>
                            </div>
                        </div>
                        <div ng-init="carregar_familias()" class="flexbox-container clearfix">
                            <div class="flex-auto showcase-info showcase-feature text-inline margin-right-40 no-margin-bottom">
                                <small class="txt-small txt-light">Marca: <strong ng-repeat="p in produtos" ng-show="p.id == cbPckProduct" class="txt-small txt-semibold">@{{ p.familia }}</strong></small>
                            </div>
                            <div ng-init="carregar_categorias()" class="flex-auto showcase-info showcase-feature text-inline margin-right-40 no-margin-bottom">
                                <small class="txt-small txt-light">Categoria: <strong ng-repeat="p in produtos" ng-show="p.id == cbPckProduct" class="txt-small txt-semibold">@{{ p.categoria }}</strong></small>
                            </div>
                            <div class="flex-auto showcase-info showcase-feature text-inline margin-right-40 no-margin-bottom">
                                <small class="txt-small txt-light">Natureza: <strong ng-repeat="n in naturezas" ng-show="n.id == cbPckNatureza" class="txt-small txt-semibold">@{{ n.nome }}</strong></small>
                            </div>
                            <!-- TODO - Código EAN -->
                            <div class="flex-auto showcase-info showcase-feature text-inline no-margin-right no-margin-bottom">
                                <small class="txt-small txt-light">Código de Barras: 
                                    <strong ng-show="pckEANCode == '1'" class="txt-small txt-semibold">N/A</strong>
                                    <strong ng-show="pckEANCode == '2'" class="txt-small txt-semibold">Pendente</strong>
                                    <strong ng-show="txtPckEANCode !== '' && pckEANCode == '3'" class="txt-small txt-semibold">@{{ txtPckEANCode }}</strong>
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block">
                <div class="flexbox-container flexbox-group margin-top-20 margin-bottom-20 margin-left-10 margin-right-10">
                    <h4 class="txt-lighter txt-small txt-uppercase margin-bottom-15">Nome da Embalagem</h4>
                    <p class="txt-regular">
                        O sistema cria o nome da embalagem de acordo com o preenchimento dos campos abaixo. Preencha com atenção pois nomenclatura da embalagem é 
                        fundamental para que o sistema funcione corretamente.
                    </p>
                </div>
                <div class="bs">
                    <div class="flexbox-container">
                        <div class="flex flex-fluid">
                            <selectize ng-change="carregar_tipos()" placeholder="Selecione o produto" id="cbPckProduct" config="productsConfig" options="produtos" ng-model="cbPckProduct"></selectize>
                            <input name="cbPckProduct" value="@{{ cbPckProduct }}" type="hidden">
                        </div>
                        <div class="flex flex-fluid">
                            <selectize ng-change="carregar_tamanhos()" placeholder="Selecione o tipo de embalagem" name="cbPckType" id="cbPckType" config="packageTypeConfig" options="tipos" ng-model="cbPckType"></selectize>
                            <input name="cbPckType" value="@{{ cbPckType }}" type="hidden">
                        </div>
                        <div class="flex flex-fluid">
                            <selectize placeholder="Selecione o volume" name="cbPckContentSize" id="cbPckContentSize" config="contentsConfig" options="tamanhos" ng-model="cbPckContentSize" ng-disabled="disable" required="true"></selectize></div>
                            <input name="cbPckContentSize" value="@{{ cbPckContentSize }}" type="hidden">
                        </div>
                    </div>
                </div>
                <div class="bs">
                    <div class="margin-left-10 margin-right-10 margin-bottom-15">
                        <h4 class="txt-lighter txt-small txt-uppercase margin-bottom-15">A embalagem é promocional?</h4>
                        <p class="txt-regular">
                            As embalagens promocionais são agrupadas por campanha. É importante que as embalagens compartilhem o mesmo nome de campanha para que fiquem 
                            organizadas da forma correta.
                        </p>
                    </div>
                    <div class="margin-left-50">
                        <div class="flexbox-container">
                            <div class="flex margin-right-50">
                                <div class="input-group flexbox-container flexbox-group flex-align-center">
                                    <div class="flex margin-right-30">
                                        <label class="radio-btn-control">    
                                            <input type="radio" ng-change="carregar_variacoes();cbPckCampaign='';" ng-model="packageType" name="packageType" value="Nao" />
                                            <span class="checkmark"></span>
                                            <span class="radio-btn-txt txt-semibold">Não</span>
                                        </label>
                                    </div>
                                    <div class="flex">
                                        <label class="radio-btn-control">    
                                            <input type="radio" ng-change="carregar_campanhas()" ng-model="packageType" name="packageType" value="Sim" />
                                            <span class="checkmark"></span>
                                            <span class="radio-btn-txt txt-semibold">Sim</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div ng-show="packageType === 'Sim'" class="flex flex-fluid">
                                <div class="flexbox-container">
                                    <div class="flex flex-fluid">
                                        <selectize placeholder="Selecione a campanha" name="cbPckCampaign" id="cbPckCampaign" config="campaignConfig" options="campanhas" ng-change="cadastrar_campanha();carregar_variacoes();" ng-model="cbPckCampaign" ng-disabled="disable" required="true"></selectize>
                                        <input name="cbPckCampaign" value="@{{ cbPckCampaign }}" type="hidden">
                                    </div>
                                    <div ng-show="cbPckCampaign === '0'" class="flex flex-fluid">
                                        <input type="text" placeholder="Nome da nova campanha" ng-required="cbPckCampaign === '0'" ng-model="packageCampaignName" name="packageCampaignName" id="packageCampaignName" class="input-control only-alpha-numeric" />
                                        <p class="margin-top-10 margin-left-10 margin-right-10">
                                            <strong class="txt-small txt-bold">Dica:</strong> <span class="txt-small txt-regular">Escolha um nome curto e abrangente, como o nome do evento seguido do ano.</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bs">
                    <div class="margin-left-10 margin-right-10 margin-bottom-15">
                        <h4 class="txt-lighter txt-small txt-uppercase margin-bottom-15">Qual a natureza da embalagem?</h4>
                        <p class="txt-regular">
                            Escolha entre: Primária, Secundária ou Terciária. No caso de Secundárias e Terciárias, indique quais são as embalagens nelas contidas.
                        </p>
                    </div>
                    <div class="flexbox-container flex-align-center">
                        <div class="flex flex-4-large">
                            <selectize ng-disabled="!cbPckProduct" placeholder="Selecione" name="cbPckNatureza" id="cbPckNatureza" config="packageNaturezaConfig" options="naturezas" ng-model="cbPckNatureza" ng-disabled="disable" ng-change="carregar_embalagens_por_natureza()" required="true"></selectize>
                        </div>

                        <div ng-show="cbPckNatureza === '3'" class="flex flex-fluid">
                            <selectize placeholder="Indique as embalagens primárias contidas" name="cbPckEmbalagens" id="cbPckEmbalagens" config="primaryPackageConfig" options="embalagens_por_natureza" ng-model="cbPckEmbalagens"></selectize>            
                        </div>

                        <div ng-show="cbPckNatureza === '4'" class="flex flex-fluid">
                            <selectize placeholder="Indique as embalagens secundárias contidas" name="cbPckEmbalagens" id="cbPckEmbalagens" config="secondaryPackageConfig" options="embalagens_por_natureza" ng-model="cbPckEmbalagens"></selectize>            
                        </div>

                        <div ng-show="cbPckNatureza === '3' || cbPckNatureza === '4'" class="flex">
                            <button type="button" ng-click="add_embalagem_selecionada()" class="btn-control btn-positive">Adicionar</button>
                        </div>
                    </div>

                    <div class="flexbox-container flex-align-center margin-top-20">
                        
                        <table class="artw-shared-item-table table-border table-hover no-animate">
                            <tbody>
                                <tr ng-repeat="embalagem in embalagens_selecionadas track by $index">
                                    <td width="">
                                        <div class="d-flex flex-align-center">
                                            <i class="icon fa fa-cube"></i>
                                            <span class="text-side margin-left-10">@{{embalagem.nome}}</span>
                                        </div>
                                    </td>
                                    <td width="50">
                                        <div class="dropdown drop-to-left">
                                            <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                            <div class="dropdown-list">
                                                <ul> 
                                                    <li><a href="javascript:;" ng-click="excluir_embalagem_selecionada($index)" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        
                    </div>

                </div>

                <div class="bs">
                    <div class="margin-left-10 margin-right-10 margin-bottom-15">
                        <h4 class="txt-lighter txt-small txt-uppercase margin-bottom-15">Possui o código de barras?</h4>
                        <p class="txt-regular">
                            Se a embalagem possuir código de barras e você ainda não souber os números marque “Ainda não tenho o código”.
                        </p>
                    </div>
                    <div class="margin-left-50">
                        <div class="flexbox-container">
                            <div class="flex">
                                <div class="input-group flexbox-container flexbox-group flex-align-center">
                                    <div class="flexbox-container flexbox-group flex">
                                        <div class="flex margin-right-30">
                                            <label class="radio-btn-control">    
                                                <input type="radio" ng-model="pckEANCode" name="pckEANCode" value="1" />
                                                <span class="checkmark"></span>
                                                <span class="radio-btn-txt txt-semibold">Não se aplica</span>
                                            </label>
                                        </div>
                                        <div class="flex margin-right-30">
                                            <label class="radio-btn-control">    
                                                <input type="radio" ng-model="pckEANCode" name="pckEANCode" value="2" />
                                                <span class="checkmark"></span>
                                                <span class="radio-btn-txt txt-semibold">Ainda não tenho o código</span>
                                            </label>
                                        </div>
                                        <div class="flex margin-right-30">
                                            <label class="radio-btn-control">    
                                                <input type="radio" ng-model="pckEANCode" name="pckEANCode" value="3" />
                                                <span class="checkmark"></span>
                                                <span class="radio-btn-txt txt-semibold">Já tenho o código</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div ng-show="pckEANCode === '3'" class="flex flex-fluid">
                                <input type="text" placeholder="Digite o código de barras" maxlength="14" ng-model="txtPckEANCode" name="txtPckEANCode" id="txtPckEANCode" class="input-control only-numeric" />
                                <p ng-show="packageCreateForm.txtPckEANCode.$dirty && txtPckEANCode.length !== 8 && txtPckEANCode.length !== 13 && txtPckEANCode.length < 12" class="margin-top-10 margin-left-10 margin-right-10">
                                    <span class="txt-small txt-regular txt-color-primary">Insira o código de barras com 8, 12, 13 ou 14 dígitos.</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div ng-show="variacoes.length > 1 && !packageHasVariation" ng-class="{'bs border-color-primary margin-bottom-40 padding-bottom-40': variacoes.length > 1 && !packageHasVariation}">
                    <div class="margin-left-10 margin-right-10 margin-bottom-15">
                        <h4 class="txt-lighter txt-small txt-uppercase margin-bottom-15">Embalagem já cadastrada no sistema!</h4>
                        <p class="txt-regular">
                            Embalagens iguais precisam de um nome de variação para coexistirem no sistema. Só utilize este recurso se as embalagens forem coexistir 
                            no mercado, caso contrário abra um ticket na embalagem já existente para assim criar uma nova versão.
                        </p>
                    </div>
                    <div class="flexbox-container flex-align-center">
                        <div class="flex flex-fluid">
                            <input type="text" placeholder="Nome da variação" ng-model="variacaoName" name="variacaoName" id="variacaoName" class="input-control only-alpha-numeric" />
                            <p ng-if="variacaoName && (variacoes | filter:{nome:variacaoName}:true).length && variacaoName !== variacao_edicao" class="txt-small txt-regular txt-color-primary margin-top-10 margin-left-10 margin-right-10">
                                Essa variação já está em uso, dê um nome diferente.
                            </p>
                            <p class="margin-top-10 margin-left-10 margin-right-10">
                                <strong class="txt-small txt-bold">Dica:</strong> <span class="txt-small txt-regular">Cores, plantas técnicas ou fornecedores diferentes funcionam bem como nomes de variação.</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div ng-show="variacoes.length <= 1 || packageHasVariation" class="bs border-color-primary">
                    <div class="margin-left-10 margin-right-10 margin-bottom-15">
                        <h4 class="txt-lighter txt-small txt-uppercase margin-bottom-15">A embalagem possui variações?</h4>
                        <p class="txt-regular">
                            Embalagens iguais precisam de um nome de variação para coexistirem no sistema. Só utilize este recurso se as embalagens forem coexistir no 
                            mercado, caso contrário abra um ticket na embalagem já existente para assim criar uma nova versão. Se não tiver certeza clique em “Não”.
                        </p>
                    </div>
                    <div class="margin-left-50">
                        <div class="flexbox-container">
                            <div class="flex margin-right-50">
                                <div class="input-group flex-align-center flexbox-container flexbox-group">
                                    <div class="flex margin-right-30">
                                        <label class="radio-btn-control">    
                                            <input type="radio" ng-change="cadastrar_variacao()" ng-model="packageHasVariation" name="packageHasVariation" value="Nao" />
                                            <span class="checkmark"></span>
                                            <span class="radio-btn-txt txt-semibold">Não</span>
                                        </label>
                                    </div>
                                    <div class="flex">
                                        <label class="radio-btn-control">    
                                            <input type="radio" ng-change="cadastrar_variacao()" ng-model="packageHasVariation" name="packageHasVariation" value="Sim" />
                                            <span class="checkmark"></span>
                                            <span class="radio-btn-txt txt-semibold">Sim</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div ng-show="packageHasVariation === 'Sim'" class="flex flex-fluid">
                                <input type="text" placeholder="Nome da variação" ng-model="variacaoName" name="variacaoName" id="variacaoName" class="input-control only-alpha-numeric" />
                                <p class="margin-top-10 margin-left-10 margin-right-10">
                                    <strong class="txt-small txt-bold">Dica:</strong> <span class="txt-small txt-regular">Identifique o que essa embalagem possui de diferente e utilize isso como nome para essa variação. Cores, plantas técnicas ou fornecedores diferentes funcionam bem.</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-group txt-center margin-top-40" ng-if="(cbPckProduct !== '' && cbPckType !== '' && cbPckContentSize !== '' && cbPckNatureza)
                && (packageType === 'Nao' || (packageType === 'Sim' && ((cbPckCampaign !== '' && cbPckCampaign !== '0') || (cbPckCampaign === '0' && packageCampaignName))))
                && ( (pckEANCode == '1') || (pckEANCode == '2') || ((txtPckEANCode.length === 8 || txtPckEANCode.length >= 12) && pckEANCode == '3') )
                && ((packageHasVariation === 'Nao') || ( packageHasVariation === 'Sim' && (variacaoName) && !variacoes.length ) || ( !(variacoes | filter:{nome:variacaoName}:true).length ) || ( (variacoes | filter:{nome:variacaoName}:true).length && variacaoName == variacao_edicao ) )">
                    <button type="button" ng-click="enviarFormulario()" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                        <span><i class="icon fa fa-check"></i> Salvar</span>
                    </button>
                </div>
            </div>
        </form>
    </section>
</div>
@stop