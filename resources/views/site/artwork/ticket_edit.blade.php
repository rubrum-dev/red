@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
{!! Breadcrumbs::renderIfExists('artwork.ticket_editar', $ticket->id) !!}
{{--<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/artwork.css') }}" />--}}
<script>
    $(function () {
        if ($("#tfStartDate").val()){
            var tfStartDate = $("#tfStartDate").val().split("/");
            var minDate = new Date(tfStartDate[2], tfStartDate[1] - 1, tfStartDate[0]);
            minDate.setDate(minDate.getDate() + 1);
            var minDate2 = new Date(tfStartDate[2], tfStartDate[1] - 1, tfStartDate[0]);
            minDate2.setDate(minDate2.getDate() + 2);

            var dateFormat = "dd/mm/yy",
                from = $("#tfArtSendDate")
            
            .datepicker({
                //defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                minDate: minDate
            })
            .on("change", function () {
                to.datepicker("option", "minDate", getDate(this, 1));
            }),
            to = $("#tfDeadlineDate").datepicker({
                //defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                minDate: minDate2
            })
            .on("change", function () {
                from.datepicker("option", "maxDate", getDate(this, -1));
            });
        }

        function getDate(element, qtd) {
            var date;
            try {
                var date2 = $.datepicker.parseDate(dateFormat, element.value);
                date2.setDate(date2.getDate() + qtd);
                date = date2;
            } catch (error) {
                date = null;
            }
            return date;
        }
    });
</script>
<script>
	var id_ticket = {{$ticket->id}};
	var edicao = true;
</script>
<script src="{{ asset('/js/site/ui.datepicker-pt-BR.js') }}"></script>
<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edit.js') }}"></script>
<script src="{{ asset2('/js/site/artwork/package-alteration-request.js') }}"></script>
<div class="content content-fluid" ng-app="app" ng-controller="TicketEditCtrl">
    @if (count($errors) > 0)
        <div class="alert alert-danger" role="alert">
            <div class="alert-error">
                @foreach ($errors->all() as $error)
                    {{ $error}}<br />
                @endforeach
            </div>
        </div>
    @endif
    {{--<small style="word-break:break-word;">{{$ticket}}</small>--}}
    <!-- Showcase panel -->
	<div class="showcase-panel clearfix">
		<div class="showcase-left">
			<span class="helper"></span>
			{{ Html::image(asset($thumb), 'Logo', array('id' => 'imgLogo')) }}
		</div>
		<div class="showcase-body">
			<div class="showcase-container">
				<div class="flexbox-container clearfix">
					<div class="flex-auto showcase-info showcase-item-detail text-inline">
						<h3 class="txt-bold">{{ $ticket->sku_nome_completo }}</h3>
						<small class="text-small txt-color-alt margin-top-5">embalagem</small>
					</div>
					<div class="flex-auto showcase-info showcase-item-detail text-inline">
						<h3 class="txt-light">Copa 2018</h3>
						<small class="text-small txt-color-alt margin-top-5">campanha</small>
					</div>
					<div class="flex-auto showcase-info showcase-item-detail text-inline">
						<h3 class="txt-light">Argentina</h3>
						<small class="text-small txt-color-alt margin-top-5">variação</small>
					</div>
					<div class="flex-auto showcase-info showcase-item-detail text-inline">
						<h3 class="txt-light">Rolha Metálica</h3>
						<small class="text-small txt-color-alt margin-top-5">item</small>
					</div>
				</div>
				<div class="flexbox-container clearfix">
					<div class="flex-auto showcase-info showcase-feature text-inline no-margin-bottom">
						<small class="text-small txt-bold txt-color-primary">Ticket: {{ $ticket->numero }}</small>
						<small class="text-small txt-bold txt-color-primary">Ciclo: 0</small>
					</div>
					<div class="flex-auto showcase-info showcase-feature text-inline no-margin-bottom">
						<small class="text-small txt-light">Marca: <strong class="txt-semibold">Skol</strong></small>
						<small class="text-small txt-light">Categoria: <strong class="txt-semibold">Cerveja</strong></small>
					</div>
					<div class="flex-auto showcase-info showcase-feature text-inline no-margin-bottom">
						<small class="text-small txt-light">Natureza: <strong class="txt-semibold">Promocional</strong></small>
						<small class="text-small txt-light">Função: <strong class="txt-semibold">Primária</strong></small>
					</div>
					<div class="flex-auto showcase-info showcase-feature text-inline no-margin-bottom">
						<small class="text-small txt-light">Mercados: <strong class="txt-semibold">DE, AR, BR, CL, ES, US, FR, MX, PY, PT</strong></small>
					</div>
				</div>
			</div>
		</div>
	</div>
    <!-- END Showcase panel -->
    <!-- Form Editar Ticket -->
	{!! Form::open(array('name' => 'formAltRequest', 'id' => 'formAltRequest', 'class' => 'artw-ticket-create', 'files' => 'true')) !!}
	    <section class="margin-top-30">
            <div class="block bs">
				<h4 class="master-title-alt txt-bold margin-bottom-20">Informações</h4>
            	<div class="flexbox-container">
					<div class="artw-initial-info-col flex flex-auto">
						<label class="master-title-sub label-control txt-small txt-uppercase">Resumo</label>
						<textarea @if ($ticket->id_status > 2) @endif name="txtMainDescription" placeholder="Descrição geral" rows="3" id="txtMainDescription" class="input-control textbox-control">{{ $ticket->descricao }}</textarea>
                    </div>
					<div class="artw-start-date-col flex flex-auto">
						<label class="master-title-sub label-control txt-small txt-uppercase">Início</label>
                        <div class="date-control disabled readonly">
                            <input disabled value="{{ $ticket->dt_ini->format('d/m/Y') }}" type="text" name="tfStartDate" placeholder="dd/mm/aaaa" id="tfStartDate" class="date-input-readonly input-control" />
                            <span class="addon fa fa-calendar"></span>
                        </div>
                    </div>
					<div class="artw-approval-date-col flex flex-auto">
						<label class="master-title-sub label-control txt-small txt-uppercase">Aprovação</label>
						<div class="date-control">	
                            <input value="{{ $ticket->dt_envio->format('d/m/Y') }}" type="text" name="tfArtSendDate" placeholder="dd/mm/aaaa" id="tfArtSendDate" class="date-input-readonly input-control" />
                            <span class="addon fa fa-calendar"></span>
                        </div>
					</div>
					<div class="artw-closure-date-col flex flex-auto">
						<label class="master-title-sub label-control txt-small txt-uppercase">Encerramento</label>
						<div class="date-control">
                            <input onkeydown="return false;" type="text" autocomplete="off" name="tfDeadlineDate" placeholder="dd/mm/aaaa" value="{{ $ticket->dt_fim->format('d/m/Y') }}" id="tfDeadlineDate" class="date-input-readonly input-control" />
						    <span class="addon fa fa-calendar"></span>
                        </div>
					</div>
				</div>
            </div>
            @if ($ticket->id_status <= 2)
                <div ng-cloak class="block bs">
                    <label class="master-title-sub label-control txt-small txt-uppercase margin-left-10 margin-right-10">Instruções</label>
                    <div id="alterationTypeWrap" class="input-fields-wrapper request-type">
                        <div ng-repeat="(k, solicitacao) in solicitacoes" class="input-fields-group flexbox-container margin-bottom-20">
                            <div class="artw-instructions-col flex flex-auto">
                                <div class="flexbox-container">
                                    <div class="flex-12-large flex-auto margin-bottom-10 clearfix">
                                        <div class="select-control">	
                                            <select disabled="disabled">
                                                <option value="">Escolha o tipo de alteração</option>
                                                <option ng-selected="solicitacao.id_tipo == tipo.id" ng-repeat="(k, tipo) in tipos" value="@{{ tipo.id }}">@{{ tipo.nome }}</option>
                                            </select>
                                            <input ng-disabled="!solicitacao.id" ng-value='solicitacao.id_tipo' type='hidden'>
                                            <input ng-disabled="solicitacao.id" ng-value='solicitacao.id_tipo' type='hidden' name='cbAlterationType[]'>
                                        </div>
                                    </div>
                                    <div class="flex-12-large flex-auto margin-bottom-10 clearfix">
                                        <textarea disabled="disabled" placeholder="Descreva de forma específica" rows="4" class="input-control textbox-control">@{{ solicitacao.descricao }}</textarea>
                                        <input ng-disabled="!solicitacao.id" ng-value='solicitacao.descricao' type='hidden'>
                                        <input ng-disabled="solicitacao.id" ng-value='solicitacao.descricao' type='hidden' name='txtDescriptionSpec[]'>
                                    </div>
                                </div>
                            </div>
                            <div class="remove-instruction-col flex flex-auto">
                                <button ng-click="remover(k)" class="btn-control btn-alt">Excluir</button>
                            </div>
                        </div>
                        <div class="input-fields-group flexbox-container margin-bottom-20">
                            <div class="artw-instructions-col flex flex-auto">
                                <div class="flexbox-container">
                                    <div class="flex-12-large flex-auto margin-bottom-10 clearfix">
                                        <div class="select-control">	
                                            <select ng-model="add_tipo">
                                                <option value="">Escolha o tipo de alteração</option>
                                                <option ng-repeat="(k, tipo) in tipos" value="@{{ tipo.id }}">@{{ tipo.nome }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="flex-12-large flex-auto margin-bottom-10 clearfix">
                                        <textarea ng-model="add_descricao" placeholder="Descreva de forma específica" rows="3" class="input-control textbox-control"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="add-instruction-col flex flex-auto">
                                <button ng-click="adicionar()" type="button" class="btn-control btn-positive">Adicionar</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </section>
        <!-- Button group -->
		<div class="btn-group txt-center">
            <div class="bs border-color-primary margin-top-30 padding-top-30">    
                <button type="submit" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-right">
                    <span><i class="icon fa fa-arrow-right"></i> Próxima Etapa</span>
                </button>
            </div>
        </div>
        <!-- END Button group -->
    {!! Form::close() !!}
    <!-- END Form Editar Ticket -->
</div>
@stop
