@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
{!! Breadcrumbs::renderIfExists('artwork.historico', $variacao->id) !!}
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/v-accordion.min.css') }}" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script>
	var id_ticket = {{ $ticket->id }};
	var csrf_token = '{{ csrf_token() }}';
</script>
<script src="{{ asset('/js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/datatables/accent-neutralise.js') }}"></script>
<script src="{{ asset('/js/site/snap.svg-min.js') }}"></script>
<script src="{{ asset('/js/site/artwork/ticket-edit.js') }}"></script>
<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-route.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset('/js/angular/v-accordion.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload-shim.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload.min.js') }}"></script>
<script src="{{ asset('/js/angular/ticket-resolve-loader.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/embalagens_modal_compartilhar.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edicao_aba_visao_geral.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edicao_aba_anexos.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edicao_aba_participantes.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edicao_aba_tarefas.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edicao_aba_mensagens.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edicao_aba_pdf_fornecedor.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edicao_aba_opcoes.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/historico.js') }}"></script>
<div class="content content-fluid" ng-app="app" ng-controller="TicketHistoryCtrl" ng-cloak>
	<h3 class="master-title margin-top-10 margin-bottom-15">Histórico</h3>
	<ul class="tabs clearfix">
		<li><a href="javascript:;" ng-click="artw_tab=1" ng-class="{'active' : artw_tab==1}">Versão {{ $versao->numero }}</a></li>
		@if ($ticket->artwitem->artwVersoes->count() > 1)
			<li><a href="javascript:;" ng-click="artw_tab=2" ng-class="{'active' : artw_tab==2}">Todas as Versões</a></li>
		@endif
	</ul>
	<div ng-show="artw_tab == 1" class="artwork-container margin-top-30 margin-bottom-40">
		
        @include('site.artwork.ticket_topo_compartilhado_inc')
        
		<!-- Timeline -->
		<div class="timeline" ng-cloak>
			<div style="flex: unset;" class="event-box current">
				<div class="event">
					<i class="event-icon fa fa-flag"></i>
					<div class="event-dropdown-list to-right">
						<ul>
							<li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
						</ul>
					</div>
					<span class="event-txt">Aberto</span>
				</div>
			</div>
			<div class="event-box is-done">
				<div class="filling-bar"></div>
			</div>
			<div style="flex: unset;" class="event-box current">
				<div class="event">
					<i class="event-icon fa fa-paint-brush"></i>
					<span class="event-txt">Edição</span>
				</div>
			</div>
			<!--div ng-class="(ticket.etapas[3] === 1) ? 'event-box is-done' : 'event-box'" class="event-box">
				<div class="filling-bar"></div>
			</div>
			<div ng-class="(ticket.etapas[3] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
				<div class="event"> 
					<div class="dot"></div>
				</div>
			</div-->
			<div style="flex:0.5;" class="event-box is-done">
				<div class="filling-bar"></div>
			</div>
			<div style="flex: unset;" class="event-box current">
				<div class="event"> 
					<div class="dot"></div>
				</div>
			</div>
			<div style="flex:0.5;" class="event-box is-done">
				<div class="filling-bar"></div>
			</div>
			<div style="flex: unset;" class="event-box current">
				<div class="event">
					<i class="event-icon fa fa-eye"></i>
					<span class="event-txt">Revisão</span>
				</div>
			</div>
			<div style="flex:0.5;" class="event-box is-done">
				<div class="filling-bar"></div>
			</div>
			<div style="flex: unset;" class="event-box current">
				<div class="event"> 
					<div class="dot"></div>
				</div>
			</div>
			<div style="flex:0.5;" class="event-box is-done">
				<div class="filling-bar"></div>
			</div>
			<!-- V1 - Opcional -->
			<div ng-show="(ticket.perfis[2] | filter:{deleted_at:null}:true).length || (ticket.perfis[2] | filter:{aprovado:1}:true).length || (ticket.perfis[2] | filter:{aprovado:2}:true).length" style="flex: unset;" class="event-box current">
				<div class="event">
					<i class="event-icon fa fa-thumbs-up"></i>
					<span class="event-txt">Primeira <br /> Aprovação</span>
				</div>
			</div>
			<div ng-show="(ticket.perfis[2] | filter:{deleted_at:null}:true).length || (ticket.perfis[2] | filter:{aprovado:1}:true).length || (ticket.perfis[2] | filter:{aprovado:2}:true).length" style="flex: unset; flex-basis: 90px;" class="event-box is-done">
				<div class="filling-bar"></div>
			</div>
			<!-- END V1 - Opcional -->
			<div style="flex: unset;" style="flex: unset;" class="event-box current">
				<div class="event">
					<i class="event-icon fa fa-gavel"></i>
					<span class="event-txt">Demais <br /> Aprovações</span>
				</div>
			</div>
			<div style="flex: unset; flex-basis: 90px;" class="event-box is-done">
				<div class="filling-bar"></div>
			</div>
			<div style="flex: unset;" class="event-box current">
				<div class="event">
					<i class="event-icon fa fa-paint-brush"></i>
					<span class="event-txt">Finalização</span>
				</div>
			</div>
			<div style="flex:0.5;" class="event-box is-done">
				<div class="filling-bar"></div>
			</div>
			<div style="flex: unset;" class="event-box current">
				<div class="event"> 
					<div class="dot"></div>
				</div>
			</div>
			<div style="flex:0.5;" class="event-box is-done">
				<div class="filling-bar"></div>
			</div>
            
			<!-- Envio da Arte -->
			<div ng-show="ticket.perfis[6]" style="flex: unset;" class="event-box current">
				<div class="event">
					<i class="event-icon fa fa-paper-plane"></i>
					<span class="event-txt">Envio da <br /> Arte</span>
				</div>
			</div>
            <div ng-show="ticket.perfis[6]" style="flex:0.5;" class="event-box is-done">
				<div class="filling-bar"></div>
			</div>
            <div ng-show="ticket.perfis[6]" style="flex: unset;" class="event-box current">
				<div class="event"> 
					<div class="dot"></div>
				</div>
			</div>
            <div ng-show="ticket.perfis[6]" style="flex:0.5;" class="event-box is-done">
				<div class="filling-bar"></div>
			</div>
			<!-- END Envio da Arte -->
            
            <!-- Envio da Arte -->
			<div ng-show="ticket.perfis[7]" style="flex: unset;" class="event-box current">
				<div class="event">
					<i class="event-icon fa fa-industry"></i>
					<span class="event-txt">PDF do<br />Fornecedor</span>
				</div>
			</div>
            <div ng-show="ticket.perfis[7]" style="flex:0.5;" class="event-box is-done">
				<div class="filling-bar"></div>
			</div>
            <div ng-show="ticket.perfis[7]" style="flex: unset;" class="event-box current">
				<div class="event"> 
					<div class="dot"></div>
				</div>
			</div>
            <div ng-show="ticket.perfis[7]" style="flex:0.5;" class="event-box is-done">
				<div class="filling-bar"></div>
			</div>
			<!-- END Envio da Arte -->
			
			
			<div style="flex: unset;" class="event-box current">
				<div class="event">
					<i class="event-icon fa fa-flag-checkered"></i>
					<span class="event-txt">Encerrado</span>
				</div>
			</div>
		</div>
		<!-- END Timeline -->
		<form name="formTicketHistory" action="" method="POST">
			<ul class="tabs clearfix" ng-cloak>
				<li><a ng-class="{'active' : activetab == '/visao-geral'}" ng-href="#/visao-geral">Visão Geral</a></li>
				<li><a ng-class="{'active' : activetab == '/anexos', 'disabled' : ticket.versao.numero === 0}" ng-href="#/anexos">Anexos</a></li>
				<li><a ng-class="{'active' : activetab == '/participantes', 'disabled' : ticket.versao.numero === 0}" ng-href="#/participantes">Participantes</a></li>
				<li><a ng-class="{'active' : activetab == '/tarefas', 'disabled' : ticket.versao.numero === 0}" ng-href="#/tarefas">Tarefas</a></li>
				<li><a ng-class="{'active' : activetab == '/mensagens', 'disabled' : ticket.versao.numero === 0}" ng-href="#/mensagens">Mensagens</a></li>
                <li><a ng-class="{'active': (activetab == '/pdf-fornecedor')}" ng-href="#/pdf-fornecedor">PDF do Fornecedor</a></li>
			</ul>
			<!-- resolve-loader -->
			<div resolve-loader></div>
			<!-- END resolve-loader -->
			<div class="fadein" ng-view></div>
        </form>
	</div>
	<div ng-show="artw_tab == 2" class="artwork-container margin-top-30 margin-bottom-40">
		<section class="artw-versions-history bs no-border">
			<div class="block margin-bottom-30">
				<table cellpading="0" cellspacing="0" id="artwVersionsHistory" class="artw-versions-table dataTable table-border table-hover">
					<thead>
						<tr>
							<th width=""><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Ticket</small></span></th>
							<th width="120"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Versão</small></span></th>
							<th width="134"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Encerramento</small></span></th>
						</tr>
					</thead>
					<tbody>
                        
                        @foreach ($todasVersoes as $v)
						<tr role="row">	
							<td width="">
                                <a href="{{ route('site.artwork.history', [$variacao->id, $v->id]) }}">
                                    <span class="icon fa fa-ticket"></span>
                                    
                                    @if ($item->compartilhado)
                                        <span class="text-side">{{ $v->artwTicket->numero }} • {{ $variacao->sku_nome_completo }} • {{ $v->artwTicket->sku_nome_completo }}</span>
                                    @else
                                        <span class="text-side">{{ $v->artwTicket->numero }} • {{ $v->artwTicket->sku_nome_completo }}</span>
                                    @endif
                                </a>
                            </td>
							<td width="120"><a href="{{ route('site.artwork.history', [$variacao->id, $v->id]) }}" class="txt-version"><span class="text-side no-margin">Versão {{ $v->numero }}</span></a></td>
							<td width="134"><a href="{{ route('site.artwork.history', [$variacao->id, $v->id]) }}" class="txt-closing-date"><span class="text-side no-margin">{{ $v->created_at->format('d/m/Y') }}</span></a></td>
						</tr>
                        @endforeach
                        
					</tbody>
				</table>
				<script>
					$(function () {
						var table = $('#artwVersionsHistory').DataTable({
							dom: '<"datatable-toolbar flexbox-container clearfix"' +
									'<"datatable-filter-container flex flex-3-large"f>' +
								'>' +
								'<"clearfix"' +
									'<tr>' +
								'>' +
								'<"datatable-pagination flexbox-container flex-align-center clearfix"' +
									'<"flex"<"select-control"l>><"flex flex-auto"p><"flex"i>' +
								'>',
							bAutoWidth: false,
							processing: false,
							serverSide: false,
							stateSave: true,
							order: [
								[0, 'desc']
							],
							pageLength: 25,
							lengthMenu: [
								[10, 25, 50, -1], 
								['Exibir 10 por página', 'Exibir 25 por página', 'Exibir 50 por página', 'Exibir todos']
							],
							language: {
								'url': '/js/datatables/language/Portuguese-Brasil.json'
							},
							initComplete: function () {
								$('#artwVersionsHistory_filter input[type="search"]').attr('placeholder', 'Buscar');
								
								var dataTables_filter_search = '<i></i>';
								var dataTables_filter_cancel = '<i></i>';
								
								$('.datatable-filter-container .dataTables_filter').append(dataTables_filter_search);
								$('.datatable-filter-container .dataTables_filter').append(dataTables_filter_cancel);
								$('.datatable-filter-container .dataTables_filter i').addClass('dataTables_filter_search');
								$('.datatable-filter-container .dataTables_filter i + i').addClass('dataTables_filter_cancel');

								function dataTablesFilterCancelShowHide() {
									if ($('.datatable-filter-container .dataTables_filter input[type="search"]').val() !== '') {
										$('.dataTables_filter_search').hide();
										$('.dataTables_filter_cancel').show();
									
									} else {
										$('.dataTables_filter_search').show();
										$('.dataTables_filter_cancel').hide();
										
										table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
									}
								}

								$(document).on('click', '.datatable-filter-container .dataTables_filter_cancel', function () {
									$('#artwVersionsHistory_filter input[type="search"]').val('');
									
									dataTablesFilterCancelShowHide();
								});

								// Remove accented character from search input as well
								$(document).on('keyup', '#artwVersionsHistory_filter input[type="search"]', function () {
									table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
									
									dataTablesFilterCancelShowHide();
								});

							},
							fnDrawCallback: function( oSettings ) {
								if (oSettings.aiDisplay.length < 1) {
									$('.dataTable thead tr').hide();
									$('.dataTable thead tr th').hide();
									$('.datatable-pagination').hide();
									$('.dataTables_empty').addClass('fa-ticket');
									$('.dataTables_empty').text('Nenhum ticket encontrado.');
								} else {
									$('.dataTable thead tr').show();
									$('.dataTable thead tr th').show();
									$('.datatable-pagination').show();
									$('.dataTables_empty').removeClass('fa-ticket');
									$('.dataTables_empty').text('');
								}
							}
						});
					});
				</script>
			</div>
		</section>
	</div>
</div>
@stop
