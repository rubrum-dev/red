@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
<script type="text/javascript" src="{{ asset('/js/site/artwork/ticket-edit.js') }}"></script>
<div class="content clearfix">
	<div class="top-head">
		<div class="top-packing clearfix">
			<div class="top-inline top-packing-logo">
				<img src="http://localhost:8080/images/uploads/logotipos/b744dda07d22f937577d161a906df23d.png" title="" alt="" />
			</div>
			<div class="top-inline top-packing-title">
				<h2>Skol Pilsen Lata 269ml</h2>
			</div>
			<div class="top-inline top-ticket-nav">
				<ul>
					<li><span class="icon-options-settings"></span> Ticket 171234</li>
					<li><span class="icon-officine"></span> Editar</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="ticket-timeline">
		<ul class="clearfix">
			<li class="current in-progress">Em edição <span class="icon-three-points"></span></li>
			<li class="current in-progress">Em revisão <span class="icon-list-square"></span></li>
			<li class="current">Marketing <span class="icon-cube"></span></li>
			<li class="">Em aprovação <span class="icon-text-justify-center"></span></li>
			<li class="">Aprovado <span class="icon-bag"></span></li>
			<li class="">Enviado <span class="icon-arrow-oblique-expand-directions"></span></li>
		</ul>
	</div>
	<div class="ticket-container">
		<section class="initial-info-section clearfix">
			<hr />
			<table border="0" cellpadding="0" cellspacing="0">
				<thead>
					<tr>
						<th width="480">Informações iniciais</th>
						<th width="80">Data de início</th>
						<th width="80">Arte enviada</th>
						<th width="80">Prazo final</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td width="480">Alteração de texto legal, código de barras e elementos gráficos. Solicitada pelo departamento jurídico e pela diretoria de marketing.  Prestar atenção no prazo pois este já foi estendido.</td>
						<td width="80">05/05/2017</td>
						<td width="80">15/05/2017</td>
						<td width="80">25/05/2017</td>
					</tr>
				</tbody>
			</table>
		</section>
		<section class="membership-info-section clearfix">
			<hr />
			<table border="0" cellpadding="0" cellspacing="0">
				<thead>
					<tr>
						<th width="400">Participantes</th>
						<th width="420">Aprovação</th>
						<th width="480">Reenviar Notificação</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td width="400">Tabata Quevedo</td>
						<td width="420">Aprovador</td>
						<td width="480"><a href="#" class="icon icon-mail"></a></td>
					</tr>
					<tr>
						<td width="400">Vanessa Doti</td>
						<td width="420">Marketing</td>
						<td width="480"><a href="#" class="icon icon-mail"></a></td>
					</tr>
					<tr>
						<td width="400">Fernanda Andrade</td>
						<td width="420"><span class="icon-status icon-is-ok"></span> Revisado às 16h46 de 07/05/17</td>
						<td width="480"><a href="#" class="icon icon-mail"></a></td>
					</tr>
					<tr>
						<td width="400">Rodrigo Korovichenco</td>
						<td width="420"><span class="icon-status icon-is-ok"></span> Participante</td>
						<td width="480"><a href="#" class="icon icon-mail"></a></td>
					</tr>
					<tr>
						<td width="400">Alessandro Leite</td>
						<td width="420"><span class="icon-status icon-is-ok"></span> Arte-final enviada às 10h00 de 07/05/17</td>
						<td width="480"><a href="#" class="icon icon-mail"></a></td>
					</tr>
				</tbody>
			</table>
		</section>
		<section class="alteration-cicle-section clearfix">
			<hr />
			<h4>Alterações do Ciclo 2</h4>
			<div class="alter-cicle-wrap">
				<div class="alteration-cicle-row clearfix">
					<div class="box-inline col-member-description">
						<span class="icon icon-grids"></span>
						<div class="description-txt clearfix">
							<h4>Elementos Gráficos</h4>
							<span>Fernando A.</span>
							<span>05/05/2017 às 17:00</span>
						</div>
					</div>
					<div class="box-inline col-general-description">
						<p>
							Inserir novo selo de qualidade criado pela agência. De preferência perto do código de
							barras. Inserir mais listras no fundo amarelo chapado. Inserir um fundo branco atrás
							do selo para dar mais ênfase.
						</p>
						<button type="button" style="display: none;"><span class="btn-icon icon-marker-points"></span> Comentar</button>
						<div class="chat-history chat-comment-container cicle-accordion">
							<h4 class="clearfix"><a href="#" class="teste-clique"><i class="accordion-state-txt">Exibir</i> todos <span class="icon-plus"></span></a></h4>
							<section class="chat-history-section cicle-accordion-section">
							<ul class="chat-comment-list">
								<li>
									<div class="comment">
										<h4 style="color: #33A752">Rodrigo Korovichenco às 10h22 de 07/05/17</h4>
										<p>
											O selo não ficou bom nesse local. Reposicionar no lado oposto.
										</p>
									</div>
									<button type="button" class="reply-btn" style="display: none;"><span class="icon-arrow-curve-right"></span> Responder</button>
									<ul>
										<li>
											<div class="comment">
												<h4 style="color: #EA7434">Vanessa Doti às 12h45 de 07/05/17</h4>
												<p>
													De agora em diante onde estiver grafado “bebida mista gaseificada” deverá ser grafada
													“bebida mista alcoólica gaseificada”.
												</p>
											</div>
										</li>
										<li>
											<div class="comment">
												<h4 style="color: #4185F3">Alessandro Leite às 11h23 de 07/05/17</h4>
												<p>
													No lado oposto interfere no texto legal.
												</p>
											</div>
										</li>
									</ul>
								</li>
							</ul>
							</section>
						</div>
					</div>
					<div class="box-inline col-edition-status">
						<div class="chk-edition-control clearfix">
							<input type="checkbox" checked disabled name="chkEditionIsOk[]" id="chkEditionIsOk_2" value="" class="check-edit-status" />
							<label for="chkEditionIsOk_2" class="chk-edition-fake"></label>
							<label for="chkEditionIsOk_2" class="chk-edition-txt">Revisado</label>
						</div>
						<div class="checked-info">
							<span>Fernanda A.</span>
							<span>07/05/17 às 16h35</span>
						</div>
					</div>
				</div>
			</div>
			<div class="cicle-accordion">
				<hr/>
				<h4><a href="#" class="teste-clique"><span class="icon-plus"></span><i class="accordion-state-txt">Exibir</i> alteraões do Ciclo 1</a></h4>
				<section class="cicle-accordion-section alteration-cicle">
					<div class="alteration-cicle-row clearfix">
						<div class="box-inline col-member-description">
							<span class="icon icon-text-width"></span>
							<div class="description-txt clearfix">
								<h4>Elementos Textuais</h4>
								<span>Fernando A.</span>
								<span>05/05/2017 às 17:00</span>
							</div>
						</div>
						<div class="box-inline col-general-description">
							<p>
								Alterar de “bebida mista” para “bebida mista gaseificada”. Alterar de “4,0% vol.
								alcoólico” para “7,0% vol. alcoólico”. Alterar a posição do código de barras, aprox.
								5mm para a direita. Aumentar 1 ponto no corpo da fonte em “350ml” na parte
								frontal.
							</p>
						</div>
						<div class="box-inline col-edition-status">
							<div class="chk-edition-control clearfix">
								<input type="checkbox" disabled name="chkEditionIsOk[]" id="chkEditionIsOk_1" checked class="check-edit-status" />
								<label for="chkEditionIsOk_1" class="chk-edition-fake"></label>
								<label for="chkEditionIsOk_1" class="chk-edition-txt">Revisado</label>
							</div>
							<div class="checked-info is-visible">
								<span>Fernando A.</span>
								<span>07/05/17 às 16h35</span>
							</div>
						</div>
					</div>
					<div class="alteration-cicle-row clearfix">
						<div class="box-inline col-member-description">
							<span class="icon icon-grids"></span>
							<div class="description-txt clearfix">
								<h4>Elementos Gráficos</h4>
								<span>Fernando A.</span>
								<span>05/05/2017 às 17:00</span>
							</div>
						</div>
						<div class="box-inline col-general-description">
							<p>
								Inserir novo selo de qualidade criado pela agência. De preferência perto do código de
								barras. Inserir mais listras no fundo amarelo chapado. Inserir um fundo branco atrás
								do selo para dar mais ênfase.
							</p>
						</div>
						<div class="box-inline col-edition-status">
							<div class="chk-edition-control clearfix">
								<input type="checkbox" disabled name="chkEditionIsOk[]" id="chkEditionIsOk_1" checked class="check-edit-status" />
								<label for="chkEditionIsOk_1" class="chk-edition-fake"></label>
								<label for="chkEditionIsOk_1" class="chk-edition-txt">Revisado</label>
							</div>
							<div class="checked-info is-visible">
								<span>Fernando A.</span>
								<span>07/05/17 às 16h35</span>
							</div>
						</div>
					</div>
					<div class="alteration-cicle-row clearfix">
						<div class="box-inline col-member-description">
							<span class="icon icon-text-center"></span>
							<div class="description-txt clearfix">
								<h4>Texto Legal</h4>
								<span>Fernando A.</span>
								<span>05/05/2017 às 17:00</span>
							</div>
						</div>
						<div class="box-inline col-general-description">
							<p>
								Alterar de “Endereço da indústria...” para “Endereço da fábrica...”. Alterar o n. de
								registro do MAPA para 543387698. Inverter a ordem do dos blocos de endereço e do
								CNPJ.
							</p>
						</div>
						<div class="box-inline col-edition-status">
							<div class="chk-edition-control clearfix">
								<input type="checkbox" disabled name="chkEditionIsOk[]" id="chkEditionIsOk_1" checked class="check-edit-status" />
								<label for="chkEditionIsOk_1" class="chk-edition-fake"></label>
								<label for="chkEditionIsOk_1" class="chk-edition-txt">Revisado</label>
							</div>
							<div class="checked-info is-visible">
								<span>Fernando A.</span>
								<span>07/05/17 às 16h35</span>
							</div>
						</div>
					</div>
				</section>
			</div>
		</section>
		<section class="file-attachment-section clearfix">
			<hr />
			<h4>Arquivos</h4>
			<ul class="file-attachment-list">
				<li>
					<span class="icon icon-document-fill"></span>
					<div class="file-attachment-txt">
						<h4>Selo criado pela agência</h4>
						<small>Anexado por Fernanda A. às 17h03 de 05/05/17</small>
					</div>
				</li>
				<li>
					<span class="icon icon-document-fill"></span>
					<div class="file-attachment-txt">
						<h4>Imagem referência logo sem borda e sombra</h4>
						<small>Anexado por Fernanda A. às 17h03 de 05/05/17</small>
					</div>
				</li>
			</ul>
		</section>
		<section class="update-attachment-section clearfix">
			<hr />
			<div class="box-inline update-attachment-column">
				<div class="updated-version-container">
					<h4>Layout Atualizado</h4>
					<div class="updated-version-wrap">
						<ul class="box-inline updated-version-list">
							<li>
								<span class="icon-axis-rules"></span>
								<div class="updated-version-txt">
									<h4>PDF Skol Pilsen Lata 269ml - Ciclo 1</h4>
									<small>Enviado por Alessandro Leite às 10h15 de 10/08/16</small>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="box-inline update-attachment-column">
				<div class="previous-version-container">
					<h4>Versão Anterior</h4>
					<div class="previous-version-wrap">
						<ul class="box-inline previous-version-list">
							<li>
								<span class="icon-axis-rules"></span>
								<div class="previous-version-txt">
									<h4>PDF Skol Pilsen Lata 269ml - Versão 1</h4>
									<small>Enviado por Alessandro Leite às 10h15 de 10/08/16</small>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<div class="btn-group">
			<hr />
			<button type="button"><span class="btn-icon-right icon-ban-circle"></span> Reprovar</button>
			<button type="submit"><span class="btn-icon-left icon-check"></span> Aprovar</button>
		</div>
	</div>
</div>
@stop
