@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
{!! Breadcrumbs::renderIfExists('artwork.solicitar_alteracao', $variacao->id) !!}
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />

<script>
	$(function () {
		var tfStartDate = $("#tfStartDate").val().split("/");
		var minDate = new Date(tfStartDate[2], tfStartDate[1] - 1, tfStartDate[0]);
		minDate.setDate(minDate.getDate() + 1);
		var minDate2 = new Date(tfStartDate[2], tfStartDate[1] - 1, tfStartDate[0]);
		minDate2.setDate(minDate2.getDate() + 2);

		var dateFormat = "dd/mm/yy",
			from = $("#tfArtSendDate")
			.datepicker({
				//defaultDate: "+1w",
				changeMonth: true,
				numberOfMonths: 1,
				minDate: minDate,
				beforeShow:function(textbox, instance){
                    var rect = textbox.getBoundingClientRect();
                    
                    setTimeout(function () {
                        $('.artw-approval-date-col').append($('#ui-datepicker-div'));
                    }, 0);
                }
			})
			.on("change", function () {
				//to.datepicker("option", "minDate", getDate(this, 1));
			}),
			to = $("#tfDeadlineDate").datepicker({
				//defaultDate: "+1w",
				changeMonth: true,
				numberOfMonths: 1,
				minDate: minDate,
				beforeShow:function(textbox, instance){
                    var rect = textbox.getBoundingClientRect();
                    
                    setTimeout(function () {
                        $('.artw-closure-date-col').append($('#ui-datepicker-div'));
                    }, 0);
                }
			})
			.on("change", function () {
				//from.datepicker("option", "maxDate", getDate(this, -1));
			});

		function getDate(element, qtd) {
			var date;
			
			try {
				var date2 = $.datepicker.parseDate(dateFormat, element.value);
				date2.setDate(date2.getDate() + qtd);
				date = date2;
			} catch (error) {
				date = null;
			}
			return date;
		}
	});
</script>
<script>
	var tipos = [];

	@if(old('cbAlterationType'))
		@foreach(old('cbAlterationType') as $tipo)
			tipos.push('{{ $tipo }}')
		@endforeach
	@endif

	var descricoes = [];

	@if(old('txtDescriptionSpec'))
		@foreach(old('txtDescriptionSpec') as $desc)
			descricoes.push('{{ html_entity_decode($desc, ENT_COMPAT, 'UTF-8') }}')
		@endforeach
	@endif

	var template = {};

	if (tipos && descricoes) {
		var solicitacoes = [];

		for (index = 0; index < tipos.length; ++index) {
			template = {
				'id': tipos[0],
				'descricao': descricoes[0]
			};
			solicitacoes.push(template);
		}
	} else {
		var solicitacoes = [
			//{'id': '1', 'descricao': 'Teste 1'},
			//{'id': '2', 'descricao': 'Teste 2'}
		]
	}
	
	var edicao = false;
</script>
<script src="{{ asset('/js/site/ui.datepicker-pt-BR.js') }}"></script>
<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload-shim.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload.min.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/embalagens_modal_compartilhar.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_create.js') }}"></script>
<script src="{{ asset2('/js/site/artwork/package-alteration-request.js') }}"></script>
<div class="content content-fluid" ng-app="app" ng-controller="TicketCreateCtrl" ng-cloak>
	@if (count($errors) > 0)
        <script>
            @foreach ($errors->all() as $error)
                myAlert('{!! $error !!}', 'warning');
            @endforeach
        </script>
    @endif
	
	@include('site.artwork.ticket_topo_inc')
	
	<!-- Form Criar Ticket -->
	{!! Form::open(array('name' => 'formAltRequest', 'id' => 'formAltRequest', 'class' => 'artw-ticket-create', 'files' => 'true', 'ng-submit' => 'validar($event)')) !!}
		@if (URL::previous() == route('site.artwork.package.create.get'))
			<input type="hidden" name="nova_embalagem" value="1"> 
		@else
			<input type="hidden" name="nova_embalagem" value="0"> 
		@endif
		<section class="margin-top-30">
			<div class="block">
				<h4 class="master-title-alt txt-bold margin-bottom-20">Informações</h4>
            	<div class="flexbox-container">
					<div class="artw-initial-info-col flex flex-fluid">
						<label class="master-title-sub label-control txt-small txt-uppercase">Resumo</label>
						<textarea placeholder="Descreva de forma objetiva" name="txtMainDescription" rows="6" id="txtMainDescription" class="input-control textbox-control">{{ old('txtMainDescription') }}</textarea>
					</div>
					<div class="artw-additional-info-col flex flex-auto">
						<div class="flexbox-container flexbox-group margin-bottom-40">
							<div class="flex flex-fluid">	
								<label class="master-title-sub label-control txt-small txt-uppercase">Projeto</label>
								<selectize placeholder="Escolha o projeto (opcional)" name="cbProjeto" id="cbProjeto" config="projetoConfig" options="projetos" ng-model="cbProjeto" required="true"></selectize>
								<input type="hidden" ng-model="cbProjeto" name="id_projeto" ng-value="cbProjeto">
							</div>
							<div ng-show="cbProjeto === '0'" class="flex flex-fluid">	
								<label class="master-title-sub label-control txt-small txt-uppercase">Nome do Projeto</label>
								<input type="text" placeholder="Digite o nome do projeto" name="tfNomeProjeto" id="tfNomeProjeto" class="input-control" />	
							</div>
							<div class="flex artw-project-name-col">	
								<label class="master-title-sub label-control txt-small txt-uppercase">Código</label>
								<input type="text" maxlength="25" placeholder="Código (opcional)" name="codigo_cliente" id="tfCodigoTicket" class="input-control only-alpha-numeric" />	
							</div>
						</div>
						<div class="flexbox-container flexbox-group">
							<div class="artw-start-date-col flex flex-fluid">
								<label class="master-title-sub label-control txt-small txt-uppercase">Início</label>
								<div class="date-control disabled readonly">	
									<input value='{{ date(' d/m/Y ') }}' type="text" readonly autocomplete="off" name="tfStartDate" placeholder="dd/mm/aaaa" id="tfStartDate" class="input-control" />
									<span class="addon fa fa-calendar"></span>
								</div>
							</div>
							<div class="artw-approval-date-col flex flex-fluid">
								<label class="master-title-sub label-control txt-small txt-uppercase">Aprovação</label>
								<div class="date-control">	
									<input onkeydown="return false;" type="text" autocomplete="off" name="tfArtSendDate" placeholder="dd/mm/aaaa" id="tfArtSendDate" class="input-control date-input-readonly" />
									<span class="addon fa fa-calendar"></span>
								</div>
							</div>
							<div class="artw-closure-date-col flex flex-fluid">
								<label class="master-title-sub label-control txt-small txt-uppercase">Encerramento</label>
								<div class="date-control">	
									<input onkeydown="return false;" type="text" autocomplete="off" name="tfDeadlineDate" placeholder="dd/mm/aaaa" value="{{ old('tfDeadlineDate') }}" id="tfDeadlineDate" class="date-input-readonly input-control" />
									<span class="addon fa fa-calendar"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div ng-cloak class="block">
				<label class="master-title-sub label-control txt-small txt-uppercase margin-left-10 margin-right-10">Instruções</label>
				<div id="alterationTypeWrap" class="input-fields-wrapper request-type">
					<div ng-repeat="(k, solicitacao) in solicitacoes" class="input-fields-group flexbox-container margin-bottom-20">
						<div class="artw-instructions-col flex flex-auto">
							<div class="flexbox-container">
								<div class="flex-12-large flex-auto margin-bottom-10 clearfix">
									<div class="select-control">	
										<select>
											<option value="">Escolha o tipo de alteração</option>
											<option ng-selected="solicitacao.id == tipo.id" ng-repeat="(k, tipo) in tipos" value="@{{ tipo.id }}">@{{ tipo.nome }}</option>
										</select>
										<input ng-value="solicitacao.id" type="hidden" name="cbAlterationType[]">
									</div>
								</div>
								<div class="flex-12-large flex-auto margin-bottom-10 clearfix">
									<textarea auto-height ng-model="solicitacao.descricao" placeholder="Descreva de forma específica" rows="3" class="input-control textbox-control textbox-auto-resizable">@{{ solicitacao.descricao }}</textarea>
									<input ng-value='solicitacao.descricao' type='hidden' name='txtDescriptionSpec[]'>
								</div>
							</div>
						</div>
						<div class="remove-instruction-col flex flex-auto">
							<button ng-click="remover(k)" class="btn-control btn-alt">Excluir</button>
						</div>
					</div>
					<div class="add-instruction-group input-fields-group flexbox-container margin-bottom-20">
						<div class="artw-instructions-col flex flex-auto">
							<div class="flexbox-container">
								<div class="flex-12-large flex-auto margin-bottom-10 clearfix">
									<div class="select-control">	
										<select ng-model="add_tipo">
											<option value="">Escolha o tipo de alteração</option>
											<option ng-repeat="(k, tipo) in tipos" value="@{{ tipo.id }}">@{{ tipo.nome }}</option>
										</select>
									</div>
								</div>
								<div class="flex-12-large flex-auto margin-bottom-10 clearfix">
									<textarea auto-height ng-model="add_descricao" placeholder="Descreva de forma específica" rows="3" class="input-control textbox-control textbox-auto-resizable"></textarea>
								</div>
							</div>
						</div>
						<div class="add-instruction-col flex flex-auto">
							<button ng-click="adicionar()" type="button" class="btn-control btn-positive">Adicionar</button>
						</div>
					</div>
				</div>
			</div>
			<div ng-cloak class="block margin-bottom-30">
				<h4 class="master-title-alt txt-bold margin-bottom-20">Anexos</h4>
				<div id="fileAttachWrap" class="fileAttachWrap block margin-bottom-20">
					<div class="artw-file-upload input-fields-wrapper">
						<div class="input-fields-group artw-attach-files-group artw-attach-files">
							<div class="files-control-multi flexbox-container margin-bottom-10">
								<div class="flex artw-attach-files-col">
									<input type="file" accept=".docx,.doc,.pdf,.xml,.jpg,.jpeg,.png,.bmp,.gif,.tif,.tiff,.psd,.eps,.ai,.svg,.ppt,.pptx,.xls,.xlsx,.zip,.rar,.7z,.txt" valid-file=".docx,.doc,.pdf,.xml,.jpg,.jpeg,.png,.bmp,.gif,.tif,.tiff,.psd,.eps,.ai,.svg,.ppt,.pptx,.xls,.xlsx,.zip,.rar,.7z,.txt" name="artworkFileUpload[]" class="valid-file-ext" />
									<span class="files-control-fake"><strong></strong><i class="addon fa fa-paperclip"></i></span>
								</div>
								<div class="btn-add flex add-files-submit-col">
									<button type="button" id="btnAddFileAttach" class="files-btn btn-add-fields">Adicionar</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			{{--<div ng-cloak class="block margin-bottom-30">
				<h4 class="master-title-alt txt-bold margin-bottom-20">Anexos</h4>
				<div id="fileAttachWrap" class="fileAttachWrap block margin-bottom-20">
					<div class="artw-file-upload input-fields-wrapper">
						<div class="input-fields-group artw-attach-files-group artw-attach-files">
							<div class="files-control flexbox-container margin-bottom-10">
								<div class="flex artw-attach-files-col">
									<input type="file" accept=".docx,.doc,.pdf,.xml,.jpg,.jpeg,.png,.bmp,.gif,.tif,.tiff,.psd,.eps,.ai,.ppt,.pptx,.xls,.xlsx,.zip,.rar,.7z,.txt" valid-file=".docx,.doc,.pdf,.xml,.jpg,.jpeg,.png,.bmp,.gif,.tif,.tiff,.psd,.eps,.ai,.ppt,.pptx,.xls,.xlsx,.zip,.rar,.7z,.txt" ng-model="arquivo" name="arquivo" id="arquivo" />
									<span class="files-control-fake"><strong></strong><i class="addon fa fa-paperclip"></i></span>
								</div>
								<div class="btn-add flex add-files-submit-col">
									<button type="button" ng-click="add_arquivo(arquivo)" id="btnAddFileAttach" class="files-btn btn-add-fields">Adicionar</button>
								</div>
							</div>
						</div>
					</div>
				</div>
                <!-- Arquivos / Anexos -->
                <ul class="list-group">
                    <li ng-repeat="arquivo in arquivos" class="list-group-item">
                        <div class="panel primary panel-hollow">
                            <a target="_blank" ng-href="" class="link"></a>
                            <i ng-class="fileExtensionClass(arquivo)" class="icon fa"></i>
                            <div class="panel-body">
                                <h4 class="panel-heading txt-semibold no-margin">@{{ arquivo.descricao }}</h4>
                                <small class="description-small txt-regular no-margin">Anexado às @{{ arquivo.data_criacao }} - <strong class="txt-small txt-regular">@{{ arquivo.filesize | fileSize }}</strong></small>
                            </div>

                            <div class="panel-options dropdown drop-to-left pull-right">
                                <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-btn dropdown-toggle  fa fa-ellipsis-h"></a>
                                <div class="dropdown-list">
                                    <ul>    
                                        <li><a href="javascript:;" ng-click="remover_arquivo($index);" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </li>
                </ul>
                <!-- END Arquivos / Anexos -->
			</div>--}}
		</section>
		<!-- Button group -->
		<div class="btn-group txt-center margin-bottom-40">
			<div class="bs border-color-primary padding-top-40">
				<button type="submit" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-right">
					<span><i class="icon fa fa-arrow-right"></i> Próxima Etapa</span>
				</button>
			</div>
		</div>
		<!-- END Button group -->
	{!! Form::close() !!}
	<!-- END Form Criar Ticket -->
</div>
@stop
