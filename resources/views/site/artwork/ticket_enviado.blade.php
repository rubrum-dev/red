@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
<script type="text/javascript" src="{{ asset('/js/site/artwork/ticket-edit.js') }}"></script>
<div class="content clearfix">
	<div class="top-head">
		<div class="top-packing clearfix">
			<div class="top-inline top-packing-logo">
				<img src="http://localhost:8080/images/uploads/logotipos/b744dda07d22f937577d161a906df23d.png" title="" alt="" />
			</div>
			<div class="top-inline top-packing-title">
				<h2>Skol Pilsen Lata 269ml</h2>
			</div>
			<div class="top-inline top-ticket-nav">
				<ul>
					<li><span class="icon-options-settings"></span> Ticket 171234</li>
					<li><span class="icon-arrow-curve-recycle"></span> Ciclo 2</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="ticket-timeline">
		<ul class="clearfix">
			<li class="current in-progress">Em edição <span class="icon-three-points"></span></li>
			<li class="current in-progress">Em revisão <span class="icon-list-square"></span></li>
			<li class="current in-progress">Marketing <span class="icon-cube"></span></li>
			<li class="current in-progress">Em aprovação <span class="icon-text-justify-center"></span></li>
			<li class="current in-progress">Aprovado <span class="icon-bag"></span></li>
			<li class="current">Enviado <span class="icon-arrow-oblique-expand-directions"></span></li>
		</ul>
	</div>
	<div class="ticket-container">
		<form name="formTicketEdit" action="" method="POST" class="ticket-alt-request-form">
			<section class="initial-info-section clearfix">
				<hr />
				<table border="0" cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<th width="480">Informações iniciais</th>
							<th width="80">Data de início</th>
							<th width="80">Arte enviada</th>
							<th width="80">Prazo final</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td width="480">Alteração de texto legal, código de barras e elementos gráficos. Solicitada pelo departamento jurídico e pela diretoria de marketing.  Prestar atenção no prazo pois este já foi estendido.</td>
							<td width="80">05/05/2017</td>
							<td width="80">15/05/2017</td>
							<td width="80">25/05/2017</td>
						</tr>
					</tbody>
				</table>
			</section>
			<section class="membership-info-section clearfix">
				<hr />
				<table border="0" cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<th width="400">Participantes</th>
							<th width="580">Aprovação</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td width="400">Tabata Quevedo</td>
							<td width="580"><span class="icon-status icon-is-ok"></span> Aprovado às 12h35 de 13/05/17</td>
						</tr>
						<tr>
							<td width="400">Vanessa Doti</td>
							<td width="580"><span class="icon-status icon-is-ok"></span> Aprovado às 10h45 de 13/05/17</td>
						</tr>
						<tr>
							<td width="400">Fernanda Andrade</td>
							<td width="580"><span class="icon-status icon-is-ok"></span> Aprovado Merketing às 13h45 de 11/05/17</td>
						</tr>
						<tr>
							<td width="400">Rodrigo Korovichenco</td>
							<td width="580"><span class="icon-status icon-is-ok"></span> Participante</td>
						</tr>
						<tr>
							<td width="400">Alessandro Leite</td>
							<td width="580"><span class="icon-status icon-is-ok"></span> Arte-final enviada às 10h00 de 07/05/17</td>
						</tr>
					</tbody>
				</table>
			</section>
			<section class="alteration-cicle-section clearfix">
				<hr />
				<h4>Alterações do Ciclo 2</h4>
				<div class="alter-cicle-wrap">
					<div class="alteration-cicle-row clearfix">
						<div class="box-inline col-member-description">
							<span class="icon icon-text-width"></span>
							<div class="description-txt clearfix">
								<h4>Elementos Gráficos</h4>
								<span>Fernando A.</span>
								<span>05/05/2017 às 17:00</span>
							</div>
						</div>
						<div class="box-inline col-general-description">
							<p>
								Alterar de “bebida mista” para “bebida mista gaseificada”. Alterar de “4,0% vol.
								alcoólico” para “7,0% vol. alcoólico”. Alterar a posição do código de barras, aprox.
								5mm para a direita. Aumentar 1 ponto no corpo da fonte em “350ml” na parte
								frontal.
							</p>
							<div class="chat-comment-container">
								<ul class="chat-comment-list">
									<li>
										<div class="comment">
											<h4 style="color: #EA4334">Fernanda Andrade às 19h02 de 06/05/17</h4>
											<p>
												De agora em diante onde estiver grafado “bebida mista gaseificada” deverá ser grafada
												“bebida mista alcoólica gaseificada”.
											</p>
										</div>
									</li>
								</ul>
							</div>
						</div>
						<div class="box-inline col-edition-status">
							<div class="chk-edition-control clearfix">
								<input type="checkbox" checked disabled name="chkEditionIsOk[]" id="chkEditionIsOk_1" value="" class="check-edit-status" />
								<label for="chkEditionIsOk_1" class="chk-edition-fake"></label>
								<label for="chkEditionIsOk_1" class="chk-edition-txt">Revisado</label>
							</div>
							<div class="checked-info is-visible">
								<span>Fernanda A.</span>
								<span>07/05/17 às 16h35</span>
							</div>
						</div>
					</div>
				</div>
				<div class="cicle-accordion">
					<hr/>
					<h4><a href="#" class="teste-clique"><span class="icon-plus"></span><i class="accordion-state-txt">Exibir</i> alteraões do Ciclo 1</a></h4>
					<section class="cicle-accordion-section alteration-cicle">
						<div class="alteration-cicle-row clearfix">
							<div class="box-inline col-member-description">
								<span class="icon icon-text-width"></span>
								<div class="description-txt clearfix">
									<h4>Elementos Textuais</h4>
									<span>Fernando A.</span>
									<span>05/05/2017 às 17:00</span>
								</div>
							</div>
							<div class="box-inline col-general-description">
								<p>
									Alterar de “bebida mista” para “bebida mista gaseificada”. Alterar de “4,0% vol.
									alcoólico” para “7,0% vol. alcoólico”. Alterar a posição do código de barras, aprox.
									5mm para a direita. Aumentar 1 ponto no corpo da fonte em “350ml” na parte
									frontal.
								</p>
							</div>
							<div class="box-inline col-edition-status">
								<div class="chk-edition-control clearfix">
									<input type="checkbox" disabled name="chkEditionIsOk[]" id="chkEditionIsOk_1" checked class="check-edit-status" />
									<label for="chkEditionIsOk_1" class="chk-edition-fake"></label>
									<label for="chkEditionIsOk_1" class="chk-edition-txt">Revisado</label>
								</div>
								<div class="checked-info is-visible">
									<span>Fernando A.</span>
									<span>07/05/17 às 16h35</span>
								</div>
							</div>
						</div>
						<div class="alteration-cicle-row clearfix">
							<div class="box-inline col-member-description">
								<span class="icon icon-grids"></span>
								<div class="description-txt clearfix">
									<h4>Elementos Gráficos</h4>
									<span>Fernando A.</span>
									<span>05/05/2017 às 17:00</span>
								</div>
							</div>
							<div class="box-inline col-general-description">
								<p>
									Inserir novo selo de qualidade criado pela agência. De preferência perto do código de
									barras. Inserir mais listras no fundo amarelo chapado. Inserir um fundo branco atrás
									do selo para dar mais ênfase.
								</p>
							</div>
							<div class="box-inline col-edition-status">
								<div class="chk-edition-control clearfix">
									<input type="checkbox" disabled name="chkEditionIsOk[]" id="chkEditionIsOk_1" checked class="check-edit-status" />
									<label for="chkEditionIsOk_1" class="chk-edition-fake"></label>
									<label for="chkEditionIsOk_1" class="chk-edition-txt">Revisado</label>
								</div>
								<div class="checked-info is-visible">
									<span>Fernando A.</span>
									<span>07/05/17 às 16h35</span>
								</div>
							</div>
						</div>
						<div class="alteration-cicle-row clearfix">
							<div class="box-inline col-member-description">
								<span class="icon icon-text-center"></span>
								<div class="description-txt clearfix">
									<h4>Texto Legal</h4>
									<span>Fernando A.</span>
									<span>05/05/2017 às 17:00</span>
								</div>
							</div>
							<div class="box-inline col-general-description">
								<p>
									Alterar de “Endereço da indústria...” para “Endereço da fábrica...”. Alterar o n. de
									registro do MAPA para 543387698. Inverter a ordem do dos blocos de endereço e do
									CNPJ.
								</p>
							</div>
							<div class="box-inline col-edition-status">
								<div class="chk-edition-control clearfix">
									<input type="checkbox" disabled name="chkEditionIsOk[]" id="chkEditionIsOk_1" checked class="check-edit-status" />
									<label for="chkEditionIsOk_1" class="chk-edition-fake"></label>
									<label for="chkEditionIsOk_1" class="chk-edition-txt">Revisado</label>
								</div>
								<div class="checked-info is-visible">
									<span>Fernando A.</span>
									<span>07/05/17 às 16h35</span>
								</div>
							</div>
						</div>
					</section>
				</div>
			</section>
			<section class="file-attachment-section clearfix">
				<hr />
				<h4>Arquivos</h4>
				<ul class="file-attachment-list file-attachment-list-active">
					<li>
						<span class="icon icon-document-fill"></span>
						<div class="file-attachment-txt">
							<h4>Selo criado pela agência</h4>
							<small>Anexado por Fernanda A. às 17h03 de 05/05/17</small>
						</div>
					</li>
					<li>
						<span class="icon icon-document-fill"></span>
						<div class="file-attachment-txt">
							<h4>Imagem referência logo sem borda e sombra</h4>
							<small>Anexado por Fernanda A. às 17h03 de 05/05/17</small>
						</div>
					</li>
				</ul>
			</section>
			<section class="update-attachment-section clearfix">
				<hr />
				<div class="box-inline update-attachment-column">
					<div class="updated-version-container">
						<h4>Layout Atualizado</h4>
						<div class="updated-version-wrap">
							<ul class="box-inline updated-version-list">
								<li>
									<span class="icon-axis-rules"></span>
									<div class="updated-version-txt">
										<h4>PDF Skol Pilsen Lata 269ml - Versão 1</h4>
										<small>Enviado por Alessandro Leite às 10h15 de 10/08/16</small>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-inline update-attachment-column">
					<div class="previous-version-container">
						<h4>Ciclo Anterior</h4>
						<div class="previous-version-wrap">
							<ul class="box-inline previous-version-list">
								<li>
									<span class="icon-axis-rules"></span>
									<div class="previous-version-txt">
										<h4>PDF Skol Pilsen Lata 269ml - Versão 1</h4>
										<small>Enviado por Alessandro Leite às 10h15 de 10/08/16</small>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
			<section class="artwork-upload-section clearfix">
				<hr />
				<h4>Enviar arte final</h4>
				<div class="send-artwork-container clearfix">
					<div class="send-artwork-group clearfix">
						<ul class="send-artwork-list box-inline">
							<li>
								<span class="icon-axis-rules"></span>
								<div class="send-artwork-txt">
									<h4>PDF Skol Pilsen Lata 269ml - Versão 1</h4>
									<small>Enviado por Alessandro Leite às 10h15 de 10/08/16</small>
								</div>
							</li>
						</ul>
						<div class="send-artwork-input-group box-inline">
							<div class="box-inline select-control">
								<select name="cbSupplierChooser">
									<option value="">Selecionar o fornecedor</option>
								</select>
							</div>
							<div class="col-supplier box-inline input-control">
								<input type="text" placeholder="Email" name="tfEmailSupplier" />
							</div>
							<button type="button"><span class="icon icon-arrow-right-light"></span></button>
						</div>
					</div>
					<div class="send-artwork-txt-info">
						<p>
							Selecione o fornecedor ou digite o e-mail para enviar a arte-final. Uma cópia do PDF de aprovação também será enviado automaticamente.
						</p>
					</div>
				</div>
				<div class="file-attachment-container clearfix">
					<div class="box-inline file-attachment-control file-attachment-col clearfix">
						<span class="icon-paperclip-oblique">Anexar</span>
						<input file-upload type="file" name="artworkFileUpload" value="" placeholder="Nenhum arquivo" />
						<span class="file-placeholder">Procurar...</span>
					</div>
					<div class="box-inline tf-legend-col">
						<input ng-model="legenda" type="text" name="" value="" placeholder="Legenda" class="input-control" />
					</div>
					<button ng-click="uploadArquivo(ticket.id)" type="button"><span class="btn-icon icon-upload"></span> Carregar</button>
				</div>
			</section>
			<section class="artwork-send-history-section">
				<hr />
				<div class="send-artwork-container clearfix">
					<div class="send-artwork-col box-inline">
						<h4>Envios de Artwork</h4>
						<ul class="send-artwork-list">
							<li>
								<span class="icon-axis-rules"></span>
								<div class="send-artwork-txt">
									<h4>Arte-final Skol Pilsen Lata 269ml</h4>
									<small>Anexado por Alessandro Leite às 12h00 de 14/05/17</small>
								</div>
							</li>
							<li>
								<span class="icon-axis-rules"></span>
								<div class="send-artwork-txt">
									<h4>Arte-final Skol Pilsen Lata 269ml</h4>
									<small>Anexado por Alessandro Leite às 12h00 de 14/05/17</small>
								</div>
							</li>
						</ul>
					</div>
					<div class="mail-recipients-col box-inline">
						<h4>Destinatários</h4>
						<ul class="send-artwork-list">
							<li>
								<span class="icon-mail"></span>
								<div class="send-artwork-txt">
									<h4>Ball Corporation</h4>
									<small>marketing@ball.com.br, controle@ball.com.br, qualidade@ball.com.br</small>
									<small>Enviado por Fernanda A. às 12h15 de 14/05/17</small>
								</div>
							</li>
							<li>
								<span class="icon-mail"></span>
								<div class="send-artwork-txt">
									<h4>atendimento@agencia.com.br</h4>
									<small>Enviado por Fernanda A. às 12h00 de 25/08/17</small>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</section>
			<div class="btn-group">
				<hr />
				<button type="submit"><span class="btn-icon-left icon-arrow-oblique-expand-directions"></span> Declarar Arte-Final Enviada</button>
			</div>
		</form>
	</div>
</div>
@stop
