@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm.css') }}" />
{{--<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm-custom.css')}}" />--}}
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />
{{--<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/artwork.css') }}" />--}}

<script>
	var id_ticket = {{$ticket->id}};
	var csrf_token = '{{ csrf_token() }}';
</script>
<script src="{{ asset('/js/angular/angular.min.js')}}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js')}}"></script>
<script src="{{ asset2('/js/angular/controllers/embalagens_modal_compartilhar.js') }}"></script>
<script src="{{ asset('/js/angular/v-accordion.min.js')}}"></script>
<script src="{{ asset('/js/angular/angular-sanitize.min.js')}}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js')}}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload-shim.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload.min.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>

<script src="{{ asset2('/js/angular/enviar-arte-resolve-loader.js') }}"></script>
<script src="{{ asset2('/js/angular/pdf-fornecedor-resolve-loader.js') }}"></script>

<script src="{{ asset2('/js/angular/controllers/ticket_edicao_aba_visao_geral.js') }}"></script>

<script src="{{ asset2('/js/angular/controllers/ticket_edicao_aba_pdf_fornecedor.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edicao.js')}}"></script>

{!! Breadcrumbs::renderIfExists('artwork.enviar_arte', $variacao->id) !!}

<div class="content content-fluid" ng-app="app" ng-controller="TicketEdicaoCtrl" ng-cloak>
	<h3 class="master-title master-title-alt margin-top-10 margin-bottom-30 padding-bottom-20">Enviar Arte-Final</h3>
	<div class="artwork-container margin-bottom-40">
		
        @include('site.artwork.ticket_topo_compartilhado_inc')
        
		<div resolve-loader></div>

		<form class="fadein" ng-show="$root.loaded" ng-submit="$event.preventDefault()" name="formTicketEdit" action="" method="POST">
			<div ng-controller="abaVisaoGeralCtrl">

				<!-- Layout de Aprovação / Arte-Final -->
				<div class="block margin-top-20 margin-left-10 margin-right-10">
					<div class="block">
						<h4 class="master-title-sub txt-light txt-uppercase">Layout de Aprovação</h4>
						<div id="layout-aprovacao" class="block">
							<div ng-mouseenter="interval_tickets_stop()" class="panel secondary">
								<a target="_blank" href="{{ route('site.artwork.baixarPdf', [$versao->id, 'versao']) }}" class="link"></a>
								<i class="icon fa fa-file-pdf-o"></i>
								<div class="panel-body">
									<h4 class="panel-heading txt-semibold">{{ str_replace(' - ', ' • ', $versao->nome) }}</h4>
									<small class="description-small txt-small txt-regular no-margin">Anexado por @{{ticket.ciclo_atual.layout.nome_usuario}} às @{{ticket.ciclo_atual.layout.data_criacao}} - <strong class="txt-small txt-regular">@{{ ticket.ciclo_atual.layout.filesize }} MB</strong></small>
								</div>
							</div>
						</div>
					</div>
					<div class="block" ng-repeat="(a, arte) in ticket.artes">
						<h4 class="master-title-sub txt-light txt-uppercase">Arte-final</h4>
						<div id="envio-arte" class="block">
							<div ng-mouseenter="interval_tickets_stop()" class="panel secondary">
								<a ng-href="@{{arte.path}}" class="link"></a>
								<i class="icon fa fa-file-archive-o"></i>
								<div class="panel-body">
									<h4 class="panel-heading txt-semibold">@{{arte.nome}} • Versão {{ $versao->numero }}</h4>
									<small class="description-small txt-small txt-regular no-margin">Anexado por @{{arte.nome_usuario}} às @{{arte.data_criacao}}</small>
									<small class="description-small txt-small txt-regular no-margin">Arquivo original: @{{arte.path_info.basename}} - <strong class="txt-small txt-regular">@{{ arte.filesize }} MB</strong></small>
								</div>
							</div>
						</div>
						<h4 class="master-title-sub txt-light txt-uppercase">Enviar Arte-final</h4>
						<div class="block">
							<div class="artw-sending-group flexbox-container flexbox-group">
								<div class="flex flex-3-large">
									<div class="select-control">
										<select ng-focus="interval_tickets_stop()" ng-mouseenter="interval_tickets_stop()" ng-change="setar_fornecedor(a)" ng-model="arte.fornecedorSelecionado" name="cbSupplierChooser" id="cbSupplierChooser">
											<option value="">Selecionar o fornecedor</option>
											<option ng-repeat="(f, fornecedor) in fornecedores" value="@{{ f }}" ng-value="@{{ f }}">@{{ fornecedor.nome }}</option>
										</select>
									</div>
								</div>
								<div class="flex flex-fluid">
									<input ng-focus="interval_tickets_stop()" ng-mouseenter="interval_tickets_stop()" ng-model="enviar_emails[a]" type="text" placeholder="Digite os e-mails separados por vígula" name="tfEmailSupplier" id="tfEmailSupplier" class="input-control" />
								</div>
							</div>
							<div class="flexbox-container flexbox-group margin-top-20">
								<div class="flex flex-fluid">    
									<textarea rows="4" placeholder="Escreva uma mensagem para o fornecedor" ng-model="arte.msg_fornecedor" name="msg_fornecedor" id="msg_fornecedor" class="input-control textbox-control"></textarea>
								</div>
							</div>
							<div class="flexbox-container flexbox-group margin-top-20">
								<div class="flexbox-container margin-left-auto">
									<div class="flex">
										<button type="reset" class="btn-control btn-alt">Cancelar</button>
									</div>
									<div class="flex col-artwork-submit">   
										<button ng-click="enviar_emails(a, arte.id)" type="button" class="btn-control btn-positive">Enviar</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END Layout de Aprovação / Arte-Final -->
				<!-- Envio da Arte-final -->
				<div ng-show="ticket.id_status >= 5 && ticket.artes_envios.length" class="block margin-left-10 margin-right-10 cleaffix">
					<h4 class="master-title-sub txt-light txt-uppercase">Envios Arte-final</h4>
						
					<!-- Panel Group -->
					<div class="panel primary panel-group no-hover">
						<!-- Panel group items -->
						<div ng-repeat="(a, envio) in ticket.artes_envios" class="flexbox-container item">
							<i class="icon fa fa-paper-plane"></i>
							<div class="panel-body">
								<h4 class="panel-heading" ng-show="envio.fornecedor"><span class="txt-semibold">@{{envio.fornecedor}}</span></h4>
								<h4 class="panel-heading" ng-show="!envio.fornecedor"><span class="txt-semibold">@{{envio.emails}}</span></h4>
								<small class="description-small txt-regular" ng-show="envio.fornecedor">@{{envio.emails}}</small>
								<small ng-if="!envio.cancelado_em" class="description-small txt-regular">Enviado por @{{envio.nome_usuario}} às @{{envio.data_criacao}}</small>
                                <small ng-if="envio.cancelado_em" class="description-small txt-regular">Cancelado por @{{envio.cancelado_por}} às @{{envio.cancelado_em}}</small>
								<div class="flex artw-downloaded-info clearfix">
									<i class="icon txt-color-alt txt-color-off fa fa-download"></i>
									<strong ng-if="envio.downloads === 0" class="artw-has-downloaded txt-semibold txt-color-alt margin-bottom-5">Arte-Final ainda não foi baixada</strong>
									<strong ng-if="envio.downloads === 1" class="artw-has-downloaded txt-semibold txt-color-default margin-bottom-5">Arte-final foi baixada @{{ envio.downloads }} vez</strong>
									<strong ng-if="envio.downloads !== 1 && envio.downloads !== 0" class="artw-has-downloaded txt-semibold txt-color-default margin-bottom-5">Arte-final foi baixada @{{ envio.downloads }} vezes</strong>
									<small ng-if="!envio.cancelado_em && envio.dt_expiracao && envio.status && !envio.expirado" class="access-link-expires txt-regular txt-small">Link de acesso às artes está ativo expira em @{{ envio.dt_expiracao }}</small>
									<small ng-if="!envio.cancelado_em && envio.dt_expiracao && !envio.status && !envio.expirado" class="access-link-expires txt-regular txt-small">Link de acesso às artes está inativo expira em @{{ envio.dt_expiracao }}</small>
									<small ng-if="!envio.cancelado_em && envio.dt_expiracao && envio.expirado" class="access-link-expires txt-regular txt-small">Link de acesso às artes expirou em @{{ envio.dt_expiracao }}</small>
								</div>
							</div>
							<div ng-if="!envio.cancelado_em" ng-mouseenter="interval_tickets_stop()" class="panel-options dropdown drop-to-left pull-right">
								<a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-template="Opções" tooltip-hide-trigger="click touchstart touchend mouseleave" href="javascript:;" class="dropdown-btn dropdown-toggle fa fa-ellipsis-h"></a>
								<div class="dropdown-list">
									<ul>
										<li>
											<label ng-class="{'dropdown-switch switch-control switch-control-flex animate disabled': envio.expirado}" class="dropdown-switch switch-control switch-control-flex animate">
												<input ng-click="setStatusEnvio(envio.id)" ng-checked="envio.status" type="checkbox" ng-disabled="envio.expirado" name="link_arte[]" />
												<span class="switch-toggle abs-right-offset"></span>
												<strong ng-if="envio.status && !envio.expirado" class="switch-txt switch-txt-icon fa fa-link no-margin"><small class="txt-small txt-bold">Link Ativo</small></strong>
												<strong ng-if="!envio.status && !envio.expirado" class="switch-txt switch-txt-icon fa fa-unlink no-margin"><small class="txt-small txt-bold">Link Inativo</small></strong>
												<strong ng-if="envio.expirado" class="switch-txt switch-txt-icon txt-color-alt fa fa-chain-broken no-margin"><span class="txt-small txt-color-alt txt-bold">Link Expirado</span></strong>
											</label>
										</li>    
										{{--<li><a href="javascript:;" ng-click="renovarLink(envio.id)" ng-class="{'disabled txt-color-alt': !envio.status || envio.expirado }" class="txt-bold"><span class="icon fa fa-refresh"></span>Renovar Link</a></li>--}}
										{{--<li><a href="javascript:;" ng-click="reenviarEmail(envio.id)" class="txt-bold"><span class="icon fa fa-paper-plane"></span>Reenviar E-mails</a></li>--}}
										<li><a href="javascript:;" ng-click="verMsgFornecedor(envio.mensagem)" class="txt-bold"><span class="icon fa fa-envelope"></span>Ver Mensagem</a></li>
										<li ng-if="envio.expirado"><a href="javascript:;" class="txt-bold disabled"><span class="icon fa fa-times-circle"></span>Cancelar Envio</a></li>
										<li ng-if="!envio.expirado"><a href="javascript:;" ng-click="cancelarEnvio(envio.id)" class="txt-bold"><span class="icon fa fa-times-circle"></span>Cancelar Envio</a></li>
									</ul>
								</div>
							</div>
						</div>
						<!-- END Panel group items -->
					</div>
					<!-- END Panel Group -->
				</div>
				<!-- END Envio da Arte-final -->

			</div>
		</form>
	</div>
</div>
@stop
