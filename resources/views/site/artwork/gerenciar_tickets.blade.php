@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
{!! Breadcrumbs::renderIfExists('artwork.manager') !!}
<script type="text/javascript" src="{{ asset('/js/site/artwork/manage-tickets-sorter.js') }}"></script>
<div class="content clearfix">
    <div class="top-head">
        <div class="top-inline top-head-title">
		    <h2 class="text-left"><span>Gerenciador de tickets</span></h2>
        </div>
        <div class="top-inline top-nav-right">
		    <ul>
                <li><a @if ($tipo == 'todos')class="active"@endif href="{{ route('site.artwork.manager') }}"><span class="icon icon-mailbox"></span> Todos os Tickets</a></li>
                <li><a @if ($tipo == 'meus')class="active"@endif href="{{ route('site.artwork.manager', ['meus']) }}"><span class="icon icon-credit-card"></span> Meus Tickets</a></li>
                <li><a @if ($tipo == 'compartilhados')class="active"@endif href="{{ route('site.artwork.manager', ['compartilhados']) }}"><span class="icon icon-social-sharethis"></span> Compartilhados</a></li>
            </ul>
        </div>
    </div>
    <div class="sort-tickets-container clearfix">
        <div class="sort-tickets-inline sort-tickets-status">
            <a @if ($tipo == 'minhas_tarefas')class="active"@endif href="{{ route('site.artwork.manager', ['minhas_tarefas']) }}"><span>{{ count($minhas) }}</span><span>Minhas Tarefas</span><span class="icon icon-speed"></span></a>
        </div>
        <div class="sort-tickets-inline sort-tickets-status">
            <a @if ($tipo == 'atrasadas')class="active"@endif href="{{ route('site.artwork.manager', ['atrasadas']) }}"><span>{{ count($atrasadas) }}</span><span>Atrasadas</span><span class="icon icon-alarm-clock"></span></a>
        </div>
        <form name="sortTicketsByStatus" action="" method="POST" class="sort-tickets-inline sort-tickets-form">
            <div class="sort-tickets-control">
                <select name="cboTicketStatus">
                    <option selected="selected">Filtrar status dos tickets</option>
                    @foreach ($status as $value)
                        <option value="{{ $value->nome }}">{{ $value->nome }}</option>
                    @endforeach
                </select>
            </div>
        </form>
        <div class="sort-tickets-inline new-package-box">
            <a href="{{ url('/site/artwork/package/create') }}" class="new-package-btn"><span class="icon-magic-wand"></span> Nova Embalagem</a>
        </div>
    </div>
    <div class="table-tickets-container">
        <table cellpadding="0" cellspacing="0" id="myTable2" class="sort-tickets-table table-hover">
            <thead>
                <tr>
                    <th width="50" onclick="sortTable(0)"><a href="javascript:;">Todos os Tickets <span class="tickets-counter">{{ count($tickets) }}</span> <span href="#" class="icon icon-arrow-down-thin"></span></a></th>
                    <th width="18" onclick="sortTable(1)"><a href="javascript:;">Notificações <span class="icon icon-arrow-down-thin"></span></a></th>
                    <th width="12" onclick="sortTable(2)"><a href="javascript:;">Tarefas <span class="icon icon-arrow-down-thin"></span></a></th>
                    <th width="14" onclick="sortTable(3)"><a href="javascript:;">Status <span class="icon icon-arrow-down-thin"></span></a></th>
                    <th width="12" onclick="sortTable(4)"><a href="javascript:;">Perfil <span class="icon icon-arrow-down-thin"></span></a></th>
                    <th width="12" onclick="sortTable(5)"><a href="javascript:;">Prazo <span class="icon icon-arrow-down-thin"></span></a></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tickets as $ticket)
                <tr>
                    <td width="50">
                        <a href="
                        @if ($ticket->id_perfil === 0)
                            {{ route('site.artwork.ticket.editNew.get', [$ticket->id]) }}
                        @elseif ($ticket->id_perfil == -1)
                            {{ route('site.artwork.ticket.members.get', [$ticket->id]) }}
                        @else
                            {{ route('site.artwork.ticket.edit.step1.get', [$ticket->numero]) }}
                        @endif
                        ">
                            <span class="@if ($ticket->nova_embalagem) icon-magic-wand @else icon-options-settings @endif"></span>
                            <span class="ticket-name-txt {{ $ticket->id_perfil === 0 ? 'bold' : '' }}">{{ $ticket->numero }} {{ $ticket->sku_nome_completo }}</span>
                        </a>
                    </td>
                    <td width="18" class="{{ $ticket->id_perfil === 0 ? 'bold' : '' }}">0</td>
                    <td width="12" class="{{ $ticket->id_perfil === 0 ? 'bold' : '' }}">{{ $ticket->nr_tarefas }}</td>
                    <td width="14" class="{{ $ticket->id_perfil === 0 ? 'bold' : '' }}">{{ $ticket->status }}</td>
                    <td width="12" class="{{ $ticket->id_perfil === 0 ? 'bold' : '' }}">{{ $ticket->perfil }}</td>
                    <td width="12" class="{{ $ticket->id_perfil === 0 ? 'bold' : '' }}" @if ($ticket->atrasada)style="color: red"@endif  >{{ $ticket->prazo }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop
