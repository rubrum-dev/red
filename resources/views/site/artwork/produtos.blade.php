@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
{{--<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/artwork.css') }}" />--}}

{!! Breadcrumbs::renderIfExists('artwork.produtos', $family) !!}

<div class="content content-fluid">
	<div class="artwork-container">
		<section class="product-list padding-bottom-40 no-margin-bottom">
			<h3 class="flexbox-container flex-vertical-center master-title"><i class="icon fa fa-folder-open"></i>{{ $familia->nome }}</h3>
			<ul class="tabs clearfix">
				<li><a href="javascript:;" class="txt-color-default active">Produtos de Mercado</a></li>
				{{--<li><a href="javascript:;" class="disabled">Produtos Descontinuados</a></li>--}}
			</ul>
			{!! Form::open(array('class' => '', 'id' => 'form-products', 'method' => 'get', 'url' => Request::url())) !!}
				<div class="block margin-top-20">
					<div class="flexbox-container">
						<div class="flexbox-column flex flex-3-large">
							<div class="select-control">
								@if(isset($categorias))
									{!! Form::select('categorias', $categorias, app('request')->input('categorias'), array('id' => 'categoria', 'placeholder' => 'Categorias', 'onchange' => 'this.form.submit()')) !!}
								@endif
							</div>
						</div>
						@if((in_array('artwork_criar_embalagem', Auth::user()->modulos)))
							<div class="artw-create-package-btn flex">
								<a href="{{ route('site.artwork.package.create.get') }}" class="btn-function"><i class="icon fa fa-cube"></i> Nova Embalagem</a>
							</div>
						@endif
					</div>
				</div>
			{!! Form::close() !!}
			@if(count($produtos) > 0)
				<table cellpading="0" cellspacing="0" class="artw-product-table table-border table-hover no-animate">
					<tbody>
						@foreach($produtos as $key => $prod)
							<tr class="table-group">
								<td colspan="2">
									<table cellpading="0" cellspacing="0">
										<tbody>
											<tr>
												<td width="" class="table-cell-h">
													<a href="{{ URL::route('site.artwork.package', array($familia->id, $key)) }}" class="d-flex flex-align-center table-link txt-color-default"><i class="icon fa fa-folder txt-color-alt"></i><span class="text-side txt-regular">{{ $prod }}</span></a>
												</td>
												<!--td width="50">
													<div class="dropdown drop-to-left">
														<a class="dropdown-toggle fa fa-ellipsis-h disabled"></a>
														<div class="dropdown-list">
															<ul>    
																<li><a href="javascript:;" class="txt-bold"><span class="icon fa fa-edit"></span>Alterar Nome da Produto</a></li>
																<li><a href="javascript:;" class="txt-bold"><span class="icon fa fa-truck"></span>Descontinuar Produto</a></li>
																<li><a href="javascript:;" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir Produto</a></li>
															</ul>
														</div>
													</div>
												</td-->
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			@else
				<div class="artw-no-item fa fa-diamond">
					Nenhum produto localizado
				</div>
			@endif
		</section>
	</div>
</div>
@stop
