    <!-- Showcase panel -->
	<div class="showcase-panel clearfix">
        {{--<div class="showcase-left">
            <span class="helper"></span>
            {{ Html::image(asset($thumb), 'Logo', array('id' => 'imgLogo')) }}
        </div>--}}
        
        @if (!$item->compartilhado)
            <div class="showcase-left">
                <span class="helper"></span>
                @if(pathinfo($thumb)['filename'] === 'ticket-no-image-rubrum')
                    {{ Html::image(asset('/images/layout/cliente_'. config('app.costummer_id') .'/logo-home.png'), 'Logo') }}
                @else
                    @if($item->artwVariacao->sku->produtos->replicar_logo_marca)
                        <div style="width: 100%; height: 100%; background-color: {{ $cor_hexa }}">
                            <img src="{{ asset($thumb) }}" title="" alt="Logo" id="imgLogo" />
                        </div>
                    @else
                        <img src="{{ asset($thumb) }}" title="" alt="Logo" id="imgLogo" />
                    @endif
                @endif
            </div>
        @else
            <div class="showcase-left blank">
                <span class="helper"></span>
                <span class="icon fa fa-share-alt-square"></span>
            </div>
        @endif

		<div class="showcase-body">
			<div class="showcase-container">
				<div class="flexbox-container flex-no-wrap clearfix">
                    
                    <!-- Item não compartilhado -->
                    <div class="flex-auto showcase-info showcase-item-detail text-inline col-100-max">
						<h3 class="txt-bold">{{ $variacao->sku_nome }}</h3>
						<small class="text-small txt-color-alt margin-top-5">embalagem</small>
					</div>
                    @if ($item->campanha)
                        <div class="flex-auto showcase-info showcase-item-detail text-inline col-15-max">
                            <h3 class="txt-light">{{ $item->campanha }}</h3>
                            <small class="text-small txt-color-alt margin-top-5">campanha</small>
                        </div>
                    @endif
                    @if ($variacao->principal !== 1 && $variacao->nome && (isset($ticket) && !$ticket->cancelado_em || !isset($ticket)) )
                        <div class="flex-auto showcase-info showcase-item-detail text-inline col-15-max">
                            <h3 class="txt-light">{{ $variacao->nome }}</h3>
                            <small class="text-small txt-color-alt margin-top-5">variação</small>
                        </div>
                    @endif
                    @if ($item->artwItemTipo && $item->artwItemTipo->nome && !$item->compartilhado && (isset($ticket) && !$ticket->cancelado_em || !isset($ticket)) )
                        <div class="flex-auto showcase-info showcase-item-detail text-inline col-15-max">
                            <h3 class="txt-light">{{ $item->artwItemTipo->nome }}</h3>
                            <small class="text-small txt-color-alt margin-top-5">item</small>
                        </div>
                    @endif
                    <!-- Item não compartilhado -->
                    
                    <!-- Item compartilhado -->
                    @if ($item->artwItemTipo && $item->artwItemTipo->nome && $item->compartilhado && (isset($ticket) && !$ticket->cancelado_em || !isset($ticket)) )
                        <div class="flex-auto showcase-info showcase-item-detail text-inline col-15-max">
                            <h3 class="txt-bold">{{ $item->artwItemTipo->nome }}</h3>
                            <small class="text-small txt-color-alt margin-top-5">item</small>
                        </div>
                    @endif
                    
                    @if ($item->compartilhado)
                        <div class="flex-auto showcase-info showcase-item-detail text-inline col-15-max">
                            <h3 class="txt-light">{{ $item->nome }}</h3>
                            <small class="text-small txt-color-alt margin-top-5">característica</small>
                        </div>
                    @endif
                    @if ($item->compartilhado)
                        <div class="flex-auto showcase-info showcase-item-detail force-word-break text-inline col-5-max">
                            <h3 class="txt-light">{{ $item->numero }}</h3>
                            <small class="text-small txt-color-alt margin-top-5">número</small>
                        </div>
                    @endif
                    <!-- Item compartilhado -->
                    
				</div>
				<div class="flexbox-container clearfix">
					<div class="flex-auto showcase-info showcase-feature text-inline no-margin-bottom">
						<small class="text-small txt-bold txt-color-primary">Ticket: {{ (isset($ticket) && $ticket->numero) ? $ticket->numero : 'Novo' }}</small>
                        
						@if (isset($ticket) && $ticket->artwVersoes)
                            <small class="text-small txt-bold txt-color-primary">Versão: {{ $ticket->artwVersoes->last()->numero }}</small>
                        @else
                            <small class="text-small txt-bold txt-color-primary">Ciclo: {{ (isset($ticket) && $ticket->ciclo_atual()->numero) ? $ticket->ciclo_atual()->numero : '0' }}</small>
                        @endif
                        
					</div>
                    
                    <!-- Item não compartilhado -->
					<div class="flex-auto showcase-info showcase-feature text-inline no-margin-bottom">
						<small class="text-small txt-regular">Marca: <strong class="txt-semibold">{{ $variacao->sku->produtos->familias->nome }}</strong></small>
						<small class="text-small txt-regular">Categoria: <strong class="txt-semibold">{{ $variacao->sku->produtos->categorias->nome }}</strong></small>
					</div>
					<div class="flex-auto showcase-info showcase-feature text-inline no-margin-bottom">
						<small class="text-small txt-regular">Natureza: <strong class="txt-semibold">{{ $variacao->sku->dimensoes->tiposEmbalagens->naturezaEmbalagens->nome }}</strong></small>
						<small class="text-small txt-regular">Função: <strong class="txt-semibold">{{ $variacao->funcao }}</strong></small>
					</div>

                    <!-- EAN - TODO  -->
                    <div class="flex-auto showcase-info showcase-feature text-inline no-margin-bottom">
                        <small class="text-small txt-regular">Código de Barras: 
                            @if ($item->artwVariacao->tem_codigo_ean === 1 && !$item->artwVariacao->codigo_ean)
                            <strong class="txt-semibold">PENDENTE</strong>
                            @elseif ($item->artwVariacao->tem_codigo_ean === 1 && $item->artwVariacao->codigo_ean)
                            <strong class="txt-semibold">{{$item->artwVariacao->codigo_ean}}</strong>
                            @elseif ($item->artwVariacao->tem_codigo_ean === 0)
                            <strong class="txt-semibold">N/A</strong>
                            @else
                            <strong class="txt-semibold">PENDENTE</strong>
                            @endif
                        </small>
					</div>
                    <!-- END EAN TODO -->
                    
					<!--div class="flex-fluid showcase-info text-inline no-margin-bottom">
						<small class="text-small txt-regular">Mercados: 
							<strong class="txt-semibold">
                                @foreach ($variacao->mercados as $i=>$mercado)
                                    {{ $mercado->sigla }}{{ (($i+1) < $item->artwVariacao->mercados->count()) ? ', ' : '' }}
                                @endforeach
							</strong>
						</small>
                    </div-->
                    <!-- Item não compartilhado -->
                                        
				</div>
			</div>
		</div>
	</div>
	<!-- END Showcase panel -->