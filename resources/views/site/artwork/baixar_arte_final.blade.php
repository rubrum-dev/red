@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
<script>
    var id_fornecedor = {{ $arteEnvio->id_fornecedor }}
    var id_ticket = {{ $ticket->id }};
    var token = '{{ $token }}';
    var csrf_token = '{{ csrf_token() }}';
</script>

<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js')}}"></script>
<script src="{{ asset('/js/angular/ng-file-upload-shim.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload.min.js') }}"></script>

<script src="{{ asset2('/js/angular/controllers/baixar_arte.js')}}"></script>

<div ng-app="app" ng-controller="BaixarArteCtrl" class="content content-fluid clearfix" ng-cloak>
    <div class="artw-download-container margin-top-40 margin-bottom-40">
        <!-- Showcase panel -->
        <div class="showcase-panel no-margin-top margin-bottom-30 clearfix">
            @if (!$item->compartilhado)
                <div class="showcase-left">
                    <span class="helper"></span>
                    {{ Html::image(asset($thumb), 'Logo', array('id' => 'imgLogo')) }}
                </div>
            @else
                <div class="showcase-left blank">
                    <span class="helper"></span>
                    <span class="icon fa fa-share-alt-square"></span>
                </div>
            @endif
            <div class="showcase-body">
                <div class="showcase-container">
                    <div class="flexbox-container flex-no-wrap clearfix">
                        <!-- Item não compartilhado -->
                        @if (!$item->compartilhado)
                        <div class="flex-auto showcase-info showcase-item-detail text-inline col-100-max">
                            <h3 class="txt-bold">{{ (isset($ticket) && $ticket->cancelado_em && $ticket->cancelado_nome_original) ? $ticket->cancelado_nome_original : $item->sku_nome }}</h3>
                            <small class="text-small txt-color-alt margin-top-5">embalagem</small>
                        </div>
                        @endif
                        @if ($item->campanha && !$item->compartilhado)
                            <div class="flex-auto showcase-info showcase-item-detail text-inline col-20-max">
                                <h3 class="txt-light">{{ $item->campanha }}</h3>
                                <small class="text-small txt-color-alt margin-top-5">campanha</small>
                            </div>
                        @endif
                        @if ($item->variacao && !$item->compartilhado)
                            <div class="flex-auto showcase-info showcase-item-detail text-inline col-20-max">
                                <h3 class="txt-light">{{ $item->variacao }}</h3>
                                <small class="text-small txt-color-alt margin-top-5">variação</small>
                            </div>
                        @endif
                        @if ($item->artwItemTipo && $item->artwItemTipo->nome && !$item->compartilhado)
                            <div class="flex-auto showcase-info showcase-item-detail text-inline col-20-max">
                                <h3 class="txt-light">{{ $item->artwItemTipo->nome }}</h3>
                                <small class="text-small txt-color-alt margin-top-5">item</small>
                            </div>
                        @endif
                        <!-- Item não compartilhado -->

                        <!-- Item compartilhado -->
                        @if ($item->artwItemTipo && $item->artwItemTipo->nome && $item->compartilhado)
                            <div class="flex-auto showcase-info showcase-item-detail text-inline col-100-max">
                                <h3 class="txt-bold">{{ $item->artwItemTipo->nome }}</h3>
                                <small class="text-small txt-color-alt margin-top-5">item</small>
                            </div>
                        @endif

                        @if ($item->compartilhado)
                            <div class="flex-auto showcase-info showcase-item-detail text-inline col-30-max">
                                <h3 class="txt-light">{{ $item->nome }}</h3>
                                <small class="text-small txt-color-alt margin-top-5">característica</small>
                            </div>
                        @endif
                        @if ($item->compartilhado)
                            <div class="flex-auto showcase-info showcase-item-detail text-inline force-word-break col-5-max">
                                <h3 class="txt-light">{{ $item->numero }}</h3>
                                <small class="text-small txt-color-alt margin-top-5">número</small>
                            </div>
                        @endif
                        <!-- Item compartilhado -->
                    </div>
                    <div class="flexbox-container clearfix">
                        
                        <div class="flex-auto showcase-info showcase-feature text-inline no-margin-bottom">
                            <small class="text-small txt-bold txt-color-primary">Ticket: {{$ticket->numero}}</small>
                            <small class="text-small txt-bold txt-color-primary">Versão {{ $arteEnvioEmail->artwArteEnvio->artwArte->versao }}</small>
                        </div>
                        <!-- Item não compartilhado -->
                        @if (!$item->compartilhado)
                        <div class="flex-auto showcase-info showcase-feature text-inline no-margin-bottom">
                            <small class="text-small txt-regular">Marca: <strong class="txt-semibold">{{ $item->artwVariacao->sku->produtos->familias->nome }}</strong></small>
                            <small class="text-small txt-regular">Categoria: <strong class="txt-semibold">{{ $item->artwVariacao->sku->produtos->categorias->nome }}</strong></small>
                        </div>
                        <div class="flex-auto showcase-info showcase-feature text-inline no-margin-bottom">
                            <small class="text-small txt-regular">Natureza: <strong class="txt-semibold">{{ $item->artwVariacao->sku->dimensoes->tiposEmbalagens->naturezaEmbalagens->nome }}</strong></small>
                            <small class="text-small txt-regular">Função: <strong class="txt-semibold">{{ $item->artwVariacao->funcao }}</strong></small>
                        </div>
                        {{--<div class="flex-auto showcase-info showcase-feature text-inline no-margin-bottom">
                            <small class="text-small txt-regular">Mercados: 
                                <strong class="txt-semibold">
                                    @foreach ($item->artwVariacao->mercados as $i=>$mercado)
                                        {{ $mercado->sigla }}{{ (($i+1) < $item->artwVariacao->mercados->count()) ? ', ' : '' }}
                                    @endforeach
                                </strong>
                            </small>
                        </div>--}}

                        <!-- EAN - TODO  -->
                        <div class="flex-auto showcase-info showcase-feature text-inline no-margin-bottom">
                            <small class="text-small txt-regular">Código de Barras: 
                                @if ($item->artwVariacao->tem_codigo_ean === 1 && !$item->artwVariacao->codigo_ean)
                                <strong class="txt-semibold">PENDENTE</strong>
                                @elseif ($item->artwVariacao->tem_codigo_ean === 1 && $item->artwVariacao->codigo_ean)
                                <strong class="txt-semibold">{{$item->artwVariacao->codigo_ean}}</strong>
                                @elseif ($item->artwVariacao->tem_codigo_ean === 0)
                                <strong class="txt-semibold">N/A</strong>
                                @else
                                <strong class="txt-semibold">PENDENTE</strong>
                                @endif
                            </small>
                        </div>
                        <!-- END EAN TODO -->

                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- END Showcase panel -->
        <section>
            <form>
                <div class="block">
                    <h4 class="master-title-alt txt-bold margin-bottom-20">Baixar Arte-final</h4>
                    <div class="margin-left-10 margin-right-10">
                        @if ($liberaDownload)
                        <p>
                            Essa página estará disponível por <strong>30 dias</strong>, após o dia {{ $arteEnvioEmail->artwArteEnvio->dt_expiracao->format('d/m/Y') }} 
                            ela será expirada. Baixe os arquivos antes do prazo de expiração.
                        </p>
                        @else
                        <p>
                            Os arquivos não estão mais disponíveis, expiraram no dia {{ $arteEnvioEmail->artwArteEnvio->dt_expiracao->format('d/m/Y') }}.
                        </p>
                        @endif
                    </div>
                </div>
                @if ($liberaDownload)
                    <div class="block">
                        <div class="margin-left-10 margin-right-10">
                            <h4 class="master-title-sub txt-light txt-uppercase">Arte-final</h4>
                            <p class="margin-bottom-20">
                                Pacote que contém todos os arquivos em alta resolução, vetores e fontes.
                            </p>
                            <div class="panel secondary">
                                <a target="_blank" href="{{ route('downloadArte') }}?token={{ $token }}" class="link"></a>
                                <i class="icon fa fa-file-archive-o"></i>
                                <div class="panel-body">
                                    <h4 class="panel-heading txt-semibold">{{ $arteEnvioEmail->artwArteEnvio->artwArte->nome }} • Versão {{ $arteEnvioEmail->artwArteEnvio->artwArte->versao }}</h4>
                                    <small class="description-small txt-regular">Anexado por {{ $arteEnvioEmail->artwArteEnvio->artwArte->admUsuario->nome_abreviado }} às {{ $arteEnvioEmail->artwArteEnvio->artwArte->updated_at->format('H\hm \d\e d/m/Y') }}</small>
                                    <small class="description-small txt-regular no-margin">Arquivo Original: {{ $arteEnvioEmail->artwArteEnvio->artwArte->nome_arquivo }} - <strong class="txt-small txt-regular">{{ $arteEnvioEmail->artwArteEnvio->artwArte->filesize }} MB</strong></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block">
                        <div class="margin-left-10 margin-right-10">
                            <h4 class="master-title-sub txt-light txt-uppercase">Layout de Aprovação</h4>
                            <p class="margin-bottom-20">
                                Arquivo PDF em baixa resolução que deve ser usado apenas para conferência.
                            </p>
                            <div class="panel secondary">
                                <a target="_blank" href="{{ route('downloadPdf') }}?token={{ $token }}" class="link"></a>
                                <i class="icon fa fa-file-pdf-o"></i>
                                <div class="panel-body">
                                    <h4 class="panel-heading txt-semibold">PDF {{ $arteEnvioEmail->artwArteEnvio->artwArte->nome }} • Versão {{ $arteEnvioEmail->artwArteEnvio->artwArte->versao }}</h4>
                                    <small class="description-small txt-regular">Anexado por {{ $arteEnvioEmail->artwArteEnvio->artwArte->artwTicket->ciclo_atual()->artwCiclosLayout->last()->admUsuario->nome_abreviado }} às {{ $arteEnvioEmail->artwArteEnvio->artwArte->artwTicket->ciclo_atual()->artwCiclosLayout->last()->updated_at->format('H\hm \d\e d/m/Y') }} - <strong class="txt-small txt-regular">{{ $arteEnvioEmail->artwArteEnvio->artwArte->artwTicket->ciclo_atual()->artwCiclosLayout->last()->filesize }} MB</strong></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if ($item->compartilhado)
                        <div class="block">
                            <div class="margin-left-10 margin-right-10">
                                <h6 class="txt-small txt-light txt-uppercase margin-bottom-15">Embalagens que Compartilham o Item</h6>
                                <div class="text-block margin-bottom-15">
                                    <p>
                                        Este item é compartilhado, isso significa que essa mesma arte-final serve para as seguintes embalagens:
                                    </p>
                                </div>
                                <table ng-init="artwItemSharing({{$item->id}})" class="artw-shared-item-table table-border table-hover no-animate">
                                    <tbody>
                                        <tr ng-repeat="compartilhada in itemSelecionado.compartilhadas">
                                            <td width="" class="table-cell-h">
                                                <i ng-class="{'txt-color-unlit': itemSelecionado.compartilhadas.length == 1}" class="pull-left icon fa fa-cube"></i>
                                                <span ng-class="{'txt-color-alt': itemSelecionado.compartilhadas.length == 1}" class="text-side">@{{compartilhada.nome_original}}</span> 
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endif
                @endif
                @if ($arteEnvio->id_fornecedor)
                <div ng-show="loaded" class="block">
                    <h4 class="master-title-alt txt-bold margin-bottom-20">Anexar PDF do Fornecedor</h4>
                    <div class="margin-left-10 margin-right-10">
                        <div ng-hide="fornecedorPdfs.length && (fornecedorPdfs[0].status == 0 || fornecedorPdfs[0].status == 1)" class="panel notice no-hover margin-bottom-30">
                            <i class="icon fa fa-exclamation-triangle"></i>
                            <div class="panel-body">
                                <h4 class="panel-heading txt-semibold no-margin">Subir em padrão PDF/X-3 com fontes em curvas para evitar divergências na renderização.</h4>
                            </div>
                        </div>
                        <div ng-hide="fornecedorPdfs.length && (fornecedorPdfs[0].status == 0 || fornecedorPdfs[0].status == 1)" class="flexbox-container flexbox-group margin-bottom-30">
                            <div class="col-files-attach flex flex-fluid files-control attachPDF">
                                <input type="file" accept="application/pdf" ng-model="pdf" name="file" id="pdfFileUpload" valid-file=".pdf" ngf-select ngf-model-invalid="errorFile" required />
                                <span class="files-control-fake"><strong></strong><i class="addon fa fa-paperclip"></i></span>
                            </div>
                            <div class="flex col-artwork-submit">   
                                <button ng-click="upload_arquivo($event)" type="button" class="btn-control btn-positive">Adicionar</button>
                            </div>
                            <div ng-show="pdf.progress > 0" class="progress-circle animate">
                                <svg id="svg" viewbox="0 0 100 100">
                                    <circle cx="50" cy="50" r="40" fill="none"/>
                                    <path fill="none" stroke-linecap="round" stroke-width="4" stroke-dasharray="@{{pdf.progress*2.5}},250.2" d="M50 10 a 40 40 0 0 1 0 80 a 40 40 0 0 1 0 -80"/>
                                    <text id="count" class="progress" x="50" y="50" text-anchor="middle" dy="7" ng-bind="pdf.progress + '%'"></text>
                                </svg>
                            </div>
                        </div>
                        <div ng-if="!fornecedorPdfs.length" class="margin-bottom-30">
                            <div class="panel default no-hover">
                                <i class="icon fa fa-file-pdf-o"></i>
                                <div class="panel-body">
                                    <h4 class="panel-heading txt-semibold no-margin">Nenhum PDF anexado ainda.</h4>
                                </div>
                            </div>
                        </div>
                        <div ng-if="fornecedorPdfs.length" class="margin-bottom-30">
                            <h4 class="master-title-sub txt-light txt-uppercase">PDF do Fornecedor</h4>
                            <div ng-repeat="(p, pdf) in fornecedorPdfs track by $index" class="workflow-supplier-card panel secondary">
                                <div class="panel-body">
                                    <h4 class="panel-heading animate">
                                        <a target="_blank" ng-href="@{{pdf.path}}">
                                            <i class="icon fa fa-file-pdf-o"></i>
                                            <strong class="txt-semibold">@{{ pdf.nome }}</strong>
                                            <small class="description-small txt-regular">Anexado por @{{ pdf.nome_usuario }}, @{{ pdf.data_criacao }}</small>
                                            <small class="description-small txt-regular no-margin">Arquivo original: @{{ pdf.nome_arquivo }} - @{{ pdf.filesize }} MB</small>
                                        </a>
                                    </h4>
                                    <div class="flex workflow-supplier-info margin-top-15">
                                        <i ng-class="{ 'fa-check': (pdf.status == 1), 'fa-industry': (pdf.status == 0) || (pdf.status == 3) }" class="icon fa"></i>
							            <div class="panel-body">
                                            <h4 class="panel-heading txt-semibold">Fornecedor • @{{ pdf.fornecedor }}</h4>
                                            <small ng-if="pdf.status == 3" class="description-small">Aguardando enviar para o cliente</small>
                                            <small ng-if="pdf.status == 0" class="description-small">Aguardando aprovação</small>
                                            <small ng-if="pdf.status == 1" class="description-small">Aprovado por @{{ pdf.nome_aprovacao }}, as @{{ pdf.data_aprovacao }}</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <div class="block">
                                        <div ng-if="pdf.status == 3" class="dropdown drop-to-left pull-right">
                                            <a data-title="Opções" href="javascript:;" class="dropdown-btn dropdown-toggle ui-tooltip top fa fa-ellipsis-h"></a>
                                            <div class="dropdown-list">
                                                <ul>
                                                    <li><a href="javascript:;" ng-click="excluir()" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div ng-if="fornecedorPdfs.length && fornecedorPdfs[0].status == 3" class="btn-group txt-center margin-top-30 no-margin-bottom">
                    <div class="bs border-color-primary padding-top-40">
                        <button type="button" ng-click="enviar_para_cliente()" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i> Enviar para o Cliente</span>
                        </button>
                    </div>
                </div>
                @endif
            </form>
        </section>
    </div>
</div>
@stop