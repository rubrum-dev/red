@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('artwork.ticket', $ticket->id) !!}

<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm.css') }}" />
{{--<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm-custom.css')}}" />--}}
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('/css/angular/v-accordion.min.css')}}" />
{{--<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/workflow.css') }}" />--}}

<script>
    var id_ticket = {{ $ticket->id }};
    var csrf_token = '{{ csrf_token() }}';
    var edicao = true;
    var libera_fornecedor_pdf = {{ $libera_edicao['fornecedor_pdf'] }};
</script>
<script src="{{ asset('/js/site/snap.svg-min.js') }}"></script>
<script src="{{ asset('/js/site/artwork/ticket-edit.js') }}"></script>
<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-route.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset('/js/angular/v-accordion.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload-shim.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload.min.js') }}"></script>
<script src="{{ asset('/js/angular/ticket-resolve-loader.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/modal_grupo_trabalho.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/embalagens_modal_compartilhar.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edicao_aba_visao_geral.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edicao_aba_anexos.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edicao_aba_participantes.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edicao_aba_tarefas.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edicao_aba_mensagens.js') }}"></script>

<script src="{{ asset2('/js/angular/controllers/ticket_edicao_aba_pdf_fornecedor.js') }}"></script>

<script src="{{ asset2('/js/angular/controllers/ticket_edicao_aba_opcoes.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edicao_v2.js') }}"></script>

<div class="content content-fluid" ng-app="app" ng-controller="TicketEdicaoCtrl" ng-cloak>
    
    @include('site.artwork.ticket_topo_inc')
    
    <!-- Timeline -->
    <div class="timeline">
        <div ng-class="(ticket.etapas[0] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.etapas[0] && !ticket.etapas[1]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.etapas[0] && !ticket.etapas[1]) && ticket.cancelado_em}" class="event-icon fa fa-flag"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.etapas[0] && !ticket.etapas[1]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.etapas[0] && !ticket.etapas[1]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Aberto</span>
            </div>
        </div>
        <div ng-class="(ticket.etapas[2] === 1) ? 'event-box is-done' : 'event-box'" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[2] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.etapas[2] && !ticket.etapas[3]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.etapas[2] && !ticket.etapas[3]) && ticket.cancelado_em}" class="event-icon fa fa-paint-brush"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.etapas[2] && !ticket.etapas[3]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.etapas[2] && !ticket.etapas[3]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Edição</span>
            </div>
        </div>
        <!--div ng-class="(ticket.etapas[3] === 1) ? 'event-box is-done' : 'event-box'" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[3] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <div class="dot"></div>
            </div>
        </div-->
        <div ng-class="(ticket.etapas[3] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[3] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <span ng-if="(ticket.etapas[3] && !ticket.etapas[4]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.etapas[3] && !ticket.etapas[4]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <div ng-class="{'sub paused-at paused-at-btn clickable fa fa-pause-circle': (ticket.etapas[3] && !ticket.etapas[4]) && ticket.pausado_em, 'sub canceled-at fa fa-times-circle': (ticket.etapas[3] && !ticket.etapas[4]) && ticket.cancelado_em}" class="dot"></div>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>

            </div>
        </div>
        <div ng-class="(ticket.etapas[4] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[4] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.etapas[4] && !ticket.etapas[5]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.etapas[4] && !ticket.etapas[5]) && ticket.cancelado_em}" class="event-icon fa fa-eye"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.etapas[4] && !ticket.etapas[5]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.etapas[4] && !ticket.etapas[5]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Revisão</span>
            </div>
        </div>
        <div ng-class="(ticket.etapas[5] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[5] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <span ng-if="(ticket.etapas[5] && !ticket.etapas[6]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.etapas[5] && !ticket.etapas[6]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <div ng-class="{'sub paused-at paused-at-btn clickable fa fa-pause-circle': (ticket.etapas[5] && !ticket.etapas[6]) && ticket.pausado_em, 'sub canceled-at fa fa-times-circle': (ticket.etapas[5] && !ticket.etapas[6]) && ticket.cancelado_em}" class="dot"></div>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div ng-class="(ticket.etapas[6] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <!-- V1 - Opcional -->
        <div ng-show="(ticket.perfis[2] | filter:{deleted_at:null}:true).length || (ticket.perfis[2] | filter:{aprovado:1}:true).length" ng-class="(ticket.etapas[6] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.etapas[6] && !ticket.etapas[7]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.etapas[6] && !ticket.etapas[7]) && ticket.cancelado_em}" class="event-icon fa fa-thumbs-up"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.etapas[6] && !ticket.etapas[7]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.etapas[6] && !ticket.etapas[7]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Primeira<br />Aprovação</span>
            </div>
        </div>
        <div ng-show="(ticket.perfis[2] | filter:{deleted_at:null}:true).length || (ticket.perfis[2] | filter:{aprovado:1}:true).length" ng-class="(ticket.etapas[7] === 1) ? 'event-box is-done' : 'event-box'" style="flex: 1;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <!-- END V1 - Opcional -->
        <div ng-class="(ticket.etapas[7] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.etapas[7] && !ticket.etapas[8]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.etapas[7] && !ticket.etapas[8]) && ticket.cancelado_em}" class="event-icon fa fa-gavel"></i>
                <div class="event-dropdown-list to-right">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.etapas[7] && !ticket.etapas[8]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.etapas[7] && !ticket.etapas[8]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Demais<br />Aprovações</span>
            </div>
        </div>
        <div ng-class="(ticket.etapas[8] === 1) ? 'event-box is-done' : 'event-box'" style="flex: 1;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[8] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.etapas[8] && !ticket.etapas[9]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.etapas[8] && !ticket.etapas[9]) && ticket.cancelado_em}" class="event-icon fa fa-paint-brush"></i>
                <div class="event-dropdown-list to-left">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.etapas[8] && !ticket.etapas[9]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.etapas[8] && !ticket.etapas[9]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Finalização</span>
            </div>
        </div>
        <div ng-class="(ticket.etapas[9] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[9] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <span ng-if="(ticket.etapas[9] && !ticket.etapas[10]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.etapas[9] && !ticket.etapas[10]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <div ng-class="{'sub paused-at paused-at-btn clickable fa fa-pause-circle': (ticket.etapas[9] && !ticket.etapas[10]) && ticket.pausado_em, 'sub canceled-at fa fa-times-circle': (ticket.etapas[9] && !ticket.etapas[10]) && ticket.cancelado_em}" class="dot"></div>
                <div class="event-dropdown-list to-left">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div ng-class="(ticket.etapas[10] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.44;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <!-- V1 - Opcional - Envio da Arte -->
        <div ng-show="((ticket.perfis[6] | filter:{deleted_at:null}:true).length) || (ticket.perfis[6].length && ticket.artes_envios.length)" ng-class="(ticket.etapas[10] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': (ticket.etapas[10] && !ticket.etapas[11]) && ticket.pausado_em, 'canceled-at fa-times': (ticket.etapas[10] && !ticket.etapas[11]) && ticket.cancelado_em}" class="event-icon fa fa-paper-plane"></i>
                <div class="event-dropdown-list to-left">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="(ticket.etapas[10] && !ticket.etapas[11]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.etapas[10] && !ticket.etapas[11]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Envio da<br />Arte</span>
            </div>
        </div>
        <div ng-show="((ticket.perfis[6] | filter:{deleted_at:null}:true).length) || (ticket.perfis[6].length && ticket.artes_envios.length)" ng-class="(ticket.etapas[11] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <!-- END V1 - Opcional - Envio da Arte -->
        
        <div ng-show="((ticket.perfis[6] | filter:{deleted_at:null}:true).length)" ng-class="(ticket.etapas[11] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <span ng-if="(ticket.etapas[11] && !ticket.etapas[12]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.etapas[11] && !ticket.etapas[12]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <div ng-class="{'sub paused-at paused-at-btn clickable fa fa-pause-circle': (ticket.etapas[11] && !ticket.etapas[12]) && ticket.pausado_em, 'sub canceled-at fa fa-times-circle': (ticket.etapas[11] && !ticket.etapas[12]) && ticket.cancelado_em}" class="dot"></div>
                <div class="event-dropdown-list to-left">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div ng-show="((ticket.perfis[6] | filter:{deleted_at:null}:true).length)" ng-class="(ticket.etapas[12] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <!-- V1 - Opcional - PDF do Fornecedor -->
        <div ng-show="((ticket.perfis[7] | filter:{deleted_at:null}:true).length) || (ticket.perfis[7].length && ticket.fornecedorPdfsAprovados.length)" ng-class="( ( ticket.etapas[10] === 1 && ticket.etapas[11] === 1 && ticket.etapas[12] === 1 && ((ticket.perfis[6] | filter:{deleted_at:null}:true).length) ) || ( ticket.etapas[10] === 1 && ticket.etapas[11] === 1 && !ticket.etapas[12] && !((ticket.perfis[6] | filter:{deleted_at:null}:true).length) ) ) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': ( ( ticket.etapas[10] === 1 && ticket.etapas[11] === 1 && ticket.etapas[12] === 1 && ticket.perfis[6] ) || ( ticket.etapas[10] === 1 && ticket.etapas[11] === 1 && !ticket.etapas[12] && !ticket.perfis[6] ) ) && !ticket.etapas[14] && ticket.pausado_em, 'canceled-at fa-times': ( ( ticket.etapas[10] === 1 && ticket.etapas[11] === 1 && ticket.etapas[12] === 1 && ticket.perfis[6] ) || ( ticket.etapas[10] === 1 && ticket.etapas[11] === 1 && !ticket.etapas[12] && !ticket.perfis[6] ) ) && !ticket.etapas[14] && ticket.cancelado_em}" class="event-icon fa fa-industry"></i>
                <div class="event-dropdown-list to-left">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="( ( ticket.etapas[10] === 1 && ticket.etapas[11] === 1 && ticket.etapas[12] === 1 && ticket.perfis[6] ) || ( ticket.etapas[10] === 1 && ticket.etapas[11] === 1 && !ticket.etapas[12] && !ticket.perfis[6] ) ) && !ticket.etapas[14] && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="( ( ticket.etapas[10] === 1 && ticket.etapas[11] === 1 && ticket.etapas[12] === 1 && ticket.perfis[6] ) || ( ticket.etapas[10] === 1 && ticket.etapas[11] === 1 && !ticket.etapas[12] && !ticket.perfis[6] ) ) && !ticket.etapas[14] && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">PDF do<br />Fornecedor</span>
            </div>
        </div>
        <div ng-show="((ticket.perfis[7] | filter:{deleted_at:null}:true).length) || (ticket.perfis[7].length && ticket.fornecedorPdfsAprovados.length)" ng-class="(ticket.etapas[14] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <!-- END V1 - Opcional - PDF do Fornecedor -->
        
        <div ng-show="((ticket.perfis[7] | filter:{deleted_at:null}:true).length)" ng-class="(ticket.etapas[14] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <span ng-if="(ticket.etapas[14] && !ticket.etapas[15]) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="(ticket.etapas[14] && !ticket.etapas[15]) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <div ng-class="{'sub paused-at paused-at-btn clickable fa fa-pause-circle': (ticket.etapas[14] && !ticket.etapas[15]) && ticket.pausado_em, 'sub canceled-at fa fa-times-circle': (ticket.etapas[14] && !ticket.etapas[15]) && ticket.cancelado_em}" class="dot"></div>
                <div class="event-dropdown-list to-left">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div ng-show="((ticket.perfis[7] | filter:{deleted_at:null}:true).length)" ng-class="(ticket.etapas[15] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <div ng-class="{'event-box current': (ticket.etapas[13] === 1 && ticket.perfis[6]) || ticket.etapas[15] === 1 || (ticket.etapas[10] === 1 && !ticket.perfis[6] && !ticket.perfis[7]) }" style="flex: unset;" class="event-box">
        
            <div class="event">
                <i ng-class="{'paused-at paused-at-btn clickable fa-pause': ((ticket.etapas[13] === 1 && ticket.perfis[6]) || ticket.etapas[15] === 1 || (ticket.etapas[10] === 1 && !ticket.perfis[6] && !ticket.perfis[7])) && ticket.pausado_em, 'canceled-at fa-times': ((ticket.etapas[13] === 1 && ticket.perfis[6]) || ticket.etapas[15] === 1 || (ticket.etapas[10] === 1 && !ticket.perfis[6] && !ticket.perfis[7])) && ticket.cancelado_em}" class="event-icon fa fa-flag-checkered"></i>
                <div class="event-dropdown-list to-left">
                    <ul>
                        <li><a ng-click="despausar_ticket()" href="javascript:;" class="txt-bold"><span class="icon fa fa-play-circle"></span>Reativar Ticket</a></li>
                    </ul>
                </div>
                <span ng-if="((ticket.etapas[13] === 1 && ticket.perfis[6]) || ticket.etapas[15] === 1 || (ticket.etapas[10] === 1 && !ticket.perfis[6] && !ticket.perfis[7])) && ticket.pausado_em" class="event-state-txt">Pausado</span>
                <span ng-if="((ticket.etapas[13] === 1 && ticket.perfis[6]) || ticket.etapas[15] === 1 || (ticket.etapas[10] === 1 && !ticket.perfis[6] && !ticket.perfis[7])) && ticket.cancelado_em" class="event-state-txt">Cancelado</span>
                <span class="event-txt">Encerramento</span>
            </div>
        </div>
    </div>
    <!-- END Timeline -->
    <!-- Content -->
    <div class="workflow-container">
        <form name="formTicketEdicao" action="" method="POST" onsubmit="return false;" class="form-workflow-ctrl">
            <!-- Tabs -->
            <ul class="tabs clearfix" ng-cloak>
                <li><a ng-click="$root.change_tab($event)" ng-class="{'active' : activetab == '/visao-geral'}" ng-href="#/visao-geral">Visão Geral</a></li>
                <li><a ng-click="$root.change_tab($event)" ng-class="{'active' : activetab == '/anexos'}" ng-href="#/anexos">Anexos</a></li>
                <li><a ng-click="$root.change_tab($event)" ng-class="{'active' : activetab == '/participantes'}" ng-href="#/participantes">Participantes</a></li>
                <li><a ng-click="$root.change_tab($event)" ng-class="{'active' : activetab == '/tarefas'}" ng-href="#/tarefas">Tarefas</a></li>
                <li><a ng-click="$root.change_tab($event)" ng-class="{'active' : activetab == '/mensagens'}" ng-href="#/mensagens">Mensagens</a></li>
                
                <!--li ng-show="((ticket.perfis[7] | filter:{deleted_at:null}:true).length) || (ticket.perfis[7].length && ticket.fornecedorPdfsAprovados.length)"><a ng-click="$root.change_tab($event)" ng-class="{'disabled': (ticket.id_status == 0 || ticket.id_status == 1 || ticket.id_status == 2 || ticket.id_status == 3 || ticket.id_status == 4 || ticket.id_status == 5 || ticket.id_status == 10),'active': (activetab == '/pdf-fornecedor')}" ng-href="#/pdf-fornecedor">PDF do Fornecedor</a></li-->
                <li><a ng-click="$root.change_tab($event)" ng-class="{'active': (activetab == '/pdf-fornecedor')}" ng-href="#/pdf-fornecedor">PDF do Fornecedor</a></li>

                <li><a ng-click="$root.change_tab($event)" ng-class="{'active' : activetab == '/opcoes', 'disabled': ticket.cancelado_em || ticket.id_status == 6}" ng-href="#/opcoes">Opções</a></li>
            </ul>
            <!-- END Tabs -->
            
            <!-- resolve-loader -->
            <div resolve-loader></div>
            <!-- END resolve-loader -->

            <!-- ng-view -->
            <div ng-view class="fadein"></div>
            <!-- END ng-view -->
            
            <!-- Button group -->
            <div ng-show="statechange">
                <div ng-show="ticket.libera_ticket && (activetab == '/visao-geral' || activetab == '/tarefas' || activetab == '/pdf-fornecedor')" class="btn-group txt-center no-animate">
                    <div ng-show="ticket.libera_status && ticket.ciclo_atual.layout && ticket.id_status === 1 && ticket.qtde_revisores" class="bs border-color-primary padding-top-40 no-padding-bottom">
                        <button mw-confirm-click="acao_ticket('2');" mw-confirm-click-message="Deseja submeter à revisão?" ng-show="ticket.libera_status && ticket.ciclo_atual.layout && ticket.id_status === 1 && ticket.qtde_revisores" type="button" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i>Submeter à Revisão</span>
                        </button>
                    </div>
                    <div ng-show="ticket.libera_status && ticket.ciclo_atual.layout && ticket.id_status === 1 && !ticket.qtde_revisores" class="bs border-color-primary padding-top-40 no-padding-bottom">
                        <button onclick="myAlert('Não há revisores adicionados a esse ticket.<br />Para proseguir, adicione pelo menos um revisor.', 'warning')" ng-show="ticket.libera_status && ticket.ciclo_atual.layout && ticket.id_status === 1 && !ticket.qtde_revisores" type="button" class="btn-control btn-call-to-action call-to-action-default btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i>Submeter à Revisão</span>
                        </button>
                    </div>
                    <div ng-show="step3 && ticket.id_status === 2 && ticket.libera_status && (ticket.qtde_marketings || ticket.qtde_aprovadores)" class="bs border-color-primary padding-top-40 no-padding-bottom">
                        <button mw-confirm-click="acao_ticket('3');" mw-confirm-click-message="Deseja submeter à aprovação?" type="button" ng-show="step3 && ticket.id_status === 2 && ticket.libera_status && (ticket.qtde_marketings || ticket.qtde_aprovadores)" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i>Submeter à Aprovação</span>
                        </button>
                    </div>
                    <div ng-show="step3 && ticket.id_status === 2 && ticket.libera_status && !ticket.qtde_marketings && !ticket.qtde_aprovadores" class="bs border-color-primary padding-top-40 no-padding-bottom">
                        <button onclick="myAlert('Para prosseguir para os ciclos de aprovação, adicione pelo menos um participante como aprovador nesse ticket.', 'warning')" type="button" ng-show="step3 && ticket.id_status === 2 && ticket.libera_status && !ticket.qtde_marketings && !ticket.qtde_aprovadores" class="btn-control btn-call-to-action call-to-action-default btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i>Submeter à Aprovação</span>
                        </button>
                    </div>
                    <div ng-show="(!ticket.em_reprovacao || (ticket.em_reprovacao && ticket.em_reprovacao_by == ticket.id_usuario_logado)) && (ticket.id_status === 3 && ticket.libera_status || ticket.id_status === 3 && ticket.libera_status && ticket.qtde_aprovadores)" class="bs border-color-primary padding-top-40 no-padding-bottom">
                        <button onClick="location='{{Route('site.artwork.ticket.edit.deny.get', [$ticket->id])}}'" type="button" ng-show="ticket.id_status === 3 && ticket.libera_status" class="btn-control btn-call-to-action call-to-action-negative btn-icon btn-icon-left">
                            <span><i class="icon fa fa-times"></i>Reprovar</span>
                        </button>
        
                        <button mw-confirm-click="acao_ticket('4');" mw-confirm-click-message="Deseja aprovar?" type="button" ng-show="ticket.id_status === 3 && ticket.libera_status && ticket.qtde_aprovadores && !ticket.em_reprovacao" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i>Aprovar</span>
                        </button>
        
                        <button mw-confirm-click="acao_ticket('4');" mw-confirm-click-message="Ao aprovar todas as anotações serão excluídas.<br /> Deseja confirmar?" type="button" ng-show="ticket.id_status === 3 && ticket.libera_status && ticket.qtde_aprovadores && ticket.em_reprovacao" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i>Aprovar</span>
                        </button>

                        <button onclick="myAlert('Para prosseguir para os ciclos de aprovação, adicione pelo menos um participante como aprovador nesse ticket.', 'warning')" type="button" ng-show="ticket.id_status === 3 && ticket.libera_status && !ticket.qtde_aprovadores && !ticket.em_reprovacao" class="btn-control btn-call-to-action call-to-action-default btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i>Aprovar</span>
                        </button>
                    </div>
                
                    <div ng-if="activetab == '/visao-geral'">
                        <div class="flexbox-container flex-align-center" ng-show="ticket.id_status === 3 && ticket.em_reprovacao && ticket.em_reprovacao_by != ticket.id_usuario_logado">
                            <div class="flex flex-fluid">
                                <div class="panel alternative">
                                    <i class="icon fa fa-hourglass-half"></i>
                                    <div class="panel-body flex-inline flex-align-center txt-left no-padding-right">
                                        <h4 class="panel-heading txt-semibold">@{{ticket.em_reprovacao_nome}}, que é membro da sua equipe, já iniciou a reprovação. Quem inicia responde por toda a equipe.</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div ng-show="(!ticket.em_reprovacao || (ticket.em_reprovacao && ticket.em_reprovacao_by == ticket.id_usuario_logado)) && (ticket.id_status === 3 && ticket.libera_status && !ticket.qtde_aprovadores || ticket.id_status === 4 && ticket.libera_status)" ng-class="{'no-border no-padding': ticket.id_status === 3 && ticket.libera_status && !ticket.qtde_aprovadores}" class="bs border-color-primary padding-top-40 no-padding-bottom">
                        <button onClick="location='{{Route('site.artwork.ticket.edit.deny.get', [$ticket->id])}}'" type="button" ng-show="ticket.id_status === 4 && ticket.libera_status" class="btn-control btn-call-to-action call-to-action-negative btn-icon btn-icon-left">
                            <span><i class="icon fa fa-times"></i>Reprovar</span>
                        </button>
                        
                        <button mw-confirm-click="acao_ticket('5');" mw-confirm-click-message="Deseja aprovar?" type="button" ng-show="ticket.id_status === 4 && ticket.libera_status && !ticket.em_reprovacao" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i>Aprovar</span>
                        </button>
        
                        <button mw-confirm-click="acao_ticket('5');" mw-confirm-click-message="Ao aprovar todas as anotações serão excluídas.<br /> Deseja confirmar?" type="button" ng-show="ticket.id_status === 4 && ticket.libera_status && ticket.em_reprovacao" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i>Aprovar</span>
                        </button>
        
                    </div>
                    
                    <div class="flexbox-container flex-align-center" ng-show="ticket.id_status === 4 && ticket.em_reprovacao && ticket.em_reprovacao_by != ticket.id_usuario_logado">
                        <div class="flex flex-fluid">    
                            <div class="panel alternative">    
                                <i class="icon fa fa-hourglass-half"></i>    
                                <div class="panel-body flex-inline flex-align-center txt-left no-padding-right">
                                    <h4 class="panel-heading txt-semibold">@{{ticket.em_reprovacao_nome}}, que é membro da sua equipe, já iniciou a reprovação. Quem inicia responde por toda a equipe.</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div ng-show="ticket.id_status === 5 && ticket.envio_finalizado && !ticket.envio_autorizado && (ticket.revisor)" class="bs border-color-primary padding-top-40 no-padding-bottom">
                        <button mw-confirm-click="acao_ticket('autorizar_envio');" mw-confirm-click-message="Deseja autorizar envio de arte-final?" ng-show="ticket.id_status === 5 && ticket.envio_finalizado && !ticket.envio_autorizado && (ticket.revisor)" type="button" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i> Autorizar Envio de Arte-final</span>
                        </button>
                    </div>
                    <div ng-show="ticket.id_status === 5 && ticket.arteFinalista && ticket.artes.length && !ticket.envio_autorizado" class="bs border-color-primary padding-top-40 no-padding-bottom">
                        <button mw-confirm-click="acao_ticket('10');" mw-confirm-click-message="Deseja finalizar a anexação de arte-final?" ng-show="ticket.id_status === 5 && ticket.arteFinalista && ticket.artes.length && !ticket.envio_finalizado" type="button" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i> Finalizar Anexação de Arte-final</span>
                        </button>
                    </div>
                    <div ng-show="ticket.id_status === 10 && ticket.participanteEnvio && ticket.artes.length && ticket.artes_envios.length && !ticket.envio_finalizado" class="bs border-color-primary padding-top-40 no-padding-bottom">
                        <button mw-confirm-click="acao_ticket('finalizar_envio_fornecedor');" mw-confirm-click-message="Deseja declarar a arte-final como enviada?" ng-show="ticket.id_status === 10 && ticket.participanteEnvio && ticket.artes_envios.length" type="button" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i><strong>Declarar Arte-final Enviada</strong></span>
                        </button>
                    </div>
                    
                    <div ng-show="!ticket.etapas[15] && ticket.id_status === 11 && ticket.envioFornecedorPdf && ticket.fornecedorPdfsTotal && !ticket.fornecedorPdfsPendentes && ticket.libera_status" class="bs border-color-primary padding-top-40 no-padding-bottom">
                        <button mw-confirm-click="acao_ticket('11');" mw-confirm-click-message="Deseja finalizar PDF do fornecedor?" ng-show="ticket.id_status === 11 && ticket.envioFornecedorPdf && ticket.fornecedorPdfsTotal && !ticket.fornecedorPdfsPendentes" type="button" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i> Finalizar PDF do Fornecedor</span>
                        </button>
                    </div>
                    <!--
                    Etapas: @{{ticket.etapas}}<br>
                    Perfis: @{{ticket.perfis}}
                    -->                
                    <div ng-show="ticket.etapas[15] && ticket.id_status !== 6 && ticket.libera_status" class="bs border-color-primary padding-top-40 no-padding-bottom">
                        <button mw-confirm-click="acao_ticket('6');" mw-confirm-click-message="Deseja encerrar o ticket?" type="button" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i><strong>Encerrar Ticket</strong></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- END Button group -->
        </form>
    </div>
    <!-- END Content -->
</div>
@stop