@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

<script>
	var id_produto = '{{ $product }}';
    var csrf_token = '{{ csrf_token() }}';
</script>

<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />

<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload-shim.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload.min.js') }}"></script>
<script src="{{ asset2('/js/angular/embalagens-resolve-loader.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/embalagens_modal_compartilhar.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/embalagens.v3.js') }}"></script>
<script src="{{ asset2('/js/site/artwork/embalagens.js') }}"></script>
                                
{!! Breadcrumbs::renderIfExists('artwork.embalagens', $family, $product) !!}

<div class="content content-fluid" ng-app="app" ng-controller="EmbalagensCtrl">
    <!-- Showcase panel -->
    <div ng-cloak class="showcase-panel flexbox-container flex-align-center clearfix">
        <div class="showcase-left">
            <div ng-if="$root.loaded">
                <span class="helper"></span>
                <div ng-if="embalagens.thumb.split('/').slice(-1).join().split('.').shift() !== 'ticket-no-image-rubrum' && embalagens.replicar_logo_marca === 0">
                    <img ng-src="@{{ embalagens.thumb }}" title="" alt="" />
                </div>
                <div ng-if="embalagens.thumb.split('/').slice(-1).join().split('.').shift() !== 'ticket-no-image-rubrum' && embalagens.replicar_logo_marca === 1"  style="width: 100%; height: 100%; background-color: @{{ embalagens.cor_hexa }}">
                    <img ng-src="@{{ embalagens.thumb }}" title="" alt="" />
                </div>
                <img ng-if="embalagens.thumb.split('/').slice(-1).join().split('.').shift() === 'ticket-no-image-rubrum'" ng-src="{{ asset2('images/layout/cliente_'. config('app.costummer_id') .'/logo-home.png') }}" title="" alt="" />
            </div>
            <div ng-if="!$root.loaded">    
                <figure class="linear-background showcase-panel-figure"></figure>
            </div>
        </div>
        <div class="showcase-body">
            <div class="showcase-container">
                <div class="flexbox-container clearfix">
                    <div class="flex-auto showcase-info showcase-item-detail text-inline grid-1-3 no-margin-bottom">
                        <div ng-if="$root.loaded">
                            <h3 class="txt-bold">@{{ embalagens.nome_produto }}</h3>
                            <small class="text-small txt-color-alt margin-top-5">produto</small>
                        </div>
                        <div ng-if="!$root.loaded">
                            <h3 class="linear-background showcase-panel-heading"></h3>
                            <small class="linear-background showcase-panel-info-secondary"></small>
                        </div>
                    </div>
                    <div class="flex-auto showcase-info showcase-item-detail text-inline grid-2-3 no-margin-bottom">
                        <div ng-if="$root.loaded">
                            <h3 class="txt-light">@{{ embalagens.marca }}</h3>
                            <small class="text-small txt-color-alt margin-top-5">marca</small>
                        </div>
                        <div ng-if="!$root.loaded">
                            <h3 class="linear-background showcase-panel-heading"></h3>
                            <small class="linear-background showcase-panel-info-secondary"></small>
                        </div>
                    </div>
                    <div class="flex-auto showcase-info showcase-item-detail text-inline grid-3-3 no-margin-bottom">
                        <div ng-if="$root.loaded">
                            <h3 class="txt-light">@{{ embalagens.categoria }}</h3>
                            <small class="text-small txt-color-alt margin-top-5">categoria</small>
                        </div>
                        <div ng-if="!$root.loaded">
                            <h3 class="linear-background showcase-panel-heading"></h3>
                            <small class="linear-background showcase-panel-info-secondary"></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Showcase panel -->
    <div ng-cloak class="artwork-container margin-bottom-40">
        <!-- Tabs -->
        <ul class="tabs clearfix">
            <li><a ng-click="tab=1" ng-class="{'active' : tab==1}" href="javascript:;">Embalagens de Mercado &bull; <span class="tabs-notifier">@{{ embalagens.ativos.promocionais.length + embalagens.ativos.regulares.length }}</span></a></li>
            <li><a ng-click="tab=3" ng-class="{'active' : tab==3}" href="javascript:;">Embalagens Descontinuadas &bull; <span class="tabs-notifier">@{{ embalagens.inativos.promocionais.length + embalagens.inativos.regulares.length }}</span></a></li>
        </ul>
        <!-- END Tabs -->
        <div ng-show="$root.loaded" class="block margin-top-20 margin-bottom-10 fadein">
            <div class="flexbox-container">
                @if((in_array('artwork_criar_embalagem', Auth::user()->modulos)))
                    <div class="artw-create-package-btn flex">
                        <a href="{{ route('site.artwork.package.create.get') }}" class="btn-function"><i class="icon fa fa-cube"></i> Nova Embalagem</a>
                    </div>
                @endif
            </div>
        </div>
        <div resolve-loader></div>
        <!-- Embalagens de Mercado -->
        <div class="tab-content" ng-show="tab == 1">
            <!-- Embalagens Regulares -->
            <div ng-show="embalagens.ativos.regulares.length && loaded" ng-class="{'sub-bs': embalagens.ativos.regulares.length, 'no-border no-margin-bottom no-padding-bottom': !embalagens.ativos.promocionais.length}" class="fadein">
                <table cellpading="0" cellspacing="0" class="artw-package-table table-border table-hover no-margin">
                    <thead>
                        <tr>
                            <th ng-click="carregar_embalagens()" width=""><span class="txt-small txt-light txt-uppercase">Embalagens Regulares</span></th>
                            <th width="50"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- ngRepeat: embalagem in embalagens.ativos.regulares -->
                        <tr ng-repeat="embalagem in embalagens.ativos.regulares" class="table-group no-animate">
                            <td colspan="2">
                                <table cellpading="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td width="">
                                                <label class="txt-package-name">
                                                    <input ng-show="embalagem.exibir_formulario" type="checkbox" ng-model="embalagem.stateJS" ng-click="exibeNovoItem(embalagem, false)" class="ng-toggle" />
                                                    <input ng-show="!embalagem.exibir_formulario" type="checkbox" ng-model="embalagem.stateJS" ng-click="exibeNovoItem(embalagem, true)" class="ng-toggle" />
                                                    <span class="txt-color-default">
                                                        <i ng-class="{'icon fa fa-folder-open txt-color-alt': embalagem.stateJS, 'icon fa fa-folder txt-color-alt': !embalagem.stateJS }" class="icon fa fa-folder txt-color-alt"></i>
                                                        <div class="text-side txt-regular">
                                                            <span class="text-side-col txt-regular margin-right-20">@{{ embalagem.nome_original }}</span>
                                                            <!--small class="txt-color-alt txt-small txt-uppercase margin-right-10" ng-repeat="mercado in embalagem.mercados">@{{ mercado.sigla }}</small-->
                                                            <!-- TODO - Código EAN -->
                                                            <a ng-show="embalagem.tem_codigo_ean && !embalagem.codigo_ean" class="ui-icon-badge ean-code margin-right-10"><i tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Código de Barras Pendente" class="fa fa-barcode"></i></a>
                                                            <a ng-show="embalagem.tem_codigo_ean && embalagem.codigo_ean" class="ui-icon-badge ean-code margin-right-10"><i tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Código de Barras" class="txt-regular txt-small">@{{embalagem.codigo_ean}}</i></a>
                                                            {{--<a ng-show="embalagem.imagem_packshelf" ng-href="@{{embalagem.imagem_packshelf}}" target="_blank" class="ui-icon-badge view-image ui-icon-badge-primary"><i tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Ver imagem do PackShelf" class="fa fa-camera"></i></a>--}}
                                                            <a ng-show="embalagem.imagem_packshelf" ng-click="mostrarImagemPackshelf(embalagem.id_sku)" class="ui-icon-badge view-image ui-icon-badge-primary"><i tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Ver imagem do PackShelf" class="fa fa-camera"></i></a>
                                                            <!-- END Código EAN -->
                                                        </div>
                                                    </span>
                                                </label>
                                            </td>
                                            <td width="50">
                                                @if( (in_array('artwork_criar_embalagem', Auth::user()->modulos)) || (in_array('artwork_excluir_descontinuar', Auth::user()->modulos)) )
                                                    <div class="dropdown drop-to-left">
                                                        <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                                        <div class="dropdown-list dropdown-list-auto">
                                                            <ul> 
                                                                @if((in_array('artwork_criar_embalagem', Auth::user()->modulos)))
                                                                    <li><a ng-click="embalagem.stateJS = true;exibeNovoItem(embalagem, true)" href="javascript:;" class="txt-bold"><span class="icon fa fa-file"></span>Adicionar Item</a></li>
                                                                @endif
                                                                
                                                                @if((in_array('artwork_criar_embalagem', Auth::user()->modulos)))
                                                                    <li><a ng-href="@{{ embalagem.link_edicao }}" class="txt-bold"><span class="icon fa fa-edit"></span>Alterar Embalagem</a></li>
                                                                @endif
                                                                <li><a href="javascript:;" class="txt-bold disabled txt-color-alt txt-color-off"><span class="icon fa fa-truck"></span>Descontinuar Embalagem</a></li>
                                                                @if((in_array('artwork_excluir_descontinuar', Auth::user()->modulos)))
                                                                    <li><a href="javascript:;" ng-click="excluir_embalagem(embalagem)" ng-class="{'disabled txt-color-alt txt-color-off':embalagem.tickets_gerais != 0}" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir Embalagem</a></li>
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    </div>
                                                @endif
                                            </td>
                                        </tr>
                                        <!-- ngRepeat: item in embalagem.itens -->
                                        <tr ng-repeat="item in embalagem.itens" ng-if="embalagem.stateJS" class="table-sub">
                                            <td colspan="2">
                                                <div class="table-sub-content slide-toggle-js">
                                                    <table cellpading="0" cellspacing="0" class="no-margin-bottom">
                                                        <tbody>
                                                            <!-- Table sub-group -->
                                                            <tr>
                                                                <td width="">
                                                                    <div ng-class="{'txt-no-link': !item.versao}" class="txt-package-item-name no-animate" ng-show="!item.exibir_editar_item">
                                                                        <a ng-href="@{{ item.versao.path }}" ng-show="item.versao" target="_BLANK" class="txt-item-name"><span class="text-side no-margin txt-regular"><i ng-class="{'fa-share-alt-square': item.compartilhado}" class="icon fa fa-file"></i><strong class="flex-inline txt-regular txt-color-default margin-left-10">@{{ item.nome }}</strong></span></a>
                                                                        <a href="javascript:;" ng-show="!item.versao" ng-class="{'txt-item-name txt-color-alt txt-no-link no-hover': !item.versao}" class="txt-item-name"><span class="text-side no-margin txt-regular"><i ng-class="{'fa-share-alt-square': item.compartilhado}" class="icon fa fa-file txt-color-unlit"></i><strong class="d-block txt-regular txt-color-alt margin-left-10">@{{ item.nome }}</strong></span></a>
                                                                        <span ng-show="!item.versao" class="txt-in-creation txt-color-alt txt-center txt-small txt-uppercase margin-left-20">Aguardando aprovação</span>
                                                                    </div>
                                                                    <div ng-show="item.exibir_editar_item" class="flexbox-container flexbox-group flex-align-center artw-add-item-sub no-animate">
                                                                        <i ng-class="{'fa-share-alt-square': item.compartilhado}" class="icon fa fa-file"></i>
                                                                        <div class="flex col-selecione-item margin-left-5 animate">
                                                                            <div class="select-control">
                                                                                <select ng-init="item.id_tipo_original = item.id_tipo" ng-model="item.id_tipo" ng-options="item_tipo.id as item_tipo.nome for item_tipo in embalagens.item_tipos" class="input-required"></select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="flexbox-container flexbox-group flex col-btn-group animate">
                                                                            <div class="flex col-member-edit-cancel">
                                                                                <button type="button" ng-click="item.exibir_editar_item = false" class="btn-control btn-alt">Cancelar</button>
                                                                            </div>
                                                                            <div class="flex col-member-edit-save">
                                                                                <button ng-click="alterar_item(item)" type="button" class="btn-control btn-positive">Salvar</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td width="150">
                                                                    <span ng-show="item.versao.numero >= 0" class="txt-packing-version txt-center"><a ng-href="@{{ item.versao.path }}" target="_BLANK">Versão @{{ item.versao.numero }}</a></span>
                                                                    <!--span ng-show="!item.versao" class="txt-in-creation txt-color-alt txt-center">Aguardando aprovação</span!-->
                                                                </td>
                                                                <td width="56">
                                                                    <span ng-show="item.numero_ticket_aberto" tooltips tooltip-side="top" tooltip-size="small" tooltip-smart="true" tooltip-template="Ticket em Andamento: @{{ item.numero_ticket_aberto }}">
                                                                        
                                                                        <a ng-if="item.link_ticket_aberto" target="_blank" ng-href="@{{ item.link_ticket_aberto }}" class="btn-function active"><i class="icon fa fa-external-link"></i></a>
                                                                        <a ng-if="!item.link_ticket_aberto" ng-href="@{{ item.link_ticket_aberto }}" ng-click="$event.preventDefault()" class="btn-function btn-in-progress"><i class="icon fa fa-ticket"></i></a>
                                                                        
                                                                    </span>
                                                                    @if((in_array('artwork_abrir_tickets', Auth::user()->modulos)))
                                                                        <span ng-show="!item.numero_ticket_aberto" tooltips tooltip-side="top" tooltip-size="small" tooltip-smart="true" tooltip-template="Abrir Ticket">    
                                                                            <a ng-href="@{{ item.link_abrir }}" ng-show="!item.numero_ticket_aberto" class="btn-function"><i class="icon fa fa-ticket"></i></a>
                                                                        </span>
                                                                    @endif
                                                                </td>
                                                                <td width="50">
                                                                    @if(
                                                                        (in_array('artwork_historico', Auth::user()->modulos)) || 
                                                                        (in_array('artwork_enviar_arte_final', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_criar_embalagem', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_baixar_arte_final', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_abrir_inspector', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_excluir_descontinuar', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_criar_embalagem', Auth::user()->modulos) ||
                                                                        Auth::user()->gerencia_embalagem || Auth::user()->gerente)
                                                                    )
                                                                    <div class="dropdown drop-to-left">
                                                                        <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                                                        <div class="dropdown-list dropdown-list-auto">
                                                                            <ul>
                                                                                @if((in_array('artwork_historico', Auth::user()->modulos)))
                                                                                    <li ng-show="item.link_historico"><a ng-href="@{{ item.link_historico }}" class="txt-bold"><span class="icon fa fa-history"></span>Histórico</a></li>
                                                                                @endif
                                                                                
                                                                                @if((in_array('artwork_enviar_arte_final', Auth::user()->modulos)))
                                                                                    <li ng-show="item.link_enviar"><a ng-href="@{{ item.link_enviar }}" class="txt-bold"><span class="icon fa fa-paper-plane"></span>Enviar Arte-Final</a></li>
                                                                                @endif
                                                                                
                                                                                @if((in_array('artwork_fornecedor_pdf', Auth::user()->modulos)))
                                                                                    <li ng-show="item.link_fornecedorpdf"><a ng-href="@{{ item.link_fornecedorpdf }}" class="txt-bold"><span class="icon fa fa-industry"></span>PDF do Fornecedor</a></li>
                                                                                @endif
                                                                                
                                                                                @if((in_array('artwork_criar_embalagem', Auth::user()->modulos)))
                                                                                    <li><a ng-click="exibeEditarItem(embalagem, item)" href="javascript:;" class="txt-bold"><span class="icon fa fa-edit"></span>Alterar Item</a></li>
                                                                                @endif
                                                                                
                                                                                @if((in_array('artwork_compartilhar_item', Auth::user()->modulos)))
                                                                                    <li ng-show="!item.compartilhado" ng-controller="compartilharItemCtrl"><a ng-click="artwItemSharing(item.id)" href="javascript:;" class="txt-bold"><span class="icon fa fa-share-alt"></span>Compartilhar Item</a></li>
                                                                                @endif
                                                                                
                                                                                <li ng-show="item.compartilhado" ng-controller="compartilharItemCtrl"><a ng-click="artwItemSharing(item.id)" href="javascript:;" class="txt-bold"><span class="icon fa fa-share-alt"></span>Ver Compartilhamento</a></li>
                                                                                
                                                                                @if((in_array('artwork_baixar_arte_final', Auth::user()->modulos)))
                                                                                    <li ng-show="item.link_arte"><a ng-href="@{{ item.link_arte }}" target="_blank" class="link-baixar-arte txt-bold"><span class="icon fa fa-download"></span>Baixar Arte-Final</a></li>
                                                                                @endif
                                                                                
                                                                                @if((in_array('artwork_abrir_inspector', Auth::user()->modulos)) && config('app.cloudflow'))
                                                                                    <li ng-show="item.versao.inspector"><a ng-href="@{{ item.versao.inspector }}" target="_blank" class="link-baixar-arte txt-bold"><span class="icon fa fa-crosshairs"></span>Abrir no Inspector</a></li>
                                                                                @endif
                                                                                
                                                                                @if((in_array('artwork_excluir_descontinuar', Auth::user()->modulos)))
                                                                                    <li><a ng-click="excluir_item(item)" href="javascript:;" ng-class="{'txt-bold disabled':item.tickets_gerais != 0}" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir Item</a></li>
                                                                                    <li><a ng-class="{'disabled': item.numero_ticket_aberto || item.compartilhado}" ng-click="descontinuar_item(item)" href="javascript:;" class="txt-bold"><span class="icon fa fa-truck"></span>Descontinuar Item</a></li>
                                                                                @endif
                                                                                
                                                                                @if((in_array('artwork_criar_embalagem', Auth::user()->modulos)))
                                                                                    <li ng-class="{'borderless': !item.versao.numero}" ng-show="!item.versao.numero"><a href="javascript:;" ng-click="artwFileUpload(embalagem.id, item.id)" class="txt-bold"><span class="icon fa fa-upload"></span>Carregar Arquivos de Arte</a></li>
                                                                                @endif

                                                                                @if(Auth::user()->gerente || Auth::user()->gerencia_embalagem)
                                                                                    <li ng-show="item.versao.numero"><a ng-click="cancelar_versao_confirm(item.versao.id_ticket)" href="javascript:;" class="txt-bold"><span class="icon fa fa-times-circle"></span>Cancelar Versão</a></li>
                                                                                @endif
                                                                                <!--li><a href="javascript:;" class="disabled"><span class="dropdown-item-ico icon-box-blank"></span>Associar Planta Técnica</a></li-->
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            <!-- END Table sub-group -->
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- END ngRepeat: item in embalagem.itens -->
                                        <!-- ngIf: !embalagem.itens.length && !embalagem.exibir_formulario && embalagem.stateJS  -->
                                        <tr ng-if="!embalagem.itens.length && !embalagem.exibir_formulario && embalagem.stateJS" class="table-sub no-animate">
                                            <td colspan="2">
                                                <div class="table-sub-content no-item slide-toggle-js">
                                                    <table cellpading="0" cellspacing="0" class="no-margin-bottom">
                                                        <tbody>
                                                            <tr>
                                                                <td width="">
                                                                    <div class="txt-regular">
                                                                        <span class="txt-regular txt-color-alt">Essa embalagem não possui nenhum item cadastrado</span>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- END ngIf: !embalagem.itens.length && !embalagem.exibir_formulario && embalagem.stateJS -->
                                        <!-- Incluir item -->
                                        <tr ng-show="embalagem.exibir_formulario" class="table-sub no-animate">
                                            <td colspan="2">
                                                <div class="table-sub-content slide-toggle-js">
                                                    <table cellpading="0" cellspacing="0" class="no-margin-bottom">
                                                        <tbody>
                                                            <!-- Table sub-group -->
                                                            <tr>
                                                                <td colspan="4">
                                                                    <div class="flexbox-container flexbox-group flex-align-center artw-add-item-sub animate">
                                                                        <i class="icon fa fa-file"></i>
                                                                        <div class="flex col-selecione-item margin-left-5">
                                                                            <div class="select-control">
                                                                                <select ng-model="embalagem.id_item_tipo_novo" name="novoItemTipo" id="novoItemTipo" class="input-required">
                                                                                    <option value="">Selecione o Item</option>
                                                                                    <option ng-repeat="item_tipo in embalagens.item_tipos" value="@{{ item_tipo.id }}">@{{ item_tipo.nome }}</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="flexbox-container flexbox-group flex col-btn-group">
                                                                            <div class="flex col-member-edit-cancel">
                                                                                <button type="button" ng-click="embalagem.exibir_formulario = false" class="btn-control btn-alt">Cancelar</button>
                                                                            </div>
                                                                            <div class="flex col-member-edit-save">
                                                                                <button type="button" ng-click="criar_item(embalagem)" class="btn-control btn-positive">Salvar</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <!-- END Table sub-group -->
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- END Incluir item -->
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <!-- END ngRepeat: embalagem in embalagens.ativos.regulares -->
                    </tbody>
                </table>
            </div>
            <!-- END Embalagens Regulares -->
            <!-- Embalagens Promocionais -->
            <div ng-show="embalagens.ativos.promocionais.length && loaded" ng-class="{'sub-bs': embalagens.ativos.promocionais.length}">
                <table cellpading="0" cellspacing="0" class="artw-package-table table-border table-hover no-margin">
                    <thead>
                        <tr>
                            <th ng-click="carregar_embalagens()" width=""><span class="txt-small txt-light txt-uppercase">Embalagens Promocionais</span></th>
                            <th width="50"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- ngRepeat: embalagem in embalagens.ativos.promocionais -->
                        <tr ng-repeat="embalagem in embalagens.ativos.promocionais" class="table-group no-animate">
                            <td colspan="2">
                                <table cellpading="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td width="">
                                                <label class="txt-package-name">
                                                    <input ng-show="embalagem.exibir_formulario" type="checkbox" ng-model="embalagem.stateJS" ng-click="exibeNovoItem(embalagem, false)" class="ng-toggle" />
                                                    <input ng-show="!embalagem.exibir_formulario" type="checkbox" ng-model="embalagem.stateJS" ng-click="exibeNovoItem(embalagem, true)" class="ng-toggle" />
                                                    <span class="txt-color-default">
                                                        <i ng-class="{'icon fa fa-folder-open txt-color-alt': embalagem.stateJS, 'icon fa fa-folder txt-color-alt': !embalagem.stateJS }" class="icon fa fa-folder txt-color-alt"></i>
                                                        <div class="text-side txt-regular">
                                                            <span class="text-side-col txt-regular margin-right-20">@{{ embalagem.nome_original }}</span>
                                                            <!--small class="txt-color-alt txt-small txt-uppercase margin-right-10" ng-repeat="mercado in embalagem.mercados">@{{ mercado.sigla }}</small-->
                                                            <!-- TODO - Código EAN -->
                                                            <a ng-show="embalagem.tem_codigo_ean && !embalagem.codigo_ean" class="ui-icon-badge ean-code margin-right-10"><i tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Código de Barras Pendente" class="fa fa-barcode"></i></a>
                                                            <a ng-show="embalagem.tem_codigo_ean && embalagem.codigo_ean" class="ui-icon-badge ean-code margin-right-10"><i tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Código de Barras" class="txt-regular txt-small">@{{embalagem.codigo_ean}}</i></a>
                                                            {{--<a ng-show="embalagem.imagem_packshelf" ng-href="@{{embalagem.imagem_packshelf}}" target="_blank" class="ui-icon-badge view-image ui-icon-badge-primary"><i tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Ver imagem do PackShelf" class="fa fa-camera"></i></a>--}}
                                                            <a ng-show="embalagem.imagem_packshelf" ng-click="mostrarImagemPackshelf(embalagem.id_sku)" class="ui-icon-badge view-image ui-icon-badge-primary"><i tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Ver imagem do PackShelf" class="fa fa-camera"></i></a>
                                                            <!-- END Código EAN -->
                                                        </div>
                                                    </span>
                                                </label>
                                            </td>
                                            <td width="50">
                                                @if( (in_array('artwork_criar_embalagem', Auth::user()->modulos)) || (in_array('artwork_excluir_descontinuar', Auth::user()->modulos)) )
                                                    <div class="dropdown drop-to-left">
                                                        <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                                        <div class="dropdown-list dropdown-list-auto">
                                                            <ul>
                                                                @if((in_array('artwork_criar_embalagem', Auth::user()->modulos)))
                                                                    <li><a ng-click="embalagem.stateJS = true;exibeNovoItem(embalagem, true)" href="javascript:;" class="txt-bold"><span class="icon fa fa-file"></span>Adicionar Item</a></li>
                                                                @endif
                                                                
                                                                @if((in_array('artwork_criar_embalagem', Auth::user()->modulos)))
                                                                    <li><a ng-href="@{{ embalagem.link_edicao }}" class="txt-bold"><span class="icon fa fa-edit"></span>Alterar Embalagem</a></li>
                                                                @endif
                                                                
                                                                <li><a href="javascript:;" class="txt-bold disabled txt-color-alt txt-color-off"><span class="icon fa fa-truck"></span>Descontinuar Embalagem</a></li>
                                                                
                                                                @if((in_array('artwork_excluir_descontinuar', Auth::user()->modulos)))
                                                                    <li><a href="javascript:;" ng-click="excluir_embalagem(embalagem)" ng-class="{'disabled txt-color-alt txt-color-off':embalagem.tickets_gerais != 0}" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir Embalagem</a></li>
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    </div>
                                                @endif
                                            </td>
                                        </tr>
                                        <!-- ngRepeat: item in embalagem.itens -->
                                        <tr ng-repeat="item in embalagem.itens" ng-if="embalagem.stateJS" class="table-sub">
                                            <td colspan="2">
                                                <div class="table-sub-content slide-toggle-js">
                                                    <table cellpading="0" cellspacing="0" class="no-margin-bottom">
                                                        <tbody>
                                                            <!-- Table sub-group -->
                                                            <tr>
                                                                <td width="">
                                                                    <div ng-class="{'txt-no-link': !item.versao}" class="txt-package-item-name no-animate" ng-show="!item.exibir_editar_item">
                                                                        <a ng-href="@{{ item.versao.path }}" ng-show="item.versao" target="_BLANK" class="txt-item-name"><span class="text-side no-margin txt-regular"><i ng-class="{'fa-share-alt-square': item.compartilhado}" class="icon fa fa-file"></i><strong class="flex-inline txt-regular txt-color-default margin-left-10">@{{ item.nome }}</strong></span></a>
                                                                        <a href="javascript:;" ng-show="!item.versao" ng-class="{'txt-item-name txt-color-alt no-hover': !item.versao}" class="txt-item-name"><span class="text-side no-margin txt-regular"><i ng-class="{'fa-share-alt-square': item.compartilhado}" class="icon fa fa-file txt-color-unlit"></i><strong class="d-block txt-regular txt-color-alt margin-left-10">@{{ item.nome }}</strong></span></a>
                                                                        <span ng-show="!item.versao" class="txt-in-creation txt-small txt-color-alt txt-uppercase txt-center margin-left-20">Aguardando aprovação</span>
                                                                    </div>
                                                                    <div ng-show="item.exibir_editar_item" class="flexbox-container flexbox-group flex-align-center artw-add-item-sub no-animate">
                                                                        <i ng-class="{'fa-share-alt-square': item.compartilhado}" class="icon fa fa-file"></i>
                                                                        <div class="flex col-selecione-item margin-left-5 animate">
                                                                            <div class="select-control">
                                                                                <select ng-init="item.id_tipo_original = item.id_tipo" ng-model="item.id_tipo" ng-options="item_tipo.id as item_tipo.nome for item_tipo in embalagens.item_tipos" class="input-required"></select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="flexbox-container flexbox-group flex col-btn-group animate">
                                                                            <div class="flex col-member-edit-cancel">
                                                                                <button type="button" ng-click="item.exibir_editar_item = false" class="btn-control btn-alt">Cancelar</button>
                                                                            </div>
                                                                            <div class="flex col-member-edit-save">
                                                                                <button ng-click="alterar_item(item)" type="button" class="btn-control btn-positive">Salvar</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td width="150">
                                                                    <span ng-show="item.versao.numero >= 0" class="txt-packing-version txt-center"><a ng-href="@{{ item.versao.path }}" target="_BLANK">Versão @{{ item.versao.numero }}</a></span>
                                                                    <!--span ng-show="!item.versao" class="txt-in-creation txt-color-alt txt-center">Aguardando aprovação</span-->
                                                                </td>
                                                                <td width="56">
                                                                    <span ng-show="item.numero_ticket_aberto" tooltips tooltip-side="top" tooltip-size="small" tooltip-smart="true" tooltip-template="Ticket em Andamento: @{{ item.numero_ticket_aberto }}">
                                                                        
                                                                        <a ng-if="item.link_ticket_aberto" target="_blank" ng-href="@{{ item.link_ticket_aberto }}" class="btn-function active"><i class="icon fa fa-external-link"></i></a>
                                                                        <a ng-if="!item.link_ticket_aberto" ng-href="@{{ item.link_ticket_aberto }}" ng-click="$event.preventDefault()" class="btn-function btn-in-progress"><i class="icon fa fa-ticket"></i></a>
                                                                        
                                                                    </span>
                                                                    @if((in_array('artwork_abrir_tickets', Auth::user()->modulos)))
                                                                        <span ng-show="!item.numero_ticket_aberto" tooltips tooltip-side="top" tooltip-size="small" tooltip-smart="true" tooltip-template="Abrir Ticket">    
                                                                            <a ng-href="@{{ item.link_abrir }}" ng-show="!item.numero_ticket_aberto" class="btn-function"><i class="icon fa fa-ticket"></i></a>
                                                                        </span>
                                                                    @endif
                                                                </td>
                                                                <td width="50">
                                                                    @if(
                                                                        (in_array('artwork_historico', Auth::user()->modulos)) || 
                                                                        (in_array('artwork_enviar_arte_final', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_criar_embalagem', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_baixar_arte_final', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_abrir_inspector', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_excluir_descontinuar', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_criar_embalagem', Auth::user()->modulos))
                                                                    )
                                                                    <div class="dropdown drop-to-left">
                                                                        <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                                                        <div class="dropdown-list dropdown-list-auto">
                                                                            <ul>
                                                                                @if((in_array('artwork_historico', Auth::user()->modulos)))
                                                                                    <li ng-show="item.link_historico"><a ng-href="@{{ item.link_historico }}" class="txt-bold"><span class="icon fa fa-history"></span>Histórico</a></li>
                                                                                @endif
                                                                                @if((in_array('artwork_enviar_arte_final', Auth::user()->modulos)))
                                                                                    <li ng-show="item.link_enviar"><a ng-href="@{{ item.link_enviar }}" class="txt-bold"><span class="icon fa fa-paper-plane"></span>Enviar Arte-Final</a></li>
                                                                                @endif

                                                                                @if((in_array('artwork_fornecedor_pdf', Auth::user()->modulos)))
                                                                                    <li ng-show="item.link_fornecedorpdf"><a ng-href="@{{ item.link_fornecedorpdf }}" class="txt-bold"><span class="icon fa fa-industry"></span>PDF do Fornecedor</a></li>
                                                                                @endif
                                                                                                                                                                
                                                                                @if((in_array('artwork_criar_embalagem', Auth::user()->modulos)))
                                                                                    <li><a ng-click="exibeEditarItem(embalagem, item)" href="javascript:;" class="txt-bold"><span class="icon fa fa-edit"></span>Alterar Item</a></li>
                                                                                @endif

                                                                                <li ng-show="!item.compartilhado" ng-controller="compartilharItemCtrl"><a ng-click="artwItemSharing(item.id)" href="javascript:;" class="txt-bold"><span class="icon fa fa-share-alt"></span>Compartilhar Item</a></li>
                                                                                <li ng-show="item.compartilhado" ng-controller="compartilharItemCtrl"><a ng-click="artwItemSharing(item.id)" href="javascript:;" class="txt-bold"><span class="icon fa fa-share-alt"></span>Ver Compartilhamento</a></li>
                                                                                                                                                                                                                                                
                                                                                @if((in_array('artwork_baixar_arte_final', Auth::user()->modulos)))
                                                                                    <li ng-show="item.link_arte"><a ng-href="@{{ item.link_arte }}" target="_blank" class="link-baixar-arte txt-bold"><span class="icon fa fa-download"></span>Baixar Arte-Final</a></li>
                                                                                @endif
                                                                                @if((in_array('artwork_abrir_inspector', Auth::user()->modulos)) && config('app.cloudflow'))
                                                                                    <li ng-show="item.versao.inspector"><a ng-href="@{{ item.versao.inspector }}" target="_blank" class="link-baixar-arte txt-bold"><span class="icon fa fa-crosshairs"></span>Abrir no Inspector</a></li>
                                                                                @endif
                                                                                @if((in_array('artwork_excluir_descontinuar', Auth::user()->modulos)))
                                                                                    <li><a ng-click="excluir_item(item)" href="javascript:;" ng-class="{'txt-bold disabled':item.tickets_gerais != 0}" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir Item</a></li>
                                                                                    <li><a ng-class="{'disabled': item.numero_ticket_aberto || item.compartilhado}" ng-click="descontinuar_item(item)" href="javascript:;" class="txt-bold"><span class="icon fa fa-truck"></span>Descontinuar Item</a></li>
                                                                                @endif
                                                                                @if((in_array('artwork_criar_embalagem', Auth::user()->modulos)))
                                                                                    <li ng-class="{'borderless': !item.versao.numero}" ng-show="!item.versao.numero"><a href="javascript:;" ng-click="artwFileUpload(embalagem.id, item.id)" class="txt-bold"><span class="icon fa fa-upload"></span>Carregar Arquivos de Arte</a></li>
                                                                                @endif
                                                                                
                                                                                @if(Auth::user()->gerente || Auth::user()->gerencia_embalagem)
                                                                                    <li ng-show="item.versao.numero"><a ng-click="cancelar_versao_confirm(item.versao.id_ticket)" href="javascript:;" class="txt-bold"><span class="icon fa fa-times-circle"></span>Cancelar Versão</a></li>
                                                                                @endif
                                                                                <!--li><a href="javascript:;" class="disabled"><span class="dropdown-item-ico icon-box-blank"></span>Associar Planta Técnica</a></li-->
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            <!-- END Table sub-group -->
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- END ngRepeat: item in embalagem.itens -->
                                        <!-- ngIf: !embalagem.itens.length && !embalagem.exibir_formulario && embalagem.stateJS  -->
                                        <tr ng-if="!embalagem.itens.length && !embalagem.exibir_formulario && embalagem.stateJS" class="table-sub no-animate">
                                            <td colspan="2">
                                                <div class="table-sub-content no-item slide-toggle-js">
                                                    <table cellpading="0" cellspacing="0" class="no-margin-bottom">
                                                        <tbody>
                                                            <tr>
                                                                <td width="">
                                                                    <div class="txt-regular">
                                                                        <span class="txt-regular txt-color-alt">Essa embalagem não possui nenhum item cadastrado</span>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- END ngIf: !embalagem.itens.length && !embalagem.exibir_formulario && embalagem.stateJS -->
                                        <!-- Incluir item -->
                                        <tr ng-show="embalagem.exibir_formulario" class="table-sub no-animate">
                                            <td colspan="2">
                                                <div class="table-sub-content slide-toggle-js">
                                                    <table cellpading="0" cellspacing="0" class="no-margin-bottom">
                                                        <tbody>
                                                            <!-- Table sub-group -->
                                                            <tr>
                                                                <td colspan="4">
                                                                    <div class="flexbox-container flexbox-group flex-align-center artw-add-item-sub animate">
                                                                        <i class="icon fa fa-file"></i>
                                                                        <div class="flex col-selecione-item margin-left-5">
                                                                            <div class="select-control">
                                                                                <select ng-model="embalagem.id_item_tipo_novo" name="novoItemTipo" id="novoItemTipo">
                                                                                    <option value="">Selecione o Item</option>
                                                                                    <option ng-repeat="item_tipo in embalagens.item_tipos" value="@{{ item_tipo.id }}">@{{ item_tipo.nome }}</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="flexbox-container flexbox-group flex col-btn-group">
                                                                            <div class="flex col-member-edit-cancel">
                                                                                <button type="button" ng-click="embalagem.exibir_formulario = false" class="btn-control btn-alt">Cancelar</button>
                                                                            </div>
                                                                            <div class="flex col-member-edit-save">
                                                                                <button type="button" ng-click="criar_item(embalagem)" class="btn-control btn-positive">Salvar</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <!-- END Table sub-group -->
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- END Incluir item -->
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <!-- END ngRepeat: embalagem in embalagens.ativos.promocionais -->
                    </tbody>
                </table>
            </div>
            <!-- END Embalagens Promocinais -->
            <div ng-if="!embalagens.ativos.regulares.length && !embalagens.ativos.promocionais.length && $root.loaded" class="artw-no-item fas fa-truck">
                Todas as embalagens deste produto estão descontinuadas
            </div>
        </div>
        <!-- END Embalagens de Mercado -->
        <!-- Embalagens de Descontinuadas -->
        <div class="tab-content" ng-show="tab == 3">
            <!-- Embalagens Regulares -->
            <div ng-show="embalagens.inativos.regulares.length && loaded" ng-class="{'sub-bs': embalagens.inativos.regulares.length, 'no-border no-margin-bottom no-padding-bottom': !embalagens.inativos.promocionais.length}">
                <table cellpading="0" cellspacing="0" class="artw-package-table table-border table-hover no-margin">
                    <thead>
                        <tr>
                            <th colspan="2" ng-click="carregar_embalagens()" width=""><span class="txt-small txt-light txt-uppercase">Embalagens Regulares</span></th>
                            <th colspan="1" width="50"></th>
                            <!--th width="50"></th-->
                        </tr>
                    </thead>
                    <tbody>
                        <!-- ngRepeat: embalagem in embalagens.inativos.regulares -->
                        <tr ng-repeat="embalagem in embalagens.inativos.regulares" class="table-group no-animate">
                            <td colspan="3">
                                <table cellpading="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td colspan="2" width="">
                                                <label class="txt-package-name">
                                                    <input ng-show="embalagem.exibir_formulario" type="checkbox" ng-model="embalagem.stateJS" ng-click="exibeNovoItem(embalagem, false)" class="ng-toggle" />
                                                    <input ng-show="!embalagem.exibir_formulario" type="checkbox" ng-model="embalagem.stateJS" ng-click="exibeNovoItem(embalagem, true)" class="ng-toggle" />
                                                    <span class="txt-color-default">
                                                        <i ng-class="{'icon fa fa-folder-open txt-color-alt': embalagem.stateJS, 'icon fa fa-folder txt-color-alt': !embalagem.stateJS }" class="icon fa fa-folder txt-color-alt"></i>
                                                        <div class="text-side txt-regular">
                                                            <span class="text-side-col txt-regular margin-right-20">@{{ embalagem.nome_original }}</span>
                                                            <!--small class="txt-color-alt txt-small txt-uppercase margin-right-10" ng-repeat="mercado in embalagem.mercados">@{{ mercado.sigla }}</small-->
                                                            <!-- TODO - Código EAN -->
                                                            <a ng-show="embalagem.tem_codigo_ean && !embalagem.codigo_ean" class="ui-icon-badge ean-code margin-right-10"><i tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Código de Barras Pendente" class="fa fa-barcode"></i></a>
                                                            <a ng-show="embalagem.tem_codigo_ean && embalagem.codigo_ean" class="ui-icon-badge ean-code margin-right-10"><i tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Código de Barras" class="txt-regular txt-small">@{{embalagem.codigo_ean}}</i></a>
                                                            {{--<a ng-show="embalagem.imagem_packshelf" ng-href="@{{embalagem.imagem_packshelf}}" target="_blank" class="ui-icon-badge view-image ui-icon-badge-primary"><i tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Ver imagem do PackShelf" class="fa fa-camera"></i></a>--}}
                                                            <a ng-show="embalagem.imagem_packshelf" ng-click="mostrarImagemPackshelf(embalagem.id_sku)" class="ui-icon-badge view-image ui-icon-badge-primary"><i tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Ver imagem do PackShelf" class="fa fa-camera"></i></a>
                                                            <!-- END Código EAN -->
                                                        </div>
                                                    </span>
                                                </label>
                                            </td>
                                            <!--td width="150"></td-->
                                            <td colspan="1" width="50">
                                                @if( (in_array('artwork_criar_embalagem', Auth::user()->modulos)) || (in_array('artwork_excluir_descontinuar', Auth::user()->modulos)) )
                                                    <div class="dropdown drop-to-left">
                                                        <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                                        <div class="dropdown-list dropdown-list-auto">
                                                            <ul>    
                                                                @if((in_array('artwork_criar_embalagem', Auth::user()->modulos)))
                                                                    <li><a ng-href="@{{ embalagem.link_edicao }}" class="txt-bold"><span class="icon fa fa-edit"></span>Alterar Embalagem</a></li>
                                                                @endif
                                                                @if((in_array('artwork_excluir_descontinuar', Auth::user()->modulos)))
                                                                    <li><a ng-click="excluir_embalagem(embalagem)" href="javascript:;" ng-class="{'disabled':embalagem.tickets_gerais != 0}" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir Embalagem</a></li>
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    </div>
                                                @endif
                                            </td>
                                        </tr>
                                        <!-- ngRepeat: item in embalagem.itens -->
                                        <tr ng-repeat="item in embalagem.itens" ng-if="embalagem.stateJS" class="table-sub">
                                            <td colspan="3">
                                                <div class="table-sub-content slide-toggle-js">
                                                    <table cellpading="0" cellspacing="0" class="no-margin-bottom">
                                                        <tbody>
                                                            <!-- Table sub-group -->
                                                            <tr>
                                                                <td width="">
                                                                    <div ng-class="{'txt-no-link': !item.versao}" class="txt-package-item-name no-animate" ng-show="!item.exibir_editar_item">
                                                                        <a ng-href="@{{ item.versao.path }}" ng-show="item.versao" target="_BLANK" class="txt-item-name"><span class="text-side no-margin txt-regular"><i class="icon fa fa-file"></i><strong class="flex-inline txt-regular txt-color-default margin-left-10">@{{ item.nome }}</strong></span></a>
                                                                        <a href="javascript:;" ng-show="!item.versao" ng-class="{'txt-item-name txt-color-alt no-hover': !item.versao}" class="txt-item-name"><span class="text-side no-margin txt-regular"><i class="icon fa fa-file txt-color-unlit"></i><strong class="d-block txt-regular txt-color-alt margin-left-10">@{{ item.nome }}</strong></span></a>
                                                                        <span ng-show="!item.versao" class="txt-in-creation txt-color-alt txt-small txt-uppercase txt-center margin-left-20">Aguardando aprovação</span>
                                                                    </div>
                                                                    <div ng-show="item.exibir_editar_item" class="flexbox-container flex-align-center flexbox-group artw-add-item-sub no-animate">
                                                                        <i ng-class="{'fa-share-alt-square': item.compartilhado}" class="icon fa fa-file"></i>
                                                                        <div class="flex col-selecione-item margin-left-5 animate">
                                                                            <div class="select-control">
                                                                                <select ng-init="item.id_tipo_original = item.id_tipo" ng-model="item.id_tipo" ng-options="item_tipo.id as item_tipo.nome for item_tipo in embalagens.item_tipos" class="input-required"></select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="flexbox-container flexbox-group flex col-btn-group animate">
                                                                            <div class="flex col-member-edit-cancel">
                                                                                <button type="button" ng-click="item.exibir_editar_item = false" class="btn-control btn-alt">Cancelar</button>
                                                                            </div>
                                                                            <div class="flex col-member-edit-save">
                                                                                <button ng-click="alterar_item(item)" type="button" class="btn-control btn-positive">Salvar</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td width="150">
                                                                    <span ng-show="item.versao.numero >= 0" class="txt-packing-version txt-center"><a ng-href="@{{ item.versao.path }}" target="_BLANK">Versão @{{ item.versao.numero }}</a></span>
                                                                    <!--span ng-show="!item.versao" class="txt-in-creation txt-color-alt txt-center">Aguardando aprovação</span-->
                                                                </td>
                                                                <td width="50">
                                                                    @if(
                                                                        (in_array('artwork_historico', Auth::user()->modulos)) || 
                                                                        (in_array('artwork_enviar_arte_final', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_criar_embalagem', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_baixar_arte_final', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_abrir_inspector', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_excluir_descontinuar', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_criar_embalagem', Auth::user()->modulos))
                                                                    )
                                                                        <div class="dropdown drop-to-left">
                                                                            <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                                                            <div class="dropdown-list dropdown-list-auto">
                                                                                <ul>
                                                                                    @if((in_array('artwork_historico', Auth::user()->modulos)))
                                                                                        <li ng-show="item.link_historico"><a ng-href="@{{ item.link_historico }}" class="txt-bold"><span class="icon fa fa-history"></span>Histórico</a></li>
                                                                                    @endif

                                                                                    @if((in_array('artwork_baixar_arte_final', Auth::user()->modulos)))
                                                                                        <li ng-show="item.link_arte"><a ng-href="@{{ item.link_arte }}" target="_blank" class="link-baixar-arte txt-bold"><span class="icon fa fa-download"></span>Baixar Arte-Final</a></li>
                                                                                    @endif
                                                                                    
                                                                                    @if((in_array('artwork_abrir_inspector', Auth::user()->modulos)) && config('app.cloudflow'))
                                                                                        <li ng-show="item.versao.inspector"><a ng-href="@{{ item.versao.inspector }}" target="_blank" class="link-baixar-arte txt-bold"><span class="icon fa fa-crosshairs"></span>Abrir no Inspector</a></li>
                                                                                    @endif

                                                                                    @if((in_array('artwork_excluir_descontinuar', Auth::user()->modulos)))
                                                                                        <li><a ng-click="excluir_item(item)" href="javascript:;" ng-class="{'disabled':item.tickets_gerais != 0}" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir Item</a></li>
                                                                                        <li><a ng-click="retornar_item(item)" href="javascript:;" class="txt-bold"><span class="icon fa fa-shopping-cart"></span>Retornar para Mercado</a></li>
                                                                                    @endif
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            <!-- END Table sub-group -->
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- END ngRepeat: item in embalagem.itens -->
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <!-- END ngRepeat: embalagem in embalagens.inativos.regulares -->
                    </tbody>
                </table>
            </div>
            <!-- END Embalagens Regulares -->
            <!-- Embalagens Promocionais -->
            <div ng-show="embalagens.inativos.promocionais.length && loaded" ng-class="{'sub-bs': embalagens.inativos.promocionais.length}">
                <table cellpading="0" cellspacing="0" class="artw-package-table table-border table-hover no-margin">
                    <thead>
                        <tr>
                            <th colspan="2" ng-click="carregar_embalagens()" width=""><span class="txt-small txt-light txt-uppercase">Embalagens Promocionais</span></th>
                            <th colspan="1" width="50"></th>
                            <!--th width="50"></th-->
                        </tr>
                    </thead>
                    <tbody>
                        <!-- ngRepeat: embalagem in embalagens.inativos.promocionais -->
                        <tr ng-repeat="embalagem in embalagens.inativos.promocionais" class="table-group no-animate">
                            <td colspan="3">
                                <table cellpading="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td colspan="2" width="">
                                                <label class="txt-package-name">
                                                    <input ng-show="embalagem.exibir_formulario" type="checkbox" ng-model="embalagem.stateJS" ng-click="exibeNovoItem(embalagem, false)" class="ng-toggle" />
                                                    <input ng-show="!embalagem.exibir_formulario" type="checkbox" ng-model="embalagem.stateJS" ng-click="exibeNovoItem(embalagem, true)" class="ng-toggle" />
                                                    <span class="txt-color-default">
                                                        <i ng-class="{'icon fa fa-folder-open txt-color-alt': embalagem.stateJS, 'icon fa fa-folder txt-color-alt': !embalagem.stateJS }" class="icon fa fa-folder txt-color-alt"></i>
                                                        <div class="text-side txt-regular">
                                                            <span class="text-side-col txt-regular margin-right-20">@{{ embalagem.nome_original }}</span>
                                                            <!--small class="txt-color-alt txt-small txt-uppercase margin-right-10" ng-repeat="mercado in embalagem.mercados">@{{ mercado.sigla }}</small-->
                                                            <!-- TODO - Código EAN -->
                                                            <a ng-show="embalagem.tem_codigo_ean && !embalagem.codigo_ean" class="ui-icon-badge ean-code margin-right-10"><i tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Código de Barras Pendente" class="fa fa-barcode"></i></a>
                                                            <a ng-show="embalagem.tem_codigo_ean && embalagem.codigo_ean" class="ui-icon-badge ean-code margin-right-10"><i tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Código de Barras" class="txt-regular txt-small">@{{embalagem.codigo_ean}}</i></a>
                                                            {{--<a ng-show="embalagem.imagem_packshelf" ng-href="@{{embalagem.imagem_packshelf}}" target="_blank" class="ui-icon-badge view-image ui-icon-badge-primary"><i tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Ver imagem do PackShelf" class="fa fa-camera"></i></a>--}}
                                                            <a ng-show="embalagem.imagem_packshelf" ng-click="mostrarImagemPackshelf(embalagem.id_sku)" class="ui-icon-badge view-image ui-icon-badge-primary"><i tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Ver imagem do PackShelf" class="fa fa-camera"></i></a>
                                                            <!-- END Código EAN -->
                                                        </div>
                                                    </span>
                                                </label>
                                            </td>
                                            <!--td width="150"></td-->
                                            <td colspan="1" width="50">
                                                @if( (in_array('artwork_criar_embalagem', Auth::user()->modulos)) || (in_array('artwork_excluir_descontinuar', Auth::user()->modulos)) )
                                                    <div class="dropdown drop-to-left">
                                                        <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                                        <div class="dropdown-list dropdown-list-auto">
                                                            <ul>    
                                                                @if((in_array('artwork_criar_embalagem', Auth::user()->modulos)))
                                                                    <li><a ng-href="@{{ embalagem.link_edicao }}" class="txt-bold"><span class="icon fa fa-edit"></span>Alterar Embalagem</a></li>
                                                                @endif
                                                                
                                                                @if((in_array('artwork_excluir_descontinuar', Auth::user()->modulos)))
                                                                    <li><a ng-click="excluir_embalagem(embalagem)" href="javascript:;" ng-class="{'disabled':embalagem.tickets_gerais != 0}" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir Embalagem</a></li>
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    </div>
                                                @endif
                                            </td>
                                        </tr>
                                        <!-- ngRepeat: item in embalagem.itens -->
                                        <tr ng-repeat="item in embalagem.itens" ng-if="embalagem.stateJS" class="table-sub">
                                            <td colspan="3">
                                                <div class="table-sub-content slide-toggle-js">
                                                    <table cellpading="0" cellspacing="0" class="no-margin-bottom">
                                                        <tbody>
                                                            <!-- Table sub-group -->
                                                            <tr>
                                                                <td width="">
                                                                    <div ng-class="{'txt-no-link': !item.versao}" class="txt-package-item-name no-animate" ng-show="!item.exibir_editar_item">
                                                                        <a ng-href="@{{ item.versao.path }}" ng-show="item.versao" target="_BLANK" class="txt-item-name"><span class="text-side no-margin txt-regular"><i class="icon fa fa-file"></i><strong class="flex-inline txt-regular txt-color-default margin-left-10">@{{ item.nome }}</strong></span></a>
                                                                        <a href="javascript:;" ng-show="!item.versao" ng-class="{'txt-item-name txt-color-alt no-hover': !item.versao}" class="txt-item-name"><span class="text-side no-margin txt-regular"><i class="icon fa fa-file txt-color-unlit"></i><strong class="d-block txt-regular txt-color-alt margin-left-10">@{{ item.nome }}</strong></span></a>
                                                                        <span ng-show="!item.versao" class="txt-in-creation txt-color-alt txt-small txt-center margin-left-20">Aguardando aprovação</span>
                                                                    </div>
                                                                    <div ng-show="item.exibir_editar_item" class="flexbox-container flexbox-group flex-align-center artw-add-item-sub no-animate">
                                                                        <i ng-class="{'fa-share-alt-square': item.compartilhado}" class="icon fa fa-file"></i>
                                                                        <div class="flex col-selecione-item margin-left-5 animate">
                                                                            <div class="select-control">
                                                                                <select ng-init="item.id_tipo_original = item.id_tipo" ng-model="item.id_tipo" ng-options="item_tipo.id as item_tipo.nome for item_tipo in embalagens.item_tipos" class="input-required"></select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="flexbox-container flexbox-group flex col-btn-group animate">
                                                                            <div class="flex col-member-edit-cancel">
                                                                                <button type="button" ng-click="item.exibir_editar_item = false" class="btn-control btn-alt">Cancelar</button>
                                                                            </div>
                                                                            <div class="flex col-member-edit-save">
                                                                                <button ng-click="alterar_item(item)" type="button" class="btn-control btn-positive">Salvar</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td width="150">
                                                                    <span ng-show="item.versao.numero >= 0" class="txt-packing-version txt-center"><a ng-href="@{{ item.versao.path }}" target="_BLANK">Versão @{{ item.versao.numero }}</a></span>
                                                                    <!--span ng-show="!item.versao" class="txt-in-creation txt-color-alt txt-center">Aguardando aprovação</span-->
                                                                </td>
                                                                <td width="50">
                                                                    @if(
                                                                        (in_array('artwork_historico', Auth::user()->modulos)) || 
                                                                        (in_array('artwork_enviar_arte_final', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_criar_embalagem', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_baixar_arte_final', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_abrir_inspector', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_excluir_descontinuar', Auth::user()->modulos)) ||
                                                                        (in_array('artwork_criar_embalagem', Auth::user()->modulos))
                                                                    )
                                                                        <div class="dropdown drop-to-left">
                                                                            <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                                                            <div class="dropdown-list dropdown-list-auto">
                                                                                <ul>
                                                                                    @if((in_array('artwork_historico', Auth::user()->modulos)))
                                                                                        <li ng-show="item.link_historico"><a ng-href="@{{ item.link_historico }}" class="txt-bold"><span class="icon fa fa-history"></span>Histórico</a></li>
                                                                                    @endif
                                                                                    
                                                                                    @if((in_array('artwork_baixar_arte_final', Auth::user()->modulos)))
                                                                                        <li ng-show="item.link_arte"><a ng-href="@{{ item.link_arte }}" target="_blank" class="link-baixar-arte txt-bold"><span class="icon fa fa-download"></span>Baixar Arte-Final</a></li>
                                                                                    @endif
                                                                                    
                                                                                    @if((in_array('artwork_abrir_inspector', Auth::user()->modulos)) && config('app.cloudflow'))
                                                                                        <li ng-show="item.versao.inspector"><a ng-href="@{{ item.versao.inspector }}" target="_blank" class="link-baixar-arte txt-bold"><span class="icon fa fa-crosshairs"></span>Abrir no Inspector</a></li>
                                                                                    @endif
                                                                                    
                                                                                    @if((in_array('artwork_excluir_descontinuar', Auth::user()->modulos)))
                                                                                        <li><a ng-click="excluir_item(item)" href="javascript:;" ng-class="{'disabled':item.tickets_gerais != 0}" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir Item</a></li>
                                                                                        <li><a ng-click="retornar_item(item)" href="javascript:;" class="txt-bold"><span class="icon fa fa-shopping-cart"></span>Retornar para Mercado</a></li>
                                                                                    @endif
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            <!-- END Table sub-group -->
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- END ngRepeat: item in embalagem.itens -->
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <!-- END ngRepeat: embalagem in embalagens.inativos.promocionais -->
                    </tbody>
                </table>
            </div>
            <!-- END Embalagens Promocinais -->
            <div ng-if="!embalagens.inativos.regulares.length && !embalagens.inativos.promocionais.length && $root.loaded" class="artw-no-item fa fa-shopping-cart">
                Todas as embalagens deste produto estão ativas em mercado
            </div>
        </div>
        <!-- END Embalagens Descontinuadas -->
    </div>
</div>
@stop
