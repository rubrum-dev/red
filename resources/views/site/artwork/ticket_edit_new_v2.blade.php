@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
{!! Breadcrumbs::renderIfExists('artwork.ticket_editar', $ticket->id) !!}
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />

<script>
    var id_ticket = {{$ticket->id}};
    var id_usuario = {{Auth::user()->id}};
    var csrf_token = '{{ csrf_token() }}';
    var edicao = true;
</script>
<script>
    $(function () {
        if ($("#tfStartDate").val()){
            var tfStartDate = $("#tfStartDate").val().split("/");
            var minDate = new Date(tfStartDate[2], tfStartDate[1] - 1, tfStartDate[0]);
            minDate.setDate(minDate.getDate() + 1);
            var minDate2 = new Date(tfStartDate[2], tfStartDate[1] - 1, tfStartDate[0]);
            minDate2.setDate(minDate2.getDate() + 2);

            var dateFormat = "dd/mm/yy",
                from = $("#tfArtSendDate")
            
            .datepicker({
                //defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                minDate: minDate,
                beforeShow:function(textbox, instance){
                    var rect = textbox.getBoundingClientRect();
                    
                    setTimeout(function () {
                        $('.artw-approval-date-col').append($('#ui-datepicker-div'));
                    }, 0);
                }
            })
            .on("change", function () {
                to.datepicker("option", "minDate", getDate(this, 1));
            }),
            to = $("#tfDeadlineDate").datepicker({
                //defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                minDate: minDate2,
                beforeShow:function(textbox, instance){
                    var rect = textbox.getBoundingClientRect();
                    
                    setTimeout(function () {
                        $('.artw-closure-date-col').append($('#ui-datepicker-div'));
                    }, 0);
                }
            })
            .on("change", function () {
                from.datepicker("option", "maxDate", getDate(this, -1));
            });
        }

        function getDate(element, qtd) {
            var date;
            try {
                var date2 = $.datepicker.parseDate(dateFormat, element.value);
                date2.setDate(date2.getDate() + qtd);
                date = date2;
            } catch (error) {
                date = null;
            }
            return date;
        }
    });
</script>
<script>
    $(function () {
        $('#formAltRequest').submit(function () {
            if($('.team-member-group').is(':visible') && $('.selectize-input input[type="text"]').val() === '') {
                $('.team-member-group selectize[name="cbMembershipTeamName[]"] + .selectize-control .selectize-input').addClass('input-error');
                myAlert('Selecione um membro da equipe.', 'warning');
                return false;
            }
        });
    });
</script>
<script src="{{ asset('/js/site/jquery-ui.min.js')}}"></script>
<script src="{{ asset('/js/site/ui.datepicker-pt-BR.js')}}"></script>
<script src="{{ asset2('/js/site/artwork/package-alteration-request.js') }}"></script>
<script src="{{ asset('/js/angular/angular.min.js')}}"></script>
<script src="{{ asset('/js/angular/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.min.js')}}"></script>
<script src="{{ asset2('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js')}}"></script>
<script src="{{ asset2('/js/angular/controllers/embalagens_modal_compartilhar.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edit_new_v2.js')}}"></script>
<div class="content content-fluid" ng-app="app" ng-controller="TicketEditCtrl" ng-cloak>
    @if (count($errors) > 0)
        <div class="alert alert-danger" role="alert">
            <div class="alert-error">
                @foreach ($errors->all() as $error)
                    {{ $error}}<br />
                @endforeach
            </div>
        </div>
    @endif
    
    @include('site.artwork.ticket_topo_inc')
    
    <!-- Timeline -->
    <div class="timeline" ng-cloak>
        <div ng-class="(ticket.etapas[0] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-flag"></i>
                <span class="event-txt">Aberto</span>
            </div>
        </div>
        <div ng-class="(ticket.etapas[2] === 1) ? 'event-box is-done' : 'event-box'" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[2] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-paint-brush"></i>
                <span class="event-txt">Edição</span>
            </div>
        </div>
        <div ng-class="(ticket.etapas[3] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[3] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <div class="dot"></div>
            </div>
        </div>
        <div ng-class="(ticket.etapas[4] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[4] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-eye"></i>
                <span class="event-txt">Revisão</span>
            </div>
        </div>
        <div ng-class="(ticket.etapas[5] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[5] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <div class="dot"></div>
            </div>
        </div>
        <div ng-class="(ticket.etapas[6] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <!-- V1 - Opcional -->
        <div ng-show="ticket.perfis[2]" ng-class="(ticket.etapas[6] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-thumbs-up"></i>
                <span class="event-txt">Primeira <br /> Aprovação</span>
            </div>
        </div>
        <div ng-show="ticket.perfis[2]" ng-class="(ticket.etapas[7] === 1) ? 'event-box is-done' : 'event-box'" style="flex: 1;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <!-- END V1 - Opcional -->
        <div ng-class="(ticket.etapas[7] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-gavel"></i>
                <span class="event-txt">Demais <br /> Aprovações</span>
            </div>
        </div>
        <div ng-class="(ticket.etapas[8] === 1) ? 'event-box is-done' : 'event-box'" style="flex: 1;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[8] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-paint-brush"></i>
                <span class="event-txt">Finalização</span>
            </div>
        </div>
        <div ng-class="(ticket.etapas[9] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[9] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <div class="dot"></div>
            </div>
        </div>
        <div ng-class="(ticket.etapas[10] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.44;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <!-- V1 - Opcional -->
        <div ng-show="ticket.perfis[6]" ng-class="(ticket.etapas[10] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-paper-plane"></i>
                <span class="event-txt">Envio da <br /> Arte</span>
            </div>
        </div>
        <div ng-show="ticket.perfis[6]" ng-class="(ticket.etapas[11] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <!-- END V1 - Opcional -->
        <div ng-show="ticket.perfis[6]" ng-class="(ticket.etapas[11] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event"> 
                <div class="dot"></div>
            </div>
        </div>
        <div ng-show="ticket.perfis[6]" ng-class="(ticket.etapas[12] === 1) ? 'event-box is-done' : 'event-box'" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="(ticket.etapas[13] === 1) ? 'event-box current' : 'event-box'" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-flag-checkered"></i>
                <span class="event-txt">Encerrado</span>
            </div>
        </div>
    </div>
    <!-- END Timeline -->
    
    <!-- Form Criar Ticket -->
    {!! Form::open(array('name' => 'formAltRequest', 'id' => 'formAltRequest', 'class' => 'artw-ticket-create', 'files' => 'true', 'ng-submit' => 'validar($event)')) !!}
        <section class="margin-top-30">
            @if ($ticket->id_status >= 0)
                <div class="block">
                    <h4 class="master-title-alt txt-bold margin-bottom-20">Informações</h4>
                    <div class="flexbox-container">
                        <div class="artw-initial-info-col flex flex-fluid">
                            <label class="master-title-sub label-control txt-small txt-uppercase">Resumo</label>
                            <textarea @if ($ticket->id_status > 2 && $ticket->id_status < 9) disabled @endif name="txtMainDescription" placeholder="Descrição geral" rows="6" id="txtMainDescription" class="input-control textbox-control">{{ $ticket->descricao }}</textarea>
                        </div>
                        <div class="artw-additional-info-col flex flex-auto">
                            <div class="flexbox-container flexbox-group margin-bottom-40">
                                <div class="flex flex-fluid">	
                                    <label class="master-title-sub label-control txt-small txt-uppercase">Projeto</label>
                                    <selectize ng-init="cbProjeto = {{ $ticket->id_projeto }}" placeholder="Escolha o projeto (opcional)" name="cbProjeto" id="cbProjeto" config="projetoConfig" options="projetos" ng-model="cbProjeto" required="true"></selectize>
								    <input type="hidden" ng-model="cbProjeto" name="id_projeto" ng-value="cbProjeto">
                                </div>
                                <div ng-show="cbProjeto === '0'" class="flex flex-fluid">	
                                    <label class="master-title-sub label-control txt-small txt-uppercase">Nome do Projeto</label>
                                    <input type="text" placeholder="Digite o nome do projeto" name="tfNomeProjeto" id="tfNomeProjeto" class="input-control" />	
                                </div>
                                <div class="flex artw-project-name-col">	
                                    <label class="master-title-sub label-control txt-small txt-uppercase">Código</label>
                                    <input value="{{ $ticket->codigo_cliente }}" type="text" maxlength="25" placeholder="Código (opcional)" name="codigo_cliente" id="tfCodigoTicket" class="input-control only-alpha-numeric" />
                                </div>
                            </div>
                            <div class="flexbox-container flexbox-group">
                                <div class="artw-start-date-col flex flex-fluid">
                                    <label class="master-title-sub label-control txt-small txt-uppercase">Início</label>
                                    <div class="date-control readonly">	
                                        <input disabled autocomplete="off" value="{{ $ticket->dt_ini->format('d/m/Y') }}" type="text" name="tfStartDate" placeholder="dd/mm/aaaa" id="tfStartDate" class="input-control valid-tdate" />
                                        <span class="addon fa fa-calendar"></span>
                                    </div>
                                </div>
                                <div class="artw-approval-date-col flex flex-fluid">
                                    <label class="master-title-sub label-control txt-small txt-uppercase">Aprovação</label>
                                    <div class="date-control">	
                                        <input onkeydown="return false;" autocomplete="off" @if ($ticket->id_status > 2 && $ticket->id_status < 9) disabled @endif value="{{ $ticket->dt_envio->format('d/m/Y') }}" type="text" name="tfArtSendDate" placeholder="dd/mm/aaaa" id="tfArtSendDate" class="date-input-readonly input-control" />
                                        <span class="addon fa fa-calendar"></span>
                                    </div>
                                </div>
                                <div class="artw-closure-date-col flex flex-fluid">
                                    <label class="master-title-sub label-control txt-small txt-uppercase">Encerramento</label>
                                    <div class="date-control">	
                                        <input onkeydown="return false;" autocomplete="off" value="{{ $ticket->dt_fim->format('d/m/Y') }}" type="text" name="tfDeadlineDate" placeholder="dd/mm/aaaa" id="tfDeadlineDate" class="date-input-readonly input-control" />
                                        <span class="addon fa fa-calendar"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($ticket->id_status <= 2 || $ticket->id_status == 9)
                    <div ng-cloak class="block">
                        <h4 class="master-title-alt txt-bold margin-bottom-20">Incluir Instrução</h4>
                        <div class="block margin-left-10 margin-right-10 margin-bottom-20">
                            <p >
                                Os envolvidos no ticket também receberão uma notificação por e-mail sobre a inclusão de novas instruções. 
                            </p>
                        </div>
                        <div id="alterationTypeWrap" class="input-fields-wrapper request-type">
                            <div ng-repeat="(k, solicitacao) in solicitacoes" class="input-fields-group flexbox-container margin-bottom-20">
                                <div class="artw-instructions-col flex flex-auto">
                                    <div class="flexbox-container">
                                        <div class="flex-12-large flex-auto margin-bottom-10 clearfix">
                                            <div ng-class="{ 'disabled': solicitacao.id }" class="select-control">
                                                <select ng-disabled="solicitacao.id">
                                                    <option value="">Escolha o tipo de alteração</option>
                                                    <option ng-selected="solicitacao.id_tipo == tipo.id" ng-repeat="(k, tipo) in tipos" value="@{{ tipo.id }}">@{{ tipo.nome }}</option>
                                                </select>
                                                <input ng-disabled="!solicitacao.id" ng-value='solicitacao.id_tipo' type='hidden'>
                                                <input ng-disabled="solicitacao.id" ng-value='solicitacao.id_tipo' type='hidden' name='cbAlterationType[]'>
                                            </div>
                                        </div>
                                        <div class="flex-12-large flex-auto no-margin-bottom clearfix">
                                            <textarea auto-height ng-readonly="solicitacao.id" ng-model="solicitacao.descricao" placeholder="Descrição específica" rows="4" class="input-control textbox-control textbox-auto-resizable">@{{ solicitacao.descricao_texto }}</textarea>
                                            <input ng-disabled="!solicitacao.id" ng-value='solicitacao.descricao' type='hidden'>
                                            <input ng-disabled="solicitacao.id" ng-value='solicitacao.descricao' type='hidden' name='txtDescriptionSpec[]'>
                                        </div>
                                    </div>
                                </div>
                                <div class="remove-instruction-col flex flex-auto">
                                    <button ng-show="solicitacao.id" ng-click="remover(k)" disabled class="btn-control btn-alt no-margin">Excluir</button>
                                    <button ng-show="!solicitacao.id" ng-click="remover(k)" class="btn-control btn-alt no-margin">Excluir</button>
                                </div>
                            </div>
                            <div class="add-instruction-group input-fields-group flexbox-container margin-bottom-10">
                                <div class="artw-instructions-col flex flex-auto">
                                    <div class="flexbox-container">
                                        <div class="flex-12-large flex-auto margin-bottom-10 clearfix">
                                            <div class="select-control">	
                                                <select ng-model='add_tipo'>
                                                    <option value="">Escolha o tipo de alteração</option>
                                                    <option ng-repeat="(k, tipo) in tipos" value="@{{ tipo.id }}">@{{ tipo.nome }}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="flex-12-large flex-auto no-margin-bottom clearfix">
                                            <textarea auto-height ng-model='add_descricao' placeholder="Descrição específica" rows="3" class="input-control textbox-control textbox-auto-resizable"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="add-instruction-col flex flex-auto">
                                    <button ng-click="adicionar()" type="button" class="btn-control btn-positive">Adicionar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endif
        </section>
        <!-- Button group -->
        <div class="btn-group txt-center margin-top-30">
            <div class="bs border-color-primary padding-top-40">
                <a href="{{ route('site.artwork.ticket.edit.step1.get', [$ticket->numero]) }}" class="btn-control btn-call-to-action call-to-action-negative btn-icon btn-icon-left">
                    <span><i class="icon fa fa-times txt-light"></i> Cancelar</span> 
                </a>
                <button type="submit" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                    <span><i class="icon fa fa-check txt-light"></i> Salvar</span>
                </button>
            </div>
        </div>
        <!-- END Button group -->
    {!! Form::close() !!}
    <!-- END Form Criar Ticket -->
</div>
@stop
