@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('artwork.versoes_anteriores', $versao->id) !!}

<script type="text/javascript" src="{{ asset('/js/site/artwork/ticket-edit.js') }}"></script>
<div class="content clearfix">
	<div class="top-head">
		<div class="top-inline top-packing-logo">
			<a href="javascript:;" class="logo-down"><span class="logo-down-ico">Baixar Logo</span></a>
			{{ Html::image(asset($thumb), 'Logo', array('id' => 'imgLogo')) }}
		</div>
		<div class="top-inline top-head-title top-head-title-center">
        	<h2><span>{{ $sku->nome }} {{ $variacao->principal !== 1 ? " - " . $variacao->nome : "" }} {{ $variacao->campanha ? " - " . $variacao->campanha->nome : "" }}</span></h2>
		</div>
		<div class="top-inline top-nav-right">
			<ul>
				<li><a href="{{ route('site.artwork.history', [$versao]) }}"><span class="icon icon-compass-2"></span>Histórico</a></li>
			</ul>
		</div>
	</div>
    <div class="table-tickets-container table-hover">
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th width="590">Versões Anteriores</th>
                    <th width="180">Ticket Gerador</th>
                    <th width="100">Aprovação</th>
                </tr>
            </thead>
            <tbody>
				@foreach ($versoes as $versao)
	                <tr>
	                    <td width="590"><a href="{{ route('site.artwork.history', [$versao->id]) }}"><span class="icon-bag"></span><span class="ticket-name-txt">{{ str_replace('PDF ', '', $versao->nome) }}</span></a></td>
	                    <td width="180">{{ $versao->artwTicket->numero }}</td>
	                    <td width="100">{{ $versao->created_at->format('d/m/Y') }}</td>
	                </tr>
				@endforeach
            </tbody>
        </table>
    </div>
</div>
@stop
