@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
@if (URL::previous() == route('site.artwork.ticket.edit.get', ['variacao' => $variacao->id, 'ticket' => $ticket->id]))
    {!! Breadcrumbs::renderIfExists('artwork.ticket_editar', $ticket->id) !!}
@else
    {!! Breadcrumbs::renderIfExists('artwork.solicitar_alteracao', $variacao->id) !!}
@endif
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />
<script>
    var id_ticket = {{$ticket->id}};
    var id_usuario = {{Auth::user()->id}};
    var csrf_token = '{{ csrf_token() }}';

    @if (URL::previous() != route('site.artwork.manager'))
        jQuery(document).ready(function($) {
            if (window.history && window.history.pushState) {
                $(window).on('popstate', function() {
                    var hashLocation = location.hash;
                    var hashSplit = hashLocation.split("#!/");
                    var hashName = hashSplit[1];

                    if (hashName !== '') {
                        var hash = window.location.hash;
                        
                        if (hash === '') {
                            location.href='{{ route('site.artwork.ticket.edit.get', ['variacao' => $ticket->artwVariacao->id, 'ticket' => $ticket->id]) }}'
                        }
                    }
                });

                window.history.pushState('forward', null, '');
            }
        });
    @endif
</script>
<script>
    $(function () {
        $('#formMembershipInclude').submit(function () {
            if($('.team-member-group').is(':visible') && $('.selectize-input input[type="text"]').val() === '') {
                $('.team-member-group selectize[name="cbMembershipTeamName[]"] + .selectize-control .selectize-input').addClass('has-error');
                myAlert('Selecione um membro da equipe.', 'warning');
                return false;
            }
            
            var error = 0;
            
            $('select[name^="cbMembershipProfile"]').each(function() {
                if ($(this).val() == '') {
                    $(this).focus();
                    $(this).addClass('has-error');
                    error++;
                }
            });
            
            if (error) {
                myAlert('Selecione o seu Perfil de Participação neste ticket.', 'warning');
                return false;
            }
        });
    });
</script>
<script src="{{ asset('/js/angular/angular.min.js')}}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/modal_grupo_trabalho.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/embalagens_modal_compartilhar.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_members.js') }}"></script>
<div class="content content-fluid" ng-app="app" ng-controller="TicketMembersCtrl">
    @if (count($errors) > 0)
        <div class="alert alert-danger" role="alert">
            <div class="alert-error">
                @foreach ($errors->all() as $error)
                    {{ $error }}<br />
                @endforeach
            </div>
        </div>
    @endif
    
    @include('site.artwork.ticket_topo_inc')
    
    <!-- Timeline -->
    <div class="timeline" ng-cloak>
        <div ng-class="{'idle': !cbMembershipProfile[0]}" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-flag"></i>
                <span class="event-txt">Aberto</span>
            </div>
        </div>
        <div ng-class="{'idle': !cbMembershipProfile[0]}" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="{'idle': !(cbMembershipProfile|filter:'5').length > 0 }" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-paint-brush"></i>
                <span class="event-txt">Edição</span>
            </div>
        </div>
        <div ng-class="{'idle': !(cbMembershipProfile|filter:'5').length > 0 }" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="{'idle': !(cbMembershipProfile|filter:'5').length > 0 }" style="flex: unset;" class="event-box">
            <div class="event"> 
                <div class="dot"></div>
            </div>
        </div>
        <div ng-class="{'idle': !(cbMembershipProfile|filter:'5').length > 0 }" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="{'idle': !(cbMembershipProfile|filter:'5').length > 0 }" style="flex: unset;" class="event-box">
            <div class="event idle"> 
                <div class="dot"></div>
            </div>
        </div>
        <div ng-class="{'idle': !(cbMembershipProfile|filter:'5').length > 0 }" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="{'idle': !(cbMembershipProfile|filter:'3').length > 0 }" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-eye"></i>
                <span class="event-txt">Revisão</span>
            </div>
        </div>
        <div ng-class="{'idle': !(cbMembershipProfile|filter:'3').length > 0 }" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="{'idle': !(cbMembershipProfile|filter:'3').length > 0 }" style="flex: unset;" class="event-box">
            <div class="event"> 
                <div class="dot"></div>
            </div>
        </div>
        <div ng-class="{'idle': !(cbMembershipProfile|filter:'3').length > 0 }" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <!-- V1 - Opcional -->
        <div ng-if="(cbMembershipProfile|filter:'2').length" ng-class="{'idle': !(cbMembershipProfile|filter:'2').length > 0 }" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-thumbs-up"></i>
                <span class="event-txt">Primeira <br /> Aprovação</span>
            </div>
        </div>
        <div ng-if="(cbMembershipProfile|filter:'2').length" ng-class="{'idle': !(cbMembershipProfile|filter:'2').length > 0 }" style="flex: 1;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <!-- END V1 - Opcional -->
        <div ng-class="{'idle': !(cbMembershipProfile|filter:'1').length > 0 }" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-gavel"></i>
                <span class="event-txt">Demais <br /> Aprovações</span>
            </div>
        </div>
        <div ng-class="{'idle': !(cbMembershipProfile|filter:'1').length > 0 }" style="flex: 1;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="{'idle': !(cbMembershipProfile|filter:'3').length > 0 || !(cbMembershipProfile|filter:'5').length > 0 }" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-paint-brush"></i>
                <span class="event-txt">Finalização</span>
            </div>
        </div>
        <div ng-class="{'idle': !(cbMembershipProfile|filter:'3').length > 0 || !(cbMembershipProfile|filter:'5').length > 0 }" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="{'idle': !(cbMembershipProfile|filter:'3').length > 0 || !(cbMembershipProfile|filter:'5').length > 0 }" style="flex: unset;" class="event-box">
            <div class="event"> 
                <div class="dot"></div>
            </div>
        </div>
        <div ng-class="{'idle': !(cbMembershipProfile|filter:'3').length > 0 || !(cbMembershipProfile|filter:'5').length > 0 }" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        
        <!-- V1 - Opcional -->
        <div ng-if="(cbMembershipProfile|filter:'6').length" ng-class="{'idle': !(cbMembershipProfile|filter:'5').length > 0 }" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-paper-plane"></i>
                <span class="event-txt">Envio da <br /> Arte</span>
            </div>
        </div>
        <div ng-if="(cbMembershipProfile|filter:'6').length" ng-class="{'idle': !(cbMembershipProfile|filter:'5').length > 0 }" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-if="(cbMembershipProfile|filter:'6').length" ng-class="{'idle': !(cbMembershipProfile|filter:'5').length > 0 }" style="flex: unset;" class="event-box">
            <div class="event"> 
                <div class="dot"></div>
            </div>
        </div>
        <!-- END V1 - Opcional -->
        
        <!-- V1 - Opcional -->
        <div ng-if="(cbMembershipProfile|filter:'6').length" ng-class="{'idle': !(cbMembershipProfile|filter:'5').length > 0 }" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-if="(cbMembershipProfile|filter:'7').length" ng-class="{'idle': !(cbMembershipProfile|filter:'7').length > 0 }" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-industry"></i>
                <span class="event-txt">PDF do <br /> Fornecedor</span>
            </div>
        </div>
        <div ng-if="(cbMembershipProfile|filter:'7').length" ng-class="{'idle': !(cbMembershipProfile|filter:'7').length > 0 }" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-if="(cbMembershipProfile|filter:'7').length" ng-class="{'idle': !(cbMembershipProfile|filter:'7').length > 0 }" style="flex: unset;" class="event-box">
            <div class="event"> 
                <div class="dot"></div>
            </div>
        </div>
        <!-- END V1 - Opcional -->
        
        <div ng-if="(cbMembershipProfile|filter:'6').length || (cbMembershipProfile|filter:'7').length" ng-class="{'idle': !(cbMembershipProfile|filter:'5').length > 0 }" style="flex:0.445;" class="event-box">
            <div class="filling-bar"></div>
        </div>
        <div ng-class="{'idle': !(cbMembershipProfile|filter:'5').length > 0 || !(cbMembershipProfile|filter:'3').length > 0 }" style="flex: unset;" class="event-box">
            <div class="event">
                <i class="event-icon fa fa-flag-checkered"></i>
                <span class="event-txt">Encerrado</span>
            </div>
        </div>
    </div>
    <!-- END Timeline -->
    <form name="formMembershipInclude" action="" method="POST" id="formMembershipInclude">
        <input type="hidden" name="_token" value="{{ csrf_token()}}">
        @if (URL::previous() == route('site.artwork.ticket.edit.get', ['variacao' => $variacao->id, 'ticket' => $ticket->id]))
            <input type="hidden" name="edicao" value="1">
        @endif
        <section class="bs border-color-primary margin-bottom-40 padding-bottom-30">
            <h4 class="master-title-alt txt-bold margin-bottom-20">Participantes</h4>
            <div class="block no-margin-top margin-bottom-20">
                <div class="flexbox-container">
                    <div ng-controller="grupoTrabalhoCtrl" class="flex margin-left-auto">
                        <a href="javascript:;" ng-click="criar_grupo_trabalho()" class="btn-function"><i class="icon fa fa-handshake-o"></i>Grupos de Trabalho</a>
                    </div>
                </div>
            </div>
            <div ng-cloak class="block">
                <table cellpading="0" cellspacing="0" class="workflow-membership-table table-border table-hover">
                    <thead>
                        <tr>
                            <th width=""><span class="txt-small txt-uppercase">Nome</span></th>
                            <th width="300"><span class="txt-small txt-uppercase">Empresa / Departamento</span></th>
                            <th width="240"><span class="txt-small txt-uppercase">Função</span></th>
                            <td width="50px"></td>
                            <th width="70px"><span></span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Table-group -->
                        <tr ng-repeat="(k, participante) in $root.participantes" class="table-group no-animate">
                            <td colspan="5">
                                <table cellpading="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td width="">
                                                <div class="d-flex flex-align-center">
                                                    <i ng-class="{ 'fa-users has-teamwork': ( (participante.equipe_membros.length > 0 && cbMembershipProfile[k] == 1) || (participante.equipe_membros.length > 0 && cbMembershipProfile[k] == 2) || (ticket.id_perfil == 1 || ticket.id_perfil == 2) ), 'fa-user': (cbMembershipProfile[k] != 1 && cbMembershipProfile[k] != 2) || (ticket.id_perfil != 1 && ticket.id_perfil != 2) || participante.equipe_membros.length == 0, 'txt-color-primary': ticket.id_usuario == participante.id_usuario }" class="icon fa"></i>
                                                    <div class="text-side fill-avaliable">
                                                        <div ng-if="!participante.id || participante.id_usuario !== participante.id_usuario_logado || k === 0" ng-class="{'no-style-input': !participante.id || participante.id_usuario !== participante.id_usuario_logado || k === 0}" class="select-control">
                                                            <select ng-disabled="(participante.id_usuario !== participante.id_usuario_logado) || (k === 0)" name="cbMembershipName[]" class="txt-semibold no-padding">
                                                                <option value="" selected>Selecionar participante</option>
                                                                <option ng-if="participante.id_usuario == membro.id" ng-selected="participante.id_usuario == membro.id" ng-repeat="(k, membro) in membros.membersAll" value="@{{membro.id}}">@{{membro.nome}}</option>
                                                            </select>
                                                            <select ng-if="participante.id && participante.id_usuario === participante.id_usuario_logado && k > 0" name="cbMembershipAltName[@{{k}}]">
                                                                <option value="" selected>Selecionar participante</option>
                                                                <option ng-if="participante.id_usuario == membro.id" ng-selected="participante.id_usuario == membro.id" ng-repeat="(k, membro) in membros.membersAll" value="@{{membro.id}}">@{{membro.nome}}</option>
                                                            </select>
                                                            <input ng-disabled="participante.id_usuario === participante.id_usuario_logado && k > 0" name="cbMembershipName[]" type="hidden" ng-value="participante.id_usuario">
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td width="300">
                                                <div ng-if="membros.membersAll">    
                                                    <span ng-repeat="(k, membro) in membros.membersAll" ng-if="participante.id_usuario == membro.id">@{{membro.empresa}} <strong ng-if="membro.departamento">/ @{{membro.departamento}}</strong> <strong ng-if="!membro.departamento">/ <i class="txt-color-alt no-italic">Departamento</i></strong></span>
                                                </div>
                                            </td>
                                            <td width="240">
                                                <div ng-class="{'no-style-input': (participante.id) || (!participante.id && k > 0)}" class="select-control">
                                                    <select ng-change="selecionar_primeiro(participante.id_usuario, cbMembershipProfile[k], participante.id_perfil.toString())" ng-disabled="(participante.id) || (!participante.id && k > 0)" ng-model="cbMembershipProfile[k]" ng-init='cbMembershipProfile[k] = participante.id_perfil.toString()' name="cbMembershipProfile[@{{k}}]" ng-class="{'no-padding': (participante.id) || (!participante.id && k > 0)}" class="members-profile-validate">
                                                        <option value="" selected>Perfil</option>
                                                        <option ng-selected="participante.id_perfil == 5" value="5">Edição e Finalização</option>
                                                        <option ng-selected="participante.id_perfil == 3" value="3">Revisão</option>
                                                        <option ng-selected="participante.id_perfil == 2" value="2">Primeira Aprovação</option>
                                                        <option ng-selected="participante.id_perfil == 1" value="1">Demais Aprovações</option>
                                                        <option ng-selected="participante.id_perfil == 6" value="6">Envio da Arte</option>
                                                        <option ng-selected="participante.id_perfil == 7" value="7">PDF do Fornecedor</option>
                                                        <option ng-selected="participante.id_perfil == 4" value="4">Participante</option>
                                                    </select>
                                                    <input ng-model="cbMembershipProfile[k]" name="cbMembershipProfile[@{{k}}]" type="hidden" ng-value="cbMembershipProfile[k]">
                                                </div>
                                            </td>
                                            <td width="50px" class="txt-center">
                                                <div ng-if="cbMembershipProfile[k] == 1 || cbMembershipProfile[k] == 2" class="add-team-btn">
                                                    <div ng-click="adicionar_equipe_toggle(participante, participantes, $index)">    
                                                        <a tooltips tooltip-append-to-body="false" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-template="Adicionar Equipe" ng-class="{'btn-function active': participante.adiciona_equipe && !membro_selecionado, 'btn-function': participante.adiciona_equipe && membro_selecionado || !participante.adiciona_equipe }" href="javascript:;" class="btn-function"><i class="icon fa fa-users no-float no-margin"></i></a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td width="70px" class="txt-right">
                                                <div ng-if="!participante.id && k > 0" class="dropdown drop-to-left no-margin">
                                                    <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                                    <div class="dropdown-list">
                                                        <ul>    
                                                            <li><a ng-click="remover(k)" href="javascript:;" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr ng-if="cbMembershipProfile[k] == 1 || cbMembershipProfile[k] == 2" ng-repeat="(e, equipe) in participante.equipe_membros" class="table-sub">
                                            <td colspan="5">
                                                <div class="table-sub-content">
                                                    <table cellpading="0" cellspacing="0" class="no-margin-bottom">
                                                        <tbody>
                                                            <!-- Table sub-group -->
                                                            <tr>
                                                                <td width="">
                                                                    <div class="text-side no-margin">
                                                                        <div class="select-control no-style-input">
                                                                            <select name="cbMembershipTeamName[]" disabled class="txt-semibold no-padding">
                                                                                <option value="" selected>Membros da equipe</option>
                                                                                <option ng-selected="membro.id == equipe.id_usuario" ng-repeat="(k, membro) in membros.membersAll" value="@{{membro.id}}">@{{membro.nome}}</option>
                                                                            </select>
                                                                            <input name="cbMembershipTeamName[@{{k}}][]" type="hidden" ng-value="equipe.id_usuario">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td width="300">
                                                                    <span ng-repeat="(k, membro) in membros.membersAll" ng-if="membro.id == equipe.id_usuario">@{{membro.empresa}} <strong ng-if="membro.departamento">/ @{{membro.departamento}}</strong> <strong ng-if="!membro.departamento">/ <i class="txt-color-alt no-italic">Departamento</i></strong></span>
                                                                </td>
                                                                <td width="240">
                                                                    <div class="select-control no-style-input column-membership-profile">
                                                                        <select name="cbMembershipTeamProfile[]" ng-model="cbMembershipProfile[k]" disabled class="no-padding">
                                                                            <option value="">Perfil</option>
                                                                            <option value="5">Edição e Finalização</option>
                                                                            <option value="3">Revisão</option>
                                                                            <option value="2">Primeira Aprovação</option>
                                                                            <option value="1">Demais Aprovações</option>
                                                                            <option value="6">Envio da Arte</option>
                                                                            <option value="7">PDF do Fornecedor</option>
                                                                            <option value="4">Participante</option>
                                                                        </select>
                                                                    </div>
                                                                </td>
                                                                <td width="50px" class="txt-center"></td>
                                                                <td width="70px" class="txt-right">
                                                                    <div ng-if="!equipe.id" class="dropdown drop-to-left no-margin">
                                                                        <a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-toggle fa fa-ellipsis-h"></a>
                                                                        <div class="dropdown-list">
                                                                            <ul>    
                                                                                <li><a ng-click="remover_membro(k, e)" href="javascript:;" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <!-- END Table sub-group -->
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- Adiconar Equipe -->
                                        <tr ng-if="participante.adiciona_equipe" class="table-sub">
                                            <td colspan="5">
                                                <div class="table-sub-content">
                                                    <table cellpading="0" cellspacing="0" class="no-margin-bottom">
                                                        <tbody>
                                                            <!-- Table sub-group -->
                                                            <tr>
                                                                <td colspan="5">
                                                                    <div class="membership-team-col flexbox-container flexbox-group artw-member-edit-sub">
                                                                        <div class="flex col-select-member">
                                                                            <selectize config="myConfig" options='membros.members' name="cbMembershipTeamName[]" ng-model="participante.add_participante"></selectize>
                                                                        </div>
                                                                        <div class="flexbox-container flexbox-group flex col-btn-group">
                                                                            <div class="flex col-member-edit-cancel">
                                                                                <button ng-click="participante.adiciona_equipe = participante.adiciona_equipe ? false : true" type="button" class="btn-control btn-alt">Cancelar</button>
                                                                            </div>
                                                                            <div class="flex col-member-edit-save">
                                                                                <button ng-click="adicionar_membro(participante, k)" type="button" class="btn-control btn-positive">Adicionar</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <!-- END Table sub-group -->
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- END Adicionar Equipe -->
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <!-- Table-group -->
                    </tbody>
                </table>
            </div>

            <!-- Adicionar Participante -->
            <div class="artw-members-group block">
                <div class="input-fields-group flexbox-container">
                    <div class="col-select-member flexbox-column flex">
                        <selectize config="myConfig" options='membros.members' name="cbMembershipTeamName[]" ng-model="add_participante"></selectize>
                    </div>
                    <div class="col-select-position flexbox-column flex">
                        <div class="select-control">
                            <select ng-model="add_perfil" class="members-profile-validate">
                                <option value="" selected>Selecione a função</option>
                                <option value="5">Edição e Finalização</option>
                                <option value="3">Revisão</option>
                                <option value="2">Primeira Aprovação</option>
                                <option value="1">Demais Aprovações</option>
                                <option value="6">Envio da Arte</option>
                                <option value="7">PDF do Fornecedor</option>
                                <option value="4">Participante</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-add-member flexbox-column flex">
                        <button ng-click="adicionar()" type="button" class="btn-control btn-positive">Adicionar</button>
                    </div>
                </div>
            </div>
            <!-- END Adicionar Participante -->

        </section>
        <div class="btn-group txt-center">
            @if (URL::previous() != route('site.artwork.manager'))
                <button onclick="window.location.href='{{ route('site.artwork.ticket.edit.get', ['ticket' => $ticket->id, 'variacao' => $ticket->artwVariacao->id]) }}'" type="button" class="btn-control btn-call-to-action call-to-action-negative btn-icon btn-icon-left">
                    <span><i class="icon fa fa-arrow-left"></i> Etapa Anterior</span>
                </button>
            @endif
            <button type="submit" id="sendBtn" ng-disabled="!cbMembershipProfile[0]" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-right">
                <span><i class="icon fa fa-arrow-right"></i> Confirmar e Enviar</span>
            </button>
        </div>
    </form>
    <!-- END Participantes -->
</div>
@stop
