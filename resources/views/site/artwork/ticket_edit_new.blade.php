@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
{!! Breadcrumbs::renderIfExists('artwork.ticket_editar', $ticket->id) !!}
<script>
	var id_ticket = {{$ticket->id}};
	var id_usuario = {{Auth::user()->id}};
    var csrf_token = '{{ csrf_token() }}';
	var edicao = true;
</script>
<script>
    $(function () {
        if ($("#tfStartDate").val()){
            var tfStartDate = $("#tfStartDate").val().split("/");
            var minDate = new Date(tfStartDate[2], tfStartDate[1] - 1, tfStartDate[0]);
            minDate.setDate(minDate.getDate() + 1);
            var minDate2 = new Date(tfStartDate[2], tfStartDate[1] - 1, tfStartDate[0]);
            minDate2.setDate(minDate2.getDate() + 2);

            var dateFormat = "dd/mm/yy",
                from = $("#tfArtSendDate")
            
            .datepicker({
                //defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                minDate: minDate
            })
            .on("change", function () {
                to.datepicker("option", "minDate", getDate(this, 1));
            }),
            to = $("#tfDeadlineDate").datepicker({
                //defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                minDate: minDate2
            })
            .on("change", function () {
                from.datepicker("option", "maxDate", getDate(this, -1));
            });
        }

        function getDate(element, qtd) {
            var date;
            try {
                var date2 = $.datepicker.parseDate(dateFormat, element.value);
                date2.setDate(date2.getDate() + qtd);
                date = date2;
            } catch (error) {
                date = null;
            }
            return date;
        }
    });
</script>
<script>
    $(function () {
        $('#formAltRequest').submit(function () {
            if($('.team-member-group').is(':visible') && $('.selectize-input input[type="text"]').val() === '') {
                $('.team-member-group selectize[name="cbMembershipTeamName[]"] + .selectize-control .selectize-input').addClass('input-error');
                myAlert('Selecione um membro da equipe.', 'warning');
                return false;
            }
        });
    });
</script>
<script src="{{ asset('/js/site/jquery-ui.min.js')}}"></script>
<script src="{{ asset('/js/site/ui.datepicker-pt-BR.js')}}"></script>
<script src="{{ asset('/js/site/artwork/package-alteration-request.js') }}"></script>
<script src="{{ asset('/js/angular/angular.min.js')}}"></script>
<script src="{{ asset('/js/angular/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.min.js')}}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js')}}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edit_new.js')}}"></script>
<div class="content" ng-app="app" ng-controller="TicketEditCtrl">
    @if (count($errors) > 0)
    <div class="alert alert-danger" role="alert">
        <div class="alert-error">
            @foreach ($errors->all() as $error)
            {{ $error}}<br />
            @endforeach
        </div>
    </div>
    @endif
    <div ng-cloak class="top-head">
        <div class="top-inline top-packing-logo">
            {{ Html::image(asset($thumb), 'Logo', array('id' => 'imgLogo')) }}
        </div>
		<div class="top-inline top-head-title top-head-title-center top-head-title-sku-name padding-left-30 padding-right-30">
            <h2 class="text-center"><span>{{ $ticket->sku_nome_completo }}</span></h2>
        </div>
        <div class="top-inline top-nav-right">
            <ul>
				<li><strong class="ticket-info"><span class="icon icon-options-settings"></span> Ticket {{ $ticket->numero }}</strong></li>
                <li><strong class="ticket-info"><span class="icon icon-arrow-curve-recycle"></span> Ciclo @{{ticket.ciclo_atual.numero}}</strong></li>
			</ul>
        </div>
    </div>
    <div class="ticket-timeline">
		<ul class="clearfix">
			<li ng-class="{'current': ticket.id_status >= '0' && ticket.id_status < '9', 'in-progress': ticket.id_status > '0' && ticket.id_status < '9'}">Em Aberto <span class="icon-quote"></span></li>
			<li ng-class="{'current': ticket.id_status >= '1' && ticket.id_status < '9', 'in-progress': ticket.id_status > '1' && ticket.id_status < '9'}">Em Edição <span class="icon-three-points"></span></li>
			<li ng-class="{'current': ticket.id_status >= '2' && ticket.id_status < '9', 'in-progress': ticket.id_status > '2' && ticket.id_status < '9'}">Em Revisão <span class="icon-list-square"></span></li>
			<li ng-show='ticket.count_marketing > 0' ng-class="{'current': ticket.id_status >= '3' && ticket.id_status < '9', 'in-progress': ticket.id_status > '3' && ticket.id_status < '9'}">Marketing <span class="icon-cube"></span></li>
			<li ng-class="{'current': ticket.id_status >= '4' && ticket.id_status < '9', 'in-progress': ticket.id_status > '4' && ticket.id_status < '9'}">Em Aprovação <span class="icon-text-justify-center"></span></li>
			<li ng-class="{'current': ticket.id_status >= '5' && ticket.id_status < '9', 'in-progress': ticket.id_status > '5' && ticket.id_status < '9'}">Aprovado <span class="icon-bag"></span></li>
			<li ng-class="{'current': ticket.id_status >= '6' && ticket.id_status < '9', 'in-progress': ticket.id_status > '6' && ticket.id_status < '9'}">Enviado <span class="icon-arrow-oblique-expand-directions"></span></li>
		</ul>
	</div>
    {!! Form::open(array('name' => 'formAltRequest', 'id' => 'formAltRequest', 'class' => 'ticket-alt-request-form', 'files' => 'true')) !!}
    @if ($ticket->id_status > 0)
    <hr />
    <fieldset>
        <div class="clearfix">
            <div class="column-initial-info form-inline">
                <label>Informações iniciais</label>
                <div>
                    <textarea @if ($ticket->id_status > 2) disabled @endif name="txtMainDescription" placeholder="Descrição geral" rows="3" id="txtMainDescription" class="input-control">{{ $ticket->descricao }}</textarea>
                </div>
            </div>
            <div class="form-inline">
                <div class="column-date-info clearfix">
                    <div class="form-inline column-start-date">
                        <label>Data de início</label>
                        <div>
                            <input disabled autocomplete="off" value="{{ $ticket->dt_ini->format('d/m/Y') }}" type="text" name="tfStartDate" placeholder="dd/mm/aaaa" id="tfStartDate" class="input-control valid-tdate" />
                        </div>
                    </div>
                    <div class="form-inline column-send-date">
                        <label>Envio para Aprovação</label>
                        <div>
                            <input autocomplete="off" @if ($ticket->id_status > 2) disabled @endif value="{{ $ticket->dt_envio->format('d/m/Y') }}" type="text" name="tfArtSendDate" placeholder="dd/mm/aaaa" id="tfArtSendDate" class="date-input-readonly input-control" />
                        </div>
                    </div>
                    <div class="form-inline column-deadline-date">
                        <label>Entrega no Fornecedor</label>
                        <div>
                            <input autocomplete="off" value="{{ $ticket->dt_fim->format('d/m/Y') }}" type="text" name="tfDeadlineDate" placeholder="dd/mm/aaaa" id="tfDeadlineDate" class="date-input-readonly input-control" />
                        </div>
                    </div>
                </div>
                <!--
                <div class="btn-control btn-cancel-package clearfix">
                    <button type="button" id="btnCancelPackage"><span class="icon-ban-circle"></span> Descontinuar embalagem</button>
                </div>
                -->
            </div>
        </div>
    </fieldset>
    @if ($ticket->id_status <= 2)
    <fieldset>
        <hr />
        <div ng-cloak class="form-inline">
            <label>Alterações</label>
            <div id="alterationTypeWrap" class="input-fields-wrapper request-type">
                <div ng-repeat="(k, solicitacao) in solicitacoes" class="input-fields-group clearfix request-type-children">
                    <div class="form-inline column-edition-type">
                        <div class="select-control">
                            <select disabled="disabled">
                                <option value="">Escolha o tipo de alteração</option>
                                <option ng-selected="solicitacao.id_tipo == tipo.id" ng-repeat="(k, tipo) in tipos" value="@{{ tipo.id }}">@{{ tipo.nome }}</option>
                            </select>
                            <input ng-disabled="!solicitacao.id" ng-value='solicitacao.id_tipo' type='hidden'>
                            <input ng-disabled="solicitacao.id" ng-value='solicitacao.id_tipo' type='hidden' name='cbAlterationType[]'>
                        </div>
                    </div>
                    <div class="form-inline column-description-spec">
                        <textarea disabled="disabled" placeholder="Descrição específica" rows="4" class="input-control">@{{ solicitacao.descricao_texto }}</textarea>
                        <input ng-disabled="!solicitacao.id" ng-value='solicitacao.descricao' type='hidden'>
                        <input ng-disabled="solicitacao.id" ng-value='solicitacao.descricao' type='hidden' name='txtDescriptionSpec[]'>
                    </div>
                    <div ng-show="!solicitacao.id" class="form-inline btn-add">
                        <button ng-click="remover(k)" type="button" style="min-width:0px;width:auto;height:32px;" class="btn-hollow">
                            <span><i class="icon icon-cross"></i></span>
                        </button>
                    </div>
                </div>
                <div class="input-fields-group clearfix">
                    <div class="form-inline column-edition-type">
                        <div class="select-control">
                            <select ng-model='add_tipo'>
                                <option value="">Escolha o tipo de alteração</option>
                                <option ng-repeat="(k, tipo) in tipos" value="@{{ tipo.id }}">@{{ tipo.nome }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-inline column-description-spec">
                        <textarea ng-model='add_descricao' placeholder="Descrição específica" rows="4" class="input-control"></textarea>
                    </div>
                    <div class="form-inline btn-add">
                        <button ng-click="adicionar()" type="button" class="btn-hollow">
                            <span><i class="icon icon-plus"></i><strong>Adicionar</strong></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    @endif
    @endif
	<fieldset>
		<hr />
		<div ng-cloak id="membershipWrap">
            <!-- @{{participantes}} -->
			<div class="input-fields-group clearfix" ng-repeat="(k, participante) in participantes">
                <div class="clearfix">
                    <div class="form-inline column-access-type">
                        <div class="form-inline column-membership">
                            <label ng-show="k === 0">Participantes</label>
                            <div class="select-control">
                                <select ng-show="!participante.id || participante.id_usuario !== participante.id_usuario_logado || k === 0" ng-disabled="(participante.id_usuario !== participante.id_usuario_logado) || (k === 0)" name="cbMembershipName[]">
                                    <option value="" selected>Selecionar participante</option>
                                    <option ng-selected="participante.id_usuario == membro.id" ng-repeat="(m, membro) in membros" value="@{{membro.id}}">@{{membro.nome}}</option>
                                </select>
                                <!--
                                <selectize ng-show="!participante.id || participante.id_usuario !== participante.id_usuario_logado || k === 0" ng-disabled="(participante.id_usuario !== participante.id_usuario_logado) || (k === 0)" config="myConfig" options='participante.id_usuario' name="cbMembershipName[]"></selectize>
                                <selectize ng-show="participante.id && participante.id_usuario === participante.id_usuario_logado && k > 0" name="cbMembershipAltName[@{{k}}]"></selectize>
                                -->
                                <!--
                                <select ng-show="participante.id && participante.id_usuario === participante.id_usuario_logado && k > 0" name="cbMembershipAltName[@{{k}}]">
                                    <option value="" selected>Selecionar participante...</option>
                                    <option ng-selected="participante.id_usuario == membro.id" ng-repeat="(k, membro) in membros" value="@{{membro.id}}">@{{membro.nome}}</option>
                                </select>
                                -->
                                <selectize config="userLoginConfig" options='membros' ng-init="participante.id_participante_selected = participante.id_usuario" ng-model="participante.id_participante_selected" ng-if="participante.id && participante.id_usuario === participante.id_usuario_logado && k > 0" ng-selected="participante.id_usuario" name="cbMembershipAltName[@{{k}}]"></selectize>
                                <input ng-disabled="participante.id_usuario === participante.id_usuario_logado && k > 0" name="cbMembershipName[]" type="hidden" ng-value="participante.id_usuario">
                            </div>
                        </div>
                        <div class="form-inline column-membership-profile">
                            <label ng-show="k === 0">Perfil de Participação</label>
                            <div class="select-control">
                                <select ng-disabled="(participante.id) || (!participante.id)" ng-model="cbMembershipProfile[k]" ng-init='cbMembershipProfile[k] = participante.id_perfil.toString()' name="cbMembershipProfile[@{{k}}]" class="members-profile-validate">
                                    <option value="" selected>Perfil</option>
                                    <option ng-selected="participante.id_perfil == 1" value="1">Aprovador</option>
                                    <option ng-selected="participante.id_perfil == 2" value="2">Marketing</option>
                                    <option ng-selected="participante.id_perfil == 3" value="3">Revisor</option>
                                    <option ng-selected="participante.id_perfil == 4" value="4">Participante</option>
                                    <option ng-selected="participante.id_perfil == 5" value="5">Arte-finalista</option>
                                    <option ng-selected="participante.id_perfil == 6" value="6">Envio de Arte</option>
                                </select>
                                <input ng-model="cbMembershipProfile[k]" name="cbMembershipProfile[@{{k}}]" type="hidden" ng-value="cbMembershipProfile[k]">
                            </div>
                        </div>
                    </div>
                    <!--div class="form-inline column-membership-notify"-->
                        <!--label ng-show="k === 0" class="form-notify-control">Seguir</label-->
                        <!--div class="form-notify-control checkbox-control"-->
                            <!--input ng-checked="participante.recebe_email == 1" value='1' type="checkbox" name="chkNotificationsSwitch[@{{k}}]" id="chkNotificationsSwitch_@{{k}}" /-->
                            <!--input name="chkNotificationsSwitch[]" type="hidden" ng-value="participante.recebe_email"-->
                            <!--label for="chkNotificationsSwitch_@{{k}}"></label-->
                        <!--/div-->
                    <!--/div-->
                    <div class="form-inline column-membership-team">
                        <label ng-show="k === 0">Equipe</label>
                        <a ng-show="cbMembershipProfile[k] == 1 || cbMembershipProfile[k] == 2" ng-click="participante.adiciona_equipe = (participante.adiciona_equipe == 1) ? 0 : 1" href="javascript:;" class="icon icon-male-symbol"></a>
                        <a ng-show="cbMembershipProfile[k] != 1 && cbMembershipProfile[k] != 2" class="icon"></a>
                    </div>
                    <div class="form-inline btn-add column-membership-remove">
                        <label ng-show="k === 0">Excluir</label>
                        <button ng-show='!participante.id && k > 0' ng-click="remover_membro(k)" type="button" style="min-width:0px;width:auto;height:32px;" class="btn-remove-members btn-hollow">
                            <span><i class="icon icon-cross"></i></span>
                        </button>
                        <button ng-show='( {{Auth::user()->id}} == ticket.id_usuario || {{Auth::user()->id}} == participante.id_usuario ) && participante.id && (participante.id_perfil == 1 || participante.id_perfil == 2) && (cbMembershipProfile|filter:participante.id_perfil).length > 1' ng-click="remover_membro(k)" type="button" style="min-width:0px;width:auto;height:32px;" class="btn-remove-members btn-hollow">
                            <span><i class="icon icon-cross"></i></span>
                        </button>
                        <button ng-show='k > 0 && {{Auth::user()->id}} == ticket.id_usuario && participante.id && participante.id_perfil != 1 && participante.id_perfil != 2' ng-click="remover_membro(k)" type="button" style="min-width:0px;width:auto;height:32px;" class="btn-remove-members btn-hollow">
                            <span><i class="icon icon-cross"></i></span>
                        </button>
                    </div>
                </div>
                <!-- Looping de membros da equipe -->
                <div ng-show="cbMembershipProfile[k] == 1 || cbMembershipProfile[k] == 2" class="input-fields-group clearfix" ng-repeat="(e, equipe) in participante.equipe_membros">
                    <div class="form-inline">
                        <div class="form-inline column-access-type">
                            <span class="icon-bullet-tree icon-arrow-curve-right"></span>
                            <div class="form-inline select-control column-membership-select">
                                <select name="cbMembershipTeamAltName[@{{k}}][]" ng-disabled="(equipe.id_usuario !== participante.id_usuario_logado)">
                                    <option value="" selected>Membros da equipe</option>
                                    <option ng-selected="membro.id == equipe.id_usuario" ng-repeat="(m, membro) in membros" value="@{{membro.id}}">@{{membro.nome}}</option>
                                </select>
                                <input name="cbMembershipTeamName[@{{k}}][]" type="hidden" ng-value="equipe.id_usuario">
                            </div>
                            <div class="form-inline">
                                <div class="select-control column-membership-profile">
                                    <select name="cbMembershipTeamProfile[]" ng-model="cbMembershipProfile[k]" disabled>
                                        <option value="">Perfil</option>
                                        <option value="1">Aprovador</option>
                                        <option value="2">Marketing</option>
                                        @if (Auth::user()->id_adm_empresa != 2)
                                        <option value="3">Revisor</option>
                                        @endif
                                        <option value="4">Participante</option>
                                        @if (Auth::user()->id_adm_empresa != 2)
                                        <option value="5">Arte-finalista</option>
                                        @endif
                                        <option value="6">Envio de Arte</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--div class="form-inline column-membership-notify"-->
                            <!--div class="checkbox-control"-->
                                <!--input ng-checked="equipe.recebe_email == 1" value='1' type="checkbox" name="chkNotificationsSwitchMember[@{{k}}][@{{e}}]" id="chkNotificationsSwitchmember_@{{k}}_@{{e}}" /-->
                                <!--input name="chkNotificationsSwitch[]" type="hidden" ng-value="participante.recebe_email"-->
                                <!--label for="chkNotificationsSwitchmember_@{{k}}_@{{e}}"></label-->
                            <!--/div-->
                        <!--/div-->
                        <!--div class="form-inline btn-add column-membership-remove push-offset"-->
                            <!--button ng-show="!equipe.id || (equipe.id && {{Auth::user()->id}} == ticket.id_usuario)" ng-click="remover_membro_equipe(k, e)" type="button" id="btnAddArtworkAlt" class="btn-remove-team-members btn-remove-members"-->
                                <!--span class="icon-cross"></span-->
                            <!--/button-->
                        <!--/div-->
                    </div>
                </div>
                <!-- Looping de membros da equipe -->
                <!-- Formulário para inserir membro da equipe -->
                <div class="input-fields-group team-member-group clearfix" ng-show="participante.adiciona_equipe">
                    <div class="form-inline membership-team-col">
                        <div class="form-inline column-access-type">
                            <span class="icon-bullet-tree icon-arrow-curve-right"></span>
                            <div class="form-inline select-control column-membership-select">
                                <selectize config="myConfig" options='membros' name="cbMembershipTeamName[]" ng-model="participante.add_participante"></selectize>
                                <!--
                                <select name="cbMembershipTeamName[]" ng-model="participante.add_participante">
                                    <option value="" selected>Membros da equipe</option>
                                    <option ng-repeat="(k, membro) in membros" ng-if="!membro.ignore" value="@{{membro.id}}">@{{membro.nome}}</option>
                                </select>
                                -->
                            </div>
                            <div class="form-inline">
                                <div class="select-control column-membership-profile">
                                    <select name="cbMembershipTeamProfile[]" ng-model="cbMembershipProfile[k]" disabled>
                                        <option value="">Perfil</option>
                                        <option ng-selected="participante.id_perfil == 1" value="1">Aprovador</option>
                                        <option ng-selected="participante.id_perfil == 2" value="2">Marketing</option>
                                        @if (Auth::user()->id_adm_empresa != 2)
                                        <option ng-selected="participante.id_perfil == 3" value="3">Revisor</option>
                                        @endif
                                        <option ng-selected="participante.id_perfil == 4" value="4">Participante</option>
                                        @if (Auth::user()->id_adm_empresa != 2)
                                        <option ng-selected="participante.id_perfil == 5" value="5">Arte-finalista</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-inline btn-add">
                                <button ng-click="adicionar_membro_equipe(k)" type="button" id="btnAddMembership" class="btn-hollow">
                                    <span><i class="icon icon-plus"></i><strong>Adicionar</strong></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Formulário para inserir membro da equipe -->
            </div>
			<div class="input-fields-group clearfix">
				<div class="clearfix">
                    <div class="form-inline column-access-type">
                        <div class="form-inline column-membership">
                            <label ng-show="participantes.length === 0">Participantes</label>
                            <div class="select-control">
                                <selectize config="myConfig" options='membros' name="cbMembershipTeamName[]" ng-model="add_participante"></selectize>
                                <!--
                                <select ng-model='add_participante'>
                                    <option value="" selected>Selecionar participante</option>
                                    <option ng-repeat="(k, membro) in membros" ng-if="!membro.ignore" value="@{{membro.id}}">@{{membro.nome}}</option>
                                </select>
                                -->
                            </div>
                        </div>
                        <div class="form-inline column-membership-profile">
                            <label ng-show="participantes.length === 0">Perfil de Participação</label>
                            <div class="select-control">
                                <select ng-model='add_perfil' class="members-profile-validate">
                                    <option value="" selected>Perfil</option>
                                    <option value="1">Aprovador</option>
                                    <option value="2">Marketing</option>
                                    <option value="3">Revisor</option>
                                    <option value="4">Participante</option>
                                    <option value="5">Arte-finalista</option>
                                    <option value="6">Envio de Arte</option>
                                </select>
                            </div>
                        </div>
                        <div style="right:0px;" class="form-inline btn-add abs-offset abs-middle-offset">
                            <button ng-click="adicionar_membro()" type="button" class="btn-hollow">
                                <span><i class="icon icon-plus"></i><strong>Adicionar</strong></span>
                            </button>
                        </div>
                    </div>
                </div>
			</div>
        </div>
	</fieldset>
    <div class="clearfix">
        <hr />
        <div class="form-btn-set clearfix">
            <input ng-repeat="(k, excluido) in cbMembershipDelete" type="hidden" name="membros_excluidos[]" ng-value="excluido">
            <input ng-repeat="(k, excluido) in cbMembershipEquipeDelete" type="hidden" name="equipe_membros_excluidos[]" ng-value="excluido">
            <!-- @{{cbMembershipEquipeDelete}}
            @{{cbMembershipProfile}}<br>
            @{{cbMembershipDelete}}<br>
            @{{(cbMembershipProfile|filter:'1').length}}<br>
            @{{(cbMembershipProfile|filter:'2').length}}
            @{{participantes}}<br> -->
            <a href="{{ route('site.artwork.ticket.edit.step1.get', [$ticket->numero]) }}" class="btn btn-negative">
                <span><i class="icon icon-left icon-ban-circle"></i><strong>Cancelar</strong></span> 
            </a>
            <button type="submit" class="btn btn-positive">
                <span><i class="icon icon-left icon-check"></i><strong>Salvar</strong></span>
            </button>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@stop
