@extends('site.site_template') 

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop 

@section('content')
{!! Breadcrumbs::renderIfExists('artwork.solicitar_alteracao', $variacao->id) !!}
<div class="content">
    <div class="top-head">
        <div class="top-inline top-packing-logo">
            {{ Html::image(asset($thumb), 'Logo', array('id' => 'imgLogo')) }}
        </div>
        <div class="top-inline top-head-title top-head-title-center top-head-title-new-sku top-head-no-float">
            <h2><span>{{ $ticket->sku_nome_completo }}</span></h2>
        </div>
        <div class="top-inline top-nav-right">
            <ul>
                <li><strong class="ticket-info"><span class="icon icon-options-settings"></span> Ticket {{ $ticket->numero }}</strong></li>
            </ul>
        </div>
    </div>
    <div class="ticket-request-info ticket-request-print">
        <section class="section-ticket-info-print">
            <table border="0" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th width="480">Informações Iniciais</th>
                        <th width="100">Data de Início</th>
                        <th width="140">Envio para Aprovação</th>
                        <th width="140">Entrega no Fornecedor </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="480" >{!! $ticket->descricao !!}</td>
                        <td width="100">{{ ($ticket->dt_ini) ? $ticket->dt_ini->format('d/m/Y') : '' }}</td>
                        <td width="140">{{ ($ticket->dt_envio) ? $ticket->dt_envio->format('d/m/Y') : '' }}</td>
                        <td width="140">{{ ($ticket->dt_fim) ? $ticket->dt_fim->format('d/m/Y') : '' }}</td>
                    </tr>
                </tbody>
            </table>
        </section>
        <section class="section-membership-print">
            <hr />
            <table border="0" cellpadding="0" cellspacing="0" class="table-print-members">
                <thead>
                    <tr>
                        <th width="780">Participantes</th>
                        <th width="370">Perfil de Participação</th>
                        <th width="180">Notificações</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($participantes as $index => $usuario)
                        <tr>
                            <td width="780">
                                @if ($ticket->admUsuario->id === $usuario->admUsuario->id)
                                <span class="icon-is-owner icon-star"></span>
                                @else
                                <span class="icon-is-member icon-play"></span>
                                @endif
                                {{ $usuario->AdmUsuario->nome_abreviado }}
                            </td>
                            <td width="370">{{ $usuario->artwPerfisUsuario->nome }}</td>
                            <td width="180"><span class="icon-notify icon-social-email"></span></td>
                        </tr>
                        @foreach ($usuario->artwPerfisUsuariosEquipeTickets as $membro)
                            <tr>
                                <td width="720">
                                    <div class="has-sub-members">    
                                        <span class="icon-status icon-arrow-curve-right icon-team-members"></span> {{ $membro->AdmUsuario->nome_abreviado }}
                                    </div>
                                </td>
                                <td width="380">{{ $usuario->artwPerfisUsuario->nome }}</td>
                                <td width="380"><span class="icon-notify icon-social-email"></span></td>
                            </tr>
                        @endforeach
                    @endforeach
                </tbody>
            </table>
        </section>
        <section class="section-art-alteration-print">
            <hr />
            <h4>Alterações</h4>
            <table border="0" cellpadding="0" cellspacing="0" class="table-print-alteration">
                <tbody>
                    @foreach ($ticket->ciclo_atual()->ArtwSolicitacoes as $solicitacao)
                    <tr>
                        <td width="175">
                            <span class="icon {{ $solicitacao->ArtwSolicitacaoTipo->icone }}"></span>
                            <div class="description-txt">
                                <h4>{{ $solicitacao->ArtwSolicitacaoTipo->nome }}</h4>
                                <small>{{ $solicitacao->AdmUsuario->nome_abreviado }}</small>
                                <small>{{ $solicitacao->created_at->format('d/m/Y \à\s H:s') }}</small>
                            </div>
                        </td>
                        <td width="480">
                            <p style="font-size:16px;"> 
                                {!! $solicitacao->descricao !!}
                            </p>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </section>
        <section style="padding-top: 15px; padding-bottom:15px;">
            <hr />
            <h4>Arquivos</h4>
            <table border="0" cellpadding="0" cellspacing="0" class="ticket-table-attachment-info">
                <tbody>
                    @forelse ($ticket->ArtwArquivos as $arquivo)
                    <tr>
                        <td>
                            <div style="padding:14px;border-radius:20px;border:1px solid #ACACAC;">    
                                <span class="icon icon-document-fill"></span>
                                <div class="description-txt">
                                    <h5><span>{{ $arquivo->descricao }}</span></h5>
                                    <p>Anexado por {{ $arquivo->AdmUsuario->nome_abreviado }} às {{ $arquivo->created_at->format('d/m/Y \à\s H:s') }}</p>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td>
                            <p style="margin:0px;">Nenhum arquivo anexado.</p>
                        </td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </section>
        <div class="form-btn-set">
            <hr />
            <button onclick="window.print()" type="button" class="btn btn-positive">
                <span><i class="icon icon-left icon-layers"></i> <strong>Imprimir</strong></span>
            </button>
        </div>
    </div>
</div>
@stop
