@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
<script>
    var csrf_token = '{{ csrf_token() }}';
</script>
<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js')}}"></script>
<script src="{{ asset('/js/angular/ng-file-upload-shim.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload.min.js') }}"></script>
<script src="{{ asset('/js/angular/controllers/fornecedor_upload_pdf.js') }}"></script>
<div class="breadcumbs-out clearfix">		
    <div class="breadcrumbs-inner content-fluid clearfix">	
        <ul>
            <li class="active">Anexar PDF do Fornecedor</li>
        </ul>
    </div>
</div>
<div ng-app="app" ng-controller="fornecedorUploadPDFCtrl" ng-cloak class="content content-fluid clearfix">
    <div class="artw-download-container margin-top-40 margin-bottom-40">
        <!-- Showcase panel -->
        <div class="showcase-panel no-margin-top margin-bottom-30 clearfix">
            <div class="showcase-left blank">
                <span class="helper"></span>
                {{ Html::image(asset('/images/layout/cliente_'. config('app.costummer_id') .'/logo-home.png'), 'Logo') }}
            </div>
            <div class="showcase-body">
                <div class="showcase-container">
                    <div class="flexbox-container flex-no-wrap clearfix">
                        <!-- Item não compartilhado -->
                        <div class="flex-auto showcase-info showcase-item-detail text-inline col-100-max">
                            <h3 class="txt-bold">Pepsi Zero Lata Sleek 350ml</h3>
                            <small class="text-small txt-color-alt margin-top-5">embalagem</small>
                        </div>
                        <div class="flex-auto showcase-info showcase-item-detail text-inline col-20-max">
                            <h3 class="txt-light">Teste</h3>
                            <small class="text-small txt-color-alt margin-top-5">campanha</small>
                        </div>
                        <div class="flex-auto showcase-info showcase-item-detail text-inline col-20-max">
                            <h3 class="txt-light">Teste</h3>
                            <small class="text-small txt-color-alt margin-top-5">variação</small>
                        </div>
                        <div class="flex-auto showcase-info showcase-item-detail text-inline col-20-max">
                            <h3 class="txt-light">Teste</h3>
                            <small class="text-small txt-color-alt margin-top-5">item</small>
                        </div>
                        <!-- Item não compartilhado -->
                    </div>
                    <div class="flexbox-container clearfix">
                        <div class="flex-auto showcase-info showcase-feature text-inline no-margin-bottom">
                            <small class="text-small txt-bold txt-color-primary">Ticket: 12345678</small>
                            <small class="text-small txt-bold txt-color-primary">Versão 1</small>
                        </div>
                        <!-- Item não compartilhado -->
                        <div class="flex-auto showcase-info showcase-feature text-inline no-margin-bottom">
                            <small class="text-small txt-regular">Marca: <strong class="txt-semibold">Teste</strong></small>
                            <small class="text-small txt-regular">Categoria: <strong class="txt-semibold">Teste</strong></small>
                        </div>
                        <div class="flex-auto showcase-info showcase-feature text-inline no-margin-bottom">
                            <small class="text-small txt-regular">Natureza: <strong class="txt-semibold">Teste</strong></small>
                            <small class="text-small txt-regular">Função: <strong class="txt-semibold">Teste</strong></small>
                        </div>
                        <!-- EAN - TODO  -->
                        <div class="flex-auto showcase-info showcase-feature text-inline no-margin-bottom">
                            <small class="text-small txt-regular">Código de Barras: 
                                <strong class="txt-semibold">N/A</strong>
                            </small>
                        </div>
                        <!-- END EAN TODO -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END Showcase panel -->
        <section>
            <form>
                <div class="block">
                    <h4 class="master-title-alt txt-bold margin-bottom-20">Baixar Arte-final</h4>
                    <div class="margin-left-10 margin-right-10">
                        <h4 class="master-title-sub txt-light txt-uppercase">Arquivos Baixáveis</h4>
                        <p>
                            Os arquivos não estão mais disponíveis, expiraram no dia XX/XX/XXXX.
                        </p>
                    </div>
                </div>
                <div class="block">
                    <h4 class="master-title-alt txt-bold margin-bottom-20">Anexar PDF do Fornecedor</h4>
                    <div class="margin-left-10 margin-right-10">
                        <h4 class="master-title-sub txt-light txt-uppercase">Carregar Arquivo</h4>
                        <p class="margin-bottom-15">
                            Carregue o arquivo para a aprovação do cliente.
                        </p>
                        <div class="flexbox-container flexbox-group margin-bottom-30">
                            <div data-title="Subir em padrão PDF/X-3 com fontes em curvas para evitar divergências na renderização." class="col-files-attach flex flex-fluid files-control attachPDF ui-tooltip top">
                                <input type="file" accept="application/pdf" ng-model="pdf" name="file" id="pdfFileUpload" valid-file=".pdf" ngf-select ngf-model-invalid="errorFile" required />
                                <span class="files-control-fake"><strong></strong><i class="addon fa fa-paperclip"></i></span>
                            </div>
                            <div class="flex col-artwork-submit">   
                                <button ng-click="upload_arquivo($event)" type="button" class="btn-control btn-positive">Adicionar</button>
                            </div>
                            <div ng-show="pdf.progress > 0" class="progress-circle animate">
                                <svg id="svg" viewbox="0 0 100 100">
                                    <circle cx="50" cy="50" r="40" fill="none"/>
                                    <path fill="none" stroke-linecap="round" stroke-width="4" stroke-dasharray="@{{pdf.progress*2.5}},250.2" d="M50 10 a 40 40 0 0 1 0 80 a 40 40 0 0 1 0 -80"/>
                                    <text id="count" class="progress" x="50" y="50" text-anchor="middle" dy="7" ng-bind="pdf.progress + '%'"></text>
                                </svg>
                            </div>
                        </div>
                        <div ng-if="!fornecedorPdfs.length" class="margin-bottom-30">
                            <div class="panel default no-hover">
                                <i class="icon fa fa-file-pdf-o"></i>
                                <div class="panel-body">
                                    <h4 class="panel-heading txt-semibold no-margin">Nenhum PDF anexado ainda.</h4>
                                </div>
                            </div>
                        </div>
                        <div ng-if="fornecedorPdfs.length" class="margin-bottom-30">
                            <div ng-repeat="(p, pdf) in fornecedorPdfs track by $index" class="workflow-supplier-card panel secondary">
                                <div class="panel-body">
                                    <h4 class="panel-heading animate">
                                        <a target="_blank" href="javascript:;">
                                            <i class="icon fa fa-file-pdf-o"></i>
                                            <strong class="txt-semibold">Rolha Pet • Ruby Cola Tradicional / Zero • 002 • Versão 1</strong>
                                            <small class="description-small txt-regular">Anexado por Admin Rubrum, 13h53 de 10/07/2023</small>
                                            <small class="description-small txt-regular no-margin">Arquivo original: @{{pdf.name}} - @{{pdf.size | bytes}}</small>
                                        </a>
                                    </h4>
                                    <div class="flex workflow-supplier-info margin-top-15">
                                        <i class="icon fa fa-industry"></i>
                                        <div class="panel-body">
                                            <h4 class="panel-heading txt-semibold">Fornecedor • Allucan</h4>
                                            <small class="description-small txt-regular">Aguardando aprovação</small>
                                            <small class="description-small txt-regular">Aprovado por Admin Rubrum, às 14h27 de 10/07/2023</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <div class="block">
                                        <div class="dropdown drop-to-left pull-right">
                                            <a data-title="Opções" href="javascript:;" class="dropdown-btn dropdown-toggle ui-tooltip top fa fa-ellipsis-h"></a>
                                            <div class="dropdown-list">
                                                <ul>    
                                                    <li><a href="javascript:;" ng-click="excluir()" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div ng-if="fornecedorPdfs.length" class="btn-group txt-center margin-top-30 no-margin-bottom">
                    <div class="bs border-color-primary padding-top-40">
                        <button type="button" ng-click="enviar_para_cliente()" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                            <span><i class="icon fa fa-check"></i> Enviar para o Cliente</span>
                        </button>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>
@stop