@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
{!! Breadcrumbs::renderIfExists('artwork.categorias') !!}
<script type="text/javascript" src="{{ asset('/js/site/jquery-alert-box.js') }}"></script>
<div class="popup-mask"></div>
<div class="content clearfix">
	<div class="top-head">
		<div class="top-inline top-head-title">
			<h2><span>Categoria de produtos</span></h2>
		</div>
		<div class="top-inline top-nav-right">
			<ul>
				<li><a href="{{ url('/site/artwork/package/create') }}"><span class="icon icon-magic-wand"></span> Nova Embalagem</a></li>
			</ul>
		</div>
	</div>
	<nav class="nav-categorias">
		<ul>
			@foreach($categorias as $categoria)
				<li>{{ Html::linkRoute('site.artwork.family', $categoria->nome, array($categoria->id)) }}</li>
			@endforeach
		</ul>
	</nav>
</div>
@stop
