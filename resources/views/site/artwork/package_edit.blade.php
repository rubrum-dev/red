@extends('site.site_template')

@section('title')
@stop

@section('content')
{!! Breadcrumbs::renderIfExists('artwork.nova_embalagem') !!}

<script>
    
    var packageType = '{{ ($variacao->id_tipo == 1) ? 'Regular' : 'Promocional' }}';
    var cbNewCategory = '{{ $variacao->sku->produtos->categorias->id }}';
    var cbPckFamily = '{{ $variacao->sku->produtos->familias->id }}';
    var cbPckProduct = '{{ $variacao->sku->produtos->id }}';
    var cbPckContentSize = '{{ $variacao->sku->dimensoes->id }}';
    var cbPckType = '{{ $variacao->sku->dimensoes->tiposEmbalagens->id }}';
    var cbPckCampaign = '{{ $variacao->id_campanha }}';
    var cbPckTypeItem = '{{ $item->id_tipo }}';
    var variacao_edicao = '{{ (!$variacao->principal) ? $variacao->nome : "" }}';
    
</script>

<script src="{{ asset('/js/angular/angular.min.js')}}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js')}}"></script>
<script src="{{ asset('/js/angular/controllers/package_edit.js?v=2')}}"></script>
<script src="{{ asset('js/site/artwork/package-create.js') }}"></script>

@if (count($errors) > 0)
<div class="alert alert-danger" role="alert">
    <div class="alert-error">
        @foreach ($errors->all() as $error)
        {{ $error}}<br />
        @endforeach
    </div>
</div>
@endif
@if (session('sucesso'))
<div class="alert alert-info">
    {{ session('sucesso') }}
</div>
@endif
<div class="content clearfix" ng-app="app" ng-controller="PackageCreateCtrl">
    <div class="top-head">
		<div class="top-inline top-head-title text-left">
			<h2>Alterar Nome da Embalagem</h2>
		</div>
	</div>
    <form name="newPackageForm" action="" method="POST" id="package-create-form">
        <input type="hidden" name="_token" value="{{ csrf_token()}}" />
        <div class="text-block">
            <h4>Nome da embalagem</h4>
            <p>
                A nomenclatura é fundamental para que tudo funcione de forma correta. É possível alterar tanto o nome quanto os 
                atributos da embalagem. Sinta-se à vontade em alterar suas escolhas até que o nome construído faça sentido.
            </p>
        </div>
        <div class="box-package-info">
            
            <span ng-hide="
                    (packageType === 'Regular' || (packageType === 'Promocional' && ((cbPckCampaign !== '' && cbPckCampaign !== '0') || (cbPckCampaign === '0' && packageCampaignName))))
                    && ((cbPckFamily !== '0' && cbPckFamily !== '') || (cbPckFamily === '0' && packageFamilyName))
                    && ((cbNewCategory !== '0' && cbNewCategory !== '0') || (cbNewCategory === '0' && packageCategName))
                    && ((cbPckProduct !== '0' && cbPckProduct !== '') || (cbPckProduct === '0' && tfNewProductName))
                    && ((cbPckType !== '0' && cbPckType !== '') || (cbPckType === '0' && packagePackingName && natureza))
                    && ((cbPckContentSize !== '0' && cbPckContentSize !== '') || (cbPckContentSize === '0' && packageContentSpec))
                    && ((variacoes.length && variacaoName) || (!variacoes.length && ((packageHasVaritation == 'Sim' && variacaoName) || packageHasVaritation == 'Nao' || variacao_edicao) ) )
                         " class="icon icon-magic-wand"></span>
            
            <span ng-show="
                    (packageType === 'Regular' || (packageType === 'Promocional' && ((cbPckCampaign !== '' && cbPckCampaign !== '0') || (cbPckCampaign === '0' && packageCampaignName))))
                    && ((cbPckFamily !== '0' && cbPckFamily !== '') || (cbPckFamily === '0' && packageFamilyName))
                    && ((cbNewCategory !== '0' && cbNewCategory !== '0') || (cbNewCategory === '0' && packageCategName))
                    && ((cbPckProduct !== '0' && cbPckProduct !== '') || (cbPckProduct === '0' && tfNewProductName))
                    && ((cbPckType !== '0' && cbPckType !== '') || (cbPckType === '0' && packagePackingName && natureza))
                    && ((cbPckContentSize !== '0' && cbPckContentSize !== '') || (cbPckContentSize === '0' && packageContentSpec))
                    && ((variacoes.length && variacaoName) || (!variacoes.length && ((packageHasVaritation == 'Sim' && variacaoName) || packageHasVaritation == 'Nao' || variacao_edicao) ) )
                         " class="icon icon-check cor-azul"></span>
            
            <h4>
                <span ng-repeat="produto in produtos" ng-show='produto.id == cbPckProduct' class="cor-azul">@{{ produto.nome|uppercase }}</span>
                <span ng-show="!cbPckProduct || cbPckProduct == '0'" ng-class="{'cor-azul': cbPckProduct == '0' && tfNewProductName}">@{{ (tfNewProductName) ? tfNewProductName : 'NOME DO PRODUTO' | uppercase }}</span>
                - 
                <span ng-repeat="tipo in tipos" ng-show='tipo.id == cbPckType' class="cor-azul">@{{ tipo.nome|uppercase }}</span>
                <span ng-show="!cbPckType || cbPckType == '0'" ng-class="{'cor-azul': cbPckType == '0' && packagePackingName}">@{{ (packagePackingName) ? packagePackingName : 'TIPO DE EMBALAGEM' | uppercase }}</span>
                - 
                <span ng-repeat="tamanho in tamanhos" ng-show='tamanho.id == cbPckContentSize' class="cor-azul">@{{ tamanho.nome|uppercase }}</span>
                <span ng-show="!cbPckContentSize || cbPckContentSize == '0'" ng-class="{'cor-azul': cbPckContentSize == '0' && packageContentSpec}">@{{ (packageContentSpec) ? packageContentSpec : 'VOLUME' | uppercase }}</span>
                
                <!--span ng-hide="packageHasVaritation === 'Nao' || !variacao_edicao"> - </span><span ng-hide="packageHasVaritation === 'Nao' || !variacao_edicao" ng-class="{'cor-azul': variacaoName || variacao_edicao}">@{{ (variacaoName) ? variacaoName : (variacao_edicao) ? variacao_edicao : 'VARIAÇÃO' | uppercase }}</span-->
                <span ng-show="packageHasVaritation != 'Nao'"> - </span><span ng-show="packageHasVaritation != 'Nao'" ng-class="{'cor-azul': variacaoName}">@{{ (variacaoName) ? variacaoName : 'VARIAÇÃO' | uppercase }}</span>
                
                <span ng-show="!packageType || packageType == 'Promocional'"> - </span>
                <span ng-repeat="campanha in campanhas" ng-show='campanha.id == cbPckCampaign' class="cor-azul">@{{ campanha.nome|uppercase }}</span>
                <span ng-show="!packageType || (packageType == 'Promocional' && cbPckCampaign === '0') || (packageType == 'Promocional' && !cbPckCampaign) " ng-class="{'cor-azul': packageCampaignName}">@{{ (packageCampaignName) ? packageCampaignName : 'CAMPANHA PROMOCIONAL' | uppercase }}</span>
            </h4>
            <p><strong>Natureza:</strong> @{{ packageType }}</p>
            <p><strong>Categoria:</strong> <span ng-repeat="categoria in categorias" ng-show='categoria.id == cbNewCategory'>@{{ categoria.nome }}</span><span ng-show="cbNewCategory === '0'">@{{ packageCategName }}</span></p>
            <p><strong>Família:</strong> <span ng-repeat="familia in familias" ng-show='familia.id == cbPckFamily'>@{{ familia.nome }}</span><span ng-show="cbPckFamily === '0'">@{{ packageFamilyName }}</p>
        </div>
        
        <!--div>
            packageType: @{{packageType}} <br />
            packageIsNewProd: @{{packageIsNewProd}} <br />
            familias: @{{familias}} <br />
            cbPckFamily: @{{cbPckFamily}} <br />
            produtos: @{{produtos}} <br />
            cbPckProduct: @{{cbPckProduct}} <br />
            tipos: @{{tipos}} <br />
            tiposItens: @{{tiposItens}} <br />
            tamanhos: @{{tamanhos}} <br />
            categorias: @{{categorias}} <br />
            cbNewCategory: @{{cbNewCategory}} <br />
            packageCategName: @{{packageCategName}}  <br />
            campanhas: @{{campanhas}} <br />
            cbPckCampaign: @{{cbPckCampaign}} <br />
            cbPckType: @{{cbPckType}} <br />
            cbPckContentSize: @{{cbPckContentSize}} <br />
            variacoes: @{{variacoes}} <br />
            variacaoName: @{{ variacaoName }} <br /> 
            packageCampaignName: @{{ packageCampaignName }} <br /> 
        </div-->
        
        <section class="form-section clearfix">
            <hr class="form-section-spacer">
            <p><label>Natureza da embalagem</label></p>
            <div class="form-inline form-inline-spacer">
                <label class="radio-btn-control radio-btn-icon">    
                    <input type="radio" ng-change="carregar_categorias()" ng-model="packageType" name="packageType" value="Regular" class="radio-btn-hidden" />
                    <span class="icon-social-500px">Regular</label>
                </label>
            </div>
            <div class="form-inline">
                <label class="radio-btn-control radio-btn-icon">    
                    <input type="radio" ng-change="carregar_categorias()" ng-model="packageType" name="packageType" value="Promocional" class="radio-btn-hidden" />
                    <span class="icon-dollar">Promocional</span>
                </label>
            </div>
        </section>
        
        <section ng-show="packageType" class="form-section clearfix">
            <hr class="form-section-spacer">
            <p><label>Categoria do produto?</label></p>
            <div class="input-inline select-control form-control-280px">
                <select ng-change="carregar_familias()" ng-model="cbNewCategory" name="cbNewCategory" id="cbNewCategory">
                    <option value="">Categoria</option>
                    <option value="0">Cadastrar nova categoria</option>
                    <option ng-repeat="categoria in categorias" value="@{{categoria.id}}">@{{categoria.nome}}</option>
                </select>
            </div>
            <div ng-show="cbNewCategory === '0'" class="input-inline form-control form-control-280px">
                <input ng-required="cbNewCategory === '0'" type="text" placeholder="Nome da nova categoria" ng-model="packageCategName" name="packageCategName" id="packageCategName" />
            </div>
            <div ng-show="cbNewCategory === '0'" class="input-inline hint">
                <p>
                    <strong>Dica:</strong> Atente-se para não cadastrar um sinônimo de uma categoria de produto já cadastrada. 
                </p>
            </div>
        </section>
        <section ng-show="cbNewCategory === '0' || cbNewCategory !== ''" class="form-section clearfix">
            <hr class="form-section-spacer">
            <p><label>Família do produto</label></p>
            <div class="input-inline select-control form-control-280px">
                <select ng-change="carregar_produtos()" ng-model="cbPckFamily" name="cbPckFamily" id="cbPckFamily">
                    <option value="">Família</option>
                    <option value="0">Cadastrar nova família</option>
                    <option ng-repeat="familia in familias" value="@{{familia.id}}">@{{familia.nome}}</option>
                </select>
            </div>
            <div ng-show="cbPckFamily === '0'" class="input-inline form-control form-control-280px">
                <input ng-required="cbPckFamily === '0'" type="text" placeholder="Nome da nova família" ng-model="packageFamilyName" name="packageFamilyName" id="packageFamilyName" />
            </div>
            <div ng-show="cbPckFamily === '0'" class="input-inline hint">
                <p>
                    <strong>Dica:</strong> As famílias servem para agrupar produtos que compartilham algum identidade entre si, 
                    mas nem sempre o nome da família está presente no nome do produto.
                </p>
            </div>
        </section>    
        <section ng-show="cbPckFamily === '0' || cbPckFamily !== ''" class="form-section clearfix">
            <hr class="form-section-spacer">
            <p><label>Nome do produto</label></p>
            <div class="input-inline select-control form-control-280px">
                <select ng-change="carregar_tipos();carregar_campanhas();" ng-model="cbPckProduct" name="cbPckProduct" id="cbPckProduct">
                    <option value="">Produto</option>
                    <option value="0">Cadastrar novo produto</option>
                    <option ng-repeat="produto in produtos" value="@{{produto.id}}">@{{produto.nome}}</option>
                </select>
            </div>
            <div ng-show="cbPckProduct === '0'" class="input-inline form-control form-control-280px">
                <input ng-required="cbPckProduct === '0'" type="text" placeholder="Nome do novo produto" ng-model="tfNewProductName" name="tfNewProductName" id="tfNewProductName" />
            </div>
            <div ng-show="cbPckProduct === '0'" class="input-inline hint">
                <p>
                    <strong>Dica:</strong> Utilize letras maiúsculas e minúsculas para o nome. Não escreva a categoria ou tipo 
                    do produto junto ao nome, apenas o nome puro do produto já basta.
                </p>
            </div>
        </section>
        
        <section ng-show="(cbPckProduct === '0' || cbPckProduct !== '') && packageType === 'Promocional'" class="form-section clearfix">
            <hr class="form-section-spacer">
            <div class="text-block">
                <h4>Campanha promocional</h4>
                <p>
                    As embalagens promocionais são agrupadas por campanha. É importante que as embalagens de uma mesma campanha 
                    promocional compartilhem o mesmo nome de campanha para que fiquem organizadas da forma correta. 
                </p>
            </div>
            <div class="input-inline select-control form-control-280px">
                <select ng-model="cbPckCampaign" name="cbPckCampaign" id="cbPckCampaign">
                    <option value="">Campanha</option>
                    <option value="0">Cadastrar nova campanha...</option>
                    <option ng-repeat="campanha in campanhas" value="@{{campanha.id}}">@{{campanha.nome}}</option>
                </select>
            </div>
            <div ng-show="cbPckCampaign === '0'" class="input-inline form-control form-control-280px">
                <input ng-required="cbPckCampaign === '0'" type="text" placeholder="Nome da nova campanha" ng-model="packageCampaignName" name="packageCampaignName" id="packageCampaignName" />
            </div>
            <div ng-show="cbPckCampaign === '0'" class="input-inline hint">
                <p>
                    <strong>Dica:</strong> Escolha um nome curto e abrangente, como o nome do evento seguido do ano 
                    (Carnaval 2018). Utilize as variações para identificar artes diferentes de uma mesma campanha promocional.
                </p>
            </div>
        </section>
        
        <section ng-show="cbPckProduct === '0' || cbPckProduct !== ''" class="form-section clearfix">
            <hr class="form-section-spacer">
            <p><label>Tipo de embalagem?</label></p>
            <div class="input-inline select-control form-control-280px">
                <select ng-change="carregar_tamanhos()" ng-model="cbPckType" name="cbPckType" id="cbPckType" class="cb-pkg-type">
                    <option value="">Tipo de embalagem</option>
                    <option value="0">Cadastrar novo tipo de embalagem</option>
                    <option ng-repeat="tipo in tipos" value="@{{tipo.id}}">@{{tipo.nome}}</option>
                </select>
            </div>
            <div ng-show="cbPckType === '0'" class="input-inline">
                <div class="form-control form-control-280px">
                    <input ng-required="cbPckType === '0'" type="text" placeholder="Nome do novo tipo de embalagem" ng-model="packagePackingName" name="packagePackingName" id="packagePackingName" />
                </div>
                <div class="radio-package-type clearfix">
                    <label class="input-inline radio-btn-control">
                        <input ng-required="cbPckType === '0'" type="radio" ng-model="natureza" name="natureza" value="2" />
                        <span class="checkmark">Primária</span>
                    </label>
                    <label class="input-inline radio-btn-control">
                        <input ng-required="cbPckType === '0'" type="radio" ng-model="natureza" name="natureza" value="3" />
                        <span class="checkmark">Secundária</span>
                    </label>
                    <label class="input-inline radio-btn-control">
                        <input ng-required="cbPckType === '0'" type="radio" ng-model="natureza" name="natureza" value="4" />
                        <span class="checkmark">Terciária</span>
                    </label>
                </div>
            </div>
            <div ng-show="cbPckType === '0'" class="input-inline hint">
                <p>
                    <strong>Dica:</strong> Atente-se para não cadastrar um sinônimo de uma embalagem já cadastrada. Utilize nomes 
                    que definam uma embalagem completa. Nomes como Rótulo, Foil ou Rolha não configuram aqui pois são apenas itens 
                    de uma embalagem e são cadastradas numa etapa posterior a esta.
                </p>
            </div>
        </section>
        <section ng-show="cbPckType === '0' || cbPckType !== ''" class="form-section clearfix">
            <hr class="form-section-spacer">
            <p><label>Volume da embalagem</label></p>
            <div class="input-inline select-control form-control-280px">
                <select ng-change="carregar_variacoes()" ng-model="cbPckContentSize" name="cbPckContentSize" id="cbPckContentSize">
                    <option value="">Volume</option>
                    <option value="0">Cadastrar novo volume</option>
                    <option ng-repeat="tamanho in tamanhos" value="@{{tamanho.id}}">@{{tamanho.nome}}</option>
                </select>
            </div>
            @{{ tamanhos }}
            <div ng-show="cbPckContentSize === '0'" class="input-inline form-control form-control-280px">
                <input ng-required="cbPckContentSize === '0'" type="text" placeholder="Novo volume" ng-model="packageContentSpec" name="packageContentSpec" id="packageContentSpec" />
            </div>
            <div ng-show="cbPckContentSize === '0'" class="input-inline hint">
                <p>
                    <strong>Dica:</strong> Deixe sempre os números e unidade de medida juntos , sem espaço. Utilize espaço apenas 
                    para separar volumes compostos como: 12 x 350ml.
                </p>
            </div>
        </section>
        
        <section ng-show="(cbPckContentSize === '0' || cbPckContentSize !== '')" class="form-section clearfix">
            <hr class="form-section-spacer">
            <p><label>Item da embalagem</label></p>
            <div class="input-inline select-control form-control-280px">
                <select ng-model="cbPckTypeItem" name="cbPckTypeItem" id="cbPackageItem">
                    <option value="">Selecione o item da embalagem</option>
                    <option value="0">Cadastrar novo item</option>
                    <option ng-repeat="item in tiposItens" value="@{{item.id}}">@{{item.nome}}</option>
                </select>
            </div>
            <div ng-show="cbPckTypeItem === '0'" class="input-inline form-control form-control-280px">
                <input type="text" placeholder="Nomeie o item da embalagem" name="packageItemName" id="packageItemName" />
            </div>
            <div ng-show="cbPckTypeItem === '0'" class="input-inline hint">
                <p>
                    <strong>Dica:</strong> Uma embalagem pode ser composta por vários itens diferentes, produzidos por 
                    fornecedores diferentes. Utilize um nome que descreva objetivamente esse item.
                </p>
            </div>
        </section>
        
        <section ng-show="(cbPckContentSize === '0' || cbPckContentSize !== '') && variacoes.length" class="form-section clearfix">
            <hr class="form-section-spacer">
            <div class="text-block">
                <h4>Essa embalagem já foi cadastrada no sistema, atribua um nome de variação para diferenciá-la</h4>
                <p>
                    O sistema permite cadastrar várias embalagens iguais, desde que possuam nomes de variação diferentes. Identifique 
                    o que essa embalagem possui de diferente da(s) outra(s) e utilize isso como nome para essa variação. 
                </p>
            </div>
            <div class="input-inline form-control form-control-280px">
                <input ng-required="(cbPckContentSize === '0' || cbPckContentSize !== '') && variacoes.length" type="text" placeholder="Nome da variação" ng-model="variacaoName" name="variacaoName" id="variacaoName" />
            </div>
            <div class="input-inline hint">
                <p>
                    <strong>Dica:</strong> Cores, plantas técnicas ou fornecedores diferentes funcionam bem como nomes de variação.
                </p>
            </div>
        </section>
        
        <section ng-show="(cbPckContentSize === '0' || cbPckContentSize !== '') && !variacoes.length && !variacao_edicao" class="form-section clearfix">
            <hr class="form-section-spacer">
            <div class="text-block">
                <h4>A embalagem possui variações?</h4>
                <p>
                    É possível atribuir um nome de variação para a emabalegm. Caso queira cadastrar uma segunda ou terceira embalagem 
                    igual a esta, esse nome de variação será usado para diferenciar uma embalagem da outra. Se não tem certeza clique 
                    em “Não”.
                </p>
            </div>
            <div class="input-inline form-control-280px">
                <div class="form-inline-spacer form-inline">
                    <label class="radio-btn-control radio-btn-icon">    
                        <input type="radio" ng-model="packageHasVaritation" name="packageVariation" id="packageVariation" value="Nao" class="radio-btn-hidden" />
                        <span class="icon-cross">Não</label>
                    </label>
                </div>
                <div class="form-inline">
                    <label class="radio-btn-control radio-btn-icon">    
                        <input type="radio" ng-model="packageHasVaritation" name="packageVariation" id="packageVariation" value="Sim" class="radio-btn-hidden" />
                        <span class="icon-check">Sim</span>
                    </label>
                </div>
            </div>
            <div ng-show="packageHasVaritation == 'Sim'" class="input-inline form-control form-control-280px">
                <input ng-required="packageHasVaritation == 'Sim'" type="text" placeholder="Nome da variação" ng-model="variacaoName" name="variacaoName" id="variacaoName" />
            </div>
            <div ng-show="packageHasVaritation == 'Sim'" class="input-inline hint">
                <p>
                    <strong>Dica:</strong> Identifique o que essa embalagem possui de diferente da(s) outra(s) e utilize isso como 
                    nome para essa variação. Cores, plantas técnicas ou fornecedores diferentes funcionam bem.
                </p>
            </div>
        </section>
        
        @if (!$variacao->principal)        
        <section ng-show="variacao_edicao" class="form-section clearfix">
            <hr class="form-section-spacer">
            <div class="text-block">
                <h4>Nome de variação da embalagem</h4>
                <p>
                    É possível atribuir um nome de variação para a embalagem, assim o sistema permite cadastrar embalagens iguais 
                    diferenciando uma da outra.
                </p>
            </div>
            <div class="input-inline form-control form-control-280px">
                <input type="text" placeholder="Nomeie a variação da embalagem" ng-model="variacaoName" name="variacaoName" id="variacaoName" />
            </div>
            <div class="input-inline hint">
                <p>
                    <strong>Dica:</strong> Identifique o que essa embalagem possui de diferente da(s) outra(s) e utilize isso 
                    como nome para essa variação. Cores, plantas técnicas ou fornecedores diferentes funcionam bem como nomes 
                    de variação.
                </p>
            </div>
        </section>
        @endif
        
        <input type="hidden" ng-value="packageHasVaritation" name="packageVariation" />
        
        <section ng-show="
                    (packageType === 'Regular' || (packageType === 'Promocional' && ((cbPckCampaign !== '' && cbPckCampaign !== '0') || (cbPckCampaign === '0' && packageCampaignName))))
                    && ((cbPckFamily !== '0' && cbPckFamily !== '') || (cbPckFamily === '0' && packageFamilyName))
                    && ((cbNewCategory !== '0' && cbNewCategory !== '0') || (cbNewCategory === '0' && packageCategName))
                    && ((cbPckProduct !== '0' && cbPckProduct !== '') || (cbPckProduct === '0' && tfNewProductName))
                    && ((cbPckType !== '0' && cbPckType !== '') || (cbPckType === '0' && packagePackingName && natureza))
                    && ((cbPckContentSize !== '0' && cbPckContentSize !== '') || (cbPckContentSize === '0' && packageContentSpec))
                    && ((variacoes.length && variacaoName) || (!variacoes.length && ((packageHasVaritation == 'Sim' && variacaoName) || packageHasVaritation == 'Nao' || variacao_edicao) ) )
                         " class="form-section clearfix">
            <div class="form-btn-group">
                <hr class="form-section-spacer">
                <button type="submit" class="call-to-action-btn create-package-btn">
                    <span class="btn-icon icon-magic-wand"></span> Salvar Alterações
                </button>
            </div>
        </section>
    </form>    
</div>
@stop