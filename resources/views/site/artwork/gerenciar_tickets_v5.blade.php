@extends('site.site_template')

@section('title')
    Artwork - Gerenciar Tickets
@stop

@section('content')
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- DataTables -->
<script src="{{ asset('/js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/datatables/accent-neutralise.js') }}"></script>
<script src="{{ asset('/js/datatables/any-number.js') }}"></script>
<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-resource.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-datatables.min.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/gerenciar_tickets_mod.js') }}"></script>

{!! Breadcrumbs::renderIfExists('artwork.manager') !!}

<div ng-app="app" ng-controller="dtTicketsModCtrl" ng-cloak class="content content-fluid clearfix">
    <div class="workflow-container">
        <section class="ticket-manager no-margin-bottom padding-bottom-40">
            <h3 class="master-title">Gerenciar Tickets</h3>
            <form>
                <ul class="tabs clearfix">
                    <li><a @if ($tipo == 'minhas_tarefas')class="active"@endif href="{{ route('site.artwork.managerV5', ['minhas_tarefas']) }}">Minhas Tarefas</a></li>
                    <li><a @if ($tipo == 'todos')class="active"@endif href="{{ route('site.artwork.managerV5') }}">Todos os Tickets</a></li>
                    <li><a @if ($tipo == 'meus')class="active"@endif href="{{ route('site.artwork.managerV5', ['meus']) }}">Abertos por Mim</a></li>
                    <li><a @if ($tipo == 'cancelados')class="active"@endif href="{{ route('site.artwork.managerV5', ['cancelados']) }}">Cancelados</a></li>
                </ul>
                <div ng-controller="dtTicketsCtrl as ticket">
                    <table datatable="" dt-options="ticket.dtOptions" cellpading="0" cellspacing="0" id="dataTables_table_0" class="table-tickets-manager table-border table-hover animate">
                        <thead>
                            <tr>
                                <th align="left" width=""><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Tickets</small></span></th>
                                <th align="left" width="307"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10"></small></span></th>
                                <th align="left" width="102"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10"></small></span></th>
                                <th align="left" width="102"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10"></small></span></th>
                                <th align="left" width="102"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10"></small></span></th>
                                <th align="left" width="102"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10"></small></span></th>
                                <th align="left" width="102"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10"></small></span></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </form>
        </section>
    </div>
</div>

<script type="text/javascript">
    var app = angular.module('app', ['ngSanitize', 'ngResource', 'datatables', 'dtTicketsMod']);

    app.controller('dtTicketsCtrl', DtTicketsManagerCtrl);

    function DtTicketsManagerCtrl(DTOptionsBuilder, DTColumnDefBuilder, $scope, $rootScope, $compile, $resource, $http) {
        var dataTableBtnFilterTicketsDOM = '<div class="flex margin-left-auto">' +
            '<div class="dropdown dropdown-sort-by no-animate">' +    
                '<a href="javascript:;" class="dropdown-btn dropdown-toggle tooltip top fa fa-sliders"><span class="tooltip-box">Filtrar</span></a>' +
                '<div class="dropdown-list">' +
                    '<ul class="checkbox-group-filter-tickets">' +
                        '<li>' +
                            '<label class="dropdown-switch switch-control switch-control-flex animate">'+
                                '<input type="checkbox" name="chkFiltrarTickets[1][]" id="chkFiltroTicketsEmbalagem" value="Embalagem" />' +
                                '<span class="switch-toggle abs-right-offset"></span>' +
                                '<strong class="switch-txt switch-txt-icon fa fa-cube"><small class="txt-small txt-bold">Tickets de Embalagem</small></strong>' +
                            '</label>' +
                        '</li>' +
                        '<li>' +
                            '<label class="dropdown-switch switch-control switch-control-flex animate">'+
                                '<input type="checkbox" name="chkFiltrarTickets[1][]" id="chkFiltroTicketsCompartilhados" value="Compartilhado" />' +
                                '<span class="switch-toggle abs-right-offset"></span>' +
                                '<strong class="switch-txt switch-txt-icon fa fa-share-alt-square"><small class="txt-small txt-bold">Tickets de Compartilhadas</small></strong>' +
                            '</label>' +
                        '</li>' +
                        '<li>' +
                            '<label class="dropdown-switch switch-control switch-control-flex animate">'+
                                '<input type="checkbox" name="chkFiltrarTickets[1][]" id="chkFiltroTicketsMidia" value="Mídia" />' +
                                '<span class="switch-toggle abs-right-offset"></span>' +
                                '<strong class="switch-txt switch-txt-icon fa fa-newspaper-o"><small class="txt-small txt-bold">Tickets de Mídia</small></strong>' +
                            '</label>' +
                        '</li>' +
                        '<li>' +
                            '<label class="dropdown-switch switch-control switch-control-flex animate">'+
                                '<input type="checkbox" name="chkFiltrarTickets[1][]" id="chkFiltroTicketsAtrasados" value="Atrasado" />' +
                                '<span class="switch-toggle abs-right-offset"></span>' +
                                '<strong class="switch-txt switch-txt-icon fa fa-bomb"><small class="txt-small txt-bold">Tickets Atrasados</small></strong>' +
                            '</label>' +
                        '</li>' +
                        '<li>' +
                            '<label class="dropdown-switch switch-control switch-control-flex animate">'+
                                '<input type="checkbox" name="chkFiltrarTickets[1][]" id="chkFiltroTicketsPausados" value="Pausado" />' +
                                '<span class="switch-toggle abs-right-offset"></span>' +
                                '<strong class="switch-txt switch-txt-icon fa fa-pause"><small class="txt-small txt-bold">Tickets Pausados</small></strong>' +
                            '</label>' +
                        '</li>' +
                    '</ul>' +
                '</div>' +
            '</div>' +
        '</div>';
        
        var dataTableToolbarBtnDOM = '<div class="flex">' +
            '<div class="dropdown dropdown-sort-by no-animate">' +    
                '<a href="javascript:;" class="dropdown-btn dropdown-toggle tooltip top fa fa-sort-amount-asc"><span class="tooltip-box">Ordenar</span></a>' +
                '<div class="dropdown-list">' +
                    '<ul>' +
                        '<li><a href="javascript:;" class="sort-by-brand txt-bold"><span class="icon fa fa-diamond"></span> Ordenar por Marca</a></li>' +
                        '<li><a href="javascript:;" class="sort-by-start-date txt-bold"><span class="icon fa fa-calendar"></span> Ordenar por Data de Início</a></li>' +
                        '<li><a href="javascript:;" class="sort-by-deadline txt-bold"><span class="icon fa fa-calendar-check-o"></span> Ordenar por Prazo</a></li>' +
                        '<li><a href="javascript:;" class="sort-by-cycle txt-bold"><span class="icon fa fa-repeat"></span> Ordenar por Ciclo</a></li>' +
                        '<li><a href="javascript:;" class="sort-by-status txt-bold"><span class="icon fa fa-tasks"></span> Ordenar por Status</a></li>' +
                    '</ul>' +
                '</div>' +
            '</div>' +
        '</div>';
        
        var dataTableFilterByStatusDOM = '<div class="select-control">' +
            '<select id="filterTicketStatus">' +
                '<option selected="selected" value="">Filtrar por status</option>' +
                '@foreach ($status as $value)<option value="{{ $value->nome }}">{{ $value->nome }}</option>@endforeach' +
            '</select>' +
        '</div>';

        var table = $('#dataTables_table_0').DataTable({
            dom: '<"datatable-toolbar flexbox-container clearfix"' +
                '   <"datatable-filter-container flex flex-3-large"f>' +
                '   <"datatable-filter-container filter-by-status flex flex-3-large">' +
                '>' +
                '<"clearfix"' +
                '   <tr>' +
                '>' +
                '<"datatable-pagination flexbox-container flex-align-center clearfix"' +
                '   <"flex"<"select-control"l>><"flex flex-auto flex-vertical-center"p><"flex"i>' +
                '>',
            bAutoWidth: false,
            processing: true,
            serverSide: false,
            stateSave: true,
            pageLength: 25,
            lengthMenu: [
                [10, 25, 50, -1],
                ['Exibir 10 por página', 'Exibir 25 por página', 'Exibir 50 por página', 'Exibir tudo']
            ],
            language: {
                'url': '/js/datatables/language/Portuguese-Brasil.json'
            },
            ajax: '{!! route('site.artwork.services.tickets', [$tipo]) !!}',
            columns: [{
                data: 'nome_completo',
                name: 'nome_completo',
                render: function(data, type, full) {
                    var options = { year: 'numeric', month: '2-digit', day: '2-digit' };

                    html = ''
                    html += '<a href="'+ full.link +'" class="flex-wrap dt-column-wrap">';
                    html +=    '   <span class="dt-column-ico">';
                    
                    if (full.compartilhado) {
                        html +=    '    <i class="fa fa-share-alt-square"></i>';
                    } else if (!full.compartilhado) {
                        html +=    '    <i class="fa fa-cube"></i>';
                    }
                    
                    if (full.cancelado_em) {
                        html +=    '    <i class="fa fa-times-circle"></i>';
                    } else {
                        html +=    '    <i class="fa '+ full.icone +'"></i>';
                    }
                    
                    html +=    '   </span>';
                    html +=    '   <span class="dt-column-txt">';
                    html +=    '       <strong class="d-block txt-regular">' + full.nome_completo + '</strong>';
                    html +=    '       <small class="dt-secondary-info d-block">'; 
                    html +=    '           <em class="txt-small txt-regular no-italic txt-color-alt">Marca: <strong class="txt-small txt-regular txt-color-alt margin-right-10">'+ full.familia_nome +'</strong></em>';
                    html +=    '           <em class="txt-small txt-regular no-italic txt-color-alt">Início: <strong class="txt-small txt-regular txt-color-alt margin-right-10">'+ full.dt_inicio +'</strong></em>'; 
                        // TO-DO: Converter para formato DD/MM/YYYY
                    html +=    '           <em class="txt-small txt-regular no-italic txt-color-alt">Prazo: <strong class="txt-small txt-regular txt-color-alt margin-right-10">'+ full.dt_final +'</strong></em>'; 
                        // TO-DO: Acrescentar informação de "Ciclo" no serviço
                    html +=    '           <em class="txt-small txt-regular no-italic txt-color-alt">Ciclo: <strong class="txt-small txt-regular txt-color-alt margin-right-10">'+ full.ciclo +'</strong></em>';
                    html +=    '           <em class="txt-small txt-regular no-italic txt-color-alt">Status: <strong class="txt-small txt-regular txt-color-alt">'+ full.status_nome +'</strong></em>';
                    html +=    '       </small>';
                    html +=    '  </span>';
                    html +=    '</a>';
                                        
                    return html;
                }
            }, {
                orderable: false,
                data: null,
                name: '',
                render: function(data, type, full) {
                    if (type == 'display') {
                        html = '';
                        html += '<div class="dt-timeline-container flex-wrap no-animate">';
                        html += '   <div class="dt-timeline">';
                        html += '       <div class="event">';
                        /* Bolinhas grandes - Tarefas */
                        $.each(full.timeline_etapas, function(index, value) {
                            html += '<div class="dropdown dropdown-task" data-menu="306">';
                            html += '   <span class="tooltip">'+ value.nome +'</span>';
                            html += value.html;
                            html += '   <div class="dropdown-list dropdown-task-list dropdown-list-auto">';
                            html += '       <div ng-show="loading" class="loading lds-ring"><div></div><div></div><div></div><div></div></div>';
                            html += '       <a href="javascript:;" ng-show="!loading" class="arrow-up fa fa-angle-up"></a>';
                            html += '       <a href="javascript:;" ng-show="!loading" class="arrow-down fa fa-angle-down"></a>';
                            html += '   </div>';
                            /* Bolinhas pequenas - Aprovadores */
                            if (full.id_status == 3 && index == 'primeira-aprovacao' || full.id_status == 4 && index == 'demais-aprovacoes') {
                                html += '           <div class="sub">';
                                html += '               <ul>';
                                html += full.timeline_subetapas;
                                html += '               </ul>';
                                html += '           </div>';
                            }
                            /* ------------------------------ */
                            html += '   </div>';
                        });
                        /* -------------------------- */
                        html += '       </div>';
                        html += '   </div>';
                        html += '</div>';
                    }

                    return html;
                }
            }, {
                data: 'familia_nome',
                name: 'familia_nome',
                visible: false,
                render: function(data, type, full) {
                    return html = '<label>'+ full.familia_nome +'</label>';
                }

            }, {
                data: 'compartilhado',
                name: 'compartilhado',
                visible: false,
                render: function(data, type, full) {
                    if (full.compartilhado) {
                        return html = '<label>Compartilhado</label>';
                    } else {
                        return html = '<label>Embalagem</label>';
                    }
                }
            }, {
                data: 'ts_inicio',
                name: 'ts_inicio',
                visible: false,
                render: function(data, type, full) {
                    return html = '<label>'+ full.ts_inicio +'</label>';
                }
            }, {
                data: 'ts_final',
                name: 'ts_final',
                visible: false,
                render: function(data, type, full) {
                    return html = '<label>'+ full.ts_final +'</label>';
                }                
            }, {
                data: 'ciclo',
                name: 'ciclo',
                type: 'any-number',
                visible: false,
                render: function(data, type, full) {
                    return html = '<label>'+ full.ciclo +'</label>';
                }

            }, {
                data: 'status_ordem',
                name: 'status_ordem',
                type: 'any-number',
                visible: false,
                render: function(data, type, full) {
                    return html = '<label>'+ full.status_ordem +'</label>';
                }
            }],
            stateSaveParams: function (settings, data) {
                //Use this to remove order from being saved
                delete data.order;
                //Or use this to set the order     
                data.order = [[0, 'asc']];
                // Use this for remove column visible parameter from being saved
                for ( var i=0, ien=data.columns.length ; i<ien ; i++ ) {
                    delete data.columns[i].visible;
                }
            },
            createdRow: function (row, data, dataIndex) {
                // Recompiling so we can bind Angular directive to the DT
                //var compiledRow = $compile(angular.element(row))($scope);
                //compiledRow.contents();
                $compile(angular.element(row))($scope);
            },
            initComplete: function () {
                var dataTables_filter_search = '<i></i>';
                var dataTables_filter_cancel = '<i></i>';

                $('.dataTables_filter input[type="search"]').attr('placeholder', 'Buscar');
                $('.datatable-filter-container').find('.dataTables_filter').append(dataTables_filter_search);
                $('.datatable-filter-container').find('.dataTables_filter').append(dataTables_filter_cancel);
                $('.datatable-filter-container').find('.dataTables_filter i').addClass('dataTables_filter_search');
                $('.datatable-filter-container').find('.dataTables_filter i + i').addClass('dataTables_filter_cancel');
                $('.dataTable thead').show();
                $('.dataTables_filter').show();
                $('.datatable-toolbar').append(dataTableBtnFilterTicketsDOM);
                $('.datatable-toolbar').append(dataTableToolbarBtnDOM);
                $('.filter-by-status').append(dataTableFilterByStatusDOM);

                function dataTablesFilterCancelShowHide() {
                    if ($('.datatable-filter-container .dataTables_filter input[type="search"]').val() !== '') {
                        $('.dataTables_filter_search').hide();
                        $('.dataTables_filter_cancel').show();

                    } else {
                        $('.dataTables_filter_search').show();
                        $('.dataTables_filter_cancel').hide();
                
                        table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
                    }
                }

                $(document).on('click', '.datatable-filter-container .dataTables_filter_cancel', function () {
                    $('.dataTables_filter').find('input[type="search"]').val('');
                    $('.filter-by-status').find('select').val('');

                    dataTablesFilterCancelShowHide();
                });

                // Remove accented character from search input as well
                $(document).on('keyup change', '#dataTables_table_0_filter input[type="search"], #filterTicketStatus', function () {
                    table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();

                    dataTablesFilterCancelShowHide();
                });

                $(document).on('click', '.checkbox-group-filter-tickets input:checkbox', function () {
                    var $box = $(this);
                    
                    if($box.is(':checked')) {
                        var group = 'input:checkbox[name="'+ $box.attr('name') +'"]';
                        
                        $(group).prop('checked', false);
                        
                        $box.prop('checked', true);
                    } else {
                        $box.prop('checked', false);
                    }
                });
                                
                $(document).on('click', 'body', function (event) {
                    if (!$(event.target).closest('.dropdown-task').length) {
                        $('a.arrow-up').hide();
                        $('.dropdown-menu-ul').scrollTop($('.dropdown-menu-ul').scrollTop()-120);
                    }
                });
                                
                $(document).on('click', 'a.arrow-up', function() {
                    var dropdownMenuUl = $(this).parent('.dropdown-list').find('.dropdown-menu-ul');
                    
                    dropdownMenuUl.scrollTop(dropdownMenuUl.stop().animate({scrollTop: dropdownMenuUl.scrollTop()-120}, 200));

                    $('a.arrow-down').show();
                    
                    if (dropdownMenuUl.scrollTop() < 1) {
                        $('a.arrow-up').hide();
                    }
                });

                $(document).on('click', 'a.arrow-down', function(event) {
                    var dropdownMenuUl = $(this).parent('.dropdown-list').find('.dropdown-menu-ul');
                    
                    dropdownMenuUl.scrollTop(dropdownMenuUl.stop().animate({scrollTop: dropdownMenuUl.scrollTop()+120}, 200));
                    
                    $('a.arrow-up').show();

                    if(dropdownMenuUl.scrollTop() + dropdownMenuUl.innerHeight() >= dropdownMenuUl[0].scrollHeight) {
                        $('a.arrow-down').hide();
                    }
                });

                window.onkeyup = function (event) {
                    if (event.keyCode == 27) {
                        $('a.arrow-up').hide();
                        $('.dropdown-menu-ul').scrollTop($('.dropdown-menu-ul').scrollTop()-120);
                        $('.dropdown-menu-ul').parent('.dropdown-list').hide();
                        $('.dropdown-toggle').removeClass('active');
                    }
                }

                $rootScope.getDropdownMenuHeight = function() {
                    $('a.arrow-down').hide();
                    $('a.arrow-up').hide();
                    $('.dropdown-menu-ul').scrollTop(0);
                                    
                    if (angular.element('.dropdown-task-list').outerHeight() > 306) {
                        $('a.arrow-down').show();
                    
                        angular.element('.dropdown-task-list').parents('.dropdown').addClass('dropdown-scrollable');
                    } else {
                        angular.element('.dropdown-task-list').parents('.dropdown').removeClass('dropdown-scrollable');
                    } 
                };

                document.addEventListener('scroll', function (event) {
                    if (event.target.className === 'dropdown-menu-ul') { // or any other filtering condition        
                        if(event.target.scrollTop + event.target.clientHeight >= event.target.scrollHeight) {
                            $('a.arrow-down').hide();
                        } else {
                            $('a.arrow-up').show();
                            $('a.arrow-down').show();

                            if (event.target.scrollTop < 1) {
                                $('a.arrow-up').hide();
                            }
                        }
                    }
                }, true /*Capture event*/);
                
                $(document).on('click', 'body', function (event) {
                    if (!$(event.target).closest('.dropdown-task').length) {
                        $('a.arrow-up').hide();
                        $('.dropdown-menu-ul').scrollTop($('.dropdown-menu-ul').scrollTop()-120);
                    }
                });
            },
            fnDrawCallback: function (oSettings) {
                if (oSettings.aiDisplay.length <= 0) {
                    $('.dataTable thead tr').hide();
                    $('.dataTable thead tr th').hide();
                    $('.datatable-pagination').hide();
                    $('.dataTables_empty').addClass('fa-ticket');
                    $('.dataTables_empty').text('Nenhum ticket encontrado.');
                } else {
                    $('.dataTable thead tr').show();
                    $('.dataTable thead tr th').show();
                    $('.datatable-pagination').show();
                    $('.dataTables_empty').removeClass('fa-ticket');
                    $('.dataTables_empty').text('');
                }
            }
        });
        
        function sortByBrandNameAsc() {
            $('.dropdown-sort-by').find('.dropdown-toggle').removeClass('fa-sort-amount-desc').addClass('fa-sort-amount-asc');
            table.order([2, 'asc']).draw();
        }

        function sortByBrandNameDesc() {
            $('.dropdown-sort-by').find('.dropdown-toggle').removeClass('fa-sort-amount-asc').addClass('fa-sort-amount-desc');
            table.order([2, 'desc']).draw();
        }

        function sortByStartDateAsc() {
            $('.dropdown-sort-by').find('.dropdown-toggle').removeClass('fa-sort-amount-desc').addClass('fa-sort-amount-asc');
            table.order([3, 'asc']).draw();
        }

        function sortByStartDateDesc() {
            $('.dropdown-sort-by').find('.dropdown-toggle').removeClass('fa-sort-amount-asc').addClass('fa-sort-amount-desc');
            table.order([3, 'desc']).draw();
        }

        function sortByDeadlineAsc() {
            $('.dropdown-sort-by').find('.dropdown-toggle').removeClass('fa-sort-amount-desc').addClass('fa-sort-amount-asc');
            table.order([4, 'asc']).draw();
        }

        function sortByDeadlineDesc() {
            $('.dropdown-sort-by').find('.dropdown-toggle').removeClass('fa-sort-amount-asc').addClass('fa-sort-amount-desc');
            table.order([4, 'desc']).draw();
        }

        function sortByCycleAsc() {
            $('.dropdown-sort-by').find('.dropdown-toggle').removeClass('fa-sort-amount-desc').addClass('fa-sort-amount-asc');
            table.order([5, 'asc']).draw();
        }

        function sortByCycleDesc() {
            $('.dropdown-sort-by').find('.dropdown-toggle').removeClass('fa-sort-amount-asc').addClass('fa-sort-amount-desc');
            table.order([5, 'desc']).draw();
        }

        function sortByStatusNameAsc() {
            $('.dropdown-sort-by').find('.dropdown-toggle').removeClass('fa-sort-amount-asc').addClass('fa-sort-amount-asc');
            table.order([6, 'asc']).draw();
        }

        function sortByStatusNameDesc() {
            $('.dropdown-sort-by').find('.dropdown-toggle').removeClass('fa-sort-amount-desc').addClass('fa-sort-amount-desc');
            table.order([6, 'desc']).draw();
        }

        $(document).on('click', '.sort-by-brand', function() {
            var el = this;
            return (el.tog^=1) ? sortByBrandNameAsc() : sortByBrandNameDesc();
        });

        $(document).on('click', '.sort-by-start-date', function() {
            var el = this;
            return (el.tog^=1) ? sortByStartDateAsc() : sortByStartDateDesc();
        });

        $(document).on('click', '.sort-by-deadline', function() {
            var el = this;
            return (el.tog^=1) ? sortByDeadlineAsc() : sortByDeadlineDesc();
        });

        $(document).on('click', '.sort-by-cycle', function() {
            var el = this;
            return (el.tog^=1) ? sortByCycleAsc() : sortByCycleDesc();
        });

        $(document).on('click', '.sort-by-status', function() {
            var el = this;
            return (el.tog^=1) ? sortByStatusNameAsc() : sortByStatusNameDesc();
        });
                
        $(document).on('change', '#filterTicketStatus', function () {
            table.search(
                $('#filterTicketStatus').val()
            ).draw();
        });

        $(document).on('change', '.checkbox-group-filter-tickets input:checkbox', function() {
            if($(this).is(':checked')) {
                table.search(this.value).draw();
            } else {
                $('.datatable-filter-container .dataTables_filter input[type="search"]').val('');
                $('#dataTables_table_0').DataTable().search('').draw();
            }
        });
    }
</script>
@stop