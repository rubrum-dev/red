@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
@if (Session::has('message'))
    <script>
        myAlert('{{ Session::get('message') }}', 'success');
    </script>
@endif
<link rel="stylesheet" type="text/css" href="{{ asset('/css/angular/v-accordion.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{ asset('/css/angular/angular-confirm.css')}}" />
<script>
	var csrf_token = '{{ csrf_token() }}';
</script>
<script src="{{ asset('/js/angular/angular.min.js')}}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.min.js')}}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset('/js/angular/v-accordion.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset('/js/angular/controllers/embalagens.js') }}"></script>
<script src="{{ asset('/js/site/artwork/embalagens.js?v=1') }}"></script>

<input name="regular" type="hidden" value="{{ $tipos['regular'] }}">
<input name="promo" type="hidden" value="{{ $tipos['promo'] }}">

{!! Breadcrumbs::renderIfExists('artwork.embalagens', $family, $product) !!}

<div class="content" ng-app="app" ng-controller="EmbalagensCtrl">
    @if (count($errors) > 0)
    <div class="alert alert-danger" role="alert">
        <div class="alert-error">
            @foreach ($errors->all() as $error)
                {{ $error}}<br />
            @endforeach
        </div>
    </div>
    @endif
    <div class="top-head">
        <div class="top-inline top-packing-logo">
            {{ Html::image(asset($thumb), 'Logo', array('title' => '', 'alt' => 'Logo')) }}
        </div>
        <div class="top-inline top-head-title top-head-title-center top-head-no-float">
            <h2><span>{{ $produto->nome }}</span></h2>
        </div>
        <div class="top-inline top-nav-right">
            <ul>
                <li><a href="#"><span class="icon icon-rec"></span> Ativas</a></li>
                <li><a href="#"><span class="icon icon-ban-circle"></span> Descontinuadas</a></li>
            </ul>
        </div>
    </div>
    @if ($tipos['novo'])
    <div class="table-pack-container" id="embalagens-desenvolvimento">
        <table class="table-pack table-hover" border="0" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th width="750">Embalagens Novas</th>
                    <th width="100">Adicionar Itens</th>
                    <th width="140">Solicitar Criação</th>
                </tr>
            </thead>
            <tbody>
                <!-- Embalagens novas -->
                @php $i = 0; @endphp
                @foreach ($skus as $key => $sku)
                    @if($sku['variacao']->novo)
                        <tr @if($i>0)class="border-top-tr"@endif>
                            @php $i++; @endphp
                            <td width="580">
                                <span class="icon-item icon-play"></span> 
                                <div style="margin-left:23px;">
                                    <span class="sku-name-txt">{{ $sku['nome'] }}</span> 
                                    @if ($sku['variacao']->artwItens->count() < 2)
                                        <a href="{{ route('site.artwork.package.edit.get', $sku['variacao']->id) }}" class="link-editar-embalagem icon-settings"></a>
                                    @endif
                                </div>
                            </td>
                            <td width="100">
                                <a href="javascript:;" ng-show="!exibir_formulario[{{ $sku['variacao']->id }}]" ng-click="exibir_formulario[{{ $sku['variacao']->id }}] = 1" @if($sku['variacao']->artwItens && $sku['variacao']->artwItens->first()->id_tipo) onclick="addNewItem('Um item será criado para esta embalagem. É necessário atribuir um nome para descrever esse item.')" @else onclick="addNewItem('Essa embalagem é composta por um item. Para diferenciá-lo do novo item que deseja adicionar, atribua um nome para ele. Em seguida atribua um nome também para o novo item.')" @endif class="table-icon-td icon-copy-paste-document adicionar-item"></a>
                                <a href="javascript:;" ng-show="exibir_formulario[{{ $sku['variacao']->id }}]" ng-click="exibir_formulario[{{ $sku['variacao']->id }}] = null" class="table-icon-td icon-copy-paste-document adicionar-item"></a>
                            </td>
                            <td width="100">
                                @if( ($sku['variacao']->artwItens && $sku['variacao']->artwItens->first()->id_tipo) || $sku['variacao']->artwItens->first()->ticketsAbertos()->count() )
                                    <a class="table-icon-td icon-magic-wand icon-disabled"></a>
                                @else
                                    {{ HTML::linkRoute('site.artwork.ticket.create.get', '', array($sku['variacao']->artwItens->first()->id), array('class' => 'table-icon-td icon-magic-wand')) }}
                                @endif
                            </td>
                        </tr>
                        <!-- Itens -->
                        <tr>
                            <td colspan="4" class="table-pack-subgroup">
                                @if($sku['variacao']->artwItens && $sku['variacao']->artwItens->first()->id_tipo)
                                    <table  border="0" cellpadding="0" cellspacing="0">
                                        <tbody>
                                            @foreach($sku['variacao']->artwItens as $item)
                                                <tr class="border-top-dashed-odd">
                                                    <td width="650">
                                                        <span class="icon-subgroup icon-arrow-curve-right"></span><span class="subgroup-text-td">{{ $item->artwItemTipo->nome }}</span>
                                                        <a href="{{ route('site.artwork.package.edit.get', [$sku['variacao']->id, $item->id]) }}" class="link-editar-embalagem icon-settings"></a>
                                                    </td>
                                                    <td width="99">
                                                        @if( $item->ticketsAbertos()->count() )
                                                            <a class="table-icon-td icon-magic-wand icon-disabled"></a>
                                                        @else
                                                            {{ HTML::linkRoute('site.artwork.ticket.create.get', '', array($item->id), array('class' => 'table-icon-td icon-magic-wand')) }}
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @endif
                                <form class="ticket-alt-request-form" ng-show="exibir_formulario[{{ $sku['variacao']->id }}]" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token()}}">
                                    <input name="id_variacao" type="hidden" value="{{ $sku['variacao']->id }}">
                                    <table  border="0" cellpadding="0" cellspacing="0">
                                        <tbody>
                                            @if($sku['variacao']->artwItens && !$sku['variacao']->artwItens->first()->id_tipo)
                                                <input name="id_item_edicao" type="hidden" value="{{ $sku['variacao']->artwItens->first()->id }}">
                                                <tr class="table-add-item-group border-top-dashed-odd">
                                                    <td colspan="7" width="980" style="padding:8px 0px;">
                                                        <div class="form-inline">
                                                            <span class="icon-subgroup icon-arrow-curve-right"></span>
                                                            <div class="form-inline select-control add-item-input">
                                                                <select name="editarItemTipo" id="editarItemTipo" ng-model="existente_tipos[{{ $sku['variacao']->id }}]" class="input-required">
                                                                    <option value="">Selecione o Item</option>
                                                                    <option value="0">Outro</option>
                                                                    @foreach($itemTipos as $tipo)
                                                                        <option value="{{ $tipo->id }}">{{ $tipo->nome }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-inline add-item-input" ng-show="existente_tipos[{{ $sku['variacao']->id }}] == 0">
                                                                <input ng-class="{'input-required': existente_tipos[{{ $sku['variacao']->id }}] == 0}" placeholder="Nome do novo item" type="text" name="editarItemTipoAdicionar" id="editarItemTipoAdicionar" class="input-control" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endif
                                            <tr class="border-top-dashed-odd table-add-item-subgroup">
                                                <td colspan="7" width="980">
                                                    <div class="form-inline">
                                                        <span class="icon-subgroup icon-arrow-curve-right"></span>
                                                        <div class="form-inline select-control add-item-input">
                                                            <select name="novoItemTipo" id="novoItemTipo" ng-model="novo_tipos[{{ $sku['variacao']->id }}]" class="input-required">
                                                                <option value="">Selecione o Item</option>
                                                                <option value="0">Outro</option>
                                                                @foreach($itemTipos as $tipo)
                                                                    <option value="{{ $tipo->id }}">{{ $tipo->nome }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-inline add-item-input" ng-show="novo_tipos[{{ $sku['variacao']->id }}] == 0">
                                                            <input ng-class="{'input-required': novo_tipos[{{ $sku['variacao']->id }}] == 0}" placeholder="Nome do novo item" type="text" name="novoItemTipoAdicionar" id="novoItemTipoAdicionar" class="input-control" />
                                                        </div>
                                                        <button type="submit" class="btn-form">
                                                            <span class="btn-icon icon-check"></span> Salvar
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </form>
                            </td>
                        </tr>
                        <!-- Itens - FIM -->
                    @endif
                @endforeach
                <!-- Embalagens novas - FIM -->
            </tbody>
        </table>
    </div>
    @endif
    @if ($tipos['regular'])
        <div class="table-pack-container" id="embalagens-regulares">
            <hr />
            <table class="table-pack table-hover" border="0" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th width="472">Embalagens Regulares</th>
                        <th width="84">Ver PDF</th>
                        <th width="120">Adicionar Itens</th>
                        <th width="80">Histórico</th>
                        <th width="84">Enviar Arte</th>
                        <th width="140">Solicitar Alteração</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- Embalagens regulares -->
                    @php $i = 0; @endphp
                    @foreach ($skus as $key => $sku)
                        @if(!$sku['promo'] && !$sku['variacao']->novo)
                            <tr @if($i>0)class="border-top-tr"@endif>
                                @php $i++; @endphp
                                <td width="472">
                                    <span class="icon-item icon-play"></span>
                                    <div style="margin-left:23px;">
                                        @if ($sku['variacao']->artwItens->count() < 2)
                                            <a style="float:left;" target="_blank" @if($sku['variacao']->artwItens->first()->artwVersoes->last()) href="{{ asset($sku['variacao']->artwItens->first()->artwVersoes->last()->path) }}" @endif class="sku-name-txt">{{ $sku['nome'] }}</a>
                                            <a href="{{ route('site.artwork.package.edit.get', $sku['variacao']->id) }}" class="link-editar-embalagem icon-settings"></a>
                                        @else
                                            <a style="float:left;" target="_blank" class="sku-name-txt">{{ $sku['nome'] }}</a>
                                        @endif
                                    </div>
                                </td>
                                @if (!$sku['variacao']->artwItens->first()->id_tipo && !$sku['variacao']->artwItens->first()->ticketsAbertos()->count())
                                    @if($sku['variacao']->artwItens->first()->artwVersoes->last())
                                        <td width="84">
                                            @if ($sku['variacao']->artwItens->first()->artwVersoes->last()->numero !== 0 || (!Auth::user()->gerente && Auth::user()->id_adm_perfil !== 1))
                                                <a href="{{ asset($sku['variacao']->artwItens->first()->artwVersoes->last()->path) }}" target="_blank">Versão {{ $sku['variacao']->artwItens->first()->artwVersoes->last()->numero }}</a>
                                            @else
                                                <a href="javascript:;" ng-click="artwFileUpload({{ $sku['variacao']->id }})">Versão 0</a>
                                            @endif
                                        </td>
                                    @else
                                        <td width="84">
                                            @if (!Auth::user()->gerente && Auth::user()->id_adm_perfil == 1)
                                                <a href="javascript:;" ng-click="artwFileUpload({{ $sku['variacao']->id }})" class="no-version-text">Versão 0</a>
                                            @else
                                                <span class="no-version-text">Versão 0</span>
                                            @endif
                                        </td>
                                    @endif
                                @elseif (!$sku['variacao']->artwItens->first()->id_tipo && $sku['variacao']->artwItens->first()->novo)
                                    @if (!Auth::user()->gerente && Auth::user()->id_adm_perfil != 1)
                                        <td width="84"><span class="txt-new-package">Novo</span></td>
                                    @else
                                        <td width="84">
                                            <a href="javascript:;" ng-click="artwFileUpload({{ $sku['variacao']->id }})">Novo</a>
                                        </td>
                                    @endif
                                @else
                                    <td width="84"></td>
                                @endif
                                <td width="120">
                                    <a href="javascript:;" ng-show='!exibir_formulario[{{ $sku['variacao']->id }}]' ng-click='exibir_formulario[{{ $sku['variacao']->id }}] = 1' @if($sku['variacao']->artwItens && $sku['variacao']->artwItens->first()->id_tipo) onclick="addNewItem('Um item será criado para esta embalagem. É necessário atribuir um nome para descrever esse item.')" @else onclick="addNewItem('Essa embalagem é composta por um item. Para diferenciá-lo do novo item que deseja adicionar, atribua um nome para ele. Em seguida atribua um nome também para o novo item.')" @endif class="table-icon-td icon-copy-paste-document adicionar-item"></a>
                                    <a href="javascript:;" ng-show='exibir_formulario[{{ $sku['variacao']->id }}]' ng-click='exibir_formulario[{{ $sku['variacao']->id }}] = null' class="table-icon-td icon-copy-paste-document adicionar-item"></a>
                                </td>
                                <td width="80"><a @if($sku['variacao']->artwItens->first()->artwVersoes->last() && !$sku['variacao']->artwItens->first()->id_tipo) href="{{ route('site.artwork.history', [$sku['variacao']->artwItens->first()->artwVersoes->last()->id]) }}" @endif class="table-icon-td icon-compass-2 {{ ( !$sku['variacao']->artwItens->first()->artwVersoes->last() || $sku['variacao']->artwItens->first()->id_tipo ) ? 'icon-disabled' : '' }}"></a></td>
                                <td width="84"><a @if($sku['variacao']->artwItens->first()->artwVersoes->last() && !$sku['variacao']->artwItens->first()->id_tipo) href="{{ route('site.artwork.send', [$sku['variacao']->artwItens->first()->id]) }}" @endif class="table-icon-td icon-mail {{ ( !$sku['variacao']->artwItens->first()->artwVersoes->last() || $sku['variacao']->artwItens->first()->id_tipo ) ? 'icon-disabled' : '' }}"></a></td>
                                <td width="140">
                                    @if( ($sku['variacao']->artwItens && $sku['variacao']->artwItens->first()->id_tipo) || $sku['variacao']->artwItens->first()->ticketsAbertos()->count() )
                                        <a class="table-icon-td icon-options-settings icon-disabled"></a>
                                    @else
                                        {{ HTML::linkRoute('site.artwork.ticket.create.get', '', array($sku['variacao']->artwItens->first()->id), array('class' => 'table-icon-td icon-options-settings')) }}
                                    @endif
                                </td>
                            </tr>
                            <!-- Itens -->
                            <tr>
                                <td colspan="6" class="table-pack-subgroup">
                                    @if($sku['variacao']->artwItens && $sku['variacao']->artwItens->first()->id_tipo)
                                        <table  border="0" cellpadding="0" cellspacing="0" style="border-collapse:unset;">
                                            <tbody>
                                                @foreach($sku['variacao']->artwItens as $item)
                                                    <tr class="border-top-dashed-odd">
                                                        <td width="462">
                                                            @if ($item->artwVersoes->last())
                                                                <span class="icon-subgroup icon-arrow-curve-right"></span><span class="subgroup-text-td"><a target="_blank" href="{{ asset($item->artwVersoes->last()->path) }}">{{ $item->artwItemTipo->nome }}</a></span>
                                                            @else
                                                                <span class="icon-subgroup icon-arrow-curve-right"></span><span class="subgroup-text-td">{{ $item->artwItemTipo->nome }}</span>
                                                            @endif
                                                            <a href="{{ route('site.artwork.package.edit.get', [$sku['variacao']->id, $item->id]) }}" class="link-editar-embalagem icon-settings"></a>
                                                        </td>
                                                        @if( $item->artwVersoes->last() && !$item->ticketsAbertos()->count() )
                                                            @if ($item->artwVersoes->last()->numero !== 0 || (!Auth::user()->gerente && Auth::user()->id_adm_perfil !== 1))
                                                                <td width="74"><a href="{{ asset($item->artwVersoes->last()->path) }}" target="_blank">Versão {{ $item->artwVersoes->last()->numero }}</a></td>
                                                            @else
                                                                <td width="74"><a href="javascript:;" ng-click="artwFileUpload({{ $sku['variacao']->id }}, {{ $item->id }})">Versão 0</a></td>
                                                            @endif
                                                        @elseif( $item->novo )
                                                            <td width="74">
                                                                @if (!Auth::user()->gerente && Auth::user()->id_adm_perfil != 1)
                                                                    <span class="txt-new-package">Novo</span>
                                                                @else
                                                                    <a class="no-version-text" href="javascript:;" ng-click="artwFileUpload({{ $sku['variacao']->id }}, {{ $item->id }})">Novo</a>
                                                                @endif
                                                            </td>
                                                        @else
                                                            <td width="74">
                                                                @if (!Auth::user()->gerente && Auth::user()->id_adm_perfil == 1)
                                                                    <a href="javascript:;" ng-click="artwFileUpload({{ $sku['variacao']->id }}, {{ $item->id }})" class="no-version-text">Versão 0</a>
                                                                @else
                                                                    <span class="no-version-text">Versão 0</span>
                                                                @endif
                                                            </td>
                                                        @endif
                                                        <td width="110"></td>
                                                        <td width="70"><a @if($item->artwVersoes->last()) href="{{ route('site.artwork.history', [$item->artwVersoes->last()->id]) }}" @endif class="table-icon-td icon-compass-2 {{ !$item->artwVersoes->last() ? 'icon-disabled' : '' }}"></a></td>
                                                        <td width="74"><a @if($item->artwVersoes->last()) href="{{ route('site.artwork.send', [$item->id]) }}" @endif class="table-icon-td icon-mail {{ !$item->artwVersoes->last() ? 'icon-disabled' : '' }}"></a></td>
                                                        <td width="130">
                                                            @if( $item->ticketsAbertos()->count() )
                                                                @if( $item->novo )
                                                                    <a class="table-icon-td icon-magic-wand icon-disabled"></a>
                                                                @else
                                                                    <a class="table-icon-td icon-options-settings icon-disabled"></a>
                                                                @endif
                                                            @else
                                                                @if( $item->novo )
                                                                    {{ HTML::linkRoute('site.artwork.ticket.create.get', '', array($item->id), array('class' => 'table-icon-td icon-magic-wand')) }}
                                                                @else
                                                                    {{ HTML::linkRoute('site.artwork.ticket.create.get', '', array($item->id), array('class' => 'table-icon-td icon-options-settings')) }}
                                                                @endif
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                    <form class="ticket-alt-request-form" ng-show="exibir_formulario[{{ $sku['variacao']->id }}]" method="post">
                                        <input type="hidden" name="_token" value="{{ csrf_token()}}">
                                        <input name="id_variacao" type="hidden" value="{{ $sku['variacao']->id }}">
                                        <table  border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                @if($sku['variacao']->artwItens && !$sku['variacao']->artwItens->first()->id_tipo)
                                                    <input name="id_item_edicao" type="hidden" value="{{ $sku['variacao']->artwItens->first()->id }}">
                                                    <tr class="table-add-item-group table-pack-subgroup border-top-dashed-odd">
                                                        <td colspan="7" width="980" style="padding:8px 0px;">
                                                            <div class="form-inline">
                                                                <span class="icon-subgroup icon-arrow-curve-right"></span>
                                                                <div class="form-inline select-control add-item-input">
                                                                    <select name="editarItemTipo" id="editarItemTipo" ng-model="existente_tipos[{{ $sku['variacao']->id }}]" class="input-required">
                                                                        <option value="">Selecione o Item</option>
                                                                        <option value="0">Outro</option>
                                                                        @foreach($itemTipos as $tipo)
                                                                            <option value="{{ $tipo->id }}">{{ $tipo->nome }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="form-inline add-item-input" ng-show="existente_tipos[{{ $sku['variacao']->id }}] == 0">
                                                                    <input ng-class="{'input-required': existente_tipos[{{ $sku['variacao']->id }}] == 0}" placeholder="Nome do novo item" type="text" name="editarItemTipoAdicionar" id="editarItemTipoAdicionar" class="input-control" />
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endif
                                                <tr class="table-add-item-subgroup border-top-dashed-odd">
                                                    <td colspan="7" width="980" style="padding:8px 0px;">
                                                        <div class="form-inline">
                                                            <span class="icon-subgroup icon-arrow-curve-right"></span>
                                                            <div class="form-inline select-control add-item-input">
                                                                <select name="novoItemTipo" id="novoItemTipo" ng-model="novo_tipos[{{ $sku['variacao']->id }}]" class="input-required">
                                                                    <option value="">Selecione o Item</option>
                                                                    <option value="0">Outro</option>
                                                                    @foreach($itemTipos as $tipo)
                                                                        <option value="{{ $tipo->id }}">{{ $tipo->nome }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-inline add-item-input" ng-show="novo_tipos[{{ $sku['variacao']->id }}] == 0">
                                                                <input ng-class="{'input-required': novo_tipos[{{ $sku['variacao']->id }}] == 0}" placeholder="Nome do novo item" type="text" name="novoItemTipoAdicionar" id="novoItemTipoAdicionar" class="input-control" />
                                                            </div>
                                                            <button type="submit" class="btn-form">
                                                                <span class="btn-icon icon-check"></span> Salvar
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </td>
                            </tr>
                            <!-- Itens - FIM -->
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
    @if ($tipos['promo'])
        <div class="table-pack-container" id="embalagens-promocionais">
            <hr />
            <table class="table-pack table-hover">
                <thead>
                    <tr>
                        <th width="472">Embalagens Promocionais</th>
                        <th width="84">Ver PDF</th>
                        <th width="120">Adicionar Itens</th>
                        <th width="80">Histórico</th>
                        <th width="84">Enviar Arte</th>
                        <th width="140">Solicitar Alteração</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- Embalagens promocionais -->
                    @php $i = 0; @endphp
                    @foreach ($skus as $key => $sku)
                        @if($sku['promo'] && !$sku['variacao']->novo)
                            <tr @if($i>0)class="border-top-tr"@endif>
                                @php $i++; @endphp
                                <td width="472">
                                    <span class="icon-item icon-play"></span>
                                    <div style="margin-left:23px;">    
                                        @if ($sku['variacao']->artwItens->count() < 2)
                                            <a target="_blank" @if($sku['variacao']->artwItens->first()->artwVersoes->last()) href="{{ asset($sku['variacao']->artwItens->first()->artwVersoes->last()->path) }}" @endif class="sku-name-txt">{{ $sku['nome'] }}</a>
                                            <a href="{{ route('site.artwork.package.edit.get', $sku['variacao']->id) }}" class="link-editar-embalagem icon-settings"></a>
                                        @else
                                            <a target="_blank" class="sku-name-txt">{{ $sku['nome'] }}</a>
                                        @endif
                                    </div>
                                </td>
                                @if (!$sku['variacao']->artwItens->first()->id_tipo && !$sku['variacao']->artwItens->first()->ticketsAbertos()->count())
                                    @if($sku['variacao']->artwItens->first()->artwVersoes->last())
                                        <td width="84">
                                            @if ($sku['variacao']->artwItens->first()->artwVersoes->last()->numero !== 0 || (!Auth::user()->gerente && Auth::user()->id_adm_perfil !== 1))
                                                <a href="{{ asset($sku['variacao']->artwItens->first()->artwVersoes->last()->path) }}" target="_blank">Versão {{ $sku['variacao']->artwItens->first()->artwVersoes->last()->numero }}</a>
                                            @else
                                                <a href="javascript:;" ng-click="artwFileUpload({{ $sku['variacao']->id }})">Versão 0</a>
                                            @endif
                                        </td>
                                    @else
                                        <td width="84">
                                            @if (!Auth::user()->gerente && Auth::user()->id_adm_perfil == 1)
                                                <a href="javascript:;" ng-click="artwFileUpload({{ $sku['variacao']->id }})" class="no-version-text">Versão 0</a>
                                            @else
                                                <span class="no-version-text">Versão 0</span>
                                            @endif
                                        </td>
                                    @endif
                                @elseif (!$sku['variacao']->artwItens->first()->id_tipo && $sku['variacao']->artwItens->first()->novo)
                                    @if (!Auth::user()->gerente && Auth::user()->id_adm_perfil != 1)
                                        <td width="84"><span class="txt-new-package">Novo</span></td>
                                    @else
                                        <td width="84">
                                            <a href="javascript:;" ng-click="artwFileUpload({{ $sku['variacao']->id }})">Novo</a>
                                        </td>
                                    @endif
                                @else
                                    <td width="84"></td>
                                @endif
                                <td width="120">
                                    <a href="javascript:;" ng-show='!exibir_formulario[{{ $sku['variacao']->id }}]' ng-click='exibir_formulario[{{ $sku['variacao']->id }}] = 1' @if($sku['variacao']->artwItens && $sku['variacao']->artwItens->first()->id_tipo) onclick="addNewItem('Um item será criado para esta embalagem. É necessário atribuir um nome para descrever esse item.')" @else onclick="addNewItem('Essa embalagem é composta por um item. Para diferenciá-lo do novo item que deseja adicionar, atribua um nome para ele. Em seguida atribua um nome também para o novo item.')" @endif class="table-icon-td icon-copy-paste-document adicionar-item"></a>
                                    <a href="javascript:;" ng-show='exibir_formulario[{{ $sku['variacao']->id }}]' ng-click='exibir_formulario[{{ $sku['variacao']->id }}] = null' class="table-icon-td icon-copy-paste-document adicionar-item"></a>
                                </td>
                                <td width="80"><a @if($sku['variacao']->artwItens->first()->artwVersoes->last() && !$sku['variacao']->artwItens->first()->id_tipo) href="{{ route('site.artwork.history', [$sku['variacao']->artwItens->first()->artwVersoes->last()->id]) }}" @endif class="table-icon-td icon-compass-2 {{ ( !$sku['variacao']->artwItens->first()->artwVersoes->last() || $sku['variacao']->artwItens->first()->id_tipo ) ? 'icon-disabled' : '' }}"></a></td>
                                <td width="84"><a @if($sku['variacao']->artwItens->first()->artwVersoes->last() && !$sku['variacao']->artwItens->first()->id_tipo) href="{{ route('site.artwork.send', [$sku['variacao']->artwItens->first()->id]) }}" @endif class="table-icon-td icon-mail {{ ( !$sku['variacao']->artwItens->first()->artwVersoes->last() || $sku['variacao']->artwItens->first()->id_tipo ) ? 'icon-disabled' : '' }}"></a></td>
                                <td width="140">
                                    @if( ($sku['variacao']->artwItens && $sku['variacao']->artwItens->first()->id_tipo) || $sku['variacao']->artwItens->first()->ticketsAbertos()->count() )
                                        <a class="table-icon-td icon-options-settings icon-disabled"></a>
                                    @else
                                        {{ HTML::linkRoute('site.artwork.ticket.create.get', '', array($sku['variacao']->artwItens->first()->id), array('class' => 'table-icon-td icon-options-settings')) }}
                                    @endif
                                </td>
                            </tr>
                            <!-- Itens -->
                            <tr>
                                <td colspan="7" class="table-pack-subgroup">
                                    @if($sku['variacao']->artwItens && $sku['variacao']->artwItens->first()->id_tipo)
                                        <table  border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                @foreach($sku['variacao']->artwItens as $item)
                                                    <tr class="border-top-dashed-odd">
                                                        <td width="462">
                                                            @if ($item->artwVersoes->last())
                                                                <span class="icon-subgroup icon-arrow-curve-right"></span><span class="subgroup-text-td"><a target="_blank" href="{{ asset($sku['variacao']->artwItens->first()->artwVersoes->last()->path) }}">{{ $item->artwItemTipo->nome }}</a></span>
                                                            @else
                                                                <span class="icon-subgroup icon-arrow-curve-right"></span><span class="subgroup-text-td">{{ $item->artwItemTipo->nome }}</span>
                                                            @endif
                                                            <a href="{{ route('site.artwork.package.edit.get', [$sku['variacao']->id, $item->id]) }}" class="link-editar-embalagem icon-settings"></a>
                                                        </td>
                                                        @if( $item->artwVersoes->last() && !$item->ticketsAbertos()->count() )
                                                            @if ($item->artwVersoes->last()->numero !== 0 || (!Auth::user()->gerente && Auth::user()->id_adm_perfil !== 1))
                                                                <td width="74"><a href="{{ asset($item->artwVersoes->last()->path) }}" target="_blank">Versão {{ $item->artwVersoes->last()->numero }}</a></td>
                                                            @else
                                                                <td width="74"><a href="javascript:;" ng-click="artwFileUpload({{ $sku['variacao']->id }}, {{ $item->id }})">Versão 0</a></td>
                                                            @endif
                                                        @elseif( $item->novo )
                                                            <td width="74">
                                                                @if (!Auth::user()->gerente && Auth::user()->id_adm_perfil != 1)
                                                                    <span class="txt-new-package">Novo</span>
                                                                @else
                                                                    <a class="no-version-text" href="javascript:;" ng-click="artwFileUpload({{ $sku['variacao']->id }}, {{ $item->id }})">Novo</a>
                                                                @endif
                                                            </td>
                                                        @else
                                                            <td width="74">
                                                                @if (!Auth::user()->gerente && Auth::user()->id_adm_perfil == 1)
                                                                    <a href="javascript:;" ng-click="artwFileUpload({{ $sku['variacao']->id }}, {{ $item->id }})" class="no-version-text">Versão 0</a>
                                                                @else
                                                                    <span class="no-version-text">Versão 0</span>
                                                                @endif
                                                            </td>
                                                        @endif
                                                        <td width="110"></td>
                                                        <td width="70"><a @if($item->artwVersoes->last()) href="{{ route('site.artwork.history', [$item->artwVersoes->last()->id]) }}" @endif class="table-icon-td icon-compass-2 {{ !$item->artwVersoes->last() ? 'icon-disabled' : '' }}"></a></td>
                                                        <td width="74"><a @if($item->artwVersoes->last()) href="{{ route('site.artwork.send', [$item->id]) }}" @endif class="table-icon-td icon-mail {{ !$item->artwVersoes->last() ? 'icon-disabled' : '' }}"></a></td>
                                                        <td width="130">
                                                            @if( $item->ticketsAbertos()->count() )
                                                                <a class="table-icon-td icon-options-settings icon-disabled"></a>
                                                            @else
                                                                {{ HTML::linkRoute('site.artwork.ticket.create.get', '', array($item->id), array('class' => 'table-icon-td icon-options-settings')) }}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                    <form class="ticket-alt-request-form" ng-show="exibir_formulario[{{ $sku['variacao']->id }}]" method="post">
                                        <input type="hidden" name="_token" value="{{ csrf_token()}}">
                                        <input name="id_variacao" type="hidden" value="{{ $sku['variacao']->id }}">
                                        <table  border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                @if($sku['variacao']->artwItens && !$sku['variacao']->artwItens->first()->id_tipo)
                                                    <input name="id_item_edicao" type="hidden" value="{{ $sku['variacao']->artwItens->first()->id }}">
                                                    <tr class="table-add-item-group border-top-dashed-odd">
                                                        <td colspan="7" width="980" style="padding:8px 0px;">
                                                            <div class="form-inline">
                                                                <span class="icon-subgroup icon-arrow-curve-right"></span>
                                                                <div class="form-inline select-control add-item-input">
                                                                    <select name="editarItemTipo" id="editarItemTipo" ng-model="existente_tipos[{{ $sku['variacao']->id }}]" class="input-required">
                                                                        <option value="">Selecione o Item</option>
                                                                        <option value="0">Outro</option>
                                                                        @foreach($itemTipos as $tipo)
                                                                            <option value="{{ $tipo->id }}">{{ $tipo->nome }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="form-inline add-item-input" ng-show="existente_tipos[{{ $sku['variacao']->id }}] == 0">
                                                                    <input ng-class="{'input-required': existente_tipos[{{ $sku['variacao']->id }}] == 0}" placeholder="Nome do novo item" type="text" name="editarItemTipoAdicionar" id="editarItemTipoAdicionar" class="input-control" />
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endif
                                                <tr class="table-add-item-group border-top-dashed-odd">
                                                    <td colspan="7" width="980" style="padding:8px 0px;">
                                                        <div class="form-inline">
                                                            <span class="icon-subgroup icon-arrow-curve-right"></span>
                                                            <div class="form-inline select-control add-item-input">
                                                                <select name="novoItemTipo" id="novoItemTipo" ng-model="novo_tipos[{{ $sku['variacao']->id }}]" class="input-required">
                                                                    <option value="">Selecione o Item</option>
                                                                    <option value="0">Outro</option>
                                                                    @foreach($itemTipos as $tipo)
                                                                        <option value="{{ $tipo->id }}">{{ $tipo->nome }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-inline add-item-input" ng-show="novo_tipos[{{ $sku['variacao']->id }}] == 0">
                                                                <input ng-class="{'input-required': novo_tipos[{{ $sku['variacao']->id }}] == 0}" placeholder="Nome do novo item" type="text" name="novoItemTipoAdicionar" id="novoItemTipoAdicionar" class="input-control" />
                                                            </div>
                                                            <button type="submit" class="btn-form">
                                                                <span class="btn-icon icon-check"></span> Salvar
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </td>
                            </tr>
                            <!-- Itens - FIM -->
                        @endif
                    @endforeach
                    <!-- Embalagens promocionais - FIM -->
                </tbody>
            </table>
        </div>
    @endif
</div>
@stop
