@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

@if (Session::has('message'))
    <script>
        myAlert('{{ Session::get('message') }}', 'success');
    </script>
@endif

<script src="{{ asset('/js/angular/angular.min.js')}}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.min.js')}}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js')}}"></script>
<script src="{{ asset('/js/angular/controllers/embalagens.js')}}"></script>

<input name="regular" type="hidden" value="{{ $tipos['regular'] }}">
<input name="promo" type="hidden" value="{{ $tipos['promo'] }}">

{!! Breadcrumbs::renderIfExists('artwork.embalagens', $cat, $family, $product) !!}

<div class="content" ng-app="app" ng-controller="EmbalagensCtrl">
    
    @if (count($errors) > 0)
    <div class="alert alert-danger" role="alert">
        <div class="alert-error">
            @foreach ($errors->all() as $error)
            {{ $error}}<br />
            @endforeach
        </div>
    </div>
    @endif
    
    <div class="top-head">
        <div class="top-inline top-packing-logo">
            <a href="javascript:;" class="logo-down"><span class="logo-down-ico">Baixar Logo</span></a>
            {{ Html::image(asset($thumb), 'Logo', array('title' => '', 'alt' => 'Logo')) }}
        </div>
        <div class="top-inline top-head-title top-head-title-center">
            <h2><span>{{ $produto->nome }}</span></h2>
        </div>
        <div class="top-inline top-nav-right">
            <ul>
                <li><a href="#"><span class="icon icon-rec"></span> Ativas</a></li>
                <li><a href="#"><span class="icon icon-ban-circle"></span> Descontinuadas</a></li>
            </ul>
        </div>
    </div>
    
    @if ($tipos['novo'])
    <div class="table-pack-container" id="embalagens-desenvolvimento">
        <table class="table-pack" border="0" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th width="580">Embalagens em Desenvolvimento</th>
                    <th width="100">Adicionar Itens</th>
                    <th width="100">Solicitar Criação</th>
                    <th width="80">Excluir</th>
                </tr>
            </thead>
            <tbody>
                
                <!-- Embalagens novas -->
                @foreach ($skus as $key => $sku)
                @if($sku['variacao']->novo)
                <tr>
                    <td width="580"><a>&bull; {{ $sku['nome'] }}</a></td>
                    <td width="100">
                        <a href="javascript:;" ng-show='!exibir_formulario[{{ $sku['variacao']->id }}]' ng-click='exibir_formulario[{{ $sku['variacao']->id }}] = 1' @if($sku['variacao']->artwItens && $sku['variacao']->artwItens->first()->id_tipo) onclick="addNewItem('Um item será criado para esta embalagem. É necessário atribuir um nome para descrever esse item.')" @else onclick="addNewItem('Essa embalagem é composta por um item. Para diferenciá-lo do novo item que deseja adicionar, atribua um nome para ele. Em seguida atribua um nome também para o novo item.')" @endif class="table-icon-td icon-copy-paste-document adicionar-item"></a>
                        <a href="javascript:;" ng-show='exibir_formulario[{{ $sku['variacao']->id }}]' ng-click='exibir_formulario[{{ $sku['variacao']->id }}] = null' class="table-icon-td icon-copy-paste-document adicionar-item"></a>
                    </td>
                    <td width="100">
                        @if( ($sku['variacao']->artwItens && $sku['variacao']->artwItens->first()->id_tipo) || $sku['variacao']->artwItens->first()->ticketsAbertos()->count() )
                        <a class="table-icon-td icon-magic-wand icon-disabled"></a>
                        @else
                        {{ HTML::linkRoute('site.artwork.ticket.create.get', '', array($sku['variacao']->artwItens->first()->id), array('class' => 'table-icon-td icon-magic-wand')) }}
                        @endif
                    </td>
                    <td width="80"><a class="table-icon-td icon-cross icon-disabled"></a></td>
                </tr>
                
                <!-- Itens -->
                <tr>
                    <td colspan="4" class="table-pack-subgroup">
                        @if($sku['variacao']->artwItens && $sku['variacao']->artwItens->first()->id_tipo)
                        <table  border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                @foreach($sku['variacao']->artwItens as $item)
                                <tr>
                                    <td width="760"><a href="javascipt:;" class="subgroup-text-disabled"><span class="icon-subgroup icon-arrow-curve-right"></span><span class="subgroup-text-td">{{ $item->artwItemTipo->nome }}</span></a></td>
                                    <td width="120">
                                    @if( $item->ticketsAbertos()->count() )
                                        <a class="table-icon-td icon-magic-wand icon-disabled"></a>
                                    @else
                                        {{ HTML::linkRoute('site.artwork.ticket.create.get', '', array($item->id), array('class' => 'table-icon-td icon-magic-wand')) }}
                                    @endif
                                    </td>
                                    <td width="87"><a href="javascript:;" class="table-icon-td icon-cross icon-disabled"></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                        <form class="ticket-alt-request-form" ng-show="exibir_formulario[{{ $sku['variacao']->id }}]" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token()}}">
                            <input name="id_variacao" type="hidden" value="{{ $sku['variacao']->id }}">
                            <table  border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                    @if($sku['variacao']->artwItens && !$sku['variacao']->artwItens->first()->id_tipo)
                                    <input name="id_item_edicao" type="hidden" value="{{ $sku['variacao']->artwItens->first()->id }}">
                                    <tr>
                                        <td colspan="7" width="980">
                                            <div class="form-inline">
                                                <span class="icon-subgroup icon-arrow-curve-right"></span>
                                                <div class="form-inline select-control add-item-input">
                                                    <select required name="editarItemTipo" id="editarItemTipo" ng-model="existente_tipos[{{ $sku['variacao']->id }}]">
                                                        <option value="">Selecione o Item</option>
                                                        <option value="0">Outro</option>
                                                        @foreach($itemTipos as $tipo)
                                                            <option value="{{ $tipo->id }}">{{ $tipo->nome }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-inline add-item-input" ng-show="existente_tipos[{{ $sku['variacao']->id }}] == 0">
                                                    <input ng-required="existente_tipos[{{ $sku['variacao']->id }}] == 0" type="text" name="editarItemTipoAdicionar" id="editarItemTipoAdicionar" class="input-control" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endif
                                    <tr class="table-add-item-subgroup">
                                        <td colspan="7" width="980">
                                            <div class="form-inline">
                                                <span class="icon-subgroup icon-arrow-curve-right"></span>
                                                <div class="form-inline select-control add-item-input">
                                                    <select required name="novoItemTipo" id="novoItemTipo" ng-model="novo_tipos[{{ $sku['variacao']->id }}]">
                                                        <option value="">Selecione o Item</option>
                                                        <option value="0">Outro</option>
                                                        @foreach($itemTipos as $tipo)
                                                            <option value="{{ $tipo->id }}">{{ $tipo->nome }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-inline add-item-input" ng-show="novo_tipos[{{ $sku['variacao']->id }}] == 0">
                                                    <input ng-required="novo_tipos[{{ $sku['variacao']->id }}] == 0" type="text" name="novoItemTipoAdicionar" id="novoItemTipoAdicionar" class="input-control" />
                                                </div>
                                                <button type="submit" class="btn-form" onclick="submit()">
                                                    <span class="btn-icon icon-magic-wand"></span> Solicitar Criação
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </td>
                </tr>
                <!-- Itens - FIM -->
                
                @endif
                @endforeach
                <!-- Embalagens novas - FIM -->
                
            </tbody>
        </table>
    </div>
    @endif
    
    @if ($tipos['regular'])
    <div class="table-pack-container" id="embalagens-regulares">
        <hr />
        <table class="table-pack" border="0" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th width="300">Embalagens Regulares</th>
                    <th width="120">Ver PDF</th>
                    <th width="100">Adicionar Itens</th>
                    <th width="80">Histórico</th>
                    <th width="80">Enviar Arte</th>
                    <th width="130">Solicitar Alteração</th>
                    <th width="80">Descontinuar</th>
                </tr>
            </thead>
            <tbody>
                
                <!-- Embalagens regulares -->
                @foreach ($skus as $key => $sku)
                @if(!$sku['promo'] && !$sku['variacao']->novo)
                <tr>
                    <td width="300"><a target="_blank" @if($sku['variacao']->artwItens->first()->artwVersoes->last()) href="{{ asset($sku['variacao']->artwItens->first()->artwVersoes->last()->path) }}" @endif>&bull; {{ $sku['nome'] }}</a></td>
                    
                    @if (!$sku['variacao']->artwItens->first()->id_tipo && !$sku['variacao']->artwItens->first()->ticketsAbertos()->count())
                        @if($sku['variacao']->artwItens->first()->artwVersoes->last())
                            <td width="120"><a href="{{ asset($sku['variacao']->artwItens->first()->artwVersoes->last()->path) }}" target="_blank">Versão {{ $sku['variacao']->artwItens->first()->artwVersoes->last()->numero }}</a></td>
                        @else
                            <td width="120"><a style="color: #ff0000">Versão 0</a></td>
                        @endif
                    @elseif (!$sku['variacao']->artwItens->first()->id_tipo && $sku['variacao']->artwItens->first()->novo)
                        <td width="120">Em Desenvolvimento</td>
                    @else
                        <td width="120"></td>
                    @endif
                    
                    <td width="100">
                        <a href="javascript:;" ng-show='!exibir_formulario[{{ $sku['variacao']->id }}]' ng-click='exibir_formulario[{{ $sku['variacao']->id }}] = 1' @if($sku['variacao']->artwItens && $sku['variacao']->artwItens->first()->id_tipo) onclick="addNewItem('Um item será criado para esta embalagem. É necessário atribuir um nome para descrever esse item.')" @else onclick="addNewItem('Essa embalagem é composta por um item. Para diferenciá-lo do novo item que deseja adicionar, atribua um nome para ele. Em seguida atribua um nome também para o novo item.')" @endif class="table-icon-td icon-copy-paste-document adicionar-item"></a>
                        <a href="javascript:;" ng-show='exibir_formulario[{{ $sku['variacao']->id }}]' ng-click='exibir_formulario[{{ $sku['variacao']->id }}] = null' class="table-icon-td icon-copy-paste-document adicionar-item"></a>
                    </td>
                    <td width="80"><a @if($sku['variacao']->artwItens->first()->artwVersoes->last() && !$sku['variacao']->artwItens->first()->id_tipo) href="{{ route('site.artwork.history', [$sku['variacao']->artwItens->first()->artwVersoes->last()->id]) }}" @endif class="table-icon-td icon-compass-2 {{ ( !$sku['variacao']->artwItens->first()->artwVersoes->last() || $sku['variacao']->artwItens->first()->id_tipo ) ? 'icon-disabled' : '' }}"></a></td>
                    <td width="80"><a @if($sku['variacao']->artwItens->first()->artwVersoes->last() && !$sku['variacao']->artwItens->first()->id_tipo) href="{{ route('site.artwork.send', [$sku['variacao']->artwItens->first()->id]) }}" @endif class="table-icon-td icon-mail {{ ( !$sku['variacao']->artwItens->first()->artwVersoes->last() || $sku['variacao']->artwItens->first()->id_tipo ) ? 'icon-disabled' : '' }}"></a></td>
                    <td width="130">
                        @if( ($sku['variacao']->artwItens && $sku['variacao']->artwItens->first()->id_tipo) || $sku['variacao']->artwItens->first()->ticketsAbertos()->count() )
                        <a class="table-icon-td icon-options-settings icon-disabled"></a>
                        @else
                        {{ HTML::linkRoute('site.artwork.ticket.create.get', '', array($sku['variacao']->artwItens->first()->id), array('class' => 'table-icon-td icon-options-settings')) }}
                        @endif
                    </td>
                    <td width="80"><a class="table-icon-td icon-ban-circle icon-disabled"></a></td>
                </tr>
                
                <!-- Itens -->
                <tr>
                    <td colspan="7" class="table-pack-subgroup">
                        @if($sku['variacao']->artwItens && $sku['variacao']->artwItens->first()->id_tipo)
                        <table  border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                @foreach($sku['variacao']->artwItens as $item)
                                <tr>
                                    <td width="300"><a class="subgroup-text-disabled"><span class="icon-subgroup icon-arrow-curve-right"></span><span class="subgroup-text-td">{{ $item->artwItemTipo->nome }}</span></a></td>
                                    
                                    @if( $item->artwVersoes->last() && !$item->ticketsAbertos()->count() )
                                        <td width="120"><a href="{{ asset($item->artwVersoes->last()->path) }}" target="_blank">Versão {{ $item->artwVersoes->last()->numero }}</a></td>
                                    @elseif( $item->novo )
                                    <td width="120">Em Desenvolvimento</td>
                                    @else
                                        <td width="120"><a style="color: #ff0000">Versão 0</a></td>
                                    @endif
                                    
                                    <td width="100"></td>
                                    
                                    <td width="80"><a @if($item->artwVersoes->last()) href="{{ route('site.artwork.history', [$item->artwVersoes->last()->id]) }}" @endif class="table-icon-td icon-compass-2 {{ !$item->artwVersoes->last() ? 'icon-disabled' : '' }}"></a></td>
                                    <td width="80"><a @if($item->artwVersoes->last()) href="{{ route('site.artwork.send', [$item->id]) }}" @endif class="table-icon-td icon-mail {{ !$item->artwVersoes->last() ? 'icon-disabled' : '' }}"></a></td>
                                    
                                    <td width="130">
                                    @if( $item->ticketsAbertos()->count() )
                                        <a class="table-icon-td icon-options-settings icon-disabled"></a>
                                    @else
                                        {{ HTML::linkRoute('site.artwork.ticket.create.get', '', array($item->id), array('class' => 'table-icon-td icon-options-settings')) }}
                                    @endif
                                        
                                    </td>
                                    <td width="80"><a href="javascript:;" class="table-icon-td icon-ban-circle icon-disabled"></a></td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                        <form class="ticket-alt-request-form" ng-show="exibir_formulario[{{ $sku['variacao']->id }}]" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token()}}">
                            <input name="id_variacao" type="hidden" value="{{ $sku['variacao']->id }}">
                            <table  border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                    @if($sku['variacao']->artwItens && !$sku['variacao']->artwItens->first()->id_tipo)
                                    <input name="id_item_edicao" type="hidden" value="{{ $sku['variacao']->artwItens->first()->id }}">
                                    <tr>
                                        <td colspan="7" width="980">
                                            <div class="form-inline">
                                                <span class="icon-subgroup icon-arrow-curve-right"></span>
                                                <div class="form-inline select-control add-item-input">
                                                    <select required name="editarItemTipo" id="editarItemTipo" ng-model="existente_tipos[{{ $sku['variacao']->id }}]">
                                                        <option value="">Selecione o Item</option>
                                                        <option value="0">Outro</option>
                                                        @foreach($itemTipos as $tipo)
                                                            <option value="{{ $tipo->id }}">{{ $tipo->nome }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-inline add-item-input" ng-show="existente_tipos[{{ $sku['variacao']->id }}] == 0">
                                                    <input ng-required="existente_tipos[{{ $sku['variacao']->id }}] == 0" type="text" name="editarItemTipoAdicionar" id="editarItemTipoAdicionar" class="input-control" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endif
                                    <tr class="table-add-item-subgroup">
                                        <td colspan="7" width="980">
                                            <div class="form-inline">
                                                <span class="icon-subgroup icon-arrow-curve-right"></span>
                                                <div class="form-inline select-control add-item-input">
                                                    <select required name="novoItemTipo" id="novoItemTipo" ng-model="novo_tipos[{{ $sku['variacao']->id }}]">
                                                        <option value="">Selecione o Item</option>
                                                        <option value="0">Outro</option>
                                                        @foreach($itemTipos as $tipo)
                                                            <option value="{{ $tipo->id }}">{{ $tipo->nome }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-inline add-item-input" ng-show="novo_tipos[{{ $sku['variacao']->id }}] == 0">
                                                    <input ng-required="novo_tipos[{{ $sku['variacao']->id }}] == 0" type="text" name="novoItemTipoAdicionar" id="novoItemTipoAdicionar" class="input-control" />
                                                </div>
                                                <button type="submit" class="btn-form" onclick="submit()">
                                                    <span class="btn-icon icon-magic-wand"></span> Solicitar Criação
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </td>
                </tr>
                <!-- Itens - FIM -->
                
                @endif
                @endforeach
                <!-- Embalagens novas -->
                
                <!--tr>
                    <td colspan="7" class="table-pack-subgroup">
                        <table  border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td width="300"><a href="javascript:;"><span class="icon-subgroup icon-arrow-curve-right"></span><span class="subgroup-text-td">Front</span></a></td>
                                    <td width="120"><a href="javascript:;">Versão 2</a></td>
                                    <td width="100"></td>
                                    <td width="80"><a href="javascript:;" class="table-icon-td icon-compass-2"></a></td>
                                    <td width="80"><a href="javascript:;" class="table-icon-td icon-mail"></a></td>
                                    <td width="130"><a href="javascript:;" class="table-icon-td icon-options-settings"></a></td>
                                    <td width="80"><a href="javascript:;" class="table-icon-td icon-ban-circle"></a></td>
                                </tr>
                                <tr>
                                    <td width="300"><a href="javascript:;"><span class="icon-subgroup icon-arrow-curve-right"></span><span class="subgroup-text-td">Back</span></a></td>
                                    <td width="120"><a href="javascript:;">Em Desenvolvimento</a></td>
                                    <td width="100"></td>
                                    <td width="80"><a href="javascript:;" class="table-icon-td icon-compass-2"></a></td>
                                    <td width="80"><a href="javascript:;" class="table-icon-td icon-mail"></a></td>
                                    <td width="130"><a href="javascript:;" class="table-icon-td icon-options-settings"></a></td>
                                    <td width="80"><a href="javascript:;" class="table-icon-td icon-cross"></a></td>
                                </tr>
                                <tr class="table-add-item-subgroup">
                                    <td colspan="7" width="980">
                                        <form class="ticket-alt-request-form">
                                            <div class="form-inline">
                                                <span class="icon-subgroup icon-arrow-curve-right"></span>
                                                <div class="form-inline select-control add-item-input">
                                                    <select>
                                                        <option value="">Selecione o Item</option>
                                                        <option value="Outro">Outro</option>
                                                    </select>
                                                </div>
                                                <div class="form-inline add-item-input">
                                                    <input type="text" class="input-control" />
                                                </div>
                                                <button type="button" onclick="addNewItem('Eu sou um alerta!')" class="btn-form">
                                                    <span class="btn-icon icon-magic-wand"></span> Solicitar Criação
                                                </button>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="300"><a href="javascript:;">&bull; Skoll Pilsen Garrafa 355ml</a></td>
                                    <td width="120"><a href="javascript:;">Versão 11</a></td>
                                    <td width="100"><a href="javascript:;" onclick="addNewItem('Essa embalagem já possui um item, porém é necessário nomeá-lo. Nomeie esse item já existente e em seguida nomeie o novo item que deseja adicionar.')" class="table-icon-td icon-copy-paste-document adicionar-item-novo"></a></td>
                                    <td width="80"><a href="javascript:;" class="table-icon-td icon-compass-2"></a></td>
                                    <td width="80"><a href="javascript:;" class="table-icon-td icon-mail"></a></td>
                                    <td width="130"><a href="javascript:;" class="table-icon-td icon-options-settings"></a></td>
                                    <td width="80"><a href="javascript:;" class="table-icon-td icon-ban-circle"></a></td>
                                </tr>
                                <tr>
                                    <td width="300"><a href="javascript:;">&bull; Skoll Pilsen Garrafa 355ml</a></td>
                                    <td width="120"><a href="javascript:;">Versão 11</a></td>
                                    <td width="100"><a href="javascript:;" onclick="addNewItem('Essa embalagem já possui um item, porém é necessário nomeá-lo. Nomeie esse item já esxistente e em seguida nomeie o novo item que deseja adicionar.')" class="table-icon-td icon-copy-paste-document adicionar-item-novo"></a></td>
                                    <td width="80"><a href="javascript:;" class="table-icon-td icon-compass-2"></a></td>
                                    <td width="80"><a href="javascript:;" class="table-icon-td icon-mail"></a></td>
                                    <td width="130"><a href="javascript:;" class="table-icon-td icon-options-settings"></a></td>
                                    <td width="80"><a href="javascript:;" class="table-icon-td icon-ban-circle"></a></td>
                                </tr>
                                <tr class="table-add-new-item-subgroup">
                                    <td colspan="7" class="table-pack-subgroup">
                                        <form id="formAddNewItemEdit" class="ticket-alt-request-form">
                                            <table  border="0" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="7" width="980">
                                                            <div class="form-inline">
                                                                <span class="icon-subgroup icon-arrow-curve-right"></span>
                                                                <div class="form-inline add-item-input">
                                                                    <input type="text" name="newItemEdit" id="newItemEdit" placeholder="Nomeie o item existente" class="input-control" />
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7" width="980">
                                                            <div class="form-inline">
                                                                <span class="icon-subgroup icon-arrow-curve-right"></span>
                                                                <div class="form-inline select-control add-item-input">
                                                                    <select name="selectItemType" id="selectItemType">
                                                                        <option value="">Selecione o Item</option>
                                                                        <option value="Outro">Outro</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-inline add-item-input">
                                                                    <input type="text" name="newItemTypeEdit" id="newItemTypeEdit" class="input-control" />
                                                                </div>
                                                                <button type="button" id="creationRequest" class="btn-form">
                                                                    <span class="btn-icon icon-magic-wand"></span> Solicitar Criação
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </form>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr-->
                
                
            </tbody>
        </table>
    </div>
    @endif
    
    @if ($tipos['promo'])
    <div class="table-pack-container" id="embalagens-promocionais">
        <hr />
        <table class="table-pack">
            <thead>
                <tr>
                    <th width="300">Embalagens Promocionais</th>
                    <th width="120">Ver PDF</th>
                    <th width="100">Adicionar Itens</th>
                    <th width="80">Histórico</th>
                    <th width="80">Enviar Arte</th>
                    <th width="130">Solicitar Alteração</th>
                    <th width="80">Descontinuar</th>
                </tr>
            </thead>
            <tbody>
                
                <!-- Embalagens promocionais -->
                @foreach ($skus as $key => $sku)
                @if($sku['promo'] && !$sku['variacao']->novo)
                <tr>
                    <td width="300"><a target="_blank" @if($sku['variacao']->artwItens->first()->artwVersoes->last()) href="{{ asset($sku['variacao']->artwItens->first()->artwVersoes->last()->path) }}" @endif>&bull; {{ $sku['nome'] }}</a></td>
                    
                    @if (!$sku['variacao']->artwItens->first()->id_tipo && !$sku['variacao']->artwItens->first()->ticketsAbertos()->count())
                        @if($sku['variacao']->artwItens->first()->artwVersoes->last())
                            <td width="120"><a href="{{ asset($sku['variacao']->artwItens->first()->artwVersoes->last()->path) }}" target="_blank">Versão {{ $sku['variacao']->artwItens->first()->artwVersoes->last()->numero }}</a></td>
                        @else
                            <td width="120"><a style="color: #ff0000">Versão 0</a></td>
                        @endif
                    @elseif (!$sku['variacao']->artwItens->first()->id_tipo && $sku['variacao']->artwItens->first()->novo)
                        <td width="120">Em Desenvolvimento</td>
                    @else
                        <td width="120"></td>
                    @endif
                    
                    <td width="100">
                        <a href="javascript:;" ng-show='!exibir_formulario[{{ $sku['variacao']->id }}]' ng-click='exibir_formulario[{{ $sku['variacao']->id }}] = 1' @if($sku['variacao']->artwItens && $sku['variacao']->artwItens->first()->id_tipo) onclick="addNewItem('Um item será criado para esta embalagem. É necessário atribuir um nome para descrever esse item.')" @else onclick="addNewItem('Essa embalagem é composta por um item. Para diferenciá-lo do novo item que deseja adicionar, atribua um nome para ele. Em seguida atribua um nome também para o novo item.')" @endif class="table-icon-td icon-copy-paste-document adicionar-item"></a>
                        <a href="javascript:;" ng-show='exibir_formulario[{{ $sku['variacao']->id }}]' ng-click='exibir_formulario[{{ $sku['variacao']->id }}] = null' class="table-icon-td icon-copy-paste-document adicionar-item"></a>
                    </td>
                    <td width="80"><a @if($sku['variacao']->artwItens->first()->artwVersoes->last() && !$sku['variacao']->artwItens->first()->id_tipo) href="{{ route('site.artwork.history', [$sku['variacao']->artwItens->first()->artwVersoes->last()->id]) }}" @endif class="table-icon-td icon-compass-2 {{ ( !$sku['variacao']->artwItens->first()->artwVersoes->last() || $sku['variacao']->artwItens->first()->id_tipo ) ? 'icon-disabled' : '' }}"></a></td>
                    <td width="80"><a @if($sku['variacao']->artwItens->first()->artwVersoes->last() && !$sku['variacao']->artwItens->first()->id_tipo) href="{{ route('site.artwork.send', [$sku['variacao']->artwItens->first()->id]) }}" @endif class="table-icon-td icon-mail {{ ( !$sku['variacao']->artwItens->first()->artwVersoes->last() || $sku['variacao']->artwItens->first()->id_tipo ) ? 'icon-disabled' : '' }}"></a></td>
                    <td width="130">
                        @if( ($sku['variacao']->artwItens && $sku['variacao']->artwItens->first()->id_tipo) || $sku['variacao']->artwItens->first()->ticketsAbertos()->count() )
                        <a class="table-icon-td icon-options-settings icon-disabled"></a>
                        @else
                        {{ HTML::linkRoute('site.artwork.ticket.create.get', '', array($sku['variacao']->artwItens->first()->id), array('class' => 'table-icon-td icon-options-settings')) }}
                        @endif
                    </td>
                    <td width="80"><a class="table-icon-td icon-ban-circle icon-disabled"></a></td>
                </tr>
                
                <!-- Itens -->
                <tr>
                    <td colspan="7" class="table-pack-subgroup">
                        @if($sku['variacao']->artwItens && $sku['variacao']->artwItens->first()->id_tipo)
                        <table  border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                @foreach($sku['variacao']->artwItens as $item)
                                <tr>
                                    <td width="300"><a class="subgroup-text-disabled"><span class="icon-subgroup icon-arrow-curve-right"></span><span class="subgroup-text-td">{{ $item->artwItemTipo->nome }}</span></a></td>
                                    
                                    @if( $item->artwVersoes->last() && !$item->ticketsAbertos()->count() )
                                        <td width="120"><a href="{{ asset($item->artwVersoes->last()->path) }}" target="_blank">Versão {{ $item->artwVersoes->last()->numero }}</a></td>
                                    @elseif( $item->novo )
                                    <td width="120">Em Desenvolvimento</td>
                                    @else
                                        <td width="120"><a style="color: #ff0000">Versão 0</a></td>
                                    @endif
                                    
                                    <td width="100"></td>
                                    
                                    <td width="80"><a @if($item->artwVersoes->last()) href="{{ route('site.artwork.history', [$item->artwVersoes->last()->id]) }}" @endif class="table-icon-td icon-compass-2 {{ !$item->artwVersoes->last() ? 'icon-disabled' : '' }}"></a></td>
                                    <td width="80"><a @if($item->artwVersoes->last()) href="{{ route('site.artwork.send', [$item->id]) }}" @endif class="table-icon-td icon-mail {{ !$item->artwVersoes->last() ? 'icon-disabled' : '' }}"></a></td>
                                    
                                    <td width="130">
                                    @if( $item->ticketsAbertos()->count() )
                                        <a class="table-icon-td icon-options-settings icon-disabled"></a>
                                    @else
                                        {{ HTML::linkRoute('site.artwork.ticket.create.get', '', array($item->id), array('class' => 'table-icon-td icon-options-settings')) }}
                                    @endif
                                        
                                    </td>
                                    <td width="80"><a href="javascript:;" class="table-icon-td icon-ban-circle icon-disabled"></a></td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                        <form class="ticket-alt-request-form" ng-show="exibir_formulario[{{ $sku['variacao']->id }}]" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token()}}">
                            <input name="id_variacao" type="hidden" value="{{ $sku['variacao']->id }}">
                            <table  border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                    @if($sku['variacao']->artwItens && !$sku['variacao']->artwItens->first()->id_tipo)
                                    <input name="id_item_edicao" type="hidden" value="{{ $sku['variacao']->artwItens->first()->id }}">
                                    <tr>
                                        <td colspan="7" width="980">
                                            <div class="form-inline">
                                                <span class="icon-subgroup icon-arrow-curve-right"></span>
                                                <div class="form-inline select-control add-item-input">
                                                    <select required name="editarItemTipo" id="editarItemTipo" ng-model="existente_tipos[{{ $sku['variacao']->id }}]">
                                                        <option value="">Selecione o Item</option>
                                                        <option value="0">Outro</option>
                                                        @foreach($itemTipos as $tipo)
                                                            <option value="{{ $tipo->id }}">{{ $tipo->nome }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-inline add-item-input" ng-show="existente_tipos[{{ $sku['variacao']->id }}] == 0">
                                                    <input ng-required="existente_tipos[{{ $sku['variacao']->id }}] == 0" type="text" name="editarItemTipoAdicionar" id="editarItemTipoAdicionar" class="input-control" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endif
                                    <tr class="table-add-item-subgroup">
                                        <td colspan="7" width="980">
                                            <div class="form-inline">
                                                <span class="icon-subgroup icon-arrow-curve-right"></span>
                                                <div class="form-inline select-control add-item-input">
                                                    <select required name="novoItemTipo" id="novoItemTipo" ng-model="novo_tipos[{{ $sku['variacao']->id }}]">
                                                        <option value="">Selecione o Item</option>
                                                        <option value="0">Outro</option>
                                                        @foreach($itemTipos as $tipo)
                                                            <option value="{{ $tipo->id }}">{{ $tipo->nome }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-inline add-item-input" ng-show="novo_tipos[{{ $sku['variacao']->id }}] == 0">
                                                    <input ng-required="novo_tipos[{{ $sku['variacao']->id }}] == 0" type="text" name="novoItemTipoAdicionar" id="novoItemTipoAdicionar" class="input-control" />
                                                </div>
                                                <button type="submit" class="btn-form" onclick="submit()">
                                                    <span class="btn-icon icon-magic-wand"></span> Solicitar Criação
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </td>
                </tr>
                <!-- Itens - FIM -->
                
                @endif
                @endforeach
                <!-- Embalagens promocionais - FIM -->
                
            </tbody>
        </table>
    </div>
    @endif
    
</div>
@stop
