@extends('site.site_template')

@section('title')
    Artwork - Gerenciar Tickets
@stop

@section('content')
{{--<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/workflow.css') }}" />--}}
<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- DataTables -->
<script src="{{ asset('/js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/datatables/accent-neutralise.js') }}"></script>

{!! Breadcrumbs::renderIfExists('artwork.manager') !!}

<div class="content content-fluid clearfix">
    <div class="workflow-container">
        <section class="ticket-manager no-margin-bottom padding-bottom-40">
            <h3 class="master-title">Gerenciar Tickets</h3>
            <form>
                <ul class="tabs clearfix">
                    <li><a @if ($tipo == 'minhas_tarefas')class="active"@endif href="{{ route('site.artwork.managerV3', ['minhas_tarefas']) }}">Minhas Tarefas</a></li>
                    <li><a @if ($tipo == 'todos')class="active"@endif href="{{ route('site.artwork.managerV3') }}">Todos os Tickets</a></li>
                    <li><a @if ($tipo == 'meus')class="active"@endif href="{{ route('site.artwork.managerV3', ['meus']) }}">Abertos por Mim</a></li>
                    <li><a @if ($tipo == 'cancelados')class="active"@endif href="{{ route('site.artwork.managerV3', ['cancelados']) }}">Cancelados</a></li>
                </ul>
                <table cellpading="0" cellspacing="0" id="ticketsTable" class="table-border table-hover">
                    <thead>
                        <tr>
                            <th width=""><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Tickets</small></span></th>
                            <th width="240"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Status / Tarefa</small></span></th>
                            <th width="117"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Prazo</small></span></th>
                        </tr>
                    </thead>
                </table>
            </form>
        </section>
        <script>
            $(function () {
                var dataTableToolbarBtnDOM = '<div class="flex flex-auto">' +
                    '<a @if ($tipo == 'atrasadas') class="btn-function active" @endif href="{{ route('site.artwork.managerV3', ['atrasadas']) }}" class="btn-function"><i class="icon fa fa-bomb"></i> Atrasados</a>' +
                    '<a @if ($tipo == 'pausados') class="btn-function active" @endif href="{{ route('site.artwork.managerV3', ['pausados']) }}" class="btn-function"><i class="icon fa fa-pause"></i> Pausados</a>' +
                '</div>';
                var dataTableFilterByStatusDOM = '<div class="select-control">' +
                    '<select id="filterTicketStatus">' +
                        '<option selected="selected" value="">Filtrar por status</option>' +
                        '@foreach ($status as $value)<option value="{{ $value->nome }}">{{ $value->nome }}</option>@endforeach' +
                    '</select>' +
                '</div>';
                var table = $('#ticketsTable').DataTable({
                    dom: '<"datatable-toolbar flexbox-container clearfix"' +
                            '<"datatable-filter-container flex flex-3-large"f>' +
                            '<"datatable-filter-container filter-by-status flex flex-3-large">' +
                        '>' +
                        '<"clearfix"' +
                            '<tr>' +
                        '>' +
                        '<"datatable-pagination flexbox-container flex-align-center clearfix"' +
                            '<"flex"<"select-control"l>><"flex flex-auto flex-vertical-center"p><"flex"i>' +
                        '>',
                    bAutoWidth: false,
                    processing: true,
                    serverSide: false,
                    stateSave: true,
                    pageLength: 25,
                    lengthMenu: [
                        [10, 25, 50, -1], 
                        ['Exibir 10 por página', 'Exibir 25 por página', 'Exibir 50 por página', 'Exibir todos']
                    ],
                    ajax: '{!! route('site.artwork.services.tickets', [$tipo]) !!}',
                    language: {
                        'url': '/js/datatables/language/Portuguese-Brasil.json'
                    },
                    columns: [
                        {
                            data: 'nome_completo',
                            name: 'nome_completo',
                            render: function (data, type, full)
                            {
                                if (!full.compartilhado) {
                                    html = '<a href="'+ full.link +'">'
                                    + '<span class="icon fa fa-ticket"></span>'
                                    + '<span class="text-side">'+ full.nome_completo +'</span>'
                                    + '</a>';
                                    return html;
                                } else {
                                    html = '<a href="'+ full.link +'">'
                                    + '<span class="icon fa fa-share-alt-square"></span>'
                                    + '<span class="text-side">'+ full.nome_completo +'</span>'
                                    + '</a>';
                                    return html;
                                }
                            }
                        },
                        { 
                            data: 'status_nome',
                            name: 'status_nome',
                            render: function (data, type, full)
                            {
                                if (full.pausado_em) {
                                    html = '<a href="javascript:;" class="no-hover">' + 
                                    '<i class="icon txt-color-alt fa fa-pause"></i>' + 
                                    '<span class="text-side">' + data + '</span>' +
                                    '</a>';
                                }

                                if (full.cancelado_em) {
                                    html = '<a href="javascript:;" class="no-hover">' + 
                                    '<i class="icon txt-color-alt fa fa-times-circle"></i>' + 
                                    '<span class="text-side">' + data + '</span>' +
                                    '</a>';
                                }

                                if ((!full.pausado_em && !full.cancelado_em) && full.id_status === 1) {
                                    html = '<a href="javascript:;" class="no-hover">' + 
                                    '<i class="icon txt-color-alt fa fa-paint-brush"></i>' + 
                                    '<span class="text-side">' + data + '</span>' +
                                    '</a>';
                                }

                                if ((!full.pausado_em && !full.cancelado_em) && full.id_status === 2) {
                                    html = '<a href="javascript:;" class="no-hover">' + 
                                    '<i class="icon txt-color-alt fa fa-eye"></i>' + 
                                    '<span class="text-side">' + data + '</span>' +
                                    '</a>';
                                }

                                if ((!full.pausado_em && !full.cancelado_em) && full.id_status === 3) {
                                    html = '<a href="javascript:;" class="no-hover">' + 
                                    '<i class="icon txt-color-alt fa fa-thumbs-up"></i>' + 
                                    '<span class="text-side">' + data + '</span>' +
                                    '</a>';
                                }

                                if ((!full.pausado_em && !full.cancelado_em) && full.id_status === 4) {
                                    html = '<a href="javascript:;" class="no-hover">' + 
                                    '<i class="icon txt-color-alt fa fa-gavel"></i>' + 
                                    '<span class="text-side">' + data + '</span>' +
                                    '</a>';
                                }

                                if ((!full.pausado_em && !full.cancelado_em) && full.id_status === 5) {
                                    html = '<a href="javascript:;" class="no-hover">' + 
                                    '<i class="icon txt-color-alt fa fa-paint-brush"></i>' + 
                                    '<span class="text-side">' + data + '</span>' +
                                    '</a>';
                                }

                                if ((!full.pausado_em && !full.cancelado_em) && full.id_status === 6) {
                                    html = '<a href="javascript:;" class="no-hover">' + 
                                    '<i class="icon txt-color-alt fa fa-flag-chekered"></i>' + 
                                    '<span class="text-side">' + data + '</span>' +
                                    '</a>';
                                }

                                if ((!full.pausado_em && !full.cancelado_em) && full.id_status === 9) {
                                    html = '<a href="javascript:;" class="no-hover">' + 
                                    '<i class="icon txt-color-alt fa fa-flag"></i>' + 
                                    '<span class="text-side">' + data + '</span>' +
                                    '</a>';
                                }
                                
                                if ((!full.pausado_em && !full.cancelado_em) && full.id_status === 10) {
                                    html = '<a href="javascript:;" class="no-hover">' + 
                                    '<i class="icon txt-color-alt fa fa-paper-plane"></i>' + 
                                    '<span class="text-side">' + data + '</span>' +
                                    '</a>';
                                }
                                
                                if ((!full.pausado_em && !full.cancelado_em) && full.id_status === 11) {
                                    html = '<a href="javascript:;" class="no-hover">' + 
                                    '<i class="icon txt-color-alt fa fa-industry"></i>' + 
                                    '<span class="text-side">' + data + '</span>' +
                                    '</a>';
                                }
                                
                                if ((!full.pausado_em && !full.cancelado_em) && full.id_status === 12) {
                                    html = '<a href="javascript:;" class="no-hover">' + 
                                    '<i class="icon txt-color-alt fa fa-flag-checkered"></i>' + 
                                    '<span class="text-side">' + data + '</span>' +
                                    '</a>';
                                }
                                
                                if ((!full.pausado_em && !full.cancelado_em) && full.id_status === 0) {
                                    html = '<a href="javascript:;" class="no-hover">' + 
                                    '<i class="icon txt-color-alt fa fa-flag"></i>' + 
                                    '<span class="text-side">' + data + '</span>' +
                                    '</a>';
                                }

                                return html;
                            }
                        },
                        {
                            data: 'prazo', name: 'prazo',
                            render: function (data, type, full)
                            {
                                if (type == 'display') {
                                    
                                    
                                    if ('{{ $tipo }}' == 'cancelados'){
                                
                                    html = '<a href="javascript:;" class="no-hover">' + 
                                    '<span class="text-side default">' + full.prazo + '</span>' +
                                    '</a>';


                                    } else {

                                    html = '<a href="javascript:;" class="no-hover">' + 
                                    '<i class="'+ (full.status_nome == 'Pausado' ? 'icon txt-color-primary fa fa-pause' : (full.atrasado ? 'icon txt-color-primary fa fa-bomb' : '' ) ) +'"></i>' + 
                                    '<span class="text-side '+ (full.atrasado ? 'txt-color-primary' : 'default') +'">' + full.prazo + '</span>' +
                                    '</a>';

                                    }
                                
                                } else if (type == 'filter') {
                                
                                    return full.prazo
                                    
                                } else {
                                
                                    return full.prazo_timestamp
                                
                                }
                        
                                return html;
                            }
                        }
                    ],
                    initComplete: function () {
                        this.api().columns().every(function () {
                            var column = this;
                            var select = $('<select><option value=""></option></select>')
                                .appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );
                                    column
                                        .search(val ? '^'+val+'$' : '', true, false )
                                        .draw();
                                });
                            column.data().unique().sort().each(function (d,j) {
                                select.append('<option value="'+d+'">'+d+'</option>')
                            });
                        });
                        
                        $('#ticketsTable_filter input[type="search"]').attr('placeholder', 'Buscar');
                        
                        var dataTables_filter_search = '<i></i>';
                        var dataTables_filter_cancel = '<i></i>';
                        
                        $('.datatable-filter-container .dataTables_filter').append(dataTables_filter_search);
                        $('.datatable-filter-container .dataTables_filter').append(dataTables_filter_cancel);
                        $('.datatable-filter-container .dataTables_filter i').addClass('dataTables_filter_search');
                        $('.datatable-filter-container .dataTables_filter i + i').addClass('dataTables_filter_cancel');

                        function dataTablesFilterCancelShowHide() {
                            if ($('.datatable-filter-container .dataTables_filter input[type="search"]').val() !== '') {
                                $('.dataTables_filter_search').hide();
                                $('.dataTables_filter_cancel').show();
                            
                            } else {
                                $('.dataTables_filter_search').show();
                                $('.dataTables_filter_cancel').hide();
                                
                                table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
                            }
                        }

                        $(document).on('click', '.datatable-filter-container .dataTables_filter_cancel', function () {
                            $('#ticketsTable_filter input[type="search"]').val('');
                            
                            dataTablesFilterCancelShowHide();
                        });

                        // Remove accented character from search input as well
                        $(document).on('keyup change', '#ticketsTable_filter input[type="search"], #filterTicketStatus', function () {
                            table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
                            
                            dataTablesFilterCancelShowHide();
                        });

                        $('.dataTable thead').show();
                        $('.dataTables_filter').show();
                        $('.datatable-toolbar').append(dataTableToolbarBtnDOM);
                        $('.filter-by-status').append(dataTableFilterByStatusDOM);
                    },
                    fnDrawCallback: function( oSettings ) {
                        if(oSettings.aiDisplay.length <= 0) {
                            $('.dataTable thead').hide();
                            $('.datatable-pagination').hide();
                            $('.dataTables_empty').addClass('fa-ticket');
                            $('.dataTables_empty').text('Nenhum ticket encontrado.');

                            if ($('.dataTables_processing').is(':visible')) {
                                $('.dataTables_empty').removeClass('fa-ticket');
                                $('.dataTables_empty').text('');
                            }
                        } else {
                            $('.dataTable thead').show();
                            $('.datatable-pagination').show();
                            $('.dataTables_empty').removeClass('fa-ticket');
                            $('.dataTables_empty').text('');
                        }
                    }
                });

                $(document).on('focus', '.dataTables_filter input[type="search"]', function () {
                    $('.dataTables_filter i').css('color', '#4284F4');
                });

                $(document).on('blur', '.dataTables_filter input[type="search"]', function () {
                    $('.dataTables_filter i').css('color', '#004392');
                });

                @if ($tipo !== 'cancelados' && $tipo !== 'pausados')
                    $('.datatable-filter-container').append(dataTableFilterByStatusDOM);
                @endif
                                
                $(document).on('change', '#filterTicketStatus', function () {
                    table.search(
                        $('#filterTicketStatus').val()
                    ).draw();
                });
            });
        </script>
    </div>
</div>
@stop
