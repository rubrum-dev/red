@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
{!! Breadcrumbs::renderIfExists('artwork.nova_embalagem') !!}
<script src="{{ asset('/js/angular/angular.min.js')}}"></script>
<!--
<script src="{{ asset('/js/angular/angular-route.min.js')}}"></script>
<script src="{{ asset('/js/angular/angular-resource.min.js')}}"></script>
-->
<script src="{{ asset('/js/angular/angular-locale_pt-br.js')}}"></script>
<script src="{{ asset('/js/angular/controllers/package_create.js?v=1')}}"></script>
@if (count($errors) > 0)
<div class="alert alert-danger" role="alert">
    <div class="alert-error">
        @foreach ($errors->all() as $error)
        {{ $error}}<br />
        @endforeach
    </div>
</div>
@endif
@if (session('sucesso'))
<div class="alert alert-info">
    {{ session('sucesso')}}
</div>
@endif
<div class="content clearfix" ng-app="app" ng-controller="PackageCreateCtrl">
    <div class="top-head">
		<div class="top-inline top-head-title">
			<h2>Nova Embalagem</h2>
		</div>
	</div>
    <!-- 
    packageType: @{{packageType}} <br />
    packageIsNewProd: @{{packageIsNewProd}} <br />
    familias: @{{familias}} <br />
    cbPckFamily: @{{cbPckFamily}} <br />
    produtos: @{{produtos}} <br />
    cbPckProduct: @{{cbPckProduct}} <br />
    tipos: @{{tipos}} <br />
    tamanhos: @{{tamanhos}} <br />
    categorias: @{{categorias}} <br />
    cbNewCategory: @{{cbNewCategory}} <br />
    campanhas: @{{campanhas}} <br />
    cbPckCampaign: @{{cbPckCampaign}} <br />
    cbPckType: @{{cbPckType}} <br />
    cbPckContentSize: @{{cbPckContentSize}} <br />
    variacoes: @{{variacoes}} <br />
    variacaoName: @{{ variacaoName }} <br /> 
    -->
    <form name="newPackageForm" action="" method="POST" id="package-create-form">
        <input type="hidden" name="_token" value="{{ csrf_token()}}" />
        <section id="packageTypeSection" class="form-section  clearfix">
            <p><label>Qual a natureza da embalagem?</label></p>
            <div ng-show="packageType === '' || packageType === 'Regular'" ng-class="{'form-inline-spacer': packageType === ''}" class="form-inline">
                <input ng-model="packageType" type="radio" name="packageType" value="Regular" id="packageTypeRegular" ng-class="{'form-inline-spacer': packageType === ''}" class="radio-btn-hidden" />
                <label for="packageTypeRegular" class="label-regular radio-btn-fake icon-social-500px"><span>Regular</span></label>
            </div>
            <div ng-show="packageType === '' || packageType === 'Promocional'" class="form-inline form-inline-spacer">
                <input ng-model="packageType" type="radio" name="packageType" value="Promocional" id="packageTypePromo" class="radio-btn-hidden" />
                <label for="packageTypePromo" class="label-promo radio-btn-fake icon-dollar"><span>Promocional</span></label>
            </div>
        </section>
        <section ng-show="packageType" id="packageIsProdSection" class="form-section clearfix" style="display: block;">
            <hr class="form-section-spacer">
            <p><label>A nova embalagem é para um produto já cadastrado no sistema?</label></p>
            <div ng-show="packageIsNewProd === '' || packageIsNewProd === 'S'" ng-class="{'form-inline-spacer': packageIsNewProd === ''}" class="form-inline">
                <input ng-change="carregar_familias()" ng-model="packageIsNewProd" type="radio" value="S" name="packageIsNewProd" id="packageIsNewProd_Y" class="radio-btn-hidden">
                <label for="packageIsNewProd_Y" class="label-product-exists-y radio-btn-fake icon-check"><span>Sim</span></label>
            </div>
            <div ng-show="packageIsNewProd === '' || packageIsNewProd === 'N'" ng-class="{'form-inline-spacer': packageIsNewProd === ''}" class="form-inline">
                <input ng-change="carregar_categorias()" ng-model="packageIsNewProd" type="radio" value="N" name="packageIsNewProd" id="packageIsNewProd_N" class="radio-btn-hidden">
                <label for="packageIsNewProd_N" class="label-product-exists-n radio-btn-fake icon-cross"><span>Não</span></label>
            </div>
        </section>
        <section ng-show="packageIsNewProd === 'N'" id="packageIsProdSection_N" class="form-section clearfix" style="display: block;">
            <div class="form-group clearfix">
                <hr class="form-section-spacer">
                <p><label>Escreva o nome do novo produto</label></p>
                <div class="form-control form-control-220px">
                    <input type="text" placeholder="Exemplo: Skol Pilsen" name="tfNewProductName" id="tfNewProductName">
                </div>
            </div>
            <div ng-show="packageType === 'Promocional' && packageIsNewProd === 'N'" id="packageNewCampaignGroup">
                <div class="form-group clearfix">
                    <p><label>Escreva o nome da nova campanha</label></p>
                    <div class="input-inline">
                        <span class="form-control form-control-220px">
                            <input type="text" placeholder="Nome da campanha" name="packageCampaignName" id="packageCampaignName">
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group clearfix">
                <p><label>Selecione a categoria</label></p>
                <div class="input-inline">
                    <span class="select-control form-control-220px">
                        <select ng-change="carregar_familias()" ng-model="cbNewCategory" name="cbNewCategory" id="cbNewCategory">
                            <option value="" selected="" disabled="">Categoria</option>
                            <option value="0">Nova categoria...</option>
                            <option ng-repeat="categoria in categorias" value="@{{categoria.id}}">@{{categoria.nome}}</option>
                        </select>
                    </span>
                </div>
                <div ng-show="cbNewCategory === '0'" id="packageCategSpec" class="input-inline">
                    <span class="form-control form-control-220px">
                        <input type="text" placeholder="Nomeie a categoria" name="packageCategName" id="packageProdCategName">
                    </span>
                </div>
            </div>
            <div ng-show="cbNewCategory" id="familyGroup" style="display: block;">
                <div class="form-group clearfix">
                    <p><label>Selecione a família</label></p>
                    <div id="packageProdFamily" class="input-inline select-control form-control-220px">
                        <select ng-change="carregar_tipos()" ng-model="cbPckFamily" name="cbPckFamily" id="cbPckFamily" class="cb-pkg-family">
                            <option value="" selected="" disabled="">Família</option>
                            <option value="0">Nova Família...</option>
                            <option ng-repeat="familia in familias" value="@{{familia.id}}">@{{familia.nome}}</option>
                        </select>
                    </div>
                    <div ng-show="cbPckFamily === '0'" id="packageProdFamilyName" class="input-inline" style="display: inline-block;">
                        <span class="form-control form-control-220px">
                            <input type="text" placeholder="Nomeie a família" name="packageFamilyName" id="packageProdFamilyName">
                        </span>
                    </div>
                </div>
            </div>
            <div ng-show="cbPckFamily" id="packageTypeGroup" style="display: block;">
                <div class="form-group clearfix">
                    <p><label>Selecione o tipo de embalagem</label></p>
                    <div class="input-inline">
                        <span class="select-control form-control-220px">
                            <select ng-change="carregar_tamanhos()" ng-model="cbPckType" name="cbPckType" id="cbPckType" class="cb-pkg-type">
                                <option value="" selected="" disabled="">Tipo de embalagem</option>
                                <option value="0">Outro tipo de embalagem...</option>
                                <option ng-repeat="tipo in tipos" value="@{{tipo.id}}">@{{tipo.nome}}</option>
                            </select>
                        </span>
                    </div>
                    <div ng-show="packageIsNewProd === 'N' && cbPckType === '0'" id="packageNewPkgNameType" class="input-inline">
                        <span class="form-control form-control-220px">
                            <input type="text" placeholder="Nomeie o tipo da embalagem" ng-model="packagePackingName" name="packagePackingName" id="packagePackingName">
                        </span>
                    </div>
                </div>
                <div ng-show="cbPckType" id="packageNewContentSection" style="display: block;">
                    <div class="form-group clearfix">
                        <p><label>Selecione o volume</label></p>
                        <div ng-show="cbPckType !== '0'" id="pckContentSizeCtrl" class="input-inline">
                            <span class="select-control form-control-220px">
                                <select ng-disabled="cbPckType === '0'" ng-model="cbPckContentSize" name="cbPckContentSize" id="cbPckContentSize" class="cb-pkg-contents">
                                    <option value="" selected="" disabled="">Volume</option>
                                    <option value="0">Novo volume...</option>
                                    <option ng-repeat="tamanho in tamanhos" value="@{{tamanho.id}}">@{{tamanho.nome}}</option>
                                </select>
                            </span>
                        </div>
                        <div ng-show="cbPckType === '0' || cbPckContentSize === '0'" id="packageContentDescription" class="input-inline">
                            <span class="package-contents-control form-control form-control-220px">
                                <input type="text" placeholder="Especifique o volume" ng-model="packageContentSpec" name="packageContentSpec">
                            </span>
                        </div>
                    </div>
                    <div class="package-new-pkg-contents clearfix"></div>
                </div>
            </div>
        </section>
        <section ng-show="packageIsNewProd === 'S'" id="packageIsProdSection_Y" class="form-section clearfix" style="display: block;">
            <div class="form-group clearfix">
                <hr class="form-section-spacer">
                <p><label>Selecione a família</label></p>
                <div class="input-inline">
                    <span class="select-control form-control-220px">
                        <select ng-change="carregar_produtos()" ng-model="cbPckFamily" name="cbPckFamily" id="cbPckFamily">
                            <option value="" selected="" disabled="">Família</option>
                            <option ng-repeat="familia in familias" value="@{{familia.id}}">@{{familia.nome}}</option>
                        </select>
                    </span>
                </div>
                <div id="packageFamilySpec" class="input-inline"></div>
            </div>
            <!-- Combo de produtos, um para cada fluxo -->
            <div ng-show="cbPckFamily && packageType == 'Regular'" id="packageProductGroup" style="display: block;">
                <div class="form-group clearfix">
                    <p><label>Selecione o produto</label></p>
                    <div class="input-inline">
                        <span class="select-control form-control-220px">
                            <select ng-change="carregar_tipos()" ng-model="cbPckProduct" name="cbPckProduct" id="cbPckProduct">
                                <option value="" selected="selected" disabled="">Produto</option>
                                <option ng-repeat="produto in produtos" value="@{{produto.id}}">@{{produto.nome}}</option>
                            </select>
                        </span>
                    </div>
                    <div id="packageProdSpec" class="input-inline"></div>
                </div>
            </div>
            <div ng-show="cbPckFamily && packageType === 'Promocional'" id="packageProductGroup" style="display: block;">
                <div class="form-group clearfix">
                    <p><label>Selecione o produto</label></p>
                    <div class="input-inline">
                        <span class="select-control form-control-220px">
                            <select ng-change="carregar_campanhas()" ng-model="cbPckProduct" name="cbPckProduct" id="cbPckProduct">
                                <option value="" selected="selected" disabled="">Produto</option>
                                <option ng-repeat="produto in produtos" value="@{{produto.id}}">@{{produto.nome}}</option>
                            </select>
                        </span>
                    </div>
                    <div id="packageProdSpec" class="input-inline"></div>
                </div>
            </div>
            <!-- FIM / Combo de produtos, um para cada fluxo -->
            <div ng-show="packageType === 'Promocional' && cbPckProduct" id="packagePromoCampaignGroup" style="display: block;">
                <div class="form-group clearfix">
                    <p><label>Selecione a campanha</label></p>
                    <div class="input-inline">
                        <span class="select-control form-control-220px">
                            <select ng-change="carregar_tipos()" ng-model="cbPckCampaign" name="cbPckCampaign" id="cbPckCampaign">
                                <option value="" selected="" disabled="">Campanha</option>
                                <option value="0">Outro tipo de campanha...</option>
                                <option ng-repeat="campanha in campanhas" value="@{{campanha.id}}">@{{campanha.nome}}</option>
                            </select>
                        </span>
                    </div>
                    <div ng-show="cbPckCampaign === '0'" id="packageCampaignSpec" class="input-inline">
                        <span class="form-control form-control-220px">
                            <input ng-disabled="cbPckCampaign !== '0'" type="text" placeholder="Nomeie a campanha" name="packageCampaignName" id="packageCampaignName">
                        </span>
                    </div>
                </div>
            </div>
            <div ng-show="(packageType === 'Regular' && cbPckProduct) || (packageType === 'Promocional' && cbPckCampaign)" id="packageTypeContentSize" style="display: block;">
                <div class="form-group clearfix">
                    <p><label>Selecione o tipo de embalagem</label></p>
                    <div class="input-inline">
                        <span class="select-control form-control-220px">
                            <select ng-change="carregar_tamanhos()" ng-model="cbPckType" name="cbPckType" id="cbPckType">
                                <option value="" selected="selected" disabled="">Tipo de embalagem</option>
                                <option value="0">Outro tipo de embalagem</option>
                                <option ng-repeat="tipo in tipos" value="@{{tipo.id}}">@{{tipo.nome}}</option>
                            </select>
                        </span>
                    </div>
                    <div ng-show="packageIsNewProd === 'S' && cbPckType === '0'" id="packagePackingSpec" class="input-inline">
                        <span class="form-control form-control-220px">
                            <input type="text" placeholder="Nomeie o tipo da embalagem" ng-model="packagePackingName" name="packagePackingName" id="packagePackingName">
                        </span>
                    </div>
                </div>
                <div ng-show="cbPckCampaign || cbPckType" class="form-group clearfix">
                    <p><label>Selecione o volume</label></p>
                    <div ng-show="cbPckType !== '0'" id="pckOtherSizeCtrl" class="input-inline" style="display: inline-block;">
                        <span class="select-control form-control-220px">
                            <select ng-change="carregar_variacoes()" ng-disabled="cbPckType === '0'" ng-model="cbPckContentSize" name="cbPckContentSize" id="cbPckContentSize">
                                <option value="" selected="selected" disabled="">Volume</option>
                                <option value="0">Outro volume</option>
                                <option ng-repeat="tamanho in tamanhos" value="@{{tamanho.id}}">@{{tamanho.nome}}</option>
                            </select>
                        </span>
                    </div>
                    <div ng-show="(cbPckType === '0' || cbPckContentSize === '0')" id="packageContentSpec" class="input-inline">
                        <span class="package-contents-control form-control form-control-220px">
                            <input type="text" placeholder="Especifique o volume" ng-model="packageContentSpec" name="packageContentSpec">
                        </span>
                    </div>
                </div>
            </div>
        </section>
        <section ng-show="variacoes.length" class="clearfix" style="display: block;">
            <div class="form-group clearfix">
                <hr class="form-section-spacer">
                <p><label>Essa embalagem já existe. Dê um nome para diferenciá-la das outras variações</label></p>
                <div class="input-inline">
                    <span class="form-control form-control-220px">
                        <input type="text" placeholder="Nome da embalagem" ng-model="variacaoName" name="variacaoName" id="variacaoName">
                    </span>
                </div>
            </div>
        </section>
        <div ng-show="( (cbPckContentSize && packageIsNewProd == 'S' && !variacoes.length) || (cbPckContentSize && packageIsNewProd == 'S' && variacoes.length && variacaoName) )  || ((cbPckContentSize || cbPckType === '0') && packageIsNewProd == 'N')" id="createNewPackBtn" class="form-btn-group" style="display: block;">
            <hr class="form-section-spacer">
            <button type="submit" class="call-to-action-btn create-package-btn"><span class="btn-icon icon-magic-wand"></span> Cadastrar Nova Embalagem</button>
        </div>
        <div id="createNewPackBtn" class="form-btn-group"></div>
    </form>
</div>
@stop