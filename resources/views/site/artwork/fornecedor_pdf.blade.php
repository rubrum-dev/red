@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-confirm.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset2('/css/angular/angular-tooltips.min.css') }}" />

<script>
	var id_ticket = {{$ticket->id}};
	var csrf_token = '{{ csrf_token() }}';
</script>
<script src="{{ asset('/js/angular/angular.min.js')}}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js')}}"></script>
<script src="{{ asset2('/js/angular/controllers/embalagens_modal_compartilhar.js') }}"></script>
<script src="{{ asset('/js/angular/v-accordion.min.js')}}"></script>
<script src="{{ asset('/js/angular/angular-sanitize.min.js')}}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js')}}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload-shim.min.js') }}"></script>
<script src="{{ asset('/js/angular/ng-file-upload.min.js') }}"></script>
<script src="{{ asset2('/js/angular/angular-tooltips.min.js') }}"></script>
<script src="{{ asset2('/js/angular/enviar-arte-resolve-loader.js') }}"></script>
<script src="{{ asset2('/js/angular/pdf-fornecedor-resolve-loader.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edicao_aba_visao_geral.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edicao_aba_pdf_fornecedor.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/ticket_edicao.js')}}"></script>

{!! Breadcrumbs::renderIfExists('artwork.fornecedor_pdf', $variacao->id) !!}

<div class="content content-fluid" ng-app="app" ng-controller="TicketEdicaoCtrl" ng-cloak>
	<h3 class="master-title master-title-alt margin-top-10 margin-bottom-30 padding-bottom-20">Anexar PDF do Fornecedor</h3>
	<div class="artwork-container margin-bottom-40">
		
        @include('site.artwork.ticket_topo_compartilhado_inc')

		<div resolve-loader-2></div>
        
		<form class="fadein" ng-show="$root.loaded" ng-submit="$event.preventDefault()" name="formTicketEdit" action="" method="POST">
			
			<div ng-controller="abaPdfFornecedorCtrl" class="workflow-artwork-sender margin-left-10 margin-right-10">
				<div class="block margin-top-30" ng-class="{'no-margin-bottom': !ticket.fornecedorPdfs.length}">
					<h4 class="master-title-sub txt-small txt-light txt-uppercase margin-bottom-20">Pdfs dos Fornecedores</h4>
					<div class="flexbox-container flexbox-group" >
						<div data-title="Subir em padrão PDF/X-3 com fontes em curvas para evitar divergências na renderização." class="col-files-attach flex flex-fluid files-control attachPDF ui-tooltip top">
							<input type="file" accept="application/pdf" ng-model="pdf" name="file" id="pdfFileUpload" valid-file=".pdf" ngf-select ngf-model-invalid="errorFile" required />
							<span class="files-control-fake"><strong></strong><i class="addon fa fa-paperclip"></i></span>
						</div>
						<div class="flex flex-4-large col-select-supplier">
							<div class="select-control">
								<select ng-model="fornecedorSelecionado" name="cbSupplierChooser" id="cbSupplierChooser">
									<option value="">Selecionar o fornecedor</option>
									<option ng-repeat="(f, fornecedor) in fornecedores" value="@{{ fornecedor.id }}" ng-value="@{{ fornecedor.id }}">@{{ fornecedor.nome }}</option>
								</select>
							</div>
						</div>
						<div class="flex col-artwork-submit">   
							<button ng-click="upload_arquivo()" type="button" class="btn-control btn-positive">Adicionar</button>
						</div>
						<div ng-show="pdf.progress > 0" class="progress-circle animate">
							<svg id="svg" viewbox="0 0 100 100">
								<circle cx="50" cy="50" r="40" fill="none"/>
								<path fill="none" stroke-linecap="round" stroke-width="4" stroke-dasharray="@{{pdf.progress*2.5}},250.2" d="M50 10 a 40 40 0 0 1 0 80 a 40 40 0 0 1 0 -80"/>
								<text id="count" class="progress" x="50" y="50" text-anchor="middle" dy="7" ng-bind="pdf.progress + '%'"></text>
							</svg>
						</div>
					</div>
				</div>
				<div ng-show="!ticket.fornecedorPdfs.length" class="block is-empty-big margin-bottom-30">
					<span class="fa fa-industry">Nenhum PDF do fornecedor anexado.</span>
				</div>
				<div ng-repeat="(p, pdf) in ticket.fornecedorPdfs track by $index" class="workflow-supplier-card panel secondary">
					<div class="panel-body">
						<h4 class="panel-heading animate">
							<a target="_blank" ng-href="@{{ pdf.path }}">
								<i class="icon fa fa-file-pdf-o"></i>
								<strong class="txt-semibold">@{{ pdf.nome }}</strong>
								<small class="description-small txt-regular">Anexado por @{{ pdf.nome_usuario }}, @{{ pdf.data_criacao }}</small>
								<small class="description-small txt-regular no-margin">Arquivo original: @{{ pdf.nome_arquivo }} - @{{ pdf.filesize }} MB</small>
							</a>
						</h4>
						<div class="flex workflow-supplier-info margin-top-15">
							<i ng-class="{ 'fa-check': (pdf.status == 1), 'fa-industry': (pdf.status == 0) }" class="icon fa"></i>
							<div class="panel-body">
								<h4 class="panel-heading txt-semibold">Fornecedor • @{{ pdf.fornecedor }}</h4>
								<small ng-if="pdf.status == 0" class="description-small txt-regular">Aguardando aprovação</small>
								<small ng-if="pdf.status == 1" class="description-small txt-regular">Aprovado por @{{ pdf.nome_aprovacao }}, às @{{ pdf.data_aprovacao }}</small>
							</div>
						</div>
					</div>
					<div class="pull-right">
						<div class="block">
							<div class="dropdown drop-to-left pull-right">
								<a tooltips tooltip-append-to-body="true" tooltip-side="top" tooltip-smart="false" tooltip-size="small" tooltip-hide-trigger="click touchstart touchend mouseleave" tooltip-template="Opções" href="javascript:;" class="dropdown-btn dropdown-toggle fa fa-ellipsis-h"></a>
								<div class="dropdown-list">
									<ul>    
										<li ng-show="pdf.link_comparar"><a ng-href="@{{ pdf.link_comparar }}" target="_blank" class="txt-bold"><span class="icon fa fa-copy"></span>Comparar PDFs</a></li>
										<li><a target="_target" ng-href="@{{ pdf.baixarPdf }}" class="txt-bold"><span class="icon fa fa-download"></span>Baixar PDF</a></li>
                                        <li><a ng-class="{'disabled':(ticket.etapas[14] && ticket.envioFornecedorPdf && p == 0)}" href="javascript:;" ng-click="excluir_pdf(pdf.id)" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div ng-if="pdf.status == 0 && pdf.etapa == 2" class="block supplier-btn-approval">
							<button ng-click="reprovar_pdf(pdf.id)" type="button" class="btn-control btn-alt">Reprovar</button>
							<button ng-click="aprovar_pdf(pdf.id)" type="button" class="btn-control btn-positive">Aprovar</button>
						</div>
					</div>
				</div>
				<!-- END PDF's do Fornecedor -->
			</div>
		</form>
	</div>
</div>
@stop
