@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

<script src="{{ asset('/js/site/artwork/package-create.js') }}"></script>

@if (count($errors) > 0)
	<div class="alert alert-danger" role="alert">
		<div class="alert-error">
			@foreach ($errors->all() as $error)
				{{ $error }}<br />
			@endforeach
		</div>
	</div>
@endif

@if (session('sucesso'))
	<div class="alert alert-info">
		{{ session('sucesso') }}
	</div>
@endif

<div class="content clearfix">
	<div class="top-head">
		<div class="top-inline top-head-title">
			<h2>Nova Embalagem</h2>
		</div>
	</div>
	<form target="_blank" name="newPackageForm" action="" method="POST" id="package-create-form">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<section id="packageTypeSection" class="form-section  clearfix">
			<p><label>Qual a natureza da embalagem?</label></p>
			<div class="form-inline form-inline-spacer">
				<input type="radio" name="packageType" value="Regular" id="packageTypeRegular" class="radio-btn-hidden" />
				<label for="packageTypeRegular" class="label-regular radio-btn-fake icon-social-500px"><span>Regular</span></label>
			</div>
			<div class="form-inline form-inline-spacer">
				<input type="radio" name="packageType" value="Promocional" id="packageTypePromo" class="radio-btn-hidden" />
				<label for="packageTypePromo" class="label-promo radio-btn-fake icon-dollar"><span>Promocional</span></label>
			</div>
		</section>
		<section id="packageIsProdSection" class="form-section clearfix"></section>
		<section id="packageIsProdSection_Y" class="form-section clearfix"></section>
		<section id="packageIsProdSection_N" class="form-section clearfix"></section>
		<div id="createNewPackBtn" class="form-btn-group"></div>
	</form>
</div>
@stop
