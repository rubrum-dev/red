@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

<div class="content content-fluid">
	<div class="artwork-container">
		<section class="product-list padding-bottom-40 no-margin-bottom">
			<h3 class="flexbox-container flex-vertical-center master-title"><i class="icon fa fa-folder-open"></i>Download dos relatórios</h3>
			
			<div>
				<p><a target="_blank" href="/images/relatorios/{{$relatorio}}.xls" class="btn-function">Relatório resumido</a></p>
				<p><a target="_blank" href="/images/relatorios/{{$relatorio}}_detalhado.xls" class="btn-function">Relatório detalhado</a></p>
			</div>
			
		</section>
	</div>
</div>
@stop
