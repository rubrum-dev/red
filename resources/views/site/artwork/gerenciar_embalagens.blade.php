@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/site/datatables.css')}}" />
<link rel="stylesheet" type="text/css" href="{{ asset('/css/angular/angular-confirm.css')}}" />
<script src="{{ asset('/js/site/datatables.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-confirm.js') }}"></script>
<script src="{{ asset('/js/angular/angular-animate.min.js') }}"></script>
<script src="{{ asset('/js/angular/controllers/gerenciar_embalagens.js') }}"></script>
<script src="{{ asset('/js/site/artwork/manage-tickets-sorter.js') }}"></script>
<script src="{{ asset('/js/site/artwork/embalagens.js') }}"></script>
<script>
    $(function () {
        $('#myTable2').DataTable({
            lengthChange: false,
            info: false,
            paging: false
        });
    });
</script>
<div class="breadcumbs-out clearfix">		
    <div class="breadcrumbs-inner clearfix">	
        <ul>
            <li><a href="javascript:;">Início</a></li>
            <li class="active">Gerenciar Embalagens</li>
            <li class="breadcrumbs-back"><a href="javascript:;"><span class="icon icon-arrow-left-light"></span>Voltar</a></li>
        </ul>
    </div>
</div>
<div class="content" ng-app="app" ng-controller="GerenciarEmbalagensCtrl">
    <div class="top-head">
        <div class="top-inline top-head-title top-head-no-float">
            <h2><span>Gerenciar Embalagens</span></h2>
        </div>
        <div class="top-inline top-nav-right">
            <ul>
                <li><a href="javascript:;"><span class="icon icon-magic-wand"></span>Cadastrar Embalagem</a></li>
            </ul>
        </div>
    </div>
    <div class="package-container">
        <div class="sort-package-filter clearfix">
            <form name="" action="" method="POST" id="package-create-form">
                <div class="form-inline form-box-inline">
                    <div class="form-control search-control form-control-270px">
                        <input type="search" placeholder="Buscar" />
                        <button type="submit" class="btn-search icon-search"></button>
                    </div>
                </div>
                <div class="form-inline form-box-inline">
                    <div class="select-control form-control-270px">
                        <select name="" id="">
                            <option value="">Filtrar por família</option>
                        </select>
                    </div>
                </div>
                <div class="form-inline form-box-inline">
                    <div class="select-control form-control-270px">
                        <select name="" id="">
                            <option value="">Filtrar por categoria</option>
                        </select>
                    </div>
                </div>
                <div class="form-inline form-box-inline">
                    <div class="dropdown">
                        <a href="javascript:;" class="dropdown-btn filter-settings-btn icon-three-points"></a>
                        <div class="dropdown-menu">
                            <h5>Opções de Filtro</h5>
                            <ul>
                                <li>
                                    <div class="dropdown-item clearfix">
                                        <label class="switch-control full-width">
                                            <input type="checkbox" name="" />
                                            <span class="switch-toggle pull-right"></span>
                                            <span class="switch-txt">Regulares</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="dropdown-item clearfix">
                                        <label class="switch-control full-width">
                                            <input type="checkbox" name="" />
                                            <span class="switch-toggle pull-right"></span>
                                            <span class="switch-txt">Promocionais</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="dropdown-item clearfix">
                                        <label class="switch-control full-width">
                                            <input type="checkbox" name="" />
                                            <span class="switch-toggle pull-right"></span>
                                            <span class="switch-txt">Primárias</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="dropdown-item clearfix">
                                        <label class="switch-control full-width">
                                            <input type="checkbox" name="" />
                                            <span class="switch-toggle pull-right"></span>
                                            <span class="switch-txt">Secundárias</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="dropdown-item clearfix">
                                        <label class="switch-control full-width">
                                            <input type="checkbox" name="" />
                                            <span class="switch-toggle pull-right"></span>
                                            <span class="switch-txt">Terciárias</span>
                                        </label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="table-pack-container">
            <hr />
            <table border="0" cellpadding="0" cellspacing="0" id="myTable2" class="sort-tickets-table table-pack table-hover">
                <thead>
                    <tr>
                        <th width="740"><a href="javascript:;" class="sortable-th txt-uppercase txt-small txt-book">Embalagens</a></th>
                        <th width="80"><small class="txt-uppercase txt-small txt-book">Packshelf</small></th>
                        <th width="80"><small class="txt-uppercase txt-small txt-book">Artwork</small></th>
                        <th width="50"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="embalagem in embalagens" class="table-pack-group">
                        <td width="720">
                            <p class="sku-name-txt text-no-pointer no-margin">
                                <label>
                                    <span class="icon-item icon-rec"></span> 
                                    <span style="margin-left:23px;">@{{ embalagem.name }} <small class="txt-lang txt-uppercase">@{{ embalagem.language }}</small></span>
                                </label>
                            </p>
                        </td>
                        <td width="80">
                            <label class="switch-control">
                                <input type="checkbox" name="swPackShelf" ng-model="swPackShelf" />
                                <span class="switch-toggle"></span>
                            </label>
                        </td>
                        <td width="80">
                            <label class="switch-control">
                                <input type="checkbox" name="swArtwork" ng-model="swArtwork" />
                                <span class="switch-toggle"></span>
                            </label>
                        </td>
                        <td width="50">
                            <div class="dropdown">
                                <a href="javascript:;" class="dropdown-btn table-icon-td icon-three-points"></a>
                                <div class="dropdown-menu">
                                    <h5>Opções da Embalagem</h5>
                                    <ul>
                                        <li><a href="javascript:;"><span class="dropdown-item-ico icon-officine"></span>Alterar Embalagem</a></li>
                                        <li><a href="javascript:;"><span class="dropdown-item-ico icon-cross"></span>Excluir</a></li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop