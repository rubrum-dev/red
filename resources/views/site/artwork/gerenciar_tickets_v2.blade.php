@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
<link rel="stylesheet" href="{{ asset('/css/datatables/jquery.dataTables.min.css') }}">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- DataTables -->
<script src="{{ asset('/js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/datatables/accent-neutralise.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/site/artwork/manage-tickets-sorter.js?v=15') }}"></script>

{!! Breadcrumbs::renderIfExists('artwork.manager') !!}

<div class="content clearfix">
    <div class="top-head">
        <div class="top-inline top-head-title no-padding">
		    <h2 class="text-left"><span>Gerenciador de tickets</span></h2>
        </div>
    </div>
    <div class="sort-tickets-container clearfix">
        <div class="sort-tickets-status">
            <a @if ($tipo == 'minhas_tarefas')class="active"@endif href="{{ route('site.artwork.manager', ['minhas_tarefas']) }}">{{--<span>{{ count($minhas) }}</span>--}}<span>Minhas Tarefas</span><span class="icon icon-speed"></span></a>
        </div>
        <div class="sort-tickets-status">
            <a @if ($tipo == 'todos')class="active"@endif href="{{ route('site.artwork.manager') }}">{{--<span>{{ count($minhas) }}</span>--}}<span>Todos os Tickets</span><span class="icon icon-mailbox"></span></a>
        </div>
        <div class="sort-tickets-status">
            <a @if ($tipo == 'meus')class="active"@endif href="{{ route('site.artwork.manager', ['meus']) }}">{{--<span>{{ count($minhas) }}</span>--}}<span>Meus Tickets</span><span class="icon icon-credit-card"></span></a>
        </div>
        <div class="sort-tickets-status">
            <a @if ($tipo == 'atrasadas')class="active"@endif href="{{ route('site.artwork.manager', ['atrasadas']) }}">{{--<span>{{ count($atrasadas) }}</span>--}}<span>Atrasados</span><span class="icon icon-alarm-clock"></span></a>
        </div>
        <div class="sort-tickets-status">
            <a @if ($tipo == 'pausados')class="active"@endif href="{{ route('site.artwork.manager', ['pausados']) }}">{{--<span>{{ count($minhas) }}</span>--}}<span>Pausados</span><span class="icon icon-pause"></span></a>
        </div>
        <div class="sort-tickets-status">
            <a @if ($tipo == 'cancelados')class="active"@endif href="{{ route('site.artwork.manager', ['cancelados']) }}">{{--<span>{{ count($minhas) }}</span>--}}<span>Cancelados</span><span class="icon icon-button-close"></span></a>
        </div>
    </div>
    <form name="sortTicketsByStatus" action="" method="POST" class="ticket-alt-request-form">
        <div style="margin-top:52px;" class="table-tickets-container">
            <table cellpadding="0" cellspacing="0" id="myTable3" class="sort-tickets-table table-hover">
                <thead>
                    <tr>
                        <th width="700"><a href="javascript:;">Todos os tickets</a></th>
                        <th width="130"><a href="javascript:;">Status</a></th>
                        <th width="130"><a href="javascript:;">Prazo</a></th>
                    </tr>
                </thead>
            </table>
            <script>
                $(function() {
                    var filterByStatusDOM = '<div class="sort-tickets-form">' +
                    '   <div class="sort-tickets-control">' +
                    '       <select id="filterTicketStatus">' +
                    '           <option selected="selected" value="">Filtrar por status</option>' +
                    '           @foreach ($status as $value)<option value="{{ $value->nome }}">{{ $value->nome }}</option>@endforeach' +
                    '       </select>' +
                    '   </div>' +
                    '</div>';
                    var table = $('#myTable3').DataTable({
                        dom: "<'datatable-toolbar clearfix'<'form-inline select-control'l><'datatable-filter-container'f>>" +
                            "<'clearfix'<tr>>" +
                        "<'datatable-pagination clearfix'<'col-sm-5'i><'col-sm-7'p>>",
                        processing: true,
                        serverSide: false,
                        stateSave: true,
                        pageLength: 25,
                        lengthMenu: [
                            [10, 25, 50, -1], 
                            ['Exibir 10 por página', 'Exibir 25 por página', 'Exibir 50 por página', 'Exibir todos']
                        ],
                        ajax: '{!! route('site.artwork.services.tickets', [$tipo]) !!}',
                        language: {
                            "url": "/js/datatables/Portuguese-Brasil.json"
                        },
                        columns: [
                            {
                                data: 'nome_completo',
                                name: 'nome_completo',
                                render: function (data, type, full)
                                {
                                    html = '<a href="'+full.link+'">'
                                    + '<span class="'+(full.nova_embalagem ? 'icon-magic-wand' : 'icon-options-settings')+'"></span>'
                                    + '<span class="ticket-name-txt ">'+full.nome_completo+'</span>'
                                    + '</a>';
                                    
                                    return html;
                                }
                            },
                            { data: 'status_nome', name: 'status_nome' },
                            {
                                data: 'prazo', name: 'prazo',
                                render: function (data, type, full)
                                {
                                    html = '<div class="'+(full.atrasado ? 'txt-vermelho' : '')+'">'+full.prazo+'</div>';
                                    return html;
                                }
                            }
                        ],
                        initComplete: function () {
                            this.api().columns().every( function () {
                                var column = this;
                                var select = $('<select><option value=""></option></select>')
                                    .appendTo( $(column.footer()).empty() )
                                    .on( 'change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );
                                        column
                                            .search( val ? '^'+val+'$' : '', true, false )
                                            .draw();
                                    });

                                column.data().unique().sort().each( function ( d, j ) {
                                    select.append( '<option value="'+d+'">'+d+'</option>' )
                                });
                            });

                            $('.datatable-filter-container .dataTables_filter input[type="search"]').attr('placeholder', 'Busca');
                            
                            var dataTables_filter_cancel = '<i></i>';
                            var dataTables_filter_search = '<i></i>';
                            
                            $('.datatable-filter-container .dataTables_filter').append(dataTables_filter_search);
                            $('.datatable-filter-container .dataTables_filter').append(dataTables_filter_cancel);
                            $('.datatable-filter-container .dataTables_filter i').addClass('dataTables-filter-search');
                            $('.datatable-filter-container .dataTables_filter i + i').addClass('dataTables-filter-cancel');
                            
                            function dataTablesFilterCancelShowHide() {
                                if ($('.datatable-filter-container .dataTables_filter input[type="search"]').val() !== '') {
                                    $('.dataTables-filter-search').hide();
                                    $('.dataTables-filter-cancel').show();
                                
                                } else {
                                    $('.dataTables-filter-search').show();
                                    $('.dataTables-filter-cancel').hide();
                                    
                                    table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
                                }
                            }
                           
                            $(document).on('click', '.datatable-filter-container .dataTables-filter-cancel', function () {
                                $('#myTable3_filter input[type="search"]').val('');
                                
                                dataTablesFilterCancelShowHide();
                            });

                            // Remove accented character from search input as well
                            $(document).on('keyup change', '#myTable3_filter input[type="search"], #filterTicketStatus', function () {
                                table.search(
                                    jQuery.fn.DataTable.ext.type.search.string(this.value)
                                ).draw();
                                
                                dataTablesFilterCancelShowHide();
                            });
                            
                            @if ($tipo !== 'cancelados' && $tipo !== 'pausados')
                                $('.datatable-filter-container').append(filterByStatusDOM);
                            @endif
                            
                            
                            $(document).on('change', '#filterTicketStatus', function() {
                                table.search(
                                    jQuery.fn.DataTable.ext.type.search.string(this.value),'',true
                                ).draw();
                            });
                        }
                    });
                    
                });
            </script>
        </div>
    </form>
</div>
@stop
