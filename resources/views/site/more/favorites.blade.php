@extends('site.site_template')

@section('title')
    Favoritos
@stop

@section('content')
{!! Breadcrumbs::renderIfExists('favoritos') !!}
<script src="{{ asset('/js/site/favorites.js') }}"></script>
<div class="principal">
	<div class="listagem-favorites">
        <div class="top-title top-others">
            <h3>FAVORITOS</h3>
            <p>
                Os favoritos fornecem alertas das atualizações dos produtos marcados.
                Para acessá-las clique no ícone do sino do cabeçalho.
            </p>
        </div>
		<div class="list-more">
            @if(count($allRecentsFavorites) > 0)
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <th>Imagem</th>
                        <th class="name-margin">Produto</th>
                        <th>Família</th>
                        <th>Remover</th>
                    </thead>
                    <tbody>
	                    @foreach($allRecentsFavorites as $produto)
                        <tr>
                            @if(!isset($produto['thumb']))
                            <td class="image-center">
                                <div class="image-border-right">
                                    {{ Html::image(asset('/images/icons/frontend/img_default.png'), 'Favorito') }}
                                </div>
                            </td>
                            @else
                            <td class="image-center">
                                <div class="image-border-right">
                                    {{ Html::image(asset($produto['thumb']), 'Favorito') }}
                                </div>
                            </td>
                            @endif
                            <td class="name-margin news-padding-desc">
                                {{ Html::linkRoute('site.search.product', $produto['nome'], array($produto['family'], $produto['product'])) }}
                            </td>
                            <td> {{ $produto['family_name'] }} </td>
                            <td>
                                <a href="javascript:void(0)">
                                    {{ Html::image(asset('/images/icons/frontend/trash.png'), 'favorite', array('class' => 'btn-delete favorite-trash', 'id' => 'prod'.$produto['product'], 'data-prod-id' => $produto['product'], 'data-family-id' => $produto['family'])) }}
                                </a>
                            </td>
                        </tr>
	                    @endforeach
                    </tbody>
                </table>
                {!! $allRecentsFavorites->render() !!}
    		@else
    			 <h3>Nenhum Favorito Cadastrado</h3>
    		@endif
        </div>
  </div>
  <!-- Importação do Sidebar -->
  @include('_partials.sidebar')
</div>
@stop
