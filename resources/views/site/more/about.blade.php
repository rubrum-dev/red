@extends('site.site_template')

@section('title')
    Saiba Mais
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('about') !!}

<div class="principal">
	<div class="listagem">
		<div class="top-title">		
			{{ Html::image(asset('/images/icons/frontend/family.png'), 'Familia') }}
			<h3>SAIBA MAIS</h3>

		</div> 
		<div class="box-imagens">			
				
		</div>
	</div>

	<!-- Importação do Sidebar -->
	@include('_partials.sidebar')

</div>



@stop