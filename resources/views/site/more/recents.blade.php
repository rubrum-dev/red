@extends('site.site_template')
@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop
@section('content')
{!! Breadcrumbs::renderIfExists('recentes') !!}
<div class="principal">
    <div class="top-head">
        <div class="top-inline top-head-title">
            <h2><span>Itens Adicionados Recentemente</span></h2>
        </div>
        <div class="top-inline top-head-filter right">
            <form class="item-category-filter">
                <div class="select-control">
                    <select id="categoria" name="categorias">
                        <option value="">Filtrar Categorias</option>
                    </select>
                </div>
            </form>
        </div>
    </div>
    <div class="listagem">
        <div class="sku-lista-embalagens">
            @if(count($allRecentsSkus) > 0)
                <div style="margin:0px -20px;">
                    <div align="center" class="sku-listagem-interna clearfix">
                        @foreach($allRecentsSkus as $sku)
                            <div class="itens-lista-sku skuRegular">
                                <div class="unitario-sku">
                                    <!--a href="{{ url('/site', $sku->produtos->id_familia, $sku->nome) }}" class="sku-modal" rel="modal:open" data-toggle="modal" data-id-sku="{{ $sku->id }}"-->
                                    <div class="box-sku-img">
                                        {{ Html::linkRoute('site.sku', '', array($sku->produtos->id_familia, $sku->produtos->id, 'sku' => $sku->id)) }}
                                        <span class="icon icon-eye">Visualizar</span>
                                        @if(isset($sku->thumb))
                                            {{ Html::image(asset($sku->thumb), 'SKU') }}
                                        @elseif(isset($sku->thumb_volume))
                                            {{ Html::image(asset($sku->thumb_volume), 'SKU') }}
                                        @else
                                            {{ Html::image(asset('/images/icons/frontend/img_default.png'), 'SKU') }}
                                        @endif
                                    </div>
                                    <h4 class="nome-volume-sku">{{ Html::linkRoute('site.sku', $sku->nome, array($sku->produtos->id_familia, $sku->produtos->id, 'sku' => $sku->id)) }}</h4>
                                    <small>Adicionado em</small>
                                    <small>{{ $sku->data }}</small>
                                    <!--/a-->
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                {!! $allRecentsSkus->render() !!}
            @else
                <h3>Nenhum SKU Cadastrado</h3>
            @endif
        </div>
	</div>
</div>
@stop
