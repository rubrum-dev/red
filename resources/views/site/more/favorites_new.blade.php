@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
<script src="{{ asset2('/js/site/favorites.js') }}"></script>

{!! Breadcrumbs::renderIfExists('favoritos') !!}

<div class="content content-fluid">
    <div class="packshelf-container margin-top-10 margin-bottom-30">
        <h3 class="master-title">Gerenciar Favoritos</h3>
        <section class="product-list favorites margin-top-30">
            @if(count($allRecentsFavorites) > 0)
                <h4 class="master-title-sub txt-small txt-light txt-uppercase margin-bottom-5">Produtos Favoritos</h4>
                <div class="listing">
                    <ul>
                        @foreach($allRecentsFavorites as $produto)
                            <li class="txt-center">
                                <div style="background-color: {{ $produto['cor_hexa'] }}" class="box-image animate">
                                    <a href="javascript:;" title="Favorito" alt="Favorito"></a>
                                    <div class="box-image-thumb-redim">
                                        {{--<img src="{{ asset($produto['thumb']) }}" title="{{$produto['nome']}}" data-adaptive-background class="box-image-logo" />--}}
                                        <img src="{{ asset($produto['thumb']) }}" title="{{$produto['nome']}}" alt="{{$produto['nome']}}" class="box-image-logo" />
                                    </div>
                                    <a href="javascript:;" data-prod-id="{{$produto['product']}}" data-family-id="{{$produto['family']}}" id="{{'prod'.$produto['product']}}" data-title="Excluir Favorito" class="btn-delete btn-function ui-tooltip top">
                                        <i class="icon fa fa-trash no-margin"></i>
                                    </a>
                                </div>
                                <span class="product-name txt-regular"><a href="javascript:;">{{ $produto['nome'] }}</a></span>
                            </li>
                        @endforeach
                    </ul>
                </div>
                {!! $allRecentsFavorites->render() !!}
            @else
                <div class="is-empty-big">
                    <span class="fa fa-star">Nenhum produto adicionado aos Favoritos</span>
                </div>
            @endif
        </section>
    </div>
</div>
@stop