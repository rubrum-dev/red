@extends('site.site_template')

@section('title')
    Suporte
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('suport') !!}

<div class="principal">
	<div class="listagem">
		<div class="top-title">		
			{{ Html::image(asset('/images/icons/frontend/family.png'), 'Familia') }}
			<h3>SUPORTE</h3>

		</div> 
		<div class="box-imagens">			
				
		</div>
	</div>

	<!-- Importação do Sidebar -->
	@include('_partials.sidebar')

</div>



@stop