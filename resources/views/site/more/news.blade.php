@extends('site.site_template')

@section('title')
    Alerta dos Favoritos
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('suport') !!}

<script src="{{ asset('/js/site/viewed-news.js') }}"></script>

<div class="principal">
	<div class="listagem listagem-alertas">
        <div class="top-title top-others">
        	<h3>ALERTAS DOS FAVORITOS</h3>
            <p>Essas são as alertas dos produtos favoritos. Para marcar um favorito clique nos
                ícones de estrela presentes abaixo dos nomes dos produtos.</p>
        </div>

		<div class="list-more">
            @if(count($news) > 0)
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <th>Módulo</th>
                        <th class="name-margin">Descrição</th>
                        <th>Produto</th>
                        <th>Data</th>
                    </thead>
                    <tbody>
                    @foreach($news as $new)
                        @if(Auth::user()->updated_at < $new['data_full'])
                            <tr class="line_new_news">
                        @else
                            <tr class="line_old_news">
                        @endif
                            @if($new['modulo_id'] == 1)
                                <td class="image-center image-mod-news">
                                    <div class="image-border-right">
                                        {{ Html::image(asset('/images/layout/logo_guide.png'), 'Guide') }}
                                    </div>
                                </td>
                            @elseif($new['modulo_id'] == 2)
                                <td class="image-center image-mod-news">
                                    <div class="image-border-right">
                                        {{ Html::image(asset('/images/layout/logo_sku.png'), 'File') }}
                                    </div>
                                </td>
                            @elseif($new['modulo_id'] == 3)
                                <td class="image-center image-mod-news">
                                    <div class="image-border-right">
                                        {{ Html::image(asset('/images/layout/logo_sku.png'), 'Sku') }}
                                    </div>
                                </td>
                            @elseif($new['modulo_id'] == 4)
                                <td class="image-center image-mod-news">
                                    <div class="image-border-right">
                                        {{ Html::image(asset('/images/layout/logo_enxoval.png'), 'Enxoval') }}
                                    </div>
                                </td>
                            @endif
                            <td class="name-margin news-padding-desc"> {{ Html::link($new['url'], $new['descricao']) }} </td>
                            <td> {{ Html::link($new['url'], $new['produto_nome']) }} </td>
                            <td> {{ $new['data'] }} </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $news->render() !!}
    		@else
		        <h3>Nenhuma Novidade</h3>
    		@endif
		</div>
	</div>

	<!-- Importação do Sidebar -->
	@include('_partials.sidebar')

</div>



@stop
