@extends('site.site_template')

@section('title')
    Recentes
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('recentes') !!}

<link rel="stylesheet" type="text/css" href="{{ asset('/css/site/slide.css?v=1.1.3') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('/css/site/common.css?v=1.1.3') }}" />
<script src="{{ asset('/js/site/slide.js?v=1.1.3') }}"></script>
<script src="{{ asset('/js/site/site-news-v2.js?v=1.1.3') }}"></script>

<div class="principal">
    <div class="listagem-lasts">
        <div class="top-title top-others">
        	<h3>RECENTES</h3>
            <p>Itens recentemente adiconados ao sistema.</p>
        </div>

        <div class="list-more">
            @if(count($allRecentsSkus) > 0)
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <th>Imagem</th>
                        <th class="name-margin">SKU</th>
                        <th>Família</th>
                        <th>Data</th>
                    </thead>
                    <tbody>
                    @foreach($allRecentsSkus as $sku)
                        <tr>
                            @if(isset($sku->thumb))
                                <td class="image-center">
                                    <div class="image-border-right">
                                        {{ Html::image(asset($sku->thumb), 'SKU') }}
                                    </div>
                                </td>
                            @elseif(isset($sku->thumb_volume))
                                <td class="image-center">
                                    <div class="image-border-right">
                                        {{ Html::image(asset($sku->thumb_volume), 'SKU') }}
                                    </div>
                                </td>
                            @else
                                <td class="image-center">
                                    <div class="image-border-right">
                                        {{ Html::image(asset('/images/icons/frontend/img_default.png'), 'SKU') }}
                                    </div>
                                </td>
                            @endif
                            <td class="name-margin">
                                {{ Html::linkRoute('site.sku', $sku->nome, array($sku->produtos->id_familia, $sku->produtos->id, 'sku' => $sku->id)) }}
                            </td>
                            <td> {{ $sku->familia }} </td>
                            <td> {{ $sku->data }} </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $allRecentsSkus->render() !!}
    		@else
		         <h3>Nenhum SKU Cadastrado</h3>
    		@endif
        </div>

    </div>

    <!-- Importação do Sidebar -->
    @include('_partials.sidebar')

</div>

@stop
