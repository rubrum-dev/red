@extends('site.site_template')

@section('title')
    Busca
@stop

@section('content')

{!! Breadcrumbs::renderIfExists('busca') !!}

<script src="{{ asset('/js/site/favorites.js') }}"></script>

<div class="principal">
	<div class="listagem">
        <div class="top-title top-others">
        	<h3>BUSCA: {{ mb_strtoupper($search, 'UTF-8') }}</h3>
        </div>

		<div class="list-more">
			@if(count($results) > 0)
				@if(count($results) == 1)
					<p>1 Item Encontrado</p>
				@else
					<p>{{ $results->total() }} Itens Encontrados</p>
				@endif
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <th>Imagem</th>
                        <th class="name-margin">Nome</th>
                        <th>Tipo</th>
                    </thead>
                    <tbody>
                		@foreach($results as $value)
	                    	<tr>
	                    		@if(isset($value['thumb']))
                                    <td class="image-center">
                                        <div class="image-border-right">
                                            {{ Html::image($value['thumb'], 'Imagem') }}
                                        </div>
                                    </td>
                                @elseif(isset($value['thumb_volume']))
                                    <td class="image-center">
                                        <div class="image-border-right">
                                            {{ Html::image($value['thumb_volume'], 'Imagem') }}
                                        </div>
                                    </td>
	                            @else
                                    <td class="image-center">
                                        <div class="image-border-right">
                                            {{ Html::image(asset('/images/icons/frontend/img_default.png'), 'Favorito') }}
                                        </div>
                                    </td>
	                            @endif
		                    		<td class="name-margin">
		                    			<a href="{{ url($value['url']) }}">
		                    				{{ $value['nome'] }}
	                    				</a>
	                    			</td>
		                    		<td>{{ $value['tipo'] }}</td>
	                    	</tr>
                		@endforeach
                    </tbody>
                </table>
                {!! $results->render() !!}
        	@else
    			<h3>Nenhum Item Localizado</h3>
        	@endif
        </div>

  </div>

  <!-- Importação do Sidebar -->
  @include('_partials.sidebar')

</div>

@stop
