@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')
<script src="{{ asset('/js/site/product.js') }}"></script>

{!! Breadcrumbs::renderIfExists('produtos', $family) !!}

<div class="content content-fluid">
	<div class="packshelf-container margin-top-10 margin-bottom-30">
		<section class="product-list">
			<h3 class="master-title">{{ $familia->nome }}</h3>
			<ul class="tabs clearfix">
				<li><a href="javascript:;" class="txt-color-default active">Produtos de Mercado</a></li>
			</ul>
			<div class="block margin-top-20 margin-bottom-20">
				<div class="flexbox-container">
					@if(isset($categorias) || isset($tiposEmbalagens))
						<div class="flexbox-column flex flex-fluid">
                            {!! Form::open(array('class' => '', 'id' => 'form-category', 'method' => 'get', 'url' => Request::url())) !!}
								<div class="flexbox-group flex flex-4-large">
									<div class="select-control">
										@if(isset($categorias))
											{!! Form::select('categorias', $categorias, app('request')->input('categorias'), array('id' => 'categoria', 'placeholder' => 'Categorias', 'onchange' => 'this.form.submit()')) !!}
										@endif
										@if(isset($tiposEmbalagens))
											{!! Form::select('tiposEmbalagens', $tiposEmbalagens, null, array('id' => 'embalagem', 'placeholder' => 'Tipos de Embalagens', 'onchange' => 'this.form.submit()')) !!}
										@endif
									</div>
								</div>
							{!! Form::close() !!}
						</div>
					@endif
				</div>
			</div>
			@if(count($produtos) > 0)
        	    <div class="listing">
					<ul>
						@foreach($produtos as $produto)
							@if (!$produto->replicar_logo_marca)
                                @foreach($produto->logotipos as $logotipo)
                                    @if($logotipo->id_tipo_logotipo == 1)
									<li>
                                        <div style="background-color: {{ $produto->cor_hexa }}" class="box-image animate">
                                            @if (Request::route('modulo') == 'sku')
                                                <a href="{{ route('site.index') . '/' . $family . '/produtos/' . $produto->id . '/sku' }}" title="{{ $produto->nome }} - SKU"></a>
                                            @endif
                                            @if(!Auth::guest() && Request::route('modulo') == 'guide' && $produto->checkGuides && (in_array($user->id_adm_perfil, [1,2]) || (in_array($family, $user->familias) && in_array(1, $user->modulos))))
                                                <a href="{{ route('site.index') . '/' . $family . '/produtos/' . $produto->id . '/guide/versoes' }}" title="{{ $produto->nome }} - Guide">
                                            @endif
                                            @if(!Auth::guest() && Request::route('modulo') == 'ads' && $produto->checkEnxovais && (in_array($user->id_adm_perfil, [1,2]) || (in_array($family, $user->familias) && in_array(4, $user->modulos))))
                                                <a href="{{ route('site.index') . '/' . $family . '/produtos/' . $produto->id . '/guide/enxovais' }}" title="{{ $produto->nome }} - Enxoval"></a>
                                            @endif
											<div class="box-image-thumb-redim">	
												<img src="{{ asset($logotipo->thumb) }}" title="{{$produto->nome}}" alt="{{$produto->nome}}" class="box-image-logo" />
											</div>
										</div>
                                        @if (Request::route('modulo') == 'sku')
                                            <span class="product-name txt-regular"><a href="{{ route('site.index') . '/' . $family . '/produtos/' . $produto->id . '/sku' }}" title="{{ $produto->nome }} - SKU">{{ $produto->nome }}</a></span>
                                        @endif
                                        @if(!Auth::guest() && Request::route('modulo') == 'guide' && $produto->checkGuides && (in_array($user->id_adm_perfil, [1,2]) || (in_array($family, $user->familias) && in_array(1, $user->modulos))))
                                            <span class="product-name txt-regular"><a href="{{ route('site.index') . '/' . $family . '/produtos/' . $produto->id . '/guide/versoes' }}" title="{{ $produto->nome }} - Guide">{{ $produto->nome }}</a></span>
                                        @endif
                                        @if(!Auth::guest() && Request::route('modulo') == 'ads' && $produto->checkEnxovais && (in_array($user->id_adm_perfil, [1,2]) || (in_array($family, $user->familias) && in_array(4, $user->modulos))))
                                            <span class="product-name txt-regular"><a href="{{ route('site.index') . '/' . $family . '/produtos/' . $produto->id . '/guide/enxovais' }}" title="{{ $produto->nome }} - Enxoval">{{ $produto->nome }}</a></span>
                                        @endif
									</li>
                                    @endif
                                @endforeach
							@else
							<li>
								<div style="background-color: {{ $produto->cor_hexa }}" class="box-image animate">
									@if (Request::route('modulo') == 'sku')
										<a href="{{ route('site.index') . '/' . $family . '/produtos/' . $produto->id . '/sku' }}" title="{{ $produto->nome }} - SKU"></a>
									@endif
									@if(!Auth::guest() && Request::route('modulo') == 'guide' && $produto->checkGuides && (in_array($user->id_adm_perfil, [1,2]) || (in_array($family, $user->familias) && in_array(1, $user->modulos))))
										<a href="{{ route('site.index') . '/' . $family . '/produtos/' . $produto->id . '/guide/versoes' }}" title="{{ $produto->nome }} - Guide">
									@endif
									@if(!Auth::guest() && Request::route('modulo') == 'ads' && $produto->checkEnxovais && (in_array($user->id_adm_perfil, [1,2]) || (in_array($family, $user->familias) && in_array(4, $user->modulos))))
										<a href="{{ route('site.index') . '/' . $family . '/produtos/' . $produto->id . '/guide/enxovais' }}" title="{{ $produto->nome }} - Enxoval"></a>
									@endif
									<div class="box-image-thumb-redim">	
										<img src="{{ asset($produto->familias->logotipos->first()->thumb) }}" title="{{$produto->nome}}" alt="{{$produto->nome}}" class="box-image-logo" />
									</div>
								</div>
								<span class="product-name txt-regular"><a href="{{ route('site.index') . '/' . $family . '/produtos/' . $produto->id . '/guide/enxovais' }}" title="{{ $produto->nome }} - Enxoval">{{ $produto->nome }}</a></span>
							</li>
                            @endif
						@endforeach
					</ul>
				</div>
			@else
				<div class="is-empty-big">
					<span class="fa fa-cubes">Nenhum item localizado</span>
				</div>
			@endif
		</section>
	</div>
</div>
@stop
