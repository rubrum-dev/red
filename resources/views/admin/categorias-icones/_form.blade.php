<!-- Modal -->
<div class="modal fade" id="modalCategorias" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<span class="glyphicon glyphicon-plus-sign"></span>
				</h4>
			</div>
			<div class="modal-body">

				{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-category')) !!}
					{{ Form::hidden('id', '', array('id' => 'editId')) }}
					<div class="form-group">
						{!! Form::label('lblNome', 'Nome', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
					    	{!! Form::text('nome', '', array('class' => 'form-control', 'id' => 'editNome')) !!}
				    	</div>
				    </div>				    			    
		    		<div class="modalButtons">
						{!! Form::button('Cancelar', array('class' => 'btn btn-info', 'data-dismiss' => 'modal', 'id' => 'btn-cancel-modal')) !!}
					    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}	
					</div>

				{!! Form::close() !!}

			</div>
		</div>

	</div>
</div>