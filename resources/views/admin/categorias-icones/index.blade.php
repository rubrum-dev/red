@extends('admin.admin_template')

@section('title')
	Categorias de Ícones
@stop

@section('content')

<script src="{{ asset('/js/admin/categorias-icones.js') }}"></script>
<script>
	$(function () {
		$('#btn-search').click(function () {
			var src = $('#txt-search').val();
			src.trim() == '' ? window.location = '<?php echo url('admin/icon-category'); ?>' : window.location = '<?php echo url('admin/icon-category/search'); ?>/' + src;
		});
	});
</script>

<div class="list-title-header">
	<h1><span class="glyphicon glyphicon-book"></span> Categorias de Ícones</h1>			
</div>

<div class="list-header">
	<div>
		<div class="input-group col-xs-6 search">
			<input type="text" id="txt-search" class="form-control" placeholder="Buscar por Nome ...">
			
			<span class="input-group-btn">
				<button id="btn-search" class="btn btn-default" type="button">Buscar</button>
			</span>
		</div>		

		{{ Html::link('admin/product', 'Produtos', array('class' => 'btn btn-warning btn-search-left')) }}

		<button type="button" id="btn-create" class="btn btn-info" data-toggle="modal" data-target="#modalCategorias">
			<span class="glyphicon glyphicon-plus-sign"></span> Nova Categoria de Ícone
		</button>

		<!-- Modal de Criação/Edição de Categoria -->
		@include('admin.categorias-icones._form')

	</div>
</div>

<div class="table-responsive clear">

@if (count($categorias) >= 1)
	<table id="list-category" class="table table-striped">
		<thead>
			<tr>
				<th>Nome</th>
				<th>Ações</th>
			</tr>
		</thead>
		<tbody>
			@foreach($categorias as $categoria)
				<tr>
					<td>{{ $categoria->nome }}</td>				
					<td>
						<button type="button" id="btn-edit-{{$categoria->id}}" class="btn btn-primary btn-sm btn-edit" data-toggle="modal" data-target="#modalCategorias" data-category-id="{{ $categoria->id }}">
							<span class="glyphicon glyphicon-edit"></span> Editar
						</button>
						@if (Auth::user()->id_adm_perfil == 1)
							<button type="button" id="btn-remove-{{$categoria->id}}" class="btn btn-danger btn-sm btn-remove" data-category-id="{{ $categoria->id }}">
								<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Remover
							</button>
						@endif
					</td>
				</tr>
			@endforeach	
		</tbody>
	</table>
@else
	<div>	
		<h4>Nenhuma Categoria de Ícone Localizada</h4>
	</div>
@endif

</div>

@stop