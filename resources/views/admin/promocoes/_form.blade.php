<!-- Modal -->
<div class="modal fade" id="modalPromocao" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close fa fa-times" data-dismiss="modal"></button>
				<h4 class="modal-title">
					<span class="glyphicon glyphicon-plus-sign"></span>
				</h4>
			</div>
			<div class="modal-body">
				{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-ftps', 'files' => 'true')) !!}
					{{ Form::hidden('id', '', array('id' => 'editId')) }}
					<div class="form-group form-group-flex flex-align-baseline">
						{!! Form::label('lblNome', 'Nome', array('class' => 'col-xs-2 control-label no-margin no-padding txt-right')) !!}
						<div class="col-xs-12">
					    	{!! Form::text('nome', '', array('class' => 'form-control', 'id' => 'editNome')) !!}
				    	</div>
				    </div>
					<div class="form-group form-group-flex flex-align-baseline">
						{!! Form::label('lblProduto', 'Produto', array('class' => 'col-xs-2 control-label no-margin no-padding txt-right')) !!}
						<div class="col-xs-12">
					    	{!! Form::select('id_produto', $produtos, null, array('placeholder' => 'Selecione o Produto...', 'class' => 'form-control', 'id' => 'editProduto')) !!}
				    	</div>
				    </div>
					<div class="form-group form-group-flex flex-align-baseline" id="box-expiracao">
						{!! Form::label('lblExpiracao', 'Expiração', array('class' => 'col-xs-2 control-label no-margin no-padding txt-right')) !!}
						<div class="col-xs-12">
							{!! Form::text('dt_expiracao', '', array('class' => 'form-control', 'id' => 'editExpiracao', 'placeholder' => 'dd/mm/aaaa')) !!}
						</div>
					</div>
					<div class="form-group form-group-flex flex-align-baseline">
						{{--
						{!! Form::label('lblStatus', 'Status', array('class' => 'col-xs-3 control-label no-margin no-padding txt-right')) !!}
						<div class="col-xs-12">
							{!! Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), null, array('class' => 'form-control', 'id' => 'editStatus')) !!}
						</div>
						--}}
						<label class="col-xs-2 control-label"></label>
						<div class="col-xs-12">
							<div class="row">
								<label for="editStatus" class="col-xs-12 no-margin">
									{!! Form::checkbox('status', '', null, array('id' => 'editStatus')) !!} 
									<span class="no-margin-left no-margin-right">Campanha Promocional Ativa</span>
									<span class="txt-small txt-color-alt margin-left-10">Exibe na lista de campanhas promocionais disponíveis no cadastro de embalagens.</span>
								</label>
							</div>
						</div>
				    </div>
			    	<div class="form-group form-group-flex flex-align-start" id="listFile">
				    	{!! Form::label('lblArquivo', 'Imagem', array('class' => 'col-xs-2 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
				    	<div class="col-xs-12">
							<table id="list-tipos-logos" class="table no-margin">
								<tr>
									<td class="col-xs-2">
							    		{{ Html::image(asset('/images/backend/noImage.png'), 'alt', array('class' => 'img-rounded img-logo')) }}
							    	</td>
						    	</tr>
						    	<tr>
									<td class="col-xs-2">
										{!! Form::file('image') !!}
									</td>
								</tr>
							</table>
				    	</div>
				    </div>
		    		<div class="modalBtnGroup">
						{!! Form::button('Cancelar', array('class' => 'btn-control btn-alt', 'data-dismiss' => 'modal', 'id' => 'btn-cancel-modal')) !!}
					    {!! Form::submit('Salvar', array('class' => 'btn-control btn-positive')) !!}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
