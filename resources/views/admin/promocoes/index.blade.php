@extends('admin.admin_template')
@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/datatables/dataTables.bootstrap.min.css?v=1.0') }}" />
<script type="text/javascript" src="{{ asset('/js/admin/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/admin/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/datatables/accent-neutralise.js') }}"></script>
<script src="{{ asset2('/js/admin/promocoes.js') }}"></script>
<!--script>
	$(function () {
		$('#btn-search').click(function () {
			var src = $('#txt-search').val();
			src.trim() == '' ? window.location = '<?php //echo url('admin/promotions'); ?>' : window.location = '<?php //echo url('admin/promotions/search'); ?>/' + src;
		});
	});
</script-->
<div class="admin-container">
	<div class="list-title-header">
		<h3>Gerenciar Campanhas Promocionais</h3>
		{{--<span class="count">Total: {{ $total }} campanhas promocionais</span>--}}
	</div>
	@include('admin.promocoes._form')
	<div class="table-responsive clear">
		<div class="form-group-add">
    		<button type="button" id="btn-create" class="btn btn-sm btn-function" data-toggle="modal" data-target="#modalPromocao">
				<i class="icon fa fa-trophy"></i> 
				<span class="txt-semibold">Adicionar Campanha Promocional</span>
			</button>
		</div>
		@if (count($promocoes) >= 1)
			<table id="list-promocoes" class="table table-striped">
				<thead>
					<tr>
						<th width=""><span>Campanhas</span></th>
						<th width="400"><span>Produto</span></th>
						<th width="90"><span>Expiração</span></th>
						<th width="90"><span>Status</span></th>
						<th width="30"><span></span></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td width="">
							<a href="javascript:;" class="txt-no-link">
								<i class="icon fa fa-trophy"></i>
								<span class="text-side">{{ $promocao->nome }}</span>
							</a>
						</td>
						<td width=""><span>{{ $promocao->produtos->nome }}</span></td>
						<td width=""><span>{{ $promocao->dt_expiracao }}</span></td>
						<td width=""><span>{{ $promocao->status ? 'Ativo' : 'Inativo' }}</span></td>
						<td width="">
							{{--<a type="button" id="btn-edit-{{$promocao->id}}" class="btn btn-primary btn-sm btn-edit" data-toggle="modal" data-target="#modalPromocao" data-promocao-id="{{ $promocao->id }}">
								<span class="icon fa fa-edit"></span> Editar
							</a>--}}
							{{--<a type="button" id="btn-remove-{{$promocao->id}}" class="btn btn-danger btn-sm btn-remove" data-promocao-id="{{ $promocao->id }}">
								<span class="icon fa fa-trash"></span> Remover
							</a>--}}
							<div class="dataTable_btn_group">
								<div class="dropdown drop-to-left">
									<a href="javascript:;" class="ui-tooltip top dropdown-toggle fa fa-ellipsis-h"><span class="tooltip-box">Opções</span></a>
									<div class="dropdown-list">
										<ul> 
											<li><a href="javascript:;" data-toggle="modal" data-target="#modalPromocao" data-promocao-id="{{ $promocao->id }}" id="btn-edit-{{$promocao->id}}" class="btn-edit txt-bold"><span class="icon fa fa-edit"></span>Alterar</a></li>
											<li><a href="javascript:;" data-promocao-id="{{ $promocao->id }}" id="btn-remove-{{$promocao->id}}" class="btn-remove txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
										</ul>
									</div>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		@else
			<div class="listing-is-empty">
				<span><i class="icon fa fa-trophy"></i>Nenhuma Campanha Promocional Localizada</span>
			</div> 
		@endif
		<script>
			$(function() {
				var table = $('#list-promocoes').DataTable({
					dom: '<"dataTable-head"<"dataTable-filter row"<"dataTable-filter-in row"<"dataTable-searchbox-container col-xs-4"f>>>>' +
						'<<"table-responsive"tr>>' +
					'<"dataTable-bottom row"<"dataTable-selectbox-container"l><"dataTables_paginate_flex"p><i>>',
					bAutoWidth: false,
					processing: true,
					serverSide: false,
					stateSave: true,
					bStateSave: true,
                    pageLength: 25,
					lengthMenu: [
						[10, 25, 50, -1], 
						['Exibir 10 por página', 'Exibir 25 por página', 'Exibir 50 por página', 'Exibir todos']
					],
					columnDefs: [
						{ orderable: false, targets: 4 },
                    ],
					language: {
						'url': '/js/datatables/language/Portuguese-Brasil.json'
                    },
					initComplete: function () {
						this.api().columns().every( function () {
							var column = this;
							var select = $('<select><option value=""></option></select>')
								.appendTo( $(column.footer()).empty() )
								.on( 'change', function () {
									var val = $.fn.dataTable.util.escapeRegex(
										$(this).val()
									);
									column
										.search( val ? '^'+val+'$' : '', true, false )
										.draw();
								});
							column.data().unique().sort().each( function ( d, j ) {
								select.append( '<option value="'+d+'">'+d+'</option>' )
							});
						});

						$('.dataTable-searchbox-container input[type="search"]').attr('placeholder', 'Buscar');

						var dataTables_filter_search = '<i></i>';
						var dataTables_filter_cancel = '<i></i>';
						
						$('.dataTable-searchbox-container .dataTables_filter').append(dataTables_filter_search);
						$('.dataTable-searchbox-container .dataTables_filter').append(dataTables_filter_cancel);
						$('.dataTable-searchbox-container .dataTables_filter i').addClass('dataTables_filter_search');
						$('.dataTable-searchbox-container .dataTables_filter i + i').addClass('dataTables_filter_cancel');

						function dataTablesFilterCancelShowHide() {
							if ($('.dataTable-searchbox-container .dataTables_filter input[type="search"]').val() !== '') {
								$('.dataTables_filter_search').hide();
								$('.dataTables_filter_cancel').show();
							
							} else {
								$('.dataTables_filter_search').show();
								$('.dataTables_filter_cancel').hide();
								
								table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
							}
						}

						$(document).on('click', '.dataTable-searchbox-container .dataTables_filter_cancel', function () {
							$('.dataTables_filter input[type="search"]').val('');
							
							dataTablesFilterCancelShowHide();
						});

						// Remove accented character from search input as well
						$(document).on('keyup change', '.dataTables_filter input[type="search"]', function () {
							table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
						
							dataTablesFilterCancelShowHide();
						});
					},
					fnDrawCallback: function( oSettings ) {
						if(oSettings.aiDisplay.length <= 0) {
							$('.dataTable thead tr').hide();
							$('.dataTable-bottom').hide();
							$('.dataTables_empty').addClass('fa-trophy');
							$('.dataTables_empty').text('Nenhuma campanha promocional encontrada.');
						} else {
							$('.dataTable thead tr').show();
							$('.dataTable-bottom').show();
							$('.dataTables_empty').removeClass('fa-trophy');
							$('.dataTables_empty').text('');
						}
					}
				});
			});
		</script>
	</div>
</div>
@stop
