@extends('site.site_template')

@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop

@section('content')

<!-- TO-DO - Breadcrumb -->
<div class="breadcumbs-out clearfix">		
    <div class="breadcrumbs-inner content-fluid clearfix">	
        <ul>
            <li><a href="{{ route('admin.grupos-trabalho.gerenciar') }}">Gerenciar Grupos de Trabalho</a></li>
            <li class="active">Editar Grupo de Trabalho</li>
        </ul>
    </div>
</div>
<!-- END TO-DO - Breadcrumb -->

<script>
    var csrf_token = '{{ csrf_token() }}';
    var id_usuario = {{Auth::user()->id}};
</script>

<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/angular-locale_pt-br.js') }}"></script>
<script src="{{ asset('/js/angular/angular-selectize.js') }}"></script>
<script src="{{ asset2('/js/angular/controllers/admin_grupos_trabalho_editar.js') }}"></script>

<div ng-app="app" ng-controller="GruposTrabalhoEditarController" ng-cloak class="content content-fluid clearfix">
    <section class="artw-package-edit-container margin-top-10 margin-bottom-40">
        <form name="workgroupCreateForm" action="" method="POST" novalidate>
            <input type="hidden" name="_token" value="{{ csrf_token()}}" />
            <h3 class="master-title txt-bold margin-bottom-20">Editar Grupo de Trabalho</h3>
            <div class="block">
                <div class="bs no-border no-padding margin-bottom-30">
                    <h4 class="txt-lighter txt-small txt-uppercase margin-bottom-15">Grupo de Trabalho *</h4>
                    <div class="flexbox-container flexbox-group">
                        <div class="flex flex-fluid">
                            <input type="text" placeholder="Nome do grupo de trabalho" ng-model="grupo_nome" name="grupoTrabalhoNome" id="grupoTrabalhoNome" class="input-control only-alpha-numeric" />
                        </div>
                    </div>
                </div>
                
                <div class="bs no-border no-padding margin-bottom-25">
                    <h4 class="subtitle txt-small txt-uppercase txt-lighter margin-bottom-15">Opções</h4>
                    <div class="flexbox-container">
                        <div class="flex-fluid">
                            <ul class="list-group-indent">
                                <li class="list-item-horizontal">
                                    <label class="indent clickable no-underline">
                                        <div class="pull">
                                            <i class="icon fa fa-globe"></i>
                                            <span class="text txt-bold">Tornar Global <small class="txt-small txt-regular margin-left-20">O grupo de trabalho fica disponível para ser utilizado por qualquer usuário.</small></span>
                                            <span class="switch-control pull-right no-margin">
                                                <input ng-model="grupo_global" type="checkbox" name="chkGrupoGlobal" id="chkGrupoGlobal" />
                                                <span class="switch-toggle abs-right-offset no-margin"></span>
                                            </span>
                                        </div>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div ng-class="{'bs border-color-primary padding-bottom-30': grupo_nome !== '' && $root.participantes.length > 0}">
                    <div ng-show="$root.participantes.length > 0" class="flexbox-container flex-align-center margin-bottom-15">
                        <table cellpading="0" cellspacing="0" class="workflow-membership-table table-border table-hover">
                            <thead>
                                <th width=""><span class="txt-small txt-light txt-uppercase">Nome</span></th>
                                <th width="240"><span class="txt-small txt-light txt-uppercase">Empresa / Departamento</span></th>
                                <th width="240"><span class="txt-small txt-light txt-uppercase">Função</span></th>
                                <th width="70"><span class="txt-small txt-light txt-uppercase"></span></th>
                                <th width="50"><span class="txt-small txt-light txt-uppercase"></span></th>
                            </thead>
                            <tbody>
                                <!-- Table-group -->
                                <tr ng-repeat="(k, participante) in $root.participantes" class="table-group no-animate">
                                    <td colspan="5">
                                        <table cellpading="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td width="">
                                                        <div class="d-flex flex-align-center">
                                                            <i ng-class="{ 'fa-users has-teamwork': ( (participante.equipe_membros.length > 0 && cbMembershipProfile[k] == 1) || (participante.equipe_membros.length > 0 && cbMembershipProfile[k] == 2) ), 'fa-user': (cbMembershipProfile[k] != 1 && cbMembershipProfile[k] != 2) || participante.equipe_membros.length == 0 }" class="icon fa"></i>
                                                            <div class="text-side">
                                                                <div ng-if="!participante.id || participante.id_usuario !== participante.id_usuario_logado || k === 0" ng-class="{'no-style-input': !participante.id || participante.id_usuario !== participante.id_usuario_logado || k === 0}" class="select-control">
                                                                    <select ng-disabled="(participante.id_usuario !== participante.id_usuario_logado) || (k === 0)" name="cbMembershipName[]" class="txt-semibold no-padding">
                                                                        <option value="" selected>Selecionar participante</option>
                                                                        <option ng-if="participante.id_usuario == membro.id" ng-selected="participante.id_usuario == membro.id" ng-repeat="(k, membro) in membros.membersAll" value="@{{membro.id}}">@{{membro.nome}}</option>
                                                                    </select>
                                                                    <input ng-disabled="participante.id_usuario === participante.id_usuario_logado && k > 0" name="cbMembershipName[]" type="hidden" ng-value="participante.id_usuario">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td width="240">
                                                        <div ng-if="membros.membersAll">    
                                                            <span ng-repeat="(k, membro) in membros.membersAll" ng-if="participante.id_usuario == membro.id">@{{membro.empresa}} <strong ng-if="membro.departamento">/ @{{membro.departamento}}</strong> <strong ng-if="!membro.departamento">/ <i class="txt-color-alt no-italic">Departamento</i></strong></span>
                                                        </div>
                                                    </td>
                                                    <td width="240">
                                                        <div class="no-style-input select-control">
                                                            <select disabled ng-change="selecionar_primeiro(participante.id_usuario, cbMembershipProfile[k], participante.id_perfil.toString())" ng-model="cbMembershipProfile[k]" ng-init='cbMembershipProfile[k] = participante.id_perfil.toString()' name="cbMembershipProfile[@{{k}}]" class="no-padding">
                                                                <option value="" selected>Perfil</option>
                                                                <option ng-selected="participante.id_perfil == 5" value="5">Edição e Finalização</option>
                                                                <option ng-selected="participante.id_perfil == 3" value="3">Revisão</option>
                                                                <option ng-selected="participante.id_perfil == 2" value="2">Primeira Aprovação</option>
                                                                <option ng-selected="participante.id_perfil == 1" value="1">Demais Aprovações</option>
                                                                <option ng-selected="participante.id_perfil == 6" value="6">Envio da Arte</option>
                                                                <option ng-selected="participante.id_perfil == 7" value="7">PDF do Fornecedor</option>
                                                                <option ng-selected="participante.id_perfil == 4" value="4">Participante</option>
                                                            </select>
                                                            <input ng-model="cbMembershipProfile[k]" name="cbMembershipProfile[@{{k}}]" type="hidden" ng-value="cbMembershipProfile[k]">
                                                        </div>
                                                    </td>
                                                    <td width="70">
                                                        <div data-title="Adicionar Equipe" ng-if="cbMembershipProfile[k] == 1 || cbMembershipProfile[k] == 2" class="add-team-btn ui-tooltip top margin-left-auto">
                                                            <div ng-click="adicionar_equipe_toggle(participante, participantes, $index)">    
                                                                <a ng-class="{'btn-function active': participante.adiciona_equipe && !membro_selecionado, 'btn-function': participante.adiciona_equipe && membro_selecionado || !participante.adiciona_equipe }" href="javascript:;" class="btn-function"><i class="icon fa fa-users no-float no-margin"></i></a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td width="50">
                                                        <div class="dropdown drop-to-left no-margin">
                                                            <a href="javascript:;" data-title="Opções" class="dropdown-toggle ui-tooltip top fa fa-ellipsis-h"></a>
                                                            <div class="dropdown-list">
                                                                <ul>    
                                                                    <li><a ng-click="remover(k)" href="javascript:;" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr ng-if="cbMembershipProfile[k] == 1 || cbMembershipProfile[k] == 2" ng-repeat="(e, equipe) in participante.equipe_membros" class="table-sub">
                                                    <td colspan="5">
                                                        <div class="table-sub-content">
                                                            <table cellpading="0" cellspacing="0" class="no-margin-bottom">
                                                                <tbody>
                                                                    <!-- Table sub-group -->
                                                                    <tr>
                                                                        <td width="">
                                                                            <div class="text-side no-margin">
                                                                                <div class="select-control no-style-input">
                                                                                    <select name="cbMembershipTeamName[]" disabled class="txt-semibold no-padding">
                                                                                        <option value="" selected>Membros da equipe</option>
                                                                                        <option ng-selected="membro.id == equipe.id_usuario" ng-repeat="(k, membro) in membros.membersAll" value="@{{membro.id}}">@{{membro.nome}}</option>
                                                                                    </select>
                                                                                    <input name="cbMembershipTeamName[@{{k}}][]" type="hidden" ng-value="equipe.id_usuario">
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td width="240">
                                                                            <span ng-repeat="(k, membro) in membros.membersAll" ng-if="membro.id == equipe.id_usuario">@{{membro.empresa}} <strong ng-if="membro.departamento">/ @{{membro.departamento}}</strong> <strong ng-if="!membro.departamento">/ <i class="txt-color-alt no-italic">Departamento</i></strong></span>
                                                                        </td>
                                                                        <td width="240">
                                                                            <div class="select-control no-style-input column-membership-profile">
                                                                                <select name="cbMembershipTeamProfile[]" ng-model="cbMembershipProfile[k]" disabled class="no-padding">
                                                                                    <option value="">Perfil</option>
                                                                                    <option value="5">Edição e Finalização</option>
                                                                                    <option value="3">Revisão</option>
                                                                                    <option value="2">Primeira Aprovação</option>
                                                                                    <option value="1">Demais Aprovações</option>
                                                                                    <option value="6">Envio da Arte</option>
                                                                                    <option value="7">PDF do Fornecedor</option>
                                                                                    <option value="4">Participante</option>
                                                                                </select>
                                                                            </div>
                                                                        </td>
                                                                        <td width="70" class="txt-center"></td>
                                                                        <td width="50" class="txt-right">
                                                                            <div ng-if="!equipe.id" class="dropdown drop-to-left no-margin">
                                                                                <a href="javascript:;" data-title="Opções" class="dropdown-toggle ui-tooltip top fa fa-ellipsis-h"></a>
                                                                                <div class="dropdown-list">
                                                                                    <ul>    
                                                                                        <li><a ng-click="remover_membro(k, e)" href="javascript:;" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- END Table sub-group -->
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!-- Adiconar Equipe -->
                                                <tr ng-if="participante.adiciona_equipe" class="table-sub">
                                                    <td colspan="5">
                                                        <div class="table-sub-content">
                                                            <table cellpading="0" cellspacing="0" class="no-margin-bottom">
                                                                <tbody>
                                                                    <!-- Table sub-group -->
                                                                    <tr>
                                                                        <td colspan="5">
                                                                            <div class="membership-team-col flexbox-container flexbox-group artw-member-edit-sub">
                                                                                <div class="flex col-select-member">
                                                                                    <selectize config="myConfig" options='membros.members' name="cbMembershipTeamName[]" ng-model="participante.add_participante"></selectize>
                                                                                </div>
                                                                                <div class="flexbox-container flexbox-group flex col-btn-group">
                                                                                    <div class="flex col-member-edit-cancel">
                                                                                        <button ng-click="participante.adiciona_equipe = participante.adiciona_equipe ? false : true" type="button" class="btn-control btn-alt">Cancelar</button>
                                                                                    </div>
                                                                                    <div class="flex col-member-edit-save">
                                                                                        <button ng-click="adicionar_membro(participante, k)" type="button" class="btn-control btn-positive">Adicionar</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- END Table sub-group -->
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!-- END Adicionar Equipe -->
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <!-- Table-group -->
                            </tbody>
                        </table>
                    </div>
                    <!-- Adicionar Participante -->
                    <div class="artw-members-group block">
                        <div class="input-fields-group flexbox-container">
                            <div class="col-select-member flexbox-column flex">
                                <selectize config="myConfig" options='membros.members' name="cbMembershipTeamName[]" ng-model="add_participante"></selectize>
                            </div>
                            <div class="col-select-position flexbox-column flex">
                                <div class="select-control">
                                    <select ng-model="add_perfil" class="members-profile-validate">
                                        <option value="" selected>Selecione a função</option>
                                        <option value="5">Edição e Finalização</option>
                                        <option value="3">Revisão</option>
                                        <option value="2">Primeira Aprovação</option>
                                        <option value="1">Demais Aprovações</option>
                                        <option value="6">Envio da Arte</option>
                                        <option value="7">PDF do Fornecedor</option>
                                        <option value="4">Participante</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-add-member flexbox-column flex">
                                <button ng-click="adicionar()" type="button" class="btn-control btn-positive">Adicionar</button>
                            </div>
                        </div>
                    </div>
                    <!-- END Adicionar Participante -->
                </div>
                <div ng-show="grupo_nome !== '' && participantes.length > 0" class="btn-group txt-center margin-top-40" class="form-section clearfix">
                    <a href="{{ route('admin.grupos-trabalho.gerenciar') }}" class="btn-control btn-call-to-action call-to-action-negative btn-icon btn-icon-left">
                        <span><i class="icon fa fa-times"></i> Cancelar</span>
                    </a>
                    <button type="button" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                        <span><i class="icon fa fa-check"></i> Salvar Grupo de Trabalho</span>
                    </button>
                </div>
            </div>
        </form>
    </section>
</div>
@stop