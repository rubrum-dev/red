<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('/images/favico/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Rubrum Software - @yield('title')</title>
    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:400,300" />
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" />
	<!-- Font-Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/fontawesome/4.7.0/css/font-awesome.min.css') }}" />
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/OverlayScrollbars.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/site/jquery-confirm.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/app.css') }}" />
    
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/non-responsive.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/slide.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/cliente_'. config('app.costummer_id') .'/placeholder-shimmer.css') }}" />	
	<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/cliente_'. config('app.costummer_id') .'/admin.css') }}" />
    <!-- Favico -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=RyQmEa60ez">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=RyQmEa60ez">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=RyQmEa60ez">
    <link rel="manifest" href="/site.webmanifest?v=RyQmEa60ez">
    <link rel="mask-icon" href="/safari-pinned-tab.svg?v=RyQmEa60ez" color="#c41425">
    <link rel="shortcut icon" href="/favicon.ico?v=RyQmEa60ez">
    <meta name="apple-mobile-web-app-title" content="Rubrum">
    <meta name="application-name" content="Rubrum">
    <meta name="msapplication-TileColor" content="#c41425">
    <meta name="theme-color" content="#ffffff">
    <!-- Scripts -->
    <script src="{{ asset('/js/admin/jquery.min.js') }}"></script>
    <script src="{{ asset('/js/admin/bootstrap.min.js') }}"></script>
    <script src="{{ asset2('/js/site/underscore-min.js') }}"></script>
	<script defer src="{{ asset('/js/admin/jquery.validate.min.js') }}"></script>
    <script defer src="{{ asset('/js/admin/additional-methods.min.js') }}"></script>
    <script defer src="{{ asset('/js/admin/jquery.mask.min.js') }}"></script>
    <script defer src="{{ asset2('/js/site/jquery-confirm.js') }}"></script>
    <script defer src="{{ asset2('/js/site/jquery-confirm-config.js') }}"></script>
    <script src="{{ asset2('/js/site/OverlayScrollbars.min.js') }}"></script>
	<script defer src="{{ asset2('/js/site/slide.js') }}"></script>
    <script src="{{ asset2('/js/site/site-news.js') }}"></script>
    <script src="{{ asset2('/js/site/my-alert.js') }}"></script>
	<script src="{{ asset2('/js/site/my-dialog.js') }}"></script>
    <script defer src="{{ asset2('/js/site/site.js') }}"></script>
</head>
<body>
<div style="display:none;" class="loading-big lds-css ng-scope">
    <div class="line-wobble"></div>
</div>	
<div class="master">
    <!-- Header V2 -->
    <header class="header">
        <div class="header-in content-fluid clearfix">
            <a class="header-brand" href="{{ route('site.index') }}"></a>
            @if (!Auth::guest())
                <a href="javascript:;" class="menu-btn"></a>
                <span class="user-account user-btn"><i class="icon fa fa-user-circle"></i></span>
                <nav class="user-account-nav">
                    <h4><i class="icon fa fa-user-circle"></i><strong class="txt-small txt-bold">{{ Auth::user()->nome_abreviado }}</strong><small class="txt-small txt-regular">{{ Auth::user()->email }}</small></h4>
                    <h4><i class="icon fa fa-building"></i><strong class="txt-small txt-bold">{{ Auth::user()->admEmpresa->nome ?? '' }}</strong><small class="txt-small txt-regular">{{ Auth::user()->admDepartamento->nome ?? '' }}</small></h4>
                    <ul class="user-account-list">
                        <li><a href="javascript:;" onclick="siteNews('click')"><i class="icon fa fa-bolt"></i><span class="txt-bold">Atualizações</span></a></li>
                        <li><a href="{{ url('/site/user') }}"><i class="icon fa fa-cog"></i><span class="txt-bold">Configurar Conta</span></a></li>
                        <li><a href="{{ url('/logout') }}"><i class="icon fa fa-power-off"></i><span class="txt-bold">Sair</span></a></li>
                    </ul>
                </nav>
            @else 
                <a href="{{ url('/') }}" class="user-account"><i class="icon fa fa-lock"></i></a>
            @endif
            <div class="collapsible">
                <div class="collapsible-in content-fluid">
                    @if (!Auth::guest())
                        <ul>
                            @if (Auth::guest())
                                <li><a href="{{ url('/site') }}"><i class="icon fa fa-4x fa-camera margin-bottom-15"></i> Packshelf</a></li>
                            @endif
                            @if (!Auth::guest())
                                @if((in_array('artwork', Auth::user()->modulos)))
                                    <li><a href="{{ url('/site/artwork') }}"><i class="icon fa fa-4x fa-paint-brush margin-bottom-15"></i> ArtWork</a></li>
                                @endif
                                @if((in_array('workflow', Auth::user()->modulos)))
                                    <li><a href="{{ url('/site/artwork/managerV3/minhas_tarefas') }}"><i class="icon fa fa-4x fa-list-alt margin-bottom-15"></i>WorkFlow</a></li>
                                @endif
                                @if((in_array('techdraw', Auth::user()->modulos)))
                                    <li><a href="{{ url('/site/techdraw') }}"><i class="icon fa fa-4x fa-pencil margin-bottom-15"></i>TechDraw</a></li>
                                @endif
                                <li><a href="{{ url('/site') }}"><i class="icon fa fa-4x fa-camera margin-bottom-15"></i>PackShelf</a></li>
                                @if((in_array('adsmart', Auth::user()->modulos)) && (config('app.costummer_id') == 1 || config('app.costummer_id') == 'dev' || config('app.costummer_id') == 'stg' || config('app.costummer_id') == 2) )
										<!-- Módulo AdSmart -->
										<li><a href="{{ url('/site/adsmart') }}"><i class="icon fa fa-4x fa-bullhorn margin-bottom-15"></i>AdSmart</a></li>
										<!-- END Módulo AdSmart -->
									@endif
                            @endif
                        </ul>
                        @if (Auth::user()->admin || Auth::user()->gerencia_embalagem || Auth::user()->gerencia_foto || Auth::user()->gerencia_midia || Auth::user()->gerencia_usuario || Auth::user()->gerencia_fornecedor)
							<ul>
                                <li class="padding-bottom-30">
                                    <ul>

                                        @if (Auth::user()->admin || Auth::user()->gerente || Auth::user()->gerencia_embalagem)
                                        <li>
                                            <ul>
                                                <li><a href="javascript:;" class="submenu-gerenciar-embalagens no-padding-bottom"><i class="icon fa fa-2x fa-cube margin-bottom-10"></i>Gerenciar Embalagens</a></li>
                                            </ul>
                                        </li>
                                        @endif

                                        @if (Auth::user()->admin || Auth::user()->gerente || Auth::user()->gerencia_foto)
                                        <li>
                                            <ul>
                                                <li><a href="{{ url('/admin/sku/fotos') }}" class="no-padding-bottom"><i class="icon fa fa-2x fa-camera margin-bottom-10"></i>Gerenciar Fotos</a></li>
                                            </ul>
                                        </li>
                                        @endif

                                        @if ( (Auth::user()->admin || Auth::user()->gerente || Auth::user()->gerencia_midia) && (config('app.costummer_id') == 1 || config('app.costummer_id') == 'dev' || config('app.costummer_id') == 'stg' || config('app.costummer_id') == 2) )
                                        <li>
                                            <ul>
                                                <li><a href="javascript:;" class="submenu-gerenciar-midias no-padding-bottom"><i class="icon fa fa-2x fa-newspaper-o margin-bottom-10"></i>Gerenciar Mídias</a></li>
                                            </ul>
                                        </li>
                                        @endif

                                        @if (Auth::user()->admin || Auth::user()->gerente || Auth::user()->gerencia_usuario)
                                            <li>
                                                <ul>
                                                    <li><a href="javascript:;" class="submenu-gerenciar-usuarios no-padding-bottom"><i class="icon fa fa-2x fa-user-circle margin-bottom-10"></i>Gerenciar Usuários</a></li>
                                                </ul>
                                            </li>
                                        @endif

                                        @if (Auth::user()->admin || Auth::user()->gerente || Auth::user()->gerencia_fornecedor)
                                            <li>
                                                <ul>
                                                    <li><a href="{{ route('site.fornecedores.get') }}" class="no-padding-bottom"><i class="icon fa fa-2x fa-industry margin-bottom-10"></i>Gerenciar Fornecedores</a></li>
                                                </ul>
                                            </li>
                                        @endif

                                    </ul>
                                </li>
                            </ul>
                            <ul>
                                <li><a href="{{ route('site.dashboard') }}"><i class="icon fa fa-2x fa-tachometer margin-bottom-10"></i>Dashboard</a></li>
                                <li><a href="javascript:;" class="link-ajuda"><i class="icon fa fa-2x fa-graduation-cap margin-bottom-10"></i>Suporte</a></li>
                            </ul>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </header>
	<!-- END Header V2 -->
    <div class="wrapped">
        <!-- Div .main -->
        <div id="main">
            <!-- Div .content -->
            <div class="content content-fluid">
                <div class="min-height-container">
				    @yield('content')
                </div>
                <div class="push"></div>
            </div>
            <!-- END Div .content -->
        </div>
        <!-- END Div .main -->
        <div class="clear-footer"></div>
        <!-- Footer -->
        <div class="footer clearfix">
            <div class="footer-inner content-fluid clearfix">
                <span class="copyright">Rumbrum Software &copy; {{ date("Y") }}</span>
                <ul>
                    <li><a href="javascript:;" class="no-click no-hover">Versão 1.8.1</a></li>
                </ul>
                <div class="footer-right animate">
                    <a target="_blank" href="http://www.rubrum.com.br"></a>
                </div>
            </div>
        </div>
        <!-- END Footer -->
    </div>
</div>
</body>
</html>
