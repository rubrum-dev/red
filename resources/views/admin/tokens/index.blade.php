@extends('admin.admin_template')

@section('title')
	Tokens
@stop

@section('content')

<script src="{{ asset('/js/admin/tokens.js') }}"></script>

<div class="list-title-header">
	<h1><!--span class="glyphicon glyphicon-qrcode"></span-->Tokens</h1>
</div>
<div class="pull-right">
	<p>Total: {{ $total }} tokens</p>
</div>

<div class="table-responsive clear">

@if (count($tokens) >= 1)
	<table id="list-tokens" class="table table-striped">
		<thead>
			<tr>
				<th>Produto</th>
				<th>Token</th>
				<th>Expiração</th>
				<th>Ações</th>
			</tr>
		</thead>
		<tbody>
			@foreach($tokens as $token)
				<tr>
					<td>{{ $token->produtos->nome }}</td>
					<td>{{ $token->token }}</td>
					<td>{{ $token->dt_expiracao }}</td>
					<td>
                        <button type="button" id="btn-edit-{{$token->id}}" class="btn btn-success btn-sm btn-extend" data-token-id="{{ $token->id }}">
							<span class="glyphicon glyphicon-refresh"></span> Renovar Token
						</button>
						<button type="button" id="btn-remove-{{$token->id}}" class="btn btn-danger btn-sm btn-remove" data-token-id="{{ $token->id }}">
							<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Remover
						</button>
                        <button type="button" class="btn btn-primary btn-sm copy-link" data-token-id="{{ $token->id }}" data-token-url="{{ $token->url }}">
							<span class="glyphicon glyphicon-link" aria-hidden="true"></span> Copiar Link
						</button>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	<div>
		<h4>Nenhum Token Localizado</h4>
	</div>
@endif

</div>

@stop
