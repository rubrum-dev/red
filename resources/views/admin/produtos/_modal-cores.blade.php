<!-- Modal -->
<div class="modal fade" id="modalCores" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<span class="glyphicon glyphicon-plus-sign"></span>
				</h4>
			</div>
			<div class="modal-body">

				{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-color')) !!}
					{{ Form::hidden('id_prod', $produto->id, array('id' => 'editIdProd')) }}
					{{ Form::hidden('id', '', array('id' => 'editId')) }}
					<div class="form-group">
						{!! Form::label('lblNome', 'Nome', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
					    	{!! Form::text('nome', '', array('class' => 'form-control', 'id' => 'editNome')) !!}
				    	</div>
				    </div>
					<div class="form-group">
						{!! Form::label('lblTipo', 'Tipo de Cor', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
							{!! Form::select('id_tipo_cor', $tipos, null, array('placeholder' => 'Selecione o Tipo...', 'class' => 'form-control', 'id' => 'editTipo')) !!}
						</div>
					</div>
				    <div class="form-group">
						{!! Form::label('lblNivel', 'Nível de Cor', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
					    	{!! Form::select('id_nivel_cor', $niveis, null, array('placeholder' => 'Selecione o Nivel...', 'class' => 'form-control', 'id' => 'editNivel')) !!}
				    	</div>
				    </div>
				    <div class="form-group">
						{!! Form::label('lblSpot', 'SPOT', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
					    	{!! Form::text('spot', '', array('class' => 'form-control', 'id' => 'editSpot')) !!}
				    	</div>
				    </div>
				    <div class="form-group">
						{!! Form::label('lblCmyk', 'CMYK', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
					    	{!! Form::text('cmyk', '', array('class' => 'form-control', 'id' => 'editCmyk')) !!}
				    	</div>
				    </div>
				    <div class="form-group">
						{!! Form::label('lblRgb', 'RGB', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
					    	{!! Form::text('rgb', '', array('class' => 'form-control', 'id' => 'editRgb')) !!}
				    	</div>
				    </div>
				    <div class="form-group">
						{!! Form::label('lblHex', 'WEB', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-6">
					    	{!! Form::text('hex', '', array('class' => 'form-control', 'id' => 'editHex')) !!}
				    	</div>
				    	<div class="col-sm-2">
					    	{!! Form::text('', '', array('class' => 'form-control', 'id' => 'editBoxColor', 'disabled')) !!}
				    	</div>
				    </div>
				    <div class="form-group">
						{!! Form::label('lblDescricao', 'Descrição', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
					    	{!! Form::textarea('descricao', '', array('class' => 'form-control', 'id' => 'editDescricao')) !!}
				    	</div>
				    </div>
				    <div class="form-group">
						{!! Form::label('lblStatus', 'Status', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
					    	{!! Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), null, array('class' => 'form-control', 'id' => 'editStatus')) !!}
				    	</div>
				    </div>
		    		<div class="modalButtons">
						{!! Form::button('Cancelar', array('class' => 'btn btn-info', 'data-dismiss' => 'modal', 'id' => 'btn-cancel-modal')) !!}
					    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
					</div>

				{!! Form::close() !!}

			</div>
		</div>

	</div>
</div>
