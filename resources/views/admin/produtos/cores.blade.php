@extends('admin.admin_template')

@section('title')
	Cores
@stop

@section('content')

<script src="{{ asset('/js/admin/cores.js') }}"></script>

<div class="list-title-header">
	<h1><span class="glyphicon glyphicon-tree-deciduous"></span> Cores: {{ $produto->nome }}</h1>
</div>

	<!-- Modal de Criação/Edição de Versões -->
	@include('admin.produtos._modal-cores')

@if (session('status'))
    <div class="alert alert-success box-msg">
        {{ session('status') }}
    </div>
@endif

<div class="table-responsive clear prod-relations" id="list-cores" data-prod-id="{{ $produto->id }}">

	@foreach($tiposCores as $tipoCor)
		<?php $a = 1; ?>
		<div class="col-xs-6" style="height: 400px; margin-top: 2%;">
			{{ $tipoCor->nome }}
			<div class="box-vertical-interno">

				@foreach ($niveisCores as $nivelCor)
					<div class="box-horizontal-interno-tipo">
						{{ $nivelCor->nome }}
						<table id="list-tipos-cores" class="table">
							@foreach ($produto->cores as $value)
								@if($nivelCor->id == $value->id_nivel_cor && $tipoCor->id == $value->id_tipo_cor)
									@if ($a == 1)
										<tr>
									@endif

									<td class="col-sm-1" style="border-top: none;">
										<div class="box-product-misuse">
											<center>
												<div class="img-rounded div-color" style="background-color: #{{$value->hex}}"></div>
											</center>
											<div class="box-product-misuse-text">
												{{ $value->nome }} <br />
												{{ $value->status == 1 ? 'Ativo' : 'Inativo'}}
											</div>
											<div>
												<button type="button" id="btn-edit-{{ $value->id }}" class="btn btn-primary btn-xs btn-edit" data-toggle="modal" data-target="#modalCores" data-cor-id="{{ $value->id }}">
													<span class="glyphicon glyphicon-edit"></span>
												</button>
												<button type="button" id="btn-remove-{{ $value->id }}" class="btn btn-danger btn-xs btn-remove" data-cor-id="{{ $value->id  }}">
													<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
												</button>
											</div>
										</div>
									</td>
									@if ($a % 3 == 0)
										</tr>
									@endif
									<?php $a++ ?>

								@endif
							@endforeach
							</tr>
						</table>
					</div>

				@endforeach

			</div>
		</div>
	@endforeach
	<div class="modalButtons">
		<button type="button" id="btn-create" class="btn btn-warning" data-toggle="modal" data-target="#modalCores">
			Nova Cor
		</button>
		{{ Html::link('admin/product/search/id/'.$produto->id, 'Voltar ao Produto', array('class' => 'btn btn-info')) }}
	</div>
</div>

@stop
