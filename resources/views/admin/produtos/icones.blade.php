@extends('admin.admin_template')

@section('title')
	Ícones
@stop

@section('content')

<script src="{{ asset('/js/admin/icones.js') }}"></script>

<div class="list-title-header">
	<h1><span class="glyphicon glyphicon-tree-deciduous"></span> Ícones: {{ $produto->nome }}</h1>
</div>

	<!-- Modal de Criação/Edição de Ícones -->
	@include('admin.produtos._modal-icones')

@if (session('status'))
    <div class="alert alert-success box-msg">
        {{ session('status') }}
    </div>
@endif

<div class="table-responsive clear prod-relations" id="list-icones" data-prod-id="{{ $produto->id }}">

	@foreach($categoriasIcones as $categoriaIcone) 
		<?php $a = 1; ?>
		<div class="col-xs-6" style="height: 400px; margin-top: 2%;">
			{{ $categoriaIcone->nome }}
			<div class="box-vertical-interno">
				
				<table id="list-tipos-icones" class="table">
					@foreach ($produto->icones as $value)
						@if($categoriaIcone->id == $value->id_categoria_icone)
							@if ($a == 1)
								<tr>
							@endif

							<td class="col-sm-1" style="border-top: none;">
								<div class="box-product-misuse">
									{{ Html::image(asset($value->thumb), 'alt', array('class' => 'img-rounded img-logo')) }}
									<div class="box-product-misuse-text">
										{{ $value->descricao }} <br />
										{{ $value->status == 1 ? 'Ativo' : 'Inativo'}}
									</div>
									<div>
										<button type="button" id="btn-edit-{{ $value->id }}" class="btn btn-primary btn-xs btn-edit" data-toggle="modal" data-target="#modalIcones" data-icone-id="{{ $value->id }}">
											<span class="glyphicon glyphicon-edit"></span>
										</button>
										<button type="button" id="btn-remove-{{ $value->id }}" class="btn btn-danger btn-xs btn-remove" data-icone-id="{{ $value->id  }}">
											<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
										</button>
									</div>
								</div>
							</td>
							@if ($a % 3 == 0)
								</tr>
							@endif
							<?php $a++ ?>

						@endif
					@endforeach		
					</tr>
				</table>

			</div>		
		</div>
	@endforeach	
	<div class="modalButtons">	
		<button type="button" id="btn-create" class="btn btn-warning" data-toggle="modal" data-target="#modalIcones">
			Novo Ícone
		</button>
		{{ Html::link('admin/icon-category', 'Gerenciar Categorias', array('class' => 'btn btn-success linkCadastro')) }}
		{{ Html::link('admin/product/search/id/'.$produto->id, 'Voltar ao Produto', array('class' => 'btn btn-info')) }}
	</div>
	
</div>

@if (count($categorias) == 0)
	<div class="alert alert-warning box-msg" style="margin: 10px 0">
        <h5>Para o cadastro de ícones, é necessário que a categoria seja cadastrada antes</h5>
    </div>
@endif



@stop