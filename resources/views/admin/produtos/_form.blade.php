<!-- Modal -->
<div class="modal fade" id="modalProdutos" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close fa fa-times" data-dismiss="modal"></button>
				<h4 class="modal-title">Adicionar Produto</h4>
			</div>
			<div class="modal-loading">
				<div class="line-wobble"></div>
			</div>
			<div class="modal-body">
				{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-product', 'files' => 'true')) !!}
					{{ Form::hidden('id', '', array('id' => 'editId')) }}
					<div class="form-group form-group-flex flex-align-start">
						{!! Form::label('lblNome', 'Produto *', array('class' => 'col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
						<div class="col-xs-12">
							{!! Form::text('nome', '', array('class' => 'form-control', 'id' => 'editNome', 'placeholder' => 'Nome do Produto')) !!}
						</div>
					</div>
					<div class="form-group form-group-flex flex-align-start">
						{!! Form::label('lblFamilia', 'Marca *', array('class' => 'col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
						<div class="col-xs-12">
							<select onChange="$('input[name=cor_hexa]').val($('#editFamilia').find(':selected').data('hexa'));" class="form-control" id="editFamilia" name="id_familia">
								<option selected="selected" value="#kkk">Selecione a Marca</option>
								@foreach ($familias as $familia)
								<option data-hexa="{{ $familia->cor_hexa }}" value="{{ $familia->id }}">{{ $familia->nome }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group form-group-flex flex-align-start">
						{!! Form::label('lblCategoria', 'Categoria de Produto *', array('class' => 'col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
						<div class="col-xs-12">
							{!! Form::select('id_categoria_produto', $categorias, null, array('placeholder' => 'Selecione a Categoria', 'class' => 'form-control', 'id' => 'editCategoria')) !!}
						</div>
					</div>
					<div class="form-group form-group-flex flex-align-start" id="box-ordem">
						{!! Form::label('lblOrdem', 'Ordem de Exibição', array('class' => 'col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
						<div class="col-xs-12">
							{!! Form::select('ordem', [], null, array('class' => 'form-control', 'id' => 'editOrdem')) !!}
						</div>
					</div>
					<div class="form-group form-group-flex flex-align-center no-margin-left no-margin-right margin-top-20 no-margin-bottom">
						<label class="col-xs-3 control-label no-margin"></label>
						<div class="col-xs-12 no-padding">
							<label for="editStatus" class="no-margin">
								{!! Form::checkbox('status', '1', '', array('id' => 'editStatus')) !!} 
								<span class="no-margin-left no-margin-right">Produto Ativo</span>
								<span class="txt-small txt-color-alt margin-left-10">Exibe o produto no ArtWork e PackShelf e em todas as listas de cadastro.</span>
							</label>
						</div>
					</div>
					<div class="form-group no-margin-bottom" id="logos-id">
						@if (count($tiposLogos) >= 1)
							<table id="list-tipos-logos" class="no-margin-bottom">
								<?php $i = 1; ?>
								@foreach ($tiposLogos->reverse() as $tipoLogo)
									<tr>
										<td class="no-padding">
											<div class="form-group form-group-flex flex-align-start no-margin-left no-margin-right margin-top-10 no-margin-bottom">
												<label class="control-label col-xs-3 no-margin no-padding-left no-padding-right txt-right">
													{{ $i == 2 ? 'Logo com Fundo' : '' }}
													{{ $i == 1 ? 'Logo sem Fundo' : '' }}
												</label>
												<div class="name-logo col-xs-12 txt-color-alt">
													<p class="hint txt-small">
														{{ $i == 2 ? 'Formato PNG medindo 155 x 155px com fundo colorido' : '' }}
														{{ $i == 1 ? 'Formato PNG com 90 x 90px com fundo transparente' : '' }}
													</p>
													<div class="d-inline-flex ui-tooltip-2 top">
														<div class="center-logo image-only">
															{{ Html::image(asset(''), 'alt', array('class' => 'img-rounded img-logo', 'id' => $tipoLogo->id )) }}
														</div>
													</div>
													<div class="upload-logo">
														{!! Form::file('images['.$tipoLogo->id.']') !!}
													</div>
												</div>
											</div>
										</td>
									</tr>
									<?php $i++ ?>
								@endforeach
							</table>
						@endif
					</div>
					<div class="form-group form-group-flex flex-align-start no-margin-bottom" id="editFile">
						<table id="list-tipos-logos" class="no-margin">
							<tr>
								<td class="no-padding">
									<div class="form-group form-group-flex flex-align-start no-margin">
										{!! Form::label('lblStatus', 'Logo para Baixar', array('class' => 'col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
										<div class="name-logo col-xs-12 txt-color-alt">
											<p class="hint txt-small">
												Arquivo compactado com todas as versões do logo em vetor e PNG
											</p>
											<div data-title="Nenhum arquivo" class="d-inline-flex ui-tooltip-2 top">
												<div class="center-logo file-archive-only">
													{{ Html::image(asset(''), 'alt', array('class' => 'img-rounded img-logo')) }}
												</div>
											</div>
											<div class="upload-logo">
												{!! Form::file('file') !!}
											</div>
										</div>
									</div>
								</td>
							</tr>
						</table>
					</div>
					<div class="form-group form-group-flex flex-align-center no-margin-left no-margin-right margin-top-20 margin-bottom-20">
						<label class="col-xs-3 control-label no-margin"></label>
						<div class="colUseFamilyLogo col-xs-12 no-padding">
							<label for="chkUseFamilyLogo" class="no-margin">
								{!! Form::checkbox('useFamilyLogo', '1', '', array('id' => 'chkUseFamilyLogo')) !!} 
								<span class="no-margin-left no-margin-right">Replicar o logo usado em Marca</span>
							</label>
						</div>
					</div>
					<div class="form-group form-group-flex margin-top-15 margin-bottom-15" id="logo-cor">
						<table id="list-tipos-logos" class="no-margin">
							<tr>
								<td class="no-padding">
									<div class="form-group form-group-flex flex-align-start no-margin">
										{!! Form::label('lblCorHexa', 'Definir cor do fundo', array('class' => 'col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
										<div class="name-logo col-xs-12 txt-color-alt">
											<div class="row">
												<div class="col-xs-12">	
													{!! Form::text('cor_hexa', '', array('class' => 'form-control minicolors', 'id' => 'cor_hexa', 'placeholder' => '#FFFFFF', 'autocomplete' => 'off')) !!}
												</div>
											<div>
										</div>
									</div>
								</td>
							</tr>
						</table>
					</div>
					<div class="modalBtnGroup">
						{!! Form::button('Cancelar', array('class' => 'btn-control btn-alt', 'data-dismiss' => 'modal', 'id' => 'btn-cancel-modal')) !!}
						{!! Form::submit('Salvar', array('class' => 'btn-submit btn-control btn-positive')) !!}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
<div id="packShelfImgProgress" class="progress-circle animate">
	<svg id="svg" viewbox="0 0 100 100">
		<circle cx="50" cy="50" r="40" fill="none"/>
		<path fill="none" stroke-linecap="round" stroke-width="4" d="M50 10 a 40 40 0 0 1 0 80 a 40 40 0 0 1 0 -80"/>
		<text id="count" class="progress" x="50" y="50" text-anchor="middle" dy="7"></text>
	</svg>
</div>
