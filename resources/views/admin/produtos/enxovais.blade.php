@extends('admin.admin_template')

@section('title')
	Enxovais
@stop

@section('content')

<script src="{{ asset('/js/admin/enxovais.js') }}"></script>

<div class="list-title-header">
	<h1><span class="glyphicon glyphicon-tree-deciduous"></span> Enxovais: {{ $produto->nome }}</h1>
</div>

	<!-- Modal de Criação/Edição de Enxovais -->
	@include('admin.produtos._modal-enxovais')

@if (session('status'))
    <div class="alert alert-success box-msg">
        {{ session('status') }}
    </div>
@endif

<div class="table-responsive clear prod-relations" id="list-enxovais" data-prod-id="{{ $produto->id }}">

		<?php $a = 1; ?>
		<div class="col-xs-12" style="height: 400px;">
			<div class="box-vertical-interno">
				<div class="box-horizontal-interno-tipo">
					<table id="list-tipos-enxovais" class="table">
						@foreach ($produto->enxovais as $value)
							@if ($a == 1)
								<tr>
							@endif

							<td class="col-sm-1" style="border-top: none;">
								<div class="box-family-outfit">
									{{ Html::image(asset($value->thumb), 'alt', array('class' => 'img-rounded img-logo')) }}
									<div class="box-family-outfit-text">
										{{ $value->nome }} <br />
										{{ $value->status == 1 ? 'Ativo' : 'Inativo'}}
									</div>
									<div>
										<button type="button" id="btn-edit-{{ $value->id }}" class="btn btn-primary btn-xs btn-edit" data-toggle="modal" data-target="#modalEnxovais" data-enxoval-id="{{ $value->id }}">
											<span class="glyphicon glyphicon-edit"></span>
										</button>
										<button type="button" id="btn-remove-{{ $value->id }}" class="btn btn-danger btn-xs btn-remove" data-enxoval-id="{{ $value->id  }}">
											<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
										</button>
									</div>
								</div>
							</td>
							@if ($a % 4 == 0)
								</tr>
							@endif
							<?php $a++ ?>
						@endforeach
						</tr>
					</table>
				</div>
			</div>
		</div>
	<div class="modalButtons">
		<button type="button" id="btn-create" class="btn btn-warning" data-toggle="modal" data-target="#modalEnxovais">
			Novo Enxoval
		</button>
		{{ Html::link('admin/product', 'Voltar ao Produto', array('class' => 'btn btn-info')) }}
	</div>

</div>

@stop
