@extends('admin.admin_template')

@section('title')
	Tipografias
@stop

@section('content')

<div class="list-title-header">
	<h1><span class="glyphicon glyphicon-tree-deciduous"></span> Tipografias: {{ $produto->nome }}</h1>
</div>

@if (session('status'))
    <div class="alert alert-success box-msg">
        {{ session('status') }}
    </div>
@endif

<div class="table-responsive clear prod-relations">

	{!! Form::open(array('class' => 'form-horizontal')) !!}
		{{ Form::hidden('id', $produto->id, array('id' => 'editId')) }}

		@foreach($tiposTipografias as $tipoTipografia) 
			<?php $a = 1; ?>
			<div class="col-xs-6" style="height: 400px; margin-top: 2%;">
				{{ $tipoTipografia->nome }}
				<div class="box-vertical-interno">
					<table id="list-tipos-tipografias" class="table">
						@foreach ($tipografias as $value)
							<?php $checked = null; ?>
							
							@if($tipoTipografia->id == $value->id_tipo_tipografia)
								@if ($a == 1)
									<tr>
								@endif
								<?php 
									if(in_array($value->id, $arrayTipografias))
								 		$checked = 'checked'; 
								?>
								<td class="col-sm-1" style="border-top: none;">
									<div class="box-product-typographys">
										{{ Html::image(asset($value->thumb), 'alt', array('class' => 'img-rounded img-logo')) }}
										<div class="box-product-typographys-text">
											{{ $value->nome }} <br />
											{!! Form::checkbox('tipografias[]', $value->id, null, array('id' => 'editTypographys'.$value->id, $checked)) !!}
										</div>
									</div>
								</td>
								@if ($a % 2 == 0)
									</tr>
								@endif
								<?php $a++ ?>
							@endif	

						@endforeach		
						</tr>		
					</table>
				</div>		
			</div>
		@endforeach	
		<div class="modalButtons">	
			{{ Html::link('admin/typography', 'Gerenciar Tipografias', array('class' => 'btn btn-warning linkCadastro')) }}	
			{{ Html::link('admin/product/search/id/'.$produto->id, 'Voltar ao Produto', array('class' => 'btn btn-info')) }}	
		    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}	
		</div>
	{!! Form::close() !!}
</div>



@stop