@extends('admin.admin_template')

@section('title')
	Versões
@stop

@section('content')

<script src="{{ asset('/js/admin/versoes.js') }}"></script>

<div class="list-title-header">
	<h1><span class="glyphicon glyphicon-tree-deciduous"></span> Versões: {{ $produto->nome }}</h1>
</div>

	<!-- Modal de Criação/Edição de Versões -->
	@include('admin.produtos._modal-versoes')

@if (session('status'))
    <div class="alert alert-success box-msg">
        {{ session('status') }}
    </div>
@endif

<div class="table-responsive clear prod-relations" id="list-versoes" data-prod-id="{{ $produto->id }}">

	@foreach($categoriasVersoes as $categoriaVersao)
		<?php $a = 1; ?>
		<div class="col-xs-6" style="height: 400px; margin-top: 2%;">
			{{ $categoriaVersao->nome }}
			<div class="box-vertical-interno">

				@foreach ($tiposProporcoesVersoes as $tipoProporcaoVersao)

					<div class="box-horizontal-interno-tipo">
						{{ $tipoProporcaoVersao->nome }}
						<table id="list-tipos-versoes" class="table">
							@foreach ($produto->versoes as $value)
								@if($tipoProporcaoVersao->id == $value->id_tipo_proporcao_versao && $categoriaVersao->id == $value->id_categoria_versao)
									@if ($a == 1)
										<tr>
									@endif								

									<td class="col-sm-1" style="border-top: none;">
										<div class="box-product-misuse">
											{{ Html::image(asset($value->thumb), 'alt', array('class' => 'img-rounded img-logo')) }}
											<div class="box-product-misuse-text">
												{{ $value->descricao }} <br />
												{{ $value->status == 1 ? 'Ativo' : 'Inativo'}}
											</div>
											<div>
												<button type="button" id="btn-edit-{{ $value->id }}" class="btn btn-primary btn-xs btn-edit" data-toggle="modal" data-target="#modalVersoes" data-versao-id="{{ $value->id }}">
													<span class="glyphicon glyphicon-edit"></span>
												</button>
												<button type="button" id="btn-remove-{{ $value->id }}" class="btn btn-danger btn-xs btn-remove" data-versao-id="{{ $value->id  }}">
													<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
												</button>
											</div>
										</div>
									</td>
									@if ($a % 3 == 0)
										</tr>
									@endif
									<?php $a++ ?>

								@endif
							@endforeach
							</tr>
						</table>
					</div>

				@endforeach

			</div>
		</div>
	@endforeach
	<div class="modalButtons">
		<button type="button" id="btn-create" class="btn btn-warning" data-toggle="modal" data-target="#modalVersoes">
			Nova Versão
		</button>
		{{ Html::link('admin/product/search/id/'.$produto->id, 'Voltar ao Produto', array('class' => 'btn btn-info')) }}
	</div>

</div>



@stop
