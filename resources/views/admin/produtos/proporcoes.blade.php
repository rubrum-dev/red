@extends('admin.admin_template')

@section('title')
	Proporções
@stop

@section('content')

<script src="{{ asset('/js/admin/proporcoes.js') }}"></script>

<div class="list-title-header">
	<h1><span class="glyphicon glyphicon-tree-deciduous"></span> Proporções: {{ $produto->nome }}</h1>
</div>

	<!-- Modal de Criação/Edição de Proporções -->
	@include('admin.produtos._modal-proporcoes')

@if (session('status'))
    <div class="alert alert-success box-msg">
        {{ session('status') }}
    </div>
@endif

<div class="table-responsive clear prod-relations" id="list-proporcoes" data-prod-id="{{ $produto->id }}">

	@foreach($categoriasProporcoes as $categoriaProporcao) 
		<?php $a = 1; ?>
		<div class="col-xs-6" style="height: 400px; margin-top: 2%;">
			{{ $categoriaProporcao->nome }}
			<div class="box-vertical-interno">
				
				@foreach ($tiposProporcoesVersoes as $tipoProporcaoVersao)

					<div class="box-horizontal-interno-tipo">
						{{ $tipoProporcaoVersao->nome }}
						<table id="list-tipos-proporcoes" class="table">
							@foreach ($produto->proporcoes as $value)
								@if($tipoProporcaoVersao->id == $value->id_tipo_proporcao_versao && $categoriaProporcao->id == $value->id_categoria_proporcao)
									@if ($a == 1)
										<tr>
									@endif

									<td class="col-sm-1" style="border-top: none;">
										<div class="box-product-misuse">
											{{ Html::image(asset($value->thumb), 'alt', array('class' => 'img-rounded img-logo')) }}
											<div class="box-product-misuse-text">
												{{ $value->descricao }} <br />
												{{ $value->status == 1 ? 'Ativo' : 'Inativo'}}
											</div>
											<div>
												<button type="button" id="btn-edit-{{ $value->id }}" class="btn btn-primary btn-xs btn-edit" data-toggle="modal" data-target="#modalProporcoes" data-proporcao-id="{{ $value->id }}">
													<span class="glyphicon glyphicon-edit"></span>
												</button>
												<button type="button" id="btn-remove-{{ $value->id }}" class="btn btn-danger btn-xs btn-remove" data-proporcao-id="{{ $value->id  }}">
													<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
												</button>
											</div>
										</div>
									</td>
									@if ($a % 3 == 0)
										</tr>
									@endif
									<?php $a++ ?>

								@endif
							@endforeach		
							</tr>
						</table>
					</div>

				@endforeach

			</div>		
		</div>
	@endforeach	
	<div class="modalButtons">	
		<button type="button" id="btn-create" class="btn btn-warning" data-toggle="modal" data-target="#modalProporcoes">
			Nova Proporção
		</button>
		{{ Html::link('admin/product/search/id/'.$produto->id, 'Voltar ao Produto', array('class' => 'btn btn-info')) }}
	</div>
	
</div>



@stop