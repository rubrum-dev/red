<!-- Modal -->
<div class="modal fade" id="modalUsosAplicacoes" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<span class="glyphicon glyphicon-plus-sign"></span>
				</h4>
			</div>
			<div class="modal-body">

				{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-application', 'files' => 'true')) !!}
					{{ Form::hidden('id_prod', $produto->id, array('id' => 'editIdProd')) }}
					{{ Form::hidden('id', '', array('id' => 'editId')) }}
				    <div class="form-group">
						{!! Form::label('lblTipo', 'Tipo', array('class' => 'col-sm-3 control-label')) !!}	
						<div class="col-sm-8">
					    	{!! Form::select('id_tipo_uso_aplicacao', $tipos, null, array('placeholder' => 'Selecione o Tipo...', 'class' => 'form-control', 'id' => 'editUsoAplicacao')) !!}
				    	</div>
				    </div>	
					<div class="form-group">
						{!! Form::label('lblDescricao', 'Descrição', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
					    	{!! Form::textarea('descricao', '', array('class' => 'form-control', 'id' => 'editDescricao')) !!}
				    	</div>
				    </div>
				    <div class="form-group">
						{!! Form::label('lblStatus', 'Status', array('class' => 'col-sm-3 control-label')) !!}	
						<div class="col-sm-8">
					    	{!! Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), null, array('class' => 'form-control', 'id' => 'editStatus')) !!}
				    	</div>
				    </div>	
				    <div class="form-group" id="list-tipos-logos">
						{!! Form::label('lblImagem', 'Imagem', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-2 box-product-application">
							{{ Html::image(asset('/images/backend/noImage.png'), 'alt', array('class' => 'img-rounded img-logo', 'id' => 1 )) }}</div>
						<div class="col-sm-2 upload-application">
							{!! Form::file('image') !!}
						</div>
					</div>
					<div class="modalButtons">
						{!! Form::button('Cancelar', array('class' => 'btn btn-info', 'data-dismiss' => 'modal', 'id' => 'btn-cancel-modal')) !!}
					    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}	
					</div>

				{!! Form::close() !!}

			</div>
		</div>

	</div>
</div>