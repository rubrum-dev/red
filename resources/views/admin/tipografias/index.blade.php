@extends('admin.admin_template')

@section('title')
	Tipografias
@stop

@section('content')

<script src="{{ asset('/js/admin/tipografias.js') }}"></script>
<script>
	$(function () {
		$('#btn-search').click(function () {
			var src = $('#txt-search').val();
			src.trim() == '' ? window.location = '<?php echo url('admin/typography'); ?>' : window.location = '<?php echo url('admin/typography/search'); ?>/' + src;
		});
	});
</script>

<div class="list-title-header">
	<h1><!--span class="glyphicon glyphicon-gbp"></span-->Tipografias</h1>
</div>
<div class="pull-right">
	<p>Total: {{ $total }} tipografias</p>
</div>

<div class="list-header">
	<div>
		<div class="input-group col-xs-6 search">
			<input type="text" id="txt-search" class="form-control" placeholder="Buscar por Nome ...">

			<span class="input-group-btn">
				<button id="btn-search" class="btn btn-default" type="button">Buscar</button>
			</span>
		</div>

		{{ Html::link('admin/product', 'Produtos', array('class' => 'btn btn-warning btn-search-left')) }}

		<button type="button" id="btn-create" class="btn btn-info" data-toggle="modal" data-target="#modalTipografias">
			<span class="glyphicon glyphicon-plus-sign"></span> Nova Tipografia
		</button>

		<!-- Modal de Criação/Edição de Tipografia -->
		@include('admin.tipografias._form')

	</div>
</div>

<div class="table-responsive clear">

@if (count($tipografias) >= 1)
	<table id="list-typography" class="table table-striped">
		<thead>
			<tr>
				<th>Nome</th>
				<th>Tipo</th>
				<th>Status</th>
				<th>Ações</th>
			</tr>
		</thead>
		<tbody>
			@foreach($tipografias as $tipografia)
				<tr>
					<td>{{ $tipografia->nome }}</td>
					<td>{{ $tipografia->tipo->nome }}</td>
					<td>{{ $tipografia->status ? 'Ativo' : 'Inativo' }}</td>
					<td>
						<button type="button" id="btn-edit-{{$tipografia->id}}" class="btn btn-primary btn-sm btn-edit" data-toggle="modal" data-target="#modalTipografias" data-typography-id="{{ $tipografia->id }}">
							<span class="glyphicon glyphicon-edit"></span> Editar
						</button>
						<button type="button" id="btn-remove-{{$tipografia->id}}" class="btn btn-danger btn-sm btn-remove" data-typography-id="{{ $tipografia->id }}">
							<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Remover
						</button>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	<div>
		<h4>Nenhuma Tipografia Localizada</h4>
	</div>
@endif

</div>

@stop
