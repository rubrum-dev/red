@extends('site.site_template')
@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop
@section('content')
<div class="breadcumbs-out clearfix">		
    <div class="breadcrumbs-inner content-fluid clearfix">	
        <ul>
            <li class="active">Tipos de Instrução</li>
        </ul>
    </div>
</div>
<script>
    var csrf_token = '{{ csrf_token() }}';
</script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- DataTables -->
<script src="{{ asset('/js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/datatables/accent-neutralise.js') }}"></script>

<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/controllers/admin_tipos_instrucao.js') }}"></script>
<div ng-app="app" ng-controller="adminTiposInstrucaoCtrl" class="content content-fluid clearfix">
    <section class="artwork-container manager-container margin-top-10 margin-bottom-40">
        <h3 class="master-title">Gerenciar Tipos de Instrução</h3>
        <form name="instructionTypesForm" action="" method="POST" novalidate>
            <input type="hidden" name="_token" value="{{ csrf_token()}}" />
            <section>
                <form>
                    <table cellpading="0" cellspacing="0" id="instructionTypesManagerTable" class="manager-table table-border table-hover">
                        <thead>
                            <tr>
                                <th width=""><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Tipos de Instrução</small></span></th>
                                <th width="120"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Estado</small></span></th>
                                <th width="50"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- TO-DO Loop Tipos de Instrução -->
                            <tr>
                                <td width="">
                                    <a href="{{ route('admin.tipos-instrucao.editar') }}" class="flex-wrap">
                                        <i class="icon fa fa-tasks"></i>
                                        <span class="text-side">Teste 1</span>
                                    </a>
                                </td>
                                <td width="120">Ativo</td>
                                <td width="50">
                                    <div class="dropdown drop-to-left">
                                        <a href="javascript:;" class="tooltip top dropdown-toggle fa fa-ellipsis-h"><span class="tooltip-box">Opções</span></a>
                                        <div class="dropdown-list">
                                            <ul> 
                                                <li><a href="{{ route('admin.tipos-instrucao.editar') }}" class="txt-bold"><span class="icon fa fa-edit"></span>Alterar</a></li>
                                                <li><a href="javascript:;" ng-click="excluir_tipo_instrucao(id)" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!-- END TO-DO Loop Tipos de Instrução -->
                        </tbody>
                    </table>
                </form>
            </section>
        </form>
    </section>
    <script>
        $(function() {
            var dataTableToolbarBtnDOM = '<div class="flex flex-auto">' +
                '<a href="{{ route('admin.tipos-instrucao.adicionar') }}" class="btn-function"><i class="icon fa fa-tasks"></i> Criar Tipo de Instrução</a>' +
            '</div>';
            var table = $('#instructionTypesManagerTable').DataTable({
                dom: '<"datatable-toolbar flexbox-container clearfix"' +
                        '<"datatable-filter-container flex flex-3-large"f>' +
                        '<"datatable-filter-container filter-by-status flex flex-3-large">' +
                    '>' +
                    '<"clearfix"' +
                        '<tr>' +
                    '>' +
                    '<"datatable-pagination flexbox-container flex-align-center clearfix"' +
                        '<"flex"<"select-control"l>><"flex flex-auto"p><"flex"i>' +
                    '>',
                bAutoWidth: false,
                processing: true,
                serverSide: false,
                stateSave: true,
                pageLength: 25,
                lengthMenu: [
                    [10, 25, 50, -1], 
                    ['Exibir 10 por página', 'Exibir 25 por página', 'Exibir 50 por página', 'Exibir todos']
                ],
                language: {
                    "url": "/js/datatables/language/Portuguese-Brasil.json"
                },
                initComplete: function () {
                    this.api().columns().every( function () {
                        var column = this;
                        var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
                                column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();
                            });
                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                    });
                    $('.datatable-filter-container .dataTables_filter input[type="search"]').attr('placeholder', 'Busca');
                    var dataTables_filter_cancel = '<i></i>';
                    var dataTables_filter_search = '<i></i>';
                    
                    $('.datatable-filter-container .dataTables_filter').append(dataTables_filter_search);
                    $('.datatable-filter-container .dataTables_filter').append(dataTables_filter_cancel);
                    $('.datatable-filter-container .dataTables_filter i').addClass('dataTables_filter_search');
                    $('.datatable-filter-container .dataTables_filter i + i').addClass('dataTables_filter_cancel');
                    
                    function dataTablesFilterCancelShowHide() {
                        if ($('.datatable-filter-container .dataTables_filter input[type="search"]').val() !== '') {
                            $('.dataTables_filter_search').hide();
                            $('.dataTables_filter_cancel').show();
                        
                        } else {
                            $('.dataTables_filter_search').show();
                            $('.dataTables_filter_cancel').hide();
                            
                            table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
                        }
                    }
                    
                    $(document).on('click', '.datatable-filter-container .dataTables_filter_cancel', function () {
                        $('#myTable6_filter input[type="search"]').val('');
                        
                        dataTablesFilterCancelShowHide();
                    });
                    // Remove accented character from search input as well
                    $(document).on('keyup', '#myTable6_filter input[type="search"]', function () {
                        table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
                        
                        dataTablesFilterCancelShowHide();
                    });
                    
                    $('.dataTable thead').show();
                    $('.dataTables_filter').show();
                    $('.datatable-toolbar').append(dataTableToolbarBtnDOM);
                },
                fnDrawCallback: function( oSettings ) {
                    if(oSettings.aiDisplay.length <= 0) {
                        $('.dataTable thead th').hide();
                        $('.datatable-pagination').hide();
                        $('.dataTables_empty').addClass('fa-tasks');
                        $('.dataTables_empty').text('Nenhum tipo de instrução encontrado.');
                    } else {
                        $('.dataTable thead th').show();
                        $('.datatable-pagination').show();
                        $('.dataTables_empty').removeClass('fa-tasks');
                        $('.dataTables_empty').text('');
                    }
                }
            });
        });
    </script>
</div>
@stop