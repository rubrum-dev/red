<!-- Modal -->
<div class="modal fade" id="modalEmbalagens" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close fa fa-times" data-dismiss="modal"></button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package')) !!}
					{{ Form::hidden('ordemCheck', $count == [] ? 0 : count($count), array('id' => 'editOrdemId')) }}
					{{ Form::hidden('id', '', array('id' => 'editId')) }}
					<div class="form-group form-group-flex flex-align-start">
						{!! Form::label('lblNome', 'Nome *', array('class' => 'col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
						<div class="col-xs-12">
					    	{!! Form::text('nome', '', array('class' => 'form-control', 'id' => 'editNome', 'placeholder' => 'Nome do Tipo de Embalagem')) !!}
				    	</div>
				    </div>
					<div class="form-group form-group-flex flex-align-start" id="box-ordem">
						{!! Form::label('lblOrdem', 'Ordem de Exibição', array('class' => 'col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
						<div class="col-xs-12">
					    	{!! Form::select('ordem', $count, null, array('class' => 'form-control', 'id' => 'editOrdem')) !!}
				    	</div>
				    </div>
				    <div class="modalBtnGroup">
						{!! Form::button('Cancelar', array('class' => 'btn-control btn-alt', 'data-dismiss' => 'modal', 'id' => 'btn-cancel-modal')) !!}
					    {!! Form::submit('Salvar', array('class' => 'btn-control btn-positive')) !!}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
