<!-- Modal -->
<div class="modal fade" id="modalVolumes" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close fa fa-times" data-dismiss="modal"></button>
				<h4 class="modal-title">Adicionar Volume</h4>
			</div>
			<div class="modal-loading">
                <div class="line-wobble"></div>
            </div>
			<div class="modal-body">
				{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package', 'files' => 'true')) !!}
					{{ Form::hidden('id', '', array('id' => 'editId')) }}
					<div class="form-group form-group-flex flex-align-start">
						{!! Form::label('lblNome', 'Volume *', array('class' => 'col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
						<div class="col-xs-12">
					    	{!! Form::text('nome', '', array('placeholder' => 'Nome do Volume', 'class' => 'form-control', 'id' => 'editNome')) !!}
				    	</div>
				    </div>
				    <div class="form-group form-group-flex flex-align-start">
						{!! Form::label('lblEmbalagem', 'Tipo de Embalagem *', array('class' => 'col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right')) !!}	
						<div class="col-xs-12">
					    	{!! Form::select('id_tipo_embalagem', $tiposEmbalagens, null, array('placeholder' => 'Selecione o Tipo de Embalagem', 'class' => 'form-control', 'id' => 'editEmbalagem')) !!}
				    	</div>
				    </div>
					<div class="form-group form-group-flex flex-align-start" id="box-ordem">
						{!! Form::label('lblOrdem', 'Ordem de Exibição', array('class' => 'col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
						<div class="col-xs-12">
							{!! Form::text('ordem', '', array('class' => 'form-control', 'id' => 'editOrdem', 'placeholder' => 'Ordem de Exibição')) !!}
				    	</div>
				    </div>
				    {{--<div class="form-group form-group-flex flex-align-center no-margin-bottom">--}}
						{{--
						{!! Form::label('lblStatus', 'Status', array('class' => 'col-xs-3 control-label no-margin no-padding txt-right')) !!}	
						<div class="col-xs-12">
							{!! Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), null, array('class' => 'form-control', 'id' => 'editStatus')) !!}
						</div>
						--}}
						{{--<label class="col-xs-3 control-label"></label>
						<div class="col-xs-12">
							<div class="row">
								<label for="editStatus" class="col-xs-12 no-margin">
									{!! Form::checkbox('status', '1', '', array('id' => 'editStatus')) !!} 
									<span class="no-margin-left no-margin-right">Volume Ativo</span>
									<span class="txt-small txt-color-alt margin-left-10">Exibe na lista de volumes disponíveis no cadastro de embalagem.</span>
								</label>
							</div>
						</div>--}}
				    {{--</div>--}}
				    {{--<div class="form-group form-group-flex flex-align-start" id="listImage">
						{!! Form::label('lblThumb', 'Thumb', array('class' => 'col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right')) !!}	
						<div class="col-xs-12">
							<table id="list-tipos-logos" class="table">
								<tr>
									<td class="col-xs-2 no-padding-left no-padding-right">
							    		{{ Html::image(asset('/images/backend/noImage.png'), 'alt', array('class' => 'img-rounded img-logo')) }}
							    	</td>
						    	</tr>
						    	<tr>
									<td class="col-xs-2 no-padding-left no-padding-right">
										{!! Form::file('image') !!}
									</td>									
								</tr>
							</table>
				    	</div>
			    	</div>--}}			    
		    		<div class="modalBtnGroup">
						{!! Form::button('Cancelar', array('class' => 'btn-control btn-alt', 'data-dismiss' => 'modal', 'id' => 'btn-cancel-modal')) !!}
					    {!! Form::submit('Salvar', array('class' => 'btn-control btn-positive')) !!}	
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
{{--<div id="packShelfImgProgress" class="progress-circle animate">
	<svg id="svg" viewbox="0 0 100 100">
		<circle cx="50" cy="50" r="45" fill="#004192"/>
		<path fill="none" stroke-linecap="round" stroke-width="5" stroke="#fff" d="M50 10 a 40 40 0 0 1 0 80 a 40 40 0 0 1 0 -80"/>
		<text id="count" class="progress" x="50" y="50" text-anchor="middle" dy="7" font-size="20" fill="#fff"></text>
	</svg>
</div>--}}