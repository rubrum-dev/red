@extends('admin.admin_template')
@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/datatables/dataTables.bootstrap.min.css?v=1.0') }}" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script>
	var categorias = {!! json_encode($categorias) !!};
</script>

<script type="text/javascript" src="{{ asset('/js/admin/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/admin/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/datatables/accent-neutralise.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/datatables/absolute.js') }}"></script>
<script type="text/javascript" src="{{ asset2('/js/admin/categorias-produtos.js') }}"></script>
<script defer type="text/javascript" src="{{ asset('/js/site/dataTables-resolve-loader.js') }}"></script>

<div class="admin-container">
	<div class="list-title-header">
		<h3>Gerenciar Categorias de Produtos</h3>
	</div>
	@include('admin.categorias-produtos._form')
	<div class="table-responsive clear">
		<div class="form-group-add">
			<button type="button" id="btn-create" class="btn btn-sm btn-function" data-toggle="modal" data-target="#modalCategorias">
				<i class="icon fa fa-tags"></i> 
				<span class="txt-semibold">Adicionar Categoria de Produto</span>
			</button>
		</div>
		<div class="dt-content-container">
			<table style="display:none;" id="list-category" class="table table-striped">
				<thead>
					<tr>
						<th width=""><span>Categorias de Produto</span></th>
						<th width="30"><span></span></th>
					</tr>
				</thead>
				<tbody>
					@foreach($categorias as $categoria)
						<tr>
							<td width="">
								<a href="javascript:;" class="txt-no-link">
									<i class="icon fa fa-tags"></i>
									<span class="text-side">{{ $categoria->nome }}</span>
								</a>
							</td>
							<td width="30">
								<div class="dataTable_btn_group">
									<div class="dropdown drop-to-left">
										<a href="javascript:;" class="ui-tooltip top dropdown-toggle fa fa-ellipsis-h"><span class="tooltip-box">Opções</span></a>
										<div class="dropdown-list">
											<ul> 
												<li><a href="javascript:;" data-toggle="modal" data-target="#modalCategorias" data-category-id="{{ $categoria->id }}" id="btn-edit-{{$categoria->id}}" class="btn-edit txt-bold"><span class="icon fa fa-edit"></span>Alterar</a></li>
												@if (Auth::user()->id_adm_perfil == 1)
													<li><a href="javascript:;" id="btn-remove-{{$categoria->id}}" data-category-id="{{ $categoria->id }}" class="btn-remove txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
												@endif
											</ul>
										</div>
									</div>
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<script>
			$(function() {
				$('.table').on( 'init.dt', function (e, settings) {
                	$('.dataTable-bottom').hide();
					
					setTimeout(function() {
						if(settings.aiDisplay.length > 0) {
							$('.dataTable-bottom').show();
						}		
					}, new Date().getMilliseconds());
				});

				var table = $('#list-category').DataTable({
					dom: '<"dataTable-head"<"dataTable-filter"<"dataTable-filter-in row"<"dataTable-searchbox-container col-xs-4"f>>>>' +
						'<<"table-responsive"tr>>' +
					'<"dataTable-bottom"<"dataTable-selectbox-container"l><"dataTables_paginate_flex"p><i>>',
					bAutoWidth: false,
					processing: false,
					serverSide: false,
					stateSave: true,
					bStateSave: true,
					order: [
						[0, 'asc']
					],
                    pageLength: 25,
					lengthMenu: [
						[10, 25, 50, -1], 
						['Exibir 10 por página', 'Exibir 25 por página', 'Exibir 50 por página', 'Exibir todos']
					],
					columnDefs: [
						{ orderable: false, targets: 1 }
					],
					language: {
						'url': '/js/datatables/language/Portuguese-Brasil.json'
                    },
					initComplete: function () {
						var dataTables_filter_search = '<i></i>';
						var dataTables_filter_cancel = '<i></i>';

						$('.dataTable-searchbox-container input[type="search"]').attr('placeholder', 'Buscar');
						
						$('.dataTable-searchbox-container .dataTables_filter').append(dataTables_filter_search);
						$('.dataTable-searchbox-container .dataTables_filter').append(dataTables_filter_cancel);
						$('.dataTable-searchbox-container .dataTables_filter i').addClass('dataTables_filter_search');
						$('.dataTable-searchbox-container .dataTables_filter i + i').addClass('dataTables_filter_cancel');

						function dataTablesFilterCancelShowHide() {
							if ($('.dataTable-searchbox-container .dataTables_filter input[type="search"]').val() !== '') {
								$('.dataTables_filter_search').hide();
								$('.dataTables_filter_cancel').show();
							
							} else {
								$('.dataTables_filter_search').show();
								$('.dataTables_filter_cancel').hide();
								
								table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
							}
						}

						$(document).on('click', '.dataTable-searchbox-container .dataTables_filter_cancel', function () {
							$('.dataTables_filter input[type="search"]').val('');
							
							dataTablesFilterCancelShowHide();
						});

						$(document).on('keyup', '.dataTables_filter input[type="search"]', function () {
							dataTablesFilterCancelShowHide();
						});

						$('.dataTable').show();
						$('.dataTable thead').show();
						$('.dataTable tbody').show();
						$('.dataTables_filter').show();
					},
					fnDrawCallback: function( oSettings ) {
						if(oSettings.aiDisplay.length <= 0) {
							$('.dataTable thead tr').hide();
							$('.dataTable-bottom').hide();
							$('.dataTables_empty').addClass('fa-tags');
							$('.dataTables_empty').text('Nenhuma categoria de produto encontrada.');
						} else {
							$('.dataTable thead tr').show();
							$('.dataTable-bottom').show();
							$('.dataTables_empty').removeClass('fa-tags');
							$('.dataTables_empty').text('');
						}
					}
				});
			});
		</script>
	</div>
</div>
@stop
