<!-- Modal -->
<div class="modal fade" id="modalFtps" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<span class="glyphicon glyphicon-plus-sign"></span>
				</h4>
			</div>
			<div class="modal-body">

				{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-ftps', 'files' => 'true')) !!}
					{{ Form::hidden('id', '', array('id' => 'editId')) }}
					<div class="form-group">
						{!! Form::label('lblNome', 'Nome', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
					    	{!! Form::text('nome', '', array('class' => 'form-control', 'id' => 'editNome')) !!}
				    	</div>
				    </div>
			    	<div class="form-group" id="listFile">
				    	{!! Form::label('lblArquivo', 'Arquivo', array('class' => 'col-sm-3 control-label')) !!}
				    	<div class="col-sm-8">
							<table id="list-tipos-logos" class="table">
								<tr>
									<td class="col-sm-2">
							    	{{ Html::image(asset('/images/backend/noFile.png'), 'alt', array('class' => 'img-rounded img-logo')) }}
							    	</td>
						    	</tr>
						    	<tr>
									<td class="col-sm-2">
										{!! Form::file('file') !!}
									</td>
								</tr>
							</table>
				    	</div>
				    </div>
					<center id="center-gif" style="display: none;">
						<img src="{{ asset('/images/common/loading.gif') }}" alt="Logo" width="50">
					</center>
		    		<div class="modalButtons">
						{!! Form::button('Cancelar', array('class' => 'btn btn-info', 'data-dismiss' => 'modal', 'id' => 'btn-cancel-modal')) !!}
					    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
					</div>

				{!! Form::close() !!}

			</div>
		</div>

	</div>
</div>
