@extends('admin.admin_template')

@section('title')
	Arquivos
@stop

@section('content')

<script src="{{ asset('/js/admin/ftps.js') }}"></script>
<script>
	$(function () {
		$('#btn-search').click(function () {
			var src = $('#txt-search').val();
			src.trim() == '' ? window.location = '<?php echo url('admin/ftp'); ?>' : window.location = '<?php echo url('admin/ftp/search'); ?>/' + src;
		});
	});
</script>

<div class="list-title-header">
	<h1>Arquivos</h1>
</div>
<div class="pull-right">
	<p>Total: {{ $total }} arquivos</p>
</div>

<div class="list-header">
	<div>
		<div class="input-group col-xs-6 search">
			<input type="text" id="txt-search" class="form-control" placeholder="Buscar por Nome ...">

			<span class="input-group-btn">
				<button id="btn-search" class="btn btn-default" type="button">Buscar</button>
			</span>
		</div>

		<button type="button" id="btn-create" class="btn btn-info" data-toggle="modal" data-target="#modalFtps">
			<span class="glyphicon glyphicon-plus-sign"></span> Novo Arquivo
		</button>

		<!-- Modal de Criação/Edição de Tipografia -->
		@include('admin.ftps._form')

	</div>
</div>

<div class="table-responsive clear">

@if (count($ftps) >= 1)
	<table id="list-ftps" class="table table-striped">
		<thead>
			<tr>
				<th>Nome</th>
				<th>Criado por</th>
				<th>Link</th>
				<th>Ações</th>
			</tr>
		</thead>
		<tbody>
			@foreach($ftps as $ftp)
				<tr>
					<td>{{ $ftp->nome }}</td>
					<td>{{ $ftp->admUsuarios->nome }}</td>
					<td class="link-ftp">{{ $ftp->path }}</td>
					<td>
						<button type="button" id="btn-edit-{{$ftp->id}}" class="btn btn-primary btn-sm btn-edit" data-toggle="modal" data-target="#modalFtps" data-ftp-id="{{ $ftp->id }}">
							<span class="glyphicon glyphicon-edit"></span> Editar
						</button>
						<button type="button" id="btn-remove-{{$ftp->id}}" class="btn btn-danger btn-sm btn-remove" data-ftp-id="{{ $ftp->id }}">
							<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Remover
						</button>
						<button type="button" class="btn btn-info btn-sm copy-link" data-ftp-id="{{ $ftp->id }}">
							<span class="glyphicon glyphicon-link" aria-hidden="true"></span> Copiar Link
						</button>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	<div>
		<h4>Nenhum Arquivo Localizado</h4>
	</div>
@endif

</div>

@stop
