<!-- Modal -->
<div class="modal fade" id="modalPerfis" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close fa fa-times" data-dismiss="modal"></button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-loading">
				<div class="line-wobble"></div>
			</div>
			<div class="modal-body">
				{!! Form::open(array('class' => 'form-horizontal')) !!}
					{{ Form::hidden('id', '', array('id' => 'editId')) }}
					<div class="form-group form-group-flex flex-align-start">
						{!! Form::label('lblNome', 'Nome *', array('class' => 'col-xs-2 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
						<div class="box-nome col-xs-12">
					    	{!! Form::text('nome', '', array('placeholder' => 'Nome do Perfil de Acesso', 'class' => 'form-control', 'id' => 'editNome')) !!}
				    	</div>
				    </div>
					<div id="checkFamiliesGroup" class="form-group form-group-flex flex-align-start no-margin-bottom">
						@if(!empty($families))
							{!! Form::label('lblFamilias', 'Marcas *', array('class' => 'col-xs-2 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
							<div class="col-xs-12 box-modulos">
								<small class="hint txt-small txt-color-alt no-margin-bottom">Selecione as marcas que deseja permitir o acesso para esse perfil</small>
								<div id="colFamilies" class="col-xs-12 row margin-bottom-10">
									@foreach($families as $key => $family)
										<label class="col-xs-4 control-label txt-left no-margin no-padding-left padding-right-30">
											<strong style="display: flex; align-items: baseline;">	
												<span style="display: inline-block;" class="no-margin">{!! Form::checkbox('families[]', $key, null, array('id' => 'editFamilia'.$key, 'style' => 'vertical-align: middle;', 'class' => 'selectFamila no-margin')) !!}</span>
												<span class="txt-regular no-margin-top no-margin-bottom no-margin-right margin-left-5">{{ $family }}</span>
											</strong>
										</label>
									@endforeach
								</div>
							</div>
						@endif
				    </div>
					<div class="form-group form-group-flex flex-align-center">
						{!! Form::label('lblMarcacao', ' ', array('class' => 'col-xs-2 control-label no-padding-left no-padding-right')) !!}
						<div class="col-xs-12">
							<div class="check-uncheck-all margin-bottom-5">
								<a href="javascript:void(0)" id="desmarcacao" class="txt-color-primary">
									<i class="checkmark fa fa-check-square-o"></i><strong class="txt-regular">Selecionar Todas as Marcas</strong>
								</a>
								<a href="javascript:void(0)" id="marcacao" class="txt-color-primary">
									<i class="checkmark fa fa-square-o"></i><strong class="txt-regular">Selecionar Todas as Marcas</strong>
								</a>
							</div>
				    	</div>
				    </div>
					<div class="modalBtnGroup">
						{!! Form::button('Cancelar', array('class' => 'btn-control btn-alt', 'data-dismiss' => 'modal', 'id' => 'btn-cancel-modal')) !!}
					    {!! Form::submit('Salvar', array('class' => 'btn-control btn-positive')) !!}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
