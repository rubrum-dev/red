@extends('site.site_template')
@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop
@section('content')
<div class="breadcumbs-out clearfix">		
    <div class="breadcrumbs-inner content-fluid clearfix">	
        <ul>
            <li class="active">Cargos</li>
        </ul>
    </div>
</div>
<script>
    var csrf_token = '{{ csrf_token() }}';
</script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- DataTables -->
<script src="{{ asset('/js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/datatables/accent-neutralise.js') }}"></script>

<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/controllers/admin_cargos.js') }}"></script>
<script defer type="text/javascript" src="{{ asset2('/js/site/dataTables-resolve-loader.js') }}"></script>

<div ng-app="app" ng-controller="adminCargosCtrl" class="content content-fluid clearfix">
    <section class="artwork-container manager-container margin-top-10 margin-bottom-40">
        <h3 class="master-title">Gerenciar Cargos</h3>
        <div class="dt-content-container box-relative">            
            <form name="positionsForm" action="" method="POST" novalidate>
                <input type="hidden" name="_token" value="{{ csrf_token()}}" />
                <section>
                    <form>
                        <table style="display:none;" cellpading="0" cellspacing="0" id="positionsManagerTable" class="table manager-table table-border table-hover">
                            <thead>
                                <tr>
                                    <th width=""><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Cargos</small></span></th>
                                    <th width="120"><span class="txt-lighter"><small class="txt-uppercase pull-right margin-left-10">Estado</small></span></th>
                                    <th width="50"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- TO-DO - Loop Cargos -->
                                @foreach($cargos as $cargo)
                                <tr>
                                    <td width="">
                                        <a href="{{ route('admin.cargos.editar', $cargo->id) }}" class="flex-wrap">
                                            <i class="icon fa fa-id-card"></i>
                                            <span class="text-side">{{ $cargo->nome }}</span>
                                        </a>
                                    </td>
                                    <td width="120">{{ ($cargo->status) ? 'Ativo' : 'Inativo' }}</td>
                                    <td width="50">
                                        <div class="dropdown drop-to-left">
                                            <a href="javascript:;" class="tooltip top dropdown-toggle fa fa-ellipsis-h"><span class="tooltip-box">Opções</span></a>
                                            <div class="dropdown-list">
                                                <ul> 
                                                    <li><a href="{{ route('admin.cargos.editar', $cargo->id) }}" class="txt-bold"><span class="icon fa fa-edit"></span>Alterar</a></li>

                                                    @if (count($cargo->admUsuarios))
                                                    <li><a href="javascript:;" class="txt-bold disabled"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                    @else
                                                    <li><a href="javascript:;" ng-click="excluir_cargo('{{ $cargo->id }}')" class="txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                    @endif
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                <!-- END TO-DO - Loop Cargos -->
                            </tbody>
                        </table>
                    </form>
                </section>
            </form>
        </div>
    </section>
    <script>
        $(function() {
            $('.table').on( 'init.dt', function (e, settings) {
				$('.datatable-pagination').hide();
				setTimeout(function () {
					if(settings.aiDisplay.length > 0) {
						$('.datatable-pagination').show();
					}		
				}, new Date().getMilliseconds());
			});

            var dataTableToolbarBtnDOM = '<div class="flex flex-auto">' +
                '<a href="{{ route('admin.cargos.adicionar') }}" class="btn-function"><i class="icon fa fa-id-card"></i> Criar Cargo</a>' +
            '</div>';

            var table = $('#positionsManagerTable').DataTable({
                dom: '<"datatable-toolbar flexbox-container clearfix"' +
                        '<"datatable-filter-container flex flex-3-large"f>' +
                        '<"datatable-filter-container filter-by-status flex flex-3-large">' +
                    '>' +
                    '<"clearfix"' +
                        '<tr>' +
                    '>' +
                    '<"datatable-pagination flexbox-container flex-align-center clearfix"' +
                        '<"flex"<"select-control"l>><"flex flex-auto"p><"flex"i>' +
                    '>',
                bAutoWidth: false,
                processing: false,
                serverSide: false,
                stateSave: true,
                pageLength: 25,
                lengthMenu: [
                    [10, 25, 50, -1], 
                    ['Exibir 10 por página', 'Exibir 25 por página', 'Exibir 50 por página', 'Exibir todos']
                ],
                language: {
                    "url": "/js/datatables/language/Portuguese-Brasil.json"
                },
                initComplete: function () {
                    $('.datatable-filter-container .dataTables_filter input[type="search"]').attr('placeholder', 'Busca');
                    
                    var dataTables_filter_cancel = '<i></i>';
                    var dataTables_filter_search = '<i></i>';
                    
                    $('.datatable-filter-container .dataTables_filter').append(dataTables_filter_search);
                    $('.datatable-filter-container .dataTables_filter').append(dataTables_filter_cancel);
                    $('.datatable-filter-container .dataTables_filter i').addClass('dataTables_filter_search');
                    $('.datatable-filter-container .dataTables_filter i + i').addClass('dataTables_filter_cancel');
                    
                    function dataTablesFilterCancelShowHide() {
                        if ($('.datatable-filter-container .dataTables_filter input[type="search"]').val() !== '') {
                            $('.dataTables_filter_search').hide();
                            $('.dataTables_filter_cancel').show();
                        } else {
                            $('.dataTables_filter_search').show();
                            $('.dataTables_filter_cancel').hide();
                            
                            table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
                        }
                    }
                    
                    $(document).on('click', '.datatable-filter-container .dataTables_filter_cancel', function () {
                        $('#positionsManagerTable_filter input[type="search"]').val('');
                        
                        dataTablesFilterCancelShowHide();
                    });
                    
                    $(document).on('keyup', '#positionsManagerTable_filter input[type="search"]', function () {
                        dataTablesFilterCancelShowHide();
                    });
                    
                    $('.dataTable').show();
                    $('.dataTable thead').show();
                    $('.dataTables_filter').show();
                    $('.datatable-toolbar').append(dataTableToolbarBtnDOM);
                },
                fnDrawCallback: function( oSettings ) {
                    if(oSettings.aiDisplay.length <= 0) {
                        $('.dataTable thead th').hide();
                        $('.datatable-pagination').hide();
                        $('.dataTables_empty').addClass('fa-id-card');
                        $('.dataTables_empty').text('Nenhum cargo encontrado.');
                    } else {
                        $('.dataTable thead th').show();
                        $('.datatable-pagination').show();
                        $('.dataTables_empty').removeClass('fa-id-card');
                        $('.dataTables_empty').text('');
                    }
                }
            });
        });
    </script>
</div>
@stop