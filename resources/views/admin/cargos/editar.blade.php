@extends('site.site_template')
@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop
@section('content')
<div class="breadcumbs-out clearfix">		
    <div class="breadcrumbs-inner content-fluid clearfix">	
        <ul>
            <li><a href="{{ route('admin.cargos.gerenciar') }}">Cargos</a></li>
            <li class="active">Editar Cargo</li>
        </ul>
    </div>
</div>
<script>
    var csrf_token = '{{ csrf_token() }}';

    var id = '{{ $cargo->id }}';

    var nomeCargo = '{!! $cargo->nome !!}';

    var chkCargoAtivo = {{ ($cargo->status) ? 'true' : 'false' }};

</script>
<script src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script src="{{ asset('/js/angular/controllers/admin_cargo_editar.js') }}"></script>
<div ng-app="app" ng-controller="CargoEditarCtrl" ng-cloak class="content content-fluid clearfix">
    <section class="artw-package-edit-container margin-top-10 margin-bottom-40">
        <form name="positionEditForm" action="" method="POST" novalidate>
            <input type="hidden" name="_token" value="{{ csrf_token()}}" />
            <h3 class="master-title txt-bold margin-bottom-20">Editar Cargo</h3>
            <div class="block">
                <div class="bs no-border no-padding margin-bottom-30">
                    <div class="flexbox-container">
                        <div class="flex flex-fluid">
                            <label class="label-control txt-lighter txt-small txt-uppercase margin-bottom-10">Nome do Cargo *</label>                
                            <input type="text" placeholder="Nome do Cargo" ng-model="nomeCargo" name="nomeCargo" id="nomeCargo" class="input-control only-alpha-numeric" />
                        </div>
                    </div>
                </div>
                <div class="bs border-color-primary padding-bottom-30">
                    <div class="flexbox-container flex-align-center margin-top-30">
                        <div class="flex flex-fluid">
                            <label class="label-control txt-lighter txt-small txt-uppercase margin-bottom-10">Opções *</label>                
                            <ul class="list-group-indent">
                                <li class="list-item-horizontal">
                                    <label class="indent clickable no-underline">
                                        <strong class="pull">
                                            <i class="icon fa fa-id-card"></i>
                                            <span class="text txt-bold no-underline">Cargo Ativo <small class="txt-small txt-regular margin-left-20">Exibe nas listas de cargo nas configurações da conta e cadastro de usuário.</small></span>
                                            <span class="switch-control pull-right no-margin">
                                                <input ng-model="chkCargoAtivo" name="chkCargoAtivo" id="chkCargoAtivo" type="checkbox" />
                                                <span class="switch-toggle abs-right-offset no-margin"></span>
                                            </span>
                                        </strong>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>    
                </div>
                <div class="btn-group txt-center margin-top-40" ng-if="nomeCargo !== ''" class="form-section clearfix">
                    <a href="{{ route('admin.cargos.gerenciar') }}" class="btn-control btn-call-to-action call-to-action-negative btn-icon btn-icon-left">
                        <span><i class="icon fa fa-times"></i> Cancelar</span>
                    </a>
                    <button ng-click="enviarFormulario()" type="button" class="btn-control btn-call-to-action call-to-action-alt btn-icon btn-icon-left">
                        <span><i class="icon fa fa-check"></i> Salvar Cargo</span>
                    </button>
                </div>
            </div>
        </form>
    </section>
</div>
@stop