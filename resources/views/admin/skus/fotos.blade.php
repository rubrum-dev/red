@extends('admin.admin_template')
@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/datatables/dataTables.bootstrap.min.css') }}" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<script type="text/javascript" src="{{ asset('/js/admin/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/admin/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/datatables/accent-neutralise.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/datatables/absolute.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/angular/angular-locale_pt-br.min.js') }}"></script>
<script type="text/javascript" src="{{ asset2('/js/angular/controllers/admin_skus.js') }}"></script>
<script defer type="text/javascript" src="{{ asset2('/js/site/dataTables-resolve-loader.js') }}"></script>

<div class="admin-container">
    <div class="list-title-header clearfix">
        <h3>Gerenciar Fotos</h3>
    </div>
    <div ng-app="app" ng-controller="AdminSkusCtrl" ng-cloak>
        <div class="table-listagem clear">
            @include('admin.skus._form')
            <div class="dt-content-container box-relative">
                <table style="display:none;" id="list-sku" class="table">
                    <thead>
                        <tr>
                            <th width=""><span>Embalagens</span></th>
                            <th width="30"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($produtos) >= 1)
                            @foreach($produtos as $produto)
                                <tr>
                                    <td width="">
                                        <a href="javascript:;" class="txt-no-link">
                                            <i class="icon fa fa-camera"></i>
                                            <span class="text-side">{{ trim($produto->nome_completo) }}</span>
                                        </a>
                                    </td>
                                    <td width="30">
                                        <div class="dataTable_btn_group">
                                            <div class="dropdown drop-to-left">
                                                <a class="ui-tooltip top dropdown-toggle fa fa-ellipsis-h"><span class="tooltip-box">Opções</span></a>
                                                <div class="dropdown-list">
                                                    <ul> 
                                                        <li><a ng-href="{{ url('/admin/sku/finalart/')}}/{{ $produto->id }}" class="txt-bold"><span class="icon fa fa-camera"></span>Imagens do PackShelf</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </td>                                
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <script>
                $(function() {
                    $('.table').on( 'init.dt', function (e, settings) {
                        $('.btn-add-new > .btn-function').hide();
                        $('.dataTable-bottom').hide();
                        setTimeout(function () {
                            if(settings.aiDisplay.length > 0) {
                                $('.dataTable-bottom').show();
                            }		
                        }, new Date().getMilliseconds());
                    });
                    
                    var table = $('#list-sku').DataTable({
                        dom: '<"dataTable-head"<"dataTable-filter"<"dataTable-filter-in row"<"dataTable-searchbox-container"f>>>>' +
                            '<<"table-responsive"tr>>' +
                        '<"dataTable-bottom"<"dataTable-selectbox-container"l><"dataTables_paginate_flex"p><i>>',
                        bAutoWidth: false,
                        processing: false,
                        serverSide: false,
                        stateSave: true,
                        bStateSave: true,
                        order: [
                            [0, 'asc']
                        ],
                        pageLength: 25,
                        lengthMenu: [
                            [10, 25, 50, -1], 
                            ['Exibir 10 por página', 'Exibir 25 por página', 'Exibir 50 por página', 'Exibir todos']
                        ],
                        columnDefs: [
                            { orderable: false, targets: 1 },
                        ],
                        language: {
                            'url': '/js/datatables/language/Portuguese-Brasil.json'
                        },
                        initComplete: function () {
                            var dataTables_filter_search = '<i></i>';
                            var dataTables_filter_cancel = '<i></i>';
                                                        
                            $('.dataTable-searchbox-container input[type="search"]').attr('placeholder', 'Buscar');
                            
                            $('.dataTable-searchbox-container .dataTables_filter').append(dataTables_filter_search);
                            $('.dataTable-searchbox-container .dataTables_filter').append(dataTables_filter_cancel);
                            $('.dataTable-searchbox-container .dataTables_filter i').addClass('dataTables_filter_search');
                            $('.dataTable-searchbox-container .dataTables_filter i + i').addClass('dataTables_filter_cancel');

                            function dataTablesFilterCancelShowHide() {
                                if ($('.dataTable-searchbox-container .dataTables_filter input[type="search"]').val() !== '') {
                                    $('.dataTables_filter_search').hide();
                                    $('.dataTables_filter_cancel').show();
                                
                                } else {
                                    $('.dataTables_filter_search').show();
                                    $('.dataTables_filter_cancel').hide();
                                    
                                    table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
                                }
                            }

                            $(document).on('click', '.dataTable-searchbox-container .dataTables_filter_cancel', function () {
                                $('.dataTables_filter input[type="search"]').val('');
                                
                                dataTablesFilterCancelShowHide();
                            });

                            // Remove accented character from search input as well
                            $(document).on('keyup', '.dataTables_filter input[type="search"]', function () {
                                dataTablesFilterCancelShowHide();
                            });

                        	$('.dataTable').show();
						    $('.dataTable thead').show();
						    $('.dataTable tbody').show();
						    $('.dataTables_filter').show();
					    },
                        fnDrawCallback: function(oSettings) {
                            if(oSettings.aiDisplay.length <= 0) {
                                $('.dataTable thead tr').hide();
                                $('.dataTable-bottom').hide();
                                $('.dataTables_empty').addClass('fa-cube');
                                $('.dataTables_empty').text('Nenhuma embalagem encontrada.');
                            } else {
                                $('.dataTable thead tr').show();
                                $('.dataTable-bottom').show();
                                $('.dataTables_empty').removeClass('fa-cube');
                                $('.dataTables_empty').text('');
                            }
                        }
                    });
                    //$(document).on('focus', '.dataTables_filter input[type="search"]', function () {
                    //    $('.dataTables_filter i').css('color', '#4284F4');
                    //});
                    //$(document).on('blur', '.dataTables_filter input[type="search"]', function () {
                    //    $('.dataTables_filter i').css('color', '#004392');
                    //});
                });
            </script>
        </div>
    </div>
</div>
@stop
