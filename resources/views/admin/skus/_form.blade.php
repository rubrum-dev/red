<!-- Modal -->
<div ng-init="bsModalOpen()" class="modal fade" id="modalSkus" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button ng-click="bsModalClose()" type="button" class="close fa fa-times" data-dismiss="modal"></button>
                <h4 class="modal-title">@{{ dados.id_variacao ? 'Editar Embalagem' : 'Nova Embalagem' }}</h4>
            </div>
            <div class="modal-loading">
                <div class="line-wobble"></div>
            </div>
            <div class="modal-body">
                <form accept-charset="UTF-8" class="form-horizontal" id="form-sku" ng-submit="salvar()" enctype="multipart/form-data">
                    {{ Form::token() }}
                    <input id="editId" name="id" type="hidden" ng-value="dados.id_variacao">
                    <div class="form-group form-group-flex flex-align-baseline">
                        <label for="lblTipo" class="col-xs-3 control-label no-margin no-padding txt-right">Função</label>
                        <div class="col-xs-12">
                            <label class="no-margin-bottom">
                                <input checked="checked" ng-model="dados.promocional" name="promocional" type="radio" value="0">&nbsp;Regular
                            </label>
                            <label class="margin-left-15 no-margin-bottom">
                                <input ng-model="dados.promocional" name="promocional" type="radio" value="1">&nbsp;Promocional
                            </label>
                        </div>
                    </div>
                    <div ng-show="dados.id_variacao" class="form-group form-group-flex flex-align-start" id="box-nome">
                        <label for="lblNome" class="col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right">Nome da Embalagem</label>
                        <div class="col-xs-12">
                            <input class="form-control" placeholder="Nome da Embalagem" id="editNome" disabled name="nome" type="text" ng-value="dados.sku_nome_completo">
                        </div>
                    </div>
                    <div class="form-group form-group-flex flex-align-start">
                        <label for="lblProduto" class="col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right">Produto *</label>
                        <div class="col-xs-12">
                            <select ng-change="carregar_campanhas(1)" ng-disabled="!produtos" class="form-control" id="editProduto" ng-model="dados.id_produto" name="id_produto" required>
                                <option value="">Selecione o Produto</option>
                                <option ng-selected="produto.id == dados.id_produto" ng-repeat="produto in produtos" ng-value="produto.id">@{{ produto.nome}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-group-flex flex-align-start" id="nome-embalagem-regular">
                        <label for="lblDimensao" class="col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right">Volume *</label>
                        <div class="col-xs-12">
                            <select ng-disabled="!dados.id_produto" class="form-control" id="editDimensao" name="id_dimensao" ng-model="dados.id_dimensao" required>
                                <option selected="selected" value="">Selecione o Volume</option>
                                <option ng-selected="embalagem.id == dados.id_dimensao" ng-repeat="embalagem in embalagens" ng-value="embalagem.id">@{{ embalagem.tipo + ' - ' + embalagem.nome}}</option>
                            </select>
                        </div>
                    </div>

                    <!-- Natureza de Embalagem -->
                    <div class="form-group form-group-flex flex-align-start">
                        <label for="lblNatureza" class="col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right">Natureza *</label>
                        <div class="col-xs-12">
                            <select class="form-control" id="editNatureza" ng-model="dados.id_natureza_embalagem" ng-change="carregar_embalagens_por_natureza(1)" name="id_natureza" required>
                                <option value="">Selecione a Natureza</option>
                                <option ng-if="natureza.id > 1" ng-selected="natureza.id == dados.id_natureza_embalagem" ng-repeat="natureza in naturezas" ng-value="natureza.id">@{{natureza.nome}}</option>
                            </select>
                        </div>
                    </div>

                    <!-- TODO - Indicar Embalagens Primárias / Secundárias -->
                    <div ng-show="dados.id_natureza_embalagem == '3' || dados.id_natureza_embalagem == '4'" class="form-group form-group-flex flex-align-center">
                        <label class="col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right"></label>
                        <div ng-show="!processing" class="col-xs-10">
                            <div ng-show="dados.id_natureza_embalagem == '3'">
                                <select class="form-control" id="cbPckEmbalagensPrimarias" ng-model="cbPckEmbalagens" name="cbPckEmbalagensPrimarias">
                                    <option value="">Indique as embalagens primárias contidas</option>
                                    <option ng-repeat="embalagem_por_natureza in embalagens_por_natureza" ng-value="embalagem_por_natureza.id">@{{embalagem_por_natureza.nome}}</option>
                                </select>
                            </div>

                            <div ng-show="dados.id_natureza_embalagem == '4'">
                                <select class="form-control" id="cbPckEmbalagensSecundarias" ng-model="cbPckEmbalagens" name="cbPckEmbalagensSecundarias">
                                    <option value="">Indique as embalagens secundárias contidas</option>
                                    <option ng-repeat="embalagem_por_natureza in embalagens_por_natureza" ng-value="embalagem_por_natureza.id">@{{embalagem_por_natureza.nome}}</option>
                                </select>
                            </div>
                        </div>
                        <div ng-show="processing" class="col-xs-10">
                            <select disabled class="form-control">
                                <option value="">Carregando...</option>
                            </select>
                        </div>
                        <div class="col-xs-2 no-padding-left">
                            <button ng-disabled="!cbPckEmbalagens" type="button" ng-click="add_embalagem_selecionada()" class="btn btn-primary btn-fluid">Adicionar</button>
                        </div>
                    </div>
                    
                    <!-- TODO - Embalagens Selecionadas -->
                    <div ng-show="dados.embalagens_selecionadas.length > 0" class="form-group form-group-flex margin-top-20">
                        <label class="col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right"></label>
                        <div class="col-xs-12">
                            <table class="admin-table">
                                <tbody>
                                    <tr ng-repeat="embalagem_selecionada in dados.embalagens_selecionadas track by $index">
                                        <td width="">
                                            <i class="icon fa fa-cube"></i>
                                            <span class="text-side">@{{embalagem_selecionada.nome}}</span>
                                            <input type="hidden" name="embalagens_selecionadas[]" ng-value="embalagem_selecionada.id">
                                        </td>
                                        <td width="50">
                                            <div class="dropdown drop-to-left">
                                                <a href="javascript:;" class="ui-tooltip top dropdown-toggle fa fa-ellipsis-h"><span class="tooltip-box">Opções</span></a>
                                                <div class="dropdown-list">
                                                    <ul> 
                                                        <li><a href="javascript:;" ng-click="excluir_embalagem_selecionada($index)" class="txt-bold"><i class="icon fa fa-trash"></i>Excluir</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                    <!-- TODO Código EAN -->
                    <div class="form-group form-group-flex flex-align-baseline">
                        <label for="lblEANCode" class="col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right">Código de Barras *</label>
                        <div class="col-xs-12">
                            <div class="ean-code-group">
                                <label class="no-margin-bottom">
                                    <input checked="checked" ng-model="dados.eanCode" name="eanCode" type="radio" value="1">&nbsp;Não se aplica
                                </label>
                                <label class="margin-left-15 no-margin-bottom">
                                    <input ng-model="dados.eanCode" name="eanCode" type="radio" value="2">&nbsp;Ainda não tenho o código
                                </label>
                                <label class="margin-left-15 no-margin-bottom">
                                    <input ng-model="dados.eanCode" name="eanCode" type="radio" value="3">&nbsp;Já tenho o código
                                </label>
                            </div>
                        </div>
                    </div>
                    <div ng-show="dados.eanCode === '3'" class="form-group form-group-flex flex-align-start" id="box-nome">
                        <label for="lblTxtEanCode" class="col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right"></label>
                        <div class="col-xs-12">
                            <input type="text" placeholder="Digite código EAN" ng-required="dados.eanCode === 3" ng-model="dados.txtEANCode" name="txtEANCode" id="txtEANCode" class="form-control">
                        </div>
                    </div>
                    <!-- END Código EAN -->

                    <div class="form-group form-group-flex flex-align-baseline">
                        <label for="lblTipo" class="col-xs-3 control-label no-margin no-padding txt-right">Possui variação?</label>
                        <div class="col-xs-12">
                            <label class="no-margin-bottom">
                                <input checked="checked" ng-model="dados.possuiVariacao" name="possuiVariacao" type="radio" value="0" id="possuiVariacao_0">&nbsp;Não
                            </label>
                            <label class="margin-left-15 no-margin-bottom">
                                <input ng-model="dados.possuiVariacao" name="possuiVariacao" type="radio" value="1" id="possuiVariacao_1">&nbsp;Sim
                            </label>
                        </div>
                    </div>
                    <div ng-show="dados.possuiVariacao === '1'" class="form-group form-group-flex flex-align-start" id="box-nome">
                        <label for="variacao" class="col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right">Variação *</label>
                        <div class="col-xs-12">
                            <input ng-required="possuiVariacao === '1'" placeholder="Nome da Variação" class="form-control" id="variacao" name="variacao" type="text" ng-model="dados.variacao">
                        </div>
                    </div>
                    <div ng-show="dados.promocional === '1'" class="form-group form-group-flex flex-align-start" id="nome-embalagem-regular">
                        <label for="campanha" class="col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right">Campanha *</label>
                        <div ng-show="!processing" class="col-xs-12">
                            <select ng-options="campanha.id as campanha.nome for campanha in campanhas" ng-required="dados.promocional === '1'" ng-disabled="!dados.id_produto" ng-model="id_campanha" class="form-control" id="id_campanha" name="id_campanha">
                                <option selected value="">Selecione a Campanha</option>
                            </select>
                            <input ng-value="id_campanha" type="hidden" id="id_campanha_2" name="id_campanha_2">
                        </div>
                        <div ng-show="processing" class="col-xs-12">
                            <select disabled class="form-control">
                                <option value="">Carregando...</option>
                            </select>
                        </div>
                    </div>
                    <div ng-show="id_campanha == '0'" class="form-group form-group-flex flex-align-start" id="box-nome">
                        <label for="nova_campanha" class="col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right">Nova Campanha *</label>
                        <div class="col-xs-12">
                            <input ng-model="campanha_nome" ng-required="dados.id_campanha === '0'" placeholder="Nome da Nova Campanha" class="form-control" id="campanha" name="campanha" type="text">
                        </div>
                    </div>
                    <!--div ng-show="dados.promocional === '1'" class="form-group form-group-flex flex-align-start" id="box-expiracao">
                        <label for="lblExpiracao" class="col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right">Expira&ccedil;&atilde;o</label>
                        <div class="col-xs-12">
                            <input ng-required="dados.promocional === '1'" ng-model="dados.dt_expiracao_" class="form-control" id="editExpiracao" placeholder="dd/mm/aaaa" name="dt_expiracao" type="text" value="">
                        </div>
                    </div-->
                    <div class="modalBtnGroup">
                        <button ng-click="bsModalClose()" class="btn-control btn-alt" data-dismiss="modal" id="btn-cancel-modal" type="button">Cancelar</button>
                        <input class="btn-control btn-positive" type="submit" value="Salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->