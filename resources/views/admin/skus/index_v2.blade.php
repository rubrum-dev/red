@extends('admin.admin_template')
@section('title')
Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais
@stop
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/datatables/dataTables.bootstrap.min.css') }}" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<script type="text/javascript" src="{{ asset('/js/admin/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/admin/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/datatables/accent-neutralise.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/datatables/absolute.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/angular/angular-locale_pt-br.min.js') }}"></script>
<script type="text/javascript" src="{{ asset2('/js/angular/controllers/admin_embalagens.js') }}"></script>
<script defer type="text/javascript" src="{{ asset('/js/site/dataTables-resolve-loader.js') }}"></script>

<div class="admin-container">
    <div class="list-title-header clearfix">
        <h3>Gerenciar Embalagens</h3>
    </div>
    <div ng-app="app" ng-controller="AdminSkusCtrl" ng-cloak>
        <div class="table-listagem clear">
            <div class="form-group-add">
                <button ng-click="resetar()" type="button" id="btn-create" class="btn btn-sm btn-function" data-toggle="modal" data-target="#modalSkus">
                    <i class="icon fa fa-cube"></i> 
                    <span class="txt-semibold">Adicionar Embalagem</span> 
                </button>
            </div>
            @include('admin.skus._form')
            <div class="dt-content-container">
                <table style="display:none;" id="list-sku" class="table">
                    <thead>
                        <tr>
                            <th width=""><span>Embalagens</span></th>
                            <th width="150"><span>Função</span></th>
                            <th width="150"><span>Natureza</span></th>
                            <th width="30"></th>
                        </tr>
                    </thead>
                    <tbody>
                    @if (count($variacoes) >= 1)
                        @foreach($variacoes as $variacao)
                            <tr>
                                <td width="">
                                    <a href="javascript:;" class="txt-no-link">
                                        <i class="icon fa fa-cube"></i>
                                        <span class="text-side">{{ trim($variacao->nome_completo) }}</span>
                                    </a>
                                </td>
                                <td width="150"><span>{{ ($variacao->id_campanha) ? 'Promocional' : 'Regular' }}</span></td>
                                <td width="150"><span>{{ ($variacao->natureza) ? $variacao->natureza->nome : '--' }}</span></td>
                                <td width="30">
                                    <div class="dataTable_btn_group">
                                        <div class="dropdown drop-to-left">
                                            <a ng-click="carregar_variacao('{{ $variacao->id}}')" href="javascript:;" class="ui-tooltip top dropdown-toggle fa fa-ellipsis-h"><span class="tooltip-box">Opções</span></a>
                                            <div class="dropdown-list">
                                                <ul> 
                                                    <!--li><a ng-href="{{ url('/admin/sku/finalart/')}}/@{{ dados.id_variacao}}" class="txt-bold"><span class="icon fa fa-camera"></span>Imagens do PackShelf</a></li-->
                                                    <li><a href="javascript:;" data-toggle="modal" data-target="#modalSkus" id="btn-edit-{{$variacao->id}}" class="btn-edit txt-bold"><span class="icon fa fa-edit"></span>Alterar</a></li>
                                                    @if ($variacao->wasRecentlyCreated)
                                                        <li><a href="javascript:;" mw-confirm-click="excluir_variacao('{{ $variacao->id}}');" mw-confirm-click-message="Deseja excluir essa embalagem?" id="btn-remove-{{$variacao->id}}" class="btn-remove txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </td>                                
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <script>
                $(function() {
                    $('.table').on( 'init.dt', function (e, settings) {
                        $('.dataTable-bottom').hide();
                        setTimeout(function () {
                            if(settings.aiDisplay.length > 0) {
                                $('.dataTable-bottom').show();
                            }		
                        }, new Date().getMilliseconds());
                    });
                    
                    var alertBoxHTML = '<div class="panel alternative margin-top-30 margin-left-10 margin-right-10">' +
                        '<i class="icon fa fa-exclamation-triangle"></i>' +
                        '<div class="panel-body d-inline-flex flex-align-center txt-left no-padding-right">' +
                            '<h5 class="panel-heading txt-semibold no-margin">Para o cadastro de embalagens, é necessário que algum volume e produto sejam cadastrados antes.</h5>' +
                        '</div>' +
                    '</div>';

                    var table = $('#list-sku').DataTable({
                        dom: '<"dataTable-head"<"dataTable-filter"<"dataTable-filter-in row"<"dataTable-searchbox-container"f>>>>' +
                            '<<"table-responsive"tr>>' +
                        '<"dataTable-bottom"<"dataTable-selectbox-container"l><"dataTables_paginate_flex"p><i>>',
                        bAutoWidth: false,
                        processing: false,
                        serverSide: false,
                        stateSave: true,
                        bStateSave: true,
                        order: [
                            [0, 'asc'],
                            [1, ''],
                            [2, ''],
                        ],
                        pageLength: 25,
                        lengthMenu: [
                            [10, 25, 50, -1], 
                            ['Exibir 10 por página', 'Exibir 25 por página', 'Exibir 50 por página', 'Exibir todos']
                        ],
                        columnDefs: [
                            { orderable: false, targets: 3 },
                        ],
                        language: {
                            'url': '/js/datatables/language/Portuguese-Brasil.json'
                        },
                        initComplete: function () {
                            var dataTables_filter_search = '<i></i>';
                            var dataTables_filter_cancel = '<i></i>';
                                                        
                            $('.dataTable-searchbox-container input[type="search"]').attr('placeholder', 'Buscar');
                            
                            $('.dataTable-searchbox-container .dataTables_filter').append(dataTables_filter_search);
                            $('.dataTable-searchbox-container .dataTables_filter').append(dataTables_filter_cancel);
                            $('.dataTable-searchbox-container .dataTables_filter i').addClass('dataTables_filter_search');
                            $('.dataTable-searchbox-container .dataTables_filter i + i').addClass('dataTables_filter_cancel');

                            function dataTablesFilterCancelShowHide() {
                                if ($('.dataTable-searchbox-container .dataTables_filter input[type="search"]').val() !== '') {
                                    $('.dataTables_filter_search').hide();
                                    $('.dataTables_filter_cancel').show();
                                
                                } else {
                                    $('.dataTables_filter_search').show();
                                    $('.dataTables_filter_cancel').hide();
                                    
                                    table.search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
                                }
                            }

                            $(document).on('click', '.dataTable-searchbox-container .dataTables_filter_cancel', function () {
                                $('.dataTables_filter input[type="search"]').val('');
                                
                                dataTablesFilterCancelShowHide();
                            });

                            $(document).on('keyup', '.dataTables_filter input[type="search"]', function () {
                                dataTablesFilterCancelShowHide();
                            });

                            $('.dataTable').show();
                            $('.dataTable thead').show();
                            $('.dataTable tbody').show();
                            $('.dataTables_filter').show();

                            @if (count($variacoes) < 1)
                                @if (count(@$produtos) == 0 || count($dimensoes) == 0)
                                    $(alertBoxHTML).insertBefore('#list-sku');
                                @endif
                            @endif
                        },
                        fnDrawCallback: function(oSettings) {
                            if(oSettings.aiDisplay.length < 1) {
                                $('.dataTable thead tr').hide();
                                $('.dataTable-bottom').hide();
                                $('.dataTables_empty').addClass('fa-cube');
                                $('.dataTables_empty').text('Nenhuma embalagem encontrada.');
                            } else {
                                $('.dataTable thead tr').show();
                                $('.dataTable-bottom').show();
                                $('.dataTables_empty').removeClass('fa-cube');
                                $('.dataTables_empty').text('');
                            }
                        }
                    });
                    //$(document).on('focus', '.dataTables_filter input[type="search"]', function () {
                    //    $('.dataTables_filter i').css('color', '#4284F4');
                    //});
                    //$(document).on('blur', '.dataTables_filter input[type="search"]', function () {
                    //    $('.dataTables_filter i').css('color', '#004392');
                    //});
                });
            </script>
        </div>
    </div>
</div>
@stop