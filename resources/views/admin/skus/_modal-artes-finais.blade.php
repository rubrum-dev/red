<!-- Modal -->
<div class="modal fade" id="modalArtes" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close fa fa-times" data-dismiss="modal"></button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-loading">
				<div class="line-wobble"></div>
			</div>
			<div class="modal-body">
				{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-art', 'files' => 'true')) !!}
					{{ Form::hidden('id_sku', $sku->id, array('id' => 'editIdSku')) }}
					{{ Form::hidden('id', '', array('id' => 'editId')) }}
					<div class="form-group form-group-flex flex-align-start">
						{!! Form::label('lblNomeSKU', 'Legenda da Imagem *', array('class' => 'col-xs-4 control-label no-margin-bottom no-padding-left no-padding-right txt-right')) !!}
						<div class="col-xs-12">
					    	{!! Form::text('nome', '', array('class' => 'form-control', 'id' => 'editNome', 'placeholder' => 'Digite a legenda da imagem')) !!}
				    	</div>
				    </div>
					<div class="form-group form-group-flex flex-align-start" id="listImage">
						{!! Form::label('lblThumb', 'Imagem Miniatura *', array('class' => 'col-xs-4 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
						<div class="col-xs-12">
							<table id="list-tipos-logos">
								<tr>
									<td>
										<div class="block margin-top-10">
											<p class="txt-small txt-color-alt margin-bottom-10">
												Formato PNG com fundo transparente medindo 150px (larg.) x 200px (alt.)
											</p>
											<div data-title="Nenhum arquivo." class="d-inline-flex ui-tooltip-2 top">
												<div class="center-logo image-only">
													{{ Html::image(asset(''), 'alt', array('class' => 'img-rounded img-logo')) }}
												</div>
											</div>
										</div>
										<div class="block">
											<div id="upload-image" class="upload-logo">
												{!! Form::file('image', array('id' => 'image')) !!}
											</div>
										</div>
									</td>
						    	</tr>
						    </table>
				    	</div>
			    	</div>
			    	<div class="form-group form-group-flex flex-align-start" id="listCmyk">
				    	{!! Form::label('lblCmyk', 'Imagem em Baixa Resolução *', array('class' => 'col-xs-4 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
				    	<div class="col-xs-12">
							<table id="list-tipos-logos">
								<tr>
									<td>
										<div class="block margin-top-10">
											<p class="txt-small txt-color-alt margin-bottom-10">
												Formato PNG com fundo transparente com mínimo de 1500px (alt.)
											</p>
											<div data-title="Nenhum arquivo." class="d-inline-flex ui-tooltip-2 top">
												<div class="center-logo image-only">
													{{ Html::image(asset(''), 'alt', array('class' => 'img-rounded img-logo')) }}
												</div>
											</div>
										</div>
										<div class="block">
											<div id="upload-cmyk" class="upload-logo">
												{!! Form::file('cmyk', array('id' => 'cmyk')) !!}
											</div>
										</div>
									</td>
						    	</tr>
						    </table>
				    	</div>
				    </div>
				    <div class="form-group form-group-flex flex-align-start" id="listRgb">
				    	{!! Form::label('lblRgb', 'Imagem em Alta Resolução', array('class' => 'col-xs-4 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
				    	<div class="col-xs-12">
							<table id="list-tipos-logos">
								<tr>
									<td>
										<div class="block margin-top-10">
											<p class="txt-small txt-color-alt margin-bottom-10">
												Formato TIFF ou PSD
											</p>
											<div data-title="Nenhum arquivo." class="d-inline-flex ui-tooltip-2 top">
												<div class="center-logo file-image-type-only">
													{{ Html::image(asset(''), 'alt', array('class' => 'img-rounded img-logo')) }}
												</div>
											</div>
										</div>
										<div class="block flexbox-container flex-align-center flexbox-group">
											<div class="upload-logo flex flex-fluid">
												{!! Form::file('rgb', array('id' => 'rgb')) !!}
											</div>
											<div class="flex">
												<button type="button" id="btn-remove-rgb" class="btn btn-sm btn-function">
													<i class="icon fa fa-trash"></i>
													<span class="txt-semibold no-margin">Remover Imagem</span>
												</button>
											</div>
										</div>
									</td>
						    	</tr>
						    </table>
				    	</div>
				    </div>
					<div class="modalBtnGroup">
						{!! Form::button('Cancelar', array('class' => 'btn-control btn-alt', 'data-dismiss' => 'modal', 'id' => 'btn-cancel-modal')) !!}
					    {!! Form::submit('Salvar', array('class' => 'btn-control btn-positive')) !!}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
<div id="packShelfImgProgress" class="progress-circle animate">
	<svg id="svg" viewbox="0 0 100 100">
		<circle cx="50" cy="50" r="40" fill="none"/>
		<path fill="none" stroke-linecap="round" stroke-width="4" d="M50 10 a 40 40 0 0 1 0 80 a 40 40 0 0 1 0 -80"/>
		<text id="count" class="progress" x="50" y="50" text-anchor="middle" dy="7"></text>
	</svg>
</div>
	