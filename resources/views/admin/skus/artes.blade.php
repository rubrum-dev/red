@extends('admin.admin_template')

@section('title')
Artes Finais
@stop

@section('content')
<script src="{{ asset('/js/site/snap.svg-min.js') }}"></script>
<script src="{{ asset2('/js/admin/artes-finais.js') }}"></script>
<script>
    var csrf_token = '{{ csrf_token() }}';

    jQuery(document).ready(function($) {
        if (window.history && window.history.pushState) {
            $(window).on('popstate', function() {
                var hashLocation = location.hash;
                var hashSplit = hashLocation.split("#!/");
                var hashName = hashSplit[1];

                if (hashName !== '') {
                    var hash = window.location.hash;
                    
                    if (hash === '') {
                        location.href='{{ url('admin/sku/fotos') }}'
                    }
                }
            });

            window.history.pushState('forward', null, '');
        }
    });
</script>

<div class="admin-container margin-top-20 no-padding">
    <div class="breadcrumb flexbox-container">
        <a href="javascript:;" onclick="location='{{ url('admin/sku/fotos') }}'" class="btn-back txt-bold txt-small margin-left-auto"><i class="icon fa fa-arrow-left margin-right-10"></i>Voltar</a>
    </div>
    <div class="list-title-header">
        <h3>Imagens do PackShelf</h3>
    </div>
    <!-- Modal de Criação/Edição de Artes Finais -->
    @include('admin.skus._modal-artes-finais')
    @if (session('status'))
        <div class="alert alert-success box-msg">
            {{ session('status') }}
        </div>
    @endif
    <div class="flexbox-container flex-align-center margin-bottom-20 clearfix">
        <h5 class="list-title-sub txt-regular margin-left-10 no-margin-top no-margin-bottom"><i class="icon fa fa-cube txt-color-alt"></i> {{ $sku->sku_nome_completo }}</h5>
        <div class="d-inline-flex margin-left-auto">
            <button ng-click="resetar()" type="button" id="btn-create" class="btn btn-sm btn-function" data-toggle="modal" data-target="#modalArtes">
                <i class="icon fa fa-camera"></i> 
                <span class="txt-semibold">Nova Imagem</span> 
            </button>
        </div>
    </div>
    {!! Form::open(array('class' => '', 'id' => 'form-packshelf-imgs')) !!}
        @if (count($artesFinais) >= 1)
            <div class="table-responsive clear sku-relations margin-top-20" id="list-artes" data-sku-id="{{ $sku->id }}">
                <?php $a = 1; ?>
                <div class="box-vertical-interno">
                    <div class="box-horizontal-interno-tipo">
                        <table id="list-tipos-artes" class="list-tipos-artes">
                            @foreach ($artesFinais as $value)
                                @if ($a == 1)
                                    <tr>
                                @endif
                                <td class="no-border txt-center">
                                    <div class="box-product-misuse">
                                        @if($value->thumb)
                                            {{--<span>{{ Html::image(asset($value->thumb), 'alt', array('class' => 'img-object-fit img-logo')) }}</span>--}}
                                            <span>{{ Html::image(asset($value->thumb), 'alt', array('class' => 'img-object-fit img-logo')) }}</span>
                                        @else
                                            {{--<span>{{ Html::image(asset('images/backend/noImage.png'), 'alt', array('class' => 'img-rounded img-logo')) }}</span>--}}
                                            <span><i class="no-image-placeholder fa fa-picture-o"></i></span>
                                        @endif
                                        <div class="box-product-misuse-text no-margin">
                                            <span>{{ $value->nome }}</span>
                                        </div>
                                        <div class="box-product-misuse-btn">
                                            <div class="ui-tooltip top">
                                                <span class="tooltip-box">Imagem Principal</span>
                                                <label id="btn" class="ui-radio-btn-ctm btn-function no-margin-bottom margin-right-10">
                                                    <input onchange="location='{{ url('admin/sku/finalart/' . $value->id . '/principal/action') }}'" type="checkbox" name="arte_exibicao" id="editExibicao" value="{{ $value->arte_exibicao }}" {{ old('arte_exibicao', $value->arte_exibicao) === '1' ? 'checked' : '' }} {{ $value->status === '0' ? 'disabled' : '' }} />
                                                    <i class="checkmark icon fa fa-star no-margin"></i> 
                                                </label>
                                            </div>
                                            <div class="ui-tooltip top">
                                                @if($value->status === '1')
                                                    <span class="tooltip-box">Imagem Ativa</span>
                                                @else
                                                    <span class="tooltip-box">Imagem Inativa</span>
                                                @endif
                                                <label id="btn" class="ui-checkbox-ctm btn-function no-margin-bottom margin-right-10">
                                                    <input onchange="location='{{ url('admin/sku/finalart/' . $value->id . '/status/action') }}'" type="checkbox" name="status" id="editStatus" value="{{ $value->status }}" {{ old('status', $value->status) === '1' ? 'checked' : '' }} />
                                                    <i class="checkmark icon fa fa-check no-margin"></i> 
                                                </label>
                                            </div>
                                            <div class="dropdown drop-to-left">
                                                <a href="javascript:;" class="ui-tooltip top dropdown-toggle fa fa-ellipsis-h"><span class="tooltip-box">Opções</span></a>
                                                <div class="dropdown-list">
                                                    <ul> 
                                                        <li><a href="javascript:;" data-toggle="modal" data-target="#modalArtes" data-arte-id="{{ $value->id }}" id="btn-edit-{{ $value->id }}" class="btn-edit txt-bold"><span class="icon fa fa-edit"></span>Alterar</a></li>
                                                        <li><a href="javascript:;" data-arte-id="{{ $value->id }}" id="btn-remove-{{ $value->id }}" class="btn-remove txt-bold"><span class="icon fa fa-trash"></span>Excluir</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                @if ($a % 4 == 0)
                                    </tr>
                                @endif
                                <?php $a++ ?>
                            @endforeach
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        @else
            <div class="is-empty-placeholder">
                <div class="is-empty-body">
                    <i class="icon fa fa-camera"></i>
                    <span class="is-empty-text">Nenhuma imagem.</span>
                </div>
            </div>
        @endif
    {!! Form::close() !!}
</div>
@stop
