@extends('admin.admin_template')

@section('title')
	Embalagens
@stop

@section('content')

<script src="{{ asset('/js/admin/skus.js') }}"></script>
<script>
	$(function () {
		$('#btn-search').click(function () {
			var src = $('#txt-search').val();
			src.trim() == '' ? window.location = '<?php echo url('admin/sku'); ?>' : window.location = '<?php echo url('admin/sku/search'); ?>/' + src;
		});
	});
</script>

<div class="list-title-header">
	<h1>Embalagens</h1>
</div>
<div class="pull-right">
	<p>Total: {{ $total }} embalagens</p>
</div>

<div class="list-header">
	<div>
		<div class="col-xs-6 search">
			<form class="input-group">
				<input type="text" id="txt-search" class="form-control" placeholder="Buscar por Produto ...">
				<span class="input-group-btn">
					<button id="btn-search" class="btn btn-default" type="submit">Buscar</button>
				</span>
			</form>
		</div>

		<button type="button" id="btn-create" class="btn btn-info" data-toggle="modal" data-target="#modalSkus">
			<span class="glyphicon glyphicon-plus-sign"></span> Novo Sku
		</button>

		<!-- Modal de Criação/Edição de Sku -->
		@include('admin.skus._form')

	</div>
</div>

<div class="table-responsive clear">

@if (count($skus) >= 1)
	<table id="list-sku" class="table table-striped">
		<thead>
			<tr>
				<th>Nome</th>
				<th>Tipo</th>
				<th>Ordem</th>
				<th>Status</th>
				<th>Ações</th>
			</tr>
		</thead>
		<tbody>
			@foreach($skus as $sku)
				<tr>
					<td>{{ $sku->nome }}</td>
					<td>{{ $sku->promocional ? 'Promocional' : 'Regular' }}</td>
					<td>
						{{ $sku->ordem }}
						@if($sku->ordem)
							&ordm;
						@endif
					</td>
					<td>{{ $sku->status ? 'Ativo' : 'Inativo' }}</td>
					<td>
						<button type="button" id="btn-edit-{{$sku->id}}" class="btn btn-primary btn-sm btn-edit" data-toggle="modal" data-target="#modalSkus" data-sku-id="{{ $sku->id }}">
							<span class="glyphicon glyphicon-edit"></span> Gerenciar
						</button>
						<button type="button" id="btn-remove-{{$sku->id}}" class="btn btn-danger btn-sm btn-remove" data-sku-id="{{ $sku->id }}">
							<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Remover
						</button>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<center>
		{!! $skus->render() !!}
	</center>
@else
	<div>
		@if (count($produtos) == 0 || count($dimensoes) == 0)
			<div class="alert alert-warning box-msg" style="margin: 10px 0">
		        <h5>Para o cadastro de embalagem, é necessário que algum volume e produto sejam cadastrados antes</h5>
		    </div>
		@endif
		<h4>Nenhuma Embalagem Localizada</h4>
	</div>
@endif

</div>

@stop
