<!-- Modal -->
<div class="modal fade" id="modalUsuarios" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<span class="glyphicon glyphicon-plus-sign"></span>
				</h4>
			</div>
			<div class="modal-body">

				{!! Form::open(array('class' => 'form-horizontal')) !!}
					{{ Form::hidden('id', '', array('id' => 'editId')) }}
					<div class="form-group">
						{!! Form::label('lblNome', 'Nome', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
					    	{!! Form::text('nome', '', array('class' => 'form-control', 'id' => 'editNome')) !!}
				    	</div>
				    </div>
				    <div class="form-group">
						{!! Form::label('lblSobrenome', 'Sobrenome', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
					    	{!! Form::text('sobrenome', '', array('class' => 'form-control', 'id' => 'editSobrenome')) !!}
				    	</div>
				    </div>
				    <div class="form-group">
						{!! Form::label('lblEmail', 'Email', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
					    	{!! Form::text('email', '', array('class' => 'form-control', 'id' => 'editEmail')) !!}
				    	</div>
				    </div>
				    <div class="form-group">
						{!! Form::label('lblFone', 'Telefone', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
					    	{!! Form::text('fone', '', array('class' => 'form-control', 'id' => 'editFone')) !!}
				    	</div>
				    </div>
				    <div class="form-group">
						{!! Form::label('lblCargo', 'Cargo', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
					    	{!! Form::text('cargo', '', array('class' => 'form-control', 'id' => 'editCargo')) !!}
				    	</div>
				    </div>
				    <div class="form-group">
						{!! Form::label('lblDepto', 'Departamento', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
					    	{!! Form::text('depto', '', array('class' => 'form-control', 'id' => 'editDepto')) !!}
				    	</div>
				    </div>
				    <div class="form-group">
						{!! Form::label('lblPerfil', 'Perfil', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
					    	{!! Form::select('id_adm_perfil', $perfis, null, array('placeholder' => 'Selecione o Perfil...', 'class' => 'form-control', 'id' => 'editPerfil')) !!}
				    	</div>
				    </div>
				    <div class="form-group">
						{!! Form::label('lblEmpresa', 'Empresa', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
					    	{!! Form::select('id_adm_empresa', $empresas, null, array('placeholder' => 'Selecione a Empresa...', 'class' => 'form-control', 'id' => 'editEmpresa')) !!}
				    	</div>
				    </div>
				    <div class="form-group">
						{!! Form::label('lblStatus', 'Status', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8">
					    	{!! Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), null, array('class' => 'form-control', 'id' => 'editStatus')) !!}
				    	</div>
				    </div>
					<div class="form-group" id="listModulos">
						{!! Form::label('lblModulos', 'Módulos', array('class' => 'col-sm-3 control-label')) !!}
						@foreach($modulos as $key => $modulo)
							<div class="col-sm-2 control-label" style="text-align: left">
						    	{!! Form::checkbox('modulos[]', $key, null, array('id' => 'editModulo'.$key)) !!}
						    	{{ $modulo }}
						    </div>
				    	@endforeach
				    </div>
					<div class="form-group" id="listTokens">
						{!! Form::label('lblTokens', 'Tokens Guide', array('class' => 'col-sm-3 control-label')) !!}
						<div class="col-sm-8 control-label" style="text-align: left">
					    	{!! Form::checkbox('token_guide', 1, null, array('id' => 'editToken')) !!}
					    	Cria um link temporário de Guide
					    </div>
				    </div>
					<div class="modalButtons">
						{!! Form::button('Cancelar', array('class' => 'btn btn-info', 'data-dismiss' => 'modal', 'id' => 'btn-cancel-modal')) !!}
					    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
					</div>

				{!! Form::close() !!}

			</div>
		</div>

	</div>
</div>
