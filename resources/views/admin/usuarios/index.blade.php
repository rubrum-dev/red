@extends('admin.admin_template')

@section('title')
	Usuários
@stop

@section('content')

<script src="{{ asset('/js/admin/usuarios.js') }}"></script>
<script src='//cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js'></script>
<script>
	$(function () {
		$('#btn-search').click(function () {
			var src = $('#txt-search').val();
			src.trim() == '' ? window.location = "<?php echo url('admin/user'); ?>" : window.location = "<?php echo url('admin/user/search'); ?>/" + src;
		});
	});
</script>

<div class="list-title-header">
	<h1><!--span class="glyphicon glyphicon-user"></span-->Usuários</h1>
</div>
<div class="pull-right">
	<p>Total: {{ $total }} usuários</p>
</div>

<div class="list-header">
	<div>
		<div class="input-group col-xs-6 search">
			<form class="input-group">
				<input type="text" id="txt-search" class="form-control" placeholder="Buscar por Nome ...">
				<span class="input-group-btn">
					<button id="btn-search" class="btn btn-default" type="submit">Buscar</button>
				</span>
			</form>
		</div>

		<!-- Trigger the modal with a button -->
		<button type="button" id="btn-create" class="btn btn-info" data-toggle="modal" data-target="#modalUsuarios">
			<span class="glyphicon glyphicon-plus-sign"></span> Novo Usuário
		</button>

		<!-- Modal de Criação/Edição de Usuário -->
		@include('admin.usuarios._form')

	</div>
</div>

<div class="table-responsive clear">

@if (count($users) >= 1)
	<table id="list-users" class="table table-striped">
		<thead>
			<tr>
				<th>Nome</th>
				<th>Departamento</th>
				<th>Perfil</th>
				<th>Empresa</th>
				<th>Status</th>
				<th>Ações</th>
			</tr>
		</thead>
		<tbody>
			@foreach($users as $user)
				<tr>
					<td>{{ $user->nome }}</td>
					<td>{{ $user->depto }}</td>
					<td>{{ $user->profile->nome }}</td>
					<td>{{ $user->empresa->nome }}</td>
					<td>{{ $user->status ? 'Ativo' : 'Inativo' }}</td>
					<td>
						<button type="button" id="btn-edit-{{$user->id}}" class="btn btn-primary btn-sm btn-edit" data-toggle="modal" data-target="#modalUsuarios" data-user-id="{{ $user->id }}">
							<span class="glyphicon glyphicon-edit"></span> Editar
						</button>
						<button type="button" id="btn-remove-{{$user->id}}" class="btn btn-danger btn-sm btn-remove" data-user-id="{{ $user->id }}">
							<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Remover
						</button>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	<div>
		<h4>Nenhum Usuário Localizado</h4>
	</div>
@endif

</div>

@stop
