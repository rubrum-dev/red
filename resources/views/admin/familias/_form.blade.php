<!-- Modal -->
<div class="modal fade" id="modalFamilias" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close fa fa-times" data-dismiss="modal"></button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-loading">
				<div class="line-wobble"></div>
			</div>
			<div class="modal-body">
				{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-family', 'files' => 'true')) !!}
					{{ Form::hidden('id', '', array('id' => 'editId')) }}
					{{ Form::hidden('ordemCheck', $count == [] ? 0 : count($count), array('id' => 'editOrdemId')) }}
					<div class="form-group form-group-flex flex-align-start" id="box-nome">
						{!! Form::label('lblNome', 'Nome *', array('class' => 'col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
						<div class="col-xs-12">
					    	{!! Form::text('nome', '', array('class' => 'form-control', 'id' => 'editNome', 'placeholder' => 'Nome da Marca')) !!}
				    	</div>
				    </div>
					<div class="form-group form-group-flex flex-align-start" id="box-ordem">
						{!! Form::label('lblOrdem', 'Ordem de Exibição', array('class' => 'col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
						<div class="col-xs-12">
					    	{!! Form::select('ordem', $count, null, array('class' => 'form-control', 'id' => 'editOrdem')) !!}
				    	</div>
				    </div>
				    <div id="logos-id">
					    @if (count($tiposLogos) >= 1)
							<table id="list-tipos-logos" class="logo-familia no-margin">
								<tr>
									<td class="no-padding">
										<div class="form-group form-group-flex flex-align-center">
											{!! Form::label('', 'Logo com fundo', array('class' => 'col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
											<label class="name-logo control-label col-xs-12 txt-left txt-color-alt">{{ $tiposLogos[0]->nome ? 'Formato PNG medindo 155 x 155px com fundo colorido' : '' }}</label>		
										</div>
									</td>
								</tr>
								<tr>
									<td class="no-padding">
										<div class="form-group form-group-flex flex-align-center">
											<div class="col-xs-3 no-padding"></div>
											<div class="col-xs-12">
												<div data-title="Nenhum arquivo." class="d-inline-flex ui-tooltip-2 top">
													<div class="center-logo image-only">
														{{ Html::image(asset(''), 'alt', array('class' => 'img-rounded img-logo', 'id' => $tiposLogos[0]->id )) }}
													</div>
												</div>
												<div class="upload-logo">
													{!! Form::file('images') !!}
												</div>
											</div>
										</div>
									</td>
								</tr>
							</table>
							<div class="form-group form-group-flex flex-align-start margin-top-15 no-margin-bottom">
								{!! Form::label('lblCorHexa', 'Definir cor do fundo', array('class' => 'col-xs-3 control-label no-margin no-padding-left no-padding-right txt-right')) !!}
								<div class="name-logo col-xs-12 txt-color-alt">
									{!! Form::text('cor_hexa', '', array('class' => 'form-control minicolors', 'id' => 'cor_hexa', 'placeholder' => '#FFFFFF', 'autocomplete' => 'off')) !!}
								</div>
							</div>
						@endif
					</div>
					<div class="modalBtnGroup">
						{!! Form::button('Cancelar', array('class' => 'btn-control btn-alt', 'data-dismiss' => 'modal', 'id' => 'btn-cancel-modal')) !!}
					    {!! Form::submit('Salvar', array('class' => 'btn-control btn-positive')) !!}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
<div id="packShelfImgProgress" class="progress-circle animate">
	<svg id="svg" viewbox="0 0 100 100">
		<circle cx="50" cy="50" r="40" fill="none"/>
		<path fill="none" stroke-linecap="round" stroke-width="4" stroke="#fff" d="M50 10 a 40 40 0 0 1 0 80 a 40 40 0 0 1 0 -80"/>
		<text id="count" class="progress" x="50" y="50" text-anchor="middle" dy="7"></text>
	</svg>
</div>