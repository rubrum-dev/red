<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Rubrum Software - </title>
	<!-- Favico -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=RyQmEa60ez">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=RyQmEa60ez">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=RyQmEa60ez">
    <link rel="manifest" href="/site.webmanifest?v=RyQmEa60ez">
    <link rel="mask-icon" href="/safari-pinned-tab.svg?v=RyQmEa60ez" color="#c41425">
    <link rel="shortcut icon" href="/favicon.ico?v=RyQmEa60ez">
    <meta name="apple-mobile-web-app-title" content="Rubrum">
    <meta name="application-name" content="Rubrum">
    <meta name="msapplication-TileColor" content="#c41425">
    <meta name="theme-color" content="#ffffff">
	<!-- Fonts -->
	<link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=Dosis:400,500,600,700' />
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:400,300" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" />
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/fontawesome/4.7.0/css/font-awesome.min.css') }}" />
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('/css/three-dots/dist/three-dots.css?v=1.0') }}" />--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('/css/site/layout.css?v=5.5.3') }}" />--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/cliente_1/compare.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/cliente_1/layout.css') }}" />--}}
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/cliente_'. config('app.costummer_id') .'/compare.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/cliente_'. config('app.costummer_id') .'/layout.css') }}" />
    <!-- Scripts Locais -->
    <script src="{{ asset('/js/site/jquery.min.js') }}"></script>
	<script src="{{ asset('/js/site/site.js?v=2.0') }}"></script>
</head>
<body>
    <div class="lds-css ng-scope" style="display:none;">
        <div style="width:100%;height:100%" class="lds-rolling">
            <div class="lds-in">
                <div class="dot-overtaking"></div>
            </div>
        </div>
    </div>
    <div class="master">
        <div class="container error-screen clearfix">
            <div class="content">
                <div class="error-page-panel">
                    <i class="icon color-default fa fa-chain-broken"></i>
                    <div class="body">
                        <h2 class="title">
                            <span>Este link expirou</span>
                        </h2>
                        <div class="text">
                            <p>
                                O link para acessar o arquivo expirou. Entre em contato com o administrador e solicite um novo link. <br /><br />
                                Atente-se para o prazo de expiração.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>