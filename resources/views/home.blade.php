<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Rubrum Software - Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais</title>
    <script type="text/javascript" src="{{ asset('/js/site/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset2('/js/site/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset2('/js/site/jquery-confirm-config.js') }}"></script>
    <script type="text/javascript" src="{{ asset2('/js/site/OverlayScrollbars.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset2('/js/site/my-alert.js') }}"></script>
	<script type="text/javascript" src="{{ asset2('/js/site/login.js') }}"></script>
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/fontawesome/4.7.0/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/OverlayScrollbars.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/jquery-confirm.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/cliente_'. config('app.costummer_id') .'/layout.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/cliente_'. config('app.costummer_id') .'/home-v2.css') }}" />
    <!-- Favico -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=RyQmEa60ez">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=RyQmEa60ez">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=RyQmEa60ez">
    <link rel="manifest" href="/site.webmanifest?v=RyQmEa60ez">
    <link rel="mask-icon" href="/safari-pinned-tab.svg?v=RyQmEa60ez" color="#c41425">
    <link rel="shortcut icon" href="/favicon.ico?v=RyQmEa60ez">
    <meta name="apple-mobile-web-app-title" content="Rubrum">
    <meta name="application-name" content="Rubrum">
    <meta name="msapplication-TileColor" content="#c41425">
    <meta name="theme-color" content="#ffffff">
    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" />
</head>
<body>
    <div class="master">
        <div class="wrapped">
            <div class="home-container txt-center">
                <div class="home-container-box">
                    <span class="brand"></span>
                    <section>
                        <p class="access-method-txt txt-regular">
                            Escolha a forma de acesso ao Rubrum:
                        </p>
                        <div class="free-access btn-group margin-top-30 no-margin-bottom">
                            <a href="{{ url('./site') }}" class="btn-control btn-panel btn-link">
                                <i class="icon fa fa-unlock"></i>
                                <span class="btn-body"><strong class="txt-default">Acesso Livre</strong><strong class="txt-regular">Para baixar imagens e mockups não é necessário senha</strong></span>
                            </a>
                        </div>
                        <div class="sign-in btn-registered-fake btn-group margin-top-15 no-margin-bottom">
                            <a href="javascript:;" class="btn-control btn-panel btn-link">
                                <i class="icon fa fa-lock"></i>
                                <span class="btn-body"><strong class="txt-default">Acesso com Conta Rubrum</strong><strong class="txt-regular">Para usuários cadastrados direto no Rubrum</strong></span>
                            </a>
                        </div>
                        {!! Form::open(array('route' => 'site.login', 'class' => 'home-container-form form-sign-in')) !!}
                            <p class="txt-regular">
                                Acesso com Conta Rubrum
                            </p>
                            <div class="block margin-top-30">    
                                <div class="flexbox-container margin-bottom-15">
                                    <div class="input-group animate">
                                        {!! Form::text('email', Session::get('email'), array('class' => 'input-control input-email-only', 'id' => 'inputEmail', 'placeholder' => 'E-mail')) !!}
                                        <span class="addon fa fa-user-circle"></span>
                                    </div>
                                </div>
                                <div class="flexbox-container margin-bottom-20">
                                    <div class="input-group animate">
                                        {!! Form::password('senha', array('class' => 'input-control no-space', 'id' => 'inputPassword', 'placeholder' => 'Senha')) !!}
                                        <span class="addon fa fa-key"></span>
                                    </div>
                                </div>
                                <div class="btn-group no-margin-bottom">
                                    {!! Form::button('<span><i class="icon pull-left margin-right-10 fa fa-lock"></i><i class="icon pull-left no-margin fa fa-spin fa-circle-o-notch"></i><strong>Acessar</strong></span>', array('type' => 'submit', 'class' => 'btn-control btn-link btn-positive btn-signin btn-registered')) !!}
                                </div>
                                <div class="btn-group margin-top-10 no-margin-bottom">
                                    {!! Html::decode(Html::link('forgot', '<span>Recuperar Acesso e Senha</span>', array('class' => 'btn btn-control btn-hollow btn-link animate'))) !!}
                                </div>
                                <div class="back btn-group margin-top-10 no-margin-bottom">
                                    <a href="javascript:;" class="btn btn-control btn-hollow btn-link animate"><span><i class="icon pull-left margin-right-10 fa fa-arrow-left"></i>Voltar</span></a>
                                </div>
                            </div>
                        {!! Form::close() !!}
                        @if (config('app.sso_name'))
                        <div class="login-sso btn-group margin-top-15 no-margin-bottom">
                            <a href="login/sso" class="btn-control btn-panel btn-link tooltip2">
                                <span class="icon fa-stack">
                                    <i class="fa fa-cloud"></i>
                                    <i class="fa fa-key"></i>
                                </span>
                                <span class="btn-body"><strong class="txt-default">Acesso com Conta {{ config('app.sso_name') }}</strong><strong class="txt-regular">Para usuários gerenciados via SSO (Single Sign-On)</strong></span>
                                <span class="tooltiptext tooltip-right">A partir de agora todos os usuários <strong class="txt-color-primary">{{ config('app.sso_name') }}</strong> devem acessar utilizando essa opção.</span>
                            </a>
                        </div>
                        @endif
                        <!--div class="login-sso btn-group margin-top-15 no-margin-bottom">
                            <a href="https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=3d78ebb7-d9a9-430e-ab36-077657db81f7&response_type=token&redirect_uri=https://stg.rubrum.com.br/login/sso&response_mode=form_post&scope=openid%20user.read" class="btn-control btn-panel btn-link tooltip2">
                                <span class="icon fa-stack">
                                    <i class="fa fa-cloud"></i>
                                    <i class="fa fa-key"></i>
                                </span>
                                <span class="btn-body"><strong class="txt-default">Acesso com Rubrum Azure STG (testes)</strong><strong class="txt-regular">Para usuários gerenciados via SSO (Single Sign-On)</strong></span>
                            </a>
                        </div-->
                    </section>
                </div>
                <footer class="footer clearfix">
                    <div class="copyright pull-left">
                        <span class="txt-uppercase">Rubrum Software &copy; {{ date("Y") }}</span>
                    </div>
                    <a href="http://www.rubrum.com.br" target="_blank" class="footer-brand pull-right"></a>
                </footer>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <script>
            @foreach ($errors->all() as $error)
                @if (Session::get('sso') == 1)
                    $.alert({
                        title: '',
                        content: '<div class="custom-alert-contents danger">' +
                            '   <span class="icon fa fa-exclamation"></span>' +
                            '   <div class="custom-alert-heading">' +
                            '       <h4>Atenção</h4>' +
                            '   </div>' +
                            '   <p>{!! $error !!}</p>' +
                            '</div>',
                        escapeKey: 'Entendi',
                        buttons: {
                            Entendi: {
                                keys: ['esc', 'enter', 'space'],
                                text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                                btnClass: 'btn-confirm my-alert-confirm',
                                action: function (Entendi) {
                                    window.location = '/login/sso';
                                }
                            }
                        },
                        closeIcon: true,
                        closeIconClass: 'jconfirm-btn-close fa fa-times',
                        boxWidth: '910px',
                        useBootstrap: false,
                        onOpenBefore: function () {
                            //$('body').addClass('jconfirm-overlay');
                            $('body').addClass('no-scroll');
                            //$('.master').addClass('blurred');
                            $('.jconfirm-content').css('marginLeft', '0px');
                            $('.ng-confirm').addClass('blurred');
                            $('.jconfirm').css('overflow', 'auto');
                            $('.my-alert-confirm').focus();
                            
                            $('.jconfirm').overlayScrollbars({
                                scrollbars: {
                                    autoHide: 'move'
                                }
                            });
                        },
                        onClose: function () {
                            //$('body').removeClass('jconfirm-overlay');
                            $('body').removeClass('no-scroll');
                            //$('.master').removeClass('blurred');
                            $('.ng-confirm').removeClass('blurred');
                            $('.jconfirm').css('overflow', 'hidden');
                            $('.my-alert-confirm').blur();

                            if ($('.ng-confirm').length) {
                                $('.master').addClass('blurred');
                            }

                            window.location = '/login/sso';
                        }
                    });
                @else
                    @if($error === 'usuario-expirado')    
                        $.alert({
                            title: '',
                            content: '<div class="custom-alert-contents danger">' +
                                '   <span class="icon fa fa-exclamation"></span>' +
                                '   <div class="custom-alert-heading">' +
                                '       <h4>Atenção</h4>' +
                                '   </div>' +
                                '   <p>Seu usuário foi expirado pois não acessou o sistema nos últimos 90 dias.</p>' +
                                '   <p>Recupere o seu acesso através do botão abaixo seguindo as instruções.</p>' +
                                '</div>',
                            escapeKey: true,
                            buttons: {
                                Recuperar: {
                                    keys: ['enter', 'space'],
                                    text: '<span><i class="icon fa fa-key"></i>Recuperar Acesso</span>',
                                    btnClass: 'btn btn-confirm',
                                    action: function (Recuperar) {
                                        //window.location = '/forgot'
                                        reenviarToken('{{ Session::get('id_usuario') }}');
                                    }
                                },
                                Cancelar: {
                                    keys: ['esc'],
                                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                                    btnClass: 'btn btn-cancel',
                                    action: function (Cancelar) {
                                    }
                                }
                            },
                            closeIcon: true,
                            closeIconClass: 'jconfirm-btn-close fa fa-times',
                            boxWidth: '910px',
                            useBootstrap: false,
                            onOpenBefore: function () {
                                //$('body').addClass('jconfirm-overlay');
                                $('body').addClass('no-scroll');
                                //$('.master').addClass('blurred');
                                $('.jconfirm-content').css('marginLeft', '0px');
                                $('.ng-confirm').addClass('blurred');
                                $('.jconfirm').css('overflow', 'auto');
                                $('.my-alert-confirm').focus();
                                
                                $('.jconfirm').overlayScrollbars({
                                    scrollbars: {
                                        autoHide: 'move'
                                    }
                                });
                            },
                            onClose: function () {
                                //$('body').removeClass('jconfirm-overlay');
                                $('body').removeClass('no-scroll');
                                //$('.master').removeClass('blurred');
                                $('.ng-confirm').removeClass('blurred');
                                $('.jconfirm').css('overflow', 'hidden');
                                $('.my-alert-confirm').blur();

                                if ($('.ng-confirm').length) {
                                    $('.master').addClass('blurred');
                                }
                            }
                        });
                    @elseif ($error === 'token-expirado')
                        $.alert({
                            title: '',
                            content: '<div class="custom-alert-contents danger">' +
                                '   <span class="icon fa fa-exclamation"></span>' +
                                '   <div class="custom-alert-heading">' +
                                '       <h4>Atenção</h4>' +
                                '   </div>' +
                                '   <p>O link para recuperar seu acesso perdeu a validade.</p>' +
                                '   <p>Receba um novo link no seu e-mail através do botão abaixo seguindo as instruções.</p>' +
                                '</div>',
                            escapeKey: true,
                            buttons: {
                                Enviar: {
                                    keys: ['enter', 'space'],
                                    text: '<span><i class="icon fa fa-link"></i>Enviar Novo Link</span>',
                                    btnClass: 'btn btn-confirm',
                                    action: function (Enviar) {
                                        reenviarToken('{{ Session::get('id_usuario') }}');
                                    }
                                },
                                Cancelar: {
                                    keys: ['esc'],
                                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                                    btnClass: 'btn btn-cancel',
                                    action: function (Cancelar) {
                                    }
                                }
                            },
                            closeIcon: true,
                            closeIconClass: 'jconfirm-btn-close fa fa-times',
                            boxWidth: '910px',
                            useBootstrap: false,
                            onOpenBefore: function () {
                                //$('body').addClass('jconfirm-overlay');
                                $('body').addClass('no-scroll');
                                //$('.master').addClass('blurred');
                                $('.jconfirm-content').css('marginLeft', '0px');
                                $('.ng-confirm').addClass('blurred');
                                $('.jconfirm').css('overflow', 'auto');
                                $('.my-alert-confirm').focus();
                                
                                $('.jconfirm').overlayScrollbars({
                                    scrollbars: {
                                        autoHide: 'move'
                                    }
                                });
                            },
                            onClose: function () {
                                //$('body').removeClass('jconfirm-overlay');
                                $('body').removeClass('no-scroll');
                                //$('.master').removeClass('blurred');
                                $('.ng-confirm').removeClass('blurred');
                                $('.jconfirm').css('overflow', 'hidden');
                                $('.my-alert-confirm').blur();

                                if ($('.ng-confirm').length) {
                                    $('.master').addClass('blurred');
                                }
                            }
                        });
                    @else
                        myAlert('{{ $error }}', 'error');
                    @endif
                @endif
            @endforeach
        </script>
    @endif
    @if (Session::has('success'))
        <script>
            
            $(function () {
                
                $('.sign-in').click();
                
            });
            
            myAlert('{{ Session::get('success') }}', 'success');
        </script>
    @endif
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-101521196-1', 'auto');
        ga('send', 'pageview');
    </script>
</body>
</html>