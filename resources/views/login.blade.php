<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Rubrum Software - Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais</title>
    <script type="text/javascript" src="{{ asset('/js/site/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/site/jquery-confirm.js?v=2.0') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/site/my-alert.js?v=2.0') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/site/my-dialog.js?v=2.0') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/site/login.js') }}"></script>
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/site/metrize-icons.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/jquery-confirm.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/jquery-confirm-custom.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/login.css') }}" />
    <!-- Favico -->
    <<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=RyQmEa60ez">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=RyQmEa60ez">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=RyQmEa60ez">
    <link rel="manifest" href="/site.webmanifest?v=RyQmEa60ez">
    <link rel="mask-icon" href="/safari-pinned-tab.svg?v=RyQmEa60ez" color="#c41425">
    <link rel="shortcut icon" href="/favicon.ico?v=RyQmEa60ez">
    <meta name="apple-mobile-web-app-title" content="Rubrum">
    <meta name="application-name" content="Rubrum">
    <meta name="msapplication-TileColor" content="#c41425">
    <meta name="theme-color" content="#ffffff">
    <!-- Fonts -->
    <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Dosis:400,500,600,700' />
</head>
<body>
    <div class="box-home-text">
        <div class="logo-center">
            {{ Html::image(asset('/images/layout/logo_site.png')) }}
        </div>
        {{ Html::image(asset('/images/icons/frontend/logo-email-login.png')) }}
        {!! Form::open(array('route' => 'site.login')) !!}
            <span id="reauth-email" class="reauth-email"></span>
            {!! Form::email('email', '', array('class' => 'form-control', 'id' => 'inputEmail', 'placeholder' => 'E-mail', 'required', 'autofocus')) !!}
            {!! Form::password('senha', array('class' => 'form-control no-space', 'id' => 'inputPassword', 'placeholder' => 'Senha', 'required')) !!}<br />
            {!! Form::button('Entrar <span class="icon icon-arrow-right-light"></span>', array('class' => 'btn btn-lg btn-primary btn-block btn-signin')) !!}
        {!! Form::close() !!}
        <div>{{ Html::link('forgot', 'Esqueceu a Senha?', array('class' => 'forgot-password')) }}</div>
        <div>{{ Html::link('/', 'Voltar', array('class' => 'forgot-password')) }}</div>
        <div class="messages">
            @if (count($errors) > 0)
                <div class="alert-error">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br />
                    @endforeach
                </div>
            @endif
        </div>
    </div>
    <div class="container center">
        <div class="box-home-image">
            {{ Html::image(asset('/images/layout/quadro_login.png')) }}
        </div>
        <div class="marcas-home">
            {{-- {{ Html::image(asset('/images/layout/marcas_home.png')) }} --}}
            {{-- <h1 style="color: #58595B;">Em Construção</h1> --}}
        </div>
    </div>
    <div class="footer">
        {{ Html::image(asset('/images/layout/logo-rubrum-white.png'), 'Rubrum') }}
        Rubrum Software &copy; {{ date("Y") }}<br />
        {{-- <a href="#">Saiba Mais</a> --}}
    </div>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-101521196-1', 'auto');
        ga('send', 'pageview');
    </script>
</body>
</html>
