<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Rubrum Software - Gerenciamento de Embalagens e Fluxo de Aprovação de Artes-finais</title>
    <script type="text/javascript" src="{{ asset('/js/site/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset2('/js/site/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset2('/js/site/jquery-confirm-config.js') }}"></script>
    <script type="text/javascript" src="{{ asset2('/js/site/OverlayScrollbars.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset2('/js/site/my-alert.js') }}"></script>
	<script type="text/javascript" src="{{ asset2('/js/site/passwordCheckStrength.js') }}"></script>
    <script type="text/javascript" src="{{ asset2('/js/site/login.js') }}"></script>
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/fontawesome/4.7.0/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/OverlayScrollbars.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/jquery-confirm.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/cliente_'. config('app.costummer_id') .'/layout.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset2('/css/site/cliente_'. config('app.costummer_id') .'/home-v2.css') }}" />
    <!-- Favico -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=RyQmEa60ez">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=RyQmEa60ez">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=RyQmEa60ez">
    <link rel="manifest" href="/site.webmanifest?v=RyQmEa60ez">
    <link rel="mask-icon" href="/safari-pinned-tab.svg?v=RyQmEa60ez" color="#c41425">
    <link rel="shortcut icon" href="/favicon.ico?v=RyQmEa60ez">
    <meta name="apple-mobile-web-app-title" content="Rubrum">
    <meta name="application-name" content="Rubrum">
    <meta name="msapplication-TileColor" content="#c41425">
    <meta name="theme-color" content="#ffffff">
    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" />
</head>
<body>
    <div class="master">
        <div class="wrapped">
            <div class="home-container txt-center">
                <div class="home-container-box">
                    <span class="brand"></span>
                    <section class="sub-bs">
                        <p class="txt-regular">
                            Crie sua senha com no mínimo 6 caracteres.
                        </p>
                        {!! Form::open(array('autocomplete' => 'off', 'route' => 'site.first', 'class' => 'home-container-form form-first-access')) !!}
                            <div class="block margin-top-30">    
                                <div class="flexbox-container margin-bottom-20">
                                    <div class="input-group animate">
                                        {!! Form::password('password', array('autocomplete' => 'new-password', 'class' => 'input-control no-space', 'id' => 'inputNewPwd', 'placeholder' => 'Senha', 'autofocus')) !!}
                                        <span class="addon fa fa-key"></span>
                                    </div>
                                </div>
                                <div class="flexbox-container margin-bottom-20">
                                    <div class="input-group animate">
                                        {!! Form::password('password_confirmation', array('autocomplete' => 'new-password', 'class' => 'input-control no-space', 'id' => 'inputPwdConfirm', 'placeholder' => 'Repetir Senha')) !!}
                                        <span class="addon fa fa-key"></span>
                                    </div>
                                </div>
                                <div id="popover-password" class="margin-bottom-20">
                                    <div class="progress">
                                        <div id="password-strength" 
                                            class="progress-bar" 
                                            role="progressbar" 
                                            aria-valuenow="40" 
                                            aria-valuemin="0" 
                                            aria-valuemax="100" 
                                            style="width:0%">
                                        </div>
                                    </div>
                                    {{--<div id="result"></div>--}}
                                    <ul class="list-unstyled">
                                        <li><span class="upper-case"><i class="fa fa-circle" aria-hidden="true"></i>Deve conter letra maiúscula</span></li>
                                        <li><span class="lower-case"><i class="fa fa-circle" aria-hidden="true"></i>Deve conter letra minúscula</span></li>
                                        <li><span class="one-number"><i class="fa fa-circle" aria-hidden="true"></i>Deve conter número (0 - 9)</span></li>
                                        {{--<li><span class="one-special-char"><i class="fa fa-circle" aria-hidden="true"></i>Deve conter caracter especial (%, &, @, #, $)</span></li>--}}
                                        <li><span class="six-character"><i class="fa fa-circle" aria-hidden="true"></i>Deve conter no mínimo 6 caracteres</span></li>
                                    </ul>
                                </div>
                                <div class="agreement flexbox-container margin-bottom-10">
                                    <label class="agreement-label checkbox-control">
                                        <input type="checkbox" name="chkAceitarTermos" id="chkAceitarTermos" />
                                        <span class="checkmark"></span>
                                        <small class="checkbox-txt agreement-txt">Aceito os <a href="site/termos-de-uso" target="_blank">Termos de Uso</a></small>
                                    </label>
                                </div>
                                <div class="agreement flexbox-container margin-bottom-20">
                                    <label class="agreement-label checkbox-control">
                                        <input type="checkbox" name="chkAceitarPoliticaPrivacidade" id="chkAceitarPoliticaPrivacidade" />
                                        <span class="checkmark"></span>
                                        <small class="checkbox-txt agreement-txt">Concordo com a <a href="site/politica-de-privacidade" target="_blank">Política de Privacidade</a></small>
                                    </label>
                                </div>
                                <div class="btn-group no-margin">
                                    {!! Form::button('<span><i class="icon fa fa-lock pull-left margin-right-10"></i><i class="icon pull-left no-margin fa fa-spin fa-circle-o-notch"></i><strong>Salvar e Acessar</strong></span>', array('type' => 'submit', 'class' => 'btn-control btn-positive btn-enviar btn-salvar-acessar')) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </section>
                </div>
                <footer class="footer clearfix">
                    <div class="copyright pull-left">
                        <span class="txt-uppercase">Rubrum Software &copy; {{ date("Y") }}</span>
                    </div>
                    <a href="http://www.rubrum.com.br" target="_blank" class="footer-brand pull-right"></a>
                </footer>
            </div>
        </div>
    </div>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-101521196-1', 'auto');
        ga('send', 'pageview');
    </script>
</body>
</html>
