<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterArtwPerfisUsuariosTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_perfis_usuarios_tickets', function (Blueprint $table) {
            $table->integer('aprovado')->default(0)->after('recebe_email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_perfis_usuarios_tickets', function (Blueprint $table) {
            $table->dropColumn('aprovado');
        });
    }
}
