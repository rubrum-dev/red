<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNomeCompletoArtwVariacoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_variacoes', function (Blueprint $table) {
            $table->string('nome_original')->default(null)->after('nome')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_variacoes', function (Blueprint $table) {
            $table->dropColumn('nome_original');
        });
    }
}
