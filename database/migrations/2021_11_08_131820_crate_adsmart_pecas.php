<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateAdsmartPecas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adsmart_pecas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome')->nullable();

            $table->integer('id_campanha_midia')->unsigned()->nullable();
            $table->foreign('id_campanha_midia')->references('id')
                  ->on('adsmart_campanha_midias')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
