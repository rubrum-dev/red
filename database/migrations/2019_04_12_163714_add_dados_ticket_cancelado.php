<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDadosTicketCancelado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_tickets', function (Blueprint $table) {
            
            $table->string('cancelado_campanha')->default(null)->nullable()->after('cancelado_nome_original');
            $table->string('cancelado_variacao')->default(null)->nullable()->after('cancelado_campanha');
            $table->string('cancelado_item')->default(null)->nullable()->after('cancelado_variacao');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_tickets', function (Blueprint $table) {
            
            $table->dropColumn('cancelado_campanha');
            $table->dropColumn('cancelado_variacao');
            $table->dropColumn('cancelado_item');
            
        });
    }
}
