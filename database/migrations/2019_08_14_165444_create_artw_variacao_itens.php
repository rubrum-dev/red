<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtwVariacaoItens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artw_variacao_itens', function(Blueprint $table)
        {
            $table->integer('id_variacao')->unsigned()->nullable();
            $table->foreign('id_variacao')->references('id')
                  ->on('artw_variacoes')->onDelete('cascade');
            
            $table->integer('id_item')->unsigned()->nullable();
            $table->foreign('id_item')->references('id')
                  ->on('artw_itens')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artw_variacao_itens');
    }
}
