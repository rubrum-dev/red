<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsuariosModulos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_usuarios_modulos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_adm_usuario')->unsigned();
            $table->foreign('id_adm_usuario')->references('id')->on('adm_usuarios');
            $table->integer('id_adm_modulo')->unsigned();
            $table->foreign('id_adm_modulo')->references('id')->on('adm_modulos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adm_usuarios_modulos');
    }
}
