<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsuariosProdutos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_usuarios_produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_adm_usuario')->unsigned();
            $table->foreign('id_adm_usuario')->references('id')->on('adm_usuarios');
            $table->integer('id_produto')->unsigned();
            $table->foreign('id_produto')->references('id')->on('produtos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adm_usuarios_produtos');
    }
}
