<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMensagensEnvios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_comentarios', function (Blueprint $table) {
            $table->text('array_envios')->default(null)->nullable()->after('cor');
        });

        Schema::table('artw_respostas', function (Blueprint $table) {
            $table->text('array_envios')->default(null)->nullable()->after('cor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
