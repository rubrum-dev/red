<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModuloTicket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_tickets', function ($table) {
            $table->integer('modulo')->unsigned()->nullable()->default(1)->after('id');
            $table->integer('id_peca')->unsigned()->nullable()->default(null)->after('id_item');
            $table->string('slug_status')->default(null)->nullable()->after('id_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_tickets', function ($table) {
            $table->dropColumn('modulo');
            $table->dropColumn('id_peca');
            $table->dropColumn('slug_status');
        });
    }
}
