<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdItemVersoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_versoes', function (Blueprint $table) {
            $table->integer('id_item')->unsigned()->nullable()->default(null)->after('id_variacao');
            $table->foreign('id_item')->references('id')->on('artw_itens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_versoes', function (Blueprint $table) {
            $table->dropForeign('artw_versoes_id_item_foreign');
            $table->dropColumn('id_item');
        });
    }
}
