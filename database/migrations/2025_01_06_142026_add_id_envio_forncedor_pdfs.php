<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdEnvioForncedorPdfs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_fornecedor_pdfs', function (Blueprint $table) {
            $table->integer('id_envio')->unsigned()->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_fornecedor_pdfs', function (Blueprint $table) {
            $table->dropColumn('id_envio');
        });
    }
}
