<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableAdmUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adm_usuarios', function (Blueprint $table) {
            
            $raw = 'ALTER TABLE `adm_usuarios`
                    MODIFY COLUMN `id_adm_perfil` int(10) UNSIGNED NULL,
                    MODIFY COLUMN `email` varchar(255) NULL';
            
            DB::statement($raw);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
