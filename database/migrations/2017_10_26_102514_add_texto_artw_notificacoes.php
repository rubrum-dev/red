<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTextoArtwNotificacoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_notificacoes', function (Blueprint $table) {
            $table->text('texto')->default(null)->after('assunto')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_notificacoes', function (Blueprint $table) {
            $table->dropColumn('texto');
        });
    }
}
