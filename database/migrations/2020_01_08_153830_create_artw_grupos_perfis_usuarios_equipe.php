<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtwGruposPerfisUsuariosEquipe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artw_grupos_perfis_usuarios_equipe', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_usuario')->unsigned();
            $table->foreign('id_usuario')->references('id')->on('adm_usuarios');
            $table->integer('id_parent')->unsigned();
            $table->foreign('id_parent')->references('id')->on('artw_grupos_perfis_usuarios')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artw_grupos_perfis_usuarios_equipe');
    }
}
