<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Proporcoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proporcoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_categoria_proporcao')->unsigned();
            $table->foreign('id_categoria_proporcao')->references('id')->on('categorias_proporcoes');
            $table->integer('id_tipo_proporcao_versao')->unsigned();
            $table->foreign('id_tipo_proporcao_versao')->references('id')->on('tipos_proporcoes_versoes');
            $table->string('thumb');
            $table->string('descricao');
            $table->enum('status', [0,1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proporcoes');
    }
}
