<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterIdItemVariacaoArtwTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_tickets', function (Blueprint $table) {
            
            $table->dropForeign('artw_tickets_ibfk_3');
            $table->dropForeign('artw_tickets_ibfk_7');
            
            $table->foreign('id_item')->references('id')->on('artw_itens')->onDelete('set null');
            $table->foreign('id_variacao')->references('id')->on('artw_variacoes')->onDelete('set null');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
