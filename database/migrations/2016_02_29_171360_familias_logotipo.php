<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FamiliasLogotipo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('familias_logotipos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_logotipo')->unsigned();
            $table->foreign('id_logotipo')->references('id')->on('logotipos');
            $table->integer('id_familia')->unsigned();
            $table->foreign('id_familia')->references('id')->on('familias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('familias_logotipos');
    }
}
