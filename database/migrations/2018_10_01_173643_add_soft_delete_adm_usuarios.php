<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeleteAdmUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adm_usuarios', function ($table) {
            $table->softDeletes();
            $table->string('email_excluido')->default(null)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("adm_usuarios", function ($table) {
            $table->dropSoftDeletes();
            $table->dropColumn('email_excluido');
        });
    }
}
