<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpiradoEmAdmUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adm_usuarios', function (Blueprint $table) {
            $table->dateTime('expirado_em')->default(null)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adm_usuarios', function (Blueprint $table) {
            $table->dropColumn('expirado_em');
        });
    }
}
