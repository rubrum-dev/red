<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCanceladoNomeOriginalArtwTicket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_tickets', function (Blueprint $table) {
            $table->string('cancelado_nome_original')->default(null)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_tickets', function (Blueprint $table) {
            
            $table->dropColumn('cancelado_nome_original');
            
        });
    }
}
