<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVariacaoArtesFinais extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artes_finais', function (Blueprint $table) {
            $table->integer('id_variacao')->unsigned()->nullable()->default(null)->after('id_sku');
            $table->foreign('id_variacao')->references('id')->on('artw_variacoes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artes_finais', function (Blueprint $table) {
            $table->dropForeign('artes_finais_id_variacao_foreign');
            $table->dropColumn('id_variacao');
        });
    }
}
