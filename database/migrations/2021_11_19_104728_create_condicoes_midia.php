<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCondicoesMidia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adsmart_condicoes_midia', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome')->nullable();
            $table->text('texto')->nullable();
            $table->integer('abertura_ticket')->unsigned()->nullable();
            $table->integer('reprovacao')->unsigned()->nullable();
            $table->integer('obrigatoria')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adsmart_condicoes_midia');
    }
}
