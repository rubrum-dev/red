<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposAdsmartMidiaTipos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adsmart_midia_tipos', function (Blueprint $table) {
            $table->integer('status')->unsigned()->nullable()->default(0)->after('nome');
            $table->integer('bloquear_auto_aprovacao')->unsigned()->nullable()->default(0)->after('nome');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adsmart_midia_tipos', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('bloquear_auto_aprovacao');
        });
    }
}
