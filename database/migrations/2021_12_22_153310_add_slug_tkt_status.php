<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlugTktStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_tkt_status', function (Blueprint $table) {
            
            $table->integer('modulo')->unsigned()->nullable()->default(1)->after('id');
            $table->string('slug')->default(null)->nullable()->after('nome');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_tkt_status', function ($table) {
            $table->dropColumn('modulo');
            $table->dropColumn('slug');
        });
    }
}
