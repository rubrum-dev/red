<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArtesFinais extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artes_finais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_sku')->unsigned();
            $table->foreign('id_sku')->references('id')->on('skus');
            $table->string('nome')->nullable();
            $table->string('nome_arte')->nullable();
            $table->string('thumb')->nullable();
            $table->string('path_cmyk')->nullable();
            $table->string('path_rgb')->nullable();
            $table->enum('promocional', [0,1])->default(0);
            $table->dateTime('dt_expiracao')->nullable();
            $table->string('link_pdf')->nullable();
            $table->string('link_zip')->nullable();
            $table->enum('arte_exibicao', [0,1])->default(0);
            $table->enum('status', [0,1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artes_finais');
    }
}
