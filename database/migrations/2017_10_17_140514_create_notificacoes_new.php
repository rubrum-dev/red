<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificacoesNew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('artw_notificacoes_usuario')){
            Schema::drop('artw_notificacoes_usuario');
        }

        if (Schema::hasTable('artw_notificacoes')){
            Schema::drop('artw_notificacoes');
        }

        if (Schema::hasTable('artw_notificacao_tipos')){
            Schema::drop('artw_notificacao_tipos');
        }

        Schema::create('artw_notificacoes_tipos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('slug');
            $table->string('template');
            $table->string('assunto');
            $table->integer('conta_notificacao')->unsigned();
            $table->timestamps();
        });

        Schema::create('artw_notificacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipo')->unsigned();
            $table->foreign('id_tipo')->references('id')->on('artw_notificacoes_tipos');
            $table->integer('id_ticket')->unsigned();
            $table->foreign('id_ticket')->references('id')->on('artw_tickets');
            $table->integer('id_from')->unsigned();
            $table->foreign('id_from')->references('id')->on('adm_usuarios');
            $table->integer('id_to')->unsigned();
            $table->foreign('id_to')->references('id')->on('adm_usuarios');
            $table->integer('conta_notificacao')->unsigned()->nullable()->default(null);
            $table->integer('ciclo')->unsigned()->nullable()->default(null);
            $table->string('assunto');
            $table->timestamps();
        });

        Schema::create('artw_notificacoes_envios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_notificacao')->unsigned();
            $table->foreign('id_notificacao')->references('id')->on('artw_notificacoes');
            $table->integer('status')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artw_notificacoes_envios');
        Schema::drop('artw_notificacoes');
        Schema::drop('artw_notificacoes_tipos');
    }
}
