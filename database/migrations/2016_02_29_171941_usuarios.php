<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Usuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_adm_perfil')->unsigned();
            $table->foreign('id_adm_perfil')->references('id')->on('adm_perfis');
            $table->integer('id_adm_empresa')->unsigned();
            $table->foreign('id_adm_empresa')->references('id')->on('adm_empresas');
            $table->string('nome');
            $table->string('sobrenome');
            $table->string('email')->unique();
            $table->string('senha');
            $table->string('fone')->nullable();
            $table->string('cargo')->nullable();
            $table->string('depto')->nullable();
            $table->enum('status', [0,1])->default(1);
            $table->enum('primeiro_acesso', [0,1])->default(1);
            $table->enum('notificacao_favoritos', [0,1])->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adm_usuarios');
    }
}
