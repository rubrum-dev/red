<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArtwVersoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artw_versoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_sku')->unsigned();
            $table->foreign('id_sku')->references('id')->on('skus');
            $table->integer('numero')->default(1);
            $table->string('path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artw_versoes');
    }
}
