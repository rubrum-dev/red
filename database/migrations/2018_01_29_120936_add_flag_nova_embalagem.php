<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFlagNovaEmbalagem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_variacoes', function (Blueprint $table) {
            $table->integer('novo')->unsigned()->nullable()->default(0);
        });
        
        Schema::table('skus', function (Blueprint $table) {
            $table->integer('novo')->unsigned()->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_variacoes', function (Blueprint $table) {
            $table->dropColumn('novo');
        });
        
        Schema::table('skus', function (Blueprint $table) {
            $table->dropColumn('novo');
        });
    }
}
