<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsosIncorretos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usos_incorretos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipo_uso_incorreto')->unsigned();
            $table->foreign('id_tipo_uso_incorreto')->references('id')->on('tipos_usos_incorretos');
            $table->string('thumb')->nullable();
            $table->string('descricao')->nullable();
            $table->enum('status', [0,1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usos_incorretos');
    }
}
