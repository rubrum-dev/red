<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrdemTktStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_tkt_status', function ($table) {
            $table->integer('ordem')->unsigned()->nullable()->default(null);
            $table->integer('exibe')->unsigned()->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_tkt_status', function ($table) {
            $table->dropColumn('ordem');
            $table->dropColumn('exibe');
        });
    }
}
