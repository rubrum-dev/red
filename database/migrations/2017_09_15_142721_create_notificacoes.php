<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificacoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('artw_notificacoes', function (Blueprint $table) {
             $table->increments('id');
             $table->integer('id_tipo')->unsigned();
             $table->foreign('id_tipo')->references('id')->on('artw_notificacao_tipos');
             $table->integer('id_ticket')->unsigned();
             $table->foreign('id_ticket')->references('id')->on('artw_tickets');
             $table->timestamps();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::drop('artw_notificacoes');
     }

}
