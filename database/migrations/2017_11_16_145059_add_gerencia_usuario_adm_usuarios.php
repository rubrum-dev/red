<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGerenciaUsuarioAdmUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adm_usuarios', function (Blueprint $table) {
            $table->integer('gerencia_usuario')->unsigned()->nullable()->default(null);
            $table->integer('id_departamento')->unsigned()->nullable()->default(null);
            $table->foreign('id_departamento')->references('id')->on('adm_departamentos');
            $table->integer('id_cargo')->unsigned()->nullable()->default(null);
            $table->foreign('id_cargo')->references('id')->on('adm_cargos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adm_usuarios', function (Blueprint $table) {
            $table->dropForeign('adm_usuarios_id_cargo_foreign');
            $table->dropForeign('adm_usuarios_id_departamento_foreign');
            $table->dropColumn('gerencia_usuario');
            $table->dropColumn('id_departamento');
            $table->dropColumn('id_cargo');
        });
    }
}
