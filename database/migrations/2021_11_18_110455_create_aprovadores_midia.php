<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAprovadoresMidia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adsmart_aprovadores_midia', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome')->nullable();
            $table->timestamps();
        });

        Schema::create('adsmart_aprovadores_midia_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_aprovador_midia')->unsigned();
            $table->foreign('id_aprovador_midia')->references('id')->on('adsmart_aprovadores_midia')->onDelete('cascade');
            $table->integer('id_usuario')->unsigned();
            $table->foreign('id_usuario')->references('id')->on('adm_usuarios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
