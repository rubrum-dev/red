<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTdVariacoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('td_variacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_techdraw')->unsigned();
            $table->foreign('id_techdraw')->references('id')->on('td_techdraws');
            $table->string('nome')->nullable();
            $table->integer('principal')->default(0);
            $table->enum('status', [0,1,2])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('td_variacoes');
    }
}
