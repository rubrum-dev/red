<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArtwTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artw_tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_sku')->unsigned();
            $table->foreign('id_sku')->references('id')->on('skus');
            $table->integer('id_status')->unsigned();
            $table->foreign('id_status')->references('id')->on('artw_tkt_status');
            $table->integer('numero');
            $table->string('descricao');
            $table->dateTime('dt_ini')->nullable();
            $table->dateTime('dt_fim')->nullable();
            $table->dateTime('dt_envio')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artw_tickets');
    }
}
