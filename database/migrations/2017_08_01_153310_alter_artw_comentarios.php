<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterArtwComentarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_comentarios', function (Blueprint $table) {
            $table->string('cor')->default(null)->after('texto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_comentarios', function (Blueprint $table) {
            $table->dropColumn('cor');
        });
    }
}
