<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSessionIdNewsUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news_usuarios', function (Blueprint $table) {
            
            $table->string('session_id')->default(null)->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news_usuarios', function (Blueprint $table) {
            
            $table->dropColumn('session_id');
            
        });
    }
}
