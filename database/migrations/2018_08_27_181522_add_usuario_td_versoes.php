<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsuarioTdVersoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('td_versoes', function (Blueprint $table) {
            $table->integer('id_usuario')->unsigned();
            $table->foreign('id_usuario')->references('id')->on('adm_usuarios');
            $table->string('nome')->default(null)->after('id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('td_versoes', function (Blueprint $table) {
            $table->dropForeign('td_versoes_id_usuario_foreign');
            $table->dropColumn('id_usuario');
            $table->dropColumn('nome');
        });
    }
}
