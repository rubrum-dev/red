<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArteEnviadaAutorizadaArtwTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_tickets', function (Blueprint $table) {
            $table->integer('envio_autorizado')->unsigned()->nullable()->default(null);
            $table->integer('envio_finalizado')->unsigned()->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_tickets', function (Blueprint $table) {
            $table->dropColumn('envio_autorizado');
            $table->dropColumn('envio_finalizado');
        });
    }
}
