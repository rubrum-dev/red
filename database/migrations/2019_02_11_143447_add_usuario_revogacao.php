<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsuarioRevogacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_perfis_usuarios_tickets', function (Blueprint $table) {
            $table->integer('id_renovacao')->unsigned()->default(null)->nullable();
            $table->foreign('id_renovacao')->references('id')->on('adm_usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_perfis_usuarios_tickets', function (Blueprint $table) {
            $table->dropForeign('artw_perfis_usuarios_tickets_id_renovacao_foreign');
            $table->dropColumn('id_renovacao');
        });
    }
}
