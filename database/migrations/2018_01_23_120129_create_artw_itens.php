<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtwItens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artw_item_tipos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome')->nullable();
            $table->timestamps();
        });
        
        Schema::create('artw_itens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipo')->unsigned()->nullable();
            $table->foreign('id_tipo')->references('id')->on('artw_item_tipos');
            $table->integer('id_variacao')->unsigned();
            $table->foreign('id_variacao')->references('id')->on('artw_variacoes');
            $table->string('nome')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artw_itens');
        Schema::drop('artw_item_tipos');
    }
}
