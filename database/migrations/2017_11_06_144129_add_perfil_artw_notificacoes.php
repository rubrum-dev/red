<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPerfilArtwNotificacoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_notificacoes', function (Blueprint $table) {
            $table->integer('id_perfil')->unsigned()->nullable()->default(null);
            $table->foreign('id_perfil')->references('id')->on('artw_perfis_usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_notificacoes', function (Blueprint $table) {
            $table->dropColumn('id_perfil');
        });
    }
}
