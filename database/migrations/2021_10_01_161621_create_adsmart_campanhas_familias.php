<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsmartCampanhasFamilias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adsmart_campanhas_familias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_campanha')->unsigned();
            $table->foreign('id_campanha')->references('id')->on('adsmart_campanhas');
            $table->integer('id_familia')->unsigned();
            $table->foreign('id_familia')->references('id')->on('familias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adsmart_campanhas_familias');
    }
}
