<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Logotipos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logotipos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipo_logotipo')->unsigned();
            $table->foreign('id_tipo_logotipo')->references('id')->on('tipos_logotipos');
            $table->string('thumb');
            $table->enum('status', [0,1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('logotipos');
    }
}
