<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMensagemArtwArtesEnvios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_artes_envios', function (Blueprint $table) {
            
            $table->text('mensagem')->default(null)->nullable()->after('emails');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_artes_envios', function (Blueprint $table) {
            
            $table->dropColumn('mensagem');
            
        });
    }
}
