<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposAdsmartCondicoesMidia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adsmart_condicoes_midia', function (Blueprint $table) {
            $table->integer('status')->unsigned()->nullable()->default(0)->after('obrigatoria');
            $table->integer('bloquear_auto_aprovacao')->unsigned()->nullable()->default(0)->after('obrigatoria');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adsmart_condicoes_midia', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('bloquear_auto_aprovacao');
        });
    }
}
