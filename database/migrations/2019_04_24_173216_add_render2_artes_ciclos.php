<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRender2ArtesCiclos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_versoes', function ($table) {
            $table->string('error')->default(null)->nullable();
        });
        
        Schema::table('artw_ciclos_layout', function ($table) {
            $table->string('error')->default(null)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_versoes', function ($table) {
            $table->dropColumn('error');
        });
        
        Schema::table('artw_ciclos_layout', function ($table) {
            $table->dropColumn('error');
        });
    }
}
