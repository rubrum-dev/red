<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEanaplicavelVariacoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_variacoes', function ($table) {
            $table->integer('tem_codigo_ean')->unsigned()->nullable()->after('codigo_ean');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_variacoes', function ($table) {
            $table->dropColumn('tem_codigo_ean');
        });
    }
}
