<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodigoCliente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artw_projetos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome')->default(null)->nullable();
            $table->timestamps();
        });


        Schema::table('artw_tickets', function (Blueprint $table) {
            $table->string('codigo_cliente')->default(null)->nullable()->after('numero');
            $table->integer('id_projeto')->unsigned()->default(null)->nullable()->after('codigo_cliente');
            $table->foreign('id_projeto')->references('id')->on('artw_projetos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_tickets', function (Blueprint $table) {
            $table->dropColumn('codigo_cliente');
        });
    }
}
