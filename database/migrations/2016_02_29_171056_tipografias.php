<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tipografias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipografias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipo_tipografia')->unsigned();
            $table->foreign('id_tipo_tipografia')->references('id')->on('tipos_tipografias');
            $table->string('nome')->nullable();
            $table->string('thumb')->nullable();
            $table->string('descricao')->nullable();
            $table->string('path_down')->nullable();
            $table->string('link')->nullable();
            $table->enum('status', [0,1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tipografias');
    }
}
