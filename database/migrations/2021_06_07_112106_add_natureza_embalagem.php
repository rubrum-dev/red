<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNaturezaEmbalagem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_variacoes', function ($table) {
            $table->integer('id_natureza_embalagem')->unsigned()->nullable()->after('id_sku');
            $table->foreign('id_natureza_embalagem')->references('id')->on('natureza_embalagens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_variacoes', function ($table) {
            $table->dropColumn('id_natureza_embalagem');
        });
    }
}
