<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNomeArquivoArtwArtes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_artes', function (Blueprint $table) {
            $table->string('nome_arquivo')->default(null)->after('nome')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_artes', function (Blueprint $table) {
            $table->dropColumn('nome_arquivo');
        });
    }
}
