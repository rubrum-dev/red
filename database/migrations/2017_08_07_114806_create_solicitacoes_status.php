<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitacoesStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artw_solicitacoes_status', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_solicitacao')->unsigned();
            $table->foreign('id_solicitacao')->references('id')->on('artw_solicitacoes');
            $table->integer('id_ciclo')->unsigned();
            $table->foreign('id_ciclo')->references('id')->on('artw_ciclos');
            $table->integer('id_usuario')->unsigned();
            $table->foreign('id_usuario')->references('id')->on('adm_usuarios');
            $table->integer('id_status')->unsigned();
            $table->foreign('id_status')->references('id')->on('artw_solicitacao_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artw_solicitacoes_status');
    }
}
