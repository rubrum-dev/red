<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposS3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_artes', function (Blueprint $table) {
            $table->integer('size')->unsigned()->default(null)->nullable()->after('nome_arquivo');
            $table->integer('storage')->unsigned()->default(0)->nullable()->after('size');
        });

        Schema::table('artw_ciclos_layout', function (Blueprint $table) {
            $table->string('nome_arquivo')->default(null)->nullable()->after('path');
            $table->integer('size')->unsigned()->default(null)->nullable()->after('nome_arquivo');
            $table->integer('storage')->unsigned()->default(0)->nullable()->after('size');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
