<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArtwTicketCancelamento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_tickets', function (Blueprint $table) {
            $table->datetime('cancelado_em')->nullable()->default(null);
            $table->integer('cancelado_por')->unsigned()->nullable()->default(null);
            $table->foreign('cancelado_por')->references('id')->on('adm_usuarios');
            $table->string('cancelado_motivo')->default(null)->nullable();
            
            $table->datetime('pausado_em')->nullable()->default(null);
            $table->integer('pausado_por')->unsigned()->nullable()->default(null);
            $table->foreign('pausado_por')->references('id')->on('adm_usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_tickets', function (Blueprint $table) {
            
            $table->dropForeign('artw_tickets_cancelado_por_foreign');
            $table->dropColumn('cancelado_em');
            $table->dropColumn('cancelado_por');
            $table->dropColumn('cancelado_motivo');
            
            $table->dropForeign('artw_tickets_pausado_por_foreign');
            $table->dropColumn('pausado_em');
            $table->dropColumn('pausado_por');
            
        });
        
    }
}
