<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsosAplicacoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usos_aplicacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipo_uso_aplicacao')->unsigned();
            $table->foreign('id_tipo_uso_aplicacao')->references('id')->on('tipos_usos_aplicacoes');
            $table->string('thumb');
            $table->string('descricao');
            $table->enum('status', [0,1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usos_aplicacoes');
    }
}
