<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTdItens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('td_itens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipo')->unsigned()->nullable();
            $table->foreign('id_tipo')->references('id')->on('artw_item_tipos');
            $table->integer('id_variacao')->unsigned();
            $table->foreign('id_variacao')->references('id')->on('td_variacoes');
            $table->enum('status', [0,1,2])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('td_itens');
    }
}
