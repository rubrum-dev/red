<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViewArtesCiclos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_versoes', function ($table) {
            $table->longtext('view')->nullable();
        });
        
        Schema::table('artw_ciclos_layout', function ($table) {
            $table->longtext('view')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_versoes', function ($table) {
            $table->dropColumn('view');
        });
        
        Schema::table('artw_ciclos_layout', function ($table) {
            $table->dropColumn('view');
        });
    }
}
