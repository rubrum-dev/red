<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRenderArtesCiclos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_versoes', function ($table) {
            $table->integer('indexado')->unsigned()->nullable()->default(0);
            $table->integer('renderizado')->unsigned()->nullable()->default(0);
            $table->integer('status_renderizacao')->unsigned()->nullable()->default(0);
        });
        
        Schema::table('artw_ciclos_layout', function ($table) {
            $table->integer('indexado')->unsigned()->nullable()->default(0);
            $table->integer('renderizado')->unsigned()->nullable()->default(0);
            $table->integer('status_renderizacao')->unsigned()->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_versoes', function ($table) {
            $table->dropColumn('indexado');
            $table->dropColumn('renderizado');
            $table->dropColumn('status_renderizacao');
        });
        
        Schema::table('artw_ciclos_layout', function ($table) {
            $table->dropColumn('indexado');
            $table->dropColumn('renderizado');
            $table->dropColumn('status_renderizacao');
        });
    }
}
