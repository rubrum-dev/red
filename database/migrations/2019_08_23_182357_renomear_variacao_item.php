<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenomearVariacaoItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_itens', function(Blueprint $table) {
            $table->renameColumn('id_variacao', 'id_variacao_parent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_itens', function(Blueprint $table) {
            $table->renameColumn('id_variacao_parent', 'id_variacao');
        });
    }
}
