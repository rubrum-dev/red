<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCanceladoEmArtwArtesEnvios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_artes_envios', function (Blueprint $table) {
            $table->dateTime('cancelado_em')->default(null)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_artes_envios', function (Blueprint $table) {
            $table->dropColumn('cancelado_em');
        });
    }
}
