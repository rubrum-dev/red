<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCicloArtwTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_tickets', function ($table) {
            $table->integer('ciclo')->unsigned()->nullable()->default(null)->after('numero');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_tickets', function ($table) {
            $table->dropColumn('ciclo');
        });
    }
}
