<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCorMarcasProdutos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('familias', function (Blueprint $table) {
            
            $table->string('cor_hexa')->default(null)->nullable()->after('nome');
            
        });
        
        Schema::table('produtos', function (Blueprint $table) {
            
            $table->string('cor_hexa')->default(null)->nullable()->after('nome');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('familias', function (Blueprint $table) {
            
            $table->dropColumn('cor_hexa');
            
        });
        
        Schema::table('produtos', function (Blueprint $table) {
            
            $table->dropColumn('cor_hexa');
            
        });
    }
}
