<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModuloArtwPerfisUsuariosTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_perfis_usuarios', function ($table) {
            $table->integer('modulo')->unsigned()->nullable()->default(1)->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_perfis_usuarios', function ($table) {
            $table->dropColumn('modulo');
        });
    }
}
