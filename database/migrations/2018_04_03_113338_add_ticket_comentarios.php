<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTicketComentarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_comentarios', function (Blueprint $table) {
            $table->integer('id_solicitacao')->unsigned()->nullable()->default(null)->change();
            $table->integer('id_ticket')->unsigned()->nullable()->default(null)->after('id');
            $table->foreign('id_ticket')->references('id')->on('artw_tickets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_comentarios', function (Blueprint $table) {
            $table->dropForeign('artw_comentarios_id_ticket_foreign');
            $table->dropColumn('id_ticket');
            //$table->integer('id_solicitacao')->nullable(false)->change();
        });
    }
}
