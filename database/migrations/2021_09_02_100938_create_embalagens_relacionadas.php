<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmbalagensRelacionadas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artw_variacoes_relacionadas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_variacao')->unsigned();
            $table->foreign('id_variacao')->references('id')->on('artw_variacoes');
            $table->integer('id_variacao_relacionada')->unsigned();
            $table->foreign('id_variacao_relacionada')->references('id')->on('artw_variacoes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artw_variacoes_relacionadas');
    }
}
