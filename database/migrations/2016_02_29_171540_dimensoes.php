<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Dimensoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dimensoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipo_embalagem')->unsigned();
            $table->foreign('id_tipo_embalagem')->references('id')->on('tipos_embalagens');
            $table->string('nome');
            $table->string('thumb')->nullable();
            $table->enum('status', [0,1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dimensoes');
    }
}
