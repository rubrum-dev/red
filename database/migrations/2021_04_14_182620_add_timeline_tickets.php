<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimelineTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_tickets', function (Blueprint $table) {
            
            $table->text('timeline_etapas')->default(null)->nullable()->after('descricao');
            $table->text('timeline_subetapas')->default(null)->nullable()->after('timeline_etapas');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_tickets', function (Blueprint $table) {
            
            $table->dropColumn('timeline_etapas');
            $table->dropColumn('timeline_subetapas');
            
        });
    }
}
