<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdmLogNovidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('adm_log_novidades', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('id_modulo')->unsigned();
          $table->foreign('id_modulo')->references('id')->on('adm_modulos');
          $table->integer('id_produto')->unsigned();
          $table->foreign('id_produto')->references('id')->on('produtos');
          $table->string('descricao');
          $table->enum('operacao', ['CREATE', 'UPDATE', 'DELETE']);
          $table->string('guide')->nullable();
          $table->integer('sku')->nullable();
          $table->enum('status', [0,1])->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adm_log_novidades');
    }
}
