<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdmFtps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_ftps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_adm_usuario')->unsigned();
            $table->foreign('id_adm_usuario')->references('id')->on('adm_usuarios');
            $table->string('nome')->nullable();
            $table->string('path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adm_ftps');
    }
}
