<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtwArtesEnvios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artw_artes_envios', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('id_ticket')->unsigned();
            $table->foreign('id_ticket')->references('id')->on('artw_tickets');
            
            $table->integer('id_usuario')->unsigned();
            $table->foreign('id_usuario')->references('id')->on('adm_usuarios');
            
            $table->integer('id_arte')->unsigned();
            $table->foreign('id_arte')->references('id')->on('artw_artes');
            
            $table->integer('id_fornecedor')->unsigned()->nullable()->default(null);
            
            $table->text('emails')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artw_artes_envios');
    }
}
