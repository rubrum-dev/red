<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelatorios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rel_tipos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('slug');
            $table->integer('status')->unsigned();
            $table->timestamps();
        });

        Schema::create('rel_meses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipo')->unsigned();
            $table->foreign('id_tipo')->references('id')->on('rel_tipos');
            $table->integer('id_usuario')->unsigned()->nullable();
            $table->foreign('id_usuario')->references('id')->on('adm_usuarios');
            $table->string('mes');
            $table->string('ano');
            $table->integer('status')->unsigned();
            $table->timestamps();
        });

        Schema::create('rel_meses_numeros', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->integer('id_mes')->unsigned();
            $table->foreign('id_mes')->references('id')->on('rel_meses');
            $table->integer('numero')->unsigned();
            $table->double('porcentagem', 5, 2)->unsigned();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
