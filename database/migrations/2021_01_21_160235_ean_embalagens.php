<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EanEmbalagens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_variacoes', function ($table) {
            
            $table->string('codigo_ean')->nullable()->after('nome_original');
            $table->datetime('codigo_ean_alteracao')->nullable()->after('nome_original');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_variacoes', function (Blueprint $table) {
            
            $table->dropColumn('codigo_ean');
            $table->dropColumn('codigo_ean_alteracao');
            
        });
    }
}
