<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtwFornecedorPdfs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artw_fornecedor_pdfs', function(Blueprint $table)
        {
            $table->increments('id');
            
            $table->integer('id_ticket')->unsigned()->nullable();
            $table->foreign('id_ticket')->references('id')
                  ->on('artw_tickets');
            
            $table->integer('id_fornecedor')->unsigned()->nullable();
            $table->foreign('id_fornecedor')->references('id')
                  ->on('fornecedores');
            
            $table->integer('id_usuario')->unsigned()->nullable();
            $table->foreign('id_usuario')->references('id')
                  ->on('adm_usuarios');
            
            $table->string('nome')->nullable();
            $table->string('nome_arquivo')->nullable();
            $table->string('path')->nullable();
            
            $table->datetime('dt_aprovacao')->nullable()->default(null);
            
            $table->integer('id_aprovacao')->unsigned()->nullable()->default(null);
            $table->foreign('id_aprovacao')->references('id')->on('adm_usuarios');
            
            $table->integer('status')->unsigned()->nullable()->default(0);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artw_fornecedor_pdfs');
    }
}
