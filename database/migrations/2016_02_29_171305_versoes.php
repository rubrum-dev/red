<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Versoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('versoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_categoria_versao')->unsigned();
            $table->foreign('id_categoria_versao')->references('id')->on('categorias_versoes');
            $table->integer('id_tipo_proporcao_versao')->unsigned();
            $table->foreign('id_tipo_proporcao_versao')->references('id')->on('tipos_proporcoes_versoes');
            $table->string('descricao');
            $table->string('thumb');
            $table->string('path_down');
            $table->enum('status', [0,1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('versoes');
    }
}
