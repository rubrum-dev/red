<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Skus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_produto')->unsigned();
            $table->foreign('id_produto')->references('id')->on('produtos');
            $table->integer('id_dimensao')->unsigned();
            $table->foreign('id_dimensao')->references('id')->on('dimensoes');
            $table->string('nome');
            $table->enum('promocional', [0,1])->default(0);
            $table->dateTime('dt_expiracao')->nullable();
            $table->string('thumb')->nullable();
            $table->integer('ordem')->nullable();
            $table->enum('status', [0,1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('skus');
    }
}
