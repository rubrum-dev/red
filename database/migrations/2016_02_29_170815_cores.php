<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipo_cor')->unsigned();
            $table->foreign('id_tipo_cor')->references('id')->on('tipos_cores');
            $table->integer('id_nivel_cor')->unsigned();
            $table->foreign('id_nivel_cor')->references('id')->on('niveis_cores');
            $table->string('nome');
            $table->string('spot');
            $table->string('cmyk');
            $table->string('rgb');
            $table->string('hex');
            $table->string('descricao')->nullable();
            $table->enum('status', [0,1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cores');
    }
}
