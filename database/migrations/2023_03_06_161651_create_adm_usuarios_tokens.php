<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmUsuariosTokens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adm_usuarios', function (Blueprint $table) {
            $table->integer('tem_token')->unsigned()->default(0);
        });

        Schema::create('adm_usuarios_tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_usuario')->unsigned()->nullable();
            $table->foreign('id_usuario')->references('id')->on('adm_usuarios');
            $table->string('token');
            $table->string('tipo');
            $table->dateTime('expira_em')->default(null)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adm_usuarios', function (Blueprint $table) {
            $table->dropColumn('tem_token');
        });

        Schema::drop('adm_usuarios_tokens');
    }
}
