<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTicketArtwVersoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_versoes', function (Blueprint $table) {
            $table->integer('id_ticket')->unsigned()->nullable()->default(null);
            $table->foreign('id_ticket')->references('id')->on('artw_tickets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_versoes', function (Blueprint $table) {
            $table->dropColumn('id_ticket');
        });
    }
}
