<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVariacoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artw_variacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipo')->unsigned();
            $table->foreign('id_tipo')->references('id')->on('artw_variacao_tipos');
            $table->integer('id_campanha')->unsigned()->nullable()->default(null);
            $table->foreign('id_campanha')->references('id')->on('campanhas');
            $table->integer('id_sku')->unsigned();
            $table->foreign('id_sku')->references('id')->on('skus');
            $table->string('nome')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artw_variacoes');
    }
}
