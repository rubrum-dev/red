<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterArtwVersoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_versoes', function (Blueprint $table) {
            $table->string('nome')->default(null)->after('id_sku');
            $table->integer('id_usuario')->unsigned()->nullable()->default(null);
            $table->foreign('id_usuario')->references('id')->on('adm_usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_versoes', function (Blueprint $table) {
            $table->dropColumn('nome');
            $table->dropColumn('id_usuario');
        });
    }
}
