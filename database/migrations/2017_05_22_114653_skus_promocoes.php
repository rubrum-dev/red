<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SkusPromocoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skus_promocoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_sku')->unsigned();
            $table->foreign('id_sku')->references('id')->on('skus');
            $table->integer('id_promocao')->unsigned();
            $table->foreign('id_promocao')->references('id')->on('promocoes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('skus_promocoes');
    }
}
