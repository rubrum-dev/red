<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Icones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('icones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_categoria_icone')->unsigned();
            $table->foreign('id_categoria_icone')->references('id')->on('categorias_icones');
            $table->string('descricao')->nullable();
            $table->string('thumb')->nullable();
            $table->string('path_down')->nullable();
            $table->enum('status', [0,1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('icones');
    }
}
