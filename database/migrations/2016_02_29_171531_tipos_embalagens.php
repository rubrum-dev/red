<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TiposEmbalagens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_embalagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_natureza_embalagem')->unsigned();
            $table->foreign('id_natureza_embalagem')->references('id')->on('natureza_embalagens');
            $table->string('nome');
            $table->integer('ordem')->nullable();
            $table->enum('status', [0,1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tipos_embalagens');
    }
}
