<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_news')->unsigned();
            $table->foreign('id_news')->references('id')->on('news');
            $table->integer('id_usuario')->unsigned();
            $table->foreign('id_usuario')->references('id')->on('adm_usuarios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news_usuarios');
    }
}
