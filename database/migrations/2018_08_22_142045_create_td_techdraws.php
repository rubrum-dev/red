<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTdTechdraws extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('td_techdraws', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_dimensao')->unsigned();
            $table->foreign('id_dimensao')->references('id')->on('dimensoes')->unique();
            $table->string('nome')->nullable();
            $table->enum('status', [0, 1, 2])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('td_techdraws');
    }
}
