<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateAdsmartCampanhaMidias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adsmart_campanha_midias', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('id_campanha')->unsigned()->nullable();
            $table->foreign('id_campanha')->references('id')
                  ->on('adsmart_campanhas')->onDelete('cascade');
            
            $table->integer('id_midia_tipo')->unsigned()->nullable();
            $table->foreign('id_midia_tipo')->references('id')
                  ->on('adsmart_midia_tipos')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adsmart_campanha_midias');
    }
}
