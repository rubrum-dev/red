<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterArtwArquivos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_arquivos', function (Blueprint $table) {
            $table->integer('id_usuario')->unsigned()->nullable()->default(null);
            $table->foreign('id_usuario')->references('id')->on('adm_usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_arquivos', function ($table) {
            $table->dropColumn('id_usuario');
        });
    }
}
