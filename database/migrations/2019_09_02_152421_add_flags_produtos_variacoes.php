<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFlagsProdutosVariacoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('produtos', function (Blueprint $table) {
            
            $table->dropColumn('techdraw');
            DB::statement('ALTER TABLE produtos MODIFY COLUMN artwork int(10) UNSIGNED NULL DEFAULT 1');
            DB::statement('ALTER TABLE artw_variacoes MODIFY COLUMN status int(10) UNSIGNED NULL DEFAULT 1');
            
            $table->integer('packshelf')->unsigned()->nullable()->default(1);
            
        });
        
        Schema::table('artw_variacoes', function (Blueprint $table) {
            
            //$table->integer('status')->default(1)->change();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
