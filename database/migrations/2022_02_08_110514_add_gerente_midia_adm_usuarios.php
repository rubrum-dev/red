<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGerenteMidiaAdmUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adm_usuarios', function ($table) {
            $table->integer('gerencia_midia')->unsigned()->nullable()->default(null)->after('gerencia_usuario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adm_usuarios', function ($table) {
            $table->dropColumn('gerencia_midia');
        });
    }
}
