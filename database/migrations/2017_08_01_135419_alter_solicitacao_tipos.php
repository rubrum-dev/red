<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSolicitacaoTipos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_solicitacao_tipos', function (Blueprint $table) {
            $table->string('icone')->default(null)->after('nome');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_solicitacao_tipos', function (Blueprint $table) {
            $table->dropColumn('icone');
        });
    }
}
