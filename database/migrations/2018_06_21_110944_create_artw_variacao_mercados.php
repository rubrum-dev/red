<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtwVariacaoMercados extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artw_variacao_mercados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_mercado')->unsigned()->nullable();
            $table->foreign('id_mercado')->references('id')->on('artw_mercados');
            $table->integer('id_variacao')->unsigned();
            $table->foreign('id_variacao')->references('id')->on('artw_variacoes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artw_variacao_mercados');
    }
}
