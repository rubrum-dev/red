<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArtwPerfisUsuariosTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artw_perfis_usuarios_tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_ticket')->unsigned();
            $table->foreign('id_ticket')->references('id')->on('artw_tickets');
            $table->integer('id_usuario')->unsigned();
            $table->foreign('id_usuario')->references('id')->on('adm_usuarios');
            $table->integer('id_perfil')->unsigned();
            $table->foreign('id_perfil')->references('id')->on('artw_perfis_usuarios');
            $table->enum('recebe_email', [0,1])->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artw_perfis_usuarios_tickets');
    }
}
