<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataAprovacaoArtwTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_tickets', function (Blueprint $table) {
            $table->datetime('dt_envio_autorizado')->nullable()->default(null);
            $table->datetime('dt_envio_finalizado')->nullable()->default(null);

            $table->integer('id_envio_autorizado')->unsigned()->nullable()->default(null);
            $table->foreign('id_envio_autorizado')->references('id')->on('adm_usuarios');

            $table->integer('id_envio_finalizado')->unsigned()->nullable()->default(null);
            $table->foreign('id_envio_finalizado')->references('id')->on('adm_usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_tickets', function (Blueprint $table) {
            $table->dropColumn('dt_envio_autorizado');
            $table->dropColumn('dt_envio_finalizado');
            $table->dropColumn('id_envio_autorizado');
            $table->dropColumn('id_envio_finalizado');
        });
    }
}
