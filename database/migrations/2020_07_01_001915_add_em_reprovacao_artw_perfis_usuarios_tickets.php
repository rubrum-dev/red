<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmReprovacaoArtwPerfisUsuariosTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_perfis_usuarios_tickets', function ($table) {
            $table->integer('em_reprovacao')->unsigned()->nullable()->default(0)->after('aprovado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_perfis_usuarios_tickets', function ($table) {
            $table->dropColumn('em_reprovacao');
        });
    }
}
