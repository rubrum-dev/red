<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Produtos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_familia')->unsigned();
            $table->foreign('id_familia')->references('id')->on('familias');
            $table->integer('id_categoria_produto')->unsigned();
            $table->foreign('id_categoria_produto')->references('id')->on('categorias_produtos');
            $table->string('nome');
            $table->integer('ordem')->nullable();
            $table->enum('status', [0,1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('produtos');
    }
}
