<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGerentesAdmUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adm_usuarios', function ($table) {
            $table->integer('gerencia_fornecedor')->unsigned()->nullable()->default(null)->after('gerencia_midia');
            $table->integer('gerencia_embalagem')->unsigned()->nullable()->default(null)->after('gerencia_fornecedor');
            $table->integer('gerencia_foto')->unsigned()->nullable()->default(null)->after('gerencia_embalagem');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adm_usuarios', function ($table) {
            $table->dropColumn('gerencia_fornecedor');
            $table->dropColumn('gerencia_embalagem');
            $table->dropColumn('gerencia_foto');
        });
    }
}
