<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArtwArtesEnviosEmails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artw_artes_envios_emails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_arte_envio')->unsigned()->nullable();
            $table->foreign('id_arte_envio')->references('id')->on('artw_artes_envios');
            $table->string('email')->nullable();
            $table->string('token')->nullable();
            $table->dateTime('dt_expiracao')->nullable();
            $table->integer('downloads_arte')->default(0);
            $table->integer('downloads_pdf')->default(0);
            $table->integer('envios')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artw_artes_envios_emails');
    }
}
