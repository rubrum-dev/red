<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusAdsmartPecas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adsmart_pecas', function (Blueprint $table) {
            $table->integer('status')->unsigned()->nullable()->default(1)->after('id_campanha_midia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adsmart_pecas', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
