<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PerfisFamilias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_perfis_familias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_adm_perfil')->unsigned();
            $table->foreign('id_adm_perfil')->references('id')->on('adm_perfis');
            $table->integer('id_familia')->unsigned();
            $table->foreign('id_familia')->references('id')->on('familias');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adm_perfis_familias');
    }
}
