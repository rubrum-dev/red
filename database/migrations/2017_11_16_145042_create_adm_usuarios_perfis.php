<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmUsuariosPerfis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_usuarios_perfis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_usuario')->unsigned();
            $table->foreign('id_usuario')->references('id')->on('adm_usuarios');
            $table->integer('id_perfil')->unsigned();
            $table->foreign('id_perfil')->references('id')->on('adm_perfis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adm_usuarios_perfis', function (Blueprint $table) {
            $table->dropForeign('adm_usuarios_perfis_id_perfil_foreign');
            $table->dropForeign('adm_usuarios_perfis_id_usuario_foreign');
        });
        
        Schema::drop('adm_usuarios_perfis');
    }
}
