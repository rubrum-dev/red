<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrigemArtwNotificacoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_notificacoes', function (Blueprint $table) {
            $table->string('publico')->default(null)->after('assunto')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_notificacoes', function (Blueprint $table) {
            $table->dropColumn('publico');
        });
    }
}
