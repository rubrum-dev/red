<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFornecedoresEmails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fornecedores_emails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_fornecedor')->unsigned();
            $table->foreign('id_fornecedor')->references('id')->on('fornecedores');
            $table->string('nome')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fornecedores_emails');
    }
}
