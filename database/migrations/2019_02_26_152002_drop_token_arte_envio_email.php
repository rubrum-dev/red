<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTokenArteEnvioEmail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_artes_envios_emails', function ($table) {
            $table->dropColumn('dt_expiracao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_artes_envios_emails', function ($table) {
            $table->dateTime('dt_expiracao')->nullable();
        });
    }
}
