<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTdVersoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('td_versoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_sku')->unsigned();
            $table->foreign('id_sku')->references('id')->on('skus');
            $table->integer('id_usuario')->unsigned()->nullable()->default(null);
            $table->foreign('id_usuario')->references('id')->on('adm_usuarios');
            $table->integer('id_ticket')->unsigned()->nullable()->default(null);
            $table->foreign('id_ticket')->references('id')->on('artw_tickets');
            $table->integer('id_variacao')->unsigned()->nullable()->default(null);
            $table->foreign('id_variacao')->references('id')->on('artw_variacoes');
            $table->string('nome')->default(null);
            $table->integer('numero')->default(1);
            $table->string('path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('td_versoes');
    }
}
