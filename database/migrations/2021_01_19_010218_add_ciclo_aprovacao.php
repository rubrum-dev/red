<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCicloAprovacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_perfis_usuarios_tickets', function ($table) {
            
            $table->integer('ciclo_aprovacao')->unsigned()->nullable()->after('ciclo');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_perfis_usuarios_tickets', function ($table) {
            
            $table->dropColumn('ciclo_aprovacao');
            
        });
    }
}
