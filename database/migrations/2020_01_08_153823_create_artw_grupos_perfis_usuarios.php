<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtwGruposPerfisUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artw_grupos_perfis_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_grupo_perfil')->unsigned();
            $table->foreign('id_grupo_perfil')->references('id')->on('artw_grupos_perfis')->onDelete('cascade');
            $table->integer('id_usuario')->unsigned();
            $table->foreign('id_usuario')->references('id')->on('adm_usuarios');
            $table->integer('id_perfil')->unsigned();
            $table->foreign('id_perfil')->references('id')->on('artw_perfis_usuarios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artw_grupos_perfis_usuarios');
    }
}
