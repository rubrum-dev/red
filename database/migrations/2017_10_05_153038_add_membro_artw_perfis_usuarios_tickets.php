<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMembroArtwPerfisUsuariosTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_perfis_usuarios_tickets', function (Blueprint $table) {
            $table->integer('id_membro_aprovador')->unsigned()->nullable()->default(null);
            $table->foreign('id_membro_aprovador')->references('id')->on('artw_perfis_usuarios_equipe_tickets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_perfis_usuarios_tickets', function (Blueprint $table) {
            $table->dropColumn('id_membro_aprovador');
        });
    }
}
