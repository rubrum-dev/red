<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDtExpiracaoArtwVariacoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_variacoes', function (Blueprint $table) {
            $table->datetime('dt_expiracao')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_variacoes', function (Blueprint $table) {
            $table->dropColumn('dt_expiracao');
        });
    }
}
