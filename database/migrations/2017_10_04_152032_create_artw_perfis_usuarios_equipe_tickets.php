<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtwPerfisUsuariosEquipeTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artw_perfis_usuarios_equipe_tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_parent')->unsigned();
            $table->foreign('id_parent')->references('id')->on('artw_perfis_usuarios_tickets');
            $table->integer('id_usuario')->unsigned();
            $table->foreign('id_usuario')->references('id')->on('adm_usuarios');
            $table->enum('recebe_email', [0,1])->default(0);
            $table->integer('aprovado')->default(0);
            $table->integer('ciclo')->unsigned()->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artw_perfis_usuarios_equipe_tickets');
    }
}
