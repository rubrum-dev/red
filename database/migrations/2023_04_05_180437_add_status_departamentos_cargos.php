<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusDepartamentosCargos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adm_departamentos', function (Blueprint $table) {
            $table->integer('status')->unsigned()->default(1)->after('nome');
        });

        Schema::table('adm_cargos', function (Blueprint $table) {
            $table->integer('status')->unsigned()->default(1)->after('nome');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adm_departamentos', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('adm_cargos', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
