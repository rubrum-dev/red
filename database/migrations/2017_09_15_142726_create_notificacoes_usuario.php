<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificacoesUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('artw_notificacoes_usuario', function (Blueprint $table) {
             $table->increments('id');
             $table->integer('id_notificacao')->unsigned();
             $table->foreign('id_notificacao')->references('id')->on('artw_notificacoes');
             $table->integer('id_usuario')->unsigned();
             $table->foreign('id_usuario')->references('id')->on('adm_usuarios');
             $table->integer('status')->unsigned();
             $table->timestamps();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::drop('artw_notificacoes_usuario');
     }
}
