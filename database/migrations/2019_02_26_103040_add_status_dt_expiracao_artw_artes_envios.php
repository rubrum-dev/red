<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusDtExpiracaoArtwArtesEnvios extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artw_artes_envios', function ($table) {
            $table->dateTime('dt_expiracao')->nullable();
            $table->integer('status')->unsigned()->nullable()->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artw_artes_envios', function ($table) {
            $table->dropColumn('dt_expiracao');
            $table->dropColumn('status');
        });
    }
}
