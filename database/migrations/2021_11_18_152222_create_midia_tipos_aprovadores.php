<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMidiaTiposAprovadores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adsmart_midia_tipos_aprovadores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_midia_tipo')->unsigned();
            $table->foreign('id_midia_tipo')->references('id')->on('adsmart_midia_tipos')->onDelete('cascade');
            $table->integer('id_aprovador_midia')->unsigned();
            $table->foreign('id_aprovador_midia')->references('id')->on('adsmart_aprovadores_midia');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
