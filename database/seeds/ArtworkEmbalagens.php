<?php

use Illuminate\Database\Seeder;

class ArtworkEmbalagens extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('artw_variacoes')->truncate();
        DB::table('artw_variacao_tipos')->truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::beginTransaction();

        $arrayInsert = [
            ['id' => 1, 'nome' => 'Regular'],
            ['id' => 2, 'nome' => 'Promocional'],
        ];
        DB::table('artw_variacao_tipos')->insert($arrayInsert);

        $skus = DB::table('skus')
            ->select('skus.*')
            ->join('produtos', 'produtos.id', '=', 'skus.id_produto')
            ->where('produtos.artwork', 1)
            ->get();

        foreach ($skus as $k=>$sku) {
            $arrayInsert = [
                ['nome' => 'Original', 'principal' => 1, 'id_tipo' => 1, 'id_sku' => $sku->id]
            ];

            DB::table('artw_variacoes')->insert($arrayInsert);
        }

        DB::commit();
    }
}
