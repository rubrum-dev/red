<?php

use Illuminate\Database\Seeder;
use App\Skus;
use App\ArtesFinais;

class SkuVariacoes extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $skusPendentes = Skus
            ::select('skus.*')
            ->leftjoin('artw_variacoes', 'artw_variacoes.id_sku', '=', 'skus.id')
            ->where('artw_variacoes.id', NULL)
            ->get();

        DB::beginTransaction();

        foreach ($skusPendentes as $sku) {

            $arrayInsert = [
                ['nome' => 'Original', 'principal' => 1, 'id_tipo' => ($sku->promocional) ? 2 : 1, 'id_sku' => $sku->id]
            ];

            DB::table('artw_variacoes')->insert($arrayInsert);
        }

        $skusOutros = Skus
            ::select('skus.*')
            ->leftJoin('artw_variacoes', function($join) {
                $join->on('artw_variacoes.id_sku', '=', 'skus.id');
                $join->on('artw_variacoes.principal', '=', DB::raw('1'));
            })
            ->where('artw_variacoes.id', NULL)
            ->get();

        foreach ($skusOutros as $sku) {

            $arrayInsert = [
                ['nome' => 'Original', 'principal' => 1, 'id_tipo' => ($sku->promocional) ? 2 : 1, 'id_sku' => $sku->id]
            ];

            DB::table('artw_variacoes')->insert($arrayInsert);
        }

        $artesFinais = ArtesFinais::get();

        foreach ($artesFinais as $arte) {

            if ($arte->skus) {
                $variacao = ($arte->skus) ? $arte->skus->artwVariacoes->where('principal', 1)->first() : null;

                if (!$variacao) {
                    echo 'Arte-final - Parou: ' . $arte->id . "\n";
                } else {
                    $arte->id_variacao = $variacao->id;
                    $arte->save();
                }
            }
        }

        $variacoes = App\ArtwVariacoes::all();

        foreach ($variacoes as $variacao) {
            
            if ($variacao->sku) {
                $variacao->status = ($variacao->sku) ? $variacao->sku->status : NULL;
                $variacao->ordem = ($variacao->sku) ? $variacao->sku->ordem : NULL;
                //$variacao->dt_expiracao = ($variacao->sku && $variacao->sku->dt_expiracao) ? $variacao->sku->dt_expiracao->format('d/m/Y') : NULL;
                $variacao->nome_original = $variacao->sku_nome_completo;
                $variacao->save();
            }
            
            if ($variacao->artwItens->count() == 0) {
                
                $item = App\ArtwItens::firstOrNew(['id_variacao' => $variacao->id]);
                $item->novo = 1; 
                $item->save(); 
                
            }
        }

        $variacoesOrfas = App\ArtwVariacoes::where('status', NULL)->get();

        foreach ($variacoesOrfas as $variacao) {
            $variacao->artwItens()->delete();
            $variacao->delete();
        }

        DB::commit();
    }
}
