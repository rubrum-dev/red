<?php

use Illuminate\Database\Seeder;
use App\Skus;

class ArtworkSkus extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $skus = Skus::where('id', '>', '544')->where('id', '<', '600')->get();
        
        $id_novo = 600;
        
        DB::beginTransaction();
        
        foreach ($skus as $sku) {
            
            $sku->id = $id_novo;
            $sku->save();
            $id_novo++;
            
        }
        
        DB::commit();
    }
}
