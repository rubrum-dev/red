<?php

use Illuminate\Database\Seeder;
use App\ArtwItens;
use App\ArtwVariacoes;
use App\Skus;
use App\ArtwTickets;

class ArtworkItens extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('artw_itens')->truncate();
        DB::table('artw_item_tipos')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        
        DB::beginTransaction();
        
        $arr = [
            ['id' => 1, 'nome' => 'Rolha Metálica'],
            ['id' => 2, 'nome' => 'Front'],
            ['id' => 3, 'nome' => 'Back'],
            ['id' => 4, 'nome' => 'Neck'],
            ['id' => 5, 'nome' => 'Foil'],
            ['id' => 6, 'nome' => 'Front/Back/Neck'],
        ];
        
        DB::table('artw_item_tipos')->insert($arr);
        
        $novos_skus = Skus::where('status', 0)->where('ordem', null)->get();
        
        foreach($novos_skus as $sku) {
            
            $sku->novo = 1;
            $sku->save();
            
            $variacoes = $sku->artwVariacoes;
            
            foreach($variacoes as $variacao) {
                
                $variacao->novo = 1;
                $variacao->save();
                
            }
            
        }
        
        $variacoes = ArtwVariacoes::all();
        
        foreach($variacoes as $variacao) {
            
            $item = new ArtwItens;
            $item->id_variacao = $variacao->id;
            $item->save();
            
        }
        
        $tickets = ArtwTickets::all();
        
        foreach ($tickets as $ticket) {
            
            $ticket->id_item = $ticket->artwVariacao->artwItens->first()->id;
            $ticket->save();
            
        }
        
        DB::commit();
    }
}
