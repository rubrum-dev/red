<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$usosIncorretos = [
            ['nome' => 'Logotipo'],
            ['nome' => 'Sobre Fundos']
        ];

        DB::table('tipos_usos_incorretos')->insert($usosIncorretos);

        //

    	$niveisCores = [
            ['nome' => 'Primárias'],
            ['nome' => 'Secundárias']
        ];

        DB::table('niveis_cores')->insert($niveisCores);

		//

    	$tiposCores = [
            ['nome' => 'Logotipo'],
            ['nome' => 'Paleta']
        ];

        DB::table('tipos_cores')->insert($tiposCores);

		//

    	$usosAplicacoes = [
            ['nome' => 'Branco'],
            ['nome' => 'Monocromático'],
            ['nome' => 'Textura'],
            ['nome' => 'Imagem']
        ];

        DB::table('tipos_usos_aplicacoes')->insert($usosAplicacoes);

		//

    	$tiposTipografias = [
            ['nome' => 'Primárias'],
            ['nome' => 'Secundárias']
        ];

        DB::table('tipos_tipografias')->insert($tiposTipografias);

		//

    	$proporcoesVersoes = [
            ['nome' => 'Preferencial'],
            ['nome' => 'Vertical'],
            ['nome' => 'Horizontal']
        ];

        DB::table('tipos_proporcoes_versoes')->insert($proporcoesVersoes);

		//

    	$categoriasProporcoes = [
            ['nome' => 'Grid de Construção'],
            ['nome' => 'Redução Máxima'],
            ['nome' => 'Área de Proteção']
        ];

        DB::table('categorias_proporcoes')->insert($categoriasProporcoes);

		//

    	$categoriasVersoes = [
            ['nome' => 'Em Cores'],
            ['nome' => 'Preto e Branco'],
            ['nome' => 'Tons de Cinza'],
            ['nome' => 'Positiva']
        ];

        DB::table('categorias_versoes')->insert($categoriasVersoes);

		//

    	$categoriasProdutos = [
            ['nome' => 'Refrigerante'],
            ['nome' => 'Cerveja'],
            ['nome' => 'Energético'],
            ['nome' => 'Isotônico'],
            ['nome' => 'Mistas']
        ];

        DB::table('categorias_produtos')->insert($categoriasProdutos);

		//

    	$tiposLogotipos = [
            ['nome' => 'Exibição Família e Produto. (Fundo cor chapada. Logo negativo)'],
            ['nome' => 'Exibição Guide (Em cores. Fundo transparente)']
        ];

        DB::table('tipos_logotipos')->insert($tiposLogotipos);

		//

    	$naturezaEmbalagens = [
            ['nome' => 'Promocionais'],
            ['nome' => 'Primárias'],
            ['nome' => 'Secundárias'],
            ['nome' => 'Terciárias']
        ];

        DB::table('natureza_embalagens')->insert($naturezaEmbalagens);

        //

        $tiposEmbalagens = [
            ['id_natureza_embalagem' => 1, 'nome' => 'Promocional', 'ordem' => 0],
            ['id_natureza_embalagem' => 2, 'nome' => 'Lata', 'ordem' => 1],
            ['id_natureza_embalagem' => 2, 'nome' => 'Garrafa', 'ordem' => 2],
            ['id_natureza_embalagem' => 3, 'nome' => 'Pack', 'ordem' => 3],
        ];

        DB::table('tipos_embalagens')->insert($tiposEmbalagens);

        //

        $dimensoes = [
            ['id_tipo_embalagem' => 1, 'nome' => 'Promocional', 'thumb' => '/images/layout/categoria_promo.png']
        ];

        DB::table('dimensoes')->insert($dimensoes);

		//

    	$admPerfis = [
            ['nome' => 'Master'],
            ['nome' => 'Editor'],
            ['nome' => 'Diretor'],
            ['nome' => 'Gerente'],
            ['nome' => 'Visitante']
        ];

        DB::table('adm_perfis')->insert($admPerfis);

		//

    	$admModulos = [
            ['nome' => 'Guide'],
            ['nome' => 'File'],
            ['nome' => 'SKU'],
            ['nome' => 'Enxoval']
        ];

        DB::table('adm_modulos')->insert($admModulos);

        //

        $admEmpresas = [
            ['nome' => 'PaintPack']
        ];

        DB::table('adm_empresas')->insert($admEmpresas);

        //

    	$admUsuario = [
            'id_adm_perfil' => 1,
            'id_adm_empresa' => 1,
            'nome' => 'Admin',
            'sobrenome' => 'PaintPack',
            'email' => 'fernando@paintpack.com.br',
            'senha' => bcrypt('pack2005'),
            'fone' => '11 22334455',
            'cargo' => 'Programador',
            'depto' => 'Administração',
            'status' => 1,
            'primeiro_acesso' => 0
        ];

        DB::table('adm_usuarios')->insert($admUsuario);

    }
}
