<?php

use Illuminate\Database\Seeder;
use App\AdmUsuarios;
use App\AdmCargos;
use App\AdmDepartamentos;

class ArtworkUsuarios extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuarios = AdmUsuarios::all();
        
        DB::beginTransaction();
        
        foreach ($usuarios as $usuario) {
            
            $cargo = AdmCargos::firstOrNew(['nome' => $usuario->cargo]);
            $cargo->save();
            
            $usuario->id_cargo = $cargo->id;
            
            $departamento = AdmDepartamentos::firstOrNew(['nome' => $usuario->depto]);
            $departamento->save();
            
            $usuario->id_departamento = $departamento->id;
            
            $usuario->save();
        }
        
        DB::commit();        
        
    }
}
