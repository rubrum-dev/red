<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class Artwork extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tktStatus = [
            ['id' => 100, 'nome' => 'Novo'],
            ['id' => 1, 'nome' => 'Em Edição'],
            ['id' => 2, 'nome' => 'Em Revisão'],
            ['id' => 3, 'nome' => 'Marketing'],
            ['id' => 4, 'nome' => 'Em Aprovação'],
            ['id' => 5, 'nome' => 'Aprovado'],
            ['id' => 6, 'nome' => 'Enviado'],
            ['id' => 7, 'nome' => 'Pausado'],
            ['id' => 8, 'nome' => 'Cancelado'],
            ['id' => 9, 'nome' => 'Em Digitacao']
        ];
        DB::table('artw_tkt_status')->insert($tktStatus);

        //

        $tktPerfis = [
            ['nome' => 'Aprovador'],
            ['nome' => 'Marketing'],
            ['nome' => 'Revisor'],
            ['nome' => 'Participante'],
            ['nome' => 'Arte-finalista']
        ];
        DB::table('artw_perfis_usuarios')->insert($tktPerfis);
        
        $tktStatus = [
            ['id' => 100, 'nome' => 'Novo'],
            ['id' => 1, 'nome' => 'Em andamento'],
            ['id' => 2, 'nome' => 'Aprovado'],
            ['id' => 3, 'nome' => 'Reprovado']
        ];
        DB::table('artw_ciclo_status')->insert($tktStatus);
        
        $tktStatus = [
            ['nome' => 'Em andamento'],
            ['nome' => 'Revisado'],
            ['nome' => 'Reprovado'],
            ['nome' => 'Em Aberto']
        ];
        DB::table('artw_solicitacao_status')->insert($tktStatus);
        
        $tktStatus = [
            ['nome' => 'Texto Legal', 'icone' => 'icon-text-center'],
            ['nome' => 'Elementos Gráficos', 'icone' => 'icon-grids'],
            ['nome' => 'Elementos Textuais', 'icone' => 'icon-text-width'],
            ['nome' => 'Tabela Nutricional', 'icone' => 'icon-fire'],
            ['nome' => 'Composição do produto', 'icone' => 'icon-waves'],
            ['nome' => 'Nova Identidade Visual', 'icone' => 'icon-shield'],
            ['nome' => 'Planta Técnica', 'icone' => 'icon-box-blank'],
            ['nome' => 'Outros', 'icone' => 'icon-options-settings']
        ];
        DB::table('artw_solicitacao_tipos')->insert($tktStatus);
    }
}
