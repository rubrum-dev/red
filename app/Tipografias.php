<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipografias extends Model
{
	protected $fillable = array(
		'id_tipo_tipografia',
		'nome',
		'thumb',		
		'descricao',
		'path_down',
		'link',
		'status'
	);
    
    public function produtos()
	{
		return $this->belongsToMany('App\Produtos', 'produtos_tipografias', 'id_tipografia', 'id_produto');
	}

	public function tiposTipografias()
    {
    	return $this->belongsTo('App\TiposTipografias', 'id_tipo_tipografia');
    }
}
