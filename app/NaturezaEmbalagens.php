<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NaturezaEmbalagens extends Model
{
    public function tiposEmbalagens()
    {
    	return $this->hasMany('App\TiposEmbalagens', 'id_natureza_embalagem');
    }
}
