<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmPerfis extends Model
{

	protected $fillable = array('nome', 'gerencia_usuario', 'status');

    public function admUsuarios()
    {
    	return $this->hasMany('App\AdmUsuarios', 'id_adm_perfil');
    }

    public function familias()
    {
    	return $this->belongsToMany('App\Familias', 'adm_perfis_familias', 'id_adm_perfil', 'id_familia');
    }
}
