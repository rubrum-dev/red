<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwComentarios extends Model
{

    protected $fillable = [
        'id_solicitacao',
        'id_usuario',
        'texto'
    ];

    public function artwSolicitacao()
    {
        return $this->belongsTo('App\ArtwSolicitacoes', 'id_solicitacao');
    }

    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario')->withTrashed();
    }
    
    public function artwRespostas()
    {
        return $this->hasMany('App\ArtwRespostas', 'id_comentario');
    }
    
    public function getTextoAttribute($value)
    {
        return nl2br($value);
    }
    
    public function artwTicket()
    {
        return $this->belongsTo('App\ArtwTickets', 'id_ticket');
    }

}
