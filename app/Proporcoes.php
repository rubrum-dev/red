<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proporcoes extends Model
{
    protected $fillable = array(
		'id_categoria_proporcao',
		'id_tipo_proporcao_versao',
		'descricao',
		'thumb',
		'status'
	);

    public function produtos()
	{
		return $this->belongsToMany('App\Produtos', 'produtos_proporcoes', 'id_proporcao', 'id_produto');
	}

	public function tiposProporcoesVersoes()
    {
    	return $this->belongsTo('App\TiposProporcoesVersoes', 'id_tipo_proporcao_versao');
    }

    public function categoriasVersoes()
    {
    	return $this->belongsTo('App\CategoriasProporcoes', 'id_categoria_proporcao');
    }
}
