<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Icones extends Model
{
    protected $fillable = array(
		'id_categoria_icone',
		'descricao',
		'thumb',
		'path_down',
		'status'
	);

    public function produtos()
	{
		return $this->belongsToMany('App\Produtos', 'produtos_icones', 'id_icone', 'id_produto');
	}

    public function categoriasIcones()
    {
    	return $this->belongsTo('App\CategoriasIcones', 'id_categoria_icone');
    }
}
