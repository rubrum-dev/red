<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwTktStatus extends Model
{

    protected $table = 'artw_tkt_status';

    public function artwTickets()
    {
        return $this->hasMany('App\ArtwTickets', 'id_status');
    }

}
