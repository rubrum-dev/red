<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
//use Illuminate\Support\Facades\DB;

class ArtwVariacoes extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'id_natureza_embalagem',
        'id_campanha',
        'id_tipo',
        'id_sku',
        'nome'
    ];
    
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
    
    protected $dates = [
        'dt_expiracao'
    ];

    public function natureza()
    {
    	return $this->belongsTo('App\NaturezaEmbalagens', 'id_natureza_embalagem');
    }

    public function artesFinais()
    {
        return $this->hasMany('App\ArtesFinais', 'id_variacao');
    }
    
    public function artwVersoes()
    {
        return $this->hasMany('App\ArtwVersoes', 'id_variacao');
    }
    
    public function variacaoItens()
    {
        return $this->hasMany('App\ArtwVariacaoItens', 'id_variacao');
    }

    public function variacoesRelacionadas()
    {
        //return $this->hasMany('App\ArtwVariacoesRelacionadas', 'id_variacao');

        return $this->belongsToMany('App\ArtwVariacoesRelacionadas', 'artw_variacoes_relacionadas', 'id_variacao', 'id_variacao_relacionada')->withTimestamps();
    }

    public function relacionadas()
    {
        return $this->hasMany('App\ArtwVariacoesRelacionadas', 'id_variacao');
    }

    public function mercados()
    {
        return $this->belongsToMany('App\ArtwMercados', 'artw_variacao_mercados', 'id_variacao', 'id_mercado');
    }

    public function admModulos()
    {
        return $this->belongsToMany('App\AdmModulos', 'adm_usuarios_modulos', 'id_adm_usuario', 'id_adm_modulo');
    }

    public function setDtExpiracaoAttribute($value)
    {
        if ($value) {
            $this->attributes['dt_expiracao'] = Carbon::createFromFormat('d/m/Y H:i:s', $value . ' 00:00:00')->toDateTimeString('Y-m-d H:i:s');
        }
    }

    public function campanha()
    {
        return $this->belongsTo('App\Campanhas', 'id_campanha');
    }

    public function artwVariacaoTipo()
    {
        return $this->belongsTo('App\ArtwVariacaoTipos', 'id_tipo');
    }

    public function sku()
    {
        return $this->belongsTo('App\Skus', 'id_sku')->withTrashed();
    }

    public function skuAll()
    {
        return $this->belongsTo('App\Skus', 'id_sku')->withTrashed();
    }

    public function itens()
    {
        //return $this->hasMany('App\ArtwItens', 'id_variacao');
        return $this->belongsToMany('App\ArtwItens', 'artw_variacao_itens', 'id_variacao', 'id_item');
    }

    public function artwItens()
    {
        $itens = $this
            ->hasMany('App\ArtwItens', 'id_variacao')
            ->select('artw_itens.*')
            ->join('artw_item_tipos', 'artw_item_tipos.id', '=', 'artw_itens.id_tipo')
            ->orderBy('artw_item_tipos.nome');

        return $itens;
    }

    public function getSkuNomeCompletoAttribute()
    {
        $this->sku = $this->sku;

        if ($this->sku) {

            $nome = $this->sku->produtos->nome
                . ' ' .
                $this->sku->dimensoes->tiposEmbalagens->nome
                . ' ' .
                $this->sku->dimensoes->nome;
        } else {
            $nome = 'Erro - ' . $this->id;
        }

        if ($this->campanha && isset($this->campanha->nome)) {
            $nome .= ' • ' . $this->campanha->nome;
        }

        if ($this->principal !== 1 && $this->nome) {
            $nome .= ' • ' . $this->nome;
        }

        unset($this->sku);

        return $nome;
    }

    public function getSkuNomeAbreviadoAttribute()
    {
        $this->sku = $this->sku;

        $nome = $this->sku->dimensoes->tiposEmbalagens->nome
            . ' ' .
            $this->sku->dimensoes->nome;

        if ($this->principal !== 1 && $this->nome) {
            $nome .= ' • ' . $this->nome;
        }

        if ($this->campanha) {
            $nome .= ' • ' . $this->campanha->nome;
        }

        unset($this->sku);

        return $nome;
    }
    
    public function getSkuNomeAttribute()
    {
        $this->sku = $this->sku;

        $nome = $this->sku->produtos->nome
            . ' ' .
            $this->sku->dimensoes->tiposEmbalagens->nome
            . ' ' .
            $this->sku->dimensoes->nome;

        unset($this->sku);

        return $nome;
    }

    public function getFuncaoAttribute()
    {
        return ($this->id_campanha) ? 'Promocional' : 'Regular';
    }

    public function ticketsGerais()
    {
        $statusAbertos = [0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12];

        $tickets = ArtwTickets
            ::distinct()
            
            ->join('artw_itens', 'artw_itens.id', '=', 'artw_tickets.id_item')
            
            ->join('artw_variacao_itens', 'artw_variacao_itens.id_item', '=', 'artw_itens.id')
            
            ->where('artw_variacao_itens.id_variacao', $this->id)
            ->where('numero', '>', 170000)
            ->whereIn('id_status', $statusAbertos)
            ->count();

        return $tickets;
    }

    public function deletar()
    {
        /*
        foreach ($this->artwItens as $item) {

            $item->delete();
        }
         * 
         */
        
        //echo $this->artwVersoes;
        
        foreach ($this->artwVersoes as $versao)
        {
            $versao->id_variacao = null;
            $versao->save();
        }
        
        //echo $this->artwVersoes;
        //exit;
        
        $this->variacaoItens()->delete();
        
        $this->artesFinais()->delete();

        return $this->delete();
    }
}
