<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwNotificacoesTipos extends Model
{
    public $fillable = [
        'nome',
        'slug',
        'template',
        'assunto',
        'conta_notificacao'
    ];

    public function artwNotificacoes()
    {
        return $this->hasMany('App\ArtwNotificacoes', 'id_notificacao');
    }
}
