<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwNotificacoesUsuario extends Model
{
    protected $fillable = [
        'id_notificacao',
        'id_usuario',
    ];

    public function artwNotificacao()
    {
        return $this->belongsTo('App\ArtwNotificacao', 'id_notificacao');
    }

    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario');
    }
}
