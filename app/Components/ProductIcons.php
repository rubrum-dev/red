<?php

namespace App\Components;

use App\Produtos;
use App\Icones;
use App\CategoriasIcones;
use App\Components\Common;
use Illuminate\Support\Facades\File;

use App\Http\Requests;

class ProductIcons
{
    private $icone;

    public function __construct(Icones $icone)
    {
        $this->icone = $icone;
    }

    public function getIcon($id)
    {
        $produto = Produtos::find($id);
        $categoriasIcones = CategoriasIcones::all();
        $categorias = array();

        foreach ($categoriasIcones as $value) {
            $categorias[$value->id] = $value->nome;
        }

		return view('admin.produtos.icones', compact('produto', 'categoriasIcones', 'categorias'));
    }

    public function create($request)
    {

        if(empty($request->id_categoria_icone)){
            return false;
        }

        $icon = $this->icone;
        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;
        $file = isset($dados['file']) ? $dados['file'] : null;
        $imagePath = '/images/uploads/icones/';
        $filePath = '/files/uploads/icones/';

        // Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $image];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

        // Validação de Upload de Arquivo
        $data = ['file' => $file];
        $rules = ['file' => 'mimes:zip'];
        $message = 'Formato de arquivo invalido. Utilize um arquivo ZIP';
        $validate->validaUpload($data, $rules, $message);

        $produto = Produtos::find($dados['id_prod']);

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;
        }

        if(!is_null($file)){
            $extension = $file->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $fileName = $hash.".".$extension;
            $destinationPath = storage_path().$filePath;
            $upload_success = $file->move($destinationPath, $fileName);
            $dados['path_down'] = $filePath.$fileName;
        }

        $produto->icones()->attach($icon->create($dados));
        $log = new Common;
        // Log nova ícone em Produto - 1 => Módulo Guide
        $log->setFavoritesLogs(1, $produto->id, 'CREATE', 'Novo ícone de produto', 'icones');
        die;
    }

    public function edit($id)
    {
        $editIcon = $this->icone->find($id);
        return response(compact('editIcon'));
    }

    public function update($request)
    {
        if(empty($request->id_categoria_icone)){
            return false;
        }

        $icon = $this->icone->find($request->id);
        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;
        $file = isset($dados['file']) ? $dados['file'] : null;
        $imagePath = '/images/uploads/icones/';
        $filePath = '/files/uploads/icones/';

        // Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $image];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

        // Validação de Upload de Arquivo
        $data = ['file' => $file];
        $rules = ['file' => 'mimes:zip'];
        $message = 'Formato de arquivo invalido. Utilize um arquivo ZIP';
        $validate->validaUpload($data, $rules, $message);

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;

            if(File::isFile(public_path().$icon->thumb)){
                File::delete(public_path().$icon->thumb);
            }
        }

        if(!is_null($file)){
            $extension = $file->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $fileName = $hash.".".$extension;
            $destinationPath = storage_path().$filePath;
            $upload_success = $file->move($destinationPath, $fileName);
            $dados['path_down'] = $filePath.$fileName;

            if(File::isFile(storage_path().$icon->path_down)){
                File::delete(storage_path().$icon->path_down);
            }
        }

        if($icon->update($dados)){
            $log = new Common;
            // Log update ícone em Produto - 1 => Módulo Guide
            $log->setFavoritesLogs(1, $icon->produtos->first()->id, 'UPDATE', 'Atualização de ícone de produto', 'icones');
        }
        die;
    }

    public function remove($id)
    {
        $removeIcon = $this->icone->find($id);
        $prodId = $removeIcon->produtos->first()->id;

        if(File::isFile(public_path().$removeIcon->thumb)){
            File::delete(public_path().$removeIcon->thumb);
        }

        if(File::isFile(storage_path().$removeIcon->path_down)){
            File::delete(storage_path().$removeIcon->path_down);
        }

        $removeIcon->produtos()->detach();
        if($removeIcon->delete()){
            $log = new Common;
            // Log remoção de ícone em Produto - 1 => Módulo Guide
            $log->setFavoritesLogs(1, $prodId, 'DELETE', 'Remoção de ícone de produto', 'icones');
        }
    }

}
