<?php
namespace App\Components;

use Mail;
use Validator;
use App\Skus;
use App\AdmUsuarios;
use App\Familias;
use App\Produtos;
use App\CategoriasProdutos;
use App\TiposEmbalagens;
use App\AdmLogNovidades;
use App\Components\Common;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use App\Http\Requests;

class SiteCommon
{

    public function getFavorites($sidebar)
    {
        $sortFavorites = array();
        $favorites = array();
        $user = null;

        if (!Auth::guest()) {
            $adm = new AdmUsuarios;
            $user = $adm->find(Auth::user()->id);
            if ($sidebar) {
                foreach ($user->produtos as $userProduto) {
                    $var = array(
                        'nome' => $userProduto->nome,
                        'family' => $userProduto->id_familia,
                        'product' => $userProduto->id,
                        'cor_hexa' => $userProduto->cor_hexa
                    );
                    $favorites[] = $var;
                }
                $sortFavorites = array_slice(array_reverse($favorites), 0, 5);
            } else {
                $allFamilies = Familias::all();
                foreach ($user->produtos as $userProduto) {
                    $product = Produtos::find($userProduto->id);
                    foreach ($allFamilies as $families) {
                        if ($families->id == $userProduto->id_familia)
                            $family = $families->nome;
                    }
                    $var = array(
                        'thumb' => ($product->replicar_logo_marca) ? $product->familias->logotipos[0]->thumb : $product->logotipos[0]->thumb,
                        'nome' => $userProduto->nome,
                        'family' => $userProduto->id_familia,
                        'family_name' => $family,
                        'product' => $userProduto->id,
                        'cor_hexa' => $userProduto->cor_hexa
                    );
                    $favorites[] = $var;
                }
                $sortFavorites = array_reverse($favorites);
            }
        }
        return $sortFavorites;
    }

    //Recebe Parametro $sidebar True para retornar 5 resultados ou False para retornar tudo
    public function getRecentSku($sidebar)
    {
        $recentSku = array();
        if ($sidebar) {
            $allSku = Skus::orderBy('id', 'desc')->take(5)->get();
            foreach ($allSku as $value) {
                $value->produtos;
                $recentSku[] = $value;
            }
        } else {
            $allSku = Skus::orderBy('id', 'desc')->get();
            $allFamilies = Familias::all();
            foreach ($allSku as $value) {
                foreach ($allFamilies as $families) {
                    if ($families->id == $value->produtos->id_familia)
                        $family = $families->nome;
                }

                $artes = ($value->artwVariacoes && $value->artwVariacoes->first()) ? $value->artwVariacoes->first()->artesFinais : [];

                foreach ($artes as $arteFinal) {
                    if ($arteFinal->arte_exibicao == 1)
                        $value->thumb = $arteFinal->thumb;
                }

                $value->familia = $family;
                $value->thumb_volume = $value->dimensoes->thumb;
                $dataEdit = date_create($value->created_at);
                $value->data = date_format($dataEdit, 'd/m/Y H:i');
                $value->produtos;
                $recentSku[] = $value;
            }
        }

        return $recentSku;
    }

    public function getFilters($type, $itens)
    {
        $categorias = array();
        $tiposEmbalagens = array();

        $allCategorias = CategoriasProdutos::all();
        $allTipos = TiposEmbalagens::where('status', 1)->orderBy('ordem')->get();

        if ($type == 'categorias') {
            foreach ($allCategorias as $value) {
                if (in_array($value->id, $itens))
                    $categorias[$value->id] = $value->nome;
            }
        }

        if ($type == 'tiposEmbalagens') {
            foreach ($allTipos as $value) {
                if (in_array($value->id, $itens))
                    $tiposEmbalagens[$value->id] = $value->nome;
            }
        }

        return compact('categorias', 'tiposEmbalagens');
    }

    public function checkUserRelations()
    {
        $adm = new AdmUsuarios;
        $famPermitidas = array();
        $modPermitidos = array();
        $like = array();
        $user = null;
        $favorites = $this->getFavorites(true);

        if (!Auth::guest()) {
            $user = $adm->find(Auth::user()->id);

            if ($user->admin) {

                $familias = Familias::all();
                $modulos = \App\AdmModulos::all();
                
            } else {
                
                $perfis = [];
                
                foreach ($user->admPerfis as $perfil) {
                    
                    $perfis[] = $perfil->id;
                    
                }
                
                //dd($perfis);
                
                $familias = Familias::select('familias.*')
                    ->distinct()
                    ->join('adm_perfis_familias', 'adm_perfis_familias.id_familia', '=', 'familias.id')
                    ->whereIn('adm_perfis_familias.id_adm_perfil', $perfis)
                    ->get();
                
                $modulos = $user->admModulos;
            }

            foreach ($familias as $familia) {
                $famPermitidas[] = $familia->id;
            }

            foreach ($modulos as $modulo) {
                $modPermitidos[] = $modulo->nome;
            }

            foreach ($favorites as $favorite) {
                $like[] = $favorite['product'];
            }
            
            $user->favorites = $like;
            $user->familias = $famPermitidas;
            //$user->modulos = $modPermitidos;
        }
        
        return $user;
    }

    public function getSearch($search)
    {
        $allFamilies = Familias::where('status', 1)->where('nome', 'like', '%' . $search . '%')->get();
        $allProducts = Produtos::where('status', 1)->where('nome', 'like', '%' . $search . '%')->get();
        $allSkus = Skus::where('nome', 'like', '%' . $search . '%')->get();
        $families = array();
        $products = array();
        $skus = array();
        $count = 0;

        foreach ($allFamilies as $family) {
            $families[] = array(
                'id' => $family->id,
                'nome' => $family->nome,
                'tipo' => 'Familia',
                'thumb' => $family->logotipos[0]->thumb,
                'url' => route('site.search.family', array($family->id))
            );
            $count++;
        }

        foreach ($allProducts as $product) {
            $products[] = array(
                'id' => $product->id,
                'nome' => $product->nome,
                'tipo' => 'Produto',
                'thumb' => $product->logotipos[0]->thumb,
                'url' => route('site.search.product', array($product->id_familia, $product->id))
            );
            $count++;
        }

        foreach ($allSkus as $sku) {
            $thumb = null;
            foreach ($sku->artesFinais as $arteFinal) {
                if ($arteFinal->arte_exibicao == 1)
                    $thumb = $arteFinal->thumb;
            }
            $skus[] = array(
                'id' => $sku->id,
                'nome' => $sku->nome,
                'tipo' => 'SKU',
                'thumb' => $thumb,
                'thumb_volume' => $sku->dimensoes->thumb,
                'url' => route('site.sku', array($sku->produtos->id_familia, $sku->id_produto, 'sku' => $sku->id))
            );
            $count++;
        }

        $results = array($families, $products, $skus);

        return array('count' => $count, 'result' => $results);
    }

    public function getNews()
    {
        $urls = null;
        $news = array();
        $user = $this->checkUserRelations();
        $mod = $user['modulos'];
        array_push($mod, 3);
        if (in_array($user->id_adm_perfil, [1, 2]))
            $logs = AdmLogNovidades::whereIn('id_produto', $user['favorites'])->where('status', 1)->orderBy('updated_at', 'desc')->take(48)->get();
        else
            $logs = AdmLogNovidades::whereIn('id_produto', $user['favorites'])->whereIn('id_modulo', $mod)->where('status', 1)->orderBy('updated_at', 'desc')->take(48)->get();
        foreach ($logs as $log) {
            $log->produtos;
            $log->admModulos;
            switch ($log->id_modulo) {
                case 1: // Guide
                    $urls = route('site.guide', array($log->produtos->id_familia, $log->id_produto, $log->guide));
                    break;
                case 2: // File
                    $urls = route('site.sku', array($log->produtos->id_familia, $log->id_produto));
                    break;
                case 3: // SKU
                    if ($log->sku && $log->operacao != 'DELETE')
                        $urls = route('site.sku', array($log->produtos->id_familia, $log->id_produto, 'sku' => $log->sku));
                    else
                        $urls = route('site.sku', array($log->produtos->id_familia, $log->id_produto));
                    break;
                case 4: // Enxoval
                    $urls = route('site.guide', array($log->produtos->id_familia, $log->id_produto, $log->guide));
                    break;
                default:
                    $urls = route('site.index');
                    break;
            }
            $data = date_create($log->updated_at);
            $value = array(
                'modulo_id' => $log->admModulos->id,
                'data_full' => $log->updated_at,
                'data' => date_format($data, 'd/m/Y'),
                'url' => $urls,
                'produto_nome' => $log->produtos->nome,
                'descricao' => $log->descricao
            );
            $news[] = $value;
        }

        return $news;
    }

    public function paginate($value)
    {
        $perPage = 20;
        $collection = new Collection($value);
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageSearchResults = $collection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $results = new LengthAwarePaginator($currentPageSearchResults, count($collection), $perPage);
        $results->setPath('');

        return $results;
    }

    public function createFrontUser($request)
    {
        $newPass = new Common;

        $rules = array(
            'email' => 'unique:adm_usuarios,email'
        );

        $validator = Validator::make($request->only('email'), $rules);

        if ($validator->fails()) {
            echo json_encode([
                'error' => true,
                'message' => 'O email informado já está cadastrado no sistema.'
            ]);
            die;
        }

        $pass = $newPass->gerarSenha(8);

        $data = array(
            'nome' => htmlentities($request->nome),
            'email' => $request->email,
            'senha' => $pass
        );

        $request['senha'] = bcrypt($pass);
        $request['id_adm_empresa'] = Auth::user()->id_adm_empresa;

        $user = new AdmUsuarios;

        if (!is_null($request->input('modulos'))) {
            if ($user->create($request->all())->admModulos()->sync($request->input('modulos'))) {
                Mail::send('emails.cadastro', $data, function ($message) use($data) {
                    $message->from('ambev@paintpackred.com', 'Ambev Red');
                    $message->to($data['email'])->subject('Cadastro de Usuário');
                });
            }
        } else {
            if ($user->create($request->all())) {
                Mail::send('emails.cadastro', $data, function ($message) use($data) {
                    $message->from('ambev@paintpackred.com', 'Ambev Red');
                    $message->to($data['email'])->subject('Cadastro de Usuário');
                });
            }
        }

        echo json_encode(['error' => false]);
        die;
    }

    public function editFrontUser($request)
    {
        $user = AdmUsuarios::find($request->id);

        if (isset($request->modulos))
            $user->admModulos()->sync($request->input('modulos'));
        else
            $user->admModulos()->detach();
        $user->update($request->all());

        echo json_encode(['error' => false]);
        die;
    }
}
