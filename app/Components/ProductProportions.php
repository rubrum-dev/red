<?php

namespace App\Components;

use App\Produtos;
use App\Proporcoes;
use App\TiposProporcoesVersoes;
use App\CategoriasProporcoes;
use App\Components\Common;
use Illuminate\Support\Facades\File;

use App\Http\Requests;

class ProductProportions
{
    private $proporcao;

    public function __construct(Proporcoes $proporcao)
    {
        $this->proporcao = $proporcao;
    }

    public function getProportion($id)
    {
        $produto = Produtos::find($id);
        $categoriasProporcoes = CategoriasProporcoes::all();
        $tiposProporcoesVersoes = TiposProporcoesVersoes::where('id', '<>', 1)->get();
        $proporcoes = array();

        foreach ($categoriasProporcoes as $value) {
            $categorias[$value->id] = $value->nome;
        }

        foreach ($tiposProporcoesVersoes as $value) {
            $tipos[$value->id] = $value->nome;
        }

        foreach ($produto->proporcoes as $proporcao) {
            $proporcao->descricao = substr($proporcao->descricao, 0, 150);
            $proporcoes[] = $proporcao;
        }

        $produto->proporcoes = $proporcoes;

		return view('admin.produtos.proporcoes', compact('produto', 'categoriasProporcoes', 'tiposProporcoesVersoes', 'categorias', 'tipos'));
    }

    public function create($request)
    {

        if(empty($request->id_categoria_proporcao) || empty($request->id_tipo_proporcao_versao)){
            return false;
        }

        $proportion = $this->proporcao;
        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;
        $imagePath = '/images/uploads/proporcoes/';

        // Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $image];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

        $produto = Produtos::find($dados['id_prod']);

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;
        }

        $produto->proporcoes()->attach($proportion->create($dados));
        $log = new Common;
        // Log nova proporção em Produto - 1 => Módulo Guide
        $log->setFavoritesLogs(1, $produto->id, 'CREATE', 'Nova proporção de produto', 'proporcoes');
        die;
    }

    public function edit($id)
    {
        $editProportion = $this->proporcao->find($id);
        return response(compact('editProportion'));
    }

    public function update($request)
    {
        if(empty($request->id_categoria_proporcao) || empty($request->id_tipo_proporcao_versao)){
            return false;
        }

        $proportion = $this->proporcao->find($request->id);
        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;
        $imagePath = '/images/uploads/proporcoes/';

        // Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $image];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;

            if(File::isFile(public_path().$proportion->thumb)){
                File::delete(public_path().$proportion->thumb);
            }
        }

        if($proportion->update($dados)){
            $log = new Common;
            // Log update proporção em Produto - 1 => Módulo Guide
            $log->setFavoritesLogs(1, $proportion->produtos->first()->id, 'UPDATE', 'Atualização de proporção de produto', 'proporcoes');
        }
        die;
    }

    public function remove($id)
    {
        $removeProportion = $this->proporcao->find($id);
        $prodId = $removeProportion->produtos->first()->id;

        if(File::isFile(public_path().$removeProportion->thumb)){
            File::delete(public_path().$removeProportion->thumb);
        }

        $removeProportion->produtos()->detach();
        if($removeProportion->delete()){
            $log = new Common;
            // Log remoção de proporção em Produto - 1 => Módulo Guide
            $log->setFavoritesLogs(1, $prodId, 'DELETE', 'Remoção de proporção de produto', 'proporcoes');
        }
    }

}
