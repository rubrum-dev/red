<?php

namespace App\Components;

use App\Produtos;
use App\UsosIncorretos;
use App\TiposUsosIncorretos;
use App\Components\Common;
use Illuminate\Support\Facades\File;

use App\Http\Requests;

class ProductMisuse
{
    private $uso;

    public function __construct(UsosIncorretos $uso)
    {
        $this->uso = $uso;
    }

    public function getMisuse($id)
    {
        $produto = Produtos::find($id);
        $tiposUsosIncorretos = TiposUsosIncorretos::all();
        $incorretos = array();

        foreach ($tiposUsosIncorretos as $value) {
            $tipos[$value->id] = $value->nome;
        }

        foreach ($produto->usosIncorretos as $incorreto) {
            $incorreto->descricao = substr($incorreto->descricao, 0, 150);
            $incorretos[] = $incorreto;
        }

        $produto->usosIncorretos = $incorretos;

		return view('admin.produtos.usos-incorretos', compact('produto', 'tiposUsosIncorretos', 'tipos'));
    }

    public function create($request)
    {
        if(empty($request->id_tipo_uso_incorreto)){
            return false;
        }

        $uso = $this->uso;
        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;

        // Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $image];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

        $produto = Produtos::find($dados['id_prod']);

        $imagePath = '/images/uploads/usos-incorretos/';

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;
        }

        $produto->usosIncorretos()->attach($uso->create($dados));
        $log = new Common;
        // Log nova uso incorreto em Produto - 1 => Módulo Guide
        $log->setFavoritesLogs(1, $produto->id, 'CREATE', 'Novo uso incorreto de produto', 'usos-incorretos');
        die;
    }

    public function edit($id)
    {
        $editMisuse = $this->uso->find($id);
        return response(compact('editMisuse'));
    }

    public function update($request)
    {
        if(empty($request->id_tipo_uso_incorreto)){
            return false;
        }

        $uso = $this->uso->find($request->id);
        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;

        // Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $image];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

        $produto = Produtos::find($dados['id_prod']);

        $imagePath = '/images/uploads/usos-incorretos/';

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;

            if(File::isFile(public_path().$uso->thumb)){
                File::delete(public_path().$uso->thumb);
            }
        }

        $uso->update($dados);
        if($uso->update($dados)){
            $log = new Common;
            // Log update uso incorreto em Produto - 1 => Módulo Guide
            $log->setFavoritesLogs(1, $uso->produtos->first()->id, 'UPDATE', 'Atualização de uso incorreto de produto', 'usos-incorretos');
        }
        die;
    }

    public function remove($id)
    {
        $removeMisuse = $this->uso->find($id);
        $prodId = $removeMisuse->produtos->first()->id;

        if(File::isFile(public_path().$removeMisuse->thumb)){
            File::delete(public_path().$removeMisuse->thumb);
        }

        $removeMisuse->produtos()->detach();
        if($removeMisuse->delete()){
            $log = new Common;
            // Log remoção de uso incorreto em Produto - 1 => Módulo Guide
            $log->setFavoritesLogs(1, $prodId, 'DELETE', 'Remoção de uso incorreto de produto', 'usos-incorretos');
        }
    }

}
