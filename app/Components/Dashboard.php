<?php
namespace App\Components;

use App\AdmUsuarios;
use App\ArtwAprovacoes;
use App\ArtwItens;
use App\ArtwSolicitacoes;
use App\RelMeses;
use App\RelMesesNumeros;
use App\RelTipos;
use App\ArtwTickets;
use App\ArtwVariacoes;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Dashboard
{
    
    public function atualizaDashboard($tipo)
    {
        $tipo = $tipo;

        $function = camel_case($tipo);

        \App::call('App\Components\Dashboard@'.$function);
    }

    public function getRelatorio($request, $detalhado = 0)
    {
        $tipo = 'relatorio_' . $request->input('tipo');

        $function = camel_case($tipo);

        if ($detalhado)
        {
            $function = $function . 'Detalhado';
        }

        if ($request->input('tipo') == 'ticketsEncerradosPorParticipanteFuncao')
        {
            return \App::call('App\Components\Dashboard@'.$function, ['data_inicio' => $request->input('data_inicio'), 'data_fim' => $request->input('data_fim'), 'funcoes_selecionadas' => $request->input('funcoes_selecionadas')]);
        }
        elseif ($request->input('tipo') == 'ticketsEmAndamentoPorParticipanteFuncao')
        {
            return \App::call('App\Components\Dashboard@'.$function, ['funcoes_selecionadas' => $request->input('funcoes_selecionadas')]);
        }
    }

    public function relatorioTicketsEncerradosPorParticipanteFuncao($data_inicio, $data_fim, $funcoes_selecionadas)
    {
        $data_inicio = Carbon::createFromFormat('d/m/Y', $data_inicio);
        $data_fim = Carbon::createFromFormat('d/m/Y', $data_fim);

        $funcoes = [];

        foreach ($funcoes_selecionadas as $funcao)
        {
            $funcoes[] = $funcao['id'];
        }

        $funcoes = implode(',',$funcoes);

        $status = '6';

        $dados = DB::select("
            SELECT
                nome,
                sobrenome,
                empresa,
                departamento,
                funcao,
                SUM(nr_tickets) AS nr_tickets
            FROM
                (
                    SELECT
                        adm_usuarios.nome,
                        adm_usuarios.sobrenome,
                        adm_empresas.nome AS empresa,
                        adm_departamentos.nome AS departamento,
                        artw_perfis_usuarios.nome AS funcao,
                        artw_perfis_usuarios_tickets.id_perfil,
                        artw_perfis_usuarios_tickets.id_usuario,
                        Count(artw_tickets.id) AS nr_tickets
                    FROM
                        artw_tickets
                    INNER JOIN artw_perfis_usuarios_tickets ON artw_perfis_usuarios_tickets.id_ticket = artw_tickets.id
                    INNER JOIN adm_usuarios ON artw_perfis_usuarios_tickets.id_usuario = adm_usuarios.id
                    LEFT JOIN adm_empresas ON adm_usuarios.id_adm_empresa = adm_empresas.id
                    LEFT JOIN adm_departamentos ON adm_usuarios.id_departamento = adm_departamentos.id
                    INNER JOIN artw_perfis_usuarios ON artw_perfis_usuarios_tickets.id_perfil = artw_perfis_usuarios.id
                    WHERE
                        artw_tickets.id_status IN (".$status.")
                        AND artw_perfis_usuarios_tickets.deleted_at IS NULL
                        AND artw_tickets.cancelado_em IS NULL
                        AND artw_perfis_usuarios_tickets.id_perfil IN (".$funcoes.")
                        AND artw_tickets.updated_at >= '".$data_inicio->format('Y-m-d')."'
                        AND artw_tickets.updated_at <= '".$data_fim->format('Y-m-d')."'
                    GROUP BY
                        artw_perfis_usuarios_tickets.id_perfil,
                        artw_perfis_usuarios_tickets.id_usuario
                    UNION ALL
                        SELECT
                            adm_usuarios.nome,
                            adm_usuarios.sobrenome,
                            adm_empresas.nome AS empresa,
                            adm_departamentos.nome AS departamento,
                            artw_perfis_usuarios.nome AS funcao,
                            artw_perfis_usuarios_tickets.id_perfil,
                            artw_perfis_usuarios_equipe_tickets.id_usuario,
                            Count(artw_tickets.id) AS nr_tickets
                        FROM
                            artw_tickets
                        INNER JOIN artw_perfis_usuarios_tickets ON artw_perfis_usuarios_tickets.id_ticket = artw_tickets.id
                        INNER JOIN artw_perfis_usuarios_equipe_tickets ON artw_perfis_usuarios_equipe_tickets.id_parent = artw_perfis_usuarios_tickets.id
                        INNER JOIN adm_usuarios ON artw_perfis_usuarios_equipe_tickets.id_usuario = adm_usuarios.id
                        LEFT JOIN adm_empresas ON adm_usuarios.id_adm_empresa = adm_empresas.id
                        LEFT JOIN adm_departamentos ON adm_usuarios.id_departamento = adm_departamentos.id
                        INNER JOIN artw_perfis_usuarios ON artw_perfis_usuarios_tickets.id_perfil = artw_perfis_usuarios.id
                        WHERE
                            artw_tickets.id_status IN (".$status.")
                            AND artw_perfis_usuarios_tickets.deleted_at IS NULL
                            AND artw_perfis_usuarios_equipe_tickets.deleted_at IS NULL
                            AND artw_tickets.cancelado_em IS NULL
                            AND artw_perfis_usuarios_tickets.id_perfil IN (".$funcoes.")
                            AND artw_tickets.updated_at >= '".$data_inicio->format('Y-m-d')."'
                            AND artw_tickets.updated_at <= '".$data_fim->format('Y-m-d')."'
                        GROUP BY
                            artw_perfis_usuarios_tickets.id_perfil,
                            artw_perfis_usuarios_equipe_tickets.id_usuario,
                            adm_usuarios.nome,
                            adm_usuarios.sobrenome,
                            adm_empresas.nome,
                            adm_departamentos.nome
                ) t1
            GROUP BY
                id_perfil,
                id_usuario,
                nome,
                sobrenome,
                empresa,
                departamento,
                funcao
        ");

        /*
        $dados = ArtwTickets
            ::select('adm_usuarios.nome', 'adm_usuarios.sobrenome', 'adm_empresas.nome AS empresa', 'adm_departamentos.nome AS departamento', 'artw_perfis_usuarios.nome AS funcao')
            ->selectRaw('Count(artw_tickets.id) AS tickets_encerrados')
            ->join('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
            ->join('adm_usuarios', 'artw_perfis_usuarios_tickets.id_usuario', '=', 'adm_usuarios.id')
            ->leftJoin('adm_empresas', 'adm_usuarios.id_adm_empresa', '=', 'adm_empresas.id')
            ->leftJoin('adm_departamentos', 'adm_usuarios.id_departamento', '=', 'adm_departamentos.id')
            ->join('artw_perfis_usuarios', 'artw_perfis_usuarios_tickets.id_perfil', '=', 'artw_perfis_usuarios.id')
            ->where('artw_tickets.id_status', 6)
            ->where('artw_tickets.updated_at', '>=', $data_inicio->format('Y-m-d'))
            ->where('artw_tickets.updated_at', '<=', $data_fim->format('Y-m-d'))
            ->whereNull('artw_perfis_usuarios_tickets.deleted_at')
            ->whereIn('artw_perfis_usuarios_tickets.id_perfil', $funcoes)
            ->groupBy('artw_perfis_usuarios_tickets.id_perfil')
            ->groupBy('artw_perfis_usuarios_tickets.id_usuario')
            ->get();
        */

        return $dados;
    }

    public function relatorioTicketsEncerradosPorParticipanteFuncaoDetalhado($data_inicio, $data_fim, $funcoes_selecionadas)
    {
        $data_inicio = Carbon::createFromFormat('d/m/Y', $data_inicio);
        $data_fim = Carbon::createFromFormat('d/m/Y', $data_fim);

        $funcoes = [];

        foreach ($funcoes_selecionadas as $funcao)
        {
            $funcoes[] = $funcao['id'];
        }

        $funcoes = implode(',',$funcoes);

        $status = '6';

        $dados = DB::select("
            SELECT
                adm_usuarios.nome,
                adm_usuarios.sobrenome,
                adm_empresas.nome AS empresa,
                adm_departamentos.nome AS departamento,
                artw_perfis_usuarios.nome AS funcao,
                artw_tickets.numero
            FROM
                artw_tickets
            INNER JOIN artw_perfis_usuarios_tickets ON artw_perfis_usuarios_tickets.id_ticket = artw_tickets.id
            INNER JOIN adm_usuarios ON artw_perfis_usuarios_tickets.id_usuario = adm_usuarios.id
            LEFT JOIN adm_empresas ON adm_usuarios.id_adm_empresa = adm_empresas.id
            LEFT JOIN adm_departamentos ON adm_usuarios.id_departamento = adm_departamentos.id
            INNER JOIN artw_perfis_usuarios ON artw_perfis_usuarios_tickets.id_perfil = artw_perfis_usuarios.id
            WHERE
                artw_tickets.id_status IN (".$status.")
                AND artw_perfis_usuarios_tickets.deleted_at IS NULL
                AND artw_tickets.cancelado_em IS NULL
                AND artw_perfis_usuarios_tickets.id_perfil IN (".$funcoes.")
                AND artw_tickets.updated_at >= '".$data_inicio->format('Y-m-d')."'
                AND artw_tickets.updated_at <= '".$data_fim->format('Y-m-d')."'
            
            UNION ALL
            
            SELECT
                adm_usuarios.nome,
                adm_usuarios.sobrenome,
                adm_empresas.nome AS empresa,
                adm_departamentos.nome AS departamento,
                artw_perfis_usuarios.nome AS funcao,
                artw_tickets.numero
            FROM
                artw_tickets
            INNER JOIN artw_perfis_usuarios_tickets ON artw_perfis_usuarios_tickets.id_ticket = artw_tickets.id
            INNER JOIN artw_perfis_usuarios_equipe_tickets ON artw_perfis_usuarios_equipe_tickets.id_parent = artw_perfis_usuarios_tickets.id
            INNER JOIN adm_usuarios ON artw_perfis_usuarios_equipe_tickets.id_usuario = adm_usuarios.id
            LEFT JOIN adm_empresas ON adm_usuarios.id_adm_empresa = adm_empresas.id
            LEFT JOIN adm_departamentos ON adm_usuarios.id_departamento = adm_departamentos.id
            INNER JOIN artw_perfis_usuarios ON artw_perfis_usuarios_tickets.id_perfil = artw_perfis_usuarios.id
            WHERE
                artw_tickets.id_status IN (".$status.")
                AND artw_perfis_usuarios_tickets.deleted_at IS NULL
                AND artw_perfis_usuarios_equipe_tickets.deleted_at IS NULL
                AND artw_tickets.cancelado_em IS NULL
                AND artw_perfis_usuarios_tickets.id_perfil IN (".$funcoes.")
                AND artw_tickets.updated_at >= '".$data_inicio->format('Y-m-d')."'
                AND artw_tickets.updated_at <= '".$data_fim->format('Y-m-d')."'
        ");

        /*
        $dados = ArtwTickets
            ::select('adm_usuarios.nome', 'adm_usuarios.sobrenome', 'adm_empresas.nome AS empresa', 'adm_departamentos.nome AS departamento', 'artw_perfis_usuarios.nome AS funcao', 'artw_tickets.numero')
            //->selectRaw('Count(artw_tickets.id) AS tickets_encerrados')
            ->join('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
            ->join('adm_usuarios', 'artw_perfis_usuarios_tickets.id_usuario', '=', 'adm_usuarios.id')
            ->leftJoin('adm_empresas', 'adm_usuarios.id_adm_empresa', '=', 'adm_empresas.id')
            ->leftJoin('adm_departamentos', 'adm_usuarios.id_departamento', '=', 'adm_departamentos.id')
            ->join('artw_perfis_usuarios', 'artw_perfis_usuarios_tickets.id_perfil', '=', 'artw_perfis_usuarios.id')
            ->where('artw_tickets.id_status', 6)
            ->where('artw_tickets.updated_at', '>=', $data_inicio->format('Y-m-d'))
            ->where('artw_tickets.updated_at', '<=', $data_fim->format('Y-m-d'))
            ->whereNull('artw_perfis_usuarios_tickets.deleted_at')
            ->whereIn('artw_perfis_usuarios_tickets.id_perfil', $funcoes)
            //->groupBy('artw_perfis_usuarios_tickets.id_perfil')
            //->groupBy('artw_perfis_usuarios_tickets.id_usuario')
            ->get();
        */

        return $dados;
    }

    public function relatorioTicketsEmAndamentoPorParticipanteFuncaoDetalhado($funcoes_selecionadas)
    {
        $funcoes = [];

        foreach ($funcoes_selecionadas as $funcao)
        {
            $funcoes[] = $funcao['id'];
        }

        $funcoes = implode(',',$funcoes);

        $status = '0, 1, 2, 3, 4, 5, 9, 10, 11, 12';

        $dados = DB::select("
            SELECT
                adm_usuarios.nome,
                adm_usuarios.sobrenome,
                adm_empresas.nome AS empresa,
                adm_departamentos.nome AS departamento,
                artw_perfis_usuarios.nome AS funcao,
                artw_tickets.numero
            FROM
                artw_tickets
            INNER JOIN artw_perfis_usuarios_tickets ON artw_perfis_usuarios_tickets.id_ticket = artw_tickets.id
            INNER JOIN adm_usuarios ON artw_perfis_usuarios_tickets.id_usuario = adm_usuarios.id
            LEFT JOIN adm_empresas ON adm_usuarios.id_adm_empresa = adm_empresas.id
            LEFT JOIN adm_departamentos ON adm_usuarios.id_departamento = adm_departamentos.id
            INNER JOIN artw_perfis_usuarios ON artw_perfis_usuarios_tickets.id_perfil = artw_perfis_usuarios.id
            WHERE
                artw_tickets.id_status IN (".$status.")
                AND artw_perfis_usuarios_tickets.deleted_at IS NULL
                AND artw_tickets.cancelado_em IS NULL
                AND artw_perfis_usuarios_tickets.id_perfil IN (".$funcoes.")
            
            UNION ALL
            
            SELECT
                adm_usuarios.nome,
                adm_usuarios.sobrenome,
                adm_empresas.nome AS empresa,
                adm_departamentos.nome AS departamento,
                artw_perfis_usuarios.nome AS funcao,
                artw_tickets.numero
            FROM
                artw_tickets
            INNER JOIN artw_perfis_usuarios_tickets ON artw_perfis_usuarios_tickets.id_ticket = artw_tickets.id
            INNER JOIN artw_perfis_usuarios_equipe_tickets ON artw_perfis_usuarios_equipe_tickets.id_parent = artw_perfis_usuarios_tickets.id
            INNER JOIN adm_usuarios ON artw_perfis_usuarios_equipe_tickets.id_usuario = adm_usuarios.id
            LEFT JOIN adm_empresas ON adm_usuarios.id_adm_empresa = adm_empresas.id
            LEFT JOIN adm_departamentos ON adm_usuarios.id_departamento = adm_departamentos.id
            INNER JOIN artw_perfis_usuarios ON artw_perfis_usuarios_tickets.id_perfil = artw_perfis_usuarios.id
            WHERE
                artw_tickets.id_status IN (".$status.")
                AND artw_perfis_usuarios_tickets.deleted_at IS NULL
                AND artw_perfis_usuarios_equipe_tickets.deleted_at IS NULL
                AND artw_tickets.cancelado_em IS NULL
                AND artw_perfis_usuarios_tickets.id_perfil IN (".$funcoes.")
        ");

        /*
        $dados = ArtwTickets
            ::select('adm_usuarios.nome', 'adm_usuarios.sobrenome', 'adm_empresas.nome AS empresa', 'adm_departamentos.nome AS departamento', 'artw_perfis_usuarios.nome AS funcao', 'artw_tickets.numero')
            //->selectRaw('Count(artw_tickets.id) AS tickets_encerrados')
            ->join('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
            ->join('adm_usuarios', 'artw_perfis_usuarios_tickets.id_usuario', '=', 'adm_usuarios.id')
            ->leftJoin('adm_empresas', 'adm_usuarios.id_adm_empresa', '=', 'adm_empresas.id')
            ->leftJoin('adm_departamentos', 'adm_usuarios.id_departamento', '=', 'adm_departamentos.id')
            ->join('artw_perfis_usuarios', 'artw_perfis_usuarios_tickets.id_perfil', '=', 'artw_perfis_usuarios.id')
            ->whereIn('id_status', [0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16])
            ->whereIn('artw_perfis_usuarios_tickets.id_perfil', $funcoes)
            ->whereNull('artw_perfis_usuarios_tickets.deleted_at')
            ->whereNull('artw_tickets.cancelado_em')
            //->whereNull('artw_tickets.pausado_em')
            //->groupBy('artw_perfis_usuarios_tickets.id_perfil')
            //->groupBy('artw_perfis_usuarios_tickets.id_usuario')
            ->get();
        */

        return $dados;
    }

    public function relatorioTicketsEmAndamentoPorParticipanteFuncao($funcoes_selecionadas)
    {
        $funcoes = [];

        foreach ($funcoes_selecionadas as $funcao)
        {
            $funcoes[] = $funcao['id'];
        }

        $funcoes = implode(',',$funcoes);

        $status = '0, 1, 2, 3, 4, 5, 9, 10, 11, 12';

        $dados = DB::select("
                SELECT
                nome,
                sobrenome,
                empresa,
                departamento,
                funcao,
                SUM(nr_tickets) AS nr_tickets
            FROM
                (
                    SELECT
                        adm_usuarios.nome,
                        adm_usuarios.sobrenome,
                        adm_empresas.nome AS empresa,
                        adm_departamentos.nome AS departamento,
                        artw_perfis_usuarios.nome AS funcao,
                        artw_perfis_usuarios_tickets.id_perfil,
                        artw_perfis_usuarios_tickets.id_usuario,
                        Count(artw_tickets.id) AS nr_tickets
                    FROM
                        artw_tickets
                    INNER JOIN artw_perfis_usuarios_tickets ON artw_perfis_usuarios_tickets.id_ticket = artw_tickets.id
                    INNER JOIN adm_usuarios ON artw_perfis_usuarios_tickets.id_usuario = adm_usuarios.id
                    LEFT JOIN adm_empresas ON adm_usuarios.id_adm_empresa = adm_empresas.id
                    LEFT JOIN adm_departamentos ON adm_usuarios.id_departamento = adm_departamentos.id
                    INNER JOIN artw_perfis_usuarios ON artw_perfis_usuarios_tickets.id_perfil = artw_perfis_usuarios.id
                    WHERE
                        artw_tickets.id_status IN (".$status.")
                        AND artw_perfis_usuarios_tickets.deleted_at IS NULL
                        AND artw_tickets.cancelado_em IS NULL
                        AND artw_perfis_usuarios_tickets.id_perfil IN (".$funcoes.")
                    GROUP BY
                        artw_perfis_usuarios_tickets.id_perfil,
                        artw_perfis_usuarios_tickets.id_usuario
                    UNION ALL
                        SELECT
                            adm_usuarios.nome,
                            adm_usuarios.sobrenome,
                            adm_empresas.nome AS empresa,
                            adm_departamentos.nome AS departamento,
                            artw_perfis_usuarios.nome AS funcao,
                            artw_perfis_usuarios_tickets.id_perfil,
                            artw_perfis_usuarios_equipe_tickets.id_usuario,
                            Count(artw_tickets.id) AS nr_tickets
                        FROM
                            artw_tickets
                        INNER JOIN artw_perfis_usuarios_tickets ON artw_perfis_usuarios_tickets.id_ticket = artw_tickets.id
                        INNER JOIN artw_perfis_usuarios_equipe_tickets ON artw_perfis_usuarios_equipe_tickets.id_parent = artw_perfis_usuarios_tickets.id
                        INNER JOIN adm_usuarios ON artw_perfis_usuarios_equipe_tickets.id_usuario = adm_usuarios.id
                        LEFT JOIN adm_empresas ON adm_usuarios.id_adm_empresa = adm_empresas.id
                        LEFT JOIN adm_departamentos ON adm_usuarios.id_departamento = adm_departamentos.id
                        INNER JOIN artw_perfis_usuarios ON artw_perfis_usuarios_tickets.id_perfil = artw_perfis_usuarios.id
                        WHERE
                            artw_tickets.id_status IN (".$status.")
                            AND artw_perfis_usuarios_tickets.deleted_at IS NULL
                            AND artw_perfis_usuarios_equipe_tickets.deleted_at IS NULL
                            AND artw_tickets.cancelado_em IS NULL
                            AND artw_perfis_usuarios_tickets.id_perfil IN (".$funcoes.")
                        GROUP BY
                            artw_perfis_usuarios_tickets.id_perfil,
                            artw_perfis_usuarios_equipe_tickets.id_usuario,
                            adm_usuarios.nome,
                            adm_usuarios.sobrenome,
                            adm_empresas.nome,
                            adm_departamentos.nome
                ) t1
            GROUP BY
                id_perfil,
                id_usuario,
                nome,
                sobrenome,
                empresa,
                departamento,
                funcao
        ");

        /*
        $dados = ArtwTickets
            ::select('adm_usuarios.nome', 'adm_usuarios.sobrenome', 'adm_empresas.nome AS empresa', 'adm_departamentos.nome AS departamento', 'artw_perfis_usuarios.nome AS funcao')
            ->selectRaw('Count(artw_tickets.id) AS tickets_andamento')
            ->join('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
            ->join('adm_usuarios', 'artw_perfis_usuarios_tickets.id_usuario', '=', 'adm_usuarios.id')
            ->leftJoin('adm_empresas', 'adm_usuarios.id_adm_empresa', '=', 'adm_empresas.id')
            ->leftJoin('adm_departamentos', 'adm_usuarios.id_departamento', '=', 'adm_departamentos.id')
            ->join('artw_perfis_usuarios', 'artw_perfis_usuarios_tickets.id_perfil', '=', 'artw_perfis_usuarios.id')
            ->whereIn('id_status', [0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16])
            ->whereIn('artw_perfis_usuarios_tickets.id_perfil', $funcoes)
            ->whereNull('artw_perfis_usuarios_tickets.deleted_at')
            ->whereNull('artw_tickets.cancelado_em')
            //->whereNull('artw_tickets.pausado_em')
            ->groupBy('artw_perfis_usuarios_tickets.id_perfil')
            ->groupBy('artw_perfis_usuarios_tickets.id_usuario')
            ->get();
        */

        return $dados;
    }

    public function ticketsEmAndamento()
    {
        $tipo = RelTipos::where('slug', str_slug(snake_case(__FUNCTION__)))->get()->first();

        $now = Carbon::now();
        //$now->modify('-10 minutes');

        $dados['Tickets']['numero'] = ArtwTickets
            ::join('artw_itens', 'artw_itens.id', '=', 'artw_tickets.id_item')
            ->whereIn('id_status', [0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16])
            ->where('numero', 'not like', '10%')
            ->whereNull('artw_tickets.cancelado_em')
            ->where('modulo', 1)
            ->get()->count();

        $dados['Tickets']['porcentagem'] = 100;

        if ($dados['Tickets']['numero'])
        {
            $dados['No prazo']['numero'] = ArtwTickets
                ::whereIn('id_status', [0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16])
                //->whereMonth('created_at', '=', $now->format('m'))
                //->whereYear('created_at', '=', $now->format('Y'))
                ->where('dt_fim', '>', Carbon::now())
                ->where('numero', 'not like', '10%')
                ->whereNull('artw_tickets.cancelado_em')
                ->where('modulo', 1)
                ->whereNull('pausado_em')
                ->get()->count();

            $pausados = ArtwTickets
                    ::whereNotIn('id_status', [0,9])
                    //->whereMonth('created_at', '=', $now->format('m'))
                    //->whereYear('created_at', '=', $now->format('Y'))
                    ->whereNotNull('pausado_em')
                    ->where('numero', 'not like', '10%')
                    ->where('modulo', 1)
                    ->get()->count();

            $dados['No prazo']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['No prazo']['numero'];

            $dados['Atrasados']['numero'] = $dados['Tickets']['numero'] - $pausados - $dados['No prazo']['numero'];
            $dados['Atrasados']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['Atrasados']['numero'];

            $dados['Pausados']['numero'] = $pausados;
            $dados['Pausados']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['Pausados']['numero'];

            $dados['Novos itens']['numero'] = ArtwTickets
                ::whereIn('id_status', [0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16])
                ->whereMonth('created_at', '=', $now->format('m'))
                ->whereYear('created_at', '=', $now->format('Y'))
                ->where('nova_embalagem', 1)
                ->where('numero', 'not like', '10%')
                ->where('modulo', 1)
                ->get()->count();
            $dados['Novos itens']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['Novos itens']['numero'];

            $dados['Alterações']['numero'] = ArtwTickets
                ::whereIn('id_status', [0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16])
                ->whereMonth('created_at', '=', $now->format('m'))
                ->whereYear('created_at', '=', $now->format('Y'))
                ->where('nova_embalagem', '!=', 1)
                ->where('numero', 'not like', '10%')
                ->where('modulo', 1)
                ->get()->count();
            $dados['Alterações']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['Alterações']['numero'];

            $dados['Cancelados']['numero'] = ArtwTickets
                ::whereIn('id_status', [0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16])
                ->whereMonth('created_at', '=', $now->format('m'))
                ->whereYear('created_at', '=', $now->format('Y'))
                ->whereNotNull('cancelado_em')
                ->where('numero', 'not like', '10%')
                ->where('modulo', 1)
                ->get()->count();
            $dados['Cancelados']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['Cancelados']['numero'];
        }

        DB::beginTransaction();

        $relMes = RelMeses::firstOrNew([
            'id_tipo' => $tipo->id,
            'id_usuario' => null,
            'mes' => $now->format('m'),
            'ano' => $now->format('Y')
        ]);

        $relMes->updated_at = date('Y-m-d H:i:s');
        $relMes->status = 1;
        $relMes->save();

        $relMes->numeros()->delete();

        foreach ($dados as $nome=>$numero)
        {
            $relMesNumero = RelMesesNumeros::firstOrNew([
                'nome' => $nome,
                'id_mes' => $relMes->id
            ]);

            $relMesNumero->numero = $numero['numero'];
            $relMesNumero->porcentagem = $numero['porcentagem'];
            $relMesNumero->save();
        }

        DB::commit();
    }
    
    public function tiposInstrucaoAberturaTicket()
    {
        $tipo = RelTipos::where('slug', str_slug(snake_case(__FUNCTION__)))->get()->first();

        $now = Carbon::now();
        $now->modify('-10 minutes');

        $solicitacoes = ArtwSolicitacoes
            ::select('artw_solicitacao_tipos.nome', DB::raw('count(artw_solicitacoes.id) as quantidade'))
            ->join('artw_solicitacao_tipos', 'artw_solicitacao_tipos.id', '=', 'artw_solicitacoes.id_tipo')
            //->where('tipo', 1)
            ->whereMonth('artw_solicitacoes.created_at', '=', $now->format('m'))
            ->whereYear('artw_solicitacoes.created_at', '=', $now->format('Y'))
            ->groupBy('artw_solicitacao_tipos.id')
            ->orderBy('quantidade', 'DESC')
            ->get();

        DB::beginTransaction();

        $relMes = RelMeses::firstOrNew([
            'id_tipo' => $tipo->id,
            'id_usuario' => null,
            'mes' => $now->format('m'),
            'ano' => $now->format('Y')
        ]);

        $relMes->updated_at = date('Y-m-d H:i:s');
        $relMes->status = 1;
        $relMes->save();

        $relMes->numeros()->delete();

        $total = $solicitacoes->sum('quantidade');

        if ($total > 0)
        {
            $relMesNumero = RelMesesNumeros::firstOrNew([
                'nome' => 'Instruções',
                'id_mes' => $relMes->id
            ]);
    
            $relMesNumero->numero = $total;
            $relMesNumero->porcentagem = 100;
            $relMesNumero->save();    
        }
        
        foreach ($solicitacoes as $solicitacao)
        {
            $relMesNumero = RelMesesNumeros::firstOrNew([
                'nome' => $solicitacao->nome,
                'id_mes' => $relMes->id
            ]);

            $relMesNumero->numero = $solicitacao->quantidade;
            $relMesNumero->porcentagem = (100 / $total) * $solicitacao->quantidade;
            $relMesNumero->save();
        }

        DB::commit();
    }

    public function aprovacoesPorDepartamento()
    {
        $tipo = RelTipos::where('slug', str_slug(snake_case(__FUNCTION__)))->get()->first();

        $now = Carbon::now();
        $now->modify('-10 minutes');

        DB::beginTransaction();

        $relMes = RelMeses::firstOrNew([
            'id_tipo' => $tipo->id,
            'id_usuario' => null,
            'mes' => $now->format('m'),
            'ano' => $now->format('Y')
        ]);

        $relMes->updated_at = date('Y-m-d H:i:s');
        $relMes->status = 1;
        $relMes->save();

        $relMes->numeros()->delete();

        $aprovacoes = ArtwAprovacoes
            ::select('adm_departamentos.nome', DB::raw('count(adm_departamentos.id) as quantidade'))
            ->join('adm_usuarios', 'artw_aprovacoes.id_usuario', '=', 'adm_usuarios.id')
            ->join('adm_departamentos', 'adm_usuarios.id_departamento', '=', 'adm_departamentos.id')
            ->where('aprovado', 1)
            ->whereMonth('artw_aprovacoes.created_at', '=', $now->format('m'))
            ->whereYear('artw_aprovacoes.created_at', '=', $now->format('Y'))
            ->groupBy('adm_departamentos.id')
            ->orderBy('quantidade', 'DESC')
            ->get();

        $total = $aprovacoes->sum('quantidade');

        if ($total > 0)
        {
            $relMesNumero = RelMesesNumeros::firstOrNew([
                'nome' => 'Aprovações',
                'id_mes' => $relMes->id
            ]);
    
            $relMesNumero->numero = $total;
            $relMesNumero->porcentagem = 100;
            $relMesNumero->save();
        }

        foreach ($aprovacoes as $aprovacao)
        {
            $relMesNumero = RelMesesNumeros::firstOrNew([
                'nome' => $aprovacao->nome,
                'id_mes' => $relMes->id
            ]);

            $relMesNumero->numero = $aprovacao->quantidade;
            $relMesNumero->porcentagem = (100 / $total) * $aprovacao->quantidade;
            $relMesNumero->save();
        }

        DB::commit();
    }

    public function ticketsEncerradosPorCiclo()
    {
        $tipo = RelTipos::where('slug', str_slug(snake_case(__FUNCTION__)))->get()->first();

        $now = Carbon::now();
        //$now->modify('-10 minutes');

        $dados = [];

        $total = ArtwTickets
            ::join('artw_itens', 'artw_itens.id', '=', 'artw_tickets.id_item')
            ->where('id_status', 6)            
            ->whereMonth('artw_tickets.updated_at', '=', $now->format('m'))
            ->whereYear('artw_tickets.updated_at', '=', $now->format('Y'))
            ->where('numero', 'not like', '10%')
            ->where('modulo', 1)
            ->get();

        $total = count($total);

        if ($total > 0)
        {
            $dados['Tickets']['numero'] = $total;
            $dados['Tickets']['porcentagem'] = 100;

            $dados['Ciclo 1']['numero'] = ArtwTickets
            ::where('id_status', 6)
            ->where('ciclo', 1)
            ->whereMonth('updated_at', '=', $now->format('m'))
            ->whereYear('updated_at', '=', $now->format('Y'))
            ->where('numero', 'not like', '10%')
            ->where('modulo', 1)
            ->get()->count();
            $dados['Ciclo 1']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['Ciclo 1']['numero'];

            $dados['Ciclo 2']['numero'] = ArtwTickets
                ::where('id_status', 6)
                ->where('ciclo', 2)
                ->whereMonth('updated_at', '=', $now->format('m'))
                ->whereYear('updated_at', '=', $now->format('Y'))
                ->where('numero', 'not like', '10%')
                ->where('modulo', 1)
                ->get()->count();
            $dados['Ciclo 2']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['Ciclo 2']['numero'];

            $dados['Ciclo 3']['numero'] = ArtwTickets
                ::where('id_status', 6)
                ->where('ciclo', 3)
                ->whereMonth('updated_at', '=', $now->format('m'))
                ->whereYear('updated_at', '=', $now->format('Y'))
                ->where('numero', 'not like', '10%')
                ->where('modulo', 1)
                ->get()->count();
            $dados['Ciclo 3']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['Ciclo 3']['numero'];

            $dados['Ciclo 4']['numero'] = ArtwTickets
                ::where('id_status', 6)
                ->where('ciclo', 4)
                ->whereMonth('updated_at', '=', $now->format('m'))
                ->whereYear('updated_at', '=', $now->format('Y'))
                ->where('numero', 'not like', '10%')
                ->where('modulo', 1)
                ->get()->count();
            $dados['Ciclo 4']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['Ciclo 4']['numero'];

            $dados['Ciclo 5+']['numero'] = ArtwTickets
                ::where('id_status', 6)
                ->where('ciclo', '>=', 5)
                ->whereMonth('updated_at', '=', $now->format('m'))
                ->whereYear('updated_at', '=', $now->format('Y'))
                ->where('numero', 'not like', '10%')
                ->where('modulo', 1)
                ->get()->count();
            $dados['Ciclo 5+']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['Ciclo 5+']['numero'];
        }

        DB::beginTransaction();

        $relMes = RelMeses::firstOrNew([
            'id_tipo' => $tipo->id,
            'id_usuario' => null,
            'mes' => $now->format('m'),
            'ano' => $now->format('Y')
        ]);

        $relMes->updated_at = date('Y-m-d H:i:s');
        $relMes->status = 1;
        $relMes->save();

        $relMes->numeros()->delete();

        foreach ($dados as $nome=>$numero)
        {
            $relMesNumero = RelMesesNumeros::firstOrNew([
                'nome' => $nome,
                'id_mes' => $relMes->id
            ]);

            $relMesNumero->numero = $numero['numero'];
            $relMesNumero->porcentagem = $numero['porcentagem'];
            $relMesNumero->save();
        }

        DB::commit();
    }

    public function embalagens()
    {
        $tipo = RelTipos::where('slug', str_slug(snake_case(__FUNCTION__)))->get()->first();

        $now = Carbon::now();
        $now->modify('-10 minutes');

        DB::beginTransaction();

        $relMes = RelMeses::firstOrNew([
            'id_tipo' => $tipo->id,
            'id_usuario' => null,
            'mes' => $now->format('m'),
            'ano' => $now->format('Y')
        ]);

        $relMes->updated_at = date('Y-m-d H:i:s');
        $relMes->status = 1;
        $relMes->save();

        $relMes->numeros()->delete();

        $embalagens = ArtwVariacoes::count();

        $dados['Embalagens']['numero'] = $embalagens;
        $dados['Embalagens']['porcentagem'] = 100;
        
        $itensTotal = ArtwItens::where('status', '!=', 0)->get()->count();

        $dados['Itens de embalagem']['numero'] = $itensTotal;
        $dados['Itens de embalagem']['porcentagem'] = 100;

        if ($itensTotal)
        {
            $itensDeAlteracao = ArtwTickets
            ::join('artw_itens', 'artw_itens.id', '=', 'artw_tickets.id_item')
            ->whereIn('id_status', [0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16])
            ->where('numero', 'not like', '10%')
            ->where('modulo', 1)
            ->whereNull('artw_tickets.cancelado_em')
            ->get()->count();

            $dados['Itens em alteração']['numero'] = $itensDeAlteracao;
            $dados['Itens em alteração']['porcentagem'] = (100 / $itensTotal) * $itensDeAlteracao;

            $itensDescontinuados = ArtwItens::where('status', 2)->get()->count();

            $dados['Itens descontinuados']['numero'] = $itensDescontinuados;
            $dados['Itens descontinuados']['porcentagem'] = (100 / $itensTotal) * $itensDescontinuados;

            foreach ($dados as $nome=>$numero)
            {
                $relMesNumero = RelMesesNumeros::firstOrNew([
                    'nome' => $nome,
                    'id_mes' => $relMes->id
                ]);

                $relMesNumero->numero = $numero['numero'];
                $relMesNumero->porcentagem = $numero['porcentagem'];
                $relMesNumero->save();
            }
        }

        DB::commit();
    }

    public function usuarios()
    {
        $tipo = RelTipos::where('slug', str_slug(snake_case(__FUNCTION__)))->get()->first();

        $now = Carbon::now();
        $now->modify('-10 minutes');

        DB::beginTransaction();

        $relMes = RelMeses::firstOrNew([
            'id_tipo' => $tipo->id,
            'id_usuario' => null,
            'mes' => $now->format('m'),
            'ano' => $now->format('Y')
        ]);

        $relMes->updated_at = date('Y-m-d H:i:s');
        $relMes->status = 1;
        $relMes->save();

        $relMes->numeros()->delete();

        $usuarios = AdmUsuarios::where('admin', null)->get()->count();

        $dados['Usuários']['numero'] = $usuarios;
        $dados['Usuários']['porcentagem'] = 100;
        
        if ($usuarios)
        {
            $gerentes = AdmUsuarios
                ::orWhere('gerencia_usuario', 1)
                ->orWhere('gerencia_midia', 1)
                ->orWhere('gerencia_fornecedor', 1)
                ->orWhere('gerencia_embalagem', 1)
                ->orWhere('gerencia_foto', 1)
                ->get()->count();

            $dados['Gerentes']['numero'] = $gerentes;
            $dados['Gerentes']['porcentagem'] = (100 / $usuarios) * $gerentes;

            $administradores = AdmUsuarios
                ::where('gerente', 1)
                ->get()->count();

            $dados['Administradores']['numero'] = $administradores;
            $dados['Administradores']['porcentagem'] = (100 / $usuarios) * $administradores;

            $inativos = AdmUsuarios
                ::where('status', 0)
                ->get()->count();

            $dados['Inativos']['numero'] = $inativos;
            $dados['Inativos']['porcentagem'] = (100 / $usuarios) * $inativos;

            foreach ($dados as $nome=>$numero)
            {
                $relMesNumero = RelMesesNumeros::firstOrNew([
                    'nome' => $nome,
                    'id_mes' => $relMes->id
                ]);

                $relMesNumero->numero = $numero['numero'];
                $relMesNumero->porcentagem = $numero['porcentagem'];
                $relMesNumero->save();
            }
        }

        DB::commit();
    }

    public function ticketsEmAndamentoUsuario()
    {
        $tipo = RelTipos::where('slug', str_slug(snake_case(__FUNCTION__)))->get()->first();

        $now = Carbon::now();

        $query = ArtwTickets
            ::join('artw_itens', 'artw_itens.id', '=', 'artw_tickets.id_item')
            ->leftJoin('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
            ->leftJoin('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
            ->whereIn('id_status', [0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16])
            ->where('numero', 'not like', '10%')
            ->whereNull('artw_tickets.cancelado_em')
            ->where('modulo', 1)
            ->where(function ($query) {
                $query->orwhere(function($query) {
                    $query->where('artw_perfis_usuarios_tickets.id_usuario', Auth::user()->id);
                    $query->where('artw_perfis_usuarios_tickets.deleted_at', null);
                });

                $query->orwhere(function($query) {
                    $query->where('artw_perfis_usuarios_equipe_tickets.id_usuario', Auth::user()->id);
                    $query->where('artw_perfis_usuarios_equipe_tickets.deleted_at', null);
                });
            })
            ->groupBy('artw_tickets.id')
            ->get();

        $dados['Tickets']['numero'] = count($query);
        $dados['Tickets']['porcentagem'] = 100;

        if ($dados['Tickets']['numero'] > 0)
        {
            $query = ArtwTickets
                ::join('artw_itens', 'artw_itens.id', '=', 'artw_tickets.id_item')
                ->leftJoin('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
                ->leftJoin('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
                ->whereIn('id_status', [0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16])
                ->where('numero', 'not like', '10%')
                ->whereNull('artw_tickets.cancelado_em')
                ->where('modulo', 1)
                ->where('artw_tickets.id_usuario', Auth::user()->id)
                ->where(function ($query) {
                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_tickets.deleted_at', null);
                    });

                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_equipe_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_equipe_tickets.deleted_at', null);
                    });
                })
                ->groupBy('artw_tickets.id')
                ->get();

            $dados['Sou Líder']['numero'] = count($query);
            $dados['Sou Líder']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['Sou Líder']['numero'];

            $query = ArtwTickets
                ::join('artw_itens', 'artw_itens.id', '=', 'artw_tickets.id_item')
                ->leftJoin('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
                ->leftJoin('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
                ->whereIn('id_status', [0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16])
                ->where('numero', 'not like', '10%')
                ->whereNull('artw_tickets.cancelado_em')
                ->where('modulo', 1)
                ->where('dt_fim', '>', Carbon::now())
                ->where(function ($query) {
                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_tickets.deleted_at', null);
                    });

                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_equipe_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_equipe_tickets.deleted_at', null);
                    });
                })
                ->groupBy('artw_tickets.id')
                ->get();

            $dados['No Prazo']['numero'] = count($query);
            $dados['No Prazo']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['No Prazo']['numero'];

            $pausados = ArtwTickets
                ::join('artw_itens', 'artw_itens.id', '=', 'artw_tickets.id_item')
                ->leftJoin('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
                ->leftJoin('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
                ->whereIn('id_status', [0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16])
                ->where('numero', 'not like', '10%')
                ->whereNotNull('artw_tickets.pausado_em')
                ->where('modulo', 1)
                ->where(function ($query) {
                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_tickets.deleted_at', null);
                    });

                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_equipe_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_equipe_tickets.deleted_at', null);
                    });
                })
                ->groupBy('artw_tickets.id')
                ->get();

            $dados['Atrasados']['numero'] = $dados['Tickets']['numero'] - count($pausados) - $dados['No Prazo']['numero'];
            $dados['Atrasados']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['Atrasados']['numero'];

            $dados['Pausados']['numero'] = count($pausados);
            $dados['Pausados']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['Pausados']['numero'];
        }

        DB::beginTransaction();

        $relMes = RelMeses::firstOrNew([
            'id_tipo' => $tipo->id,
            'id_usuario' => Auth::user()->id,
            'mes' => $now->format('m'),
            'ano' => $now->format('Y')
        ]);

        $relMes->updated_at = date('Y-m-d H:i:s');
        $relMes->status = 1;
        $relMes->save();

        $relMes->numeros()->delete();

        foreach ($dados as $nome=>$numero)
        {
            $relMesNumero = RelMesesNumeros::firstOrNew([
                'nome' => $nome,
                'id_mes' => $relMes->id
            ]);

            $relMesNumero->numero = $numero['numero'];
            $relMesNumero->porcentagem = $numero['porcentagem'];
            $relMesNumero->save();
        }

        DB::commit();
    }

    public function minhasFuncoesTicketsAndamentoAtrasado()
    {
        $tipo = RelTipos::where('slug', str_slug(snake_case(__FUNCTION__)))->get()->first();

        $now = Carbon::now();

        $query = ArtwTickets
            ::join('artw_itens', 'artw_itens.id', '=', 'artw_tickets.id_item')
            ->leftJoin('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
            ->leftJoin('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
            ->whereIn('id_status', [0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16])
            ->where('numero', 'not like', '10%')
            ->whereNull('artw_tickets.cancelado_em')
            ->where('modulo', 1)
            ->where('artw_tickets.id_usuario', Auth::user()->id)
            ->where('dt_fim', '<=', Carbon::now())
            ->where(function ($query) {
                $query->orwhere(function($query) {
                    $query->where('artw_perfis_usuarios_tickets.id_usuario', Auth::user()->id);
                    $query->where('artw_perfis_usuarios_tickets.deleted_at', null);
                });

                $query->orwhere(function($query) {
                    $query->where('artw_perfis_usuarios_equipe_tickets.id_usuario', Auth::user()->id);
                    $query->where('artw_perfis_usuarios_equipe_tickets.deleted_at', null);
                });
            })
            ->groupBy('artw_tickets.id')
            ->get();

        $dados['Líder']['numero'] = count($query);
        $dados['Líder']['porcentagem'] = 100;
            
        if ($dados['Líder']['numero'] > 0)
        {
            $query = ArtwTickets
                ::join('artw_itens', 'artw_itens.id', '=', 'artw_tickets.id_item')
                ->leftJoin('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
                ->leftJoin('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
                ->whereIn('id_status', [0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16])
                ->where('numero', 'not like', '10%')
                ->whereNull('artw_tickets.cancelado_em')
                ->whereNull('artw_tickets.pausado_em')
                ->where('modulo', 1)
                ->where('artw_tickets.id_usuario', Auth::user()->id)
                ->where('dt_fim', '<=', Carbon::now())
                ->where('artw_perfis_usuarios_tickets.id_perfil', 5)
                ->where(function ($query) {
                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_tickets.deleted_at', null);
                    });

                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_equipe_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_equipe_tickets.deleted_at', null);
                    });
                })
                ->groupBy('artw_tickets.id')
                ->get();

            $dados['Edição e Finalização']['numero'] = count($query);
            $dados['Edição e Finalização']['porcentagem'] = (100 / $dados['Líder']['numero']) * $dados['Edição e Finalização']['numero'];

            $query = ArtwTickets
                ::join('artw_itens', 'artw_itens.id', '=', 'artw_tickets.id_item')
                ->leftJoin('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
                ->leftJoin('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
                ->whereIn('id_status', [0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16])
                ->where('numero', 'not like', '10%')
                ->whereNull('artw_tickets.cancelado_em')
                ->whereNull('artw_tickets.pausado_em')
                ->where('modulo', 1)
                ->where('artw_tickets.id_usuario', Auth::user()->id)
                ->where('dt_fim', '<=', Carbon::now())
                ->where('artw_perfis_usuarios_tickets.id_perfil', 3)
                ->where(function ($query) {
                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_tickets.deleted_at', null);
                    });

                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_equipe_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_equipe_tickets.deleted_at', null);
                    });
                })
                ->groupBy('artw_tickets.id')
                ->get();

            $dados['Revisão']['numero'] = count($query);
            $dados['Revisão']['porcentagem'] = (100 / $dados['Líder']['numero']) * $dados['Revisão']['numero'];

            $query = ArtwTickets
                ::join('artw_itens', 'artw_itens.id', '=', 'artw_tickets.id_item')
                ->leftJoin('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
                ->leftJoin('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
                ->whereIn('id_status', [0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16])
                ->where('numero', 'not like', '10%')
                ->whereNull('artw_tickets.cancelado_em')
                ->whereNull('artw_tickets.pausado_em')
                ->where('modulo', 1)
                ->where('artw_tickets.id_usuario', Auth::user()->id)
                ->where('dt_fim', '<=', Carbon::now())
                ->whereIn('artw_perfis_usuarios_tickets.id_perfil', [1,2])
                ->where(function ($query) {
                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_tickets.deleted_at', null);
                    });

                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_equipe_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_equipe_tickets.deleted_at', null);
                    });
                })
                ->groupBy('artw_tickets.id')
                ->get();

            $dados['Aprovações']['numero'] = count($query);
            $dados['Aprovações']['porcentagem'] = (100 / $dados['Líder']['numero']) * $dados['Aprovações']['numero'];

            $query = ArtwTickets
                ::join('artw_itens', 'artw_itens.id', '=', 'artw_tickets.id_item')
                ->leftJoin('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
                ->leftJoin('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
                ->whereIn('id_status', [0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16])
                ->where('numero', 'not like', '10%')
                ->whereNull('artw_tickets.cancelado_em')
                ->whereNull('artw_tickets.pausado_em')
                ->where('modulo', 1)
                ->where('artw_tickets.id_usuario', Auth::user()->id)
                ->where('dt_fim', '<=', Carbon::now())
                ->where('artw_perfis_usuarios_tickets.id_perfil', 6)
                ->where(function ($query) {
                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_tickets.deleted_at', null);
                    });

                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_equipe_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_equipe_tickets.deleted_at', null);
                    });
                })
                ->groupBy('artw_tickets.id')
                ->get();

            $dados['Envio da Arte']['numero'] = count($query);
            $dados['Envio da Arte']['porcentagem'] = (100 / $dados['Líder']['numero']) * $dados['Envio da Arte']['numero'];

            $query = ArtwTickets
                ::join('artw_itens', 'artw_itens.id', '=', 'artw_tickets.id_item')
                ->leftJoin('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
                ->leftJoin('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
                ->whereIn('id_status', [0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16])
                ->where('numero', 'not like', '10%')
                ->whereNull('artw_tickets.cancelado_em')
                ->whereNull('artw_tickets.pausado_em')
                ->where('modulo', 1)
                ->where('artw_tickets.id_usuario', Auth::user()->id)
                ->where('dt_fim', '<=', Carbon::now())
                ->where('artw_perfis_usuarios_tickets.id_perfil', 7)
                ->where(function ($query) {
                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_tickets.deleted_at', null);
                    });

                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_equipe_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_equipe_tickets.deleted_at', null);
                    });
                })
                ->groupBy('artw_tickets.id')
                ->get();

            $dados['PDF do Fornecedor']['numero'] = count($query);
            $dados['PDF do Fornecedor']['porcentagem'] = (100 / $dados['Líder']['numero']) * $dados['PDF do Fornecedor']['numero'];
        }

        DB::beginTransaction();

        $relMes = RelMeses::firstOrNew([
            'id_tipo' => $tipo->id,
            'id_usuario' => Auth::user()->id,
            'mes' => $now->format('m'),
            'ano' => $now->format('Y')
        ]);

        $relMes->updated_at = date('Y-m-d H:i:s');
        $relMes->status = 1;
        $relMes->save();

        $relMes->numeros()->delete();

        foreach ($dados as $nome=>$numero)
        {
            $relMesNumero = RelMesesNumeros::firstOrNew([
                'nome' => $nome,
                'id_mes' => $relMes->id
            ]);

            $relMesNumero->numero = $numero['numero'];
            $relMesNumero->porcentagem = $numero['porcentagem'];
            $relMesNumero->save();
        }

        DB::commit();
    }

    public function ticketsEncerradosPorCicloUsuario()
    {
        $tipo = RelTipos::where('slug', str_slug(snake_case(__FUNCTION__)))->get()->first();

        $now = Carbon::now();
        //$now->modify('-10 minutes');

        $dados = [];

        $total = ArtwTickets
            ::select('artw_tickets.id')
            ->distinct()
            ->join('artw_itens', 'artw_itens.id', '=', 'artw_tickets.id_item')
            ->leftJoin('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
            ->leftJoin('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
            ->where('id_status', 6)
            ->where(function ($query) {
                $query->orwhere(function($query) {
                    $query->where('artw_perfis_usuarios_tickets.id_usuario', Auth::user()->id);
                    $query->where('artw_perfis_usuarios_tickets.deleted_at', null);
                });

                $query->orwhere(function($query) {
                    $query->where('artw_perfis_usuarios_equipe_tickets.id_usuario', Auth::user()->id);
                    $query->where('artw_perfis_usuarios_equipe_tickets.deleted_at', null);
                });
            })
            ->whereMonth('artw_tickets.updated_at', '=', $now->format('m'))
            ->whereYear('artw_tickets.updated_at', '=', $now->format('Y'))
            ->where('numero', 'not like', '10%')
            ->where('modulo', 1)
            ->groupBy('artw_tickets.id')
            ->get();

        $total = count($total);

        if ($total > 0)
        {
            $dados['Tickets']['numero'] = $total;
            $dados['Tickets']['porcentagem'] = 100;

            $dados['Ciclo 1']['numero'] = ArtwTickets
                ::leftJoin('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
                ->leftJoin('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
                ->where(function ($query) {
                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_tickets.deleted_at', null);
                    });
    
                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_equipe_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_equipe_tickets.deleted_at', null);
                    });
                })
                ->where('id_status', 6)
                ->where('artw_tickets.ciclo', 1)
                ->whereMonth('artw_tickets.updated_at', '=', $now->format('m'))
                ->whereYear('artw_tickets.updated_at', '=', $now->format('Y'))
                ->where('numero', 'not like', '10%')
                ->where('modulo', 1)
                ->groupBy('artw_tickets.id')
                ->get()->count();
            $dados['Ciclo 1']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['Ciclo 1']['numero'];

            $dados['Ciclo 2']['numero'] = ArtwTickets
                ::leftJoin('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
                ->leftJoin('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
                ->where(function ($query) {
                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_tickets.deleted_at', null);
                    });

                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_equipe_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_equipe_tickets.deleted_at', null);
                    });
                })
                ->where('id_status', 6)
                ->where('artw_tickets.ciclo', 2)
                ->whereMonth('artw_tickets.updated_at', '=', $now->format('m'))
                ->whereYear('artw_tickets.updated_at', '=', $now->format('Y'))
                ->where('numero', 'not like', '10%')
                ->where('modulo', 1)
                ->groupBy('artw_tickets.id')
                ->get()->count();
            $dados['Ciclo 2']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['Ciclo 2']['numero'];

            $dados['Ciclo 3']['numero'] = ArtwTickets
                ::leftJoin('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
                ->leftJoin('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
                ->where(function ($query) {
                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_tickets.deleted_at', null);
                    });

                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_equipe_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_equipe_tickets.deleted_at', null);
                    });
                })
                ->where('id_status', 6)
                ->where('artw_tickets.ciclo', 3)
                ->whereMonth('artw_tickets.updated_at', '=', $now->format('m'))
                ->whereYear('artw_tickets.updated_at', '=', $now->format('Y'))
                ->where('numero', 'not like', '10%')
                ->where('modulo', 1)
                ->groupBy('artw_tickets.id')
                ->get()->count();
            $dados['Ciclo 3']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['Ciclo 3']['numero'];

            $dados['Ciclo 4']['numero'] = ArtwTickets
                ::leftJoin('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
                ->leftJoin('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
                ->where(function ($query) {
                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_tickets.deleted_at', null);
                    });

                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_equipe_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_equipe_tickets.deleted_at', null);
                    });
                })
                ->where('id_status', 6)
                ->where('artw_tickets.ciclo', 4)
                ->whereMonth('artw_tickets.updated_at', '=', $now->format('m'))
                ->whereYear('artw_tickets.updated_at', '=', $now->format('Y'))
                ->where('numero', 'not like', '10%')
                ->where('modulo', 1)
                ->groupBy('artw_tickets.id')
                ->get()->count();
            $dados['Ciclo 4']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['Ciclo 4']['numero'];

            $dados['Ciclo 5+']['numero'] = ArtwTickets
                ::leftJoin('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
                ->leftJoin('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
                ->where(function ($query) {
                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_tickets.deleted_at', null);
                    });

                    $query->orwhere(function($query) {
                        $query->where('artw_perfis_usuarios_equipe_tickets.id_usuario', Auth::user()->id);
                        $query->where('artw_perfis_usuarios_equipe_tickets.deleted_at', null);
                    });
                })
                ->where('id_status', 6)
                ->where('artw_tickets.ciclo', '>=', 5)
                ->whereMonth('artw_tickets.updated_at', '=', $now->format('m'))
                ->whereYear('artw_tickets.updated_at', '=', $now->format('Y'))
                ->where('numero', 'not like', '10%')
                ->where('modulo', 1)
                ->groupBy('artw_tickets.id')
                ->get()->count();
            $dados['Ciclo 5+']['porcentagem'] = (100 / $dados['Tickets']['numero']) * $dados['Ciclo 5+']['numero'];
        }

        DB::beginTransaction();

        $relMes = RelMeses::firstOrNew([
            'id_tipo' => $tipo->id,
            'id_usuario' => Auth::user()->id,
            'mes' => $now->format('m'),
            'ano' => $now->format('Y')
        ]);

        $relMes->updated_at = date('Y-m-d H:i:s');
        $relMes->status = 1;
        $relMes->save();

        $relMes->numeros()->delete();

        foreach ($dados as $nome=>$numero)
        {
            $relMesNumero = RelMesesNumeros::firstOrNew([
                'nome' => $nome,
                'id_mes' => $relMes->id
            ]);

            $relMesNumero->numero = $numero['numero'];
            $relMesNumero->porcentagem = $numero['porcentagem'];
            $relMesNumero->save();
        }

        DB::commit();
    }

}
