<?php

namespace App\Components;

use App\Produtos;
use App\Versoes;
use App\TiposProporcoesVersoes;
use App\CategoriasVersoes;
use App\Components\Common;
use Illuminate\Support\Facades\File;

use App\Http\Requests;

class ProductVersions
{
    private $versao;

    public function __construct(Versoes $versao)
    {
        $this->versao = $versao;
    }

    public function getVersion($id)
    {
        $produto = Produtos::find($id);
        $categoriasVersoes = CategoriasVersoes::all();
        $tiposProporcoesVersoes = TiposProporcoesVersoes::where('id', '<>', 1)->get();
        $versoes = array();

        foreach ($categoriasVersoes as $value) {
            $categorias[$value->id] = $value->nome;
        }

        foreach ($tiposProporcoesVersoes as $value) {
            $tipos[$value->id] = $value->nome;
        }

        foreach ($produto->versoes as $versao) {
            $versao->descricao = substr($versao->descricao, 0, 150);
            $versoes[] = $versao;
        }

        $produto->versoes = $versoes;

		return view('admin.produtos.versoes', compact('produto', 'categoriasVersoes', 'tiposProporcoesVersoes', 'categorias', 'tipos'));
    }

    public function create($request)
    {

        if(empty($request->id_categoria_versao) || empty($request->id_tipo_proporcao_versao)){
            return false;
        }

        $version = $this->versao;
        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;
        $file = isset($dados['file']) ? $dados['file'] : null;
        $imagePath = '/images/uploads/versoes/';
        $filePath = '/files/uploads/versoes/';

        // Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $image];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

        // Validação de Upload de Arquivo
        $data = ['file' => $file];
        $rules = ['file' => 'mimes:zip'];
        $message = 'Formato de arquivo invalido. Utilize um arquivo ZIP';
        $validate->validaUpload($data, $rules, $message);

        $produto = Produtos::find($dados['id_prod']);

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;
        }

        if(!is_null($file)){
            $extension = $file->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $fileName = $hash.".".$extension;
            $destinationPath = storage_path().$filePath;
            $upload_success = $file->move($destinationPath, $fileName);
            $dados['path_down'] = $filePath.$fileName;
        }

        $produto->versoes()->attach($version->create($dados));
        $log = new Common;
        // Log nova versão em Produto - 1 => Módulo Guide
        $log->setFavoritesLogs(1, $produto->id, 'CREATE', 'Nova versão de produto', 'versoes');
        die;
    }

    public function edit($id)
    {
        $editVersion = $this->versao->find($id);
        return response(compact('editVersion'));
    }

    public function update($request)
    {
        if(empty($request->id_categoria_versao) || empty($request->id_tipo_proporcao_versao)){
            return false;
        }

        $version = $this->versao->find($request->id);
        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;
        $file = isset($dados['file']) ? $dados['file'] : null;
        $imagePath = '/images/uploads/versoes/';
        $filePath = '/files/uploads/versoes/';

        // Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $image];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

        // Validação de Upload de Arquivo
        $data = ['file' => $file];
        $rules = ['file' => 'mimes:zip'];
        $message = 'Formato de arquivo invalido. Utilize um arquivo ZIP';
        $validate->validaUpload($data, $rules, $message);

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;

            if(File::isFile(public_path().$version->thumb)){
                File::delete(public_path().$version->thumb);
            }
        }

        if(!is_null($file)){
            $extension = $file->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $fileName = $hash.".".$extension;
            $destinationPath = storage_path().$filePath;
            $upload_success = $file->move($destinationPath, $fileName);
            $dados['path_down'] = $filePath.$fileName;

            if(File::isFile(storage_path().$version->path_down)){
                File::delete(storage_path().$version->path_down);
            }
        }

        if($version->update($dados)){
            $log = new Common;
            // Log update versão em Produto - 1 => Módulo Guide
            $log->setFavoritesLogs(1, $version->produtos->first()->id, 'UPDATE', 'Atualização de versão de produto', 'versoes');
        }
        die;
    }

    public function remove($id)
    {
        $removeVersion = $this->versao->find($id);
        $prodId = $removeVersion->produtos->first()->id;

        if(File::isFile(public_path().$removeVersion->thumb)){
            File::delete(public_path().$removeVersion->thumb);
        }

        if(File::isFile(storage_path().$removeVersion->path_down)){
            File::delete(storage_path().$removeVersion->path_down);
        }

        $removeVersion->produtos()->detach();
        if($removeVersion->delete()){
            $log = new Common;
            // Log remoção de versão em Produto - 1 => Módulo Guide
            $log->setFavoritesLogs(1, $prodId, 'DELETE', 'Remoção de versão de produto', 'versoes');
        }
    }

}
