<?php

namespace App\Components;

use App\Produtos;
use App\Enxovais;
use App\Components\Common;
use Illuminate\Support\Facades\File;

use App\Http\Requests;

class ProductOutfit
{
    private $enxoval;

    public function __construct(Enxovais $enxoval)
    {
        $this->enxoval = $enxoval;
    }

    public function getOutfit($id)
    {
        $produto = Produtos::find($id);

		return view('admin.produtos.enxovais', compact('produto'));
    }

    public function create($request)
    {
        if(!isset($request->nome)){
            return false;
        }

        $tmpEnxoval = $this->enxoval;
        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;
        $file = isset($dados['file']) ? $dados['file'] : null;
        $imagePath = '/images/uploads/enxovais/';
        $filePath = '/files/uploads/enxovais/';

        // Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $image];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

        // Validação de Upload
        $data = ['file' => $file];
        $rules = ['file' => 'mimes:zip'];
        $message = 'Formato de arquivo invalido';
        $validate->validaUpload($data, $rules, $message);

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;
        }

        if(!is_null($file)){
            $extension = $file->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $fileName = $hash.".".$extension;
            $destinationPath = storage_path().$filePath;
            $upload_success = $file->move($destinationPath, $fileName);
            $dados['path_down'] = $filePath.$fileName;
        }

        $enxoval = $tmpEnxoval->create($dados);

        if($enxoval){
            $log = new Common;
            // Log novo enxoval em Produto - 4 => Módulo Enxoval
            $log->setFavoritesLogs(4, $enxoval->id_produto, 'CREATE', 'Novo enxoval de produto', 'enxovais');
        }
        die;
    }

    public function edit($id)
    {
        $editEnxoval = $this->enxoval->find($id);
        return response(compact('editEnxoval'));
    }

    public function update($request)
    {
        if(!isset($request->nome)){
            return false;
        }

        $tmpEnxoval = $this->enxoval->find($request->id);
        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;
        $file = isset($dados['file']) ? $dados['file'] : null;
        $imagePath = '/images/uploads/enxovais/';
        $filePath = '/files/uploads/enxovais/';

        // Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $image];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

        // Validação de Upload
        $data = ['file' => $file];
        $rules = ['file' => 'mimes:zip'];
        $message = 'Formato de arquivo invalido';
        $validate->validaUpload($data, $rules, $message);

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;

            if(File::isFile(public_path().$tmpEnxoval->thumb)){
                File::delete(public_path().$tmpEnxoval->thumb);
            }
        }

        if(!is_null($file)){
            $extension = $file->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $fileName = $hash.".".$extension;
            $destinationPath = storage_path().$path;
            $upload_success = $file->move($destinationPath, $fileName);
            $dados['path_down'] = $path.$fileName;

            if(File::isFile(storage_path().$tmpEnxoval->path_down)){
                File::delete(storage_path().$tmpEnxoval->path_down);
            }
        }

        if($tmpEnxoval->update($dados)){
            $log = new Common;
            // Log update enxoval em Produto - 4 => Módulo Enxoval
            $log->setFavoritesLogs(4, $tmpEnxoval->id_produto, 'UPDATE', 'Atualização de enxoval de produto', 'enxovais');
        }
        die;
    }

    public function remove($id)
    {
        $tmpEnxoval = $this->enxoval->find($id);
        $prodId = $tmpEnxoval->id_produto;

        if(File::isFile(public_path().$tmpEnxoval->thumb)){
            File::delete(public_path().$tmpEnxoval->thumb);
        }

        if(File::isFile(storage_path().$tmpEnxoval->path_down)){
            File::delete(storage_path().$tmpEnxoval->path_down);
        }

        if($tmpEnxoval->delete()){
            $log = new Common;
            // Log remoção de enxoval em Produto - 4 => Módulo Enxoval
            $log->setFavoritesLogs(4, $prodId, 'DELETE', 'Remoção de enxoval de produto', 'enxovais');
        }
    }

}
