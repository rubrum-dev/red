<?php

namespace App\Components;

use App\Produtos;
use App\UsosAplicacoes;
use App\TiposUsosAplicacoes;
use App\Components\Common;
use Illuminate\Support\Facades\File;

use App\Http\Requests;

class ProductApplications
{
    private $uso;

    public function __construct(UsosAplicacoes $uso)
    {
        $this->uso = $uso;
    }

    public function getApplication($id)
    {
        $produto = Produtos::find($id);
        $tiposUsosAplicacoes = TiposUsosAplicacoes::all();
        $aplicacoes = array();

        foreach ($tiposUsosAplicacoes as $value) {
            $tipos[$value->id] = $value->nome;
        }

        foreach ($produto->usosAplicacoes as $aplicacao) {
            $aplicacao->descricao = substr($aplicacao->descricao, 0, 150);
            $aplicacoes[] = $aplicacao;
        }

        $produto->usosAplicacoes = $aplicacoes;

		return view('admin.produtos.usos-aplicacoes', compact('produto', 'tiposUsosAplicacoes', 'tipos'));
    }

    public function create($request)
    {
        if(empty($request->id_tipo_uso_aplicacao)){
            return false;
        }

        $uso = $this->uso;
        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;

        // Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $image];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

        $produto = Produtos::find($dados['id_prod']);

        $imagePath = '/images/uploads/usos-aplicacoes/';

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;
        }

        $produto->usosAplicacoes()->attach($uso->create($dados));
        $log = new Common;
        // Log nova aplicação em Produto - 1 => Módulo Guide
        $log->setFavoritesLogs(1, $produto->id, 'CREATE', 'Nova aplicação de produto', 'usos-aplicacoes');
        die;
    }

    public function edit($id)
    {
        $editApplication = $this->uso->find($id);
        return response(compact('editApplication'));
    }

    public function update($request)
    {
        if(empty($request->id_tipo_uso_aplicacao)){
            return false;
        }

        $uso = $this->uso->find($request->id);
        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;

        // Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $image];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

        $produto = Produtos::find($dados['id_prod']);

        $imagePath = '/images/uploads/usos-aplicacoes/';

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;

            if(File::isFile(public_path().$uso->thumb)){
                File::delete(public_path().$uso->thumb);
            }
        }

        if($uso->update($dados)){
            $log = new Common;
            // Log update aplicação em Produto - 1 => Módulo Guide
            $log->setFavoritesLogs(1, $uso->produtos->first()->id, 'UPDATE', 'Atualização de aplicação de produto', 'usos-aplicacoes');
        }
        die;
    }

    public function remove($id)
    {
        $removeApplication = $this->uso->find($id);
        $prodId = $removeApplication->produtos->first()->id;

        if(File::isFile(public_path().$removeApplication->thumb)){
            File::delete(public_path().$removeApplication->thumb);
        }

        $removeApplication->produtos()->detach();
        if($removeApplication->delete()){
            $log = new Common;
            // Log remoção de aplicação em Produto - 1 => Módulo Guide
            $log->setFavoritesLogs(1, $prodId, 'DELETE', 'Remoção de versão de produto', 'versoes');
        }
    }

}
