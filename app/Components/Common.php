<?php
namespace App\Components;

use App\ArtwAprovacoes;
use Validator;
use App\Produtos;
use App\AdmUsuarios;
use App\Logotipos;
use App\UsosIncorretos;
use App\UsosAplicacoes;
use App\Versoes;
use App\Proporcoes;
use App\Icones;
use App\Skus;
use App\Familias;
use App\AdmLogNovidades;
use App\ArtwCiclos;
use App\ArtwComentarios;
use App\ArtwRespostas;
use App\ArtwSolicitacoesStatus;
use App\ArtwNotificacoes;
use App\ArtwNotificacoesTipos;
use App\ArtwTickets;
use App\ArtwTicketsStatus;
use App\ArtwVariacoes;
use App\ArtwItemTipos;
use App\ArtwPerfisUsuariosEquipeTickets;
use App\ArtwPerfisUsuariosTickets;
use App\ArtwTktStatus;
use App\Configuracoes;
use Illuminate\Support\Facades\File;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Common
{

    public function paginate($value)
    {
        $perPage = 25;
        $collection = new Collection($value);
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageSearchResults = $collection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $results = new LengthAwarePaginator($currentPageSearchResults, count($collection), $perPage);
        $results->setPath('');

        return $results;
    }

    public function getConfiguracao($nome)
    {
        return Configuracoes::where('nome', $nome)->get()->first()->valor;
    }

    public function getUsuariosValidos()
    {
        return $usuarios = AdmUsuarios
        ::orderBy('nome')
        ->orderBy('sobrenome')
        ->where('id', '!=', 1)
        //->where('status', 1)
        ->get();
    }

    public function atualizaDashboard($tipo)
    {
        $tipo = 'atualiza-dashboard-' . $tipo;

        $function = camel_case($tipo);

        \App::call('App\Components\Common@atualizaDashboardTicketsEmAndamento');
    }

    public function atualizaDashboardTicketsEmAndamento()
    {
        $tipo = 'atualizaDashboardTicketsEmAndamento';

        dd($tipo);
    }

    public function ticketAtualizaTimeline(ArtwTickets $ticket)
    {
        if (!$ticket->ciclo)
        {
            $ticket->ciclo = $ticket->ciclo_atual()->numero;
        }

        $timeline = [];

        $demais_aprovacoes = $ticket->participantesPorPerfil(1)->count();
        $primeira_aprovacao = $ticket->participantesPorPerfil(2)->count();
        $revisao = $ticket->participantesPorPerfil(3)->count();
        $edicao_finalizacao = $ticket->participantesPorPerfil(5)->count();
        $envio_arte = $ticket->participantesPorPerfil(6)->count();
        $pdf_fornecedor = $ticket->participantesPorPerfil(7)->count();

        // Aberto
        $class = '';

        if ($ticket->id_status != 9) {
            $class = 'dropdown-toggle current';
        }

        $timeline['aberto'] = [
            'id' => 'aberto',
            'nome' => 'Aberto',
            'iconClass' => 'fa-flag',
            'class' => $class
        ];

        // Edição
        $class = '';

        if ($ticket->id_status >= 1 && $ticket->id_status != 9 && $edicao_finalizacao) {
            $class = 'dropdown-toggle current';
        }

        $timeline['edicao'] = [
            'id' => 'edicao',
            'nome' => 'Edição',
            'iconClass' => 'fa-paint-brush',
            'class' => $class
        ];

        // Revisão
        $class = '';

        if ($ticket->id_status >= 2 && $ticket->id_status != 9 && $revisao) {
            $class = 'dropdown-toggle current';
        }

        if ($ticket->id_status < 2 && $ticket->id_status != 9 && $revisao) {
            $class = 'dropdown-toggle enabled';
        }

        $timeline['revisao'] = [
            'id' => 'revisao',
            'nome' => 'Revisão',
            'iconClass' => 'fa-eye',
            'class' => $class
        ];

        // Primeira Aprovação
        if ($primeira_aprovacao) {

            $class = '';

            if ($ticket->id_status >= 3 && $ticket->id_status != 9 && $primeira_aprovacao) {
                $class = 'dropdown-toggle current';
            }
    
            if ($ticket->id_status < 3 && $ticket->id_status != 9 && $primeira_aprovacao) {
                $class = 'dropdown-toggle enabled';
            }
            
            $timeline['primeira-aprovacao'] = [
                'id' => 'primeira-aprovacao',
                'nome' => 'Primeira Aprovação',
                'iconClass' => 'fa-thumbs-up',
                'class' => $class
            ];

        }

        // Demais Aprovações
        $class = '';

        if ($ticket->id_status >= 4 && $ticket->id_status != 9 && $demais_aprovacoes) {
            $class = 'dropdown-toggle current';
        }

        if ($ticket->id_status < 4 && $ticket->id_status != 9 && $demais_aprovacoes) {
            $class = 'dropdown-toggle enabled';
        }

        $timeline['demais-aprovacoes'] = [
            'id' => 'demais-aprovacoes',
            'nome' => 'Demais Aprovações',
            'iconClass' => 'fa-gavel',
            'class' => $class
        ];

        // Finalização
        $class = '';

        if ($ticket->id_status >= 5 && $ticket->id_status != 9 && $edicao_finalizacao) {
            $class = 'dropdown-toggle current';
        }

        if ($ticket->id_status < 5 && $ticket->id_status != 9 && $edicao_finalizacao) {
            $class = 'dropdown-toggle enabled';
        }

        $timeline['finalizacao'] = [
            'id' => 'finalizacao',
            'nome' => 'Finalização',
            'iconClass' => 'fa-paint-brush',
            'class' => $class
        ];

        // Envio da Arte
        if ($envio_arte) {

            $class = '';

            if ($ticket->id_status >= 10 && $ticket->id_status != 9 && $envio_arte) {
                $class = 'dropdown-toggle current';
            }
    
            if ($ticket->id_status < 10 && $ticket->id_status != 7 && $ticket->id_status != 8 && $ticket->id_status != 9 && $envio_arte) {
                $class = 'dropdown-toggle enabled';
            }
            
            $timeline['envio-arte'] = [
                'id' => 'envio-arte',
                'nome' => 'Envio da Arte',
                'iconClass' => 'fa-paper-plane',
                'class' => $class
            ];

        }

        // PDF do Fornecedor
        if ($pdf_fornecedor) {

            $class = '';

            if ($ticket->id_status >= 11 && $ticket->id_status != 9 && $pdf_fornecedor) {
                $class = 'dropdown-toggle current';
            }
    
            if ($ticket->id_status < 11 && $ticket->id_status != 7 && $ticket->id_status != 8 && $ticket->id_status != 9 && $pdf_fornecedor) {
                $class = 'dropdown-toggle enabled';
            }
            
            $timeline['pdf-fornecedor'] = [
                'id' => 'pdf-fornecedor',
                'nome' => 'PDF do Fornecedor',
                'iconClass' => 'fa-industry',
                'class' => $class
            ];

        }

        // Encerramento
        $class = '';

        if ($ticket->id_status >= 12 && $ticket->id_status != 9) {
            $class = 'dropdown-toggle current';
        }

        if ($ticket->id_status < 12 && $ticket->id_status != 7 && $ticket->id_status != 8 && $ticket->id_status != 9) {
            $class = 'dropdown-toggle enabled';
        }

        $timeline['encerramento'] = [
            'id' => 'encerramento',
            'nome' => 'Encerramento',
            'iconClass' => 'fa-flag-checkered',
            'class' => $class
        ];

        $ticket->timeline_etapas = json_encode($timeline);
        $ticket->save();

        // Gera o json das subetapas
        if ($ticket->id_status == 3 || $ticket->id_status == 4) {

            $id_perfil = ($ticket->id_status == 3) ? 2 : 1;

            $timeline = [];

            $aprovadores = ArtwPerfisUsuariosTickets
                ::select('artw_perfis_usuarios_tickets.*')
                ->join('adm_usuarios', 'artw_perfis_usuarios_tickets.id_usuario', '=', 'adm_usuarios.id')
                ->where('adm_usuarios.deleted_at', null)
                ->where('id_ticket', $ticket->id)
                ->where('id_perfil', $id_perfil)
                ->where('aprovado', 1)
                ->orderBy('updated_at', 'DESC')
                ->get();

            foreach ($aprovadores as $aprovador) {
                $timeline[] = ($aprovador->aprovado == 2 && $aprovador->ciclo < $ticket->ciclo_atual()->numero) ? 0 : $aprovador->aprovado;
            }

            $aprovadores = ArtwPerfisUsuariosTickets
                ::select('artw_perfis_usuarios_tickets.*')
                ->join('adm_usuarios', 'artw_perfis_usuarios_tickets.id_usuario', '=', 'adm_usuarios.id')
                ->where('adm_usuarios.deleted_at', null)
                ->where('id_ticket', $ticket->id)
                ->where('id_perfil', $id_perfil)
                ->where('aprovado', 2)
                ->orderBy('updated_at', 'DESC')
                ->get();

            foreach ($aprovadores as $aprovador) {
                $timeline[] = ($aprovador->aprovado == 2 && $aprovador->ciclo < $ticket->ciclo_atual()->numero) ? 0 : $aprovador->aprovado;
            }

            $aprovadores = ArtwPerfisUsuariosTickets
                ::select('artw_perfis_usuarios_tickets.*')
                ->join('adm_usuarios', 'artw_perfis_usuarios_tickets.id_usuario', '=', 'adm_usuarios.id')
                ->where('adm_usuarios.deleted_at', null)
                ->where('id_ticket', $ticket->id)
                ->where('id_perfil', $id_perfil)
                ->where('aprovado', 0)
                ->orderBy('updated_at', 'DESC')
                ->get();

            foreach ($aprovadores as $aprovador) {
                $timeline[] = ($aprovador->aprovado == 2 && $aprovador->ciclo < $ticket->ciclo_atual()->numero) ? 0 : $aprovador->aprovado;
            }

            $ticket->timeline_subetapas = json_encode($timeline);
            $ticket->save();

        } else {
            $ticket->timeline_subetapas = null;
            $ticket->save();
        }

    }

    public function reprovaTicket($ticket, $id_perfil, $perfilUsuarioTicket, $perfilUsuarioEquipeTicket, $funcao = 'acao')
    {
        $arrayPerfis = ($id_perfil == 1) ? [2, 3, 5] : [3, 5];
        
        //$outrosPerfis = $ticket->artwPerfisUsuariosTickets->whereIn('id_perfil', $arrayPerfis);
        
        $outrosPerfis = \App\ArtwPerfisUsuariosTickets::withTrashed()->where('id_ticket', $ticket->id)->whereIn('id_perfil', $arrayPerfis)->get();
        
        foreach ($outrosPerfis as $perfil) {
            $perfil->aprovado = 0;
            $perfil->id_membro_aprovador = null;
            $perfil->save();
        }

        if ($perfilUsuarioEquipeTicket && $perfilUsuarioTicket) {

            $perfilUsuarioTicket->id_membro_aprovador = $perfilUsuarioEquipeTicket->id;
        } elseif ($perfilUsuarioTicket) {

            $perfilUsuarioTicket->id_membro_aprovador = null;
        }

        // Teste de Aprovação / Reprovação
        if ($perfilUsuarioTicket && $funcao == 'acao') {
            $perfilUsuarioTicket->aprovado = 1;
            $perfilUsuarioTicket->em_reprovacao = 0;
            $perfilUsuarioTicket->save();
        }

        $ciclo_antigo = $ticket->ciclo_atual();
        $ciclo_antigo->id_status = 3;
        $ciclo_antigo->save();

        $ciclo_novo = ArtwCiclos::firstOrNew(['numero' => $ciclo_antigo->numero + 1, 'id_ticket' => $ticket->id]);
        $ciclo_novo->id_status = 1;
        $ciclo_novo->save();
        
        $ticket->ciclo = $ciclo_novo->numero;
        $ticket->save();
        
        // Volta o status com status = 1 / Em Edição
        $this->changeStatusTicket($ticket, 1);

        // Teste de Aprovação / Reprovação
        // atualiza o ciclo do perfil
        if ($perfilUsuarioTicket && $funcao == 'acao') {
            $perfilUsuarioTicket->ciclo = $ciclo_novo->numero;
            $perfilUsuarioTicket->save();
        }
    }

    public function reprovaTicketAdsmart($ticket, $id_perfil, $perfilUsuarioTicket)
    {
        $ciclo_antigo = $ticket->ciclo_atual();

        $ciclo_novo = ArtwCiclos::firstOrNew(['numero' => $ciclo_antigo->numero + 1, 'id_ticket' => $ticket->id]);

        if (!$ciclo_novo->exists) {
            $ciclo_novo->id_status = 0;
            $ciclo_novo->save();
        }

        $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);

        if (!$perfilUsuarioTicket) {
            return back()->withErrors('Perfil incorreto.');
        }

        $perfilUsuarioTicket->ciclo_aprovacao = $ciclo_antigo->numero;
        $perfilUsuarioTicket->em_reprovacao = 0;
        $perfilUsuarioTicket->em_reprovacao_by = null;
        $perfilUsuarioTicket->aprovado = 1;
        $perfilUsuarioTicket->ciclo = $ciclo_antigo->numero;
        $perfilUsuarioTicket->save();

            // Volta o status com status = 1 / Em Edição

        $ciclo_antigo->id_status = 3;
        $ciclo_antigo->save();

        $ciclo_novo->id_status = 1;
        $ciclo_novo->save();

        $ticket->ciclo = $ciclo_novo->numero;
        $ticket->save();
        
        $this->changeStatusTicket($ticket, 14);
    }

    public function changeStatusTicket($ticket, $id_status)
    {
        $debug = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 2);

        //dd($debug, $debug[1]['class'], $debug[0]['line']);

        $status = ArtwTktStatus::find($id_status);

        if ($ticket->id_status != $id_status) {
            
            $ticketsStatus = new ArtwTicketsStatus();
            $ticketsStatus->id_ticket = $ticket->id;
            $ticketsStatus->id_ciclo = $ticket->ciclo_atual()->id;
            $ticketsStatus->id_usuario = Auth::user()->id;
            $ticketsStatus->id_status = $id_status;
            $ticketsStatus->trace = $debug[1]['class'] . ' - ' . $debug[0]['line'];
            $ticketsStatus->save();

            // Faz envios de notificações
            switch ($id_status) {
                case 1:

                    // envia a versão para renderizacao
                    $versaoAtual = $ticket->artwItem->artwVersoes->last();

                    if ($versaoAtual)
                    {
                        $versaoAtual->indexado = 0;
                        $versaoAtual->save();
                    }

                    // Verifica se é o primeiro ciclo e qual tipo de alteração
                    if ($ticket->ciclo_atual()->numero == 1 && $ticket->nova_embalagem) {
                        
                        $this->enviarNotificacao($ticket, 'primeiro_alteracao', 'arte-finalistas');
                        $this->enviarNotificacao($ticket, 'primeiro_alteracao', 'dono');
                        
                    } elseif ($ticket->ciclo_atual()->numero == 1 && !$ticket->nova_embalagem) {
                        
                        $this->enviarNotificacao($ticket, 'primeiro_embalagem', 'arte-finalistas');
                        $this->enviarNotificacao($ticket, 'primeiro_embalagem', 'dono');
                        
                    } elseif ($ticket->ciclo_atual()->numero > 1) {

                        //$marketingsReprovados = $ticket->participantesReprovados(2, $ticket->ciclo_atual()->numero - 1);
                        //$aprovadoresReprovados = $ticket->participantesReprovados(1, $ticket->ciclo_atual()->numero - 1);

                        if ($ticket->id_status == 3) {
                            $this->enviarNotificacao($ticket, 'reprovado_marketing', 'arte-finalistas');
                            $this->enviarNotificacao($ticket, 'reprovado_marketing', 'revisores');
                            $this->enviarNotificacao($ticket, 'reprovado_marketing', 'seguidores');
                        } elseif ($ticket->id_status == 4) {
                            $this->enviarNotificacao($ticket, 'reprovado_aprovador', 'arte-finalistas');
                            $this->enviarNotificacao($ticket, 'reprovado_aprovador', 'revisores');
                            $this->enviarNotificacao($ticket, 'reprovado_aprovador', 'seguidores');
                        }
                    }
                    break;
                case 2:
                    
                    $this->enviarNotificacao($ticket, 'revisao_tarefa', 'revisores');
                    $this->enviarNotificacao($ticket, 'revisao_tarefa', 'seguidores');
                    break;
                case 3:

                    if ($ticket->dt_fim < date('Y-m-d H:i:s')) {
                        // Tarefa atrasada
                        $this->enviarNotificacao($ticket, 'marketing_tarefa_atrasada', 'marketings', null, 0, null, 2);
                        $this->enviarNotificacao($ticket, 'marketing_tarefa_atrasada', 'seguidores');
                    } else {
                        // Tarefa em dia
                        $this->enviarNotificacao($ticket, 'marketing_tarefa', 'marketings', null, 0, null, 2);
                        $this->enviarNotificacao($ticket, 'marketing_tarefa', 'seguidores');
                    }

                    break;
                case 4:

                    if ($ticket->dt_fim < date('Y-m-d H:i:s')) {
                        // Tarefa atrasada
                        $this->enviarNotificacao($ticket, 'aprovador_tarefa_atrasada', 'aprovadores', null, 0, null, 2);
                        $this->enviarNotificacao($ticket, 'aprovador_tarefa_atrasada', 'seguidores');
                    } else {
                        // Tarefa em dia
                        $this->enviarNotificacao($ticket, 'aprovador_tarefa', 'aprovadores', null, 0, null, 2);
                        $this->enviarNotificacao($ticket, 'aprovador_tarefa', 'seguidores');
                    }

                    break;
                case 5:

                    $this->enviarNotificacao($ticket, 'carregar_arte-final', 'arte-finalistas');
                    $this->enviarNotificacao($ticket, 'ticket_aprovado', 'revisores');
                    $this->enviarNotificacao($ticket, 'ticket_aprovado', 'seguidores');

                    break;
                case 6:
                    
                    $this->enviarNotificacao($ticket, 'ticket_finalizado', 'dono');
                    $this->enviarNotificacao($ticket, 'ticket_finalizado', 'seguidores');
                    
                    break;
                case 10:
                    
                    $qtdEnvioArtes = $ticket->artwPerfisUsuariosTickets()->where('id_perfil', 6)->get()->count();
                    
                    if ($qtdEnvioArtes) {
                        
                        $this->enviarNotificacao($ticket, 'envio_autorizado', 'envio_arte');
                        $this->enviarNotificacao($ticket, 'envio_autorizado', 'seguidores');
                        
                    } else {
                        
                        $qtdEnvioFornecedorPdf = $ticket->artwPerfisUsuariosTickets()->where('id_perfil', 7)->get()->count();
                        
                        if ($qtdEnvioFornecedorPdf) {
                            
                            $this->changeStatusTicket($ticket, 11);
                            $id_status = 11;
                            
                        } else {
                            
                            $this->changeStatusTicket($ticket, 12);
                            $id_status = 12;
                        }
                        
                    }
                    
                    $ticket->envio_autorizado = 1;
                    $ticket->dt_envio_autorizado = date('Y-m-d H:i:s');
                    $ticket->id_envio_autorizado = Auth::user()->id;
                    $ticket->save();
                    
                    break;
                case 11:
                    
                    $qtdEnvioFornecedorPdf = $ticket->artwPerfisUsuariosTickets()->where('id_perfil', 7)->get()->count();
                        
                    if ($qtdEnvioFornecedorPdf) {

                        // Envia Notificação ao PDF do Fornecedor
                        $this->enviarNotificacao($ticket, 'pdf_fornecedor', 'pdf-fornecedor');
                        $this->enviarNotificacao($ticket, 'pdf_fornecedor', 'seguidores');

                    } else {

                        $this->changeStatusTicket($ticket, 12);
                        $id_status = 12;
                    }

                    break;
                case 12:

                    // Envia Notificação ao PDF do Fornecedor
                    $this->enviarNotificacao($ticket, 'finalizar_ticket', 'dono');
                    $this->enviarNotificacao($ticket, 'finalizar_ticket', 'seguidores');

                    break;

                // Primeiro ID de ADSMTART - Anexação, anexar
                case 14:

                    if ($ticket->ciclo_atual()->numero == 1) {

                        // Envia Notificação pra anexação (primeiro ciclo)
                        $this->enviarNotificacao($ticket, 'anexacao_anexar', 'adsmart-anexadores');

                    } elseif ($ticket->ciclo_atual()->numero > 1) {

                        // Envia Notificação pra anexação (próximos ciclos, após ser reprovado)
                        $this->enviarNotificacao($ticket, 'reprovado_anexacao_anexar', 'adsmart-anexadores');

                    }

                    break;

                // Segundo ID de ADSMTART - Revisão, revisar
                case 15:

                    // Envia Notificação pra anexação (primeiro ciclo)
                    $this->enviarNotificacao($ticket, 'revisao_revisar', 'adsmart-revisores');

                    break;

                // Terveiro ID de ADSMTART - Aprovacao, aprovar
                case 16:

                    if ($ticket->dt_fim < date('Y-m-d H:i:s')) {

                        // Envia Notificação pra aprovação (atrasada)
                        $this->enviarNotificacao($ticket, 'aprovacao_aprovar_atrasada', 'adsmart-aprovadores', null, 0, null, 2);

                    } else {

                        // Envia Notificação pra aprovação (em dia)
                        $this->enviarNotificacao($ticket, 'aprovacao_aprovar', 'adsmart-aprovadores', null, 0, null, 2);

                    }

                    break;
            }
        }

        $ticket->id_status = $status->id;
        $ticket->slug_status = $status->slug;
        $ticket->save();

        // Atualiza a timeline
        if ($ticket->modulo == 1)
        {
            $this->ticketAtualizaTimeline($ticket);
        }
        
    }

    public function enviarNotificacao($ticket, $slug, $publico, $texto = null, $permitirDuplicados = 0, $membro = null, $atrasado = 0)
    {
        $tipo = ArtwNotificacoesTipos::where('slug', $slug)->get()->first();

        if (!$tipo) {
            dd('Tipo não encontrado');
        }

        if (is_numeric($publico)) {

            if ($membro) {
                $todos = $ticket->participantesPorCategoria('usuario', $publico, $membro);
            } else {
                $todos = $ticket->participantesPorCategoria('usuario', $publico);
            }

            $participantes = [];

            foreach ($todos as $row) {

                if ($membro && $row->id == $membro) {
                    $participantes[] = $row;
                } elseif (!$membro && $row->id == $publico) {
                    $participantes[] = $row;
                }
            }
        } elseif ($publico == 'dono') {
            $participantes = $ticket->participantesPorCategoria('usuario', $ticket->id_usuario);
        } else {
            $participantes = $ticket->participantesPorCategoria($publico);
        }

        //DB::beginTransaction();

        foreach ($participantes as $participante) {

            if (($atrasado == 1 && $participante->aprovado == 0) || ($atrasado == 2 && ($participante->aprovado == 0 || $participante->aprovado == 2) ) || ($atrasado == 3 && ($participante->aprovado == 0 || ($participante->aprovado == 2 && $participante->ciclo < $ticket->ciclo_atual()->numero)) ) || $atrasado == 0) {

                $id_from = (Auth::user()) ? Auth::user()->id : $ticket->id_usuario;
                $id_to = $participante->id;

                //if ($id_from != $id_to) {

                if ($permitirDuplicados) {
                    $notificacao = ArtwNotificacoes::create([
                            'id_tipo' => $tipo->id,
                            'id_ticket' => $ticket->id,
                            'id_from' => $id_from,
                            'id_to' => $id_to,
                            'ciclo' => $ticket->ciclo_atual()->id,
                    ]);
                } else {

                    $notificacao = ArtwNotificacoes::firstOrNew([
                            'id_tipo' => $tipo->id,
                            'id_ticket' => $ticket->id,
                            'id_from' => $id_from,
                            'id_to' => $id_to,
                            'ciclo' => $ticket->ciclo_atual()->id
                    ]);
                }

                if ((!$notificacao->exists || $permitirDuplicados)) {
                    $artwVariacao = $ticket->artwVariacao;
                    $sku = $ticket->sku;

                    /*
                      if ($artwVariacao->campanha) {
                      $nome = ($artwVariacao->nome == 'Original') ? $sku->nome . ' - ' . $artwVariacao->campanha->nome : $sku->nome . ' - ' . $artwVariacao->campanha->nome . ' - ' . $sku->nome_variacao;
                      } else {
                      $nome = ($artwVariacao->nome == 'Original') ? $sku->nome : $sku->nome . ' - ' . $sku->nome_variacao;
                      }
                     * 
                     */

                    $nome = ($ticket->modulo==1) ? $ticket->sku_nome_completo : $ticket->adsmart_nome_completo;

                    $notificacao->conta_notificacao = $tipo->conta_notificacao;
                    $notificacao->assunto = $tipo->assunto . ' - ' . $ticket->numero . ' - ' . $nome;
                    $notificacao->texto = $texto;
                    $notificacao->publico = $publico;
                    $notificacao->id_perfil = $participante->id_perfil;
                    $notificacao->save();
                }
            }
        }

        //DB::commit();
    }

    public function getMyLastTickets($limit = null)
    {
        $tickets = ArtwTickets
            ::select('artw_tickets.*')
            ->join('artw_perfis_usuarios_tickets', 'artw_tickets.id', '=', 'artw_perfis_usuarios_tickets.id_ticket')
            ->where('artw_perfis_usuarios_tickets.id_usuario', Auth::user()->id)
            ->whereIn('id_status', ['0,1,2,3,4,5,6,9'])
            ->where('cancelado_em', null)->where('pausado_em', null)
            ->orderBy('artw_tickets.created_at', 'DESC')
            ->groupBy('artw_tickets.id')
            ->limit($limit)
            ->get();

        return $tickets;
    }

    public function getMyTickets($tipo, $limit = null)
    {
        $tickets = array();

        switch ($tipo) {
            case 'todos':
                $allTickets = ArtwTickets::whereIn('id_status', [0, 1, 2, 3, 4, 5, 9])->where('cancelado_em', null)->where('pausado_em', null)->get();
                break;
            case 'meus':
                $allTickets = ArtwTickets::where('id_usuario', Auth::user()->id)->whereIn('id_status', [0, 1, 2, 3, 4, 5, 9])->where('cancelado_em', null)->where('pausado_em', null)->get();
                break;
            case 'compartilhados':
                $allTickets = ArtwTickets::where('id_usuario', '!=', Auth::user()->id)->whereIn('id_status', [0, 1, 2, 3, 4, 5, 9])->where('cancelado_em', null)->where('pausado_em', null)->get();
                break;
            case 'minhas_tarefas':
                $allTickets = ArtwTickets::whereIn('id_status', [0, 1, 2, 3, 4, 5, 9])->where('cancelado_em', null)->where('pausado_em', null)->get();
                break;
            case 'atrasadas':
                $allTickets = ArtwTickets::where('dt_fim', '<=', date('Y-m-d'))->whereIn('id_status', [0, 1, 2, 3, 4, 5, 9])->where('cancelado_em', null)->where('pausado_em', null)->get();
                break;
            default:
                $allTickets = [];
                break;
        }

        foreach ($allTickets as $ticket) {

            if (count($ticket->artwPerfisUsuariosTickets) > 0) {
                foreach ($ticket->artwPerfisUsuariosTickets as $userTkt) {
                    $ids_membros = [];

                    foreach ($userTkt->artwPerfisUsuariosEquipeTickets as $membro) {
                        $ids_membros[] = $membro->id_usuario;
                    }

                    if ($userTkt->id_usuario == Auth::user()->id || in_array(Auth::user()->id, $ids_membros)) {
                        switch ($userTkt->id_perfil) {
                            case 5:
                                $status_ticket = ($userTkt->aprovado == 0) ? [1, 5] : [];
                                break;
                            case 3:
                                $status_ticket = ($userTkt->aprovado == 0) ? [2, 5] : [];
                                //$status_ticket = [2, 5];
                                break;
                            case 2:
                                $status_ticket = ($userTkt->aprovado == 0) ? [3] : [];
                                //$status_ticket = [3];
                                break;
                            case 1:
                                $status_ticket = ($userTkt->aprovado == 0) ? [4] : [];
                                //$status_ticket = [4];
                                break;
                            default:
                                $status_ticket = [];
                        }

                        if (($tipo != 'minhas_tarefas') || ($tipo == 'minhas_tarefas' && in_array($ticket->id_status, $status_ticket))) {
                            $variacao = $ticket->artwVariacao;

                            $ticketNew = (object) $ticket->toArray();
                            $ticketNew->artwVariacao = (object) $variacao;

                            $ticketNew->nr_tarefas = $ticket->ciclo_atual()->artwSolicitacoes->count();

                            $prazo = date_create($ticket->dt_fim);
                            $ticketNew->prazo = date_format($prazo, 'd/m/Y');
                            $ticketNew->atrasada = ($ticket->dt_fim < date('Y-m-d H-i-s'));

                            /*
                              $ticketNew->nome = $ticket->numero . ' ' .
                              $ticket->sku->produtos->nome . ' ' .
                              $ticket->sku->dimensoes->tiposEmbalagens->nome . ' ' .
                              $ticket->sku->dimensoes->nome;

                             * 
                             */

                            $ticketNew->sku_nome_completo = $ticket->sku_nome_completo;
                            $ticketNew->perfil = $userTkt->artwPerfisUsuario->nome;
                            $ticketNew->id_perfil = $userTkt->artwPerfisUsuario->id_perfil;
                            $ticketNew->status = $ticket->artwTktStatus->nome;

                            $tickets[] = $ticketNew;
                        }
                    }
                }
            }

            if (Auth::user()->gerente == 1 && $ticket->id_status == 0) {
                $variacao = $ticket->artwVariacao;

                $ticketNew = (object) $ticket->toArray();
                $ticketNew->artwVariacao = (object) $variacao;

                $ticketNew->nr_tarefas = $ticket->ciclo_atual()->artwSolicitacoes->count();

                $prazo = date_create($ticket->dt_fim);
                $ticketNew->prazo = date_format($prazo, 'd/m/Y');
                $ticketNew->atrasada = ($ticket->dt_fim < date('Y-m-d H-i-s'));

                /*
                  $ticketNew->nome = $ticket->numero . ' ' .
                  $ticket->sku->produtos->nome . ' ' .
                  $ticket->sku->dimensoes->tiposEmbalagens->nome . ' ' .
                  $ticket->sku->dimensoes->nome;
                 * 
                 */

                $ticketNew->sku_nome_completo = $ticket->sku_nome_completo;
                $ticketNew->perfil = 'Gerente';
                $ticketNew->id_perfil = 0;
                $ticketNew->status = $ticket->artwTktStatus->nome;

                $tickets[] = $ticketNew;
            } elseif (Auth::user()->id === $ticket->id_usuario && $ticket->id_status == 9) {
                $variacao = $ticket->artwVariacao;

                $ticketNew = (object) $ticket->toArray();
                $ticketNew->artwVariacao = (object) $variacao;

                $ticketNew->nr_tarefas = $ticket->ciclo_atual()->artwSolicitacoes->count();

                $prazo = date_create($ticket->dt_fim);
                $ticketNew->prazo = date_format($prazo, 'd/m/Y');
                $ticketNew->atrasada = ($ticket->dt_fim < date('Y-m-d H-i-s'));

                /*
                  $ticketNew->nome = $ticket->numero . ' ' .
                  $ticket->sku->produtos->nome . ' ' .
                  $ticket->sku->dimensoes->tiposEmbalagens->nome . ' ' .
                  $ticket->sku->dimensoes->nome;
                 * 
                 */

                $ticketNew->sku_nome_completo = $ticket->sku_nome_completo;
                $ticketNew->perfil = 'Proprietário';
                $ticketNew->id_perfil = '-1';
                $ticketNew->status = $ticket->artwTktStatus->nome;

                $tickets[] = $ticketNew;
            } elseif (Auth::user()->id == 1 && $ticket->id_status != 8 && $ticket->id_status != 6) {
                $variacao = $ticket->artwVariacao;

                $ticketNew = (object) $ticket->toArray();
                $ticketNew->artwVariacao = (object) $variacao;

                $ticketNew->nr_tarefas = $ticket->ciclo_atual()->artwSolicitacoes->count();

                $prazo = date_create($ticket->dt_fim);
                $ticketNew->prazo = date_format($prazo, 'd/m/Y');
                $ticketNew->atrasada = ($ticket->dt_fim < date('Y-m-d H-i-s'));

                /*
                  $ticketNew->nome = $ticket->numero . ' ' .
                  $ticket->sku->produtos->nome . ' ' .
                  $ticket->sku->dimensoes->tiposEmbalagens->nome . ' ' .
                  $ticket->sku->dimensoes->nome;

                 * 
                 */
                $ticketNew->sku_nome_completo = $ticket->sku_nome_completo;
                $ticketNew->perfil = '(Master)';
                $ticketNew->id_perfil = '-2';
                $ticketNew->status = $ticket->artwTktStatus->nome;

                $tickets[] = $ticketNew;
            }
        }

        $tickets = collect($tickets);

        if ($limit) {
            $tickets = $tickets->slice(0, $limit);
        }

        return $tickets;
    }

    public function ciclo_detalhe(ArtwCiclos $ciclo, $cicloAtual = null)
    {
        $response = [];
        $response['ciclo'] = $ciclo->toArray();

        $ticket = $ciclo->artwTicket;

        if ($ticket->modulo == 1)
        {
            $layout = $ciclo->artwCiclosLayout->last();
            $response['ciclo']['layout'] = ($layout) ? $layout->toArray() : null;

            if ($layout) {

                if (config('app.cloudflow')) {
                    
                    $response['ciclo']['layout']['inspector'] = route('site.artwork.workflow.view', [$layout->id, 'ciclo', $ciclo->artwTicket->id]);
                    
                } else {
                    
                    $response['ciclo']['layout']['inspector'] = null;
                    
                }
                
                $response['ciclo']['layout']['path'] = $layout->path . '?' . $layout->id;
    
                // Determina o path do arquivo
                if ($layout->path) {
    
                    $caminho = $layout->path . '?' . $layout->id;
    
                } else {
    
                    $caminho = route('site.adsmart.abrir', [$layout->id, $layout->nome_arquivo]);
    
                }
    
                $response['ciclo']['layout']['path_original'] = $caminho;
                
                $response['ciclo']['layout']['filesize'] = $layout->filesize;
    
                $response['ciclo']['layout']['baixarPdf'] = ($layout->path) ? route('site.artwork.workflow.baixarPdf', [$layout->id, 'ciclo']) : route('site.adsmart.baixar', [$layout->id, $layout->nome_arquivo]);
    
                $response['ciclo']['layout']['nome_usuario'] = $layout->admUsuario->nome_abreviado;
                $response['ciclo']['layout']['data_criacao'] = $layout->created_at->format('H\hi \d\e d/m/Y');
            }
        }
        else
        {
            $response['ciclo']['layout'] = [];

            $layouts = $ciclo->artwCiclosLayout;

            foreach ($layouts as $k => $layout)
            {
                $response['ciclo']['layout'][$k] = $layout->toArray();

                $response['ciclo']['layout'][$k]['path'] = $layout->path . '?' . $layout->id;

                // Determina o path do arquivo
                if ($layout->path) {

                    $caminho = $layout->path . '?' . $layout->id;

                } else {

                    $caminho = route('site.adsmart.abrir', [$layout->id, $layout->nome_arquivo]);

                }

                $response['ciclo']['layout'][$k]['path_original'] = $caminho;
                
                $response['ciclo']['layout'][$k]['filesize'] = $layout->filesize;

                $response['ciclo']['layout'][$k]['baixarPdf'] = ($layout->path) ? route('site.artwork..workflow.baixarPdf', [$layout->id, 'ciclo']) : route('site.adsmart.baixar', [$layout->id, $layout->nome_arquivo]);


                $response['ciclo']['layout'][$k]['nome_usuario'] = $layout->admUsuario->nome_abreviado;
                $response['ciclo']['layout'][$k]['data_criacao'] = $layout->created_at->format('H\hi \d\e d/m/Y');
                $response['ciclo']['layout'][$k]['numero'] = $k+1;
            }
        }
        
        $response['ciclo']['aprovacoes'] = [];

        $aprovacoes = ArtwAprovacoes
            ::where('id_ticket', $ticket->id)
            ->where('ciclo', $ciclo->numero)
            ->WhereNotNull('descricao')
            ->get();
        
        foreach ($aprovacoes as $k => $aprovacao)
        {
            $response['ciclo']['aprovacoes'][$k]['nome_usuario'] = $aprovacao->admUsuario->nome_abreviado;
            $response['ciclo']['aprovacoes'][$k]['data_criacao'] = $aprovacao->created_at->format('H\hi \d\e d/m/Y');
            $response['ciclo']['aprovacoes'][$k]['descricao'] = $aprovacao->descricao;
        }

        //dd($response['ciclo']['aprovacoes']);

        $response['ciclo']['solicitacoes'] = [];

        $revisadas = 0;

        foreach ($ciclo->artwSolicitacoes as $k => $solicitacao) {
            $response['ciclo']['solicitacoes'][$k] = $solicitacao->toArray();
            $response['ciclo']['solicitacoes'][$k]['descricao'] = $solicitacao->descricao_html;
            $response['ciclo']['solicitacoes'][$k]['descricao_texto'] = $solicitacao->descricao;
            $response['ciclo']['solicitacoes'][$k]['nome_usuario'] = $solicitacao->admUsuario->nome_abreviado;
            $response['ciclo']['solicitacoes'][$k]['tipo'] = ($ticket->modulo == 1) ? $solicitacao->artwSolicitacaoTipo->nome : null;
            $response['ciclo']['solicitacoes'][$k]['condicao'] = ($ticket->modulo == 2) ? $solicitacao->adsmartCondicaoMidia->nome : null;
            $response['ciclo']['solicitacoes'][$k]['icone'] = ($ticket->modulo == 1) ? $solicitacao->artwSolicitacaoTipo->icone : null;
            $response['ciclo']['solicitacoes'][$k]['status'] = $solicitacao->artwSolicitacaoStatus->nome;
            $response['ciclo']['solicitacoes'][$k]['id_tipo'] = ($ticket->modulo==1) ? $solicitacao->id_tipo : $solicitacao->id_condicao_midia;
            

            if ($solicitacao->id_status == 2) {
                $revisadas++;
            }

            $status = ArtwSolicitacoesStatus
                ::where('id_solicitacao', $solicitacao->id)
                ->where('id_status', $solicitacao->id_status)
                ->where('id_ciclo', $ciclo->id)
                ->orderBy('created_at', 'ASC')
                ->get()
                ->last();

            //Posts::where('id','=',2)->with(['Comments' => function($query) {
            //$query->orderBy('CHAVE', 'DESC');
            //}])->get();

            if ($status) {
                $response['ciclo']['solicitacoes'][$k]['status_id_usuario'] = $status->admUsuario->id;
                $response['ciclo']['solicitacoes'][$k]['status_usuario'] = $status->admUsuario->nome_abreviado;
                $response['ciclo']['solicitacoes'][$k]['status_data'] = $status->created_at->format('H\hi \d\e d/m/Y');
                $response['ciclo']['solicitacoes'][$k]['status_data_criada'] = $status->created_at->format('Y-m-d H:i:s');
            } else {
                $response['ciclo']['solicitacoes'][$k]['status_id_usuario'] = '';
                $response['ciclo']['solicitacoes'][$k]['status_usuario'] = '';
                $response['ciclo']['solicitacoes'][$k]['status_data'] = '';
                $response['ciclo']['solicitacoes'][$k]['status_data_criada'] = '';
            }

            $response['ciclo']['solicitacoes'][$k]['data_criacao'] = $solicitacao->created_at->format('H\hi \d\e d/m/Y');

            $response['ciclo']['solicitacoes'][$k]['comentarios'] = [];

            //$comentarios = $solicitacao->artwComentarios;
            $comentarios = ArtwComentarios::where('id_solicitacao', $solicitacao->id)->orderBy('created_at', 'DESC')->get();

            foreach ($comentarios as $i => $comentario) {
                $response['ciclo']['solicitacoes'][$k]['comentarios'][$i] = $comentario->toArray();
                $response['ciclo']['solicitacoes'][$k]['comentarios'][$i]['nome_usuario'] = $comentario->admUsuario->nome_abreviado;
                $response['ciclo']['solicitacoes'][$k]['comentarios'][$i]['data_criacao'] = $comentario->created_at->format('H\hi \d\e d/m/Y');
                $response['ciclo']['solicitacoes'][$k]['comentarios'][$i]['array_envios'] = json_decode($comentario->array_envios);

                $response['ciclo']['solicitacoes'][$k]['comentarios'][$i]['respostas'] = [];

                //$respostas = $comentario->artwRespostas;
                $respostas = ArtwRespostas::where('id_comentario', $comentario->id)->orderBy('created_at', 'ASC')->get();

                foreach ($respostas as $n => $resposta) {
                    $response['ciclo']['solicitacoes'][$k]['comentarios'][$i]['respostas'][$n] = $resposta->toArray();
                    $response['ciclo']['solicitacoes'][$k]['comentarios'][$i]['respostas'][$n]['nome_usuario'] = $resposta->admUsuario->nome_abreviado;
                    $response['ciclo']['solicitacoes'][$k]['comentarios'][$i]['respostas'][$n]['data_criacao'] = $resposta->created_at->format('H\hi \d\e d/m/Y');
                    $response['ciclo']['solicitacoes'][$k]['comentarios'][$i]['respostas'][$n]['array_envios'] = json_decode($resposta->array_envios);
                }
            }
        }

        $datas = [];
        $solicitacoes = $response['ciclo']['solicitacoes'];

        foreach ($solicitacoes as $solicitacao) {
            $datas[] = $solicitacao['status_data_criada'];
        }

        array_multisort($datas, SORT_DESC, $solicitacoes);

        //dd($datas, $response['ciclo']['solicitacoes']);

        $response['ciclo']['solicitacoesOrdem'] = $solicitacoes;

        if (count($ciclo->artwSolicitacoes) == $revisadas) {
            $response['ciclo']['tudoRevisado'] = 1;
        } else {
            $response['ciclo']['tudoRevisado'] = 0;
        }

        return $response['ciclo'];
    }

    public function removeProduct($id)
    {
        $produto = new Produtos;
        $logotipo = new Logotipos;
        $uso = new UsosIncorretos;
        $app = new UsosAplicacoes;
        $version = new Versoes;
        $proportion = new Proporcoes;
        $icon = new Icones;
        $log = new AdmLogNovidades;

        $removeProduct = $produto->find($id);

        $ordemProduto = $removeProduct->ordem;
        $familiaProduto = $removeProduct->id_familia;

        foreach ($removeProduct->enxovais as $enxoval) {
            $tmpEnxoval = $enxoval->find($enxoval->id);

            if (File::isFile(storage_path() . $enxoval->path_down)) {
                File::delete(storage_path() . $enxoval->path_down);
            }

            $tmpEnxoval->delete();
        }

        foreach ($removeProduct->logotipos as $logo) {
            $removeProduct->logotipos()->detach($logo->id);
            $tmpLogo = $logotipo->find($logo->id);

            if (File::isFile(public_path() . $tmpLogo->thumb)) {
                File::delete(public_path() . $tmpLogo->thumb);
            }
            $tmpLogo->delete();
        }

        foreach ($removeProduct->usosIncorretos as $usoIncorreto) {
            $tmpUso = $uso->find($usoIncorreto->id);
            $removeProduct->usosIncorretos()->detach($usoIncorreto->id);

            if (File::isFile(public_path() . $usoIncorreto->thumb)) {
                File::delete(public_path() . $usoIncorreto->thumb);
            }
            $tmpUso->delete();
        }

        foreach ($removeProduct->usosAplicacoes as $usoAplicacao) {
            $tmpApp = $app->find($usoAplicacao->id);
            $removeProduct->usosAplicacoes()->detach($usoAplicacao->id);

            if (File::isFile(public_path() . $usoAplicacao->thumb)) {
                File::delete(public_path() . $usoAplicacao->thumb);
            }
            $tmpApp->delete();
        }

        foreach ($removeProduct->versoes as $versao) {
            $tmpVersion = $version->find($versao->id);
            $removeProduct->versoes()->detach($versao->id);

            if (File::isFile(public_path() . $versao->thumb)) {
                File::delete(public_path() . $versao->thumb);
            }
            if (File::isFile(storage_path() . $versao->path_down)) {
                File::delete(storage_path() . $versao->path_down);
            }
            $tmpVersion->delete();
        }

        foreach ($removeProduct->proporcoes as $proporcao) {
            $tmpProp = $proportion->find($proporcao->id);
            $removeProduct->proporcoes()->detach($proporcao->id);

            if (File::isFile(public_path() . $proporcao->thumb)) {
                File::delete(public_path() . $proporcao->thumb);
            }
            $tmpProp->delete();
        }

        foreach ($removeProduct->icones as $icone) {
            $tmpIcon = $icon->find($icone->id);
            $removeProduct->icones()->detach($icone->id);

            if (File::isFile(public_path() . $icone->thumb)) {
                File::delete(public_path() . $icone->thumb);
            }
            if (File::isFile(storage_path() . $icone->path_down)) {
                File::delete(storage_path() . $icone->path_down);
            }
            $tmpIcon->delete();
        }

        foreach ($removeProduct->admLogNovidades as $logNovidade) {
            $tmpLog = $log->find($logNovidade->id);
            $tmpLog->delete();
        }

        foreach ($removeProduct->skus as $sku) {
            $this->removeSku($sku->id);
        }

        $removeProduct->cores()->detach();
        $removeProduct->tipografias()->detach();
        $removeProduct->delete();

        $reordenarProdutos = $produto->where('id_familia', $familiaProduto)->where('ordem', '>', $ordemProduto)->get();
        foreach ($reordenarProdutos as $value) {
            $novaOrdem = $value->ordem - 1;
            $value->update(['ordem' => $novaOrdem]);
        }
    }

    public function removeSku($id)
    {
        $sku = new Skus;

        $removeSku = $sku->find($id);
        $ordemSku = $removeSku->ordem;

        if (File::isFile(public_path() . $removeSku->thumb)) {
            File::delete(public_path() . $removeSku->thumb);
        }

        foreach ($removeSku->artesFinais as $arteFinal) {
            $tmpArteFinal = $arteFinal->find($arteFinal->id);

            if (File::isFile(public_path() . $arteFinal->thumb)) {
                File::delete(public_path() . $arteFinal->thumb);
            }
            if (File::isFile(storage_path() . $arteFinal->path_cmyk)) {
                File::delete(storage_path() . $arteFinal->path_cmyk);
            }
            if (File::isFile(storage_path() . $arteFinal->path_rgb)) {
                File::delete(storage_path() . $arteFinal->path_rgb);
            }
            $tmpArteFinal->delete();
        }

        $removeSku->delete();

        $reordenarSkus = Skus::join('dimensoes', 'skus.id_dimensao', '=', 'dimensoes.id')
            ->join('tipos_embalagens', 'dimensoes.id_tipo_embalagem', '=', 'tipos_embalagens.id')
            ->where('dimensoes.id_tipo_embalagem', '=', $removeSku->dimensoes->id_tipo_embalagem)
            ->where('skus.ordem', '>', $ordemSku)
            ->select('skus.*')
            ->get();

        foreach ($reordenarSkus as $value) {
            $novaOrdem = $value->ordem - 1;
            $value->update(['ordem' => $novaOrdem]);
        }
    }

    public function removeFamily($id)
    {
        $family = new Familias;
        $logotipo = new Logotipos;
        $removeFamily = $family->find($id);

        $ordemFamilia = $removeFamily->ordem;

        $removeFamily->admPerfis()->detach();

        foreach ($removeFamily->logotipos as $logo) {
            $removeFamily->logotipos()->detach($logo->id);
            $tmpLogo = $logotipo->find($logo->id);
            if (File::isFile(public_path() . $tmpLogo->thumb)) {
                File::delete(public_path() . $tmpLogo->thumb);
            }
            $tmpLogo->delete();
        }

        foreach ($removeFamily->produtos as $produto) {
            $this->removeProduct($produto->id);
        }

        $removeFamily->delete();

        $reordenarFamilias = $family->where('ordem', '>', $ordemFamilia)->get();
        foreach ($reordenarFamilias as $value) {
            $novaOrdem = $value->ordem - 1;
            $value->update(['ordem' => $novaOrdem]);
        }
    }

    public function removeUser($id)
    {
        $usuario = new AdmUsuarios;

        $removeUser = $usuario->find($id);
        $removeUser->admModulos()->detach();
        $removeUser->produtos()->detach();
        $removeUser->delete();
    }

    public function gerarSenha($total_caracteres)
    {
        //$caracteres = 'ABCDEFGHIJKLMNOPQRSTUWXYZ';
        $caracteres = '';
        $caracteres .= 'abcdefghijklmnopqrstuwxyz';
        $caracteres .= '0123456789';
        //$caracteres .= '@#$&*';
        $max = strlen($caracteres) - 1;
        $senha = null;
        for ($i = 0; $i < $total_caracteres; $i++) {
            $senha .= $caracteres{mt_rand(0, $max)};
        }
        return $senha;
    }

    public function setFavoritesLogs($idMod, $idProd, $operacao, $mensagem, $guide = null, $sku = null)
    {
        $log = new AdmLogNovidades;

        $dados = array(
            'id_modulo' => $idMod,
            'id_produto' => $idProd,
            'operacao' => $operacao,
            'guide' => $guide,
            'sku' => $sku,
            'descricao' => $mensagem
        );

        $log->create($dados);

        return true;
    }

    public function validaUpload($file, $rules, $message)
    {
        $validator = Validator::make($file, $rules);

        if ($validator->fails()) {
            header('Content-type: application/json');
            echo json_encode([
                'error' => true,
                'message' => $message
            ]);
            die;
        } else {
            return true;
        }
    }

    public function geraNumeroTicket()
    {
        $ano = date('y');

        $nr_tickets = ArtwTickets
            ::where('numero', 'like', $ano . '%')
            ->where('modulo', 1)
            ->count('*');

        $numero = $ano . str_pad($nr_tickets + 1, 4, "0", STR_PAD_LEFT) . '-E';

        return $numero;
    }

    public function geraNumeroTicketAdsmart()
    {
        $ano = date('y');

        //$nr_tickets = ArtwTickets::whereRaw('year(`created_at`) = ?', [date('Y')])->count('*');

        $nr_tickets = ArtwTickets
            ::where('numero', 'like', $ano . '%')
            ->where('modulo', 2)
            ->count('*');

        $numero = $ano . str_pad($nr_tickets + 1, 4, "0", STR_PAD_LEFT) . '-M';

        return $numero;
    }

    public function geraNumeroTicketFicticio()
    {
        $ano = date('y');

        $nr_tickets = ArtwTickets
            ::where('numero', 'like', '10%')
            ->count('*');

        $numero = '10' . str_pad($nr_tickets + 1, 4, "0", STR_PAD_LEFT);

        return $numero;
    }

    public function getEmbalagens($id_produto, $status)
    {
        $embalagens = ArtwVariacoes::select(
                'artw_variacoes.*', 'artw_variacao_tipos.nome as tipo', 'produtos.nome as produto_nome', 'tipos_embalagens.nome as tipo_embalagem', 'dimensoes.nome as dimensao_nome', 'campanhas.nome as campanha'
            )
            ->distinct()
            ->leftjoin('artw_variacao_itens', 'artw_variacao_itens.id_variacao', '=', 'artw_variacoes.id')
            ->leftjoin('artw_itens', 'artw_variacao_itens.id_item', '=', 'artw_itens.id')
            ->join('skus', 'artw_variacoes.id_sku', '=', 'skus.id')
            ->join('produtos', 'skus.id_produto', '=', 'produtos.id')
            ->join('dimensoes', 'skus.id_dimensao', '=', 'dimensoes.id')
            ->join('tipos_embalagens', 'dimensoes.id_tipo_embalagem', '=', 'tipos_embalagens.id')
            ->leftJoin('campanhas', 'campanhas.id', '=', 'artw_variacoes.id_campanha')
            ->leftJoin('artw_variacao_tipos', 'artw_variacao_tipos.id', '=', 'artw_variacoes.id_tipo')
            ->where('skus.id_produto', $id_produto)
            ->where(function ($query) use ($status) {

                if ($status == 1) {

                    $query->orwhere('artw_itens.status', $status);
                    $query->orwhere('artw_itens.status', 0);
                    $query->orwhere('artw_itens.status', null);
                } else {

                    $query->where('artw_itens.status', $status);
                }
            })
            ->orderBy('produtos.nome')
            ->orderBy('tipos_embalagens.nome')
            ->orderBy('dimensoes.nome')
            ->orderBy('artw_variacoes.nome_original')
            //->withTrashed()
            ->get();

        //dd($embalagens->toSql());

        $n = 0;

        $retorno = [
            'regulares' => [],
            'promocionais' => []
        ];

        foreach ($embalagens as $e => $embalagem) {

            $data = $embalagem->toArray();
            $data['mercados'] = $embalagem->mercados; // to-do
            $data['link_edicao'] = route('site.artwork.package.edit.get', $embalagem->id);

            $nome = '';
            $nome = $embalagem->produto_nome
                . ' ' .
                $embalagem->tipo_embalagem
                . ' ' .
                $embalagem->dimensao_nome;
            if ($embalagem->campanha) {
                $nome .= ' • ' . $embalagem->campanha;
            }
            if ($embalagem->principal !== 1 && $embalagem->nome) {
                $nome .= ' • ' . $embalagem->nome;
            }
            
            // Imagem a partir do SKU
            //$imagem = $embalagem->sku->artesFinais()->where('arte_exibicao', 1)->get()->first();

            // Imagem a partir da Variação
            $imagem = $embalagem->sku->artesFinais()->where('status', 1)->get()->first();

            if ($imagem && $imagem->path_cmyk) {

                $data['imagem_packshelf'] = $imagem->path_cmyk . '?view=1';

            }

            $data['nome_original'] = $nome;
            //$data['nome_original'] = $embalagem->sku_nome_completo;
            $data['tickets_gerais'] = $embalagem->ticketsGerais();

            //$data['thumb'] = $thumb;

            $itens = [];

            $artwItens = \App\ArtwItens::select('artw_itens.*')
                ->join('artw_variacao_itens', 'artw_variacao_itens.id_item', '=', 'artw_itens.id')
                ->join('artw_item_tipos', 'artw_item_tipos.id', '=', 'artw_itens.id_tipo')
                ->where('artw_variacao_itens.id_variacao', $embalagem->id)
                ->orderBy('artw_item_tipos.nome')
                ->where('artw_itens.status', $status)
                ->get();
            
            //dd($status, $embalagem->id, $artwItens);

            foreach ($artwItens as $i => $item) {

                $itens[$i] = $item->toArray();
                $itens[$i]['tipo'] = ($item->artwItemTipo) ? $item->artwItemTipo->toArray() : [];

                $versao = ($item->artwVersoes->last()) ? $item->artwVersoes->last() : null;
                
                if ($versao) {
                    
                    $itens[$i]['versao'] = $versao;
                    $itens[$i]['versao']['path'] = ($item->artwVersoes->last()) ? route('site.artwork.abrirPdfNome', [$versao->id, 'versao', basename(public_path($versao->path))]) : '';
                    
                }

                if ($versao) {
                    $itens[$i]['versao']['inspector'] = route('site.artwork.view', [$versao->id, 'versao']);
                }

                $ticketsAbertos = $item->ticketsAbertosSemCancelados();
                              
                $ticket = null;

                if ($versao)
                {
                    $ticket = $versao->artwTicket;
                
                    if ($ticket->descricao == 'Ticket aberto para auxiliar o compartilhamento.' && $versao->numero == 0) {

                        $verificaTicket = ArtwTickets::where('id_item', $item->id)->where('descricao', 'Ticket aberto para auxiliar o upload manual de arte.')->get()->last();

                        if ($verificaTicket) {

                            $ticket = $verificaTicket;

                        }
                    }
                }
                
                $arteFinal = ($versao) ? $ticket->artwArtes->last() : null;
                
                
                $itens[$i]['nome'] = $item->nomeCompleto;
                
                $ticketAberto = $ticketsAbertos->last();

                $artwPerfisUsuariosEquipeTickets = New ArtwPerfisUsuariosEquipeTickets;

                if (Auth::user() && $ticketAberto && ($ticketAberto->checkUsuario(Auth::user()->id) || $artwPerfisUsuariosEquipeTickets->checkUsuario(Auth::user()->id, $ticketAberto->id) || Auth::user()->admin || Auth::user()->gerente))
                {
                    $linkTicket = route('site.artwork.ticket.edit.step1.get', $ticketAberto->numero);

                }
                else
                {
                    $linkTicket = '';
                }

                //$itens[$i]['tickets_abertos'] = $item->ticketsAbertos()->count();
                $itens[$i]['numero'] = $item->numero;
                $itens[$i]['tickets_gerais'] = $item->ticketsGerais();
                $itens[$i]['numero_ticket_aberto'] = ($ticketAberto) ? $ticketAberto->numero : '';

                $itens[$i]['link_ticket_aberto'] = $linkTicket;

                $itens[$i]['link_historico'] = ($versao) ? route('site.artwork.history', [$embalagem->id, $versao->id]) : null;
                $itens[$i]['link_enviar'] = (isset($ticket) && $ticket->artwArtes->last()) ? route('site.artwork.send', [$embalagem->id, $item->id]) : null;
                $itens[$i]['link_fornecedorpdf'] = (isset($ticket) && $ticket->artwArtes->last()) ? route('site.artwork.link_fornecedorpdf', [$embalagem->id, $item->id]) : null;
                $itens[$i]['link_edicao'] = route('site.artwork.package.edit.get', [$embalagem->id, $item->id]);
                //$itens[$i]['link_arte'] = ($versao && $ticket->artwArtes->last()) ? $ticket->artwArtes->last()->path : null;
                $itens[$i]['link_arte'] = ($versao && $arteFinal) ? route('site.artwork.baixarArteFinal', [$arteFinal->id]) : null;
                $itens[$i]['link_abrir'] = route('site.artwork.ticket.create.get', [$embalagem, $item->id]);
            }

            $data['itens'] = $itens;

            if ($embalagem->id_campanha) {

                $retorno['promocionais'][] = $data;
            } else {

                $retorno['regulares'][] = $data;
            }

            $n++;
        }

        return $retorno;
    }
    
    public function getTodasEmbalagens()
    {
        $embalagens = ArtwVariacoes::select(
                'artw_variacoes.*', 'artw_variacao_tipos.nome as tipo', 'produtos.nome as produto_nome', 'tipos_embalagens.nome as tipo_embalagem', 'dimensoes.nome as dimensao_nome', 'campanhas.nome as campanha'
            )
            ->distinct()
            //->leftjoin('artw_variacao_itens', 'artw_variacao_itens.id_variacao', '=', 'artw_variacoes.id')
            //->leftjoin('artw_itens', 'artw_variacao_itens.id_item', '=', 'artw_itens.id')
            ->join('skus', 'artw_variacoes.id_sku', '=', 'skus.id')
            ->join('produtos', 'skus.id_produto', '=', 'produtos.id')
            ->join('dimensoes', 'skus.id_dimensao', '=', 'dimensoes.id')
            ->join('tipos_embalagens', 'dimensoes.id_tipo_embalagem', '=', 'tipos_embalagens.id')
            ->leftJoin('campanhas', 'campanhas.id', '=', 'artw_variacoes.id_campanha')
            ->leftJoin('artw_variacao_tipos', 'artw_variacao_tipos.id', '=', 'artw_variacoes.id_tipo')
            //->where('artw_itens.status', 1)
            ->orderBy('produtos.nome')
            ->orderBy('tipos_embalagens.nome')
            ->orderBy('dimensoes.nome')
            ->orderBy('artw_variacoes.nome_original')
            ->withTrashed()
            ->get();

        //dd($embalagens->toSql());

        foreach ($embalagens as $e => $embalagem) {

            $retorno[$e] = $embalagem->toArray();
            $retorno[$e]['mercados'] = $embalagem->mercados; // to-do

            $nome = '';
            $nome = $embalagem->produto_nome
                . ' ' .
                $embalagem->tipo_embalagem
                . ' ' .
                $embalagem->dimensao_nome;
            if ($embalagem->campanha) {
                $nome .= ' • ' . $embalagem->campanha;
            }
            if ($embalagem->principal !== 1 && $embalagem->nome) {
                $nome .= ' • ' . $embalagem->nome;
            }
            
            $retorno[$e]['nome_original'] = $nome;
        }

        return $retorno;
    }
    
    public function getTodasEmbalagensPorItem($id_item)
    {
        $embalagens = ArtwVariacoes::select(
                'artw_variacoes.*', 'artw_variacao_tipos.nome as tipo', 'produtos.nome as produto_nome', 'tipos_embalagens.nome as tipo_embalagem', 'dimensoes.nome as dimensao_nome', 'campanhas.nome as campanha'
            )
            ->distinct()
            ->leftjoin('artw_variacao_itens', 'artw_variacao_itens.id_variacao', '=', 'artw_variacoes.id')
            ->leftjoin('artw_itens', 'artw_variacao_itens.id_item', '=', 'artw_itens.id')
            ->join('skus', 'artw_variacoes.id_sku', '=', 'skus.id')
            ->join('produtos', 'skus.id_produto', '=', 'produtos.id')
            ->join('dimensoes', 'skus.id_dimensao', '=', 'dimensoes.id')
            ->join('tipos_embalagens', 'dimensoes.id_tipo_embalagem', '=', 'tipos_embalagens.id')
            ->leftJoin('campanhas', 'campanhas.id', '=', 'artw_variacoes.id_campanha')
            ->leftJoin('artw_variacao_tipos', 'artw_variacao_tipos.id', '=', 'artw_variacoes.id_tipo')
            ->where('artw_variacao_itens.id_item', $id_item)
            ->where('artw_itens.status', 1)
            ->orderBy('produtos.nome')
            ->orderBy('tipos_embalagens.nome')
            ->orderBy('dimensoes.nome')
            ->orderBy('artw_variacoes.nome_original')
            ->withTrashed()
            ->get();

        //dd($embalagens->toSql());

        foreach ($embalagens as $e => $embalagem) {

            $retorno[$e] = $embalagem->toArray();
            $retorno[$e]['mercados'] = $embalagem->mercados; // to-do

            $nome = '';
            $nome = $embalagem->produto_nome
                . ' ' .
                $embalagem->tipo_embalagem
                . ' ' .
                $embalagem->dimensao_nome;
            if ($embalagem->campanha) {
                $nome .= ' • ' . $embalagem->campanha;
            }
            if ($embalagem->principal !== 1 && $embalagem->nome) {
                $nome .= ' • ' . $embalagem->nome;
            }
            
            $retorno[$e]['nome_original'] = $nome;
        }

        return $retorno;
    }

    public function verificaNomeArquivo($relativePath, $filename)
    {
        $arquivo = public_path($relativePath . $filename);

        if (file_exists($arquivo))
        {
            $path_parts = pathinfo($arquivo);

            $filename = $path_parts['filename'] . '_' . time() . '.' . $path_parts['extension'];

            return $filename;
        }
        else
        {
            return $filename;
        }
    }
    
}
