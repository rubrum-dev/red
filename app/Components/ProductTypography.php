<?php

namespace App\Components;

use App\Produtos;
use App\Tipografias;
use App\TiposTipografias;
use App\Components\Common;

use App\Http\Requests;

class ProductTypography
{
    public function getTypography($id)
    {
        $produto = Produtos::find($id);
        $allTipografias = Tipografias::all();
        $tiposTipografias = TiposTipografias::all();
        $tipografias = array();
        $arrayTipografias = array();

        foreach ($allTipografias as $tipografia) {
            $tipografia->tipo = $tipografia->tiposTipografias;
            $tipografias[] = $tipografia;
        }

        foreach ($produto->tipografias as $value) {
            $arrayTipografias[] = $value->id;
        }

		return view('admin.produtos.tipografias', compact('produto', 'tiposTipografias', 'tipografias', 'arrayTipografias'));
    }

    public function postTypography($request)
    {
        $log = new Common;
		$products = new Produtos();
        $prod = $products->find($request->id);
        if(!is_null($request->tipografias)){
            $prod->tipografias()->sync($request->tipografias);
            // Log atualização Tipografia em Produto - 1 => Módulo Guide
            $log->setFavoritesLogs(1, $prod->id, 'UPDATE', 'Atualização de tipografias de produto', 'tipografias');
        }
        else{
            $prod->tipografias()->detach();
            // Log atualização Tipografia em Produto - 1 => Módulo Guide
            $log->setFavoritesLogs(1, $prod->id, 'DELETE', 'Tipografias desvinculadas de produto', 'tipografias');
        }

		return redirect()->route('admin.product.get', ['typography', $request->id])->with('status', 'Tipografias Atualizadas!');
    }
}
