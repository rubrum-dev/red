<?php

namespace App\Components;

use App\Produtos;
use App\TiposCores;
use App\NiveisCores;
use App\CategoriasVersoes;
use App\CategoriasProporcoes;
use App\TiposProporcoesVersoes;
use App\TiposUsosAplicacoes;
use App\TiposUsosIncorretos;
use App\TiposTipografias;
use App\CategoriasIcones;

class SiteGuides
{
	public function getCores($cores)
    {
    	$tipos = array();
        $niveis = array();
        foreach ($cores as $cor) {
            $tipos[] = $cor->id_tipo_cor;
            $niveis[] = $cor->id_nivel_cor;
        }

        $produto['tiposCores'] = TiposCores::whereIn('id', array_unique($tipos))->get();
        $produto['niveisCores'] = NiveisCores::whereIn('id', array_unique($niveis))->get();

        return $produto;
    }

    public function getVersoes($versoes)
    {
    	$tipos = array();
    	$categorias = array();
        foreach ($versoes as $versao) {
            $tipos[] = $versao->id_tipo_proporcao_versao;
            $categorias[] = $versao->id_categoria_versao;
        }

        $produto['tiposVersoes'] = TiposProporcoesVersoes::whereIn('id', array_unique($tipos))->get();
        $produto['categoriasVersoes'] = CategoriasVersoes::whereIn('id', array_unique($categorias))->orderBy('ordem')->get();

        return $produto;
    }

    public function getProporcoes($proporcoes)
    {
    	$tipos = array();
    	$categorias = array();
        foreach ($proporcoes as $proporcao) {
            $tipos[] = $proporcao->id_tipo_proporcao_versao;
            $categorias[] = $proporcao->id_categoria_proporcao;
        }

        $produto['tiposProporcoes'] = TiposProporcoesVersoes::whereIn('id', array_unique($tipos))->get();
        $produto['categoriasProporcoes'] = CategoriasProporcoes::whereIn('id', array_unique($categorias))->get();

        return $produto;
    }

    public function getAplicacoes($aplicacoes)
    {
    	$tipos = array();
        foreach ($aplicacoes as $aplicacao) {
            $tipos[] = $aplicacao->id_tipo_uso_aplicacao;
        }

        $produto['tiposAplicacoes'] = TiposUsosAplicacoes::whereIn('id', array_unique($tipos))->get();

        return $produto;
    }

    public function getUsosIncorretos($incorretos)
    {
    	$tipos = array();
        foreach ($incorretos as $incorreto) {
            $tipos[] = $incorreto->id_tipo_uso_incorreto;
        }

        $produto['tiposUsosIncorretos'] = TiposUsosIncorretos::whereIn('id', array_unique($tipos))->get();

        return $produto;
    }

    public function getTipografias($tipografias)
    {
    	$tipos = array();
        foreach ($tipografias as $tipografia) {
            $tipos[] = $tipografia->id_tipo_tipografia;
        }

        $produto['tiposTipografias'] = TiposTipografias::whereIn('id', array_unique($tipos))->get();

        return $produto;
    }

    public function getIcones($icones)
    {
    	$categorias = array();
        foreach ($icones as $icone) {
            $categorias[] = $icone->id_categoria_icone;
        }

        $produto['categoriasIcones'] = CategoriasIcones::whereIn('id', array_unique($categorias))->get();

        return $produto;
    }

}
