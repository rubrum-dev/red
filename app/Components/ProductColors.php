<?php

namespace App\Components;

use App\Produtos;
use App\Cores;
use App\TiposCores;
use App\NiveisCores;
use App\Components\Common;

use App\Http\Requests;

class ProductColors
{
    private $cor;

    public function __construct(Cores $cor)
    {
        $this->cor = $cor;
    }

    public function getColor($id)
    {
        $produto = Produtos::find($id);
        $niveisCores = NiveisCores::all();
        $tiposCores = TiposCores::all();
        $cores = array();

        foreach ($niveisCores as $value) {
            $niveis[$value->id] = $value->nome;
        }

        foreach ($tiposCores as $value) {
            $tipos[$value->id] = $value->nome;
        }

        foreach ($produto->cores as $cor) {
            $cor->descricao = substr($cor->descricao, 0, 150);
            $cores[] = $cor;
        }

        $produto->cores = $cores;

        return view('admin.produtos.cores', compact('produto', 'niveisCores', 'niveis', 'tiposCores', 'tipos'));
    }

    public function create($request)
    {
        if(empty($request->nome) || empty($request->id_tipo_cor) || empty($request->id_nivel_cor) || empty($request->spot) || empty($request->cmyk) || empty($request->rgb) || empty($request->hex)){
            return false;
        }

        $color = $this->cor;
        $dados = $request->all();
        $produto = Produtos::find($dados['id_prod']);

        $produto->cores()->attach($color->create($dados));
        $log = new Common;
        // Log nova cor em Produto - 1 => Módulo Guide
        $log->setFavoritesLogs(1, $produto->id, 'CREATE', 'Nova cor de produto', 'cores');
        die;
    }

    public function edit($id)
    {
        $editColor = $this->cor->find($id);
        $editColor->tiposCores;
        $editColor->niveisCores;

        return response(compact('editColor'));
    }

    public function update($request)
    {
        if(empty($request->nome) || empty($request->id_tipo_cor) || empty($request->id_nivel_cor) || empty($request->spot) || empty($request->cmyk) || empty($request->rgb) || empty($request->hex)){
            return false;
        }

        $color = $this->cor->find($request->id);
        $dados = $request->all();

        if($color->update($dados)){
            $log = new Common;
            // Log update cor em Produto - 1 => Módulo Guide
            $log->setFavoritesLogs(1, $color->produtos->first()->id, 'UPDATE', 'Atualização de cor de produto', 'cores');
        }
        die;
    }

    public function remove($id)
    {
        $removeColor = $this->cor->find($id);
        $prodId = $removeColor->produtos->first()->id;
        $removeColor->produtos()->detach();

        if($removeColor->delete()){
            $log = new Common;
            // Log remoção de cor em Produto - 1 => Módulo Guide
            $log->setFavoritesLogs(1, $prodId, 'DELETE', 'Remoção de cor de produto', 'cores');
        }
    }
}
