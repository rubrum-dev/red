<?php
namespace App\Components;

use App\Skus;
use App\ArtesFinais;
use App\ArtwVariacoes;
use App\Components\Common;
use Illuminate\Support\Facades\File;
use App\Http\Requests;

class SkuArtesFinais
{

    private $art;

    public function __construct(ArtesFinais $art)
    {
        $this->art = $art;
    }

    public function getFinalart($id)
    {
        $sku = Skus::find($id);

        $artesFinais = $sku->artesFinais;
        
        /*
        if ($artesFinais->count() == 1) {
            $artesFinais->first()->arte_exibicao = '1';
            $artesFinais->first()->save();
        }
         * 
         */
        
        return view('admin.skus.artes', compact('sku', 'artesFinais'));
    }

    public function create($request)
    {
        if ($request->promocional == 1 && empty($request->dt_expiracao)) {
            return false;
        }

        $tmpArte = $this->art;
        $dados = $request->all();
        
        $image = isset($dados['image']) ? $dados['image'] : null;
        $cmyk = isset($dados['cmyk']) ? $dados['cmyk'] : null;
        $rgb = isset($dados['rgb']) ? $dados['rgb'] : null;
        $pdf = isset($dados['pdf']) ? $dados['pdf'] : null;
        $zip = isset($dados['zip']) ? $dados['zip'] : null;
        $imagePath = '/images/uploads/artes-finais/';
        $cmykPath = '/files/uploads/artes-finais/cmyk/';
        $rgbPath = '/files/uploads/artes-finais/rgb/';
        $pdfPath = '/files/uploads/artes-finais/pdf/';
        $zipPath = '/files/uploads/artes-finais/zip/';
        
        //$findVariacao = ArtwVariacoes::find($dados['id_sku']);
        //$findSku = $findVariacao->sku;

        $findSku = Skus::find($dados['id_sku']);
        
        // Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $image];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

        // Validação de Upload de Arquivo CMYK em Zip
        $data = ['file' => $cmyk];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem CMYK invalido';
        $validate->validaUpload($data, $rules, $message);

        // Validação de Upload de Arquivo RGB em Zip
        $data = ['file' => $rgb];
        $rules = ['file' => 'mimes:jpeg,bmp,png,tiff,zip,pdf,psd'];
        $message = 'Formato de imagem inválido';
        $validate->validaUpload($data, $rules, $message);

        // Validação de Upload de Arquivo PDF
        $data = ['file' => $pdf];
        $rules = ['file' => 'mimes:pdf'];
        $message = 'Formato de arquivo PDF invalido. Utilize um arquivo PDF';
        $validate->validaUpload($data, $rules, $message);

        // Validação de Upload de Arquivos AI em Zip
        $data = ['file' => $zip];
        $rules = ['file' => 'mimes:zip,pdf'];
        $message = 'Formato de arquivo ZIP invalido. Utilize um arquivo ZIP';
        $validate->validaUpload($data, $rules, $message);

        $dados['promocional'] = isset($dados['promocional']) ? $dados['promocional'] : "0";

        if (isset($dados['arte_exibicao'])) {
            $artesExib = $this->art->where(array('id_sku' => $findSku->id, 'arte_exibicao' => 1))->get();
            if (count($artesExib) > 0) {
                foreach ($artesExib as $ae) {
                    $ae->arte_exibicao = 0;
                    $ae->save();
                }
            }
        }

        // if($dados['promocional'] == 1){
        //     $data = str_replace('/', '-', $dados['dt_expiracao']);
        //     $expiracao = date_create($data);
        //     $dados['dt_expiracao'] = date_format($expiracao, 'Y-m-d');
        // }

        if (!is_null($image)) {
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash . "." . $extension;
            $destinationPath = public_path() . $imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath . $imageName;
        }

        if (!is_null($cmyk)) {
            $extension = $cmyk->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $fileName = $hash . "." . $extension;
            $destinationPath = storage_path() . $cmykPath;
            $upload_success = $cmyk->move($destinationPath, $fileName);
            $dados['path_cmyk'] = $cmykPath . $fileName;
        }

        if (!is_null($rgb)) {
            $extension = $rgb->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $fileName = $hash . "." . $extension;
            $destinationPath = storage_path() . $rgbPath;
            $upload_success = $rgb->move($destinationPath, $fileName);
            $dados['path_rgb'] = $rgbPath . $fileName;
        }

        if (!is_null($pdf)) {
            $extension = $pdf->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $fileName = $hash . "." . $extension;
            $destinationPath = storage_path() . $pdfPath;
            $upload_success = $pdf->move($destinationPath, $fileName);
            $dados['link_pdf'] = $pdfPath . $fileName;
        }

        if (!is_null($zip)) {
            $extension = $zip->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $fileName = $hash . "." . $extension;
            $destinationPath = storage_path() . $zipPath;
            $upload_success = $zip->move($destinationPath, $fileName);
            $dados['link_zip'] = $zipPath . $fileName;
        }
        
        $dados['id_sku'] = $findSku->id;
        //$dados['id_variacao'] = $findVariacao->id;
        
        if ($tmpArte->create($dados)) {
            $log = new Common;
            if ($pdf || $zip)
            // Log após edição de ZIP e PDF Arte Final - 2 => Módulo File
                $log->setFavoritesLogs(2, $findSku->id_produto, 'CREATE', 'Novo arquivos em ' . $findSku->nome, null, $findSku->id);
            // Log após criação de Nova Arte Final - 3 => Módulo SKU
            $log->setFavoritesLogs(3, $findSku->id_produto, 'CREATE', 'Nova Arte Final em ' . $findSku->nome, null, $findSku->id);
        }
        die;
    }

    public function edit($id)
    {
        $editArt = $this->art->find($id);
        $expiracao = date_create($editArt->dt_expiracao);
        $editArt->dt_expiracao = date_format($expiracao, 'd/m/Y');

        return response(compact('editArt'));
    }

    public function update($request)
    {
        if (empty($request->id) /* || ($request->promocional == 1 && empty($request->dt_expiracao)) */) {
            return false;
        }

        $arte = $this->art->find($request->id);
        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;
        $cmyk = isset($dados['cmyk']) ? $dados['cmyk'] : null;
        $rgb = isset($dados['rgb']) ? $dados['rgb'] : null;
        $pdf = isset($dados['pdf']) ? $dados['pdf'] : null;
        $zip = isset($dados['zip']) ? $dados['zip'] : null;
        $imagePath = '/images/uploads/artes-finais/';
        $cmykPath = '/files/uploads/artes-finais/cmyk/';
        $rgbPath = '/files/uploads/artes-finais/rgb/';
        $pdfPath = '/files/uploads/artes-finais/pdf/';
        $zipPath = '/files/uploads/artes-finais/zip/';

        // Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $image];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

        // Validação de Upload de Arquivo CMYK em Zip
        $data = ['file' => $cmyk];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem CMYK invalido';
        $validate->validaUpload($data, $rules, $message);

        // Validação de Upload de Arquivo RGB em Zip
        $data = ['file' => $rgb];
        $rules = ['file' => 'mimes:jpeg,bmp,png,tiff,zip,pdf,psd'];
        $message = 'Formato de imagem inválido';
        $validate->validaUpload($data, $rules, $message);

        // Validação de Upload de Arquivo PDF
        $data = ['file' => $pdf];
        $rules = ['file' => 'mimes:pdf'];
        $message = 'Formato de arquivo PDF invalido. Utilize um arquivo PDF';
        $validate->validaUpload($data, $rules, $message);

        // Validação de Upload de Arquivos AI em Zip
        $data = ['file' => $zip];
        $rules = ['file' => 'mimes:zip,pdf'];
        $message = 'Formato de arquivo ZIP invalido. Utilize um arquivo ZIP';
        $validate->validaUpload($data, $rules, $message);

        // $dados['promocional'] = isset($dados['promocional']) ? $dados['promocional'] : "0";
        
        //$findVariacao = ArtwVariacoes::find($dados['id_sku']);
        //$findSku = $findVariacao->sku;

        $findSku = Skus::find($dados['id_sku']);
        
        $dados['id_sku'] = $findSku->id;
        //$dados['id_variacao'] = $findVariacao->id;
        
        if (isset($dados['arte_exibicao'])) {
            $artesExib = $this->art->where('id', '<>', $dados['id'])->where(array('id_sku' => $dados['id_sku'], 'arte_exibicao' => 1))->get();
            foreach ($artesExib as $ae) {
                $ae->arte_exibicao = 0;
                $ae->save();
            }
        }

        // if($dados['promocional'] == 1){
        //     $data = str_replace('/', '-', $dados['dt_expiracao']);
        //     $expiracao = date_create($data);
        //     $dados['dt_expiracao'] = date_format($expiracao, 'Y-m-d');
        // }

        if (!is_null($image)) {
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash . "." . $extension;
            $destinationPath = public_path() . $imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath . $imageName;

            if (File::isFile(public_path() . $arte->thumb)) {
                File::delete(public_path() . $arte->thumb);
            }
        }

        if (!is_null($cmyk)) {
            $extension = $cmyk->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $fileName = $hash . "." . $extension;
            $destinationPath = storage_path() . $cmykPath;
            $upload_success = $cmyk->move($destinationPath, $fileName);
            $dados['path_cmyk'] = $cmykPath . $fileName;

            if (File::isFile(storage_path() . $arte->path_cmyk)) {
                File::delete(storage_path() . $arte->path_cmyk);
            }
        }

        if (!is_null($rgb)) {
            $extension = $rgb->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $fileName = $hash . "." . $extension;
            $destinationPath = storage_path() . $rgbPath;
            $upload_success = $rgb->move($destinationPath, $fileName);
            $dados['path_rgb'] = $rgbPath . $fileName;

            if (File::isFile(storage_path() . $arte->path_rgb)) {
                File::delete(storage_path() . $arte->path_rgb);
            }
        }

        if (!is_null($pdf)) {
            $extension = $pdf->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $fileName = $hash . "." . $extension;
            $destinationPath = storage_path() . $pdfPath;
            $upload_success = $pdf->move($destinationPath, $fileName);
            $dados['link_pdf'] = $pdfPath . $fileName;

            if (File::isFile(storage_path() . $arte->link_pdf)) {
                File::delete(storage_path() . $arte->link_pdf);
            }
        }

        if (!is_null($zip)) {
            $extension = $zip->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $fileName = $hash . "." . $extension;
            $destinationPath = storage_path() . $zipPath;
            $upload_success = $zip->move($destinationPath, $fileName);
            $dados['link_zip'] = $zipPath . $fileName;

            if (File::isFile(storage_path() . $arte->link_zip)) {
                File::delete(storage_path() . $arte->link_zip);
            }
        }

        if ($arte->update($dados)) {
            $log = new Common;
            if ($pdf || $zip)
            // Log após edição de ZIP e PDF Arte Final - 2 => Módulo File
                $log->setFavoritesLogs(2, $arte->skus->id_produto, 'UPDATE', 'Edição de arquivos em ' . $arte->skus->nome, null, $arte->skus->id);
            // Log após edição de Arte Final - 3 => Módulo SKU
            $log->setFavoritesLogs(3, $arte->skus->id_produto, 'UPDATE', 'Edição de Arte Final em ' . $arte->skus->nome, null, $arte->skus->id);
        }
        die;
    }

    public function remove($id)
    {
        $arte = $this->art->find($id);

        if (File::isFile(public_path() . $arte->thumb)) {
            File::delete(public_path() . $arte->thumb);
        }

        if (File::isFile(storage_path() . $arte->path_cmyk)) {
            File::delete(storage_path() . $arte->path_cmyk);
        }

        if (File::isFile(storage_path() . $arte->path_rgb)) {
            File::delete(storage_path() . $arte->path_rgb);
        }

        if (File::isFile(storage_path() . $arte->link_pdf)) {
            File::delete(storage_path() . $arte->link_pdf);
        }

        if (File::isFile(storage_path() . $arte->link_zip)) {
            File::delete(storage_path() . $arte->link_zip);
        }

        $id_produto = $arte->skus->id_produto;
        $sku_nome = $arte->skus->nome;

        if ($arte->delete()) {
            $log = new Common;
            $log->setFavoritesLogs(3, $id_produto, 'DELETE', 'Remoção de Arte Final em ' . $sku_nome, null, $arte->skus->id);
        }
    }

    public function removeSkuFile($type, $id)
    {
        $arte = $this->art->find($id);

        if ($type == 'thumb' && File::isFile(public_path() . $arte->thumb)) {
            File::delete(public_path() . $arte->thumb);
            $arte->update(array('thumb' => null));
        }

        if ($type == 'cmyk' && File::isFile(storage_path() . $arte->path_cmyk)) {
            File::delete(storage_path() . $arte->path_cmyk);
            $arte->update(array('path_cmyk' => null));
        }

        if ($type == 'rgb' && File::isFile(storage_path() . $arte->path_rgb)) {
            File::delete(storage_path() . $arte->path_rgb);
            $arte->update(array('path_rgb' => null));
        }

        if ($type == 'pdf' && File::isFile(storage_path() . $arte->link_pdf)) {
            File::delete(storage_path() . $arte->link_pdf);
            $arte->update(array('link_pdf' => null));
        }

        if ($type == 'zip' && File::isFile(storage_path() . $arte->link_zip)) {
            File::delete(storage_path() . $arte->link_zip);
            $arte->update(array('link_zip' => null));
        }
    }
}
