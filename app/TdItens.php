<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TdItens extends Model
{
    protected $fillable = [
        'id_tipo',
        'id_variacao',
        'status'
    ];
    
    public function tipo()
    {
        return $this->belongsTo('App\ArtwItemTipos', 'id_tipo');
    }
    
    public function variacao()
    {
        return $this->belongsTo('App\TdVariacoes', 'id_variacao');
    }
    
    public function versoes()
    {
        return $this->hasMany('App\TdVersoes', 'id_item');
    }
    
    public function getNomeCompletoAttribute()
    {
        $nome = $this->variacao->nome_completo . ' - ' . $this->tipo->nome;
        
        return $nome;
    }
    
    public function delete()
    {
        $this->versoes()->delete();
        
        return parent::delete();
    }
    
}
