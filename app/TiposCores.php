<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposCores extends Model
{
    public function cores()
    {
    	return $this->hasMany('App\Cores', 'id_tipo_cor');
    }
}