<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelMeses extends Model
{
    protected $fillable = array(
		'id_tipo',
        'id_usuario',
		'mes',
        'ano',
        'status'
    );

    public function numeros()
    {
        return $this->hasMany('App\RelMesesNumeros', 'id_mes');
    }

}
