<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriasProdutos extends Model
{
    protected $fillable = array(
		'nome'
	);

	public function produtos()
    {
    	return $this->hasMany('App\Produtos', 'id_categoria_produto');
    }
}
