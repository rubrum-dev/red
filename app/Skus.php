<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Skus extends Model
{
    use SoftDeletes;

    protected $table = 'skus';
    protected $fillable = [
        'id_produto',
        'id_dimensao',
        'nome',
        'promocional',
        'dt_expiracao',
        'thumb',
        'ordem',
        'status'
    ];
    
    protected $dates = [
        'dt_expiracao'
    ];
    
    public function getNomeAttribute($value)
    {
        return str_replace(' - ', ' ', $value);
    }

    public function artesFinais()
    {
        return $this->hasMany('App\ArtesFinais', 'id_sku');
    }

    public function produtos()
    {
        return $this->belongsTo('App\Produtos', 'id_produto');
    }

    public function dimensoes()
    {
        return $this->belongsTo('App\Dimensoes', 'id_dimensao');
    }
    
    public function dimensoesAll()
    {
        return $this->belongsTo('App\Dimensoes', 'id_dimensao')->withTrashed();
    }

    public function promocoes()
    {
        return $this->belongsToMany('App\Promocoes', 'skus_promocoes', 'id_sku', 'id_promocao');
    }

    public function artwVersoes()
    {
        return $this->hasMany('App\ArtwVersoes', 'id_sku');
    }

    public function artwVariacoes()
    {
        return $this->hasMany('App\ArtwVariacoes', 'id_sku');
    }

    public function ArtwTickets()
    {
        return $this->hasMany('App\ArtwTickets', 'id_sku');
    }

    public function getSkuNomeCompletoAttribute()
    {
            $nome = $this->produtos->nome
            . ' ' .
            $this->dimensoes->tiposEmbalagens->nome
            . ' ' .
            $this->dimensoes->nome;

        return $nome;
    }

    public function getSkuNomeVolumeAttribute()
    {
            $nome = $this->dimensoes->tiposEmbalagens->nome
            . ' ' .
            $this->dimensoes->nome;

        return $nome;
    }
}
