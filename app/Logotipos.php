<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logotipos extends Model
{
    protected $fillable = array(
		'id_tipo_logotipo',
		'thumb',
		'status'
	);

	public function tiposLogotipos()
    {
    	return $this->belongsTo('App\TiposLogotipos', 'id_tipo_logotipo');
    }

    public function familias()
    {
    	return $this->belongsToMany('App\Familias', 'familias_logotipos', 'id_logotipo', 'id_familia');
    }

    public function produtos()
    {
        return $this->belongsToMany('App\Produtos', 'produtos_logotipos', 'id_logotipo', 'id_produto');
    }
}
