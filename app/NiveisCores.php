<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NiveisCores extends Model
{
    public function cores()
    {
    	return $this->hasMany('App\Cores', 'id_nivel_cor');
    }
}
