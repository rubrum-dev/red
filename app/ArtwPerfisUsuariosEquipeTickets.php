<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class ArtwPerfisUsuariosEquipeTickets extends Model
{
    use SoftDeletes;
    use Userstamps;
    
    protected $dates = ['deleted_at'];
    
    protected $fillable = array(
        'id_perfil',
        'id_usuario',
        'id_parent',
        'recebe_email',
        'aprovado',
        'ticket',
    );

    public function artwPerfisUsuariosTicket()
    {
        return $this->belongsTo('App\ArtwPerfisUsuariosTickets', 'id_parent');
    }

    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario')->withTrashed();
    }

    public function artwPerfilUsuarioEquipeTicket($id_usuario, $id_perfil, $id_ticket)
    {
        //dd($id_usuario, $id_perfil);

        return $this->select('artw_perfis_usuarios_equipe_tickets.*')
            ->join('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id', '=', 'artw_perfis_usuarios_equipe_tickets.id_parent')
            ->where('artw_perfis_usuarios_equipe_tickets.id_usuario', $id_usuario)
            ->where('artw_perfis_usuarios_tickets.id_perfil', $id_perfil)
            ->where('artw_perfis_usuarios_tickets.id_ticket', $id_ticket)
            ->get()
            ->last();
    }
    
    public function checkUsuario($id_usuario, $id_ticket)
    {
        return $this->select('artw_perfis_usuarios_equipe_tickets.*')
            ->join('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id', '=', 'artw_perfis_usuarios_equipe_tickets.id_parent')
            ->where('artw_perfis_usuarios_equipe_tickets.id_usuario', $id_usuario)
            ->where('artw_perfis_usuarios_tickets.id_ticket', $id_ticket)
            ->get()
            ->last();
    }
}
