<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwArquivos extends Model
{

    protected $fillable = [
        'id_ticket',
        'path',
        'descricao',
    ];

    public function artwTicket()
    {
        return $this->belongsTo('App\ArtwTickets', 'id_ticket');
    }
    
    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario')->withTrashed();
    }
    
    public function getFilesizeAttribute()
    {
        $file = public_path($this->path);
        
        if (file_exists($file))
        {
            $tamanho = filesize($file) / pow(1024, 2);
            
            return str_replace(".", "," , strval(round($tamanho, 2)));
            
        } else {
            
            return 0;
            
        }
    }

}
