<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriasIcones extends Model
{
	protected $fillable = array(
		'nome'
	);
	
    public function icones()
    {
    	return $this->hasMany('App\Icones', 'id_categoria_icone');
    }
}
