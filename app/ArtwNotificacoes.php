<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwNotificacoes extends Model
{
    public $fillable = [
        'id_tipo',
        'id_ticket',
        'id_from',
        'id_to',
        'id_perfil',
        'ciclo'
    ];

    public function artwNotificacaoTipo()
    {
        return $this->belongsTo('App\ArtwNotificacoesTipos', 'id_tipo');
    }

    public function artwNotificacoesEnvios()
    {
        return $this->hasMany('App\ArtwNotificacoesEnvios', 'id_notificacao');
    }

    public function from()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_from');
    }

    public function to()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_to');
    }
    
    public function artwTicket()
    {
        return $this->belongsTo('App\ArtwTickets', 'id_ticket');
    }
    
    public function artwPerfisUsuario()
    {
        return $this->belongsTo('App\ArtwPerfisUsuarios', 'id_perfil');
    }
}
