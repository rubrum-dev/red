<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promocoes extends Model
{
    protected $fillable = array(
		'id_produto',
		'nome',
		'thumb',
		'dt_expiracao',
		'status'
	);

    public function skus()
    {
    	return $this->belongsToMany('App\Skus', 'skus_promocoes', 'id_promocao', 'id_sku');
    }

    public function produtos()
    {
    	return $this->belongsTo('App\Produtos', 'id_produto');
    }
}
