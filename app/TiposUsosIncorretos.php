<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposUsosIncorretos extends Model
{
    public function usosIncorretos()
    {
    	return $this->hasMany('App\UsosIncorretos', 'id_tipo_uso_incorreto');
    }
}
