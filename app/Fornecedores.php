<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fornecedores extends Model
{
    use SoftDeletes;
    
    protected $fillable = array(
		'nome',
        'telefone',
		'status'
    );
    
    public function fornecedoresEmails()
	{
		return $this->hasMany('App\FornecedoresEmails', 'id_fornecedor');
	}
    
    public function getNomeAttribute($nome)
    {
        if ($this->deleted_at){
            
            $nome .= '*';
            
        }
        
        return $nome;
    }
}
