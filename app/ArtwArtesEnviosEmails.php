<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ArtwArtesEnviosEmails extends Model
{
    
    protected $dates = [
        'dt_expiracao'
    ];
    
    public function artwArteEnvio()
    {
        return $this->belongsTo('App\ArtwArtesEnvios', 'id_arte_envio');
    }
    
    public function getDiasAttribute()
    {
        $now = Carbon::now();
        
        return $now->diffInDays($this->dt_expiracao);
    }
    
}
