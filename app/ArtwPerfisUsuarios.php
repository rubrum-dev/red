<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwPerfisUsuarios extends Model
{

    protected $fillable = [
        'nome'
    ];

    public function artwPerfisUsuariosTickets()
    {
        return $this->hasMany('App\ArtwPerfisUsuariosTickets', 'id_perfil');
    }

}
