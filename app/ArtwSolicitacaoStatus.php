<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwSolicitacaoStatus extends Model
{

    protected $table = 'artw_solicitacao_status';
            
    protected $fillable = [
        'nome'
    ];

    public function artwSolicitacoes()
    {
        return $this->hasMany('App\ArtwSolicitacoes', 'id_solicitacao');
    }

}
