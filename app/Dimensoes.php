<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dimensoes extends Model
{
	protected $fillable = array(
		'id_tipo_embalagem',
		'nome',
        'thumb',
		'status',
        'ordem'
	);

    public function tiposEmbalagens()
    {
    	return $this->belongsTo('App\TiposEmbalagens', 'id_tipo_embalagem');
    }
    
    public function tiposEmbalagensAll()
    {
    	return $this->belongsTo('App\TiposEmbalagens', 'id_tipo_embalagem')->withTrashed();
    }

    public function skus()
    {
    	return $this->hasMany('App\Skus', 'id_dimensao');
    }
}
