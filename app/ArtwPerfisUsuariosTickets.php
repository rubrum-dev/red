<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class ArtwPerfisUsuariosTickets extends Model
{
    use SoftDeletes;
    use Userstamps;

    protected $dates = ['deleted_at'];
    
    protected $fillable = array(
        'id_perfil',
        'id_usuario',
        'id_ticket',
        'recebe_email',
        'aprovado',
        'ticket'
    );

    public function artwPerfisUsuario()
    {
        return $this->belongsTo('App\ArtwPerfisUsuarios', 'id_perfil');
    }

    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario')->withTrashed();
    }

    public function artwTicket()
    {
        return $this->belongsTo('App\ArtwTickets', 'id_ticket');
    }

    public function membroAprovador()
    {
        return $this->belongsTo('App\ArtwPerfisUsuariosEquipeTickets', 'id_membro_aprovador')->withTrashed();
    }
    
    public function emReprovacaoPor()
    {
        return $this->belongsTo('App\AdmUsuarios', 'em_reprovacao_by')->withTrashed();
    }
    
    public function participanteRenovador()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_renovacao');
    }

    public function artwPerfisUsuariosEquipeTickets()
    {
        return $this->hasMany('App\ArtwPerfisUsuariosEquipeTickets', 'id_parent');
    }
    
    public function equipe()
    {
        $equipe = ArtwPerfisUsuariosEquipeTickets
            ::where('id_parent', $this->id)
            ->join('adm_usuarios', 'artw_perfis_usuarios_equipe_tickets.id_usuario', '=', 'adm_usuarios.id')
            ->orderBy('adm_usuarios.nome')
            ->orderBy('adm_usuarios.sobrenome')
            ->get();
        
        return $equipe;
    }
    
    public function getDeletedAtAttribute($val)
    {
        $value = ($this->admUsuario->deleted_at) ? $this->admUsuario->deleted_at : $val;
        
        unset($this->admUsuario);
        
        return $value;
    }

}
