<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UsosAplicacoes
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Produtos[] $produtos
 * @property-read \App\TiposUsosAplicacoes $tiposUsosAplicacoes
 * @mixin \Eloquent
 */
class UsosAplicacoes extends Model
{
    protected $fillable = array(
		'id_tipo_uso_aplicacao',
		'thumb',		
		'descricao',
		'status'
	);

	public function produtos()
	{
		return $this->belongsToMany('App\Produtos', 'produtos_usos_aplicacoes', 'id_uso_aplicacao', 'id_produto');
	}

    public function tiposUsosAplicacoes()
    {
    	return $this->belongsTo('App\TiposUsosAplicacoes', 'id_tipo_uso_aplicacao');
    }
}
