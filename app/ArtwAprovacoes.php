<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwAprovacoes extends Model
{
    protected $fillable = array(
        'id_ticket',
        'id_usuario',
        'ciclo',
        'aprovado',
        'id_perfil',
        'descricao'
    );

    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario')->withTrashed();
    }
}
