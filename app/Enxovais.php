<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enxovais extends Model
{
    protected $fillable = array(
		'id_produto',
		'nome',
        'thumb',
		'path_down',
		'status'
	);

    public function produtos()
    {
    	return $this->belongsTo('App\Produtos', 'id_produto');
    }
}
