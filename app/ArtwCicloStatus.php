<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwCicloStatus extends Model
{
    
    protected $table = 'artw_ciclo_status';
    
    protected $fillable = [
        'nome'
    ];

    public function artwCiclos()
    {
        return $this->hasMany('App\ArtwCiclos', 'id_status');
    }

}
