<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwVariacaoMercados extends Model
{

    protected $fillable = [
        'id_mercado',
        'id_variacao'
    ];
    
}
