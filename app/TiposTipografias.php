<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposTipografias extends Model
{
    public function tipografias()
    {
    	return $this->hasMany('App\Tipografias', 'id_tipo_tipografia');
    }
}
