<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\AdmLogNovidades
 *
 * @property-read \App\AdmModulos $admModulos
 * @property-read \App\Produtos $produtos
 * @mixin \Eloquent
 */
class AdmLogNovidades extends Model
{
    protected $fillable = array(
		'id_produto',
		'id_modulo',
		'descricao',
        'operacao',
        'guide',
        'sku',
        'status',
        'updated_at'
	);

    public function produtos()
    {
    	return $this->belongsTo('App\Produtos', 'id_produto');
    }

    public function admModulos()
    {
    	return $this->belongsTo('App\AdmModulos', 'id_modulo');
    }
}
