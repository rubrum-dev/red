<?php

    function check_download($file)
    {
        if (!is_file($file)){
            sleep(1);
            check_download($file);
        }
    }
    
    function asset2($asset)
    {
        return asset($asset) . '?v=' . filemtime(public_path($asset));
    }
    
    function ddd($variable)
    {
        echo '<pre>';
        print_r($variable);
        echo '</pre>';
        
        die(1);
    }
    
    function get_filename($file)
    {
        $extension = $file->getClientOriginalExtension();
        
        $filename = str_replace($extension, '', str_slug($file->getClientOriginalName())) . '.' . $extension;
        
        return $filename;
    }