<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ArtwTicketsStatus
 *
 * @property-read \App\AdmUsuarios $admUsuario
 * @mixin \Eloquent
 */
class ArtwTicketsStatus extends Model
{
    protected $table = 'artw_tickets_status';
    
    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario')->withTrashed();
    }
}
