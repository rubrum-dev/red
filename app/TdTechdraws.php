<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class TdTechdraws extends Model
{

    protected $fillable = [
        'id_produto',
        'id_dimensao',
        'nome',
        'status'
    ];

    public function variacoes()
    {
        return $this->hasMany('App\TdVariacoes', 'id_techdraw');
    }

    public function dimensao()
    {
        return $this->belongsTo('App\Dimensoes', 'id_dimensao');
    }
    
}
