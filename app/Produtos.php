<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produtos extends Model
{
    protected $fillable = array(
		'id_familia',
		'id_categoria_produto',
		'nome',
		'ordem',
		'artwork',
        'packshelf',
		'status',
        'cor_hexa',
		'replicar_logo_marca'
	);

	public function logotipos()
	{
		return $this->belongsToMany('App\Logotipos', 'produtos_logotipos', 'id_produto', 'id_logotipo');
	}

	public function categorias()
    {
    	return $this->belongsTo('App\CategoriasProdutos', 'id_categoria_produto');
    }

    public function familias()
    {
    	return $this->belongsTo('App\Familias', 'id_familia');
    }

    public function enxovais()
    {
    	return $this->hasMany('App\Enxovais', 'id_produto');
    }

    public function cores()
	{
		return $this->belongsToMany('App\Cores', 'produtos_cores', 'id_produto', 'id_cor');
	}

	public function tipografias()
	{
		return $this->belongsToMany('App\Tipografias', 'produtos_tipografias', 'id_produto', 'id_tipografia');
	}

	public function usosIncorretos()
	{
		return $this->belongsToMany('App\UsosIncorretos', 'produtos_usos_incorretos', 'id_produto', 'id_uso_incorreto');
	}

	public function usosAplicacoes()
	{
		return $this->belongsToMany('App\UsosAplicacoes', 'produtos_usos_aplicacoes', 'id_produto', 'id_uso_aplicacao');
	}

	public function versoes()
	{
		return $this->belongsToMany('App\Versoes', 'produtos_versoes', 'id_produto', 'id_versao');
	}

	public function proporcoes()
	{
		return $this->belongsToMany('App\Proporcoes', 'produtos_proporcoes', 'id_produto', 'id_proporcao');
	}

	public function icones()
	{
		return $this->belongsToMany('App\Icones', 'produtos_icones', 'id_produto', 'id_icone');
	}

	public function skus()
    {
    	return $this->hasMany('App\Skus', 'id_produto');
    }

    public function admUsuarios()
    {
    	return $this->belongsToMany('App\AdmUsuarios', 'adm_usuarios_produtos', 'id_produto', 'id_adm_usuario');
    }

    public function admLogNovidades()
    {
    	return $this->hasMany('App\AdmLogNovidades', 'id_produto');
    }

    public function promocoes()
    {
    	return $this->hasMany('App\Promocoes', 'id_produto');
    }

    public function admTokensGuides()
    {
    	return $this->hasMany('App\AdmTokensGuides', 'id_produto');
    }
}
