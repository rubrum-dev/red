<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwItens extends Model
{

    protected $fillable = [
        'id_tipo',
        'id_variacao_parent',
        'status',
        'nome'
    ];
    
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function artwItemTipo()
    {
        return $this->belongsTo('App\ArtwItemTipos', 'id_tipo');
    }

    public function pai()
    {
        return $this->belongsTo('App\ArtwItens', 'id_pai');
    }

    public function getArtwVariacaoAttribute()
    {
        //return $this->belongsTo('App\ArtwVariacoes', 'id_variacao')->withTrashed();
        return $this->artwVariacoes->first();
    }

    public function artwVariacoes()
    {
        return $this->belongsToMany('App\ArtwVariacoes', 'artw_variacao_itens', 'id_item', 'id_variacao')->withTrashed();
    }

    public function artwVersoes()
    {
        return $this->hasMany('App\ArtwVersoes', 'id_item');
    }

    public function artwTickets()
    {
        return $this->hasMany('App\ArtwTickets', 'id_item');
    }
    
    public function getNumeroAttribute()
    {
        $numero = ($this->compartilhado) ? str_pad($this->compartilhado, 3, "0", STR_PAD_LEFT) : null;
        
        return $numero;
    }
    
    public function getNomeCompletoAttribute()
    {
        $nome = ($this->compartilhado) ? $this->artwItemTipo->nome . ' • ' . $this->nome . ' • ' . $this->numero : $this->artwItemTipo->nome;
        
        return $nome;
    }
    
    public function getSkuNomeCompletoAttribute()
    {
        if ($this->compartilhado) {
            
            $nome = 'Item Compartilhado';
            
        } else {

            $this->sku = $this->artwVariacao->sku;

            $nome = $this->sku->produtos->nome
                . ' ' .
                $this->sku->dimensoes->tiposEmbalagens->nome
                . ' ' .
                $this->sku->dimensoes->nome;

            if ($this->artwVariacao->campanha) {
                $nome .= ' • ' . $this->artwVariacao->campanha->nome;
            }

            if ($this->artwVariacao->principal !== 1 && $this->artwVariacao->nome) {
                $nome .= ' • ' . $this->artwVariacao->nome;
            }

            if ($this->artwItemTipo) {
                $nome .= ' • ' . $this->artwItemTipo->nome;
            }

            unset($this->sku);
        }

        return $nome;
    }

    public function getSkuNomeAttribute()
    {
        if ($this->compartilhado) {
            
            $nome = 'Item Compartilhado';
;            
        } else {

            $this->sku = $this->artwVariacao->sku;

            $nome = $this->sku->produtos->nome
                . ' ' .
                $this->sku->dimensoes->tiposEmbalagens->nome
                . ' ' .
                $this->sku->dimensoes->nome;

            unset($this->sku);
        }

        return $nome;
    }

    public function getCampanhaAttribute()
    {
        $this->sku = $this->artwVariacao->sku;

        $nome = '';

        if ($this->artwVariacao->campanha) {
            $nome .= $this->artwVariacao->campanha->nome;
        }

        unset($this->sku);

        return $nome;
    }

    public function getVariacaoAttribute()
    {
        $this->sku = $this->artwVariacao->sku;

        $nome = '';

        if ($this->artwVariacao->principal !== 1 && $this->artwVariacao->nome) {
            $nome .= $this->artwVariacao->nome;
        }

        unset($this->sku);

        return $nome;
    }

    public function ticketsAbertos()
    {
        $statusAbertos = [0, 1, 2, 3, 4, 5, 9, 10, 11, 12];

        /*
          $tickets = $this
          ->artwTickets
          ->whereIn('id_status', $statusAbertos)
          //->whereNull('pausado_em')
          //->whereNull('cancelado_em')
          ;
         * 
         */

        $tickets = ArtwTickets
            ::where('id_item', $this->id)
            ->whereIn('id_status', $statusAbertos)
            ->whereNull('pausado_em')
            ->whereNull('cancelado_em')
            ->get();

        return $tickets;
    }

    public function ticketsAbertosSemCancelados()
    {
        $statusAbertos = [0, 1, 2, 3, 4, 5, 9, 10, 11, 12];

        /*
          $tickets = $this
          ->artwTickets
          ->whereIn('id_status', $statusAbertos)
          //->whereNull('pausado_em')
          //->whereNull('cancelado_em')
          ;
         * 
         */

        $tickets = ArtwTickets
            ::where('id_item', $this->id)
            ->whereIn('id_status', $statusAbertos)
            //->whereNull('pausado_em')
            ->whereNull('cancelado_em')
            ->get();

        return $tickets;
    }

    public function ticketsAbertosSemPausados()
    {
        $statusAbertos = [0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12];

        $tickets = ArtwTickets
            ::where('id_item', $this->id)
            ->whereIn('id_status', $statusAbertos)
            ->whereNull('pausado_em')
            //->whereNull('cancelado_em')
            ->count();

        return $tickets;
    }

    public function ticketsGerais()
    {
        $statusAbertos = [0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12];

        $tickets = ArtwTickets
            ::where('id_item', $this->id)
            ->where('numero', '>', 170000)
            ->whereIn('id_status', $statusAbertos)
            ->whereNull('pausado_em')
            ->whereNull('cancelado_em')
            ->count();

        return $tickets;
    }

    public function delete()
    {
        $this->artwVersoes()->delete();

        return parent::delete();
    }
}
