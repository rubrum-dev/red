<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdsmartCondicoesMidia extends Model
{
    protected $table = "adsmart_condicoes_midia";

    protected $fillable = [
        'nome',
        'texto',
        'abertura_ticket',
        'reprovacao',
        'obrigatoria'
    ];
}
