<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\SendEmails',
        'App\Console\Commands\ArtworkNotificacao',
        //'App\Console\Commands\ArtworkRender',
        'App\Console\Commands\ArtworkRenderCiclo',
        'App\Console\Commands\ArtworkRenderFornecedor',
        'App\Console\Commands\ArtworkRenderVersao',
        'App\Console\Commands\ArtworkCron',
        'App\Console\Commands\ArtworkDashboard',
        'App\Console\Commands\ArtworkUpdate',
        'App\Console\Commands\ArtworkUsuarios',
        'App\Console\Commands\ArtworkExportar',
        'App\Console\Commands\ArtworkRenderTicketsAbertos',
        'App\Console\Commands\ArtworkLimpeza',
        'App\Console\Commands\ArtworkMigrarArtesFinais'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule
            ->command('artwork:notificacao')
            //->twiceDaily(20,21,22,23)
            ->twiceDaily(20,23)
            ->timezone('America/Sao_Paulo');

        $schedule->command('artwork:usuarios')
            ->twiceDaily(21,22)
            ->timezone('America/Sao_Paulo');

        //$schedule->command('artwork:usuarios')->hourly();
        
        $schedule->command('artwork:cron')->cron('*/5 * * * *');

        //$schedule->command('artwork:dashboard')->everyMinute();

        $schedule->command('artwork:dashboard')
            ->timezone('America/Sao_Paulo')
            ->everyMinute()
            ->when(function () {
                    return date('H') == 23;
            });
        
        $schedule->command('artwork:render-ciclo')->cron('*/2 * * * *');
        $schedule->command('artwork:render-versao')->cron('*/2 * * * *');

        //$schedule->command('artwork:render-ciclo')->cron('* * * * *');
        //$schedule->command('artwork:render-versao')->cron('* * * * *');

        //$schedule->command('artwork:limpeza')->timezone('America/Sao_Paulo')->dailyAt('21:00');
        
        //$schedule->command('artwork:render-tickets-abertos')->timezone('America/Sao_Paulo')->dailyAt('21:00');

        //$schedule->command('artwork:render-tickets-abertos')
        //    ->timezone('America/Sao_Paulo')
        //    ->cron('*/5 * * * *')
        //    ->when(function () {
        //            return date('H') >= 20 || date('H') <= 21;
        //});
        
        //$schedule->command('artwork:render')->cron('*/2 * * * *');
        
        //$schedule->command('artwork:render')->everyMinute();
        
        //$schedule->command('artwork:render')->everyFiveMinutes();
        
        //$schedule->command('artwork:cron')->everyFiveMinutes();
        
        //$schedule->command('artwork:notificacao')->everyMinute();
        //$schedule->command('artwork:cron')->everyMinute();
    }
}
