<?php

namespace App\Console\Commands;

use Mail;
use App\AdmUsuarios;
use App\AdmLogNovidades;
use Illuminate\Console\Command;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Disparo automatico de emails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$news = null;
		$getUsers = AdmUsuarios::where('status', 1)->whereNotIn('id_adm_perfil', [1, 2])->get();
		$logNovidades = AdmLogNovidades::where('created_at', '>', date('Y-m-d 00:00:00', strtotime(' - 1 days')))->get();

		foreach($getUsers as $user){
			foreach ($user->admModulos as $modulo) {
				$modulos[] = $modulo->id;
			}
			foreach ($user->produtos as $produto) {
				$produtos[] = $produto->id;
			}
			foreach ($logNovidades as $log) {
				if(in_array($log->id_modulo, $modulos) && in_array($log->id_produto, $produtos)){
					$data = date_create($log->updated_at);
					$log->data = date_format($data, 'd/m/Y');
					switch ($log->id_modulo) {
						case 1: // Guide
							$log->url = route('site.guide', array($log->produtos->id_familia, $log->id_produto, $log->guide));
							$log->image_mod = asset('/images/layout/logo_guide.png');
							break;
						case 2: // File
							$log->url = route('site.sku', array($log->produtos->id_familia, $log->id_produto));
							$log->image_mod = asset('/images/layout/logo_sku.png');
							break;
						case 3: // SKU
							if($log->sku && $log->operacao != 'DELETE')
								$log->url = route('site.sku', array($log->produtos->id_familia, $log->id_produto, 'sku' => $log->sku));
							else
								$log->url = route('site.sku', array($log->produtos->id_familia, $log->id_produto));
							$log->image_mod = asset('/images/layout/logo_sku.png');
							break;
						case 4: // Enxoval
							$log->url = route('site.guide', array($log->produtos->id_familia, $log->id_produto, $log->guide));
							$log->image_mod = asset('/images/layout/logo_enxoval.png');
							break;
						default:
							$log->url = route('site.index');
							break;
					}
                    if($user->notificacao_favoritos || $log->id_modulo == 4){
                        $news[] = $log;
                    }
				}
			}

			if(count($news) >= 1){
				$data = array(
					'nome' => htmlentities($user->nome),
					'email' => $user->email,
					'news' => $news
				);

				Mail::send('emails.alteracoes_favoritos', $data, function ($message) use($data) {
					//$message->from('ambev@paintpackred.com', 'Ambev Red');
                    $message->from(config('app.mail_from'), config('app.mail_name'));
					$message->to($data['email'])->subject('Alterações nos Favoritos | Paint Pack Red');
				});
			}

			$news = null;
			$modulos = null;
			$produtos = null;
		}
        \Log::info('Notificações de Favoritos Enviados em: ' . \Carbon\Carbon::now());
    }
}
