<?php
namespace App\Console\Commands;
use App\ArtwArtes;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
class ArtworkMigrarArtesFinais extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'artwork:migrar-artes-finais {--acao=}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Processo de migração das artes-finais para o S3';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        switch ($this->option('acao')) {
            case 'copiar':
                $this->copiar();
                break;
            case 'deletar':
                $this->deletar();
                break;
            case 'voltar':
                $this->voltar();
                break;
        }
    }

    private function copiar()
    {
        $artes = ArtwArtes::select('*')->where('path', '!=', null)->limit('10000')->get();
        
        if (!is_dir(public_path('/images/uploads/artwork/ticket/arte/MOVIDOS-S3/')))
        {
            mkdir(public_path('/images/uploads/artwork/ticket/arte/MOVIDOS-S3/'));
        }

        foreach ($artes as $n=>$arte)
        {
            echo $n+1 . ' - copiar arte ' . $arte->nome_arquivo;
            $file = public_path($arte->path);
            
            if (file_exists($file))
            {
                //$s3 = Storage::disk(config('app.storage'))->put(config('app.costummer_name') . '/artwork/artes-finais/' . $arte->nome_arquivo, file_get_contents($file));


                if (Storage::disk(config('app.storage'))->exists(config('app.costummer_name') . '/artwork/artes-finais/' . $arte->nome_arquivo)) {

                    $path_parts = pathinfo($arte->nome_arquivo);
        
                    $arte->nome_arquivo = $path_parts['filename'] . '_' . time() . '.' . $path_parts['extension'];

                    echo ' - renomeado para ' . $arte->nome_arquivo;
                }



                $s3 = Storage::disk(config('app.storage'))->put(config('app.costummer_name') . '/artwork/artes-finais/' . $arte->nome_arquivo, fopen($file, 'r+'));

                if ($s3)
                {
                    // renomeia
                    rename($file, public_path('/images/uploads/artwork/ticket/arte/MOVIDOS-S3/' . $arte->nome_arquivo));

                    $size = Storage::disk(config('app.storage'))->size(config('app.costummer_name') . '/artwork/artes-finais/' . $arte->nome_arquivo);
                    $arte->path = null;
                    $arte->size = $size;
                    $arte->save();
                    echo ' - Sucesso!!!';
                } else {
                    echo ' - Erro ao transefir pro S3';
                }
                
            } else {
                echo ' - 404';

                if (Storage::disk(config('app.storage'))->exists(config('app.costummer_name') . '/artwork/artes-finais/' . $arte->nome_arquivo))
                {
                    $size = Storage::disk(config('app.storage'))->size(config('app.costummer_name') . '/artwork/artes-finais/' . $arte->nome_arquivo);
                    $arte->path = null;
                    $arte->size = $size;
                    $arte->save();
                    echo ' - path atualizado no banco';
                }
            }

            echo "\n";
        }
    }

    private function deletar()
    {
        $artes = ArtwArtes::select('*')->where('path', '=', null)->where('storage', '=', 0)->limit('10000')->get();
        
        foreach ($artes as $n=>$arte)
        {
            echo $n+1 . ' - deletar arte ' . $arte->nome_arquivo;

            if (file_exists(public_path('/images/uploads/artwork/ticket/arte/MOVIDOS-S3/' . $arte->nome_arquivo))) {
                
                if (Storage::disk(config('app.storage'))->exists(config('app.costummer_name') . '/artwork/artes-finais/' . $arte->nome_arquivo))
                {
                    unlink(public_path('/images/uploads/artwork/ticket/arte/MOVIDOS-S3/' . $arte->nome_arquivo));

                    $arte->storage = 1;
                    $arte->save();

                    echo ' - Sucesso!!!';

                } else {

                    echo $n+1 . ' arte não existe na Amazon (S3) ';

                }

            } else {

                echo $n+1 . ' arte não existe localmente (EC2) ';

            }

            echo "\n";
        }
    }

    private function voltar()
    {
        $artes = ArtwArtes::select('*')->where('path', '=', null)->where('storage', '=', 0)->limit('10000')->get();
        
        foreach ($artes as $n=>$arte)
        {
            echo $n+1 . ' - voltar arte ' . $arte->nome_arquivo;

            Storage::disk(config('app.storage'))->delete(config('app.costummer_name') . '/artwork/artes-finais/' . $arte->nome_arquivo);

            rename(public_path('/images/uploads/artwork/ticket/arte/MOVIDOS-S3/' . $arte->nome_arquivo), public_path('/images/uploads/artwork/ticket/arte/' . $arte->nome_arquivo));

            $arte->path = '/images/uploads/artwork/ticket/arte/' . $arte->nome_arquivo;
            $arte->save();

            echo ' - Sucesso!!!';

            echo "\n";
        }
    }
}