<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ArtworkRenderFornecedor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'artwork:render-fornecedor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Renderiza arquivos de PDF do Fornecedor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'teste';
    }
}
