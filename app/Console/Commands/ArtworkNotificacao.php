<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Components\Common;
use App\ArtwTickets;

class ArtworkNotificacao extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'artwork:notificacao';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verifica e cadastra as notificações sem ação do usuário';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ArtwTickets $artwTickets, Common $common)
    {
        // EM REVISÃO - Email apenas para quem Segue com Alerta de Atraso*
        $tickets = $artwTickets
            ->where('id_status', 2)
            ->where('dt_envio', '<', date('Y-m-d H:i:s'))
            ->whereNull('pausado_em')
            ->whereNull('cancelado_em')
            ->get();
        
        foreach ($tickets as $ticket) {
            //$common->enviarNotificacao($ticket, 'revisao_atrasada', 'seguidores', null, 0, null, 1);
        }
        
        // MARKETING - Email para Marketing com Alerta de Atraso*
        $tickets = $artwTickets
            ->where('id_status', 3)
            ->where('dt_fim', '<', date('Y-m-d H:i:s'))
            ->whereNull('pausado_em')
            ->whereNull('cancelado_em')
            ->get();
        
        foreach ($tickets as $ticket) {
            //$common->enviarNotificacao($ticket, 'marketing_atrasada', 'marketings', null, 0, null, 1);
            //$common->enviarNotificacao($ticket, 'marketing_atrasada', 'seguidores', null, 0, null, 1);
        }
        
        // MARKETING - Email para Aprovadores com Alerta de Atraso*
        $tickets = $artwTickets
            ->where('id_status', 4)
            ->where('dt_fim', '<', date('Y-m-d H:i:s'))
            ->whereNull('pausado_em')
            ->whereNull('cancelado_em')
            ->get();
        
        foreach ($tickets as $ticket) {
            //$common->enviarNotificacao($ticket, 'aprovador_atrasada', 'aprovadores', null, 0, null, 1);
            //$common->enviarNotificacao($ticket, 'aprovador_atrasada', 'seguidores', null, 0, null, 1);
        }
        
        //dd($tickets);
    }
}
