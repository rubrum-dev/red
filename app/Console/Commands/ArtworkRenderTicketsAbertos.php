<?php
namespace App\Console\Commands;

use Mail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\ArtwTickets;
use Carbon\Carbon;

class ArtworkRenderTicketsAbertos extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'artwork:render-tickets-abertos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia os PDFs dos tickets abertos pra renderização';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $versaoApi = '561842';
        $statusAbertos = [1, 2, 3, 4, 5, 10, 11, 12];
        $dataAtual = Carbon::now();

        /*
          $tickets = $this
          ->artwTickets
          ->whereIn('id_status', $statusAbertos)
          //->whereNull('pausado_em')
          //->whereNull('cancelado_em')
          ;
         * 
         */

        //dd($dataAtual->subDays(90)->format('Y-m-d H:i:s'));

        $tickets = ArtwTickets
            ::whereIn('id_status', $statusAbertos)
            ->whereNull('pausado_em')
            ->whereNull('cancelado_em')
            ->where('created_at', '>=', $dataAtual->subDays(90)->format('Y-m-d H:i:s'))
            ->where('versao_api', '!=', $versaoApi)
            ->get();


        foreach ($tickets as $ticket)
        {
            $cicloLayout = $ticket->artwCiclos->last()->artwCiclosLayout->last();

            if ($cicloLayout)
            {
                $cicloLayout->indexado = 0;
                $cicloLayout->renderizado = 1;
                $cicloLayout->save();
                echo "Ticket ".$ticket->numero." Ciclo ".$ticket->artwCiclos->last()->numero." pra renderizar novamente\n";
            }

            $versaoAtual = $ticket->artwItem->artwVersoes->last();

            if ($versaoAtual)
            {
                $versaoAtual->indexado = 0;
                $versaoAtual->renderizado = 1;
                $versaoAtual->save();
                echo "Ticket ".$ticket->numero." Versão ".$versaoAtual->numero." pra renderizar novamente\n";
            }

            $fornecedorPdfs = $ticket->fornecedorPdfs()->whereIn('status', [0,1])->get();

            foreach ($fornecedorPdfs as $fornecedorPdf)
            {
                $fornecedorPdf->indexado = 0;
                $fornecedorPdf->renderizado = 1;
                $fornecedorPdf->save();
                echo "Ticket ".$ticket->numero." FornecedorPDF ".$fornecedorPdf->nome_arquivo." pra renderizar novamente\n";
            }

            $ticket->versao_api = $versaoApi;
            $ticket->save();

            echo "Ticket ".$ticket->numero." ok\n\n";
        }
        
    }
    
}
