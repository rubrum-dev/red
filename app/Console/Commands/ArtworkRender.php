<?php
namespace App\Console\Commands;

use Mail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;

class ArtworkRender extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'artwork:render';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Renderiza os PDFs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->base_api = config('app.cloudflow_base');
        
        $this->client = new Client([
            'base_uri' => $this->base_api . '/portal.cgi',
            'headers' => [
                'Content-Type' => 'application/json'
            ]
            ]
        );
    }

    public function getSession()
    {
        try {

            $response = $this->client->post('/', [
                'body' => json_encode([
                    'method' => 'auth.create_session',
                    'user_name' => 'admin',
                    'user_pass' => 'Red@1234',
                    ]
                ),
                'verify' => false
                ]
            );

            $body = $response->getBody()->getContents();
            $retorno = json_decode($body);

            if (isset($retorno->error)) {

                throw new \Exception($retorno->error);
            }
        } catch (\Exception $ex) {

            $retorno = ['message' => 'Ocorreu um erro.', 'error' => $ex->getMessage(), 'trace' => $ex->getTrace()];
        }
        
        return $retorno;
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        ini_set('max_execution_time', 0);
        
        $sql = "(SELECT
                    'ciclo' AS tipo,
                    artw_ciclos_layout.id,
                    artw_ciclos_layout.path,
                    artw_ciclos_layout.created_at
                FROM
                    artw_ciclos_layout
                WHERE
                artw_ciclos_layout.indexado = 0
                GROUP BY
                    artw_ciclos_layout.id_ciclo)

                UNION ALL

                (SELECT
                    'versao' AS tipo,
                    artw_versoes.id,
                    artw_versoes.path,
                    artw_versoes.created_at
                FROM
                    artw_versoes
                WHERE
                artw_versoes.indexado = 0)
                ORDER BY created_at DESC
                LIMIT 10";
        
        $arquivos = DB::select($sql);
        
        if ($arquivos)
        {
            $this->session = $this->getSession();
        }
        
        foreach ($arquivos as $arquivo){
            
            $fileArray = explode('/', $arquivo->path);
            $fileName = end($fileArray);
            $pasta = prev($fileArray);
            
            $caminho = ($pasta == 'ciclo') ? config('app.cloudflow_ciclo') : config('app.cloudflow_versao');
            
            $file = $caminho . rawurlencode($fileName);
            
            $response = $this->client->post('/', [
                'body' => json_encode(
                    [
                        'session' => $this->session->session,
                        'method' => 'proofscope.request_render_by_url',
                        'url' => $file,
                    ]
                ),
                'verify' => false
                ]
            );
            
            $body = $response->getBody()->getContents();
            $retorno = json_decode($body);
            
            if (!isset($retorno->error)) {
                
                if (($arquivo->tipo == 'ciclo')){
                    
                    $sql = "update artw_ciclos_layout set indexado = 1 where id = " . $arquivo->id;
                    
                } else {
                    
                    $sql = "update artw_versoes set indexado = 1 where id = " . $arquivo->id;
                    
                }
                
                DB::select($sql);
                
                echo $file . " - SUCESSO\n";
                
            } else {
                
                if (($arquivo->tipo == 'ciclo')){
                    
                    $sql = "update artw_ciclos_layout set indexado = 2, error = '" . $retorno->error .  "' where id = " . $arquivo->id;
                    
                } else {
                    
                    $sql = "update artw_versoes set indexado = 2, error = '" . $retorno->error .  "' where id = " . $arquivo->id;
                    
                }
                
                DB::select($sql);
                
                echo $file . " - erro - " . $retorno->error . "\n";
            }
            
            ////sleep(1);
        }
    }
    
}
