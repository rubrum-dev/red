<?php

namespace App\Console\Commands;

use App\ArtwVersoes;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ArtworkRenderVersao extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'artwork:render-versao';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Renderiza arquivos de Versão';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sql = "SELECT
                    'versao' AS tipo,
                    artw_versoes.id,
                    artw_versoes.path,
                    artw_versoes.created_at
                FROM
                    artw_versoes
                WHERE
                    artw_versoes.indexado = 0
                ORDER BY
                    created_at ASC
                LIMIT 1";
        
        $arquivos = DB::select($sql);
        
        foreach ($arquivos as $arquivo){
            
            // Manda renderizar
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36");
            curl_setopt($ch, CURLOPT_URL, route('api.reRender', [$arquivo->tipo, $arquivo->id]));
            //curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_TIMEOUT_MS, 500);

            curl_exec($ch);

            if (curl_errno($ch)) {
                $error_message = curl_error($ch);
                //echo "<br>erreur:" . $error_message;
            }

            curl_close($ch);
            
            echo "arquivo: " . $arquivo->path . " enviado pra renderização\n";

            $arquivoVersao = ArtwVersoes::find($arquivo->id);
            $arquivoVersao->indexado = 1;
            $arquivoVersao->renderizado = 1;
            $arquivoVersao->save();
            
        }
    }
}
