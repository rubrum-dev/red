<?php
namespace App\Console\Commands;

use Mail;
use Illuminate\Console\Command;
use App\ArtwNotificacoes;
use App\ArtwNotificacoesEnvios;

class ArtworkCron extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'artwork:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia e-mails minuto a minuto';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ArtwNotificacoes $artwNotificacoes)
    {
        // Select das notificações não enviadas
        $notificacoes = $artwNotificacoes
            ->select('artw_notificacoes.*')
            ->leftJoin('artw_notificacoes_envios', 'artw_notificacoes_envios.id_notificacao', '=', 'artw_notificacoes.id')
            ->join('adm_usuarios', 'artw_notificacoes.id_to', '=', 'adm_usuarios.id')
            ->where(function ($query) {
                $query->whereNull('artw_notificacoes_envios.id');
                $query->orWhere('artw_notificacoes_envios.status', '<', 1);
            })
            ->whereNull('adm_usuarios.deleted_at')
            ->get();

        foreach ($notificacoes as $notificacao) {

            $this->enviaEmail($notificacao);
        }
    }

    public function validateEmailDomain($email, $domains)
    {
        foreach ($domains as $domain) {
            $pos = strpos($email, $domain, strlen($email) - strlen($domain));

            if ($pos === false)
                continue;

            if ($pos == 0 || $email[(int) $pos - 1] == "@" || $email[(int) $pos - 1] == ".")
                return true;
        }

        return false;
    }

    public function enviaEmail(ArtwNotificacoes $notificacao)
    {
        if ($notificacao->from && $notificacao->to) {

            $data = [
                'notificacao' => $notificacao,
                'ticket' => $notificacao->artwTicket,
                'sku' => $notificacao->artwTicket->sku
            ];

            $retorno = Mail::send('emails.notificacoes.' . $notificacao->artwNotificacaoTipo->template, $data, function ($message) use($data) {

                    //$message->from('naoresponder@catalogoambev.com.br', 'Catálogo Ambev');
                    $message->from(config('app.mail_from'), config('app.mail_name'));
                    
                    if (str_contains(request()->url(), 'dev') || str_contains(request()->url(), 'modelo') || str_contains(request()->url(), 'stg')  || str_contains(request()->url(), 'hml') || str_contains(request()->url(), 'localhost') ) {
                        
                        $message
                            //->to('bruno@paintpack.com.br')
                            ->to('bruno@rubrum.com.br')
                            ->to('vitor@rubrum.com.br')
                            ->to('thiago@rubrum.com.br')
                            ->subject($data['notificacao']->assunto);
                        
                    } else {
                        
                        $message->to($data['notificacao']->to->email)->subject($data['notificacao']->assunto);
                        
                    }
                    
                    
                });

            if ($retorno >= 1) {

                $envio = New ArtwNotificacoesEnvios;
                $envio->status = $retorno;
                $envio->id_notificacao = $notificacao->id;
                $envio->save();
            }
        }
    }
}
