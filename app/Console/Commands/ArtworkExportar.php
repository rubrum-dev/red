<?php
namespace App\Console\Commands;

use Illuminate\Support\Facades\Storage;
use Chumper\Zipper\Zipper;
use App\Familias;
use App\Produtos;
use App\ArtwItens;
use App\Components\Common;
use Illuminate\Console\Command;
use File;
class ArtworkExportar extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'artwork:exportar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Exporta os dados';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function delTree($dir) { 
        $files = array_diff(scandir($dir), array('.','..')); 
        foreach ($files as $file) { 
          (is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file"); 
        } 
        return rmdir($dir); 
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Common $common)
    {
        $caminhoExportar = 'public/images/uploads/exportar';
        
        if(file_exists($caminhoExportar))
        {
            $this->delTree($caminhoExportar);
        }
       
        File::makeDirectory($caminhoExportar, 0755, true);

        $allProdutos = Produtos
            ::select('produtos.*')
            ->join('skus', 'skus.id_produto', '=', 'produtos.id')
            ->join('artw_variacoes', 'artw_variacoes.id_sku', '=', 'skus.id')
            //->join('artw_variacao_itens', 'artw_variacao_itens.id_variacao', '=', 'artw_variacoes.id')
            ->where('produtos.status', 1)
            ->get();

        $familias = [];

        // Marcas (Famílias)
        foreach ($allProdutos as $produto) {
            $familias[$produto->familias->id]['nome'] = $produto->familias->nome;
        }       
        
        foreach ($familias as $idFamilia=>$familia)
        {
            $caminhoFamilia = $caminhoExportar . '/' . $familia['nome'];

            if(!file_exists($caminhoFamilia))
            {
                File::makeDirectory($caminhoFamilia, 0755, true);
            }

            // Produtos
            $allProdutos = Produtos
                ::select('produtos.*')
                ->join('skus', 'skus.id_produto', '=', 'produtos.id')
                ->join('artw_variacoes', 'artw_variacoes.id_sku', '=', 'skus.id')
                //->join('artw_variacao_itens', 'artw_variacao_itens.id_variacao', '=', 'artw_variacoes.id')
                ->where('produtos.status', 1)
                ->where('id_familia', $idFamilia)
                ->get();

            foreach ($allProdutos as $produto)
            {
                $caminhoProduto = $caminhoFamilia . '/' . $produto->nome;

                if(!file_exists($caminhoProduto))
                {
                    File::makeDirectory($caminhoProduto, 0755, true);
                }

                //dd($produto);

                // Embalagens de Mercado
                $embalagensAtivas = $common->getEmbalagens($produto->id, 1);
                $embalagensInativas = $common->getEmbalagens($produto->id, 2);

                $todasEmbalagens = [];

                foreach ($embalagensAtivas['regulares'] as $embalagem)
                {
                    $caminho = $caminhoProduto . '/Embalagens de Mercado/Regulares/' . str_replace('/', '|', $embalagem['nome_original']);
                    
                    $todasEmbalagens[] = [
                        'caminho' => $caminho,
                        'embalagem' => $embalagem
                    ];
                }

                foreach ($embalagensAtivas['promocionais'] as $embalagem)
                {
                    $caminho = $caminhoProduto . '/Embalagens de Mercado/Promocionais/' . str_replace('/', '|', $embalagem['nome_original']);
                    
                    $todasEmbalagens[] = [
                        'caminho' => $caminho,
                        'embalagem' => $embalagem
                    ];
                }

                foreach ($embalagensInativas['regulares'] as $embalagem)
                {
                    $caminho = $caminhoProduto . '/Embalagens Descontinuadas/Regulares/' . str_replace('/', '|', $embalagem['nome_original']);
                    
                    $todasEmbalagens[] = [
                        'caminho' => $caminho,
                        'embalagem' => $embalagem
                    ];
                }

                foreach ($embalagensInativas['promocionais'] as $embalagem)
                {
                    $caminho = $caminhoProduto . '/Embalagens Descontinuadas/Promocionais/' . str_replace('/', '|', $embalagem['nome_original']);
                    
                    $todasEmbalagens[] = [
                        'caminho' => $caminho,
                        'embalagem' => $embalagem
                    ];
                }

                foreach ($todasEmbalagens as $embalagem)
                {
                    $caminhoEmbalagem = $embalagem['caminho'];

                    //dd($caminhoEmbalagem);

                    if(!file_exists($caminhoEmbalagem))
                    {
                        File::makeDirectory($caminhoEmbalagem, 0755, true);
                    }

                    foreach ($embalagem['embalagem']['itens'] as $item)
                    {
                        $caminhoItem = $caminhoEmbalagem . '/' . str_replace('/', '|', $item['nome']);
                        
                        if(!file_exists($caminhoItem))
                        {
                            File::makeDirectory($caminhoItem, 0755, true);
                        }

                        $objItem = ArtwItens::find($item['id']);

                        foreach ($objItem->artwVersoes as $versao)
                        {
                            $caminhoVersao = $caminhoItem . '/Versão ' . $versao->numero;

                            if(!file_exists($caminhoVersao))
                            {
                                File::makeDirectory($caminhoVersao, 0755, true);
                            }

                            if (file_exists(public_path($versao->path)))
                            {
                                $dadosArquivo = pathinfo(public_path($versao->path));

                                copy(public_path($versao->path), $caminhoVersao . '/' . $dadosArquivo['basename']);

                                //dd();
                            }

                            $arteFinal = $versao->artwTicket->artwArtes->last();

                            if ($arteFinal && $arteFinal->path)
                            {
                                //dd($arteFinal->path);

                                if (file_exists(public_path($arteFinal->path)))
                                {
                                    $dadosArquivo = pathinfo(public_path($arteFinal->path));

                                    copy(public_path($versao->path), $caminhoVersao . '/' . $dadosArquivo['basename']);

                                    //dd();
                                }
                            }
                            elseif ($arteFinal && !$arteFinal->path)
                            {
                                if (Storage::disk(config('app.storage'))->exists(config('app.costummer_name') . '/artwork/artes-finais/' . $arteFinal->nome_arquivo))
                                {
                                    $file = Storage::disk(config('app.storage'))->get(config('app.costummer_name') . '/artwork/artes-finais/' . $arteFinal->nome_arquivo);

                                    file_put_contents($caminhoVersao . '/' . $arteFinal->nome_arquivo, $file);
                                }

                                //dd($versao, $arteFinal);
                                //exit;
                            }
                        }
                    }
                }

                //dd($todasEmbalagens);
            }
        }
    }

}
