<?php
namespace App\Console\Commands;

use App\AdmUsuarios;
use Illuminate\Console\Command;

class ArtworkUsuarios extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'artwork:usuarios';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Manutenção de usuários';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' -90 day'));

        $usuarios = AdmUsuarios::whereNotNull('logged_at')->where('logged_at', '<=', $data)->whereNull('expirado_em')->get();

        foreach ($usuarios as $usuario)
        {
            $usuario->expirado_em = date('Y-m-d H:i:s');
            $usuario->save();
        }

        $usuarios = AdmUsuarios::whereNotNull('expirado_em')->where('expirado_em', '<=', $data)->whereNull('arquivado_em')->get();

        foreach ($usuarios as $usuario)
        {
            $usuario->arquivado_em = date('Y-m-d H:i:s');
            $usuario->status = 0;
            $usuario->save();
        }

        $usuarios = AdmUsuarios::whereNull('logged_at')->where('created_at', '<=', $data)->whereNull('expirado_em')->get();

        foreach ($usuarios as $usuario)
        {
            $usuario->expirado_em = date('Y-m-d H:i:s');
            $usuario->save();
        }

        
    }

}
