<?php
namespace App\Console\Commands;

use App\AdmUsuarios;
use App\AdmUsuariosModulos;
use App\ArtwItens;
use App\ArtwNotificacoesTipos;
use App\Configuracoes;
use App\News;
use Mail;
use Illuminate\Console\Command;

class ArtworkUpdate extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'artwork:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atualiza a versão';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $news = News::firstOrNew([
            'id_tipo' => 1,
            'titulo' => '1.8.0'
        ]);

        $news->save();

        $configuracao = Configuracoes::firstOrNew([
            'nome' => 'limite_usuarios',
            'valor' => '50',
        ]);

        $configuracao->save();

        $notificacaoTipo = ArtwNotificacoesTipos::firstOrNew([
            'slug' => 'fornecedor_subiu_pdf'
        ]);

        $notificacaoTipo->nome = 'PDF do Fornecedor Anexado pelo Fornecedor';
        $notificacaoTipo->slug = 'fornecedor_subiu_pdf';
        $notificacaoTipo->template = 'email_notificacao_43';
        $notificacaoTipo->assunto = 'PDF do Fornecedor Anexado pelo Fornecedor';
        $notificacaoTipo->conta_notificacao = 0;
        $notificacaoTipo->save();

        $notificacaoTipo = ArtwNotificacoesTipos::firstOrNew([
            'slug' => 'pdf_fornecedor_anexado'
        ]);

        $notificacaoTipo->nome = 'PDF do Fornecedor Anexado';
        $notificacaoTipo->slug = 'pdf_fornecedor_anexado';
        $notificacaoTipo->template = 'email_notificacao_44';
        $notificacaoTipo->assunto = 'PDF do Fornecedor Anexado';
        $notificacaoTipo->conta_notificacao = 0;
        $notificacaoTipo->save();

    }

}
