<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Components\Dashboard;
use Illuminate\Support\Facades\Auth;

class ArtworkDashboard extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'artwork:dashboard';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atualiza os dados do dashboard';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Dashboard $dashboard)
    {
        $dashboard->atualizaDashboard('tickets-em-andamento');
        $dashboard->atualizaDashboard('tipos-instrucao-abertura-ticket');
        $dashboard->atualizaDashboard('aprovacoes-por-departamento');
        $dashboard->atualizaDashboard('tickets-encerrados-por-ciclo');
        $dashboard->atualizaDashboard('embalagens');
        $dashboard->atualizaDashboard('usuarios');

        if (Auth::user())
        {

            $dashboard->atualizaDashboard('tickets-em-andamento-usuario');
            $dashboard->atualizaDashboard('minhas-funcoes-tickets-andamento-atrasado');
            $dashboard->atualizaDashboard('tickets-encerrados-por-ciclo-usuario');

        }
    }
}
