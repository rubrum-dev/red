<?php

namespace App\Console\Commands;

use App\ArtwCiclosLayout;
use App\ArtwFornecedorPdfs;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ArtworkRenderCiclo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'artwork:render-ciclo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Renderiza arquivos de Ciclo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sql = "(
                SELECT
                    'ciclo' AS tipo,
                    artw_ciclos_layout.id,
                    artw_ciclos_layout.path,
                    artw_ciclos_layout.created_at,
                    artw_ciclos_layout.renderizado
                FROM
                    artw_ciclos_layout
                WHERE
                    artw_ciclos_layout.indexado = 0 AND
                    artw_ciclos_layout.deleted_at IS NULL
                GROUP BY
                    artw_ciclos_layout.id_ciclo
            )
            UNION ALL
                (
                    SELECT
                        'fornecedorpdf' AS tipo,
                        artw_fornecedor_pdfs.id,
                        artw_fornecedor_pdfs.path,
                        artw_fornecedor_pdfs.created_at,
                        artw_fornecedor_pdfs.renderizado
                    FROM
                        artw_fornecedor_pdfs
                    WHERE
                        artw_fornecedor_pdfs.indexado = 0
                )
            ORDER BY
                created_at ASC
            LIMIT 1";
        
        $arquivos = DB::select($sql);
        
        foreach ($arquivos as $arquivo){
            
            // Manda renderizar
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36");
            
            if ($arquivo->renderizado)
            {
                curl_setopt($ch, CURLOPT_URL, route('api.reRender', [$arquivo->tipo, $arquivo->id]));
            }
            else
            {
                curl_setopt($ch, CURLOPT_URL, route('api.render', [$arquivo->tipo, $arquivo->id]));
            }

            //curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_TIMEOUT_MS, 500);

            curl_exec($ch);

            if (curl_errno($ch)) {
                $error_message = curl_error($ch);
                //echo "<br>erreur:" . $error_message;
            }

            curl_close($ch);
            
            echo "arquivo: " . $arquivo->path . " enviado pra renderização\n";

            // Marca como indexado
            if ($arquivo->tipo == 'ciclo')
            {

                $arquivoCiclo = ArtwCiclosLayout::find($arquivo->id);
                $arquivoCiclo->indexado = 1;
                $arquivoCiclo->renderizado = 1;
                $arquivoCiclo->save();

            } else {
                
                $arquivoFornecedorPdf = ArtwFornecedorPdfs::find($arquivo->id);
                $arquivoFornecedorPdf->indexado = 1;
                $arquivoFornecedorPdf->renderizado = 1;
                $arquivoFornecedorPdf->save();

            }
        }
    }
    
    
}
