<?php
namespace App\Console\Commands;

use App\ArtwArtes;
use App\ArtwCiclosLayout;
use App\ArtwFornecedorPdfs;
use App\ArtwVersoes;
use App\ArtwArquivos;
use Mail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ArtworkLimpeza extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'artwork:limpeza';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Faz limpeza no disco e banco de dados';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // PDFs de Ciclo
        $caminhoPdfs = '/images/uploads/artwork/ciclo/';
        $caminhoLixeira = '/images/uploads/lixeira/artworkCiclo/';

        if (!is_dir(public_path($caminhoLixeira))) {
            mkdir(public_path($caminhoLixeira), 0777, true);
        }

        // Abre o diretório
        if ($handle = opendir(public_path($caminhoPdfs))) {
            // Lê cada entrada no diretório
            while (false !== ($entry = readdir($handle))) {
                // Ignora as entradas "." e ".."
                if ($entry != "." && $entry != "..") {

                    $apagar = 0;

                    // Exibe o nome do arquivo
                    $path = $caminhoPdfs . $entry;

                    // verifica se existe no banco
                    $verifica = ArtwCiclosLayout::where('path', $path)->withTrashed()->get()->first();

                    if ($verifica && !$verifica->deleted_at)
                    {
                        //echo " - existe no banco";
                        $apagar = 0;
                    }
                    elseif ($verifica && $verifica->deleted_at)
                    {
                        echo "PDF de Ciclo | " . $path;
                        echo " - existe no banco | DELETADO - APAGAR ARQUIVO";
                        $apagar = 1;
                    }
                    else
                    {
                        echo "PDF de Ciclo | " . $path;
                        echo " - não existe no banco | APAGAR ARQUIVO";
                        $apagar = 1;
                    }

                    if ($apagar)
                    {
                        // APAGA O ARQUIVO - CUIDADO
                        if (is_file(public_path($path)))
                        {
                            //unlink(public_path($path));
                            rename(public_path($path), public_path($caminhoLixeira . $entry));
                            echo " | MOVIDO PRA LIXEIRA!!!";
                            echo "\n";
                        }
                    }
                }
            }
            // Fecha o diretório
            closedir($handle);
        }
        
        // PDFs de Fornecedor
        $caminhoPdfs = '/images/uploads/artwork/ticket/fornecedorPdf/';
        $caminhoLixeira = '/images/uploads/lixeira/artworkFornecedor/';

        if (!is_dir(public_path($caminhoLixeira))) {
            mkdir(public_path($caminhoLixeira), 0777, true);
        }

        // Abre o diretório
        if ($handle = opendir(public_path($caminhoPdfs))) {
            // Lê cada entrada no diretório
            while (false !== ($entry = readdir($handle))) {
                // Ignora as entradas "." e ".."
                if ($entry != "." && $entry != "..") {

                    $apagar = 0;

                    // Exibe o nome do arquivo
                    $path = $caminhoPdfs . $entry;

                    // verifica se existe no banco
                    $verifica = ArtwFornecedorPdfs::where('path', $path)->where('status', '!=', 2)->get()->last();

                    //dd($verifica);

                    if ($verifica)
                    {
                        //echo " - existe no banco";
                        $apagar = 0;
                    }
                    else
                    {
                        echo "PDF de Fornecedor | " . $path;
                        echo " - não existe no banco | APAGAR ARQUIVO";
                        $apagar = 1;
                    }

                    if ($apagar)
                    {
                        // APAGA O ARQUIVO - CUIDADO
                        if (is_file(public_path($path)))
                        {
                            //unlink(public_path($path));
                            rename(public_path($path), public_path($caminhoLixeira . $entry));
                            echo " | MOVIDO PRA LIXEIRA!!!";
                            echo "\n";
                        }
                    }
                }
            }
            // Fecha o diretório
            closedir($handle);
        }

        // Versões
        $caminhoPdfs = '/images/uploads/artes-finais/versoes/';
        $caminhoLixeira = '/images/uploads/lixeira/artworkVersoes/';

        if (!is_dir(public_path($caminhoLixeira))) {
            mkdir(public_path($caminhoLixeira), 0777, true);
        }

        // Abre o diretório
        if ($handle = opendir(public_path($caminhoPdfs))) {
            // Lê cada entrada no diretório
            while (false !== ($entry = readdir($handle))) {
                // Ignora as entradas "." e ".."
                if ($entry != "." && $entry != "..") {

                    $apagar = 0;

                    // Exibe o nome do arquivo
                    $path = $caminhoPdfs . $entry;

                    // verifica se existe no banco
                    $verifica = ArtwVersoes::where('path', $path)->get()->last();

                    //dd($verifica);

                    if ($verifica)
                    {
                        //echo " - existe no banco";
                        $apagar = 0;
                    }
                    else
                    {
                        echo "PDF de Versão | " . $path;
                        echo " - não existe no banco | APAGAR ARQUIVO";
                        $apagar = 1;
                    }

                    if ($apagar)
                    {
                        // APAGA O ARQUIVO - CUIDADO
                        if (is_file(public_path($path)))
                        {
                            //unlink(public_path($path));
                            rename(public_path($path), public_path($caminhoLixeira . $entry));
                            echo " | MOVIDO PRA LIXEIRA!!!";
                            echo "\n";
                        }
                    }
                }
            }
            // Fecha o diretório
            closedir($handle);
        }

        // Arte-finais
        $caminhoPdfs = '/images/uploads/artwork/ticket/arte/';
        $caminhoLixeira = '/images/uploads/lixeira/artworkArteFinal/';

        if (!is_dir(public_path($caminhoLixeira))) {
            mkdir(public_path($caminhoLixeira), 0777, true);
        }

        // Abre o diretório
        if ($handle = opendir(public_path($caminhoPdfs))) {
            // Lê cada entrada no diretório
            while (false !== ($entry = readdir($handle))) {
                // Ignora as entradas "." e ".."
                if ($entry != "." && $entry != "..") {

                    $apagar = 0;

                    // Exibe o nome do arquivo
                    $path = $caminhoPdfs . $entry;

                    // verifica se existe no banco
                    $verifica = ArtwArtes::where('path', $path)->get()->last();

                    //dd($verifica);

                    if ($verifica)
                    {
                        //echo " - existe no banco";
                        $apagar = 0;
                    }
                    else
                    {
                        echo "Arte-final | " . $path;
                        echo " - não existe no banco | APAGAR ARQUIVO";
                        $apagar = 1;
                    }

                    if ($apagar)
                    {
                        // APAGA O ARQUIVO - CUIDADO
                        if (is_file(public_path($path)))
                        {
                            //unlink(public_path($path));
                            rename(public_path($path), public_path($caminhoLixeira . $entry));
                            echo " | MOVIDO PRA LIXEIRA!!!";
                            echo "\n";
                        }
                    }
                }
            }
            // Fecha o diretório
            closedir($handle);
        }

        // Arquivos
        $caminhoPdfs = '/images/uploads/artwork/ticket/arquivo/';
        $caminhoLixeira = '/images/uploads/lixeira/artworkArquivos/';

        if (!is_dir(public_path($caminhoLixeira))) {
            mkdir(public_path($caminhoLixeira), 0777, true);
        }

        // Abre o diretório
        if ($handle = opendir(public_path($caminhoPdfs))) {
            // Lê cada entrada no diretório
            while (false !== ($entry = readdir($handle))) {
                // Ignora as entradas "." e ".."
                if ($entry != "." && $entry != "..") {

                    $apagar = 0;

                    // Exibe o nome do arquivo
                    $path = $caminhoPdfs . $entry;

                    // verifica se existe no banco
                    $verifica = ArtwArquivos::where('path', $path)->get()->last();

                    //dd($verifica);

                    if ($verifica)
                    {
                        //echo " - existe no banco";
                        $apagar = 0;
                    }
                    else
                    {
                        echo "Arquivo | " . $path;
                        echo " - não existe no banco | APAGAR ARQUIVO";
                        $apagar = 1;
                    }

                    if ($apagar)
                    {
                        // APAGA O ARQUIVO - CUIDADO
                        if (is_file(public_path($path)))
                        {
                            //unlink(public_path($path));
                            rename(public_path($path), public_path($caminhoLixeira . $entry));
                            echo " | MOVIDO PRA LIXEIRA!!!";
                            echo "\n";
                        }
                    }
                }
            }
            // Fecha o diretório
            closedir($handle);
        }

        





    }
    
}
