<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwGruposPerfisUsuariosEquipe extends Model
{
    protected $table = 'artw_grupos_perfis_usuarios_equipe';
    
    protected $fillable = [
        'id',
        'id_parent',
        'id_usuario',
    ];
    
    protected $hidden = [
        'id'
    ];
 
    public function grupoPerfilUsuario()
    {
        return $this->belongsTo('App\ArtwGruposPerfisUsuarios', 'id_parent');
    }
    
    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario')->withTrashed();
    }
    
}
