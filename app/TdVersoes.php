<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TdVersoes extends Model
{

    protected $fillable = array(
        'id_item',
        'numero',
        'path',
        'status'
    );

    public function item()
    {
        return $this->belongsTo('App\TdItens', 'id_item');
    }
    
    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario');
    }
    
    public function getFilesizeAttribute()
    {
        $file = public_path($this->path);
        
        if (file_exists($file))
        {
            $tamanho = filesize($file) / pow(1024, 2);
            
            return str_replace(".", "," , strval(round($tamanho, 2)));
            
        } else {
            
            return 0;
            
        }
    }
    
}
