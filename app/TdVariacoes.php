<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TdVariacoes extends Model
{
    protected $fillable = [
        'id_techdraw',
        'nome',
        'principal',
        'status'
    ];
    
    public function techdraw()
    {
        return $this->belongsTo('App\TdTechdraws', 'id_techdraw');
    }
    
    public function itens()
    {
        $itens = $this
            ->hasMany('App\TdItens', 'id_variacao')
            ->select('td_itens.*')
            ->join('artw_item_tipos', 'artw_item_tipos.id', '=', 'td_itens.id_tipo')
            ->orderBy('artw_item_tipos.nome');
        
        return $itens;
    }
    
    public function getNomeCompletoAttribute()
    {
        $nome = $this->techdraw->dimensao->tiposEmbalagens->nome . ' ' . $this->techdraw->dimensao->nome;
        
        if ($this->principal !== 1 && $this->nome) {
            $nome .= ' - ' . $this->nome;
        }
        
        return $nome;
    }
    
    public function delete()
    {
        foreach ($this->itens as $item){
            
            $item->delete();
            
        }
        
        return parent::delete();
    }
    
}
