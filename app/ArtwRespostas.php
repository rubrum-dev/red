<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ArtwRespostas
 *
 * @property-read \App\AdmUsuarios $admUsuario
 * @property-read \App\ArtwComentarios $artwComentario
 * @mixin \Eloquent
 */
class ArtwRespostas extends Model
{

    protected $fillable = [
        'id_comentario',
        'id_usuario',
        'texto'
    ];

    public function artwComentario()
    {
        return $this->belongsTo('App\ArtwComentarios', 'id_resposta');
    }
    
    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario')->withTrashed();
    }
    
    public function getTextoAttribute($value)
    {
        return nl2br($value);
    }

}
