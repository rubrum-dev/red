<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArtwCiclosLayout extends Model
{
    use SoftDeletes;

    protected $table = 'artw_ciclos_layout';
    
    protected $fillable = [
        'id_ciclo',
        'id_usuario',
        'nome',
        'path'
    ];
    
    public function artwCiclo()
    {
        return $this->belongsTo('App\ArtwCiclos', 'id_ciclo');
    }
    
    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario')->withTrashed();
    }
    
    public function getFilesizeAttribute()
    {
        if ($this->size)
        {
            $tamanho = $this->size / pow(1024, 2);
                
            return str_replace(".", "," , strval(round($tamanho, 2)));

        } else {

            $file = public_path($this->path);
        
            if (file_exists($file))
            {
                $tamanho = filesize($file) / pow(1024, 2);
                
                return str_replace(".", "," , strval(round($tamanho, 2)));
                
            } else {
                
                return 0;
                
            }

        }
    }

}
