<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwCiclos extends Model
{

    protected $fillable = [
        'id_ticket',
        'id_status',
        'numero',
        'path_layout'
    ];

    public function artwCicloStatus()
    {
        return $this->belongsTo('App\ArtwCicloStatus', 'id_status');
    }
    
    public function artwTicket()
    {
        return $this->belongsTo('App\ArtwTickets', 'id_ticket');
    }

    public function artwSolicitacoes()
    {
        return $this->hasMany('App\ArtwSolicitacoes', 'id_ciclo');
    }
    
    public function artwCiclosLayout()
    {
        return $this->hasMany('App\ArtwCiclosLayout', 'id_ciclo');
    }
    
    public function artwCiclosLayoutAll()
    {
        return $this->hasMany('App\ArtwCiclosLayout', 'id_ciclo')->withTrashed();
    }

}
