<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\TiposLogotipos
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Logotipos[] $logotipos
 * @mixin \Eloquent
 */
class TiposLogotipos extends Model
{
    public function logotipos()
    {
    	return $this->hasMany('App\Logotipos', 'id_tipo_logotipo');
    }
}
