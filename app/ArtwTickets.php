<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class ArtwTickets extends Model
{

    use SoftDeletes;

    protected $fillable = array(
        'id_sku',
        'id_status',
        'id_variacao',
        'numero',
        'descricao',
        'dt_ini',
        'dt_fim',
        'dt_envio'
    );
    protected $dates = [
        'dt_ini',
        'dt_fim',
        'dt_envio',
        'dt_envio_finalizado',
        'dt_envio_autorizado',
        'cancelado_em',
        'pausado_em'
    ];

    public function artwComentarios()
    {
        return $this->hasMany('App\ArtwComentarios', 'id_ticket')->orderBy('created_at', 'DESC');
    }

    public function sku()
    {
        return $this->belongsTo('App\Skus', 'id_sku');
    }

    public function projeto()
    {
        return $this->belongsTo('App\ArtwProjetos', 'id_projeto');
    }

    public function artwVersoes()
    {
        return $this->hasMany('App\ArtwVersoes', 'id_ticket');
    }

    public function usuarioFinalizador()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_envio_finalizado')->withTrashed();
    }

    public function usuarioAutorizador()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_envio_autorizado')->withTrashed();
    }

    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario')->withTrashed();
    }

    public function canceladoPor()
    {
        return $this->belongsTo('App\AdmUsuarios', 'cancelado_por')->withTrashed();
    }

    public function pausadoPor()
    {
        return $this->belongsTo('App\AdmUsuarios', 'pausado_por')->withTrashed();
    }

    public function artwCiclos()
    {
        return $this->hasMany('App\ArtwCiclos', 'id_ticket');
    }

    public function ciclo_atual()
    {
        return $this->artwCiclos()->where('id_status', '>=', 1)->orderBy('numero', 'DESC')->limit(1)->get()->last();
    }
    
    public function fornecedorPdfs()
    {
        return $this->hasMany('App\ArtwFornecedorPdfs', 'id_ticket');
    }

    public function artwArquivos()
    {
        return $this->hasMany('App\ArtwArquivos', 'id_ticket');
    }

    public function artwArtes()
    {
        return $this->hasMany('App\ArtwArtes', 'id_ticket');
    }

    public function artwArtesEnvios()
    {
        return $this->hasMany('App\ArtwArtesEnvios', 'id_ticket');
    }

    public function artwTicketsStatus()
    {
        return $this->hasMany('App\ArtwTicketsStatus', 'id_ticket');
    }

    public function artwTktStatus()
    {
        return $this->belongsTo('App\ArtwTktStatus', 'id_status');
    }

    public function artwPerfisUsuariosTickets()
    {
        return $this->hasMany('App\ArtwPerfisUsuariosTickets', 'id_ticket');
    }

    public function participantesPorCategoria($categoria, $id_usuario = null, $id_membro = null)
    {
        if ($categoria == 'todos') {
            $participantes = $this->artwPerfisUsuariosTickets()
                ->select('artw_perfis_usuarios_tickets.*')
                ->join('artw_perfis_usuarios', 'artw_perfis_usuarios.id', '=', 'artw_perfis_usuarios_tickets.id_perfil')
                ->orderBy('artw_perfis_usuarios.ordem')
                ->get();
        } elseif ($id_membro) {
            $participantes = ArtwPerfisUsuariosEquipeTickets::
                select('artw_perfis_usuarios_equipe_tickets.*', 'artw_perfis_usuarios_tickets.aprovado as aprovado')
                //->select('artw_perfis_usuarios_tickets.aprovado')
                ->join('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id', '=', 'artw_perfis_usuarios_equipe_tickets.id_parent')
                ->where('artw_perfis_usuarios_equipe_tickets.id_usuario', $id_membro)
                ->where('artw_perfis_usuarios_tickets.id_ticket', $this->id)
                ->groupBy('artw_perfis_usuarios_equipe_tickets.id_usuario')
                ->get();
        } elseif ($categoria == 'usuario') {
            $participantes = $this->artwPerfisUsuariosTickets()
                ->select('artw_perfis_usuarios_tickets.*')
                ->join('artw_perfis_usuarios', 'artw_perfis_usuarios.id', '=', 'artw_perfis_usuarios_tickets.id_perfil')
                ->where('artw_perfis_usuarios_tickets.id_usuario', $id_usuario)
                ->orderBy('artw_perfis_usuarios.ordem')
                ->groupBy('artw_perfis_usuarios_tickets.id_usuario')
                ->get();
        } elseif ($categoria == 'seguidores') {
            $participantes = $this->artwPerfisUsuariosTickets()
                ->select('artw_perfis_usuarios_tickets.*')
                ->join('artw_perfis_usuarios', 'artw_perfis_usuarios.id', '=', 'artw_perfis_usuarios_tickets.id_perfil')
                ->where('artw_perfis_usuarios_tickets.recebe_email', 1)
                ->orderBy('artw_perfis_usuarios.ordem')
                ->get();
        } else {
            switch ($categoria) {
                case 'revisores':
                    $id_perfil = 3;
                    break;
                case 'aprovadores':
                    $id_perfil = 1;
                    break;
                case 'arte-finalistas':
                    $id_perfil = 5;
                    break;
                case 'participantes':
                    $id_perfil = 4;
                    break;
                case 'marketings':
                    $id_perfil = 2;
                    break;
                case 'envio_arte':
                    $id_perfil = 6;
                    break;
                case 'pdf-fornecedor':
                    $id_perfil = 7;
                    break;
                case 'adsmart-anexadores':
                    $id_perfil = 9;
                    break;
                case 'adsmart-revisores':
                    $id_perfil = 10;
                    break;
                case 'adsmart-aprovadores':
                    $id_perfil = 11;
                    break;
            }

            $participantes = $this->artwPerfisUsuariosTickets()
                ->select('artw_perfis_usuarios_tickets.*')
                ->join('artw_perfis_usuarios', 'artw_perfis_usuarios.id', '=', 'artw_perfis_usuarios_tickets.id_perfil')
                ->where('artw_perfis_usuarios_tickets.id_perfil', $id_perfil)
                ->orderBy('artw_perfis_usuarios.ordem')
                ->get();
        }

        //dd($categoria, $participantes);

        $return = [];

        $n = 0;
        
        foreach ($participantes as $p => $participante) {

            if ($participante->admUsuario) {
                $return[$n] = $participante->admUsuario;

                $return[$n]->id_perfil = $participante->id_perfil;
                $return[$n]->aprovado = $participante->aprovado;
                $return[$n]->ciclo = $participante->ciclo;

                if ($categoria == 'seguidores') {
                    $participantesEquipe = ArtwPerfisUsuariosEquipeTickets::
                        select('artw_perfis_usuarios_equipe_tickets.*')
                        ->select('artw_perfis_usuarios_tickets.aprovado')
                        ->join('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id', '=', 'artw_perfis_usuarios_equipe_tickets.id_parent')
                        ->where('artw_perfis_usuarios_tickets.id_ticket', $this->id)
                        ->where('artw_perfis_usuarios_equipe_tickets.recebe_email', 1)
                        ->get();
                } elseif ($categoria == 'usuario') {
                    $participantesEquipe = [];
                } else {
                    $participantesEquipe = $participante->artwPerfisUsuariosEquipeTickets;
                }
                
                $n++;

                foreach ($participantesEquipe as $e => $equipe) {

                    if (!$equipe->admUsuario) {
                        //dd($equipe);
                    }
                    
                    $return[$n] = $equipe->admUsuario;
                    $return[$n]->id_perfil = $participante->id_perfil;
                    $return[$n]->aprovado = $participante->aprovado;
                    $return[$n]->ciclo = $participante->ciclo;

                    $n++;
                }
            }
        }

        return collect($return);
    }

    public function participantes()
    {
        $participantes = $this->artwPerfisUsuariosTickets()
                ->withTrashed()
                ->select('artw_perfis_usuarios_tickets.*', 'artw_perfis_usuarios.ordem')
                ->join('artw_perfis_usuarios', 'artw_perfis_usuarios.id', '=', 'artw_perfis_usuarios_tickets.id_perfil')
                ->join('adm_usuarios', 'artw_perfis_usuarios_tickets.id_usuario', '=', 'adm_usuarios.id')
                ->orderBy('artw_perfis_usuarios.ordem')
                ->orderBy('adm_usuarios.nome')
                ->orderBy('adm_usuarios.sobrenome')
                ->get();
        
        return $participantes;
        //exit;
    }
    
    public function participantesPorPerfil($id_perfil)
    {
        return $this->artwPerfisUsuariosTickets()
                ->select('artw_perfis_usuarios_tickets.*')
                ->join('artw_perfis_usuarios', 'artw_perfis_usuarios.id', '=', 'artw_perfis_usuarios_tickets.id_perfil')
                ->join('adm_usuarios', 'artw_perfis_usuarios_tickets.id_usuario', '=', 'adm_usuarios.id')
                ->where('artw_perfis_usuarios_tickets.id_perfil', $id_perfil)
                ->where('adm_usuarios.deleted_at', null)
                ->orderBy('artw_perfis_usuarios.ordem')
                ->orderBy('adm_usuarios.nome')
                ->orderBy('adm_usuarios.sobrenome')
                ->get();
    }

    public function participantesPorPerfilWithTrashed($id_perfil)
    {
        return $this->artwPerfisUsuariosTickets()
                ->withTrashed()
                ->select('artw_perfis_usuarios_tickets.*')
                ->join('artw_perfis_usuarios', 'artw_perfis_usuarios.id', '=', 'artw_perfis_usuarios_tickets.id_perfil')
                ->join('adm_usuarios', 'artw_perfis_usuarios_tickets.id_usuario', '=', 'adm_usuarios.id')
                ->where('artw_perfis_usuarios_tickets.id_perfil', $id_perfil)
                ->where('adm_usuarios.deleted_at', null)
                ->orderBy('artw_perfis_usuarios.ordem')
                ->orderBy('adm_usuarios.nome')
                ->orderBy('adm_usuarios.sobrenome')
                ->get();
    }

    public function participantesUnicos()
    {
        $membros = AdmUsuarios::select('adm_usuarios.id', 'adm_usuarios.nome as usuario_nome', 'adm_usuarios.sobrenome as usuario_sobrenome', 'adm_usuarios.deleted_at', 'adm_usuarios.status', 'adm_usuarios.id_adm_empresa', 'adm_usuarios.id_departamento')
            ->join('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_usuario', '=', 'adm_usuarios.id')
            ->where('artw_perfis_usuarios_tickets.id_ticket', $this->id)
            ->distinct();

        $participantesUnicos = AdmUsuarios::select('adm_usuarios.id', 'adm_usuarios.nome as usuario_nome', 'adm_usuarios.sobrenome as usuario_sobrenome', 'adm_usuarios.deleted_at', 'adm_usuarios.status', 'adm_usuarios.id_adm_empresa', 'adm_usuarios.id_departamento')
            ->join('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_usuario', '=', 'adm_usuarios.id')
            ->join('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
            ->where('artw_perfis_usuarios_tickets.id_ticket', $this->id)
            ->union($membros)
            ->distinct()
            ->orderBy('usuario_nome')
            ->orderBy('usuario_sobrenome')
            ->get();

        return $participantesUnicos;
    }

    public function participantesAprovados($id_perfil, $numero_ciclo)
    {
        return $this->artwPerfisUsuariosTickets()
                ->where('id_perfil', $id_perfil)
                ->where('ciclo', $numero_ciclo)
                ->whereIn('aprovado', [1])
                ->get();
    }

    public function participantesPendentes($id_perfil, $numero_ciclo)
    {
        return $this->artwPerfisUsuariosTickets()
                ->select('artw_perfis_usuarios_tickets.*')
                ->join('adm_usuarios', 'artw_perfis_usuarios_tickets.id_usuario', '=', 'adm_usuarios.id')
                ->where('id_perfil', $id_perfil)
                ->where('ciclo', '!=', $numero_ciclo)
                ->whereIn('aprovado', [0, 2])
                ->where('artw_perfis_usuarios_tickets.deleted_at', null)
                ->where('adm_usuarios.deleted_at', null)
                ->get();
    }

    public function participantesReprovados($id_perfil, $numero_ciclo)
    {
        return $this->artwPerfisUsuariosTickets()
                ->where('id_perfil', $id_perfil)
                ->where('ciclo', $numero_ciclo)
                ->whereIn('aprovado', [2])
                ->get();
    }

    public function participantesTodos($id_perfil)
    {
        return $this->artwPerfisUsuariosTickets()
                ->where('id_perfil', $id_perfil)->get();
    }

    public function artwPerfilUsuarioTicket($id_usuario, $id_perfil)
    {
        return $this->artwPerfisUsuariosTickets()
                ->where('id_usuario', $id_usuario)
                ->where('id_perfil', $id_perfil)
                ->get()
                ->last();
    }

    public function checkUsuario($id_usuario)
    {
        return $this->artwPerfisUsuariosTickets()
                ->where('id_usuario', $id_usuario)
                ->get()
                ->last();
    }

    public function artwVariacao()
    {
        return $this->belongsTo('App\ArtwVariacoes', 'id_variacao')->withTrashed();
    }

    public function artwItem()
    {
        return $this->belongsTo('App\ArtwItens', 'id_item');
    }

    public function familia()
    {
        return $this->belongsTo('App\Familias', 'id_familia');
    }

    public function adsmartPeca()
    {
        return $this->belongsTo('App\AdsmartPecas', 'id_peca');
    }

    public function setDtIniAttribute($value)
    {
        $this->attributes['dt_ini'] = Carbon::createFromFormat('d/m/Y H:i:s', $value . ' 00:00:00')->toDateTimeString('Y-m-d H:i:s');
    }

    public function setDtFimAttribute($value)
    {
        $this->attributes['dt_fim'] = Carbon::createFromFormat('d/m/Y H:i:s', $value . ' 20:59:59')->toDateTimeString('Y-m-d H:i:s');
    }

    public function setDtEnvioAttribute($value)
    {
        $this->attributes['dt_envio'] = Carbon::createFromFormat('d/m/Y H:i:s', $value . ' 20:59:59')->toDateTimeString('Y-m-d H:i:s');
    }

    public function getSkuNomeCompletoAttribute()
    {
        if ($this->artwItem && $this->artwItem->compartilhado) {
            
            $nome = $this->artwItem->nome_completo;
            
        } elseif ($this->artwItem) {
            
            $nome = $this->artwItem->sku_nome_completo;
            
        } else {
            $nome = '--';
        }
        
        return $nome;
    }

    public function getAdsmartNomeCompletoAttribute()
    {
        $nome = $this->adsmartPeca->nome_completo;
        
        return $nome;
    }
    
    public function getCanceladoNomeOriginalAttribute($nome)
    {
        return str_replace(' - ', ' • ', $nome);
    }

    public function getDescricaoHtmlAttribute()
    {
        return nl2br($this->descricao);
    }

    public function getDescricaoAttribute($value)
    {
        return str_replace('<br />', '', $value);
    }

    public function isOpen()
    {
        $statusAbertos = [1, 2, 3, 4, 5, 10, 11, 12];

        if (in_array($this->id_status, $statusAbertos)) {
            
            return true;
            
        } else {
            
            return false;
            
        }
    }
}
