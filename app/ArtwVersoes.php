<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwVersoes extends Model
{

    protected $fillable = array(
        'id_sku',
        'id_variacao',
        'id_item',
        'numero',
        'path'
    );

    public function sku()
    {
        return $this->belongsTo('App\Skus', 'id_sku');
    }

    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario')->withTrashed();
    }

    public function artwItem()
    {
        return $this->belongsTo('App\ArtwItens', 'id_item');
    }

    public function artwTicket()
    {
        return $this->belongsTo('App\ArtwTickets', 'id_ticket');
    }
    
    public function getFilesizeAttribute()
    {
        $file = public_path($this->path);
        
        if (file_exists($file))
        {
            $tamanho = filesize($file) / pow(1024, 2);
            
            return str_replace(".", "," , strval(round($tamanho, 2)));
            
        } else {
            
            return 0;
            
        }
    }

}
