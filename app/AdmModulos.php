<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmModulos extends Model
{
    public function admUsuarios()
    {
    	return $this->belongsToMany('App\AdmUsuarios', 'adm_usuarios_modulos', 'id_adm_modulo', 'id_adm_usuario');
    }

    public function admLogNovidades()
    {
    	return $this->hasMany('App\AdmLogNovidades', 'id_modulo');
    }
}
