<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmCargos extends Model
{
	
    protected $fillable = array('nome');

    public function admUsuarios()
    {
    	return $this->hasMany('App\AdmUsuarios', 'id_cargo')->withTrashed();
    }
}
