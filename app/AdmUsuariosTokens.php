<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmUsuariosTokens extends Model
{
    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario');
    }
}
