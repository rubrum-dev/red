<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposProporcoesVersoes extends Model
{
    public function versoes()
    {
    	return $this->hasMany('App\Versoes', 'id_tipo_proporcao_versao');
    }

    public function proporcoes()
    {
    	return $this->hasMany('App\Proporcoes', 'id_tipo_proporcao_versao');
    }
}
