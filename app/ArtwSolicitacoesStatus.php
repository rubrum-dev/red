<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ArtwSolicitacoesStatus
 *
 * @property-read \App\AdmUsuarios $admUsuario
 * @mixin \Eloquent
 */
class ArtwSolicitacoesStatus extends Model
{
    protected $table = 'artw_solicitacoes_status';
    
    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario')->withTrashed();
    }
}
