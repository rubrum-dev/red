<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwProjetos extends Model
{
    protected $fillable = [
        'nome'
    ];
}
