<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelMesesNumeros extends Model
{
    protected $fillable = [
		'nome',
        'id_mes'
    ];

    protected $hidden = [
        'id',
        'id_mes',
        'created_at',
        'updated_at'
    ];

    public function getPorcentagemAttribute($value)
    {
        return number_format($value, 2, ',', '');
    }
    
}
