<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwItemTipos extends Model
{
    protected $fillable = [
        'nome'
    ];
    
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function artwItens()
    {
        return $this->hasMany('App\ArtwItens', 'id_tipo');
    }
    
}
