<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwGruposPerfis extends Model
{
    
    protected $fillable = [
        'id',
        'nome',
        'global',
        'id_usuario',
    ];
    
    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario')->withTrashed();
    }
    
    public function artwGruposPerfisUsuarios()
    {
        return $this->hasMany('App\ArtwGruposPerfisUsuarios', 'id_grupo_perfil');
    }
    
}
