<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwGruposPerfisUsuarios extends Model
{
    
    protected $fillable = [
        'id',
        'id_grupo_perfil',
        'id_usuario',
        'id_perfil',
    ];
    
    public function grupoPerfil()
    {
        return $this->belongsTo('App\ArtwGruposPerfis', 'id_grupo_perfil');
    }
    
    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario')->withTrashed();
    }
    
    public function perfilUsuario()
    {
        return $this->belongsTo('App\ArtwPerfisUsuarios', 'id_perfil');
    }
    
    public function artwGruposPerfisUsuariosEquipe()
    {
        return $this->hasMany('App\ArtwGruposPerfisUsuariosEquipe', 'id_parent');
    }
    
}
