<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwVariacaoTipos extends Model
{
    protected $fillable = [
        'nome'
    ];

    public function artwVariacoes()
    {
        return $this->hasMany('App\ArtwVariacoes', 'id_tipo');
    }
}
