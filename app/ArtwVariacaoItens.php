<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwVariacaoItens extends Model
{

    protected $fillable = [
        'id_item',
        'id_variacao'
    ];
    
}
