<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArtwArtesEnvios extends Model
{
    use SoftDeletes;
    
    protected $dates = [
        'dt_expiracao',
        'cancelado_em'
    ];
    
    public function artwArte()
    {
        return $this->belongsTo('App\ArtwArtes', 'id_arte')->withTrashed();
    }
    
    public function artwTicket()
    {
        return $this->belongsTo('App\ArtwTickets', 'id_ticket')->withTrashed();
    }
    
    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario')->withTrashed();
    }

    public function canceladoPor()
    {
        return $this->belongsTo('App\AdmUsuarios', 'cancelado_por')->withTrashed();
    }
    
    public function envioEmails()
    {
        return $this->hasMany('App\ArtwArtesEnviosEmails', 'id_arte_envio');
    }
    
    public function fornecedor()
    {
        return $this->belongsTo('App\Fornecedores', 'id_fornecedor')->withTrashed();
    }
    
    public function getDownloadsAttribute()
    {
        $downloads = $this->envioEmails->sum('downloads_arte');
        
        return $downloads;
    }
    
}
