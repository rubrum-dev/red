<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Familias
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\AdmPerfis[] $admPerfis
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Logotipos[] $logotipos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Produtos[] $produtos
 * @mixin \Eloquent
 */
class Familias extends Model
{
    protected $fillable = array(
		'id_logotipo',
        'nome',
		'ordem',
		'status',
        'cor_hexa'
	);

	public function logotipos()
	{
		return $this->belongsToMany('App\Logotipos', 'familias_logotipos', 'id_familia', 'id_logotipo');
	}

	public function admPerfis()
    {
    	return $this->belongsToMany('App\AdmPerfis', 'adm_perfis_familias', 'id_familia', 'id_adm_perfil');
    }

    public function produtos()
    {
    	return $this->hasMany('App\Produtos', 'id_familia');
    }
}
