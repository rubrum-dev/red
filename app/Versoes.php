<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Versoes
 *
 * @property-read \App\CategoriasVersoes $categoriasVersoes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Produtos[] $produtos
 * @property-read \App\TiposProporcoesVersoes $tiposProporcoesVersoes
 * @mixin \Eloquent
 */
class Versoes extends Model
{
	protected $fillable = array(
		'id_categoria_versao',
		'id_tipo_proporcao_versao',
		'descricao',
		'thumb',
		'path_down',
		'status'
	);

    public function produtos()
	{
		return $this->belongsToMany('App\Produtos', 'produtos_versoes', 'id_versao', 'id_produto');
	}

	public function tiposProporcoesVersoes()
    {
    	return $this->belongsTo('App\TiposProporcoesVersoes', 'id_tipo_proporcao_versao');
    }

    public function categoriasVersoes()
    {
    	return $this->belongsTo('App\CategoriasVersoes', 'id_categoria_versao');
    }
}
