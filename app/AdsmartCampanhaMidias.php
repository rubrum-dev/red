<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdsmartCampanhaMidias extends Model
{
    protected $fillable = [
        'id_campanha',
        'id_midia_tipo'
    ];

    public function adsmartCampanha()
    {
        return $this->belongsTo('App\AdsmartCampanhas', 'id_campanha');
    }

    public function adsmartMidiaTipo()
    {
        return $this->belongsTo('App\AdsmartMidiaTipos', 'id_midia_tipo');
    }
}
