<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\TiposEmbalagens
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Dimensoes[] $dimensoes
 * @property-read \App\NaturezaEmbalagens $naturezaEmbalagens
 * @mixin \Eloquent
 */
class TiposEmbalagens extends Model
{
	protected $fillable = array(
		'nome',
		'ordem',
		'status',
		'id_natureza_embalagem'
	);

    public function naturezaEmbalagens()
    {
    	return $this->belongsTo('App\NaturezaEmbalagens', 'id_natureza_embalagem');
    }

    public function dimensoes()
    {
    	return $this->hasMany('App\Dimensoes', 'id_tipo_embalagem');
    }
}
