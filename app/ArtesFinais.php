<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtesFinais extends Model
{

    protected $fillable = array(
        'id_sku',
        'id_variacao',
        'nome',
        'nome_arte',
        'thumb',
        'path_cmyk',
        'path_rgb',
        'promocional',
        'dt_expiracao',
        'link_pdf',
        'link_zip',
        'arte_exibicao',
        'status'
    );

    public function skus()
    {
        return $this->belongsTo('App\Skus', 'id_sku');
    }

    public function artwVariacao()
    {
        return $this->belongsTo('App\ArtwVariacoes', 'id_variacao');
    }
}
