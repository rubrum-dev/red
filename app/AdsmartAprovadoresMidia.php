<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdsmartAprovadoresMidia extends Model
{
    protected $table = "adsmart_aprovadores_midia";

    protected $fillable = [
        'nome'
    ];

    public function usuarios()
    {
        return $this->belongsToMany('App\AdmUsuarios', 'adsmart_aprovadores_midia_usuarios', 'id_aprovador_midia', 'id_usuario');
    }
}
