<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwNotificacaoTipos extends Model
{
    protected $fillable = [
        'nome',
        'slug'
    ];

    public function artwNotificacoes()
    {
        return $this->hasMany('App\ArtwNotificacoes', 'id_notificacao');
    }
}
