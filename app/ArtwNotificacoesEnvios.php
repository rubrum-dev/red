<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwNotificacoesEnvios extends Model
{
    public function artwNotificacao()
    {
        return $this->belongsTo('App\ArtwNotificacoes', 'id_notificacao');
    }
}
