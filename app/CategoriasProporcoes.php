<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriasProporcoes extends Model
{
    public function proporcoes()
    {
    	return $this->hasMany('App\Proporcoes', 'id_categoria_proporcao');
    }
}
