<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdsmartCampanhas extends Model
{
    protected $fillable = [
        'nome'
    ];
    
    public function familias()
    {
        return $this->belongsToMany('App\Familias', 'adsmart_campanhas_familias', 'id_campanha', 'id_familia');
    }

    public function campanhaMidias()
    {
    	return $this->hasMany('App\AdsmartCampanhaMidias', 'id_campanha');
    }
}
