<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwSolicitacaoTipos extends Model
{

    protected $fillable = [
        'nome'
    ];

    public function artwSolicitacoes()
    {
        return $this->hasMany('App\ArtwSolicitacoes', 'id_solicitacao');
    }

}
