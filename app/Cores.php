<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cores extends Model
{
    protected $fillable = array(
		'id_tipo_cor',
		'id_nivel_cor',
		'nome',
		'spot',
		'cmyk',
		'rgb',
		'hex',
		'descricao',
		'status'
	);

    public function tiposCores()
    {
    	return $this->belongsTo('App\TiposCores', 'id_tipo_cor');
    }

    public function niveisCores()
    {
    	return $this->belongsTo('App\NiveisCores', 'id_nivel_cor');
    }

    public function produtos()
	{
		return $this->belongsToMany('App\Produtos', 'produtos_cores', 'id_cor', 'id_produto');
	}
}
