<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdsmartPecas extends Model
{
    protected $fillable = [
        'nome',
        'id_campanha_midia'
    ];

    public function adsmartCampanhaMidia()
    {
        return $this->belongsTo('App\AdsmartCampanhaMidias', 'id_campanha_midia');
    }

    public function getNomeCompletoAttribute()
    {
        $nome = $this->adsmartCampanhaMidia->adsmartCampanha->nome;

        $nome .= ' • ' . $this->adsmartCampanhaMidia->adsmartMidiaTipo->nome;

        $nome .= ' • ' . $this->nome;

        return $nome;
    }

    public function artwTickets()
    {
        return $this->hasMany('App\ArtwTickets', 'id_peca');
    }
}
