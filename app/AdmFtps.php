<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmFtps extends Model
{
    protected $fillable = array(
		'id_adm_usuario',
		'nome',
		'path'
	);

    public function admUsuarios()
    {
    	return $this->belongsTo('App\AdmUsuarios', 'id_adm_usuario');
    }
}
