<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwFornecedorPdfs extends Model
{
    protected $fillable = [
        'id_ticket',
        'id_fornecedor',
        'id_usuario',
        'nome',
        'nome_arquivo',
        'path',
    ];
    
    protected $dates = [
        'dt_aprovacao',
    ];
    
    public function artwArteEnvio()
    {
        return $this->belongsTo('App\ArtwArtesEnvios', 'id_envio');
    }

    public function artwTicket()
    {
        return $this->belongsTo('App\ArtwTickets', 'id_ticket');
    }
    
    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario')->withTrashed();
    }
    
    public function fornecedor()
    {
        return $this->belongsTo('App\Fornecedores', 'id_fornecedor')->withTrashed();
    }
    
    public function usuarioAprovacao()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_aprovacao')->withTrashed();
    }
    
    public function getFilesizeAttribute()
    {
        $file = public_path($this->path);
        
        if (file_exists($file))
        {
            $tamanho = filesize($file) / pow(1024, 2);
            
            return str_replace(".", "," , strval(round($tamanho, 2)));
            
        } else {
            
            return 0;
            
        }
    }
    
}
