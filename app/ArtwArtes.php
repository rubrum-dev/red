<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArtwArtes extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id_ticket',
        'path',
        'descricao',
    ];

    protected $dates = ['deleted_at'];

    public function artwTicket()
    {
        return $this->belongsTo('App\ArtwTickets', 'id_ticket');
    }
    
    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario')->withTrashed();
    }
    
    public function getFilesizeAttribute()
    {
        if ($this->size)
        {
            $tamanho = $this->size / pow(1024, 2);
                
            return str_replace(".", "," , strval(round($tamanho, 2)));

        } else {

            $file = public_path($this->path);
        
            if (file_exists($file))
            {
                $tamanho = filesize($file) / pow(1024, 2);
                
                return str_replace(".", "," , strval(round($tamanho, 2)));
                
            } else {
                
                return 0;
                
            }
        }
    }
    
    public function getVersaoAttribute()
    {
        if ($this->artwTicket->artwVersoes->count())
        {
            return $this->artwTicket->artwVersoes->last()->numero;
        }
        else
        {
            $versaoAntiga = $this->artwTicket->artwItem->artwVersoes->last();

            if ($versaoAntiga) {
                $novoNumero = $versaoAntiga->numero + 1;
            } else {
                $novoNumero = 1;
            }
            
            return $novoNumero;
        }
    }
    
    public function getNameAttribute()
    {
        if ($this->path)
        {
            $pathinfo = pathinfo(asset($this->path));
        
            return ($pathinfo) ? $pathinfo['basename'] : '';
        } else {
            return $this->nome_arquivo;
        }
    }

}
