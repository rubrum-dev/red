<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FornecedoresEmails extends Model
{
    protected $fillable = [
        'id_fornecedor',
		'nome',
        'email'
    ];
    
    public function fornecedor()
    {
        return $this->belongsTo('App\Fornecedores', 'id_fornecedor');
    }
}
