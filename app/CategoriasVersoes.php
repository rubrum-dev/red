<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriasVersoes extends Model
{
    public function versoes()
    {
    	return $this->hasMany('App\Versoes', 'id_categoria_versao');
    }
}
