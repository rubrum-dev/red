<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produtos;
use App\Familias;
use App\Logotipos;
use App\Versoes;
use App\TiposLogotipos;
use App\TiposTipografias;
use App\TiposCores;
use App\NiveisCores;
use App\CategoriasProdutos;
use App\TiposUsosIncorretos;
use App\TiposUsosAplicacoes;
use App\TiposProporcoesVersoes;
use App\CategoriasVersoes;
use App\CategoriasProporcoes;
use App\CategoriasIcones;
use App\Components\Common;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

use App\Http\Requests;

class ProdutoController extends Controller
{
    private $produto;

    public function __construct(Produtos $produto)
    {
    	$this->produto = $produto;
    }

    public function index()
    {
    	$allProdutos = Produtos::orderBy('nome')->orderBy('id_familia')->get();
    	$familias = Familias::orderBy('nome')->get();
    	$allCategorias = CategoriasProdutos::orderBy('nome')->get();
    	$tiposLogos = TiposLogotipos::all();
        $tiposCores = TiposCores::all();
        $tiposTipografias = TiposTipografias::all();
        $tiposUsosIncorretos = TiposUsosIncorretos::all();
        $tiposUsosAplicacoes = TiposUsosAplicacoes::all();
        $categoriasVersoes = CategoriasVersoes::all();
        $categoriasProporcoes = CategoriasProporcoes::all();
        $categoriasIcones = CategoriasIcones::all();
        $paginate = new Common;
        $produtos = array();
        $categorias = array();

        $total = count($allProdutos);

    	foreach ($allCategorias as $value) {
    		$categorias[$value->id] = $value->nome;
    	}

        foreach ($allProdutos as $produto) {
            $produto->familias = $produto->familias;
            $produto->categorias = $produto->categorias;
            $produtos[] = $produto;
        }

        //$produtos = $paginate->paginate($produtos);

    	return view('admin.produtos.index', compact('produtos', 'familias', 'categorias', 'tiposLogos',
            'tiposCores', 'tiposTipografias', 'tiposUsosIncorretos', 'tiposUsosAplicacoes', 'categoriasVersoes',
            'categoriasProporcoes', 'categoriasIcones', 'total'));
    }

    public function search($name)
    {
    	//$produtos = $this->produto->where('nome', 'like', '%'.$name.'%')->orderBy('nome')->orderBy('id_familia')->get();
        $produtos = $this->produto->orderBy('nome')->orderBy('id_familia')->get();
    	$allFamilias = Familias::orderBy('nome')->get();
    	$allCategorias = CategoriasProdutos::orderBy('nome')->get();
    	$tiposLogos = TiposLogotipos::all();
        $tiposCores = TiposCores::all();
        $tiposTipografias = TiposTipografias::all();
        $tiposUsosIncorretos = TiposUsosIncorretos::all();
        $tiposUsosAplicacoes = TiposUsosAplicacoes::all();
        $categoriasVersoes = CategoriasVersoes::all();
        $categoriasProporcoes = CategoriasProporcoes::all();
        $categoriasIcones = CategoriasIcones::all();
        $paginate = new Common;
        $familias = array();
        $categorias = array();

        $total = count($produtos);

    	foreach ($allFamilias as $value) {
    		$familias[$value->id] = $value->nome;
    	}

    	foreach ($allCategorias as $value) {
    		$categorias[$value->id] = $value->nome;
    	}

        //$produtos = $paginate->paginate($produtos);

    	return view('admin.produtos.index', compact('produtos', 'familias', 'categorias', 'tiposLogos',
            'tiposCores', 'tiposTipografias', 'tiposUsosIncorretos', 'tiposUsosAplicacoes', 'categoriasVersoes',
            'categoriasProporcoes', 'categoriasIcones', 'total'));
    }

    public function byId($id)
    {
        $produtos = $this->produto->get();
        $allFamilias = Familias::orderBy('nome')->get();
        $allCategorias = CategoriasProdutos::orderBy('nome')->get();
        $tiposLogos = TiposLogotipos::all();
        $tiposCores = TiposCores::all();
        $tiposTipografias = TiposTipografias::all();
        $tiposUsosIncorretos = TiposUsosIncorretos::all();
        $tiposUsosAplicacoes = TiposUsosAplicacoes::all();
        $categoriasVersoes = CategoriasVersoes::all();
        $categoriasProporcoes = CategoriasProporcoes::all();
        $categoriasIcones = CategoriasIcones::all();
        $paginate = new Common;
        $familias = array();
        $categorias = array();

        $total = count($produtos);

        foreach ($allFamilias as $value) {
            $familias[$value->id] = $value->nome;
        }

        foreach ($allCategorias as $value) {
            $categorias[$value->id] = $value->nome;
        }

        //$produtos = $paginate->paginate($produtos);

        return view('admin.produtos.index', compact('produtos', 'familias', 'categorias', 'tiposLogos',
            'tiposCores', 'tiposTipografias', 'tiposUsosIncorretos', 'tiposUsosAplicacoes', 'categoriasVersoes',
            'categoriasProporcoes', 'categoriasIcones', 'total'));
    }

    public function create(Request $request)
    {
        if(empty($request->nome) || empty($request->id_categoria_produto) || empty($request->id_familia)){
            return false;
        }

        // Verifica se o produto existe
        $check = Produtos::where('nome', $request->nome)->count();
        
        if ($check) {
            
            return [
                'error' => true,
                'message' => 'Produto já cadastrado com esse nome.'
            ];
            
        }
        
        $files = $request->all();
        $images = $files['images'];
        $relativePath = '/images/uploads/logotipos/';
        $fileDown = isset($files['file']) ? $files['file'] : null;
        $filePath = '/files/uploads/versoes/';

        $files['ordem'] = $this->produto->where('id_familia', $request->id_familia)->max('ordem') + 1;

        $files['status'] = ($request->input('status')) ? 1 : 0;

        $files['replicar_logo_marca'] = ($request->input('useFamilyLogo')) ? 1 : 0;
        
        // Validação de Upload de Imagem
        foreach ($images as $image) {
            $validate = new Common;
            $data = ['file' => $image];
            $rules = ['file' => 'mimes:jpeg,bmp,png'];
            $message = 'Formato de imagem invalida';
            $validate->validaUpload($data, $rules, $message);
        }

        // Validação de Upload de Arquivo
        $data = ['file' => $fileDown];
        $rules = ['file' => 'mimes:zip'];
        $message = 'Formato de arquivo invalido. Utilize um arquivo ZIP';
        $validate->validaUpload($data, $rules, $message);

        $product = $this->produto;
        $product = $product->create($files);

        foreach ($images as $key => $image) {
            if(!is_null($image)){
                $logotipo = new Logotipos;
                $extension = $image->getClientOriginalExtension();
                $hash = md5(uniqid(rand(), true));
                $filename = $hash.".".$extension;
                $destinationPath = public_path().$relativePath;
                $upload_success = $image->move($destinationPath, $filename);

                $logo = array(
                    'thumb' => $relativePath.$filename,
                    'id_tipo_logotipo' => $key,
                    'status' => 1
                );

                $product->logotipos()->attach($logotipo->create($logo));
            }
        }

        if(!is_null($fileDown)){
            $versao = new Versoes;
            $extension = $fileDown->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $fileName = $hash.".".$extension;
            $destinationPath = storage_path().$filePath;
            $upload_success = $fileDown->move($destinationPath, $fileName);
            $dados = array(
                'id_categoria_versao' => 1,
                'id_tipo_proporcao_versao' => 1,
                'path_down' => $filePath.$fileName,
                'status' => 1
            );
            $product->versoes()->attach($versao->create($dados));
        }

    	return redirect()->route("admin.product");
    }

    public function edit($id)
    {
        $editProduct = $this->produto->find($id);
        $editProduct->logotipos;
        $editProduct->enxovais;
        $editProduct->cores;
        $editProduct->tipografias;
        $editProduct->usosIncorretos = $editProduct->usosIncorretos;
        $editProduct->usosAplicacoes = $editProduct->usosAplicacoes;
        $versoes = $editProduct->versoes()->orderBy('id_tipo_proporcao_versao')->get();
        $editProduct->proporcoes = $editProduct->proporcoes()->orderBy('id_tipo_proporcao_versao')->get();
        $editProduct->icones;
        $tiposCores = TiposCores::all();
        $niveisCores = NiveisCores::all();
        $tiposTipografias = TiposTipografias::all();
        $tiposUsosIncorretos = TiposUsosIncorretos::all();
        $tiposUsosAplicacoes = TiposUsosAplicacoes::all();
        $tiposProporcoesVersoes = TiposProporcoesVersoes::all();
        $categoriasVersoes = CategoriasVersoes::all();
        $categoriasProporcoes = CategoriasProporcoes::all();
        $categoriasIcones = CategoriasIcones::all();
        $count = null;
        $arrVersoes = array();

        foreach ($versoes as $versao) {
            $versao->descricao = substr($versao->descricao, 0, 150);
            if($versao->id_tipo_proporcao_versao != 1)
                $arrVersoes[] = $versao;
            else
                $editProduct->path_down = $versao->path_down;
        }

        $editProduct->versoes = $arrVersoes;
        $produtosPorFamilia = $this->produto->where('id_familia', $editProduct->id_familia)->get();

		foreach (range(1, count($produtosPorFamilia)) as $number) {
		    $count[] = $number;
		}

        return response(compact('editProduct', 'tiposCores', 'niveisCores', 'tiposTipografias',
            'tiposUsosIncorretos', 'tiposUsosAplicacoes', 'tiposProporcoesVersoes', 'categoriasVersoes',
            'categoriasProporcoes', 'categoriasIcones', 'count'));
    }

    public function update($id, Request $request)
    {
        if(empty($request->nome) || empty($request->id_categoria_produto) || empty($request->id_familia)){
            return false;
        }
        
        $product = $this->produto->find($id);

        // Verifica se o produto existe
        $check = Produtos::where('nome', $request->nome)->where('nome', '!=', $product->nome)->count();
        
        if ($check) {
            
            return [
                'error' => true,
                'message' => 'Produto já cadastrado com esse nome.'
            ];
            
        }
        
        $files = $request->all();
        $validate = new Common;
        $files['ordem'] = $files['ordem'] + 1;
        $files['replicar_logo_marca'] = ($request->input('useFamilyLogo')) ? 1 : 0;
        $images = $files['images'];
        $relativePath = '/images/uploads/logotipos/';
        $fileDown = isset($files['file']) ? $files['file'] : null;
        $filePath = '/files/uploads/versoes/';
        
        // Validação de Upload de Imagem
        foreach ($images as $image) {
            $data = ['file' => $image];
            $rules = ['file' => 'mimes:jpeg,bmp,png'];
            $message = 'Formato de imagem invalida';
            $validate->validaUpload($data, $rules, $message);
        }

        // Validação de Upload de Arquivo
        $data = ['file' => $fileDown];
        $rules = ['file' => 'mimes:zip'];
        $message = 'Formato de arquivo invalido. Utilize um arquivo ZIP';
        $validate->validaUpload($data, $rules, $message);

        $logos = $product->logotipos;

        $versaoDown = $product->versoes()->where('id_tipo_proporcao_versao', 1)->first();

        foreach ($images as $key => $image) {
            if(!is_null($image)){
                $logotipo = new Logotipos;
                $extension = $image->getClientOriginalExtension();
                $hash = md5(uniqid(rand(), true));
                $filename = $hash.".".$extension;
                $destinationPath = public_path().$relativePath;
                $upload_success = $image->move($destinationPath, $filename);

                foreach ($logos as $logo) {
                    if($key == $logo->id_tipo_logotipo){
                        $logoThumbPath = $logo->thumb;
                        $logoRemoveId = $logo->id;

                        if(File::isFile(public_path().$logoThumbPath)){
                            File::delete(public_path().$logoThumbPath);
                        }
                        $product->logotipos()->detach($logoRemoveId);
                        $removeLogo = $logotipo->find($logoRemoveId);
                        $removeLogo->delete();
                    }
                }

                $logo = array(
                    'thumb' => $relativePath.$filename,
                    'id_tipo_logotipo' => $key,
                    'status' => 1
                );
                $product->logotipos()->attach($logotipo->create($logo));
            }
        }

        if(!is_null($fileDown)){
            $extension = $fileDown->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $fileName = $hash.".".$extension;
            $destinationPath = storage_path().$filePath;
            $upload_success = $fileDown->move($destinationPath, $fileName);

            if($versaoDown){
                if(File::isFile(storage_path().$versaoDown->path_down)){
                    File::delete(storage_path().$versaoDown->path_down);
                }
                $versaoDown->update(['path_down' => $filePath.$fileName]);
            }
            else {
                $versao = new Versoes;
                $dados = array(
                    'id_categoria_versao' => 1,
                    'id_tipo_proporcao_versao' => 1,
                    'path_down' => $filePath.$fileName,
                    'status' => 1
                );
                $product->versoes()->attach($versao->create($dados));
            }
        }

        // Organiza a ordem de exibição conforme selecionado no item atual
        $product->ordem = !is_null($product->ordem) ? $product->ordem : $this->produto->where('id_familia', $request->id_familia)->max('ordem') + 1;
		if($files['ordem'] != $product->ordem){
			if($files['ordem'] < $product->ordem){
				$ordemProdutos = $this->produto->where('id_familia', $request->id_familia)->where('ordem', '>=', $files['ordem'])->where('ordem', '<', $product->ordem)->get();
                foreach ($ordemProdutos as $value) {
					$novaOrdem = $value->ordem + 1;
					$value->update(['ordem' => $novaOrdem]);
				}
			}
			else{
				$ordemProdutos = $this->produto->where('id_familia', $request->id_familia)->where('ordem', '<=', $files['ordem'])->where('ordem', '>', $product->ordem)->get();
                foreach ($ordemProdutos as $value) {
					$novaOrdem = $value->ordem - 1;
					$value->update(['ordem' => $novaOrdem]);
				}
			}
		}

        //$files['artwork'] = $request->input('artwork');
        //$files['packshelf'] = $request->input('packshelf');
        $files['status'] = ($request->input('status')) ? 1 : 0;

        //echo $files;
        //dd($files);
        
        $product->update($files);
    }

    public function remove($id)
    {
        $remove = new Common;
        $remove->removeProduct($id);
    }
}
