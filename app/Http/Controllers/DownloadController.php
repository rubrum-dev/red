<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\File;

use App\Http\Requests;

class DownloadController extends Controller
{
    public function guideDownload($page, $file)
    {
        $storagePath = storage_path('files/uploads/' . $page . '/' . $file);
        return response()->download($storagePath);
    }

    public function skuDownload($page, $type, $file, Request $request)
    {
        $storagePath = storage_path('files/uploads/'. $page . '/' . $type . '/' . $file);
        if(is_null($request->view)){
            return response()->download($storagePath);
        }
        else {
            $ext = explode('.', $file);
            $ext = end($ext);
            $response = Response::make(File::get($storagePath));
            $response->header('Content-Type', "image/$ext");
            return $response;
        }
    }
    
    public function skuDownloadHtml($page, $type, $file, Request $request)
    {
        if(is_null($request->view)){
            
            $storagePath = storage_path('files/uploads/'. $page . '/' . $type . '/' . $file);
            return response()->download($storagePath);
            
        }
        else {
            
            $image = route('site.download.skuFile', [$page, $type, $file]);
            
            return view('site.imagem', compact('image'));
            
        }
        
    }
    
}
