<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Components\Common;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

// Models
use App\Familias;
use App\Produtos;
use App\CategoriasProdutos;
use App\AdsmartCampanhas;
use App\AdsmartCampanhaMidias;
use App\AdsmartAprovadoresMidia;
use App\AdsmartMidiaTipos;
use App\AdsmartCondicoesMidia;
use App\AdsmartPecas;
use App\ArtwAprovacoes;
use App\ArtwTickets;
use App\ArtwSolicitacoes;
use App\ArtwCiclos;
use App\ArtwPerfisUsuariosTickets;
use App\ArtwTicketsStatus;
use App\ArtwNotificacoes;
use App\ArtwArquivos;
use App\ArtwCiclosLayout;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;

class AdsmartController extends Controller
{
    public function testarNotificacao($id_notificacao)
    {
        $notificacao = ArtwNotificacoes::find($id_notificacao);

        $ticket = $notificacao->artwTicket;

        return view('emails.notificacoes.' . $notificacao->artwNotificacaoTipo->template, compact('notificacao', 'ticket'));
    }

    public function getCampanhaCriar()
    {
        return view('site.adsmart.campanha_criar', compact(''));
    }

    public function postTipoMidiaCriar(Request $request)
    {
        DB::beginTransaction();

        $midiaTipo = AdsmartMidiaTipos::firstOrNew(['nome' => $request->input('tipoMidiaNome')]);

        if (!$midiaTipo->exists) {

            $aprovadores = [];

            foreach ((array) $request->input('aprovadores_midia_selecionados') as $aprovador)
            {
                $aprovadores[] = $aprovador['id'];
            }

            $midiaTipo->status = ($request->input('chkTipoMidiaAtivo') == 'true') ? 1 : 0;
            $midiaTipo->bloquear_auto_aprovacao = ($request->input('chkBloquarAutoAprovacao') == 'true') ? 1 : 0;
    
            $midiaTipo->save();

            $midiaTipo->aprovadores()->sync($aprovadores);

            DB::commit();

            $url = route("site.adsmart.tipos_midia");
            return response()->json(['success' => 200, 'message' => 'Tipo de Mídia criado com sucesso.', 'url' => $url], 200);

        } else {

            $url = '';
            return response()->json(['success' => 201, 'message' => 'Tipo de Mídia já existe.', 'url' => $url], 200);

        }

        //$url = route("site.artwork.package", [$variacao->sku->produtos->familias->id, $variacao->sku->produtos->id]);
    }

    public function postTipoMidiaEditar($id, Request $request)
    {
        DB::beginTransaction();

        $midiaTipo = AdsmartMidiaTipos::find($id);

        $midiaTipo->nome = $request->input('tipoMidiaNome');

        $aprovadores = [];

        foreach ((array) $request->input('aprovadores_midia_selecionados') as $aprovador)
        {
            $aprovadores[] = $aprovador['id'];
        }

        $midiaTipo->status = ($request->input('chkTipoMidiaAtivo') == 'true') ? 1 : 0;
        $midiaTipo->bloquear_auto_aprovacao = ($request->input('chkBloquarAutoAprovacao') == 'true') ? 1 : 0;

        $midiaTipo->save();

        $midiaTipo->aprovadores()->sync($aprovadores);

        DB::commit();

        $url = route("site.adsmart.tipos_midia");
        return response()->json(['success' => 200, 'message' => 'Tipo de Mídia alterado com sucesso.', 'url' => $url], 200);

        //$url = route("site.artwork.package", [$variacao->sku->produtos->familias->id, $variacao->sku->produtos->id]);
    }

    public function postAprovadoresMidiaCriar(Request $request)
    {
        DB::beginTransaction();

        $aprovador = AdsmartAprovadoresMidia::firstOrNew(['nome' => $request->input('aprovadoresMidiaNome')]);

        if (!$aprovador->exists) {

            $usuarios = [];

            foreach ((array) $request->input('usuarios_selecionados') as $usuario)
            {
                $usuarios[] = $usuario['id'];
            }

            $aprovador->save();

            $aprovador->usuarios()->sync($usuarios);

            DB::commit();

            $url = route("site.adsmart.aprovadores_midia");
            return response()->json(['success' => 200, 'message' => 'Aprovador de Mídia criado com sucesso.', 'url' => $url], 200);

        } else {

            $url = '';
            return response()->json(['success' => 201, 'message' => 'Aprovador de Mídia já existe.', 'url' => $url], 200);

        }

        //$url = route("site.artwork.package", [$variacao->sku->produtos->familias->id, $variacao->sku->produtos->id]);
    }

    public function postCondicoesMidiaCriar(Request $request)
    {
        DB::beginTransaction();

        $condicao = AdsmartCondicoesMidia::firstOrNew(['nome' => $request->input('condicaoMidiaNome')]);

        if (!$condicao->exists) {

            $condicao->texto = $request->input('txtCondicaoMidia');
            $condicao->status = ($request->input('chkCondicaoMidiaTipo_1') == 'true') ? 1 : 0;
            $condicao->abertura_ticket = ($request->input('chkCondicaoMidiaTipo_2') == 'true') ? 1 : 0;
            $condicao->reprovacao = ($request->input('chkCondicaoMidiaTipo_3') == 'true') ? 1 : 0;
            $condicao->obrigatoria = ($request->input('chkCondicaoMidiaTipo_4') == 'true') ? 1 : 0;
            $condicao->bloquear_auto_aprovacao = ($request->input('chkCondicaoMidiaTipo_5') == 'true') ? 1 : 0;

            $condicao->save();

            DB::commit();

            $url = route("site.adsmart.condicoes_midia");
            return response()->json(['success' => 200, 'message' => 'Condição de Mídia criado com sucesso.', 'url' => $url], 200);

        } else {

            $url = '';
            return response()->json(['success' => 201, 'message' => 'Condição de Mídia já existe.', 'url' => $url], 200);

        }

        //$url = route("site.artwork.package", [$variacao->sku->produtos->familias->id, $variacao->sku->produtos->id]);
    }

    public function postCondicoesMidiaEditar($id, Request $request)
    {
        DB::beginTransaction();

        $condicao = AdsmartCondicoesMidia::find($id);

        $condicao->nome = $request->input('condicaoMidiaNome');
        $condicao->texto = $request->input('txtCondicaoMidia');
        $condicao->status = ($request->input('chkCondicaoMidiaTipo_1') == 'true') ? 1 : 0;
        $condicao->abertura_ticket = ($request->input('chkCondicaoMidiaTipo_2') == 'true') ? 1 : 0;
        $condicao->reprovacao = ($request->input('chkCondicaoMidiaTipo_3') == 'true') ? 1 : 0;
        $condicao->obrigatoria = ($request->input('chkCondicaoMidiaTipo_4') == 'true') ? 1 : 0;
        $condicao->bloquear_auto_aprovacao = ($request->input('chkCondicaoMidiaTipo_5') == 'true') ? 1 : 0;

        $condicao->save();

        DB::commit();

        $url = route("site.adsmart.condicoes_midia");
        return response()->json(['success' => 200, 'message' => 'Condição de Mídia editado com sucesso.', 'url' => $url], 200);
    }

    public function postAprovadoresMidiaEditar($id, Request $request)
    {
        DB::beginTransaction();

        $aprovador = AdsmartAprovadoresMidia::find($id);

        $aprovador->nome = $request->input('aprovadoresMidiaNome');

        $usuarios = [];

        foreach ((array) $request->input('usuarios_selecionados') as $usuario)
        {
            $usuarios[] = $usuario['id'];
        }

        $aprovador->save();

        $aprovador->usuarios()->sync($usuarios);

        DB::commit();

        $url = route("site.adsmart.aprovadores_midia");
        return response()->json(['success' => 200, 'message' => 'Aprovador de Mídia alterado com sucesso.', 'url' => $url], 200);
    }

    public function postCampanhaCriar(Request $request)
    {
        DB::beginTransaction();

        $campanha = AdsmartCampanhas::firstOrNew(['nome' => $request->input('campanhaNome')]);

        if (!$campanha->exists) {

            $familias = [];

            foreach ((array) $request->input('familiasSelecionadas') as $familia)
            {
                $familias[] = $familia['id'];
            }

            $campanha->save();

            $campanha->familias()->sync($familias);

            DB::commit();

            $url = route("site.adsmart.familias");
            return response()->json(['success' => 200, 'message' => 'Campanha criada com sucesso.', 'url' => $url], 200);

        } else {

            $url = '';
            return response()->json(['success' => 201, 'message' => 'Campanha já existe.', 'url' => $url], 200);

        }

        //$url = route("site.artwork.package", [$variacao->sku->produtos->familias->id, $variacao->sku->produtos->id]);
    }

    public function postMidiaCriar(Request $request)
    {
        DB::beginTransaction();

        $midia = AdsmartCampanhaMidias::firstOrNew(['id_campanha' => $request->input('id_campanha'), 'id_midia_tipo' => $request->input('id_midia_tipo')]);

        if (!$midia->exists) {

            $midia->save();

            DB::commit();

            $url = route("site.adsmart.midias", [$request->input('id_familia'), $request->input('id_campanha')]);
            return response()->json(['success' => 200, 'message' => 'Mídia criada com sucesso.', 'url' => $url], 200);

        } else {

            $url = '';
            return response()->json(['success' => 201, 'message' => 'Mídia já existe.', 'url' => $url], 200);

        }

        //$url = route("site.artwork.package", [$variacao->sku->produtos->familias->id, $variacao->sku->produtos->id]);
    }

    public function familias(Common $common, Request $request)
    {
        $familias = Familias
            ::select('familias.*')
            ->distinct()
            ->join('adsmart_campanhas_familias', 'adsmart_campanhas_familias.id_familia', '=', 'familias.id')
            ->orderBy('familias.nome')
            ->whereIn('familias.id', Auth::user()->familias)
            ->get();

        return view('site.adsmart.familias', compact('familias'));
    }

    public function campanhas($id_familia)
    {
        $familia = Familias::find($id_familia);

        if (!in_array($familia->id, Auth::user()->familias))
        {
            return redirect()->route('site.adsmart.familias');
        }

        $campanhas = AdsmartCampanhas
            ::select('adsmart_campanhas.*')
            ->distinct()
            ->join('adsmart_campanhas_familias', 'adsmart_campanhas_familias.id_campanha', '=', 'adsmart_campanhas.id')
            ->where('adsmart_campanhas_familias.id_familia', $id_familia)
            ->orderBy('adsmart_campanhas.nome')
            ->get();

            return view('site.adsmart.campanhas', compact('campanhas', 'id_familia'));
    }

    public function midias($id_familia, $id_campanha) 
    {
        $familia = Familias::find($id_familia);
        $campanha = AdsmartCampanhas::find($id_campanha);

        if (!in_array($familia->id, Auth::user()->familias))
        {
            return redirect()->route('site.adsmart.familias');
        }

        return view('site.adsmart.midias', compact('id_familia', 'id_campanha', 'familia', 'campanha'));
    }

    public function midiaCriar($id_familia, $id_campanha) 
    {   
        $familia = Familias::find($id_familia);
        $campanha = AdsmartCampanhas::find($id_campanha);

        return view('site.adsmart.midia_criar', compact('id_familia', 'id_campanha', 'familia', 'campanha'));
    }

    public function tiposMidia() 
    {
        $midiaTipos = AdsmartMidiaTipos::orderBy('nome')->get();

        return view('site.adsmart.tipos_midia', compact('midiaTipos'));
    }

    public function tipoMidiaCriar() 
    {
        return view('site.adsmart.tipo_midia_criar');
    }

    public function tipoMidiaEditar($id) 
    {
        $tipoMidia = AdsmartMidiaTipos::find($id);

        return view('site.adsmart.tipo_midia_editar', compact('id', 'tipoMidia'));
    }

    public function aprovadoresMidia() 
    {
        $aprovadoresMidia = AdsmartAprovadoresMidia::orderBy('nome')->get();

        return view('site.adsmart.aprovadores_midia', compact('aprovadoresMidia'));
    }

    public function aprovadoresMidiaCriar() 
    {
        return view('site.adsmart.aprovadores_midia_criar');
    }

    public function aprovadoresMidiaEditar($id) 
    {
        $aprovadorMidia = AdsmartAprovadoresMidia::find($id);

        return view('site.adsmart.aprovadores_midia_editar', compact('aprovadorMidia', 'id'));
    }

    // TO-DO Excluir Campanha
    public function campanhaExcluir($id)
    {
        $campanha = AdsmartCampanhas::find($id);

        DB::beginTransaction();

        $campanha->familias()->detach();
        $campanha->delete();

        DB::commit();

        return redirect()->back()->with('message', 'Campanha excluída com sucesso.');
    }
    // END TO-DO Excluir Campanha

    public function tipoMidiaExcluir($id) 
    {
        $tipoMidia = AdsmartMidiaTipos::find($id);

        DB::beginTransaction();

        $tipoMidia->aprovadores()->detach();
        $tipoMidia->delete();

        DB::commit();

        return redirect()->route("site.adsmart.tipos_midia")->with('message', 'Tipo de Mídia excluído com sucesso.');
    }

    public function condicoesMidiaExcluir($id) 
    {
        $condicao = AdsmartCondicoesMidia::find($id);

        DB::beginTransaction();

        $condicao->delete();

        DB::commit();

        return redirect()->route("site.adsmart.condicoes_midia")->with('message', 'Critério de Avaliação excluído com sucesso.');
    }

    public function aprovadoresMidiaExcluir($id) 
    {
        $aprovadorMidia = AdsmartAprovadoresMidia::find($id);

        DB::beginTransaction();

        $aprovadorMidia->usuarios()->detach();
        $aprovadorMidia->delete();

        DB::commit();

        return redirect()->route("site.adsmart.aprovadores_midia")->with('message', 'Aprovadores de Mídia excluído com sucesso.');
    }

    public function condicoesMidia() 
    {
        $condicoesMidia = AdsmartCondicoesMidia::where('id', '!=', 99)->orderBy('nome')->get();

        return view('site.adsmart.condicoes_midia', compact('condicoesMidia'));
    }

    public function condicaoMidiaCriar() 
    {
        return view('site.adsmart.condicao_midia_criar');
    }

    public function condicaoMidiaEditar($id_condicao) 
    {
        $condicao = AdsmartCondicoesMidia::find($id_condicao);

        return view('site.adsmart.condicao_midia_editar', compact('condicao'));
    }

    public function historico($numero)
    {
        $ticket = ArtwTickets::where('numero', $numero)->where('id_status', 17)->get()->first();

        if (!$ticket) {
            abort(404);
        }

        $familia = $ticket->familia;

        if (!in_array($familia->id, Auth::user()->familias))
        {
            return redirect()->route('site.adsmart.familias');
        }

        return view('site.adsmart.historico', compact('ticket', 'familia'));
    }

    public function ticketCriar($id_peca, $id_familia)
    {
        $peca = AdsmartPecas::find($id_peca);

        $id_campanha = $peca->adsmartCampanhaMidia->id_campanha;

        $familia = Familias::find($id_familia);

        return view('site.adsmart.ticket_criar', compact('peca', 'id_familia', 'id_campanha', 'familia'));
    }

    public function ticketEdicao($numero)
    {
        $ticket = ArtwTickets::where('numero', $numero)->get()->first();

        if (!$ticket) {
            abort(404);
        }

        $familia = $ticket->familia;

        return view('site.adsmart.ticket_edicao', compact('ticket', 'familia'));
    }

    public function ticketEdicaoPost($numero, Request $request, Common $common)
    {
        $ticket = ArtwTickets::where('numero', $numero)->get()->first();

        if (!$ticket) {
            abort(404);
        }

        DB::beginTransaction();

        // Salva as solicitações
        foreach ((array) $request->input('cbAlterationType') as $k => $id_tipo) {
            $solicitacao = new ArtwSolicitacoes;
            $solicitacao->id_ciclo = $ticket->ciclo_atual()->id;
            $solicitacao->id_status = 1;
            $solicitacao->id_condicao_midia = $id_tipo;
            $solicitacao->descricao = $request->input('txtDescriptionSpec.' . $k);
            $solicitacao->titulo = ($request->input('cbAlterationName.' . $k)) ? $request->input('cbAlterationName.' . $k) : 'Outros';
            $solicitacao->id_usuario = Auth::user()->id;
            $solicitacao->save();
        }

        $dt_fim_original = clone $ticket->dt_fim;

        if ($request->input('tfArtSendDate')) {
            $ticket->dt_fim = $request->input('tfArtSendDate');
        }

        if ($request->input('txtMainDescription')) {
            $ticket->descricao = $request->input('txtMainDescription');
        }

        if ($ticket->dt_fim != $dt_fim_original) {

            $texto[] = "Aprovação: de " . $dt_fim_original->format('d/m/Y') . " para " . $ticket->dt_fim->format('d/m/Y');

            $texto = implode("\n", $texto);

            $common->enviarNotificacao($ticket, 'adsmart_data_alterada', 'dono', $texto, 1);
            $common->enviarNotificacao($ticket, 'adsmart_data_alterada', 'adsmart-anexadores', $texto, 1);
            $common->enviarNotificacao($ticket, 'adsmart_data_alterada', 'adsmart-revisores', $texto, 1);
        }

        $ticket->save();

        DB::commit();

        if (URL::previous() != route('site.adsmart.ticket.edicao', $ticket->numero)) {
         
            return redirect()->route('site.adsmart.ticket.membros', ['ticket' => $ticket->numero]);

        } else {

            return redirect()->route('site.adsmart.ticket', ['ticket' => $ticket->numero]);
        }
        
    }

    public function ticketCriarPost($id_peca, $id_familia, Request $request, Common $common)
    {
        // Criar o Ticket e redirecionar pra participantes - TO-DO
        $peca = AdsmartPecas::find($id_peca);

        DB::beginTransaction();

        $peca->status = 1;
        $peca->save();

        // Salva o Ticket
        $ticket = new ArtwTickets;
        $ticket->dt_ini = trim($request->input('tfStartDate'));
        $ticket->dt_fim = trim($request->input('tfArtSendDate'));
        $ticket->descricao = $request->input('txtMainDescription');
        $ticket->id_status = 9;
        $ticket->modulo = 2;
        $ticket->id_familia = $id_familia;
        $ticket->id_peca = $peca->id;
        $ticket->id_usuario = Auth::user()->id;
        $ticket->numero = $common->geraNumeroTicketAdsmart();
        $ticket->save();

        // Cria um ciclo
        $ciclo = new ArtwCiclos;
        $ciclo->id_ticket = $ticket->id;
        $ciclo->id_status = 1;
        $ciclo->numero = 1;
        $ciclo->save();

        // Salva as solicitações
        foreach ($request->input('cbAlterationType') as $k => $id_tipo) {
            $solicitacao = new ArtwSolicitacoes;
            $solicitacao->id_ciclo = $ciclo->id;
            $solicitacao->id_status = 1;
            $solicitacao->id_condicao_midia = $id_tipo;
            $solicitacao->descricao = $request->input('txtDescriptionSpec.' . $k);
            $solicitacao->titulo = ($request->input('cbAlterationName.' . $k)) ? $request->input('cbAlterationName.' . $k) : 'Outros';
            $solicitacao->id_usuario = Auth::user()->id;
            $solicitacao->save();
        }

        DB::commit();

        return redirect()->route('site.adsmart.ticket.membros', [$ticket->numero]);
    }

    public function abrirArquivo($id_arquivo, $filename)
    {
        $arquivo = ArtwArquivos::find($id_arquivo);
        
        $file = public_path($arquivo->path);
       
        if (file_exists($file)) {
            
            return response()->file($file);
            
        } else {
            echo 'Arquivo: ' . $file . ' não encontrado.';
        }
    }

    public function abrirLayout($id_arquivo, $filename)
    {
        $layoyt = ArtwCiclosLayout::find($id_arquivo);

        $file = Storage::disk(config('app.storage'))->get(config('app.costummer_name') . '/adsmart/ciclo/' . $layoyt->nome_arquivo);

        if ($file){

            $mimeType = Storage::disk(config('app.storage'))->mimeType(config('app.costummer_name') . '/adsmart/ciclo/' . $layoyt->nome_arquivo);

            return (new Response($file, 200))->header('Content-Type', $mimeType);

        } else {

            echo 'Arquivo: ' . $filename . ' não encontrado.';

        }
    }

    public function baixarLayout($id_arquivo, $filename)
    {
        $layoyt = ArtwCiclosLayout::find($id_arquivo);

        $file = Storage::disk(config('app.storage'))->get(config('app.costummer_name') . '/adsmart/ciclo/' . $layoyt->nome_arquivo);

        if ($file){

            $mimeType = Storage::disk(config('app.storage'))->mimeType(config('app.costummer_name') . '/adsmart/ciclo/' . $layoyt->nome_arquivo);

            $header = [
                'Content-Type' => $mimeType,
                'Content-Disposition' => 'inline; filename="'.$filename.'"',
                'Content-Disposition' => 'attachment; filename=' . $filename
            ];

            return response()->make($file, 200, $header);

        } else {

            echo 'Arquivo: ' . $filename . ' não encontrado.';

        }
    }

    public function ticketMembros($numero, Request $request, Common $common) 
    {
        $ticket = ArtwTickets::where('numero', $numero)->get()->first();

        $tipoBloquearAutoAprovacao = $ticket->adsmartPeca->adsmartCampanhaMidia->adsmartMidiaTipo->bloquear_auto_aprovacao;

        $condicaoBloquearAutoAprovacao = ArtwSolicitacoes
            ::where('id_ciclo', $ticket->ciclo_atual()->id)
            ->where('adsmart_condicoes_midia.bloquear_auto_aprovacao', 1)
            ->join('adsmart_condicoes_midia', 'adsmart_condicoes_midia.id', '=', 'artw_solicitacoes.id_condicao_midia')
            ->count();

        $bloquearAutoAprovacao = ($tipoBloquearAutoAprovacao || $condicaoBloquearAutoAprovacao) ? true : false;

        $familia = $ticket->familia;

        return view('site.adsmart.ticket_membros', compact('ticket', 'bloquearAutoAprovacao', 'familia'));
    }

    public function ticketMembrosPost($numero, Request $request, Common $common) 
    {
        $ticket = ArtwTickets::where('numero', $numero)->get()->first();

        DB::beginTransaction();

        // Participante Início
        $id_perfil = 8;

        $participante = ArtwPerfisUsuariosTickets::firstOrNew([
            'id_ticket' => $ticket->id,
            'id_usuario' => $request->input('participante_inicio'),
            'id_perfil' => $id_perfil
        ]);

        $participante->recebe_email = ($request->input('participante_inicio') == $ticket->id_usuario) ? 1 : 0;
        $participante->ciclo = 0;
        $participante->save();

        // Participante Anexação
        $id_perfil = 9;

        foreach ($request->input('participantes_anexacao') as $id_usuario)
        {
            $participante = ArtwPerfisUsuariosTickets::firstOrNew([
                'id_ticket' => $ticket->id,
                'id_usuario' => $id_usuario,
                'id_perfil' => $id_perfil
            ]);
    
            $participante->recebe_email = ($id_usuario == $ticket->id_usuario) ? 1 : 0;
            $participante->ciclo = 0;
            $participante->save();
        }

        // Participante Revisão
        $id_perfil = 10;

        foreach ($request->input('participantes_revisao') as $id_usuario)
        {
            $participante = ArtwPerfisUsuariosTickets::firstOrNew([
                'id_ticket' => $ticket->id,
                'id_usuario' => $id_usuario,
                'id_perfil' => $id_perfil
            ]);
    
            $participante->recebe_email = ($id_usuario == $ticket->id_usuario) ? 1 : 0;
            $participante->ciclo = 0;
            $participante->save();
        }

        // Participante Aprovação
        $id_perfil = 11;

        foreach ($request->input('participantes_aprovacao') as $id_usuario)
        {
            $participante = ArtwPerfisUsuariosTickets::firstOrNew([
                'id_ticket' => $ticket->id,
                'id_usuario' => $id_usuario,
                'id_perfil' => $id_perfil
            ]);
    
            $participante->recebe_email = ($id_usuario == $ticket->id_usuario) ? 1 : 0;
            $participante->ciclo = 0;
            $participante->save();
        }

        $ticket->save();

        $common->changeStatusTicket($ticket, 14);
        
        DB::commit();

        return redirect()->route('site.adsmart.ticket', ['ticket' => $ticket->numero]);
    }

    public function ticket($numero) 
    {
        $ticket = ArtwTickets::where('numero', $numero)->get()->last();

        if (!$ticket) {
            abort(404);
        }

        if ($ticket->id_status == 9) {
            return redirect()->route('site.adsmart.ticket.membros', $ticket->numero);
        }

        if (!$ticket->checkUsuario(Auth::user()->id) && !Auth::user()->admin && !Auth::user()->gerente) {
            return redirect()->route('site.artwork.managerV4', ['minhas_tarefas']);
        }

        $familia = $ticket->familia;

        return view('site.adsmart.ticket', compact('ticket', 'familia'));
    }

    public function ticketReprovar($numero) 
    {
        $ticket = ArtwTickets::where('numero', $numero)->get()->last();

        if (!$ticket) {
            abort(404);
        }

        $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, '11');

        if (!$perfilUsuarioTicket)
        {
            return redirect()->route('site.adsmart.ticket', ['ticket' => $ticket->numero]);
        }

        $familia = $ticket->familia;

        return view('site.adsmart.ticket_reprovar', compact('ticket', 'familia'));
    }

    public function ticketReprovarPost($numero, Request $request, Common $common) 
    {
        $ticket = ArtwTickets::where('numero', $numero)->get()->last();

        if (!$ticket) {
            abort(404);
        }

        $id_perfil = 11;

        $ciclo_antigo = $ticket->ciclo_atual();

        $ciclo_novo = ArtwCiclos::firstOrNew(['numero' => $ciclo_antigo->numero + 1, 'id_ticket' => $ticket->id]);

        if (!$ciclo_novo->exists) {
            $ciclo_novo->id_status = 0;
            $ciclo_novo->save();
        }

        DB::beginTransaction();

        $pendentes = $ticket->participantesPendentes($id_perfil, $ticket->ciclo_atual()->numero);

        if ($pendentes->count() == 1) {
            $libera_status = 1;
        } else {
            $libera_status = 0;
        }

        $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);

        if (!$perfilUsuarioTicket) {
            return back()->withErrors('Perfil incorreto.');
        }

        $perfilUsuarioTicket->ciclo_aprovacao = $ciclo_antigo->numero;
        $perfilUsuarioTicket->em_reprovacao = 0;
        $perfilUsuarioTicket->em_reprovacao_by = null;
        $perfilUsuarioTicket->aprovado = 2;
        $perfilUsuarioTicket->ciclo = $ciclo_antigo->numero;
        $perfilUsuarioTicket->save();

        $artwAprovacao = ArtwAprovacoes::firstOrNew([
            'id_ticket' => $ticket->id,
            'id_usuario' => Auth::user()->id,
            'ciclo' => $ticket->ciclo_atual()->numero,
            'aprovado' => 2,
            'id_perfil' => $id_perfil
        ]);

        $artwAprovacao->save();

        if ($libera_status) {

            // Volta o status com status = 1 / Em Edição

            $ciclo_antigo->id_status = 3;
            $ciclo_antigo->save();

            $ciclo_novo->id_status = 1;
            $ciclo_novo->save();

            $ticket->ciclo = $ciclo_novo->numero;
            $ticket->save();
            
            $common->changeStatusTicket($ticket, 14);
        }

        // Salva as solicitações
        foreach ($request->input('cbAlterationType') as $k => $id_tipo) {
            $solicitacao = new ArtwSolicitacoes;
            $solicitacao->id_ciclo = $ciclo_novo->id;
            $solicitacao->id_status = 1;
            $solicitacao->id_condicao_midia = $id_tipo;
            $solicitacao->descricao = $request->input('txtDescriptionSpec.' . $k);
            $solicitacao->titulo = ($request->input('cbAlterationName.' . $k)) ? $request->input('cbAlterationName.' . $k) : 'Outros';
            $solicitacao->id_usuario = Auth::user()->id;
            $solicitacao->save();
        }


        DB::commit();

        return redirect()->route('site.adsmart.ticket', ['ticket' => $ticket->numero]);
    }

    public function ticketEstatico() 
    {
        return view('site.adsmart.ticket_estatico');
    }

    public function ticketReprovado() 
    {
        return view('site.adsmart.ticket_reprovado');
    }
}
