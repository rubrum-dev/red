<?php
namespace App\Http\Controllers;

use Mail;
use Validator;
use Illuminate\Http\Request;
use App\Familias;
use App\Produtos;
use App\Skus;
use App\AdmUsuarios;
use App\AdmPerfis;
use App\AdmCargos;
use App\AdmDepartamentos;
use App\AdmEmpresas;
use App\ArtwItemTipos;
use App\ArtwNotificacoes;
use App\ArtwVariacoes;
use App\NaturezaEmbalagens;
use App\AdmUsuariosTokens;
use App\ArtwItens;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Components\SiteGuides;
use App\Components\SiteCommon;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\Components\Common;
use App\Configuracoes;
use App\News;
use App\NewsUsuarios;
use File;
use Illuminate\Support\Facades\Storage;
use Chumper\Zipper\Zipper;

class HomeController extends Controller
{

    public function __construct(SiteCommon $siteCommon)
    {
        //dd(Auth::user()->admEmpresa);
        
        $this->siteCommon = $siteCommon;
    }

    public function migrate()
    {
        $usuarios = AdmUsuarios::all();
        
        $modulos = \App\AdmModulos::whereIn('id', [15,16])->pluck('id')->toArray();
        
        DB::beginTransaction();

        foreach ($usuarios as $usuario) {

            $usuario->save();
            
            $usuario->admModulos()->sync($modulos, false);
        }

        DB::commit();

        dd($usuarios);
    }

    public function delTree($dir) { 
        $files = array_diff(scandir($dir), array('.','..')); 
        foreach ($files as $file) { 
          (is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file"); 
        } 
        return rmdir($dir); 
    }

    public function exportar(Common $common)
    {
        $caminhoExportar = 'images/uploads/exportar';
        
        if(file_exists($caminhoExportar))
        {
            $this->delTree($caminhoExportar);
        } 
        
        File::makeDirectory($caminhoExportar, 0755, true);

        $allProdutos = Produtos
            ::select('produtos.*')
            ->join('skus', 'skus.id_produto', '=', 'produtos.id')
            ->join('artw_variacoes', 'artw_variacoes.id_sku', '=', 'skus.id')
            //->join('artw_variacao_itens', 'artw_variacao_itens.id_variacao', '=', 'artw_variacoes.id')
            ->where('produtos.status', 1)
            ->get();

        $familias = [];

        // Marcas (Famílias)
        foreach ($allProdutos as $produto) {
            $familias[$produto->familias->id]['nome'] = $produto->familias->nome;
        }
        
        foreach ($familias as $idFamilia=>$familia)
        {
            $caminhoFamilia = $caminhoExportar . '/' . $familia['nome'];

            if(!file_exists($caminhoFamilia))
            {
                File::makeDirectory($caminhoFamilia, 0755, true);
            }

            // Produtos
            $allProdutos = Produtos
                ::select('produtos.*')
                ->join('skus', 'skus.id_produto', '=', 'produtos.id')
                ->join('artw_variacoes', 'artw_variacoes.id_sku', '=', 'skus.id')
                //->join('artw_variacao_itens', 'artw_variacao_itens.id_variacao', '=', 'artw_variacoes.id')
                ->where('produtos.status', 1)
                ->where('id_familia', $idFamilia)
                ->get();

            foreach ($allProdutos as $produto)
            {
                $caminhoProduto = $caminhoFamilia . '/' . $produto->nome;

                if(!file_exists($caminhoProduto))
                {
                    File::makeDirectory($caminhoProduto, 0755, true);
                }

                // Embalagens de Mercado
                $embalagensAtivas = $common->getEmbalagens($produto->id, 1);
                $embalagensInativas = $common->getEmbalagens($produto->id, 2);

                $todasEmbalagens = [];

                foreach ($embalagensAtivas['regulares'] as $embalagem)
                {
                    $caminho = $caminhoProduto . '/Embalagens de Mercado/Regulares/' . str_replace('/', '|', $embalagem['nome_original']);
                    
                    $todasEmbalagens[] = [
                        'caminho' => $caminho,
                        'embalagem' => $embalagem
                    ];
                }

                foreach ($embalagensAtivas['promocionais'] as $embalagem)
                {
                    $caminho = $caminhoProduto . '/Embalagens de Mercado/Promocionais/' . str_replace('/', '|', $embalagem['nome_original']);
                    
                    $todasEmbalagens[] = [
                        'caminho' => $caminho,
                        'embalagem' => $embalagem
                    ];
                }

                foreach ($embalagensInativas['regulares'] as $embalagem)
                {
                    $caminho = $caminhoProduto . '/Embalagens Descontinuadas/Regulares/' . str_replace('/', '|', $embalagem['nome_original']);
                    
                    $todasEmbalagens[] = [
                        'caminho' => $caminho,
                        'embalagem' => $embalagem
                    ];
                }

                foreach ($embalagensInativas['promocionais'] as $embalagem)
                {
                    $caminho = $caminhoProduto . '/Embalagens Descontinuadas/Promocionais/' . str_replace('/', '|', $embalagem['nome_original']);
                    
                    $todasEmbalagens[] = [
                        'caminho' => $caminho,
                        'embalagem' => $embalagem
                    ];
                }

                foreach ($todasEmbalagens as $embalagem)
                {
                    $caminhoEmbalagem = $embalagem['caminho'];

                    //dd($caminhoEmbalagem);

                    if(!file_exists($caminhoEmbalagem))
                    {
                        File::makeDirectory($caminhoEmbalagem, 0755, true);
                    }

                    foreach ($embalagem['embalagem']['itens'] as $item)
                    {
                        $caminhoItem = $caminhoEmbalagem . '/' . str_replace('/', '|', $item['nome']);
                        
                        if(!file_exists($caminhoItem))
                        {
                            File::makeDirectory($caminhoItem, 0755, true);
                        }

                        $objItem = ArtwItens::find($item['id']);

                        foreach ($objItem->artwVersoes as $versao)
                        {
                            $caminhoVersao = $caminhoItem . '/Versão ' . $versao->numero;

                            if(!file_exists($caminhoVersao))
                            {
                                File::makeDirectory($caminhoVersao, 0755, true);
                            }

                            if (file_exists(public_path($versao->path)))
                            {
                                $dadosArquivo = pathinfo(public_path($versao->path));

                                copy(public_path($versao->path), $caminhoVersao . '/' . $dadosArquivo['basename']);

                                //dd();
                            }

                            $arteFinal = $versao->artwTicket->artwArtes->last();

                            if ($arteFinal && $arteFinal->path)
                            {
                                //dd($arteFinal->path);

                                if (file_exists(public_path($arteFinal->path)))
                                {
                                    $dadosArquivo = pathinfo(public_path($arteFinal->path));

                                    copy(public_path($versao->path), $caminhoVersao . '/' . $dadosArquivo['basename']);

                                    //dd();
                                }
                            }
                            elseif ($arteFinal && !$arteFinal->path)
                            {
                                if (Storage::disk(config('app.storage'))->exists(config('app.costummer_name') . '/artwork/artes-finais/' . $arteFinal->nome_arquivo))
                                {
                                    $file = Storage::disk(config('app.storage'))->get(config('app.costummer_name') . '/artwork/artes-finais/' . $arteFinal->nome_arquivo);

                                    file_put_contents($caminhoVersao . '/' . $arteFinal->nome_arquivo, $file);
                                }

                                //dd($versao, $arteFinal);
                                //exit;
                            }
                        }
                    }
                }

                //dd($todasEmbalagens);
            }
        }

        //$zipper = new Zipper();

        //$zipFile = public_path('images/uploads/exportar.zip');

        //$zipper->addDir(public_path('images/uploads/exportar'));

        //$zipper->make($zipFile);

        //$zipper->close();
    }

    public function siteNews()
    {
        return view('site.news.modal_novidades');
    }

    public function pesquisa()
    {
        return view('site.pesquisa.modal_pesquisa');
    }

    public function pesquisaAgradecimento()
    {
        return view('site.pesquisa.modal_pesquisa_agradecimento');
    }

    public function siteNewsExibir(Request $request)
    {
        $news = News::all()->last();
        
        if ($request->input('dontShowNews') == 'true')
        {
            
            $newsUsuario = new NewsUsuarios();
            $newsUsuario->id_news = $news->id;
            $newsUsuario->id_usuario = Auth::user()->id;
            $newsUsuario->save();
            
        } else {
            
            $newsUsuario = NewsUsuarios
                ::where('id_news', $news->id)
                ->where('id_usuario', Auth::user()->id)
                ->get()
                ->last();
            
            $newsUsuario->delete();
        }
        
        return ['success'];
    }
    
    public function index(Request $request, $modulo = 'sku')
    {
        //Session::
        
        //dd($request->session()->getId());
        
        if (str_contains(request()->url(), 'stg')) {
            //return redirect()->route('site.artwork.family');
        }
        
        if (str_contains(request()->url(), 'stg') || str_contains(request()->url(), 'hml') || str_contains(request()->url(), 'localhost') ) {
            //echo 'não é produção';
            //exit;
        }

        $familias = array();
        $itemProd = array();

        $allProdutos = Produtos::where('status', 1)->get();
        foreach ($allProdutos as $prod) {
            $itemProd[] = $prod->id_categoria_produto;
        }

        switch ($modulo) {
            case 'sku':
                $allFamilies = Familias
                    ::select('familias.*')
                    ->join('produtos', 'familias.id', '=', 'produtos.id_familia')
                    ->join('skus', 'produtos.id', '=', 'skus.id_produto')
                    //->join('artw_variacoes', 'artw_variacoes.id_sku', '=', 'skus.id')
                    ->join('artes_finais', 'artes_finais.id_sku', '=', 'skus.id')
                    ->where('familias.status', 1)
                    ->where('produtos.status', 1)
                    ->where('artes_finais.status', 1)
                    ->where('artes_finais.arte_exibicao', 1)
                    ->orderBy('familias.ordem')
                    ->orderBy('familias.nome')
                    ->groupBy('familias.id')
                    ->get();
                break;
            case 'guide':
                $allFamilies = Familias
                    ::select('familias.*')
                    ->join('produtos', 'familias.id', '=', 'produtos.id_familia')
                    ->join('produtos_tipografias', 'produtos.id', '=', 'produtos_tipografias.id_produto')
                    ->where('familias.status', 1)
                    ->where('produtos.status', 1)
                    ->orderBy('familias.ordem')
                    ->orderBy('familias.nome')
                    ->groupBy('familias.id')
                    ->get();
                break;
            case 'ads':
                $allFamilies = Familias
                    ::select('familias.*')
                    ->join('produtos', 'familias.id', '=', 'produtos.id_familia')
                    ->join('enxovais', 'produtos.id', '=', 'enxovais.id_produto')
                    ->where('familias.status', 1)
                    ->where('produtos.status', 1)
                    ->orderBy('familias.ordem')
                    ->orderBy('familias.nome')
                    ->groupBy('familias.id')
                    ->get();
                break;
            default:
                $allFamilies = [];
                break;
        }

        //dd($allFamilies);

        $favorites = $this->siteCommon->getFavorites(true);
        $recentSku = $this->siteCommon->getRecentSku(true);
        $categorias = $this->siteCommon->getFilters('categorias', array_unique($itemProd))['categorias'];
        $user = null;

        if (!Auth::guest()) {
            $adm = new AdmUsuarios;
            $user = $adm->find(Auth::user()->id);
            $userRelations = $this->siteCommon->checkUserRelations();

            $user->favorites = $userRelations->favorites;
            $user->familias = $userRelations->familias;
            $user->modulos = $userRelations->modulos;
        }

        //dd($user->modulos);

        //dd($allFamilies);

        foreach ($allFamilies as $family) {
            $family->logotipos;
            if (!empty($request->all())) {
                if ($request->categorias === "") {
                    return redirect()->route('site.index');
                } else {
                    $cat = array();
                    foreach ($family->produtos as $produto) {
                        if ($produto->id_categoria_produto == $request->categorias)
                            $cat[] = true;
                        else
                            $cat[] = false;
                    }
                    if (in_array(true, $cat))
                        $familias[] = $family;
                }
            }
            else {
                $familias[] = $family;
            }
        }

        return view('site.familias.index', compact('familias', 'categorias', 'recentSku', 'favorites', 'user'));
    }

    public function searchFamily($family)
    {
        $familia = array();
        $family = Familias::where('id', $family)->get();
        $favorites = $this->siteCommon->getFavorites(true);
        $recentSku = $this->siteCommon->getRecentSku(true);
        $user = null;

        if (!Auth::guest()) {
            $adm = new AdmUsuarios;
            $user = $adm->find(Auth::user()->id);
            $userRelations = $this->siteCommon->checkUserRelations();

            $user->favorites = $userRelations->favorites;
            $user->familias = $userRelations->familias;
            $user->modulos = $userRelations->modulos;
        }

        foreach ($family as $value) {
            $value->logotipos;
            $familia[] = $value;
        }

        $familias = $familia;

        return view('site.familias.index', compact('familias', 'categorias', 'recentSku', 'favorites', 'user'));
    }

    public function product(Request $request, $family, $modulo = 'sku')
    {
        $user = null;
        $itemProd = array();
        $produtos = array();

        switch ($modulo) {
            case 'sku':
                $allProdutos = Produtos
                    ::select('produtos.*')
                    ->join('skus', 'produtos.id', '=', 'skus.id_produto')
                    //->join('artw_variacoes', 'artw_variacoes.id_sku', '=', 'skus.id')
                    ->leftJoin('produtos_logotipos', 'produtos_logotipos.id_produto', '=', 'produtos.id')
                    ->leftjoin('artes_finais', 'artes_finais.id_sku', '=', 'skus.id')
                    ->where(array('produtos.status' => 1, 'produtos.id_familia' => $family, 'artes_finais.status' => 1, 'artes_finais.arte_exibicao' => 1))
                    ->orderBy('produtos.ordem')
                    ->orderBy('produtos.nome')
                    ->groupBy('produtos.id')
                    ->get();
                break;
            case 'guide':
                $allProdutos = Produtos
                    ::select('produtos.*')
                    ->join('produtos_tipografias', 'produtos.id', '=', 'produtos_tipografias.id_produto')
                    ->where(array('produtos.status' => 1, 'produtos.id_familia' => $family))
                    ->orderBy('produtos.ordem')
                    ->orderBy('produtos.nome')
                    ->groupBy('produtos.id')
                    ->get();
                break;
            case 'ads':
                $allProdutos = Produtos
                    ::select('produtos.*')
                    ->join('enxovais', 'produtos.id', '=', 'enxovais.id_produto')
                    ->where(array('produtos.status' => 1, 'produtos.id_familia' => $family))
                    ->orderBy('produtos.ordem')
                    ->orderBy('produtos.nome')
                    ->groupBy('produtos.id')
                    ->get();
                break;
            default:
                $allProdutos = [];
                break;
        }

        foreach ($allProdutos as $produto) {
            foreach ($produto->versoes as $versao) {
                if ($versao->id_tipo_proporcao_versao == 1 && $versao->id_categoria_versao == 1)
                    $produto->logoDown = true;
            }

            if ((isset($produto->logoDown) && count($produto->versoes) > 1) || count($produto->cores) > 0 || count($produto->tipografias) > 0 || count($produto->usosIncorretos) > 0 || count($produto->usosAplicacoes) > 0 || count($produto->proporcoes) > 0 || count($produto->icones) > 0)
                $produto->checkGuides = true;
            else
                $produto->checkGuides = false;

            if (count($produto->enxovais) > 0)
                $produto->checkEnxovais = true;
            else
                $produto->checkEnxovais = false;

            if (!empty($request->all())) {
                if ($request->categorias == "") {
                    return redirect()->route('site.product', array($family));
                } else {
                    if ($produto->id_categoria_produto == $request->categorias)
                        $produtos[] = $produto;
                }
            }
            else {
                $produtos[] = $produto;
            }

            $itemProd[] = $produto->id_categoria_produto;
        }

        if (count($produtos) == 1) {
            switch ($modulo) {
                case 'guide':
                    return redirect()->route('site.guide', ['family' => $family, 'product' => $produtos[0]->id, 'guide' => 'versoes']);
                    break;
                case 'ads':
                    return redirect()->route('site.guide', ['family' => $family, 'product' => $produtos[0]->id, 'guide' => 'enxovais']);
                    break;
                default:
                    return redirect()->route('site.sku', ['family' => $family, 'product' => $produtos[0]->id]);
                    break;
            }
        }

        if (!Auth::guest()) {
            $adm = new AdmUsuarios;
            $user = $adm->find(Auth::user()->id);
            $userRelations = $this->siteCommon->checkUserRelations();

            $user->favorites = $userRelations->favorites;
            $user->familias = $userRelations->familias;
            $user->modulos = $userRelations->modulos;
        }

        $favorites = $this->siteCommon->getFavorites(true);
        $recentSku = $this->siteCommon->getRecentSku(true);
        $categorias = $this->siteCommon->getFilters('categorias', array_unique($itemProd))['categorias'];

        $familia = Familias::find($family);

        return view('site.produtos.index', compact('produtos', 'family', 'categorias', 'recentSku', 'favorites', 'user', 'familia'));
    }

    public function searchProduct($family, $product)
    {
        $user = null;
        $produtos = array();
        $product = Produtos::where('id', $product)->get();
        $favorites = $this->siteCommon->getFavorites(true);
        $recentSku = $this->siteCommon->getRecentSku(true);

        foreach ($product as $value) {
            if (count($value->cores) > 0 || count($value->tipografias) > 0 || count($value->usosIncorretos) > 0 || count($value->usosAplicacoes) > 0 || count($value->proporcoes) > 0 || count($value->icones) > 0)
                $value->checkGuides = true;
            else
                $value->checkGuides = false;

            if (count($value->enxovais) > 0)
                $value->checkEnxovais = true;
            else
                $value->checkEnxovais = false;

            $value->logotipos;
            $produto[] = $value;
        }

        if (!Auth::guest()) {
            $adm = new AdmUsuarios;
            $user = $adm->find(Auth::user()->id);
            $userRelations = $this->siteCommon->checkUserRelations();

            $user->favorites = $userRelations->favorites;
            $user->familias = $userRelations->familias;
            $user->modulos = $userRelations->modulos;
        }

        $produtos = $produto;

        return view('site.produtos.index', compact('produtos', 'family', 'recentSku', 'favorites', 'user'));
    }

    public function sku($family, $product, Request $request)
    {
        $urlPromo = explode("/", $request->url());
        $checkPromo = end($urlPromo);
        $promo = array();
        $itensEmb = array();
        $promo['check_expirados'] = false;
        $promo['link_promo_active'] = false;
        $hasPromo = Skus::where(array('id_produto' => $product))->orderBy('ordem')->select('promocional')->get();
        foreach ($hasPromo as $value) {
            if ($value->promocional)
                $promo['link_promo_active'] = true;
        }
        if ($checkPromo != 'promocional') {
            $allSkus = Skus::where(array('promocional' => 0, 'id_produto' => $product))->orderBy('ordem')->orderBy('nome')->get();
            $promo['check'] = false;
        } else {
            $allSkus = Skus::where(array('promocional' => 1, 'id_produto' => $product))->orderBy('dt_expiracao', 'desc')->orderBy('nome')->get();
            $promo['check'] = true;
        }

        $tiposExistente = array();
        $user = null;
        $skus = array();

        foreach ($allSkus as $sku) {
            $sku->now = date('Y-m-d');
            if (!$promo['check']) {
                //$nome = explode(' - ', $sku->nome);
                $sku->nomeProduto = $sku->produtos->nome;
                $sku->embalagemVolume = $sku->dimensoes->tiposEmbalagens->nome . ' ' . $sku->dimensoes->nome;
            } else {
                $expiracao = date_create($sku->dt_expiracao);
                $sku->expiracao = date_format($expiracao, 'd/m/Y');
                $sku->nomeProduto = $sku->nome;
                $sku->embalagemVolume = '';
            }
            $sku->dimensoes;
            $sku->produtos;
            $sku->artesFinais;

            if ($promo['check'] && $sku->dt_expiracao < $sku->now)
                $promo['check_expirados'] = true;

            if (!empty($request->all()) && !isset($request->sku)) {
                if ($request->tiposEmbalagens == "") {
                    return redirect()->route('site.sku', array($family, $product));
                } else {
                    if ($sku->dimensoes->id_tipo_embalagem == $request->tiposEmbalagens) {
                        $tiposExistente[] = $sku->dimensoes->id_tipo_embalagem;
                        $skus[] = $sku;
                    }
                }
            } else {
                $tiposExistente[] = $sku->dimensoes->id_tipo_embalagem;
                $skus[] = $sku;
            }

            $itensEmb[] = $sku->dimensoes->id_tipo_embalagem;
        }

        //return $skus;
        //exit;

        if (!Auth::guest()) {
            $adm = new AdmUsuarios;
            $user = $adm->find(Auth::user()->id);
            $userRelations = $this->siteCommon->checkUserRelations();

            $user->favorites = $userRelations->favorites;
            $user->familias = $userRelations->familias;
            $user->modulos = $userRelations->modulos;
        }

        $produtoId = $product;
        $produto = Produtos::find($product);

        foreach ($produto->logotipos as $logo) {
            if ($logo->id_tipo_logotipo == 2)
                $produto->logotipo = $logo->thumb;
        }

        foreach ($produto->versoes as $versao) {
            // Versão Preferencial para Download de Logotipo
            if ($versao->id_tipo_proporcao_versao == 1 && $versao->id_categoria_versao == 1)
                $produto->logoDown = $versao->path_down;
        }

        if ((isset($produto->logoDown) && count($produto->versoes) > 1) || count($produto->cores) > 0 || count($produto->tipografias) > 0 || count($produto->usosIncorretos) > 0 || count($produto->usosAplicacoes) > 0 || count($produto->proporcoes) > 0 || count($produto->icones) > 0)
            $produto->checkGuides = true;
        else
            $produto->checkGuides = false;

        if (count($produto->enxovais) > 0)
            $produto->checkEnxovais = true;
        else
            $produto->checkEnxovais = false;

        $recentSku = $this->siteCommon->getRecentSku(true);
        $favorites = $this->siteCommon->getFavorites(true);
        $tiposEmbalagens = $this->siteCommon->getFilters('tiposEmbalagens', array_unique($itensEmb))['tiposEmbalagens'];

        //$variacoes = ArtwVariacoes
        //::select('artw_variacoes.*')
        //->join('skus', 'skus.id', '=', 'artw_variacoes.id_sku')
        //->where('skus.id_produto', $product)
        //->get();
        //dd($product, $variacoes);

        return view('site.sku.index', compact('tiposExistente', 'skus', 'produtoId', 'produto', 'family', 'tiposEmbalagens', 'recentSku', 'favorites', 'user', 'promo'));
    }

    public function skuNew($family, $product, $expirados = false, Request $request)
    {
        $itensEmb = array();
        
        $promo['check'] = false;
        $promo['check_expirados'] = false;
        $promo['link_promo_active'] = false;
        
        $allVariacoes = Skus
            ::select('skus.*')
            ->distinct()
            //->leftjoin('skus', 'artw_variacoes.id_sku', '=', 'skus.id')
            ->leftjoin('produtos', 'skus.id_produto', '=', 'produtos.id')
            ->leftjoin('artes_finais', 'artes_finais.id_sku', '=', 'skus.id')
            ->leftjoin('dimensoes', 'skus.id_dimensao', '=', 'dimensoes.id')
            ->leftjoin('tipos_embalagens', 'dimensoes.id_tipo_embalagem', '=', 'tipos_embalagens.id')
            ->where(['skus.id_produto' => $product])
            ->where('produtos.status', 1)
            ->where('artes_finais.status', 1)
            ->where('artes_finais.arte_exibicao', 1);

        if ($expirados == false) {
            $allVariacoes = $allVariacoes->where(function ($query) {
                $query->where('skus.dt_expiracao', null)
                    ->orWhere('skus.dt_expiracao', '>=', date('Y-m-d 00:00:00'));
            });
        }

        $allVariacoes = $allVariacoes
            //->orderBy('skus.nome')
            ->orderBy('tipos_embalagens.ordem')
            ->orderBy('dimensoes.ordem')
            ->orderBy('skus.ordem')
            //->orderBy('artw_variacoes.principal', 'DESC')
            //->orderBy('artw_variacoes.nome')
            ->get();

        //echo $allVariacoes;
        //exit;

        $tiposExistente = [];
        $naturezasExistentes = [];
        $promo = false;
        $user = null;

        $skus = [];

        //dd($allSkus->first()->sku_nome_completo);

        foreach ($allVariacoes as $variacao) {

            $sku = $variacao;

            //$sku->id = $variacao->id;
            $sku->now = date('Y-m-d');
            $sku->nome = $sku->sku_nome_completo;
            $sku->nomeProduto = $sku->produtos->nome;
            //$sku->promocional = ($variacao->id_tipo == 2) ? 1 : 0;
            //$sku->dt_expiracao = $variacao->dt_expiracao;
            //$sku->ordem = $variacao->ordem;
            //$sku->status = $variacao->status;
            //$sku->id_tipo = $variacao->id_tipo;
            $sku->embalagemVolume = $sku->sku_nome_volume;
            //$sku->variacao = ($variacao->principal == 0) ? $variacao->nome : '';
            //$sku->campanha = ($variacao->id_tipo == 2) ? $variacao->nome : '';
            $sku->dimensoes;
            $sku->produtos;
            $sku->artesFinais = $variacao->artesFinais;

            if (!empty($request->all()) && !isset($request->sku)) {

                if ($request->tiposEmbalagens == "") {
                    return redirect()->route('site.sku', array($family, $product));
                } else {
                    if ($sku->dimensoes->id_tipo_embalagem == $request->tiposEmbalagens) {
                        $tiposExistente[] = $sku->dimensoes->id_tipo_embalagem;
                        $skus[] = $sku;
                    }
                }
            } else {
                $tiposExistente[] = $sku->dimensoes->id_tipo_embalagem;
                $skus[] = $sku;
            }

            if ($variacao->id_tipo == 1) {
                $naturezasExistentes[] = $sku->dimensoes->tiposEmbalagens->id_natureza_embalagem;
            } else {
                $promo = true;
                $naturezasExistentes[] = 1;
            }

            $itensEmb[] = $sku->dimensoes->id_tipo_embalagem;
        }

        //dd($naturezasExistentes);
        //return $skus;
        //exit;

        if (!Auth::guest()) {
            $adm = new AdmUsuarios;
            $user = $adm->find(Auth::user()->id);
            $userRelations = $this->siteCommon->checkUserRelations();

            $user->favorites = $userRelations->favorites;
            $user->familias = $userRelations->familias;
            $user->modulos = $userRelations->modulos;
        }

        $produtoId = $product;
        $produto = Produtos::find($product);

        if ($produto->replicar_logo_marca)
        {
            foreach ($produto->familias->logotipos as $logo) {
                    $produto->logotipo = $logo->thumb;
            }

        } else {

            foreach ($produto->logotipos as $logo) {
                if ($logo->id_tipo_logotipo == 2)
                    $produto->logotipo = $logo->thumb;
            }

        }

        foreach ($produto->versoes as $versao) {
            // Versão Preferencial para Download de Logotipo
            if ($versao->id_tipo_proporcao_versao == 1 && $versao->id_categoria_versao == 1)
                $produto->logoDown = $versao->path_down;
        }

        if ((isset($produto->logoDown) && count($produto->versoes) > 1) || count($produto->cores) > 0 || count($produto->tipografias) > 0 || count($produto->usosIncorretos) > 0 || count($produto->usosAplicacoes) > 0 || count($produto->proporcoes) > 0 || count($produto->icones) > 0)
            $produto->checkGuides = true;
        else
            $produto->checkGuides = false;

        if (count($produto->enxovais) > 0)
            $produto->checkEnxovais = true;
        else
            $produto->checkEnxovais = false;

        $recentSku = $this->siteCommon->getRecentSku(true);
        $favorites = $this->siteCommon->getFavorites(true);
        $tiposEmbalagens = $this->siteCommon->getFilters('tiposEmbalagens', array_unique($itensEmb))['tiposEmbalagens'];

        $allNaturezas = NaturezaEmbalagens
            ::select('natureza_embalagens.*')
            ->distinct()
            ->join('tipos_embalagens', 'tipos_embalagens.id_natureza_embalagem', '=', 'natureza_embalagens.id')
            ->join('dimensoes', 'dimensoes.id_tipo_embalagem', '=', 'tipos_embalagens.id')
            ->join('skus', 'skus.id_dimensao', '=', 'dimensoes.id')
            //->join('artw_variacoes', 'artw_variacoes.id_sku', '=', 'skus.id')
            ->leftjoin('produtos', 'skus.id_produto', '=', 'produtos.id')
            ->leftjoin('artes_finais', 'artes_finais.id_sku', '=', 'skus.id')
            ->where(['skus.id_produto' => $product])
            ->where('produtos.status', 1)
            ->where('artes_finais.status', 1)
            ->where('artes_finais.arte_exibicao', 1)
            ->where('natureza_embalagens.id', '!=', 1)
            ->orderBy('natureza_embalagens.nome')
            ->get();

        $naturezas = [];

        foreach ($allNaturezas as $natureza) {
            $naturezas[] = [
                'id' => $natureza->id,
                'nome' => $natureza->nome,
            ];
        }

        if ($promo) {
            $naturezas[] = [
                'id' => 1,
                'nome' => 'Promocional',
            ];
        }

        //dd($naturezas);
        //$variacoes = ArtwVariacoes
        //::select('artw_variacoes.*')
        //->join('skus', 'skus.id', '=', 'artw_variacoes.id_sku')
        //->where('skus.id_produto', $product)
        //->get();
        //dd($skus);

        return view('site.sku.index', compact('expirados', 'naturezas', 'naturezasExistentes', 'tiposExistente', 'skus', 'produtoId', 'produto', 'family', 'tiposEmbalagens', 'recentSku', 'favorites', 'user', 'promo'));
    }

    public function guide($family, $product, $guide, Request $request)
    {
        $guides = new SiteGuides;
        $produto = Produtos::find($product);
        $token = $request->token;
        $user = null;
        $arrVersoes = array();
        $versoes = $produto->versoes;

        $produtoId = $product;

        foreach ($produto->logotipos as $logo) {
            if ($logo->id_tipo_logotipo == 2)
                $produto->logotipo = $logo->thumb;
        }

        switch ($guide) {
            case 'cores':
                $produto->tiposCores = $guides->getCores($produto->cores)['tiposCores'];
                $produto->niveisCores = $guides->getCores($produto->cores)['niveisCores'];
                break;
            case 'versoes':
                $produto->categoriasVersoes = $guides->getVersoes($versoes)['categoriasVersoes'];
                $produto->tiposVersoes = $guides->getVersoes($versoes)['tiposVersoes'];
                break;
            case 'proporcoes':
                $produto->categoriasProporcoes = $guides->getProporcoes($produto->proporcoes)['categoriasProporcoes'];
                $produto->tiposProporcoes = $guides->getProporcoes($produto->proporcoes)['tiposProporcoes'];
                break;
            case 'usos-aplicacoes':
                $produto->tiposAplicacoes = $guides->getAplicacoes($produto->usosAplicacoes)['tiposAplicacoes'];
                break;
            case 'usos-incorretos':
                $produto->tiposUsosIncorretos = $guides->getUsosIncorretos($produto->usosIncorretos)['tiposUsosIncorretos'];
                break;
            case 'tipografias':
                $produto->tiposTipografias = $guides->getTipografias($produto->tipografias)['tiposTipografias'];
                break;
            case 'icones':
                $produto->categoriasIcones = $guides->getIcones($produto->icones)['categoriasIcones'];
                break;
            case 'enxovais':
                $produto->enxovais;
                break;
            default:
                return view('errors.404');
                break;
        }

        foreach ($versoes as $versao) {
            // Versão Preferencial para Download de Logotipo
            if ($versao->id_tipo_proporcao_versao == 1 && $versao->id_categoria_versao == 1)
                $produto->logoDown = $versao->path_down;
            else
                $arrVersoes[] = $versao;
        }

        $produto->versoes = $arrVersoes;

        if (!Auth::guest()) {
            $adm = new AdmUsuarios;
            $user = $adm->find(Auth::user()->id);
            $userRelations = $this->siteCommon->checkUserRelations();

            $user->favorites = $userRelations->favorites;
            $user->familias = $userRelations->familias;
            $user->modulos = $userRelations->modulos;
        }

        if ((isset($produto->logoDown) && count($produto->versoes) > 1) || count($produto->cores) > 0 || count($produto->tipografias) > 0 || count($produto->usosIncorretos) > 0 || count($produto->usosAplicacoes) > 0 || count($produto->proporcoes) > 0 || count($produto->icones) > 0)
            $produto->checkGuides = true;
        else
            $produto->checkGuides = false;

        if (count($produto->enxovais) > 0)
            $produto->checkEnxovais = true;
        else
            $produto->checkEnxovais = false;

        $recentSku = $this->siteCommon->getRecentSku(true);
        $favorites = $this->siteCommon->getFavorites(true);

        return view('site.guide.' . $guide, compact('produto', 'product', 'produtoId', 'family', 'recentSku', 'favorites', 'token', 'guide', 'user'));
    }

    public function dashboard() {
        return view('site.dashboard.index');
    }

    public function guideNew()
    {
        return view('site.guidenew');
    }

    public function guideNewBrahma()
    {
        return view('site.guidenewbrahma');
    }

    public function getUser()
    {
        $adm = new AdmUsuarios;
        $user = $adm->find(Auth::user()->id);
        $recentSku = $this->siteCommon->getRecentSku(true);
        $favorites = $this->siteCommon->getFavorites(true);

        return view('site.config.index', compact('user', 'recentSku', 'favorites'));
    }

    public function userConfig()
    {
        $adm = new AdmUsuarios;
        $user = $adm->find(Auth::user()->id);
        $recentSku = $this->siteCommon->getRecentSku(true);
        $favorites = $this->siteCommon->getFavorites(true);

        return view('site.config.configuracoes', compact('user', 'recentSku', 'favorites'));
    }

    public function getManager()
    {
        $allUsers = AdmUsuarios::whereNotIn('id_adm_perfil', [1, 2, Auth::user()->id_adm_perfil])->get();
        $allProfiles = AdmPerfis::whereNotIn('id', [1, 2, Auth::user()->id_adm_perfil])->get();
        $userRelations = $this->siteCommon->checkUserRelations();
        $familias = $userRelations->familias;
        $users = null;
        $profiles = null;
        $user = null;

        if (!Auth::guest()) {
            $adm = new AdmUsuarios;
            $user = $adm->find(Auth::user()->id);
            $userRelations = $this->siteCommon->checkUserRelations();

            $user->favorites = $userRelations->favorites;
            $user->familias = $userRelations->familias;
            $user->modulos = $userRelations->modulos;
        }

        foreach ($allUsers as $value) {
            $checkUser = false;
            $value->admPerfis;
            $value->admPerfis->familias;
            foreach ($value->admPerfis->familias as $familia) {
                if (in_array($familia->id, $familias))
                    $checkUser = true;
            }
            if ($checkUser || in_array(Auth::user()->id_adm_perfil, [1, 2]))
                $users[] = $value;
        }

        foreach ($allProfiles as $profile) {
            $checkProfile = false;
            foreach ($profile->familias as $familia) {
                if (in_array($familia->id, $familias))
                    $checkProfile = true;
            }
            if ($checkProfile || in_array(Auth::user()->id_adm_perfil, [1, 2]))
                $profiles[$profile->id] = $profile->nome;
        }

        $recentSku = $this->siteCommon->getRecentSku(true);
        $favorites = $this->siteCommon->getFavorites(true);
        $users = $this->siteCommon->paginate($users);

        dd($users);

        return view('site.manager.index', compact('users', 'profiles', 'recentSku', 'favorites', 'user'));
    }

    public function postManager(Request $request)
    {
        if (empty($request->nome) || empty($request->sobrenome) || empty($request->id_adm_perfil)) {
            return false;
        }

        if ($request->id == "")
            return $this->siteCommon->createFrontUser($request);
        else
            return $this->siteCommon->editFrontUser($request);
    }

    public function moreFavorites()
    {
        $user = null;

        if (!Auth::guest()) {
            $adm = new AdmUsuarios;
            $user = $adm->find(Auth::user()->id);
            $userRelations = $this->siteCommon->checkUserRelations();

            $user->favorites = $userRelations->favorites;
            $user->familias = $userRelations->familias;
            $user->modulos = $userRelations->modulos;
        }

        $likes = $this->siteCommon->getFavorites(false);
        $recentSku = $this->siteCommon->getRecentSku(true);
        $favorites = $this->siteCommon->getFavorites(true);

        $allRecentsFavorites = $this->siteCommon->paginate($likes);

        return view('site.more.favorites', compact('allRecentsFavorites', 'recentSku', 'favorites', 'user'));
    }

    public function moreLasts()
    {
        $user = null;

        if (!Auth::guest()) {
            $adm = new AdmUsuarios;
            $user = $adm->find(Auth::user()->id);
            $userRelations = $this->siteCommon->checkUserRelations();

            $user->favorites = $userRelations->favorites;
            $user->familias = $userRelations->familias;
            $user->modulos = $userRelations->modulos;
        }

        $skus = $this->siteCommon->getRecentSku(false);
        $recentSku = $this->siteCommon->getRecentSku(true);
        $favorites = $this->siteCommon->getFavorites(true);

        $allRecentsSkus = $this->siteCommon->paginate($skus);

        return view('site.more.lasts', compact('allRecentsSkus', 'recentSku', 'favorites', 'user'));
    }

    public function about()
    {
        $recentSku = $this->siteCommon->getRecentSku(true);
        $favorites = $this->siteCommon->getFavorites(true);
        $user = null;

        if (!Auth::guest()) {
            $adm = new AdmUsuarios;
            $user = $adm->find(Auth::user()->id);
            $userRelations = $this->siteCommon->checkUserRelations();

            $user->favorites = $userRelations->favorites;
            $user->familias = $userRelations->familias;
            $user->modulos = $userRelations->modulos;
        }
        return view('site.more.about', compact('recentSku', 'favorites', 'user'));
    }

    public function suport()
    {
        $recentSku = $this->siteCommon->getRecentSku(true);
        $favorites = $this->siteCommon->getFavorites(true);
        $user = null;

        if (!Auth::guest()) {
            $adm = new AdmUsuarios;
            $user = $adm->find(Auth::user()->id);
            $userRelations = $this->siteCommon->checkUserRelations();

            $user->favorites = $userRelations->favorites;
            $user->familias = $userRelations->familias;
            $user->modulos = $userRelations->modulos;
        }
        return view('site.more.suport', compact('recentSku', 'favorites', 'user'));
    }

    public function news()
    {
        $recentSku = $this->siteCommon->getRecentSku(true);
        $favorites = $this->siteCommon->getFavorites(true);
        $logs = $this->siteCommon->getNews();
        $news = $this->siteCommon->paginate($logs);
        $user = null;

        if (!Auth::guest()) {
            $adm = new AdmUsuarios;
            $user = $adm->find(Auth::user()->id);
            $userRelations = $this->siteCommon->checkUserRelations();

            $user->favorites = $userRelations->favorites;
            $user->familias = $userRelations->familias;
            $user->modulos = $userRelations->modulos;
        }

        return view('site.more.news', compact('recentSku', 'favorites', 'news', 'user'));
    }

    public function search($search)
    {
        $user = null;
        $results = array();
        $total = array();
        $types = $this->siteCommon->getSearch($search)['result'];

        if (!Auth::guest()) {
            $adm = new AdmUsuarios;
            $user = $adm->find(Auth::user()->id);
            $userRelations = $this->siteCommon->checkUserRelations();

            $user->favorites = $userRelations->favorites;
            $user->familias = $userRelations->familias;
            $user->modulos = $userRelations->modulos;
        }

        foreach ($types as $type) {
            foreach ($type as $value) {
                $total[] = $value;
            }
        }

        $results = $this->siteCommon->paginate($total);

        $recentSku = $this->siteCommon->getRecentSku(true);
        $favorites = $this->siteCommon->getFavorites(true);

        return view('site.busca.index', compact('results', 'recentSku', 'favorites', 'search', 'user'));
    }

    public function manageUsers(Common $common)
    {
        //dd(Auth::user()->admModulos);

        $usuarios = AdmUsuarios
            ::orderBy('nome')
            ->orderBy('sobrenome')
            ->where('id', '!=', 1)
            //->orWhere('admin', '!=', 1)
            //->orWhere('admin', null)
            ->get();

        $usuariosValidos = $common->getUsuariosValidos()->count();
        $limiteUsuarios = $common->getConfiguracao('limite_usuarios');

        return view('site.usuarios.gerenciar_v2', compact('usuarios', 'limiteUsuarios', 'usuariosValidos'));
    }

    public function notificacoes()
    {
        //dd(Auth::user()->admModulos);

        $todasNotificacoes = ArtwNotificacoes
            ::select(
                    'artw_notificacoes.*',
                    'artw_notificacoes_envios.status AS envio_status',
                    'artw_notificacoes_envios.created_at as envio_created_at'
                    )
            ->leftJoin('artw_notificacoes_envios', 'artw_notificacoes.id', '=', 'artw_notificacoes_envios.id_notificacao')
            ->orderBy('artw_notificacoes.created_at', 'DESC')
            ->orderBy('artw_notificacoes_envios.created_at', 'DESC')
            ->limit(500)
            ->get();

        $notificacoes = [];

        foreach ($todasNotificacoes as $i=>$notificacao)
        {
            $notificacoes[$i]['id'] = $notificacao->id;
            $notificacoes[$i]['remetente'] = $notificacao->from()->withTrashed()->get()->first()->nome_abreviado;
            $notificacoes[$i]['destinatario'] = $notificacao->to()->withTrashed()->get()->first()->nome_abreviado;
            $notificacoes[$i]['remetente_email'] = $notificacao->from()->withTrashed()->get()->first()->email;
            $notificacoes[$i]['destinatario_email'] = $notificacao->to()->withTrashed()->get()->first()->email;
            $notificacoes[$i]['assunto'] = $notificacao->assunto;
            $notificacoes[$i]['publico'] = $notificacao->publico;
            $notificacoes[$i]['data'] = $notificacao->created_at->format('d/m/Y H:i:s');

            switch ($notificacao->envio_status) {
                case 1:
                    $envio_status = 'Enviado';
                    break;
                case 2:
                    $envio_status = 'Enviado';
                    break;
                case 3:
                    $envio_status = 'Enviado';
                    break;
                case 4:
                    $envio_status = 'Enviado';
                    break;
                case '':
                    $envio_status = 'Pendente';
                    break;
                default:
                    $envio_status = 'Erro';
                    break;
            }

            $notificacoes[$i]['envio_status'] = $envio_status;
            $notificacoes[$i]['envio_data'] = ($notificacao->envio_created_at) ? date('d/m/Y H:i:s', strtotime($notificacao->envio_created_at)) : '-';
            $notificacoes[$i]['link'] = route('site.notificacao', $notificacao->id);
        }

        return view('site.notificacoes.index', compact('notificacoes'));
    }

    public function testarNotificacao($id_notificacao)
    {
        $notificacao = ArtwNotificacoes::find($id_notificacao);

        $ticket = $notificacao->artwTicket;

        return view('emails.notificacoes.' . $notificacao->artwNotificacaoTipo->template, compact('notificacao', 'ticket'));
    }

    public function listUsers(AdmUsuarios $admUsuarios)
    {
        $usuarios = $admUsuarios->all();

        return view('site.usuarios.gerenciar', compact('usuarios'));
    }

    public function getAddUser(AdmEmpresas $admEmpresas, AdmCargos $admCargos, AdmDepartamentos $admDepartamentos, AdmPerfis $admPerfis, Common $common)
    {
        $usuariosValidos = $common->getUsuariosValidos()->count();
        $limiteUsuarios = $common->getConfiguracao('limite_usuarios');

        // bloqueia acesso caso o número de usuários esteja acima do permitido
        if ($usuariosValidos >= $limiteUsuarios)
        {
            return redirect()->route("site.usuarios.gerenciar_v2")->with('error', 'Limite de Usuários atingido.');
        }

        $empresas = $admEmpresas->orderBy('nome')->get();
        $cargos = $admCargos->orderBy('nome')->get();
        $departamentos = $admDepartamentos->orderBy('nome')->get();

        return view('site.usuarios.adicionar', compact('empresas', 'cargos', 'departamentos', 'usuariosValidos', 'limiteUsuarios'));
    }

    public function postAddUser(Requests\UsuariosAdicionar $request, Common $common)
    {
        DB::beginTransaction();

        // Empresa
        if ($request->input('id_adm_empresa') == 0 && $request->input('empresa_nome')) {

            $empresa = AdmEmpresas::firstOrNew(['nome' => $request->input('empresa_nome')]);

            if (!$empresa->exists) {
                $empresa->save();
            }
        } else {

            $empresa = AdmEmpresas::find($request->input('id_adm_empresa'));
        }

        // Departamento
        if ($request->input('id_departamento') == 0 && $request->input('departamento_nome')) {

            $departamento = AdmDepartamentos::firstOrNew(['nome' => $request->input('departamento_nome')]);

            if (!$departamento->exists) {
                $departamento->save();
            }
        } else {

            $departamento = AdmDepartamentos::find($request->input('id_departamento'));
        }

        // Cargo
        if ($request->input('id_cargo') == 0 && $request->input('cargo_nome')) {

            $cargo = AdmCargos::firstOrNew(['nome' => $request->input('cargo_nome')]);

            if (!$cargo->exists) {
                $cargo->save();
            }
        } else {

            $cargo = AdmCargos::find($request->input('id_cargo'));
        }

        $token = str_random(60);

        $usuario = new AdmUsuarios;
        $usuario->id_adm_empresa = $empresa->id;
        $usuario->nome = $request->input('nome');
        $usuario->sobrenome = $request->input('sobrenome');
        $usuario->email = $request->input('email');
        $usuario->tem_token = 1;
        $usuario->fone = $request->input('telefone');
        $usuario->primeiro_acesso = 1;
        $usuario->status = ($request->input('status')) ? 1 : 0;
        $usuario->gerente = $request->input('gerente');
        $usuario->gerencia_usuario = $request->input('gerencia_usuario');
        $usuario->gerencia_midia = $request->input('gerencia_midia');
        $usuario->gerencia_fornecedor = $request->input('gerencia_fornecedor');
        $usuario->gerencia_embalagem = $request->input('gerencia_embalagem');
        $usuario->gerencia_foto = $request->input('gerencia_foto');
        $usuario->id_departamento = $departamento->id;
        $usuario->id_cargo = ($cargo) ? $cargo->id : null;

        $usuario->save();

        $usuario->admModulos()->sync($request->input('permissao'));
        $usuario->admPerfis()->sync($request->input('perfis'));

        $usuarioToken = new AdmUsuariosTokens();
        $usuarioToken->id_usuario = $usuario->id;
        $usuarioToken->token = $token;
        $usuarioToken->tipo = 1;
        $usuarioToken->expira_em = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' +1 day'));
        $usuarioToken->save();

        $data = array(
            'nome' => htmlentities($usuario->nome_abreviado),
            'email' => $usuario->email,
            'token' => $token
        );
        
        //return view('emails.cadastro', $data);
        
        Mail::send('emails.cadastro', $data, function ($message) use($data) {
            $message->from(config('app.mail_from'), config('app.mail_name'));
            $message->to($data['email'])->subject('Cadastro no Sistema');
            //$message->to('bruno@rubrum.com.br')->subject('Cadastro no Sistema STG');
        });

        DB::commit();

        return redirect()->route("site.usuarios.gerenciar_v2")->with('message', 'Usuário criado com sucesso.');
    }

    public function userConfigV2(AdmEmpresas $admEmpresas, AdmCargos $admCargos, AdmDepartamentos $admDepartamentos, AdmPerfis $admPerfis)
    {
        $adm = new AdmUsuarios;
        $user = $adm->find(Auth::user()->id);

        $empresas = $admEmpresas->orderBy('nome')->get();
        $cargos = $admCargos->orderBy('nome')->get();
        $departamentos = $admDepartamentos->orderBy('nome')->get();

        return view('site.config.configuracoes_v2', compact('user', 'recentSku', 'favorites', 'empresas', 'cargos', 'departamentos'));
    }

    public function postUser(Requests\UserConfigracao $request)
    {
        $user = $request->all();

        if (!empty($user['old_password'])) {

            if (!Hash::check($user['old_password'], Auth::user()->senha)) {
                return redirect()->route("site.user")->withError('Senha Antiga não confere. Verifique e tente novamente!');
            }

            /*
              if (!Hash::check($user['old_password'], Auth::user()->senha)) {
              return redirect()->route("site.user")->withError('Senha Antiga não confere. Verifique e tente novamente!');
              }

              $rules = array(
              'nome' => 'required',
              'sobrenome' => 'required',
              'fone' => 'fone',
              'password' => 'required|min:6|confirmed',
              'password_confirmation' => 'required|min:6'
              );

              $messages = array(
              'password.required' => 'Digite a nova senha.',
              'password.min' => 'A senha deve conter no mínimo 6 caracteres.',
              'password.confirmed' => 'Valores digitados não conferem.',
              'password_confirmation.required' => 'Digite a confirmação da nova senha.',
              'password_confirmation.min' => 'Confirmação deve conter no mínimo 6 caracteres.'
              );

              $validator = Validator::make($user, $rules, $messages);

              dd($validator);

              if (!$validator->passes()) {
              return redirect('site/user')->withError($validator);
              }
             * 
             */

            $data['senha'] = bcrypt($user['password']);
        }

        // Departamento
        if ($request->input('id_departamento') == 0 && $request->input('departamento_nome')) {

            $departamento = AdmDepartamentos::firstOrNew(['nome' => $request->input('departamento_nome')]);

            if (!$departamento->exists) {
                $departamento->save();
            }
        } else {

            $departamento = AdmDepartamentos::find($request->input('id_departamento'));
        }

        // Cargo
        if ($request->input('id_cargo') == 0 && $request->input('cargo_nome')) {

            $cargo = AdmCargos::firstOrNew(['nome' => $request->input('cargo_nome')]);

            if (!$cargo->exists) {
                $cargo->save();
            }
        } else {

            $cargo = AdmCargos::find($request->input('id_cargo'));
        }

        $data['nome'] = $user['nome'];
        $data['sobrenome'] = $user['sobrenome'];
        $data['fone'] = $user['fone'];
        $data['id_cargo'] = ($cargo) ? $cargo->id : null;
        $data['id_departamento'] = $departamento->id;

        $data['notificacao_favoritos'] = isset($user['notificacao_favoritos']) ? 1 : 0;

        $adm = new AdmUsuarios;
        $updateUser = $adm->find(Auth::user()->id);

        if ($updateUser->update($data)) {
            return redirect()->route("site.user")->withMessage('Cadastro alterado com sucesso!');
        }

        return redirect()->route("site.user")->withMessage('Erro ao atualizar. Tente novamente mais tarde!');
    }

    public function getEditUser($id, AdmEmpresas $admEmpresas, AdmCargos $admCargos, AdmDepartamentos $admDepartamentos, AdmPerfis $admPerfis, Common $common)
    {
        $usuariosValidos = $common->getUsuariosValidos()->count();
        $limiteUsuarios = $common->getConfiguracao('limite_usuarios');

        $usuario = AdmUsuarios::find($id);

        $perfisSelecionados = [];

        foreach ($usuario->admPerfis as $perfil) {

            $perfisSelecionados[] = [
                'id_perfil' => $perfil->id,
                'nome' => $perfil->nome
            ];
        }

        $modulos = [];

        foreach ($usuario->admModulos as $modulo) {

            $modulos[$modulo->id] = $modulo->id;
        }

        $perfisSelecionados = json_encode($perfisSelecionados);

        $empresas = $admEmpresas->orderBy('nome')->get();
        $cargos = $admCargos->orderBy('nome')->get();
        $departamentos = $admDepartamentos->orderBy('nome')->get();

        return view('site.usuarios.editar', compact('usuario', 'empresas', 'cargos', 'departamentos', 'perfisSelecionados', 'modulos', 'usuariosValidos', 'limiteUsuarios'));
    }

    public function getDeleteUser($id)
    {
        $usuario = AdmUsuarios::find($id);

        DB::beginTransaction();

        $usuario->email_excluido = $usuario->email;
        $usuario->email = null;
        $usuario->save();

        $usuario->delete();

        DB::commit();

        return redirect()->route("site.usuarios.gerenciar_v2")->with('message', 'Usuário excluído com sucesso.');
    }

    public function postEditUser($id, Request $request)
    {
        DB::beginTransaction();

        $checkUser = AdmUsuarios::where('email', $request->input('email'))->where('id', '!=', $id)->count();

        if ($checkUser)
        {
            return back()->withErrors('E-mail já cadastrado');
        }

        // Empresa
        if ($request->input('id_adm_empresa') == 0 && $request->input('empresa_nome')) {

            $empresa = AdmEmpresas::firstOrNew(['nome' => $request->input('empresa_nome')]);

            if (!$empresa->exists) {
                $empresa->save();
            }
        } else {

            $empresa = AdmEmpresas::find($request->input('id_adm_empresa'));
        }

        // Departamento
        if ($request->input('id_departamento') == 0 && $request->input('departamento_nome')) {

            $departamento = AdmDepartamentos::firstOrNew(['nome' => $request->input('departamento_nome')]);

            if (!$departamento->exists) {
                $departamento->save();
            }
        } else {

            $departamento = AdmDepartamentos::find($request->input('id_departamento'));
        }

        // Cargo
        if ($request->input('id_cargo') == 0 && $request->input('cargo_nome')) {

            $cargo = AdmCargos::firstOrNew(['nome' => $request->input('cargo_nome')]);

            if (!$cargo->exists) {
                $cargo->save();
            }
        } else {

            $cargo = AdmCargos::find($request->input('id_cargo'));
        }

        $usuario = AdmUsuarios::find($id);

        $usuario->id_adm_empresa = $empresa->id;
        $usuario->nome = $request->input('nome');
        $usuario->sobrenome = $request->input('sobrenome');
        $usuario->email = $request->input('email');
        $usuario->fone = $request->input('telefone');
        $usuario->status = ($request->input('status')) ? 1 : 0;

        if ($request->input('status'))
        {
            $usuario->gerente = $request->input('gerente');
            $usuario->gerencia_usuario = $request->input('gerencia_usuario');
            $usuario->gerencia_midia = $request->input('gerencia_midia');
            $usuario->gerencia_fornecedor = $request->input('gerencia_fornecedor');
            $usuario->gerencia_embalagem = $request->input('gerencia_embalagem');
            $usuario->gerencia_foto = $request->input('gerencia_foto');
        }
        
        $usuario->id_departamento = $departamento->id;
        $usuario->id_cargo = ($cargo) ? $cargo->id : null;

        $usuario->save();

        if ($request->input('permissao')) {
            $usuario->admModulos()->sync($request->input('permissao'));
        }

        if ($request->input('perfis')) {
            $usuario->admPerfis()->sync($request->input('perfis'));
        }

        DB::commit();

        return redirect()->route("site.usuarios.gerenciar_v2")->with('message', 'Usuário alterado com sucesso.');
    }

    
    public function departmentsManager() {

        $departamentos = AdmDepartamentos
            ::select('adm_departamentos.*')
            ->orderBy('nome')->get();

        return view('admin.departamentos.gerenciar', compact('departamentos'));
    }

    public function addDepartment() {
        return view('admin.departamentos.adicionar');
    }

    public function editDepartment($id) {

        $departamento = AdmDepartamentos::find($id);

        return view('admin.departamentos.editar', compact('departamento'));
    }

    public function saveDepartment(Request $request, $id='')
    {
        if ($id)
        {
            $check = AdmDepartamentos::where('nome', $request->input('nomeDepartamento'))->where('id', '!=', $id)->count();

            if ($check) {
                return response()->json(['success' => 201, 'message' => 'Departamento já existe.'], 200);
            }

            $departamento = AdmDepartamentos::find($id);
            $departamento->nome = $request->input('nomeDepartamento');
        }
        else
        {
            $departamento = AdmDepartamentos::firstOrNew(['nome' => $request->input('nomeDepartamento')]);

            if ($departamento->exists) {
                return response()->json(['success' => 201, 'message' => 'Departamento já existe.'], 200);
            }
        }

        DB::beginTransaction();
        
        $departamento->status = ($request->input('chkDepartamentoAtivo') == 'true') ? true : false;
        $departamento->save();

        DB::commit();

        $url = route('admin.departamentos.gerenciar');

        return response()->json(['success' => 200, 'message' => 'Departamento criado com sucesso.', 'url' => $url], 200);

    }

    public function deleteDepartment($id)
    {
        $departamento = AdmDepartamentos::find($id);
        $departamento->delete();

        $url = route('admin.departamentos.gerenciar');

        return response()->json(['success' => 200, 'message' => 'Departamento excluído com sucesso.', 'url' => $url], 200);
    }

    public function positionsManager() {

        $cargos = AdmCargos
            ::select('adm_cargos.*')
            ->orderBy('nome')->get();

        return view('admin.cargos.gerenciar', compact('cargos'));
    }

    public function addPosition() {
        return view('admin.cargos.adicionar');
    }

    public function editPosition($id) {

        $cargo = AdmCargos::find($id);

        return view('admin.cargos.editar', compact('cargo'));
    }

    public function savePosition(Request $request, $id='')
    {
        if ($id)
        {
            $check = AdmCargos::where('nome', $request->input('nomeCargo'))->where('id', '!=', $id)->count();

            if ($check) {
                return response()->json(['success' => 201, 'message' => 'Cargo já existe.'], 200);
            }

            $cargo = AdmCargos::find($id);
            $cargo->nome = $request->input('nomeCargo');
        }
        else
        {
            $cargo = AdmCargos::firstOrNew(['nome' => $request->input('nomeCargo')]);

            if ($cargo->exists) {
                return response()->json(['success' => 201, 'message' => 'Cargo já existe.'], 200);
            }
        }

        DB::beginTransaction();
        
        $cargo->status = ($request->input('chkCargoAtivo') == 'true') ? true : false;
        $cargo->save();

        DB::commit();

        $url = route('admin.cargos.gerenciar');

        return response()->json(['success' => 200, 'message' => 'Cargo criado com sucesso.', 'url' => $url], 200);

    }

    public function deletePosition($id)
    {
        $cargo = AdmCargos::find($id);
        $cargo->delete();

        $url = route('admin.cargos.gerenciar');

        return response()->json(['success' => 200, 'message' => 'Cargo excluído com sucesso.', 'url' => $url], 200);
    }
    
    public function instructionTypesManager() {
        return view('admin.tipos-instrucao.gerenciar');
    }
    
    public function addInstructionType() {
        return view('admin.tipos-instrucao.adicionar');
    }
    
    public function editInstructionType() {
        return view('admin.tipos-instrucao.editar');
    }
    
    public function packagingItemsManager() {

        $itemTipos = ArtwItemTipos
            ::select('artw_item_tipos.*')
            ->orderBy('nome')->get();

        return view('admin.itens-embalagem.gerenciar', compact('itemTipos'));
    }

    public function deletePackagingItem($id)
    {
        $itemTipo = ArtwItemTipos::find($id);
        $itemTipo->delete();

        $url = route('admin.itens-embalagem.gerenciar');

        return response()->json(['success' => 200, 'message' => 'Item de Embalagem excluído com sucesso.', 'url' => $url], 200);
    }

    public function savePackagingItem(Request $request, $id='')
    {
        if ($id)
        {
            $check = ArtwItemTipos::where('nome', $request->input('nomeItemEmbalagem'))->where('id', '!=', $id)->count();

            if ($check) {
                return response()->json(['success' => 201, 'message' => 'Item de Embalagem já existe.'], 200);
            }

            $itemTipo = ArtwItemTipos::find($id);
            $itemTipo->nome = $request->input('nomeItemEmbalagem');
        }
        else
        {
            $itemTipo = ArtwItemTipos::firstOrNew(['nome' => $request->input('nomeItemEmbalagem')]);

            if ($itemTipo->exists) {
                return response()->json(['success' => 201, 'message' => 'Item de Embalagem já existe.'], 200);
            }
        }

        DB::beginTransaction();
        
        $itemTipo->status = ($request->input('chkItemEmbalagemAtivo') == 'true') ? true : false;
        $itemTipo->save();

        DB::commit();

        $url = route('admin.itens-embalagem.gerenciar');

        return response()->json(['success' => 200, 'message' => 'Item de Embalagem criado com sucesso.', 'url' => $url], 200);

    }
    
    public function addPackagingItem() {
        return view('admin.itens-embalagem.adicionar');
    }
    
    public function editPackagingItem($id) {

        $itemTipo = ArtwItemTipos::find($id);

        return view('admin.itens-embalagem.editar', compact('itemTipo'));

    }

    public function workgroups() {
        return view('admin.grupos-trabalho.gerenciar');
    }

    public function workgroupAdd() {
        return view('admin.grupos-trabalho.adicionar');
    }

    public function workgroupEdit() {
        return view('admin.grupos-trabalho.editar');
    }

    public function supplierManager()
    {
        return view('site.fornecedores.gerenciar');
    }

    public function addSupplier()
    {
        return view('site.fornecedores.adicionar');
    }

    public function groupsManager()
    {
        return view('site.grupos.gerenciar');
    }

    public function addGroups()
    {
        return view('site.grupos.adicionar');
    }

    public function manageFavorites()
    {

        $user = null;

        if (!Auth::guest()) {
            $adm = new AdmUsuarios;
            $user = $adm->find(Auth::user()->id);
            $userRelations = $this->siteCommon->checkUserRelations();

            $user->favorites = $userRelations->favorites;
            $user->familias = $userRelations->familias;
            $user->modulos = $userRelations->modulos;
        }

        $likes = $this->siteCommon->getFavorites(false);
        $recentSku = $this->siteCommon->getRecentSku(true);
        $favorites = $this->siteCommon->getFavorites(true);

        $allRecentsFavorites = $this->siteCommon->paginate($likes);

        return view('site.more.favorites_new', compact('allRecentsFavorites', 'recentSku', 'favorites', 'user'));
    }

    public function recentlyAddedItems()
    {
        $user = null;

        if (!Auth::guest()) {
            $adm = new AdmUsuarios;
            $user = $adm->find(Auth::user()->id);
            $userRelations = $this->siteCommon->checkUserRelations();
            $user->favorites = $userRelations->favorites;
            $user->familias = $userRelations->familias;
            $user->modulos = $userRelations->modulos;
        }

        $skus = $this->siteCommon->getRecentSku(false);
        $recentSku = $this->siteCommon->getRecentSku(true);
        $favorites = $this->siteCommon->getFavorites(true);
        $allRecentsSkus = $this->siteCommon->paginate($skus);

        return view('site.more.recents', compact('allRecentsSkus', 'recentSku', 'favorites', 'user'));
    }
    
    public function escalabrahma()
    {
        return redirect()->route("site.guidenew.brahma");
    }

    public function politicaPrivacidade()
    {
        return view('site.politica_de_privacidade.index');
    }

    public function termosDeUso()
    {
        return view('site.termos_de_uso.index');
    }
}
