<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AdmEmpresas;
use App\Components\Common;

use App\Http\Requests;

class EmpresaController extends Controller
{
    private $empresa;

    public function __construct(AdmEmpresas $empresa)
    {
    	$this->empresa = $empresa;
    }

    public function index()
    {
    	$empresas = $this->empresa->all();

        $total = count($empresas);

    	return view('admin.empresas.index', compact('empresas', 'total'));
    }

    public function search($name)
    {
    	$empresas = $this->empresa->where('nome', 'like', '%'.$name.'%')->get();

        $total = count($empresas);

    	return view('admin.empresas.index', compact('empresas', 'total'));
    }

    public function create(Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

    	$company = $this->empresa;
    	$company->create($request->all());

    	return redirect()->route("admin.company");
    }

    public function edit($id)
    {
    	$editCompany = $this->empresa->find($id);

    	return response(compact('editCompany'));
    }

    public function update($id, Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

    	$company = $this->empresa->find($id);
    	$company->update($request->all());
    }

    public function remove($id, Request $request)
    {
        $removeUser = new Common;
        $removeCompany = $this->empresa->find($id);

        foreach ($removeCompany->admUsuarios as $usuario) {
            $removeUser->removeUser($usuario->id);
        }

    	$removeCompany->delete();
    }

}
