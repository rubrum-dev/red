<?php

namespace App\Http\Controllers;

use App\AdmTokensGuides;
use Illuminate\Http\Request;
use App\Components\SiteCommon;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;

class TokenGuideController extends Controller
{
    private $token;

    public function __construct(AdmTokensGuides $token)
    {
    	$this->token = $token;
    }

    public function index()
    {
        $tokens = array();
        $allTokens = $this->token->all();

        foreach ($allTokens as $token) {
            $token->url = "/site/" . $token->produtos->id_familia . "/produtos/" . $token->id_produto . "/guide/versoes?token=" . $token->token;
            $expiracao = date_create($token->dt_expiracao);
            $token['dt_expiracao'] = date_format($expiracao, 'd/m/Y');
            $tokens[] = $token;
        }

        $total = count($tokens);

    	return view('admin.tokens.index', compact('tokens', 'total'));
    }

    public function create($produto)
    {
        $token = $this->token;
        $rand = bin2hex(random_bytes(8));
        $data = date('Y-m-d');
        $expira = date('Y-m-d', strtotime("+15 days",strtotime($data)));
        $dados = array(
            'id_produto' => $produto,
            'id_adm_usuario' => Auth::user()->id,
            'token' => $rand,
            'dt_expiracao' => $expira
        );

        $newToken = $token->create($dados);

        return $newToken->token;
    }

    public function extend($id)
    {
        $token = $this->token->find($id);

        if($token){
            $data = date('Y-m-d');
            $newData = date('Y-m-d', strtotime("+15 days",strtotime($data)));
            $token->update(array('dt_expiracao' => $newData));
            return array(
                'error' => false,
                'message' => 'Token atualizado por mais 15 dias.'
            );
        }

        return array(
            'error' => true,
            'message' => 'Erro ao atualizar o Token. Tente novamente mais tarde.'
        );
    }

    public function remove($id)
    {
        $token = $this->token->find($id);

        if($token){
            $token->delete();
            return array(
                'error' => false,
                'message' => 'Token removido.'
            );
        }

        return array(
            'error' => true,
            'message' => 'Erro ao remover o Token. Tente novamente mais tarde.'
        );
    }

    public function siteIndex()
    {
        $tokens = array();
        if(in_array(Auth::user()->id_adm_perfil, [1, 2]))
            $allTokens = $this->token->all();
        else
            $allTokens = $this->token->where('id_adm_usuario', Auth::user()->id)->get();

        foreach ($allTokens as $token) {
            $token->url = "/site/" . $token->produtos->id_familia . "/produtos/" . $token->id_produto . "/guide/versoes?token=" . $token->token;
            $expiracao = date_create($token->dt_expiracao);
            $token['dt_expiracao'] = date_format($expiracao, 'd/m/Y');
            $tokens[] = $token;
        }

        $siteCommon = new SiteCommon;

        $recentSku = $siteCommon->getRecentSku(true);
        $favorites = $siteCommon->getFavorites(true);

    	return view('site.tokens.index', compact('tokens', 'recentSku', 'favorites'));
    }
}
