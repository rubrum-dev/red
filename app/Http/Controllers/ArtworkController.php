<?php
namespace App\Http\Controllers;

use App\ArtwVersoes;
use App\Dimensoes;
use App\CategoriasProdutos;
use App\TiposEmbalagens;
use App\Produtos;
use App\Campanhas;
use App\Familias;
use App\Skus;
use App\ArtwVariacoes;
use App\ArtwCiclos;
use App\ArtwTktStatus;
use App\ArtwTickets;
use App\ArtwSolicitacaoTipos;
use App\ArtwSolicitacoes;
use App\ArtwArquivos;
use App\ArtwPerfisUsuariosTickets;
use App\ArtwPerfisUsuariosEquipeTickets;
use App\ArtwTicketsStatus;
use App\ArtwItemTipos;
use App\ArtwItens;
use App\ArtwVariacaoItens;
use App\ArtwArtesEnviosEmails;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Artwork;
use App\Components\Common;
use App\ArtesFinais;
use App\ArtwAprovacoes;
use App\ArtwProjetos;
use Chumper\Zipper\Zipper;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;

class ArtworkController extends Controller
{

    public function __construct()
    {
        //
    }
    
    public function atualizaArquivos()
    {
        $arquivos = ArtwArquivos::all();

        DB::beginTransaction();

        foreach ($arquivos as $arquivo)
        {
            $arquivo->descricao = basename($arquivo->path);
            $arquivo->nome_original = basename($arquivo->path);
            $arquivo->save();
        }

        DB::commit();
    }

    public function category(Common $common)
    {
        $tickets = $common->getMyLastTickets(5);

        $categorias = CategoriasProdutos
            ::select('categorias_produtos.*')
            ->join('produtos', 'produtos.id_categoria_produto', '=', 'categorias_produtos.id')
            ->where('produtos.artwork', 1)
            ->orderBy('categorias_produtos.nome')
            ->groupBy('categorias_produtos.id')
            ->get();

        return view('site.artwork.categorias', compact('categorias', 'tickets'));
    }

    public function family(Common $common, Request $request)
    {
        $familias = array();

        if ($request->input('categorias')) {

            $allProdutos = Produtos
                ::select('produtos.*')
                ->join('skus', 'skus.id_produto', '=', 'produtos.id')
                ->join('artw_variacoes', 'artw_variacoes.id_sku', '=', 'skus.id')
                //->join('artw_variacao_itens', 'artw_variacao_itens.id_variacao', '=', 'artw_variacoes.id')
                ->where('produtos.status', 1)
                ->where('id_categoria_produto', $request->input('categorias'))
                ->whereIn('id_familia', Auth::user()->familias)
                ->get();
        } else {

            $allProdutos = Produtos
                ::select('produtos.*')
                ->join('skus', 'skus.id_produto', '=', 'produtos.id')
                ->join('artw_variacoes', 'artw_variacoes.id_sku', '=', 'skus.id')
                //->join('artw_variacao_itens', 'artw_variacao_itens.id_variacao', '=', 'artw_variacoes.id')
                ->where('produtos.status', 1)
                ->whereIn('id_familia', Auth::user()->familias)
                ->get();
        }

        $categorias = CategoriasProdutos
                ::select('categorias_produtos.*')
                ->distinct()
                ->join('produtos', 'produtos.id_categoria_produto', '=', 'categorias_produtos.id')
                ->orderBy('categorias_produtos.nome')
                ->where('categorias_produtos.status', 1)
                ->get()->pluck('nome', 'id');

        foreach ($allProdutos as $produto) {
            $familias[$produto->familias->id]['nome'] = $produto->familias->nome;
        }

        asort($familias);
        return view('site.artwork.familias', compact('familias', 'tickets', 'categorias'));
    }

    public function product($family, Common $common, Request $request)
    {
        $familia = Familias::find($family);

        if (!in_array($familia->id, Auth::user()->familias))
        {
            return redirect()->route('site.artwork.family');
        }

        $produtos = array();

        if ($request->input('categorias')) {

            $allProdutos = Produtos
                ::select('produtos.*')
                ->join('skus', 'skus.id_produto', '=', 'produtos.id')
                ->join('artw_variacoes', 'artw_variacoes.id_sku', '=', 'skus.id')
                //->join('artw_variacao_itens', 'artw_variacao_itens.id_variacao', '=', 'artw_variacoes.id')
                ->where('produtos.status', 1)
                ->where('produtos.id_categoria_produto', $request->input('categorias'))
                ->where('id_familia', $family)
                ->get();

        } else {

            $allProdutos = Produtos
                ::select('produtos.*')
                ->join('skus', 'skus.id_produto', '=', 'produtos.id')
                ->join('artw_variacoes', 'artw_variacoes.id_sku', '=', 'skus.id')
                //->join('artw_variacao_itens', 'artw_variacao_itens.id_variacao', '=', 'artw_variacoes.id')
                ->where('produtos.status', 1)
                ->where('id_familia', $family)
                ->get();
            
        }

        $familia = Familias::find($family);

        foreach ($allProdutos as $produto) {
            $produtos[$produto->id] = $produto->nome;
        }

        $categorias = CategoriasProdutos
                ::select('categorias_produtos.*')
                ->distinct()
                ->join('produtos', 'produtos.id_categoria_produto', '=', 'categorias_produtos.id')
                ->orderBy('categorias_produtos.nome')
                ->where('categorias_produtos.status', 1)
                ->where('produtos.id_familia', $family)
                ->get()->pluck('nome', 'id');

        asort($produtos);
        return view('site.artwork.produtos', compact('produtos', 'familia', 'family', 'categorias'));
    }

    public function package($family, $product)
    {
        $familia = Familias::find($family);
        $produto = Produtos::find($product);

        if (!in_array($familia->id, Auth::user()->familias))
        {
            return redirect()->route('site.artwork.family');
        }

        $logo = null;
        $skus = array();
        $tipos['regular'] = 0;
        $tipos['promo'] = 0;
        $skuModel = new Skus;
        $allSkus = $skuModel
            ->select('skus.*', 'artw_variacoes.id as id_variacao', 'artw_variacoes.nome as nome_variacao', 'artw_variacoes.principal', 'artw_variacoes.id_tipo')
            ->join('artw_variacoes', 'artw_variacoes.id_sku', '=', 'skus.id')
            ->where('id_produto', $product)
            ->get();
        //dd($allSkus);
        $logos = ($allSkus->first()) ? $allSkus->first()->produtos->logotipos : [];

        foreach ($logos as $log) {
            if ($log->id_tipo_logotipo == 2) {
                $logo = $log->thumb;
            }
        }

        $cat = null;

        foreach ($allSkus as $sku) {
            if ($sku->produtos->id_categoria_produto == $cat) {
                if ($sku->promocional) {
                    $tipos['promo'] = 1;
                }
            }

            if (!$sku->promocional) {
                $tipos['regular'] = 1;
            }

            $artwVariacao = ArtwVariacoes::find($sku->id_variacao);

            if ($artwVariacao->campanha) {
                $nome = ($sku->principal == 1) ? $sku->nome . ' - ' . $artwVariacao->campanha->nome : $sku->nome . ' - ' . $artwVariacao->campanha->nome . ' - ' . $sku->nome_variacao;
            } else {
                $nome = ($sku->principal == 1) ? $sku->nome : $sku->nome . ' - ' . $sku->nome_variacao;
            }

            $skus[$sku->id_variacao] = array(
                'id' => $sku->id,
                'nome' => $nome,
                'versao' => $sku->artwVersoes->last(),
                'promo' => ($sku->id_tipo == 2) ? 1 : 0,
                'id_variacao' => $sku->id_variacao,
                'variacao' => $artwVariacao
            );
        };

        foreach ($skus as $sku) {
            //if ($sku)
        }

        asort($skus);

        $thumb = ($logo) ? $logo : 'images/layout/ticket-no-image-rubrum.png';

        //dd($thumb);

        return view('site.artwork.embalagens', compact('skus', 'familia', 'produto', 'logo', 'thumb', 'tipos', 'family', 'product'));
    }

    public function packageNew($family, $product)
    {
        $familia = Familias::find($family);
        $produto = Produtos::find($product);
        $itemTipos = ArtwItemTipos::orderBy('nome')->get();

        if (!in_array($familia->id, Auth::user()->familias))
        {
            return redirect()->route('site.artwork.family');
        }

        $logo = null;
        $skus = array();
        $tipos['regular'] = 0;
        $tipos['promo'] = 0;
        $tipos['novo'] = 0;
        $skuModel = new Skus;
        $allSkus = $skuModel
            ->select('skus.*', 'artw_variacoes.id as id_variacao', 'artw_variacoes.nome as nome_variacao', 'artw_variacoes.principal', 'artw_variacoes.id_tipo')
            ->join('artw_variacoes', 'artw_variacoes.id_sku', '=', 'skus.id')
            ->where('id_produto', $product)
            ->whereNull('artw_variacoes.deleted_at')
            ->orderBy('artw_variacoes.nome_original')
            ->orderBy('artw_variacoes.nome')
            ->get();

        $logos = ($allSkus->first()) ? $allSkus->first()->produtos->logotipos : [];

        foreach ($logos as $log) {
            if ($log->id_tipo_logotipo == 2) {
                $logo = $log->thumb;
            }
        }

        foreach ($allSkus as $sku) {

            $artwVariacao = ArtwVariacoes::find($sku->id_variacao);

            //if ($sku->produtos->id_categoria_produto == $cat) {
            if ($sku->id_tipo == 2 && !$artwVariacao->novo) {
                $tipos['promo'] = 1;
            }
            //}

            if ($sku->id_tipo != 2 && !$artwVariacao->novo) {
                $tipos['regular'] = 1;
            }

            if ($artwVariacao->novo) {
                $tipos['novo'] = 1;
            }

            /*
              if ($artwVariacao->campanha) {
              $nome = ($sku->principal != 1 && $sku->nome_variacao) ? $sku->nome . ' - ' . $sku->nome_variacao . ' - ' . $artwVariacao->campanha->nome : $sku->nome . ' - ' . $artwVariacao->campanha->nome;
              } else {
              $nome = ($sku->principal != 1 && $sku->nome_variacao) ? $sku->nome . ' - ' . $sku->nome_variacao : $sku->nome;
              }
             * 
             */

            $nome = $artwVariacao->sku_nome_completo;

            //dd($nome);

            $skus[$sku->id_variacao] = array(
                'id' => $sku->id,
                'nome' => $nome,
                'versao' => $sku->artwVersoes->last(),
                'promo' => ($sku->id_tipo == 2) ? 1 : 0,
                'id_variacao' => $sku->id_variacao,
                'variacao' => $artwVariacao
            );
        }

        foreach ($skus as $sku) {
            //if ($sku)
        }

        //asort($skus);

        $thumb = ($logo) ? $logo : 'images/layout/ticket-no-image-rubrum.png';

        //dd($thumb);

        return view('site.artwork.embalagensV2', compact('skus', 'familia', 'produto', 'logo', 'thumb', 'tipos', 'family', 'product', 'itemTipos'));
    }

    public function packageNewV3($family, $product)
    {
        $familia = Familias::find($family);

        if (!in_array($familia->id, Auth::user()->familias))
        {
            return redirect()->route('site.artwork.family');
        }

        return view('site.artwork.embalagensV3', compact('family', 'product'));
    }

    public function packageAdmin($family, $product)
    {
        return view('site.artwork.embalagensAdmin', compact('family', 'product'));
    }

    public function postPackageNew(Artwork\PostPackageNew $request)
    {
        //dd($request->all());
        // Verifica se existe um item para edição
        if ($request->input('id_item_edicao')) {

            if ($request->input('editarItemTipo') == 0) {

                $tipo = ArtwItemTipos::firstOrNew(['nome' => $request->input('editarItemTipoAdicionar')]);

                if (!$tipo->exists) {
                    $tipo->save();
                }
            } else {

                $tipo = ArtwItemTipos::find($request->input('editarItemTipo'));
            }

            $item = ArtwItens::find($request->input('id_item_edicao'));
            $item->id_tipo = $tipo->id;
            $item->save();
        }

        if ($request->input('novoItemTipo') == 0) {

            $tipo = ArtwItemTipos::firstOrNew(['nome' => $request->input('novoItemTipoAdicionar')]);

            if (!$tipo->exists) {
                $tipo->save();
            }
        } else {

            $tipo = ArtwItemTipos::find($request->input('novoItemTipo'));
        }

        $item = ArtwItens::firstOrNew(['id_tipo' => $tipo->id, 'id_variacao' => $request->input('id_variacao')]);

        if (!$item->exists) {
            $item->novo = 1;
            $item->save();
        }

        return redirect()->route("site.artwork.package", [$item->artwVariacao->sku->produtos->familias->id, $item->artwVariacao->sku->produtos->id])->with('message', 'Itens criados com sucesso.');
        //return redirect()->route('site.artwork.ticket.create.get', [$item->id]);
    }

    public function manager(Common $common, $tipo = 'todos')
    {
        $tickets = $common->getMyTickets($tipo);

        //dd($tickets);

        $minhas = $common->getMyTickets('minhas_tarefas');
        $atrasadas = $common->getMyTickets('atrasadas');

        $status = ArtwTktStatus::all();

        return view('site.artwork.gerenciar_tickets', compact('tickets', 'status', 'tipo', 'minhas', 'atrasadas'));
    }

    public function managerV2($tipo = 'todos')
    {
        $minhas = [];
        $atrasadas = [];

        if ($tipo == 'todos') {

            $status = ArtwTktStatus::
                where('nome', '!=', 'Cancelado')
                ->orderBy('nome')
                ->get();
        } else {

            $status = ArtwTktStatus::
                where('nome', '!=', 'Pausado')
                ->where('nome', '!=', 'Cancelado')
                ->orderBy('nome')
                ->get();
        }



        return view('site.artwork.gerenciar_tickets_v2', compact('status', 'tipo', 'minhas', 'atrasadas'));
    }

    public function managerV3($tipo = 'todos')
    {
        return redirect()->route("site.artwork.managerV4", [$tipo]);
    }

    public function managerV4($tipo = 'todos')
    {
        $minhas = [];
        $atrasadas = [];

        $status = ArtwTktStatus::where('exibe', 1)->orderBy('ordem')->get();

        return view('site.artwork.gerenciar_tickets_v4', compact('status', 'tipo', 'minhas', 'atrasadas'));
    }

    public function managerV5($tipo = 'todos')
    {
        $minhas = [];
        $atrasadas = [];

        $status = ArtwTktStatus::where('exibe', 1)->orderBy('ordem')->get();

        return view('site.artwork.gerenciar_tickets_v5', compact('status', 'tipo', 'minhas', 'atrasadas'));
    }

    public function getPackageCreate()
    {
        return view('site.artwork.package_create');
    }

    public function getPackageCreateV2()
    {
        return view('site.artwork.package_create_v2');
    }

    public function getPackageCreateV3()
    {
        return view('site.artwork.package_create_v3');
    }

    public function getPackageManager()
    {
        return view('site.artwork.gerenciar_embalagens');
    }

    public function getPackageEdit($id_variacao, $id_item = null)
    {
        $variacao = ArtwVariacoes::withTrashed()->find($id_variacao);

        if (!in_array($variacao->sku->produtos->familias->id, Auth::user()->familias))
        {
            return redirect()->route('site.artwork.family');
        }

        $item = ($id_item) ? ArtwItens::find($id_item) : $variacao->artwItens->first();

        $categorias = CategoriasProdutos::select('id', 'nome')->orderBy('nome')->where('status', 1)->get()->toJson();

        //dd($categorias);

        if (!$variacao) {
            abort(404);
        }

        return view('site.artwork.package_edit', compact('variacao', 'item', 'categorias'));
    }

    public function getPackageEditV3($id_variacao, $id_item = null)
    {
        $variacao = ArtwVariacoes::find($id_variacao);

        //$item = ($id_item) ? ArtwItens::find($id_item) : $variacao->artwItens->first();

        $categorias = CategoriasProdutos::select('id', 'nome')->orderBy('nome')->where('status', 1)->get()->toJson();

        //dd($categorias);

        if (!$variacao) {
            abort(404);
        }

        return view('site.artwork.package_edit_v3', compact('variacao', 'item', 'categorias'));
    }

    public function postPackageEdit(Artwork\PostPackageCreateForm $request, $id_variacao, $id_item = null)
    {
        //dd($request->all());
        // Embalagem
        if ($request->input('cbPckType') == 0) {
            $embalagem = TiposEmbalagens::firstOrNew(['nome' => $request->input('packagePackingName')]);

            if (!$embalagem->exists) {
                $embalagem->nome = $request->input('packagePackingName');
                $embalagem->status = 0;
                $embalagem->id_natureza_embalagem = $request->input('natureza');
                $embalagem->save();
            }
        } elseif ($request->input('cbPckType')) {
            $embalagem = TiposEmbalagens::find($request->input('cbPckType'));
        }

        echo '::Embalagem: ' . $embalagem->nome . '<br>';

        // Volume
        if ($request->input('cbPckContentSize') == 0) {
            $dimensao = Dimensoes::firstOrNew(['nome' => $request->input('packageContentSpec'), 'id_tipo_embalagem' => $embalagem->id]);

            if (!$dimensao->exists) {
                $dimensao->nome = $request->input('packageContentSpec');
                $dimensao->status = 0;
                //$dimensao->id_tipo_embalagem = $embalagem->id;
                $dimensao->save();
            }
        } elseif ($request->input('cbPckContentSize')) {
            $dimensao = Dimensoes::find($request->input('cbPckContentSize'));
        }

        echo '::Volume: ' . $dimensao->nome . '<br>';

        // Categoria
        if ($request->input('cbNewCategory') == 0) {
            $categoria = CategoriasProdutos::firstOrNew(['nome' => $request->input('packageCategName')]);

            if (!$categoria->exists) {
                $categoria->nome = $request->input('packageCategName');
                $categoria->status = 0;
                $categoria->save();
            }
        } elseif ($request->input('cbNewCategory')) {
            $categoria = CategoriasProdutos::find($request->input('cbNewCategory'));
        }

        echo (isset($categoria)) ? '::Categoria: ' . $categoria->nome . '<br>' : '';

        if ($request->input('cbPckFamily') == 0) {
            $familia = Familias::firstOrNew(['nome' => $request->input('packageFamilyName')]);

            if (!$familia->exists) {
                $familia->nome = $request->input('packageFamilyName');
                $familia->status = 0;
                $familia->save();
            }
        } elseif ($request->input('cbPckFamily')) {
            $familia = Familias::find($request->input('cbPckFamily'));
        }

        echo '::Família: ' . $familia->nome . '<br>';

        if ($request->input('cbPckProduct') == 0) {
            $produto = Produtos::firstOrNew(['nome' => $request->input('tfNewProductName')]);

            if (!$produto->exists) {
                $produto->nome = $request->input('tfNewProductName');
                $produto->status = 0;
                $produto->artwork = 1;
                $produto->id_familia = $familia->id;
                $produto->id_categoria_produto = $categoria->id;
                $produto->save();
            }
        } elseif ($request->input('cbPckProduct')) {
            $produto = Produtos::find($request->input('cbPckProduct'));
        }

        echo '::Produto: ' . $produto->nome . '<br>';

        if ($request->input('packageType') == 'Promocional' && $request->input('cbPckCampaign') == 0) {
            $campanha = Campanhas::firstOrNew(['nome' => $request->input('packageCampaignName')]);

            if (!$campanha->exists) {
                $campanha->save();
            }
        } elseif ($request->input('cbPckCampaign')) {
            $campanha = Campanhas::find($request->input('cbPckCampaign'));
        }

        echo (isset($campanha)) ? '::Campanha_id: ' . $campanha->id . '<br>' : '';
        echo (isset($campanha)) ? '::Campanha_nome: ' . $campanha->nome . '<br>' : '';

        //exit;
        // Verifica o SKU no banco
        $sku = Skus::firstOrNew(['id_produto' => $produto->id, 'id_dimensao' => $dimensao->id]);

        if (!$sku->exists) {
            $sku->nome = $produto->nome . ' - ' . $embalagem->nome . ' - ' . $dimensao->nome;
            //$sku->status = 0;
            //$sku->novo = 1;
            $sku->promocional = ($request->input('packageType') == 'Promocional') ? 1 : 0;
            $sku->save();
        }

        if ($request->input('packageVariation') == 'Sim') {
            $variacao_nome = $request->input('variacaoName');
            $principal = 0;
        } else {
            $variacao_nome = 'Original';
            $principal = 1;
        }

        $id_campanha = (isset($campanha) && $campanha) ? $campanha->id : null;

        $variacao = ArtwVariacoes::find($id_variacao);

        //$variacao = ArtwVariacoes::firstOrNew(['id_sku' => $sku->id, 'nome' => $variacao_nome, 'id_campanha' => $id_campanha]);

        $variacao->id_sku = $sku->id;
        $variacao->nome = $variacao_nome;
        $variacao->id_campanha = $id_campanha;
        $variacao->id_tipo = ($request->input('packageType') == 'Promocional') ? 2 : 1;
        $variacao->principal = $principal;
        //$variacao->novo = 1;

        $variacao->nome_original = $variacao->sku_nome_completo;

        $variacao->save();

        if ($id_item) {

            if ($request->input('cbPckTypeItem') == 0) {

                $tipo_item = ArtwItemTipos::firstOrNew(['nome' => $request->input('packageItemName')]);

                if (!$tipo_item->exists) {
                    $tipo_item->save();
                }
            } else {

                $tipo_item = ArtwItemTipos::find($request->input('cbPckTypeItem'));
            }

            $item = ArtwItens::find($id_item);
            $item->id_tipo = $tipo_item->id;
            $item->save();
        } else {
            //$item = ArtwItens::firstOrNew(['id_variacao' => $variacao->id]);
            //if (!$item->exists) {
            //$item->save();
            //}
        }

        //return redirect()->route('site.artwork.ticket.create.get', [$item->id]);

        return redirect()->route("site.artwork.package", [$familia->id, $produto->id])->with('message', 'Embalagem salva com sucesso.');
    }

    public function postPackageEditV3(Artwork\PostPackageCreateForm $request, $id_variacao, $id_item = null)
    {
        //dd($request->all());
        // Embalagem
        if ($request->input('cbPckType') == 0) {
            $embalagem = TiposEmbalagens::firstOrNew(['nome' => $request->input('packagePackingName')]);

            if (!$embalagem->exists) {
                $embalagem->nome = $request->input('packagePackingName');
                $embalagem->status = 0;
                $embalagem->id_natureza_embalagem = $request->input('natureza');
                $embalagem->save();
            }
        } elseif ($request->input('cbPckType')) {
            $embalagem = TiposEmbalagens::find($request->input('cbPckType'));
        }

        echo '::Embalagem: ' . $embalagem->nome . '<br>';

        // Volume
        if ($request->input('cbPckContentSize') == 0) {
            $dimensao = Dimensoes::firstOrNew(['nome' => $request->input('packageContentSpec'), 'id_tipo_embalagem' => $embalagem->id]);

            if (!$dimensao->exists) {
                $dimensao->nome = $request->input('packageContentSpec');
                $dimensao->status = 0;
                //$dimensao->id_tipo_embalagem = $embalagem->id;
                $dimensao->save();
            }
        } elseif ($request->input('cbPckContentSize')) {
            $dimensao = Dimensoes::find($request->input('cbPckContentSize'));
        }

        echo '::Volume: ' . $dimensao->nome . '<br>';

        if ($request->input('cbPckProduct') == 0) {
            $produto = Produtos::firstOrNew(['nome' => $request->input('tfNewProductName')]);

            if (!$produto->exists) {
                $produto->nome = $request->input('tfNewProductName');
                $produto->status = 0;
                $produto->artwork = 1;
                //$produto->id_familia = $familia->id;
                //$produto->id_categoria_produto = $categoria->id;
                $produto->save();
            }
        } elseif ($request->input('cbPckProduct')) {
            $produto = Produtos::find($request->input('cbPckProduct'));
        }

        $familia = $produto->familias;

        echo '::Produto: ' . $produto->nome . '<br>';

        if ($request->input('packageType') == 'Promocional' && $request->input('cbPckCampaign') == 0) {
            $campanha = Campanhas::firstOrNew(['nome' => $request->input('packageCampaignName')]);

            if (!$campanha->exists) {
                $campanha->save();
            }
        } elseif ($request->input('cbPckCampaign')) {
            $campanha = Campanhas::find($request->input('cbPckCampaign'));
        }

        echo (isset($campanha)) ? '::Campanha_id: ' . $campanha->id . '<br>' : '';
        echo (isset($campanha)) ? '::Campanha_nome: ' . $campanha->nome . '<br>' : '';

        // Verifica o SKU no banco
        $sku = Skus::firstOrNew(['id_produto' => $produto->id, 'id_dimensao' => $dimensao->id]);

        if (!$sku->exists) {
            $sku->nome = $produto->nome . ' - ' . $embalagem->nome . ' - ' . $dimensao->nome;
            //$sku->status = 0;
            //$sku->novo = 1;
            $sku->promocional = ($request->input('packageType') == 'Promocional') ? 1 : 0;
            $sku->save();
        }

        if ($request->input('packageHasVariation') == 'Sim') {
            $variacao_nome = $request->input('variacaoName');
            $principal = 0;
        } else {
            $variacao_nome = 'Original';
            $principal = 1;
        }


        $id_campanha = (isset($campanha) && $campanha) ? $campanha->id : null;

        $checkVariacao = ArtwVariacoes::select('*')
            ->where('id', '!=', $id_variacao)
            ->where('id_sku', $sku->id)
            ->where('nome', $variacao_nome)
            ->where('id_campanha', $id_campanha)
            //->where('id_sku', $sku->id)
            ->get();

        if ($checkVariacao->count()) {
            return back()->with('warning', 'O nome selecionado para esta embalagem já está sendo utilizado. É possível cadastrar essa embalagem como uma variação, ou então selecionar um nome diferente para ela.');
        }

        $variacao = ArtwVariacoes::find($id_variacao);

        //$variacao = ArtwVariacoes::firstOrNew(['id_sku' => $sku->id, 'nome' => $variacao_nome, 'id_campanha' => $id_campanha]);

        $variacao->id_sku = $sku->id;
        $variacao->nome = $variacao_nome;
        $variacao->id_campanha = $id_campanha;
        $variacao->id_tipo = ($request->input('packageType') == 'Promocional') ? 2 : 1;
        $variacao->principal = $principal;
        //$variacao->novo = 1;

        $variacao->nome_original = $variacao->sku_nome_completo;

        $variacao->save();

        if ($id_item) {

            if ($request->input('cbPckTypeItem') == 0) {

                $tipo_item = ArtwItemTipos::firstOrNew(['nome' => $request->input('packageItemName')]);

                if (!$tipo_item->exists) {
                    $tipo_item->save();
                }
            } else {

                $tipo_item = ArtwItemTipos::find($request->input('cbPckTypeItem'));
            }

            $item = ArtwItens::find($id_item);
            $item->id_tipo = $tipo_item->id;
            $item->save();
        } else {
            //$item = ArtwItens::firstOrNew(['id_variacao' => $variacao->id]);
            //if (!$item->exists) {
            //$item->save();
            //}
        }

        //return redirect()->route('site.artwork.ticket.create.get', [$item->id]);

        $mercados = ($request->input('cbPckMarket')) ? json_decode($request->input('cbPckMarket')) : [];

        //dd($mercados);

        $variacao->mercados()->sync($mercados);

        return redirect()->route("site.artwork.package", [$familia->id, $produto->id])->with('message', 'Embalagem salva com sucesso.');
    }

    public function getTicketCreateItem($id_variacao, $id_tipo)
    {
        $variacao = ArtwVariacoes::withTrashed()->find($id_variacao);
        $itemTipo = ArtwItemTipos::find($id_tipo);
        
        if (!$variacao) {

            echo 'Variação incorreta';
        }

        if (!$id_tipo) {

            echo 'Tipo incorreto';
        }

        DB::beginTransaction();

        $item = ArtwItens::firstOrNew([
                'id_variacao_parent' => $variacao->id,
                'id_tipo' => $id_tipo,
                'compartilhado' => null,
                'status' => 1
        ]);
        
        if (!$item->exists) {

            $item->status = 0;
            $item->save();
        }

        $variacaoItem = ArtwVariacaoItens::firstOrNew([
                'id_item' => $item->id,
                'id_variacao' => $variacao->id
        ]);

        if (!$variacaoItem->exists) {

            $variacaoItem->save();
        }

        DB::commit();

        return redirect()->route("site.artwork.ticket.create.get", [$variacao, $item->id]);
    }

    public function getTicketCreate($id_variacao, $id_item)
    {
        $item = ArtwItens::find($id_item);

        if (!$item) {
            abort(404);
        }

        $variacao = ArtwVariacoes::find($id_variacao);
        
        if (!$variacao) {
            abort(404);
        }
        
        $sku = $variacao->sku;

        $solicitacao_tipos = ArtwSolicitacaoTipos::lists('nome', 'id')->toArray();

        $produto = $sku->produtos;

        if ($produto->replicar_logo_marca)
        {

            $logo = (isset($produto->logotipos)) ? $produto->familias->logotipos->first() : null;
            $cor_hexa = ($produto->cor_hexa) ? $produto->cor_hexa : $produto->familias->cor_hexa;

        } else {

            $logo = (isset($produto->logotipos)) ? $produto->logotipos->where('id_tipo_logotipo', 2)->first() : null;
            $cor_hexa = $produto->cor_hexa;

        }

        $thumb = ($logo) ? $logo->thumb : 'images/layout/ticket-no-image-rubrum.png';

        return view('site.artwork.ticket_create', compact('sku', 'variacao', 'thumb', 'solicitacao_tipos', 'item', 'cor_hexa'));
    }

    public function getTicketEdit($id_variacao, $id_ticket)
    {
        $ticket = ArtwTickets::find($id_ticket);

        $item = $ticket->artwItem;

        if (!$ticket) {
            abort(404);
        }

        $sku = $ticket->sku;
        $solicitacoes = $ticket->artwCiclos->last()->artwSolicitacoes;
        $arquivos = $ticket->artwArquivos;

        $solicitacao_tipos = ArtwSolicitacaoTipos::lists('nome', 'id')->toArray();

        $produto = $sku->produtos;

        if ($produto->replicar_logo_marca)
        {

            $logo = (isset($produto->logotipos)) ? $produto->familias->logotipos->first() : null;
            $cor_hexa = ($produto->cor_hexa) ? $produto->cor_hexa : $produto->familias->cor_hexa;

        } else {

            $logo = (isset($produto->logotipos)) ? $produto->logotipos->where('id_tipo_logotipo', 2)->first() : null;
            $cor_hexa = $produto->cor_hexa;

        }

        $thumb = ($logo) ? $logo->thumb : 'images/layout/ticket-no-image-rubrum.png';

        return view('site.artwork.ticket_edit', compact('ticket', 'item', 'solicitacoes', 'arquivos', 'sku', 'thumb', 'solicitacao_tipos', 'cor_hexa'));
    }

    public function getTicketEditNew($id_ticket)
    {
        $ticket = ArtwTickets::find($id_ticket);

        $item = $ticket->artwItem;

        if (!$ticket) {
            abort(404);
        }

        $sku = $ticket->sku;
        $solicitacoes = $ticket->artwCiclos->last()->artwSolicitacoes;
        $arquivos = $ticket->artwArquivos;

        $solicitacao_tipos = ArtwSolicitacaoTipos::lists('nome', 'id')->toArray();

        $produto = $sku->produtos;

        if ($produto->replicar_logo_marca)
        {

            $logo = (isset($produto->logotipos)) ? $produto->familias->logotipos->first() : null;
            $cor_hexa = ($produto->cor_hexa) ? $produto->cor_hexa : $produto->familias->cor_hexa;

        } else {

            $logo = (isset($produto->logotipos)) ? $produto->logotipos->where('id_tipo_logotipo', 2)->first() : null;
            $cor_hexa = $produto->cor_hexa;

        }

        $thumb = ($logo) ? $logo->thumb : 'images/layout/ticket-no-image-rubrum.png';

        return view('site.artwork.ticket_edit_new_v2', compact('ticket', 'item', 'solicitacoes', 'arquivos', 'sku', 'thumb', 'solicitacao_tipos', 'cor_hexa'));
    }

    public function postTicketEditNew($id_ticket, Artwork\PostTicketEditForm $request, Common $common)
    {
        //dd($request->all());

        $ticket = ArtwTickets::find($id_ticket);

        if (!$ticket) {
            abort(404);
        }

        $sku = $ticket->sku;

        DB::beginTransaction();

        foreach ((array) $request->input('arquivos') as $id) {
            if (!array_key_exists($id, $request->input('tfArtworkFileLegend_edit'))) {
                ArtwArquivos::destroy($id);
            }
        }

        // Trata as atualizações
        foreach ((array) $request->input('cbAlterationType_edit') as $id => $id_tipo) {
            $solicitacao = ArtwSolicitacoes::find($id);
            $solicitacao->id_tipo = $id_tipo;
            $solicitacao->descricao = $request->input('txtDescriptionSpec_edit.' . $id);
            $solicitacao->save();
        }

        foreach ((array) $request->input('tfArtworkFileLegend_edit') as $id => $descricao) {
            $solicitacao = ArtwArquivos::find($id);
            $solicitacao->descricao = $descricao;
            $solicitacao->save();
        }

        $dt_envio_original = clone $ticket->dt_envio;
        $dt_fim_original = clone $ticket->dt_fim;

        if ($request->input('id_projeto') == 0)
        {

            $projeto = ArtwProjetos::firstOrNew([
                'nome' => $request->input('tfNomeProjeto')
            ]);

            if (!$projeto->exists) {

                $projeto->save();
            }

            $id_projeto = $projeto->id;

        } else {

            $id_projeto = $request->input('id_projeto');

        }

        $ticket->id_projeto = $id_projeto;
        $ticket->codigo_cliente = $request->input('codigo_cliente');

        // Salva o Ticket
        if ($request->input('tfStartDate')) {
            $ticket->dt_ini = $request->input('tfStartDate');
        }

        if ($request->input('tfArtSendDate')) {
            $ticket->dt_envio = $request->input('tfArtSendDate');
        }

        if ($request->input('txtMainDescription')) {
            $ticket->descricao = $request->input('txtMainDescription');
        }

        if ($request->input('tfDeadlineDate')) {
            $ticket->dt_fim = $request->input('tfDeadlineDate');
        }

        if ($ticket->dt_envio != $dt_envio_original || $ticket->dt_fim != $dt_fim_original) {

            if ($ticket->dt_envio != $dt_envio_original) {
                $texto[] = "Aprovação: de " . $dt_envio_original->format('d/m/Y') . " para " . $ticket->dt_envio->format('d/m/Y');
            }

            if ($ticket->dt_fim != $dt_fim_original) {
                $texto[] = "Encerramento: de " . $dt_fim_original->format('d/m/Y') . " para " . $ticket->dt_fim->format('d/m/Y');
            }

            $texto = implode("\n", $texto);

            $common->enviarNotificacao($ticket, 'data_alterada', 'dono', $texto, 0);
            $common->enviarNotificacao($ticket, 'data_alterada', 'revisores', $texto, 0);
            $common->enviarNotificacao($ticket, 'data_alterada', 'arte-finalistas', $texto, 0);
            $common->enviarNotificacao($ticket, 'data_alterada', 'seguidores', $texto, 0);
        }

        $ticket->save();

        // Pega o ciclo atual
        $ciclo = $ticket->ciclo_atual();

        // Verifica se teve alteração e envia e-mail
        if ($request->input('cbAlterationType')) {
            $common->enviarNotificacao($ticket, 'inclusao_alteracao', 'revisores', null, 1);
            $common->enviarNotificacao($ticket, 'inclusao_alteracao', 'arte-finalistas', null, 1);
            $common->enviarNotificacao($ticket, 'inclusao_alteracao', 'seguidores', null, 1);
        }

        // Salva as solicitações
        foreach ((array) $request->input('cbAlterationType') as $k => $id_tipo) {
            $solicitacao = new ArtwSolicitacoes;
            $solicitacao->id_ciclo = $ciclo->id;
            $solicitacao->id_status = 1;
            $solicitacao->tipo = 2;
            $solicitacao->id_tipo = $id_tipo;
            $solicitacao->descricao = $request->input('txtDescriptionSpec.' . $k);
            $solicitacao->id_usuario = Auth::user()->id;
            $solicitacao->save();
        }

        $relativePath = '/images/uploads/artwork/ticket/';

        foreach ((array) $request->file('artworkFileUpload') as $k => $file) {
            $extension = $file->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $filename = $hash . "." . $extension;
            $destinationPath = public_path() . $relativePath;
            $file->move($destinationPath, $filename);

            $arquivo = new ArtwArquivos();
            $arquivo->id_ticket = $ticket->id;
            $arquivo->path = $relativePath . $filename;
            $arquivo->descricao = $request->input('tfArtworkFileLegend.' . $k);
            $arquivo->save();
        }

        DB::commit();

        if (URL::previous() == route('site.artwork.ticket.editNew.get', $ticket->id)) {

            return redirect()->route('site.artwork.ticket.edit.step1.get', ['ticket' => $ticket->id]);
        } else {

            return redirect()->route('site.artwork.ticket.members.get', ['ticket' => $ticket->id]);
        }
    }

    public function postTicketCreate($id_variacao, $id_item, Artwork\PostTicketCreateForm $request, Common $common)
    {
        //dd($request->all());

        $item = ArtwItens::find($id_item);

        if (!$item) {
            abort(404);
        }
        
        $variacao = ArtwVariacoes::find($id_variacao);
        
        if (!$variacao) {
            abort(404);
        }

        $ticketsAbertos = $item->ticketsAbertosSemCancelados();

        // Faz a verificação e muda o status do Item
        if ($ticketsAbertos->count() > 0) {
            return back()->withErrors("Coincidentemente outro usuário acabou de criar um ticket para este Item enquanto você preenchia este. Como tickets iguais não podem co-existir, este ticket não foi criado. Segue o número do ticket criado pelo outro usuário <strong>" . $ticketsAbertos->last()->numero . "</strong>.");
        }

        $sku = $variacao->sku;

        DB::beginTransaction();

        if ($item->status == 0) {
            $nova_embalagem = 1;
            $item->status = 1;
            $item->save();
        } else {
            $nova_embalagem = 0;
        }

        if ($request->input('id_projeto') == 0)
        {

            $projeto = ArtwProjetos::firstOrNew([
                'nome' => $request->input('tfNomeProjeto')
            ]);

            if (!$projeto->exists) {

                $projeto->save();
            }

            $id_projeto = $projeto->id;

        } else {

            $id_projeto = $request->input('id_projeto');

        }

        // Salva o Ticket
        $ticket = new ArtwTickets;
        $ticket->dt_ini = $request->input('tfStartDate');
        $ticket->dt_fim = $request->input('tfDeadlineDate');
        $ticket->dt_envio = $request->input('tfArtSendDate');
        $ticket->descricao = $request->input('txtMainDescription');
        $ticket->id_status = 9;
        $ticket->id_projeto = $id_projeto;
        $ticket->codigo_cliente = $request->input('codigo_cliente');
        $ticket->id_sku = $sku->id;
        $ticket->id_variacao = $variacao->id;
        $ticket->id_item = $item->id;
        $ticket->id_usuario = Auth::user()->id;
        $ticket->numero = $common->geraNumeroTicket();
        $ticket->nova_embalagem = $nova_embalagem;
        $ticket->save();

        // Cria um ciclo
        $ciclo = new ArtwCiclos;
        $ciclo->id_ticket = $ticket->id;
        $ciclo->id_status = 1;
        $ciclo->numero = 1;
        $ciclo->save();

        // Salva as solicitações
        foreach ($request->input('cbAlterationType') as $k => $id_tipo) {
            $solicitacao = new ArtwSolicitacoes;
            $solicitacao->id_ciclo = $ciclo->id;
            $solicitacao->id_status = 1;
            $solicitacao->id_tipo = $id_tipo;
            $solicitacao->descricao = $request->input('txtDescriptionSpec.' . $k);
            $solicitacao->id_usuario = Auth::user()->id;
            $solicitacao->save();
        }

        DB::commit();

        $relativePath = '/images/uploads/artwork/ticket/';

        foreach ($request->file('artworkFileUpload') as $k => $file) {
            if ($file) {
                $filename = $file->getClientOriginalName();
                $filename = $common->verificaNomeArquivo($relativePath, $filename);

                $destinationPath = public_path() . $relativePath;

                $file->move($destinationPath, $filename);

                $arquivo = new ArtwArquivos();
                $arquivo->id_ticket = $ticket->id;
                $arquivo->id_usuario = Auth::user()->id;
                $arquivo->path = $relativePath . $filename;
                $arquivo->descricao = $filename;
                $arquivo->nome_original = $filename;
                $arquivo->save();

                sleep(2);
            }
        }

        // Atualiza a timeline
        $common->ticketAtualizaTimeline($ticket);

        return redirect()->route('site.artwork.ticket.members.get', ['ticket' => $ticket->id]);
    }

    public function postTicketEdit($sku_id, $ticket_id, Artwork\PostTicketEditForm $request)
    {
        dd($request->all());

        $ticket = ArtwTickets::find($ticket_id);

        if (!$ticket) {
            abort(404);
        }

        $sku = $ticket->sku;

        $common = new Common;

        DB::beginTransaction();

        // Trata as exclusões
        foreach ((array) $request->input('solicitacoes') as $id) {
            if (!array_key_exists($id, $request->input('cbAlterationType_edit'))) {
                //ArtwSolicitacoes::destroy($id);
            }
        }

        foreach ((array) $request->input('arquivos') as $id) {
            if (!array_key_exists($id, $request->input('tfArtworkFileLegend_edit'))) {
                ArtwArquivos::destroy($id);
            }
        }

        // Trata as atualizações
        foreach ((array) $request->input('cbAlterationType_edit') as $id => $id_tipo) {
            $solicitacao = ArtwSolicitacoes::find($id);
            $solicitacao->id_tipo = $id_tipo;
            $solicitacao->descricao = $request->input('txtDescriptionSpec_edit.' . $id);
            $solicitacao->save();
        }

        foreach ((array) $request->input('tfArtworkFileLegend_edit') as $id => $descricao) {
            $solicitacao = ArtwArquivos::find($id);
            $solicitacao->descricao = $descricao;
            $solicitacao->save();
        }

        // Salva o Ticket
        if ($request->input('tfStartDate')) {
            $ticket->dt_ini = $request->input('tfStartDate');
        }

        if ($request->input('tfArtSendDate')) {
            $ticket->dt_envio = $request->input('tfArtSendDate');
        }

        if ($request->input('txtMainDescription')) {
            $ticket->descricao = $request->input('txtMainDescription');
        }

        $ticket->dt_fim = $request->input('tfDeadlineDate');
        //$ticket->dt_envio = $request->input('tfDeadlineDate');
        //$ticket->descricao = $request->input('txtMainDescription');
        $ticket->save();

        // Pega o ciclo atual
        $ciclo = $ticket->ciclo_atual();

        // Salva as solicitações
        foreach ((array) $request->input('cbAlterationType') as $k => $id_tipo) {
            $solicitacao = new ArtwSolicitacoes;
            $solicitacao->id_ciclo = $ciclo->id;
            $solicitacao->id_status = 1;
            $solicitacao->id_tipo = $id_tipo;
            $solicitacao->descricao = $request->input('txtDescriptionSpec.' . $k);
            $solicitacao->id_usuario = Auth::user()->id;
            $solicitacao->save();
        }

        $relativePath = '/images/uploads/artwork/ticket/';

        foreach ((array) $request->file('artworkFileUpload') as $k => $file) {
            $extension = $file->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $filename = $hash . "." . $extension;
            $destinationPath = public_path() . $relativePath;
            $file->move($destinationPath, $filename);

            $arquivo = new ArtwArquivos();
            $arquivo->id_ticket = $ticket->id;
            $arquivo->path = $relativePath . $filename;
            $arquivo->descricao = $request->input('tfArtworkFileLegend.' . $k);
            $arquivo->save();
        }

        DB::commit();

        return redirect()->route('site.artwork.ticket.members.get', ['ticket' => $ticket->id]);
    }

    public function ticketPrint($ticket_id, Common $common)
    {
        $ticket = ArtwTickets::find($ticket_id);

        if (!$ticket) {
            abort(404);
        }

        $participantes = $ticket->participantes();

        $idsPerfis = [];

        foreach ($ticket->artwPerfisUsuariosTickets as $participante) {
            $idsPerfis[] = $participante->id_perfil;
        }

        //if (($ticket->id_status == 0 || $ticket->id_status == 9) && in_array(1, $idsPerfis) && in_array(3, $idsPerfis) && in_array(5, $idsPerfis)) {
        if (($ticket->id_status == 0 || $ticket->id_status == 9) && in_array(5, $idsPerfis)) {
            $common->changeStatusTicket($ticket, 1);

            if ($ticket->artwItem->status == 0) {
                $item = $ticket->artwItem;
                $item->status = 1;
                $item->save();
            }
        } else {
            //$common->changeStatusTicket($ticket, 0);
        }

        $variacao = $ticket->artwVariacao;
        $sku = $ticket->sku;

        $produto = $sku->produtos;

        if ($produto->replicar_logo_marca)
        {

            $logo = (isset($produto->logotipos)) ? $produto->familias->logotipos->first() : null;
            $cor_hexa = ($produto->cor_hexa) ? $produto->cor_hexa : $produto->familias->cor_hexa;

        } else {

            $logo = (isset($produto->logotipos)) ? $produto->logotipos->where('id_tipo_logotipo', 2)->first() : null;
            $cor_hexa = $produto->cor_hexa;

        }

        $thumb = ($logo) ? $logo->thumb : 'images/layout/ticket-no-image-rubrum.png';

        return view('site.artwork.ticket_impressao', compact('ticket', 'thumb', 'sku', 'participantes', 'variacao', 'cor_hexa'));
    }

    public function ticketBaixarArquivos($ticket_id)
    {
        $ticket = ArtwTickets::where('numero', $ticket_id)->get()->last();

        if (!$ticket) {
            abort(404);
        }

        $zipper = new Zipper();

        $zipFile = 'files/ticket/arquivos/arquivos_' . $ticket->numero . '.zip';

        $zipper->make($zipFile);

        $arquivos = [];

        foreach ($ticket->artwArquivos as $arquivo) {

            if (is_file(public_path($arquivo->path))) {

                //copy(public_path($arquivo->path), public_path('files/ticket/arquivos/' . $arquivo->descricao));
                
                //$arquivos[] = public_path('files/ticket/arquivos/' . $arquivo->descricao);

                $zipper->add(public_path($arquivo->path), $arquivo->descricao);
            }
        }

        $zipper->close();

        //check_download($zipFile);

        return response()->download($zipFile)->deleteFileAfterSend(true);
    }

    //public function ticketEditV2()
    //{
    //    return view('site.artwork.ticket_edicao_v2');
    //}

    public function ticketEditStep1($ticket_id, Common $common, ArtwPerfisUsuariosEquipeTickets $artwPerfisUsuariosEquipeTickets)
    {
        $ticketById = ArtwTickets::find($ticket_id);

        if ($ticketById) {

            return redirect()->route('site.artwork.ticket.edit.step1.get', ['ticket' => $ticketById->numero]);
        }

        $ticket = ArtwTickets::where('numero', $ticket_id)->get()->last();

        if (!$ticket) {
            abort(404);
        }

        //dd($ticket->sku_nome_completo);
        
        if ($ticket->id_status == 6 && !$ticket->cancelado_em) {
            $versao = ArtwVersoes::where('id_ticket', $ticket->id)->get()->first();

            if ($versao) {

                return redirect()->route('site.artwork.history', [$versao->id_variacao, $versao->id]);
            } else {

                abort(404);
            }
        }

        //dd($ticket->checkUsuario(Auth::user()->id), $artwPerfisUsuariosEquipeTickets->checkUsuario(Auth::user()->id, $ticket->id), Auth::user()->id_adm_perfil, Auth::user()->gerente);

        if (!$ticket->checkUsuario(Auth::user()->id) && !$artwPerfisUsuariosEquipeTickets->checkUsuario(Auth::user()->id, $ticket->id) && !Auth::user()->admin && !Auth::user()->gerente) {
            //return redirect()->route('site.artwork.category');
            return redirect()->route('site.artwork.managerV4', ['minhas_tarefas']);
        }

        $idsPerfis = [];

        foreach ($ticket->artwPerfisUsuariosTickets as $participante) {
            
            if (!$participante->admUsuario->deleted_at)
            {
                $idsPerfis[] = $participante->id_perfil;
            }
            
        }

        if (($ticket->id_status == 0 || $ticket->id_status == 9) && in_array(5, $idsPerfis)) {

            $common->changeStatusTicket($ticket, 1);

            if ($ticket->artwItem->status == 0) {
                $item = $ticket->artwItem;
                $item->status = 1;
                $item->save();
            }
        } else {
            //$common->changeStatusTicket($ticket, 0);
        }

        if ($ticket->id_status == 3 || $ticket->id_status == 4) {

            $id_perfil = ($ticket->id_status == 3) ? 2 : 1;
            $proximo_status = ($ticket->id_status == 3) ? 4 : 5;
            
            $check = ($ticket->id_status == 3) ? in_array(2, $idsPerfis) : in_array(1, $idsPerfis);

            $pendentes = $ticket->participantesPendentes($id_perfil, $ticket->ciclo_atual()->numero);

            if ($pendentes->count() == 0 && $check) {

                $reprovados = $ticket->participantesReprovados($id_perfil, $ticket->ciclo_atual()->numero);

                if ($reprovados->count() == 0) {

                    $common->changeStatusTicket($ticket, $proximo_status);
                } else {

                    $perfilUsuarioEquipeTicket = $artwPerfisUsuariosEquipeTickets->artwPerfilUsuarioEquipeTicket(Auth::user()->id, $id_perfil, $ticket->id);

                    if ($perfilUsuarioEquipeTicket) {
                        $perfilUsuarioTicket = $perfilUsuarioEquipeTicket->artwPerfisUsuariosTicket;
                    } else {
                        $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);
                    }

                    //dd($perfilUsuarioTicket);

                    $common->reprovaTicket($ticket, $id_perfil, $perfilUsuarioTicket, $perfilUsuarioEquipeTicket, 'ticketEditStep1');
                }
            }
        }
        
        $libera_edicao['fornecedor_pdf'] = 0;
        
        if ($ticket->id_status == 11) {
            
            $id_perfil = 7;

            $perfilUsuarioEquipeTicket = $artwPerfisUsuariosEquipeTickets->artwPerfilUsuarioEquipeTicket(Auth::user()->id, $id_perfil, $ticket->id);

            if ($perfilUsuarioEquipeTicket) {
                $perfilUsuarioTicket = $perfilUsuarioEquipeTicket->artwPerfisUsuariosTicket;
            } else {
                $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);
            }

            if ($perfilUsuarioTicket && ($perfilUsuarioTicket->aprovado === 0 || ($perfilUsuarioTicket->aprovado == 2 && $perfilUsuarioTicket->ciclo != $ticket->ciclo_atual()->numero))) {
                
                $libera_edicao['fornecedor_pdf'] = 1;
                
            }
        }

        $sku = $ticket->sku;
        $item = $ticket->artwItem;

        $produto = $sku->produtos;

        if ($produto->replicar_logo_marca)
        {

            $logo = (isset($produto->logotipos)) ? $produto->familias->logotipos->first() : null;
            $cor_hexa = ($produto->cor_hexa) ? $produto->cor_hexa : $produto->familias->cor_hexa;

        } else {

            $logo = (isset($produto->logotipos)) ? $produto->logotipos->where('id_tipo_logotipo', 2)->first() : null;
            $cor_hexa = $produto->cor_hexa;

        }

        $thumb = ($logo) ? $logo->thumb : 'images/layout/ticket-no-image-rubrum.png';

        //return view('site.artwork.ticket_edicao', compact('ticket', 'thumb', 'sku'));
        return view('site.artwork.ticket_edicao_v2', compact('ticket', 'thumb', 'sku', 'item', 'libera_edicao', 'cor_hexa'));
    }

    //public function ticketEditStep2()
    //{
    //    return view('site.artwork.ticket_revisao');
    //}
    //public function ticketEditStep3()
    //{
    //    return view('site.artwork.ticket_marketing');
    //}
    //public function ticketEditStep4()
    //{
    //    return view('site.artwork.ticket_aprovacao');
    //}
    //public function ticketEditStep5()
    //{
    //    return view('site.artwork.ticket_aprovado');
    //}
    //public function ticketEditStep6()
    //{
    //    return view('site.artwork.ticket_enviado');
    //}

    public function getTicketEditDeny($ticket_id, Common $common, ArtwPerfisUsuariosEquipeTickets $artwPerfisUsuariosEquipeTickets)
    {
        $ticket = ArtwTickets::find($ticket_id);

        if (!$ticket) {
            abort(404);
        }
        
        if ($ticket->id_status == 3) {
            $id_perfil = 2;
        } elseif ($ticket->id_status == 4) {
            $id_perfil = 1;
        } else {
            $id_perfil = 0;
        }

        $perfilUsuarioEquipeTicket = $artwPerfisUsuariosEquipeTickets->artwPerfilUsuarioEquipeTicket(Auth::user()->id, $id_perfil, $ticket->id);

        if ($perfilUsuarioEquipeTicket) {
            $perfilUsuarioTicket = $perfilUsuarioEquipeTicket->artwPerfisUsuariosTicket;
        } else {
            $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);
        }
        
        if (!$perfilUsuarioTicket)
        {
            return redirect()->route('site.artwork.ticket.edit.step1.get', ['ticket' => $ticket->id]);
        }
        
        //if ( $perfilUsuarioTicket->aprovado || ($perfilUsuarioTicket->em_reprovacao && $perfilUsuarioTicket->em_reprovacao_by != Auth::user()->id) )
        if ( ($perfilUsuarioTicket->aprovado == 1 ) || ($perfilUsuarioTicket->aprovado == 2 && $perfilUsuarioTicket->ciclo >= $ticket->ciclo_atual()->numero) || ($perfilUsuarioTicket->em_reprovacao && $perfilUsuarioTicket->em_reprovacao_by != Auth::user()->id) )
        {
            return redirect()->route('site.artwork.ticket.edit.step1.get', ['ticket' => $ticket->id]);
        }

        if ($ticket->id_status == 0) {
            $common->changeStatusTicket($ticket, 1);
        }

        $count_marketing = $ticket->artwPerfisUsuariosTickets()->where('id_perfil', 2)->get()->count();

        $solicitacao_tipos = ArtwSolicitacaoTipos::lists('nome', 'id')->toArray();

        $sku = $ticket->sku;
        $item = $ticket->artwItem;

        $produto = $sku->produtos;

        if ($produto->replicar_logo_marca)
        {

            $logo = (isset($produto->logotipos)) ? $produto->familias->logotipos->first() : null;
            $cor_hexa = ($produto->cor_hexa) ? $produto->cor_hexa : $produto->familias->cor_hexa;

        } else {

            $logo = (isset($produto->logotipos)) ? $produto->logotipos->where('id_tipo_logotipo', 2)->first() : null;
            $cor_hexa = $produto->cor_hexa;

        }

        $thumb = ($logo) ? $logo->thumb : 'images/layout/ticket-no-image-rubrum.png';

        return view('site.artwork.ticket_reprovado', compact('ticket', 'thumb', 'sku', 'solicitacao_tipos', 'count_marketing', 'item', 'cor_hexa'));
    }

    public function postTicketEditDeny($ticket_id, Artwork\PostTicketEditDenyForm $request, ArtwPerfisUsuariosEquipeTickets $artwPerfisUsuariosEquipeTickets)
    {
        //dd($request->file('artworkFileUpload'));

        $ticket = ArtwTickets::find($ticket_id);

        if (!$ticket) {
            abort(404);
        }

        $common = new Common;

        $libera_status = 0;

        $ciclo_antigo = $ticket->ciclo_atual();

        $ciclo_novo = ArtwCiclos::firstOrNew(['numero' => $ciclo_antigo->numero + 1, 'id_ticket' => $ticket->id]);

        if (!$ciclo_novo->exists) {
            $ciclo_novo->id_status = 0;
            $ciclo_novo->save();
        }

        if ($ticket->id_status == 3) {
            $id_perfil = 2;
        } elseif ($ticket->id_status == 4) {
            $id_perfil = 1;
        }

        $perfilUsuarioEquipeTicket = $artwPerfisUsuariosEquipeTickets->artwPerfilUsuarioEquipeTicket(Auth::user()->id, $id_perfil, $ticket->id);

        if ($perfilUsuarioEquipeTicket) {
            $perfilUsuarioTicket = $perfilUsuarioEquipeTicket->artwPerfisUsuariosTicket;
        } else {
            $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);
        }

        if (!$perfilUsuarioTicket) {
            return back()->withErrors('Perfil incorreto.');
        }

        DB::beginTransaction();

        $pendentes = $ticket->participantesPendentes($id_perfil, $ticket->ciclo_atual()->numero);

        if ($pendentes->count() == 1) {
            $libera_status = 1;
        }

        if ($perfilUsuarioEquipeTicket) {
            $perfilUsuarioTicket->id_membro_aprovador = $perfilUsuarioEquipeTicket->id;
        } else {
            $perfilUsuarioTicket->id_membro_aprovador = null;
        }
        
        $perfilUsuarioTicket->ciclo_aprovacao = $ciclo_antigo->numero;
        
        $perfilUsuarioTicket->em_reprovacao = 0;
        $perfilUsuarioTicket->em_reprovacao_by = null;
        $perfilUsuarioTicket->aprovado = 2;
        $perfilUsuarioTicket->ciclo = $ciclo_antigo->numero;
        $perfilUsuarioTicket->save();

        $artwAprovacao = ArtwAprovacoes::firstOrNew([
            'id_ticket' => $ticket->id,
            'id_usuario' => Auth::user()->id,
            'ciclo' => $ticket->ciclo_atual()->numero,
            'aprovado' => 2,
            'id_perfil' => $id_perfil
        ]);

        $artwAprovacao->save();

        if ($libera_status) {
            $arrayPerfis = ($id_perfil == 1) ? [2, 3, 5] : [3, 5];

            //$outrosPerfis = $ticket->artwPerfisUsuariosTickets->whereIn('id_perfil', $arrayPerfis);
            $outrosPerfis = \App\ArtwPerfisUsuariosTickets::withTrashed()->where('id_ticket', $ticket->id)->whereIn('id_perfil', $arrayPerfis)->get();
            
            foreach ($outrosPerfis as $perfil) {
                $perfil->aprovado = 0;
                $perfil->id_membro_aprovador = null;
                $perfil->save();
            }

            // Volta o status com status = 1 / Em Edição
            //$ticket->id_status = 1;
            //$ticket->save();
            // Adiciona um registro no histórico de status de tickets
            $ticketsStatus = new ArtwTicketsStatus();
            $ticketsStatus->id_ticket = $ticket->id;
            $ticketsStatus->id_ciclo = $ciclo_novo->id;
            $ticketsStatus->id_usuario = Auth::user()->id;
            $ticketsStatus->id_status = 1;
            $ticketsStatus->save();

            $ciclo_antigo->id_status = 3;
            $ciclo_antigo->save();

            $ciclo_novo->id_status = 1;
            $ciclo_novo->save();

            $ticket->ciclo = $ciclo_novo->numero;
            $ticket->save();
            
            $common->changeStatusTicket($ticket, 1);
        }

        // Salva as solicitações
        foreach ($request->input('cbAlterationType') as $k => $id_tipo) {
            $solicitacao = new ArtwSolicitacoes;
            $solicitacao->id_ciclo = $ciclo_novo->id;
            $solicitacao->id_status = 1;
            $solicitacao->id_tipo = $id_tipo;
            $solicitacao->tipo = 2;
            $solicitacao->descricao = $request->input('txtDescriptionSpec.' . $k);
            $solicitacao->id_usuario = Auth::user()->id;
            $solicitacao->save();
        }

        $relativePath = '/images/uploads/artwork/ticket/';

        foreach ($request->file('artworkFileUpload') as $k => $file) {
            if ($file) {
                $filename = $file->getClientOriginalName();
                $filename = $common->verificaNomeArquivo($relativePath, $filename);

                $destinationPath = public_path() . $relativePath;
                $file->move($destinationPath, $filename);

                $arquivo = new ArtwArquivos();
                $arquivo->id_ticket = $ticket->id;
                $arquivo->id_usuario = Auth::user()->id;
                $arquivo->path = $relativePath . $filename;
                $arquivo->descricao = $filename;
                $arquivo->nome_original = $filename;
                $arquivo->save();

                sleep(2);
            }
        }

        // Atualiza a timeline
        $common->ticketAtualizaTimeline($ticket);

        DB::commit();

        return redirect()->route('site.artwork.ticket.edit.step1.get', ['ticket' => $ticket->id]);
    }

    public function getTicketMembers($ticket_id)
    {
        $ticket = ArtwTickets::find($ticket_id);

        if (!$ticket) {
            abort(404);
        }

        $variacao = $ticket->artwVariacao;
        $sku = $ticket->sku;
        $item = $ticket->artwItem;

        $produto = $sku->produtos;

        if ($produto->replicar_logo_marca)
        {

            $logo = (isset($produto->logotipos)) ? $produto->familias->logotipos->first() : null;
            $cor_hexa = ($produto->cor_hexa) ? $produto->cor_hexa : $produto->familias->cor_hexa;

        } else {

            $logo = (isset($produto->logotipos)) ? $produto->logotipos->where('id_tipo_logotipo', 2)->first() : null;
            $cor_hexa = $produto->cor_hexa;

        }

        $thumb = ($logo) ? $logo->thumb : 'images/layout/ticket-no-image-rubrum.png';

        return view('site.artwork.ticket_members', compact('ticket', 'thumb', 'sku', 'variacao', 'item', 'cor_hexa'));
    }

    public function getTicketMembersOld($ticket_id)
    {
        $ticket = ArtwTickets::find($ticket_id);

        if (!$ticket) {
            abort(404);
        }

        $sku = $ticket->sku;

        $produto = $sku->produtos;

        if ($produto->replicar_logo_marca)
        {

            $logo = (isset($produto->logotipos)) ? $produto->familias->logotipos->first() : null;
            $cor_hexa = ($produto->cor_hexa) ? $produto->cor_hexa : $produto->familias->cor_hexa;

        } else {

            $logo = (isset($produto->logotipos)) ? $produto->logotipos->where('id_tipo_logotipo', 2)->first() : null;
            $cor_hexa = $produto->cor_hexa;

        }

        $thumb = ($logo) ? $logo->thumb : 'images/layout/ticket-no-image-rubrum.png';

        return view('site.artwork.ticket_members', compact('ticket', 'thumb', 'sku', 'cor_hexa'));
    }

    public function postTicketMembers($ticket_id, Artwork\PostTicketRequestForm $request)
    {
        //dd($request->all());

        $ticket = ArtwTickets::find($ticket_id);

        if (!$ticket) {
            abort(404);
        }

        $perfis = $request->input('cbMembershipProfile');

        //dd($perfis);
        // Verifica se existem os três tipos de perfis necessários para prosseguir
        if (!$perfis) {
            return back()->withErrors('Perfis obrigatórios não foram selecionados: Mínimo de 1.');
        }

        //dd($request->input('cbMembershipName'), $request->input('cbMembershipAltName'), $perfis);

        foreach ($perfis as $k => $id_perfil) {
            $participante = ArtwPerfisUsuariosTickets::firstOrNew([
                    'id_ticket' => $ticket->id,
                    'id_usuario' => $request->input('cbMembershipName.' . $k),
                    'id_perfil' => $id_perfil
            ]);

            if ($request->input('cbMembershipAltName.' . $k) && $request->input('cbMembershipName.' . $k) != $request->input('cbMembershipAltName.' . $k)) {
                $participante->id_usuario = $request->input('cbMembershipAltName.' . $k);
            }

            //if (!$participante->exists)
            //{
            $participante->ciclo = 0;
            $participante->recebe_email = ($request->input('chkNotificationsSwitch.' . $k)) ? 1 : 0;
            $participante->save();
            //}

            $perfisComEquipe = [1,2];
            
            if (in_array($id_perfil, $perfisComEquipe))
            {
                $equipe = $request->input('cbMembershipTeamName.' . $k);

                if ($equipe)
                {
                    foreach ($equipe as $m => $membro)
                    {
                        $participanteEquipe = ArtwPerfisUsuariosEquipeTickets::firstOrNew([
                                'id_parent' => $participante->id,
                                'id_usuario' => $membro
                        ]);

                        $participanteEquipe->recebe_email = ($request->input('chkNotificationsSwitchMember.' . $k . '.' . $m)) ? 1 : 0;

                        $participanteEquipe->save();
                    }
                }
            }
            
            
            
        }

        //if ($request->input('edicao')){
        //return redirect()->route('site.artwork.ticket.edit.step1.get', ['ticket' => $ticket->id]);
        //} else {
        //return redirect()->route('site.artwork.ticket.print.get', ['ticket' => $ticket->id]);
        return redirect()->route('site.artwork.ticket.edit.step1.get', ['ticket' => $ticket->id]);
        //}
    }

    public function sendArtwork($id_variacao, $id_item)
    {
        $item = ArtwItens::find($id_item);

        if (!$item) {
            abort(404);
        }
        
        $variacao = ArtwVariacoes::find($id_variacao);
        
        if (!$variacao) {
            abort(404);
        }

        if (!in_array($variacao->sku->produtos->familias->id, Auth::user()->familias))
        {
            return redirect()->route('site.artwork.family');
        }
        
        $versao = $item->artwVersoes->last();

        $ticket = $versao->artwTicket;

        $sku = $variacao->sku;

        $produto = $sku->produtos;

        if ($produto->replicar_logo_marca)
        {

            $logo = (isset($produto->logotipos)) ? $produto->familias->logotipos->first() : null;
            $cor_hexa = ($produto->cor_hexa) ? $produto->cor_hexa : $produto->familias->cor_hexa;

        } else {

            $logo = (isset($produto->logotipos)) ? $produto->logotipos->where('id_tipo_logotipo', 2)->first() : null;
            $cor_hexa = $produto->cor_hexa;

        }

        $thumb = ($logo) ? $logo->thumb : 'images/layout/ticket-no-image-rubrum.png';

        return view('site.artwork.enviar_arte', compact('versao', 'variacao', 'ticket', 'sku', 'thumb', 'item', 'cor_hexa'));
    }
    
    public function fornecedorPdf($id_variacao, $id_item)
    {
        $item = ArtwItens::find($id_item);

        if (!$item) {
            abort(404);
        }
        
        $variacao = ArtwVariacoes::find($id_variacao);
        
        if (!$variacao) {
            abort(404);
        }

        if (!in_array($variacao->sku->produtos->familias->id, Auth::user()->familias))
        {
            return redirect()->route('site.artwork.family');
        }
        
        $versao = $item->artwVersoes->last();

        $ticket = $versao->artwTicket;

        $sku = $variacao->sku;

        $produto = $sku->produtos;

        if ($produto->replicar_logo_marca)
        {

            $logo = (isset($produto->logotipos)) ? $produto->familias->logotipos->first() : null;
            $cor_hexa = ($produto->cor_hexa) ? $produto->cor_hexa : $produto->familias->cor_hexa;

        } else {

            $logo = (isset($produto->logotipos)) ? $produto->logotipos->where('id_tipo_logotipo', 2)->first() : null;
            $cor_hexa = $produto->cor_hexa;

        }

        $thumb = ($logo) ? $logo->thumb : 'images/layout/ticket-no-image-rubrum.png';

        return view('site.artwork.fornecedor_pdf', compact('versao', 'variacao', 'ticket', 'sku', 'thumb', 'item', 'cor_hexa'));
    }

    public function history($id_variacao, $id_versao)
    {
        $versao = ArtwVersoes::find($id_versao);

        if (!$versao) {
            abort(404);
        }
        
        $variacao = ArtwVariacoes::find($id_variacao);
        
        if (!$variacao) {
            abort(404);
        }

        if (!in_array($variacao->sku->produtos->familias->id, Auth::user()->familias))
        {
            return redirect()->route('site.artwork.family');
        }

        $versaoAnterior = ArtwVersoes
            ::where('id_variacao', $versao->id_variacao)
            ->where('numero', $versao->numero - 1)
            ->get()
            ->last();

        $ticket = $versao->artwTicket;

        $ciclosAnteriores = ArtwCiclos
            ::where('id', '!=', $ticket->ciclo_atual()->id)
            ->where('id_ticket', $ticket->id)
            ->where('id_status', '>=', 1)
            ->orderBy('id', 'DESC')
            ->get();

        $sku = $variacao->sku;

        $produto = $sku->produtos;

        if ($produto->replicar_logo_marca)
        {

            $logo = (isset($produto->logotipos)) ? $produto->familias->logotipos->first() : null;
            $cor_hexa = ($produto->cor_hexa) ? $produto->cor_hexa : $produto->familias->cor_hexa;

        } else {

            $logo = (isset($produto->logotipos)) ? $produto->logotipos->where('id_tipo_logotipo', 2)->first() : null;
            $cor_hexa = $produto->cor_hexa;

        }

        $thumb = ($logo) ? $logo->thumb : 'images/layout/ticket-no-image-rubrum.png';

        $participantes = $ticket->participantes();

        $ciclos = $ticket->artwCiclos()->orderBy('numero', 'DESC')->get();

        $item = $ticket->artwItem;

        $todasVersoes = ArtwVersoes::where('id_item', $item->id)->orderBy('numero', 'DESC')->get();

        //dd($todasVersoes);

        return view('site.artwork.historico', compact('item', 'versao', 'todasVersoes', 'versaoAnterior', 'ciclosAnteriores', 'ticket', 'sku', 'thumb', 'variacao', 'participantes', 'ciclos', 'cor_hexa'));
    }

    public function versions($id_item)
    {
        $item = ArtwItens::find($id_item);

        $variacao = $item->artwVariacao;

        $sku = $variacao->sku;

        if (!$sku || !$variacao) {
            abort(404);
        }

        $versao = $item->artwVersoes->last();

        $ticket = $versao->artwTicket;

        $produto = $sku->produtos;

        if ($produto->replicar_logo_marca)
        {

            $logo = (isset($produto->logotipos)) ? $produto->familias->logotipos->first() : null;
            $cor_hexa = ($produto->cor_hexa) ? $produto->cor_hexa : $produto->familias->cor_hexa;

        } else {

            $logo = (isset($produto->logotipos)) ? $produto->logotipos->where('id_tipo_logotipo', 2)->first() : null;
            $cor_hexa = $produto->cor_hexa;

        }

        $thumb = ($logo) ? $logo->thumb : 'images/layout/ticket-no-image-rubrum.png';

        $versoes = ArtwVersoes::where('id_item', $item->id)->orderBy('numero', 'DESC')->get();

        return view('site.artwork.versoes_anteriores', compact('variacao', 'sku', 'thumb', 'versao', 'versoes', 'ticket', 'item', 'cor_hexa'));
    }

    public function getCicloArquivo($id_arquivo, $completo = false)
    {
        $cicloLayout = \App\ArtwCiclosLayout::find($id_arquivo);

        if ($completo) {
            $file = $cicloLayout->path;
        } else {
            $fileArray = explode('/', $cicloLayout->path);
            $file = end($fileArray);
        }

        return $file;
    }

    public function getVersaoArquivo($id_arquivo, $completo = false)
    {
        $versao = \App\ArtwVersoes::find($id_arquivo);

        if ($completo) {
            $file = $versao->path;
        } else {
            $fileArray = explode('/', $versao->path);
            $file = end($fileArray);
        }

        return $file;
    }

    public function baixarPdf($id_arquivo, $tipo)
    {
        switch ($tipo) {
            case 'ciclo':
                $file = public_path($this->getCicloArquivo($id_arquivo, true));
                break;
            case 'versao':
                $file = public_path($this->getVersaoArquivo($id_arquivo, true));
                break;
            default:
                break;
        }

        if (file_exists($file)) {
            return response()->download($file);
        } else {
            echo 'Arquivo: ' . $file . ' não encontrado.';
        }
    }
    
    public function abrirArquivo($id_arquivo, $filename)
    {
        $arquivo = ArtwArquivos::find($id_arquivo);
        
        $file = public_path($arquivo->path);
       
        if (file_exists($file)) {
            
            return response()->file($file);
            
        } else {
            echo 'Arquivo: ' . $file . ' não encontrado.';
        }
    }
    
    public function abrirPdf($id_arquivo, $tipo)
    {
        switch ($tipo) {
            case 'ciclo':
                $file = public_path($this->getCicloArquivo($id_arquivo, true));
                break;
            case 'versao':
                $file = public_path($this->getVersaoArquivo($id_arquivo, true));

                $versao = ArtwVersoes::find($id_arquivo);

                if ($versao->artwItem && $versao->artwItem->artwVariacao && !in_array($versao->artwItem->artwVariacao->sku->produtos->familias->id, Auth::user()->familias))
                {
                    return redirect()->route('site.artwork.family');
                }

                break;
            default:
                break;
        }

        if (file_exists($file)) {
            
            $headers = ['Cache-Control' => 'no-cache, must-revalidate'];
            
            return response()->file($file, $headers);
            
        } else {
            echo 'Arquivo: ' . $file . ' não encontrado.';
        }
    }

    public function baixarArteFinalOLD($id_arquivo)
    {
        $arteFinal = \App\ArtwArtes::find($id_arquivo);

        if ($arteFinal) {
            $file = public_path($arteFinal->path);
        } else {
            abort(404);
        }

        if (file_exists($file)) {
            return response()->download($file);
        } else {
            echo 'Arquivo: ' . $file . ' não encontrado.';
        }
    }

    public function baixarArteFinal($id_arquivo)
    {
        $arteFinal = \App\ArtwArtes::find($id_arquivo);

        if ($arteFinal && $arteFinal->path)
        {
            $file = public_path($arteFinal->path);
            
            if (file_exists($file)) {
                return response()->download($file);
            } else {
                echo 'Arquivo: ' . $file . ' não encontrado.';
            }
        }
        elseif ($arteFinal && !$arteFinal->path)
        {
            $file = Storage::disk(config('app.storage'))->get(config('app.costummer_name') . '/artwork/artes-finais/' . $arteFinal->nome_arquivo);

            $mimeType = Storage::disk(config('app.storage'))->mimeType(config('app.costummer_name') . '/artwork/artes-finais/' . $arteFinal->nome_arquivo);

            $header = [
                'Content-Type' => $mimeType,
                'Content-Disposition' => 'inline; filename="'.$arteFinal->nome_arquivo.'"',
                'Content-Disposition' => 'attachment; filename="'.$arteFinal->nome_arquivo.'"',
            ];

            return response()->make($file, 200, $header);
        }
        else
        {
            abort(404);
        }
    }
    
    public function baixarFornecedorPdf($id_arquivo)
    {
        $fornecedorPdf = \App\ArtwFornecedorPdfs::find($id_arquivo);

        if ($fornecedorPdf) {
            $file = public_path($fornecedorPdf->path);
        } else {
            abort(404);
        }

        if (file_exists($file)) {
            return response()->download($file);
        } else {
            echo 'Arquivo: ' . $file . ' não encontrado.';
        }
    }
    
    public function abrirFornecedorPdf($id_arquivo)
    {
        $fornecedorPdf = \App\ArtwFornecedorPdfs::find($id_arquivo);

        $ticket = $fornecedorPdf->artwTicket;

        if ($ticket->id_status == 6 && $ticket->artwItem && $ticket->artwItem->artwVariacao && !in_array($ticket->artwItem->artwVariacao->sku->produtos->familias->id, Auth::user()->familias))
        {
            return redirect()->route('site.artwork.family');
        }

        if ($fornecedorPdf) {
            $file = public_path($fornecedorPdf->path);
        } else {
            abort(404);
        }

        if (file_exists($file)) {
            return response()->file($file);
        } else {
            echo 'Arquivo: ' . $file . ' não encontrado.';
        }
    }

    public function downloadArteFinal(Request $request)
    {
        $token = $request->input('token');

        $arteEnvioEmail = ArtwArtesEnviosEmails::where('token', $token)->get()->first();

        $arteEnvio = $arteEnvioEmail->artwArteEnvio;

        if ($arteEnvio->cancelado_em || $arteEnvio->status == 0 || $arteEnvio->dt_expiracao->format('Y-m-d H:i:s') < date('Y-m-d H:i:s'))
        {
            $liberaDownload = 0;
        } else {
            $liberaDownload = 1;
        }

        $ticket = $arteEnvioEmail->artwArteEnvio->artwArte->artwTicket;

        $item = $ticket->artwitem;

        $logos = $ticket->artwItem->artwVariacao->sku->produtos->logotipos;

        $logo = '';

        foreach ($logos as $log) {
            if ($log->id_tipo_logotipo == 2) {
                $logo = $log->thumb;
            }
        }

        $thumb = ($logo) ? $logo : 'images/layout/ticket-no-image-rubrum.png';

        return view('site.artwork.baixar_arte_final', compact('ticket', 'item', 'arteEnvio', 'arteEnvioEmail', 'thumb', 'token', 'liberaDownload'));
    }

    public function downloadPdf(Request $request)
    {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $token = $request->input('token');

        $arteEnvioEmail = ArtwArtesEnviosEmails::where('token', $token)->get()->first();

        $layout = $arteEnvioEmail->artwArteEnvio->artwArte->artwTicket->ciclo_atual()->artwCiclosLayout->last();

        if ($layout && file_exists(public_path($layout->path))) {

            $arteEnvioEmail->downloads_pdf = $arteEnvioEmail->downloads_pdf + 1;
            $arteEnvioEmail->save();

            return response()->download(public_path($layout->path));
        } else {

            $titulo = 'Erro';
            $texto = 'Ocorreu um erro inesperado!';
            return view('errors.erro', compact('titulo', 'texto'));
        }
    }

    public function downloadArteOLD(Request $request)
    {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $token = $request->input('token');

        $arteEnvioEmail = ArtwArtesEnviosEmails::where('token', $token)->get()->first();

        $arte = $arteEnvioEmail->artwArteEnvio->artwArte;

        if ($arte && file_exists(public_path($arte->path))) {

            $arteEnvioEmail->downloads_arte = $arteEnvioEmail->downloads_arte + 1;
            $arteEnvioEmail->save();

            return response()->download(public_path($arte->path));
        } else {

            $titulo = 'Erro';
            $texto = 'Ocorreu um erro inesperado!';
            return view('errors.erro', compact('titulo', 'texto'));
        }
    }

    public function downloadArte(Request $request)
    {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $token = $request->input('token');

        $arteEnvioEmail = ArtwArtesEnviosEmails::where('token', $token)->get()->first();

        $arte = $arteEnvioEmail->artwArteEnvio->artwArte;

        if ($arte && $arte->path && file_exists(public_path($arte->path))) {

            $arteEnvioEmail->downloads_arte = $arteEnvioEmail->downloads_arte + 1;
            $arteEnvioEmail->save();

            return response()->download(public_path($arte->path));

        } elseif ($arte && !$arte->path) {

            $arteEnvioEmail->downloads_arte = $arteEnvioEmail->downloads_arte + 1;
            $arteEnvioEmail->save();

            $file = Storage::disk(config('app.storage'))->get(config('app.costummer_name') . '/artwork/artes-finais/' . $arte->nome_arquivo);

            $mimeType = Storage::disk(config('app.storage'))->mimeType(config('app.costummer_name') . '/artwork/artes-finais/' . $arte->nome_arquivo);

            $header = [
                'Content-Type' => $mimeType,
                'Content-Disposition' => 'inline; filename="'.$arte->nome_arquivo.'"',
                'Content-Disposition' => 'attachment; filename="'.$arte->nome_arquivo.'"',
            ];

            return response()->make($file, 200, $header);

        } else {

            $titulo = 'Erro';
            $texto = 'Ocorreu um erro inesperado!';
            return view('errors.erro', compact('titulo', 'texto'));
        }
    }
}
