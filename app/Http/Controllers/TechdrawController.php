<?php
namespace App\Http\Controllers;

// Models
use App\CategoriasProdutos;
use App\TdTechdraws;
use App\TdVariacoes;
use App\TdVersoes;
use App\TdItens;
// Outros
use Illuminate\Support\Facades\Auth;
use App\Components\Common;
use Illuminate\Http\Request;
use App\Http\Requests;

class TechdrawController extends Controller
{

    public function __construct()
    {
        //
    }

    public function abrirPdf($id_arquivo)
    {
        $arquivo = TdVersoes::find($id_arquivo);

        if ($arquivo) {
            $file = public_path($arquivo->path);
        } else {
            abort(404);
        }

        if (file_exists($file)) {
            return response()->file($file);
        } else {
            echo 'Arquivo: ' . $file . ' não encontrado.';
        }
    }
    
    public function embalagens(Common $common, TdVersoes $versoes)
    {
        $skus = $versoes
            ->select('td_versoes.*', 'dimensoes.nome AS embalagem_nome', 'tipos_embalagens.nome AS tipo')
            ->join('skus', 'td_versoes.id_sku', '=', 'skus.id')
            ->join('dimensoes', 'dimensoes.id', '=', 'skus.id_dimensao')
            ->join('tipos_embalagens', 'tipos_embalagens.id', '=', 'dimensoes.id_tipo_embalagem')
            ->get();

        $thumb = 'images/layout/ticket-no-image.png';

        return view('site.techdraw.embalagens', compact('skus', 'thumb'));
    }

    public function technicalPlan()
    {
        $tipos = \App\TiposEmbalagens::select('tipos_embalagens.*')
            ->distinct()
            ->join('dimensoes', 'dimensoes.id_tipo_embalagem', '=', 'tipos_embalagens.id')
            ->join('td_techdraws', 'td_techdraws.id_dimensao', '=', 'dimensoes.id')
            ->join('td_variacoes', 'td_variacoes.id_techdraw', '=', 'td_techdraws.id')
            ->where('tipos_embalagens.status', 1)
            ->where('dimensoes.status', 1)
            ->where('td_techdraws.status', 1)
            ->where('td_variacoes.status', 1)
             ->orderBy('tipos_embalagens.nome')
             ->orderBy('dimensoes.nome')
            ->get();
        
        return view('site.techdraw.plantas_tecnicas', compact('tipos'));
    }

    public function package($id_tipo)
    {
        return view('site.techdraw.embalagem', compact('id_tipo'));
    }

    public function getCreateNewPlan()
    {
        return view('site.techdraw.nova_planta_tecnica');
    }

    public function postCreateNewPlan(Request $request)
    {
        $td = TdTechdraws::firstOrNew(['id_dimensao' => $request->input('cbPckContentSize')]);

        if (!$td->exists) {
            $td->save();
        }
        
        if ($td->variacoes->count() || $request->input('packageHasVariation') == 'Sim') {
            $variacao_nome = $request->input('variacaoName');
            $principal = 0;
        } else {
            $variacao_nome = 'Original';
            $principal = 1;
        }
        
        $variacao = TdVariacoes::firstOrNew(['id_techdraw' => $td->id, 'nome' => $variacao_nome, 'principal' => $principal]);
        
        if (!$variacao->exists) {
            $variacao->save();
        }
        
        return redirect()->route("site.techdraw.technicalPlan.package", [$td->dimensao->id_tipo_embalagem])->with('message', 'Planta Técnica cadastrada com sucesso.');
    }

    public function getCreateNewPlanEdit($id_variacao)
    {
        $variacao = TdVariacoes::find($id_variacao);
        
        return view('site.techdraw.editar_planta_tecnica', compact('variacao'));
    }
    
    public function postCreateNewPlanEdit(Request $request, $id_variacao)
    {
        $td = TdTechdraws::firstOrNew(['id_dimensao' => $request->input('cbPckContentSize')]);

        if (!$td->exists) {
            $td->save();
        }
        
        //if ($td->variacoes->count() || $request->input('packageHasVariation') == 'Sim') {
        if ($request->input('packageHasVariation') == 'Sim') {
            $variacao_nome = $request->input('variacaoName');
            $principal = 0;
        } else {
            $variacao_nome = 'Original';
            $principal = 1;
        }

        $checkVariacao = TdVariacoes::select('*')
            ->where('id', '!=', $id_variacao)
            ->where('id_techdraw', $td->id)
            ->where('nome', $variacao_nome)
            ->where('principal', $principal)
            ->get();

        if ($checkVariacao->count()) {

            //return back()->with('warning', 'O nome selecionado para esta embalagem já está sendo utilizado. É possível cadastrar essa embalagem como uma variação, ou então selecionar um nome diferente para ela.');
            return redirect()->route("site.techdraw.technicalPlan.package", [$td->dimensao->id_tipo_embalagem])->with('message', 'Já existe uma Planta Técnica com esse nome.');
        }
        
        $variacao = TdVariacoes::find($id_variacao);
        $variacao->id_techdraw = $td->id;
        $variacao->nome = $variacao_nome;
        $variacao->principal = $principal;
        $variacao->save();
        
        return redirect()->route("site.techdraw.technicalPlan.package", [$td->dimensao->id_tipo_embalagem])->with('message', 'Planta Técnica alterada com sucesso.');
    }
}
