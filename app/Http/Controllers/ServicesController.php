<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Skus;
use App\Produtos;
use App\AdmUsuarios;
use App\Familias;
use App\Components\Common;
use App\Components\SiteCommon;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;

class ServicesController extends Controller
{

    private $sku;

    public function __construct(Skus $sku)
    {
        $this->sku = $sku;
    }

    public function loadSkuModal($family, $product, $id)
    {
        $modalSku = \App\Skus::find($id);
        
        //$modalSku = $variacao->sku;
        $artesFinaisEdit = array();
        $users = array();
        $modPermitidos = array();
        $famPermitidas = array();
        $checkExpirados = false;
        $zip_pdf = false;

        if (!Auth::guest()) {
            $adm = new admUsuarios;
            $user = $adm->find(Auth::user()->id);

            if ($user->admin) {

                $familias = Familias::all();
                $modulos = \App\AdmModulos::all();
                
            } else {
                
                $perfis = [];
                
                foreach ($user->admPerfis as $perfil) {
                    
                    $perfis[] = $perfil->id;
                    
                }
                
                //dd($perfis);
                
                $familias = Familias::select('familias.*')
                    ->distinct()
                    ->join('adm_perfis_familias', 'adm_perfis_familias.id_familia', '=', 'familias.id')
                    ->whereIn('adm_perfis_familias.id_adm_perfil', $perfis)
                    ->get();
                
                $modulos = $user->admModulos;
            }

            foreach ($familias as $familia) {
                $famPermitidas[] = $familia->id;
            }

            foreach ($modulos as $modulo) {
                $modPermitidos[] = $modulo->id;
            }
            
            $user->familias = $famPermitidas;
            $user->modulos = $modPermitidos;
        }
        
        $artesFinais = \App\ArtesFinais
            ::select('artes_finais.*')
            ->where('id_sku', $modalSku->id)
            ->where('status', 1)
            ->get();
        
        if ($modalSku) {
            $modalSku->favorito = false;
            foreach ($artesFinais as $value) {

                if (!$value->dt_expiracao || ($value->dt_expiracao && $value->dt_expiracao >= date('Y-m-d H:i:s')))
                {
                    $value->defaultThumb = $modalSku->dimensoes->thumb;
                    $value->sku_nome = $modalSku->nome;
                    if (!is_null($value->link_pdf) || !is_null($value->link_zip))
                        $zip_pdf = true;
                    $artesFinaisEdit[] = $value;
                }
            }
            
            usort(
                $artesFinaisEdit, function( $a, $b ) {
                if ($a->dt_expiracao == $b->dt_expiracao)
                    return 0;
                return ( ( $a->dt_expiracao > $b->dt_expiracao ) ? -1 : 1 );
            }
            );

            $modalSku->zip_pdf = $zip_pdf;
            $modalSku->artesFinais = $artesFinaisEdit;
            //$modalSku->nome = $modalSku->promocional ? $modalSku->nome : $modalSku->produtos->nome . ' - ' . $modalSku->dimensoes->tiposEmbalagens->nome . ' - ' . $modalSku->dimensoes->nome;
            $modalSku->nome = $modalSku->sku_nome_completo;
        }

        return response(compact('modalSku', 'user', 'checkExpirados'));
    }

    public function checkFavorite($family, $product)
    {
        $user = null;
        $favorites = array();
        $favorite = false;
        $prod = Produtos::find($product);
        $prod = $prod->nome;
        if (!Auth::guest()) {
            $adm = new admUsuarios;
            $user = $adm->find(Auth::user()->id);
            foreach ($user->produtos as $produto) {
                $favorites[] = $produto->id;
            }
            if (!in_array($product, $favorites)) {
                $user->produtos()->attach($product);
                $favorite = true;
            } else {
                $user->produtos()->detach($product);
                $favorite = false;
            }
        }
        return response(compact('favorite', 'prod'));
    }

    public function editUser($id)
    {
        $editUser = AdmUsuarios::find($id);
        $editUser->modules = $editUser->admModulos;

        return response(compact('editUser'));
    }

    public function removeUser($id)
    {
        $removeUser = new Common;
        $removeUser->removeUser($id);
    }

    public function countNews(Request $request)
    {
        $siteCommon = new SiteCommon;
        $user = admUsuarios::find(Auth::user()->id);
        $logs = $siteCommon->getNews();
        $count = 0;
        if ($request->session()->get('news') !== 1) {
            foreach ($logs as $log) {
                if ($user->updated_at < $log['data_full']) {
                    $count++;
                }
            }
        }
        return $count;
    }

    public function viewedNews(Request $request)
    {
        $request->session()->put('news', 1);
    }

    public function newUserExterno(Request $request)
    {
        $dados = $request->all();
        $common = new Common;
        $adm = new admUsuarios;

        $dados['id_adm_empresa'] = '1';
        $dados['primeiro_acesso'] = '0';
        $dados['notificacao_favoritos'] = '0';

        $user = $adm->where('email', $dados['email'])->get()->first();

        if (count($user) > 0)
            $common->removeUser($user->id);

        if ($adm->create($dados))
            return array('status' => true, 'msg' => 'Usuário Criado com Sucesso.');

        return array('status' => false, 'msg' => 'Erro ao criar usuário.');
    }

    public function editUserExterno(Request $request)
    {
        $dados = $request->all();
        $user = admUsuarios::where('email', $dados['email'])->get()->first();

        if (count($user) > 0 && $user->update($dados))
            return array('status' => true, 'msg' => 'Usuário alterado com sucesso.');

        return array('status' => false, 'msg' => 'Erro ao alterar usuário.');
    }

    public function removeUserExterno(Request $request)
    {
        $common = new Common;
        $user = admUsuarios::where('email', $request->only('email'))->get()->first();

        $common->removeUser($user->id);

        return array('status' => true, 'msg' => 'Usuário removido com sucesso.');
    }
}
