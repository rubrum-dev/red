<?php

namespace App\Http\Controllers;

use App\Components\Dashboard;
use Illuminate\Http\Request;

use App\Http\Requests;
use Carbon\Carbon;
use App\RelMeses;
use App\RelTipos;
use Chumper\Zipper\Zipper;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class DashboardServiceController extends Controller
{
    public function __construct()
    {
        $now = Carbon::now();

        $ultimoRelatorio = RelMeses::where('id_usuario', Auth::user()->id)->orderBy('updated_at', 'DESC')->get()->first();

        if (!$ultimoRelatorio || $now->diffInMinutes($ultimoRelatorio->updated_at) >= 5)
        {
            Artisan::call('artwork:dashboard');
        }
    }

    public function atualizaDashboard()
    {
        $now = Carbon::now();

        $ultimoRelatorio = RelMeses::orderBy('updated_at', 'DESC')->get()->first();

        if ($now->diffInMinutes($ultimoRelatorio->updated_at) >= 1)
        {
            Artisan::call('artwork:dashboard');
        }
    }

    public function getDashboard($mes='', $ano='')
    {
        setlocale(LC_ALL, "pt_BR", "pt_BR.iso-8859-1", "pt_BR.utf-8", "portuguese");

        if (!$mes || !$ano)
        {
            $mes = date('m');
            $ano = date('Y');
        }

        $tipos = RelTipos::where('status', 1)->get();

        $retorno = [];

        foreach ($tipos as $t=>$tipo)
        {
            $relMes = RelMeses
                ::where('mes', $mes)
                ->where('ano', $ano)
                ->where('id_tipo', $tipo->id)
                ->where(function ($query) {
                    $query->OrwhereNull('id_usuario');
                    $query->Orwhere('id_usuario', Auth::user()->id);
                })
                ->get()
                ->first();

            if ($relMes)
            {
                $dataMes = Carbon::createFromFormat('m-Y',  $relMes->mes.'-'.$relMes->ano);

                $retorno['data_atualizacao'] = $relMes->updated_at->format('H\hi \d\e d/m/Y');

                $retorno['dados'][$tipo->slug]['tipo'] = $tipo->slug;
                $retorno['dados'][$tipo->slug]['mes'] = ucfirst(substr($dataMes->localeMonth, 0, 3));
                $retorno['dados'][$tipo->slug]['ano'] = $relMes->ano;
                $retorno['dados'][$tipo->slug]['numeros'] = $relMes->numeros;
            }
        }

        return $retorno;
    }

    public function getRelatorio(Request $request, Dashboard $dashboard, Excel $excel)
    {
        // Relatóro Resumo
        $relatorio = $dashboard->getRelatorio($request);

        if (!count($relatorio))
        {
            return response()->json(['error' => 404, 'message' => 'Os dados informados não geraram nenhum relatório.'], 200);
        }

        $nome_arquivo = ucfirst($request->input('tipo')) . '_' . date('YmdHis');

        $colunas = ['Nome', 'Sobrenome', 'Empresa', 'Departamento', 'Função no Ticket', 'Quantidade de Tickets'];

        Excel::create($nome_arquivo, function($excel) use($relatorio, $colunas) {

            $excel->sheet('Excel sheet', function($sheet) use($relatorio, $colunas) {
        
                $sheet->row(1, $colunas);

                foreach ($relatorio as $i => $row) {
                    $row = (array) $row;
                    
                    $sheet->row($i+2, $row);
                }
        
            });
        
        })->store('xls', 'images/relatorios');

        // Relatóro Detalhado
        $relatorioDetalhado = $dashboard->getRelatorio($request, 1);

        $colunas = ['Nome', 'Sobrenome', 'Empresa', 'Departamento', 'Função no Ticket', 'Número do Ticket'];

        Excel::create($nome_arquivo . '_detalhado', function($excel) use($relatorioDetalhado, $colunas) {

            $excel->sheet('Excel sheet', function($sheet) use($relatorioDetalhado, $colunas) {
        
                $sheet->row(1, $colunas);

                foreach ($relatorioDetalhado as $i => $row) {
                    $row = (array) $row;
                    
                    $sheet->row($i+2, $row);
                }
        
            });
        
        })->store('xls', 'images/relatorios');

        $zipper = new Zipper();

        $zipFile = 'images/relatorios/' . $nome_arquivo . '.zip';

        $zipper->make($zipFile);

        $zipper->add('images/relatorios/' . $nome_arquivo . '.xls');
        $zipper->add('images/relatorios/' . $nome_arquivo . '_detalhado.xls');
        $zipper->close();

        unlink('images/relatorios/' . $nome_arquivo . '.xls');
        unlink('images/relatorios/' . $nome_arquivo . '_detalhado.xls');


        if (Auth::user()->admin)
        {
            $urlDownload = route('site.dashboard.services.downloadRelatorio', $nome_arquivo . '.zip');
        }
        else
        {
            $urlDownload = route('site.dashboard.services.downloadRelatorio', $nome_arquivo . '.zip');
            //$urlDownload = route('site.dashboard.services.downloadRelatorioAdmin', $nome_arquivo);
        }

        return response()->json(['success' => 200, 'message' => 'Relatório gerado com sucesso.', 'urlDownload' => $urlDownload], 200);
    }

    public function downloadRelatorio($relatorio)
    {
        return redirect('images/relatorios/' . $relatorio);
    }

    public function downloadRelatorioAdmin($relatorio)
    {
        return view('site.artwork.download_relatorio_admin', compact('relatorio'));
    }
}
