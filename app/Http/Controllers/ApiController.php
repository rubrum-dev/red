<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\ArtwPerfisUsuariosEquipeTickets;
use App\ArtwTickets;

class ApiController extends Controller
{

    private $base_api = 'https://api.rubrum.com.br';
    private $session;
    private $counter = 0;
    private $arquivo;

    public function __construct()
    {
        $this->base_api = config('app.cloudflow_base');

        $this->client = new Client([
            'base_uri' => $this->base_api . '/portal.cgi',
            'headers' => [
                'Content-Type' => 'application/json'
            ]
            ]
        );

        //$this->session = $this->getSession();
    }

    public function getSession()
    {
        try {

            $response = $this->client->post('/', [
                'body' => json_encode([
                    'method' => 'auth.create_session',
                    'user_name' => 'admin',
                    'user_pass' => 'Red@1234',
                    ]
                ),
                'verify' => false
                ]
            );

            $body = $response->getBody()->getContents();
            $retorno = json_decode($body);

            if (isset($retorno->error)) {

                throw new \Exception($retorno->error);
            }
        } catch (\Exception $ex) {

            $retorno = ['message' => 'Ocorreu um erro.', 'error' => $ex->getMessage(), 'trace' => $ex->getTrace()];
        }

        return $retorno;
    }

    public function add($file)
    {
        try {

            $response = $this->client->post('/', [
                'body' => json_encode(
                    [
                        'session' => $this->session->session,
                        'method' => 'asset.add_to_database',
                        'url' => $file,
                    ]
                ),
                'verify' => false
                ]
            );

            $body = $response->getBody()->getContents();
            $retorno = json_decode($body);

            if (isset($retorno->error)) {

                throw new \Exception($retorno->error);
            }
        } catch (\Exception $ex) {

            $retorno = ['message' => 'Ocorreu um erro.', 'error' => $ex->getMessage(), 'trace' => $ex->getTrace()];
        }

        return $retorno;
    }

    public function statusRender()
    {
        ini_set('max_execution_time', 100);

        if (!isset($this->session)) {
            $this->session = $this->getSession();
        }

        // Get View
        $response = $this->client->post('/', [
            'body' => json_encode(
                [
                    'session' => $this->session->session,
                    'method' => 'proofscope.get_view_info',
                    'query' => '?proofscope&url=cloudflow%3A%2F%2FPP_FILE_STORE%2Fteste_render%2F10.pdf',
                ]
            ),
            'verify' => false
            ]
        );

        $body = $response->getBody()->getContents();
        $retorno = json_decode($body);

        //echo json_encode($retorno->view);

        $view = $retorno->view;

        $response = $this->client->post('/', [
            'body' => json_encode(
                [
                    'session' => $this->session->session,
                    'method' => 'proofscope.get_render_status_by_view',
                    'view' => $view,
                ]
            ),
            'verify' => false
            ]
        );

        $body = $response->getBody()->getContents();
        $retorno = json_decode($body);

        dd($retorno);
    }

    public function renderStatus($tipo, $id_arquivo)
    {
        switch ($tipo) {
            case 'ciclo':
                $arquivo = \App\ArtwCiclosLayout::find($id_arquivo);
                $caminho = config('app.cloudflow_ciclo');
                break;
            case 'versao':
                $arquivo = \App\ArtwVersoes::find($id_arquivo);
                $caminho = config('app.cloudflow_versao');
                break;
            case 'fornecedorpdf':
                $arquivo = \App\ArtwFornecedorPdfs::find($id_arquivo);
                $caminho = config('app.cloudflow_fornecedorpdf');
                break;
        }
        
        switch ($tipo) {
            case 'ciclo':
                $this->path = $this->getCicloArquivo($id_arquivo);
                break;
            case 'versao':
                $this->path = $this->getVersaoArquivo($id_arquivo);
                break;
            case 'fornecedorpdf':
                $this->path = $this->getFornecedorPdf($id_arquivo);
                break;
        }
        
        $file = $caminho . rawurlencode($this->arquivo);

        if (!$arquivo->view) {

            // Get View
            $response = $this->client->post('/', [
                'body' => json_encode(
                    [
                        'session' => $this->session->session,
                        'method' => 'proofscope.get_view_info',
                        'query' => '?proofscope&url=' . $file,
                    ]
                ),
                'verify' => false
                ]
            );

            $body = $response->getBody()->getContents();
            $retorno = json_decode($body);

            if (isset($retorno->error)) {

                sleep(2);
                $this->renderStatus($tipo, $id_arquivo);
            }

            $view = $retorno->view;

            if ($view) {

                $arquivo->view = json_encode($view);
                $arquivo->save();

                echo 'View salva no banco<br>';
            }
        }

        //echo $arquivo->view;
        //exit;

        $response = $this->client->post('/', [
            'body' => json_encode(
                [
                    'session' => $this->session->session,
                    'method' => 'proofscope.get_render_status_by_view',
                    'view' => json_decode($arquivo->view),
                ]
            ),
            'verify' => false
            ]
        );

        $body = $response->getBody()->getContents();
        $retorno = json_decode($body);

        //dd($retorno);

        if (!isset($retorno->all_done)) {

            sleep(2);
            $this->renderStatus($tipo, $id_arquivo);
        }

        if (!$retorno->all_done) {

            $progress = (isset($retorno->progress)) ? $retorno->progress : 0;

            $arquivo->status_renderizacao = $progress * 100;
            $arquivo->save();

            sleep(2);
            $this->renderStatus($tipo, $id_arquivo);
        } else {

            $arquivo->status_renderizacao = $retorno->progress * 100;
            $arquivo->renderizado = 1;
            $arquivo->save();

            //echo 'Status da renderização: ' . $arquivo->status_renderizacao . '%<br>';
        }
    }

    public function render($tipo, $id_arquivo)
    {
        switch ($tipo) {
            case 'ciclo':
                $caminho = config('app.cloudflow_ciclo');
                break;
            case 'versao':
                $caminho = config('app.cloudflow_versao');
                break;
            case 'fornecedorpdf':
                $caminho = config('app.cloudflow_fornecedorpdf');
                break;
        }

        if (!isset($this->session)) {
            $this->session = $this->getSession();
        }

        if (!$this->arquivo) {

            switch ($tipo) {
                case 'ciclo':
                    $this->arquivo = $this->getCicloArquivo($id_arquivo);
                    break;
                case 'versao':
                    $this->arquivo = $this->getVersaoArquivo($id_arquivo);
                    break;
                case 'fornecedorpdf':
                    $this->arquivo = $this->getFornecedorPdf($id_arquivo);
                    break;
            }
        }

        if (!$this->arquivo) {

            echo 'Arquivo não encontrado';
            exit;
        }

        $file = $caminho . rawurlencode($this->arquivo);

        // Renderiza
        $response = $this->client->post('/', [
            'body' => json_encode(
                [
                    'session' => $this->session->session,
                    'method' => 'proofscope.request_render_by_url',
                    'url' => $file,
                ]
            ),
            'verify' => false
            ]
        );

        $body = $response->getBody()->getContents();
        $retorno = json_decode($body);
    }

    public function reRender($tipo, $id_arquivo)
    {
        switch ($tipo) {
            case 'ciclo':
                $caminho = config('app.cloudflow_ciclo');
                break;
            case 'versao':
                $caminho = config('app.cloudflow_versao');
                break;
            case 'fornecedorpdf':
                $caminho = config('app.cloudflow_fornecedorpdf');
                break;
        }

        if (!isset($this->session)) {
            $this->session = $this->getSession();
        }

        if (!$this->arquivo) {

            switch ($tipo) {
                case 'ciclo':
                    $this->arquivo = $this->getCicloArquivo($id_arquivo);
                    break;
                case 'versao':
                    $this->arquivo = $this->getVersaoArquivo($id_arquivo);
                    break;
                case 'fornecedorpdf':
                    $this->arquivo = $this->getFornecedorPdf($id_arquivo);
                    break;
            }
        }

        if (!$this->arquivo) {

            echo 'Arquivo não encontrado';
            exit;
        }

        $file = $caminho . rawurlencode($this->arquivo);
        
        /*
        // Reseta Render
        $this->client->post('/', [
            'body' => json_encode(
                [
                    'session' => $this->session->session,
                    'method' => 'assets.reset_render',
                    'url' => $file,
                ]
            ),
            'verify' => false
            ]
        );
        */

        // Reseta Metadatas
        $this->client->post('/', [
            'body' => json_encode(
                [
                    'session' => $this->session->session,
                    'method' => 'assets.reset_metadata',
                    'url' => $file,
                ]
            ),
            'verify' => false
            ]
        );

        // Reseta Thumbs
        $this->client->post('/', [
            'body' => json_encode(
                [
                    'session' => $this->session->session,
                    'method' => 'assets.reset_thumb',
                    'url' => $file,
                ]
            ),
            'verify' => false
            ]
        );

        sleep(5);

        // Renderiza
        $response = $this->client->post('/', [
            'body' => json_encode(
                [
                    'session' => $this->session->session,
                    'method' => 'proofscope.request_rerender_by_url',
                    'url' => $file,
                ]
            ),
            'verify' => false
            ]
        );

        $body = $response->getBody()->getContents();
        $retorno = json_decode($body);
    }

    public function renderOLD()
    {
        try {

            $response = $this->client->post('/', [
                'body' => json_encode(
                    [
                        'session' => $this->session->session,
                        'method' => 'proofscope.render',
                        'url' => $file,
                    ]
                ),
                'verify' => false
                ]
            );

            $body = $response->getBody()->getContents();
            $retorno = json_decode($body);

            if (isset($retorno->error)) {

                throw new \Exception($retorno->error);
            }
        } catch (\Exception $ex) {

            $retorno = ['message' => 'Ocorreu um erro.', 'error' => $ex->getMessage(), 'trace' => $ex->getTrace()];
        }

        return $retorno;
    }

    public function getView($file, $id_ticket, $arquivoCicloAtual, ArtwPerfisUsuariosEquipeTickets $artwPerfisUsuariosEquipeTickets)
    {
        $hasNote = false;
        $viewOnlyNotes = false;
        $canDeleteNotes = false;
        $showDownloadNotesReport = false;
        //$showCountPixelsTool = false;
        //$showSeamlessTools = false;
        
        if ($id_ticket)
        {
            $ticket = \App\ArtwTickets::find($id_ticket);
            
            /*
            $participante = $ticket->checkUsuario(Auth::user()->id);

            if (!$participante)
            {
                $membro = $artwPerfisUsuariosEquipeTickets->checkUsuario(Auth::user()->id, $ticket->id);
                $participante = $membro->artwPerfisUsuariosTicket;
            }
             * 
             */
            
            //if ($ticket->isOpen() && ($ticket->checkUsuario(Auth::user()->id) || $artwPerfisUsuariosEquipeTickets->checkUsuario(Auth::user()->id, $ticket->id)) )
            if ($ticket)
            {
                $hasNote = true;
                
                // Verifica se é aprovador, se está na estapa dele e se a aprovação extá pendente
                
                if ($ticket->id_status == 3 || $ticket->id_status == 4)
                {
                    if ($ticket->id_status == 3) {
                        $id_perfil = 2;
                    } elseif ($ticket->id_status == 4) {
                        $id_perfil = 1;
                    } else {
                        $id_perfil = 1;
                    }

                    $perfilUsuarioEquipeTicket = $artwPerfisUsuariosEquipeTickets->artwPerfilUsuarioEquipeTicket(Auth::user()->id, $id_perfil, $ticket->id);

                    if ($perfilUsuarioEquipeTicket) {
                        $perfilParticipante = $perfilUsuarioEquipeTicket->artwPerfisUsuariosTicket;
                    } else {
                        $perfilParticipante = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);
                    }

                    /*
                    if (!$participante)
                    {
                        exit;
                    }
                     * 
                     */
                    
                    //$perfilParticipante = $participante;
                    
                    //dd($perfilParticipante->ciclo, $ticket->ciclo_atual()->numero);
                    
                    if ( (request()->headers->get('referer') == route('site.artwork.ticket.edit.deny.get', $id_ticket)) && $perfilParticipante && ($perfilParticipante->aprovado == 0 || ($perfilParticipante->aprovado == 2 && $ticket->ciclo_atual()->numero > $perfilParticipante->ciclo)) )
                    {
                        $canDeleteNotes = true;
                    }
                    else
                    {
                        $viewOnlyNotes = true;
                    }
                }
                else
                {
                    
                    $viewOnlyNotes = true;
                    
                }
                
                if (!$arquivoCicloAtual)
                {
                    $viewOnlyNotes = true;
                }
                
                /*
                if ($participante->em_reprovacao != 1)
                {
                    //$viewOnlyNotes = true;
                }
                 * 
                 */
            }
        }
        
        try {

            $this->session = $this->getSession();

            //$this->add($file);
            //$this->render($file);

            $response = $this->client->post('/', [
                'body' => json_encode(
                    [
                        'session' => $this->session->session,
                        'method' => 'proofscope.create_view_file_url_with_options',
                        'host_url' => $this->base_api,
                        'file_url' => $file,
                        'options' => [
                            'require_login' => false,
                            'email' => Auth::user()->email,
                            //'email' => Auth::user()->nome_abreviado,
                            'time_out' => 10,
                            'viewer_parameters' => [
                                'options' => [
                                    'hasNotes' => $hasNote,
                                    'lockNotePosition' => false,
                                    'viewOnlyNotes' => $viewOnlyNotes,
                                    'canDeleteNotes' => $canDeleteNotes,
                                    'showDownloadNotesReport' => $showDownloadNotesReport,
                                    'showCountPixelsTool' => false,
                                    'showSeamlessTools' => false,
                                    'showMisregistration' => false,
                                    'showEnumerate' => false,
                                    'showViewCurves' => false,
                                    'showMeasureHalftoneTool' => false,
                                    'showPreflightNotes' => false
                                ]
                            ]
                        ]
                    ]
                ),
                'verify' => false
                ]
            );

            $body = $response->getBody()->getContents();
            $retorno = json_decode($body);

            if (isset($retorno->error)) {

                throw new \Exception($retorno->error);
            }
        } catch (\Exception $ex) {

            abort(504);
        }

        return $retorno;
    }

    public function getCompare($file1, $file2, $id_ticket = null, ArtwPerfisUsuariosEquipeTickets $artwPerfisUsuariosEquipeTickets)
    {
        $hasNote = true;
        $viewOnlyNotes = true;
        $canDeleteNotes = false;
        $showDownloadNotesReport = false;
        
        if ($id_ticket)
        {
            $ticket = \App\ArtwTickets::find($id_ticket);
            
            //if ($ticket->isOpen() && ($ticket->checkUsuario(Auth::user()->id) || $artwPerfisUsuariosEquipeTickets->checkUsuario(Auth::user()->id, $ticket->id)) )
            if ($ticket)
            {
                $hasNote = true;
                
                // Verifica se é aprovador, se está na estapa dele e se a aprovação extá pendente
                
                if ($ticket->id_status == 3 ||$ticket->id_status == 4)
                {
                    $idPerfil = ($ticket->id_status == 3) ? 2 : 1;
                    
                    $perfilParticipante = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $idPerfil);
                    
                    //dd($perfilParticipante->ciclo, $ticket->ciclo_atual()->numero);
                    
                    if ($perfilParticipante && ($perfilParticipante->aprovado == 0 || ($perfilParticipante->aprovado == 2 && $ticket->ciclo_atual()->numero > $perfilParticipante->ciclo)) )
                    {
                        //dd(1);
                        $canDeleteNotes = true;
                    }
                    else
                    {
                        //dd(2);
                        $viewOnlyNotes = true;
                    }
                }
                else
                {
                    
                    $viewOnlyNotes = true;
                    
                }
            }
        }
        
        try {

            $this->session = $this->getSession();

            //$this->add($file1);
            //$this->add($file1);
            //$this->render($file2);
            //$this->render($file2);

            $options = [
                'require_login' => false,
                'email' => Auth::user()->email,
                //'email' => Auth::user()->nome_abreviado,
                'time_out' => 10,
                'viewer_parameters' => [
                    'options' => [
                        'hasNotes' => $hasNote,
                        'lockNotePosition' => true,
                        'viewOnlyNotes' => $viewOnlyNotes,
                        'canDeleteNotes' => $canDeleteNotes,
                        'showDownloadNotesReport' => $showDownloadNotesReport,
                        'showCountPixelsTool' => false,
                        'showSeamlessTools' => false,
                        'showMisregistration' => false,
                        'showEnumerate' => false,
                        'showViewCurves' => false,
                        'showMeasureHalftoneTool' => false,
                        //'differenceDefaultView' => 'sideBySide'
                    ]
                ]
                //'viewer' => 'Difference',
            ];

            //echo json_encode($options);
            //exit;

            $response = $this->client->post('/', [
                'body' => json_encode(
                    [
                        'session' => $this->session->session,
                        'method' => 'proofscope.create_view_file_difference_url_with_options',
                        'host_url' => $this->base_api,
                        'file_url' => $file1,
                        'diff_url' => $file2,
                        'options' => $options
                    ]
                ),
                'verify' => false
                ]
            );

            $body = $response->getBody()->getContents();
            $retorno = json_decode($body);

            if (isset($retorno->error)) {

                throw new \Exception($retorno->error);
            }
        } catch (\Exception $ex) {

            abort(504);
            
        }

        return $retorno;
    }

    public function getFornecedorPdf($id_arquivo, $completo = false)
    {
        $fornecedorPdf = \App\ArtwFornecedorPdfs::find($id_arquivo);

        if (!$fornecedorPdf) {
            return;
        }

        if ($completo) {
            $file = $fornecedorPdf->path;
        } else {
            $fileArray = explode('/', $fornecedorPdf->path);
            $file = end($fileArray);
        }

        return $file;
    }

    public function getCicloArquivo($id_arquivo, $completo = false)
    {
        $cicloLayout = \App\ArtwCiclosLayout::find($id_arquivo);

        if (!$cicloLayout) {
            return;
        }

        if ($completo) {
            $file = $cicloLayout->path;
        } else {
            $fileArray = explode('/', $cicloLayout->path);
            $file = end($fileArray);
        }

        return $file;
    }

    public function getVersaoArquivo($id_arquivo, $completo = false)
    {
        $versao = \App\ArtwVersoes::find($id_arquivo);

        if (!$versao) {
            return;
        }

        if ($completo) {
            $file = $versao->path;
        } else {
            $fileArray = explode('/', $versao->path);
            $file = end($fileArray);
        }

        return $file;
    }

    public function viewService(ArtwPerfisUsuariosEquipeTickets $artwPerfisUsuariosEquipeTickets, $id_arquivo, $tipo, $id_ticket = null)
    {
        $arquivoCicloAtual = false;
        
        switch ($tipo) {
            case 'ciclo':
                
                if ($id_ticket)
                {
                    $ticket = \App\ArtwTickets::find($id_ticket);
                    
                    $cicloAtual = $ticket->ciclo_atual();
                    
                    if ($cicloAtual->artwCiclosLayout->last() && $cicloAtual->artwCiclosLayout->last()->id == $id_arquivo)
                    {
                        $arquivoCicloAtual = true;
                    }
                }
                
                //$caminho = 'cloudflow://RUBRUM_AMBEV_CICLO/';
                $caminho = config('app.cloudflow_ciclo');
                $file = $this->getCicloArquivo($id_arquivo);
                $arquivo = $caminho . rawurlencode($file);
                break;
            case 'versao':
                //$caminho = 'cloudflow://RUBRUM_AMBEV_VERSAO/';
                $caminho = config('app.cloudflow_versao');
                $file = $this->getVersaoArquivo($id_arquivo);
                $arquivo = $caminho . rawurlencode($file);
                break;
            default:
                break;
        }

        $url = $this->getView($arquivo, $id_ticket, $arquivoCicloAtual, $artwPerfisUsuariosEquipeTickets)->url;

        return redirect($url);
    }

    public function view($id_arquivo, $tipo, $id_ticket = null)
    {
        

        switch ($tipo) {
            case 'ciclo':
                //$caminho = 'cloudflow://RUBRUM_AMBEV_CICLO/';
                $caminho = config('app.cloudflow_ciclo');
                $file = $this->getCicloArquivo($id_arquivo);
                $arquivo = $caminho . rawurlencode($file);
                
                $cicloLayoyt = \App\ArtwCiclosLayout::find($id_arquivo);
                $ticket = $cicloLayoyt->artwCiclo->artwTicket;
                
                $ticketLabel1 = 'CICLO ' . $cicloLayoyt->artwCiclo->numero;
                
                break;
            case 'versao':
                //$caminho = 'cloudflow://RUBRUM_AMBEV_VERSAO/';
                $caminho = config('app.cloudflow_versao');
                $file = $this->getVersaoArquivo($id_arquivo);
                $arquivo = $caminho . rawurlencode($file);
                
                $versao = \App\ArtwVersoes::find($id_arquivo);
                $ticket = $versao->artwTicket;
                
                $ticketLabel1 = 'VERSÃO ' . $versao->numero;
                
                break;
            default:
                break;
        }

        if ($ticket->artwItem && $ticket->artwItem->artwVariacao && !in_array($ticket->artwItem->artwVariacao->sku->produtos->familias->id, Auth::user()->familias))
        {
            return redirect()->route('site.artwork.family');
        }
        
        $ticketNumero = $ticket->numero;
        $ticketTitulo = $ticket->sku_nome_completo;

        if (!$file) {
            abort('404');
        }

        $url = route('site.artwork.viewService', [$id_arquivo, $tipo, $id_ticket]);

        return view('site.api.iframe', compact('url', 'file', 'ticketNumero', 'ticketTitulo', 'ticketLabel1'));
    }

    public function workflowView($id_arquivo, $tipo, $id_ticket = null)
    {
        

        switch ($tipo) {
            case 'ciclo':
                //$caminho = 'cloudflow://RUBRUM_AMBEV_CICLO/';
                $caminho = config('app.cloudflow_ciclo');
                $file = $this->getCicloArquivo($id_arquivo);
                $arquivo = $caminho . rawurlencode($file);
                
                $cicloLayoyt = \App\ArtwCiclosLayout::find($id_arquivo);
                $ticket = $cicloLayoyt->artwCiclo->artwTicket;
                
                $ticketLabel1 = 'CICLO ' . $cicloLayoyt->artwCiclo->numero;
                
                break;
            case 'versao':
                //$caminho = 'cloudflow://RUBRUM_AMBEV_VERSAO/';
                $caminho = config('app.cloudflow_versao');
                $file = $this->getVersaoArquivo($id_arquivo);
                $arquivo = $caminho . rawurlencode($file);
                
                $versao = \App\ArtwVersoes::find($id_arquivo);
                $ticket = $versao->artwTicket;
                
                $ticketLabel1 = 'VERSÃO ' . $versao->numero;
                
                break;
            default:
                break;
        }

        if ($ticket->artwItem && $ticket->artwItem->artwVariacao && !in_array($ticket->artwItem->artwVariacao->sku->produtos->familias->id, Auth::user()->familias))
        {
            return redirect()->route('site.artwork.family');
        }
        
        $ticketNumero = $ticket->numero;
        $ticketTitulo = $ticket->sku_nome_completo;

        if (!$file) {
            abort('404');
        }

        $url = route('site.artwork.workflow.viewService', [$id_arquivo, $tipo, $id_ticket]);

        return view('site.api.iframe', compact('url', 'file', 'ticketNumero', 'ticketTitulo', 'ticketLabel1'));
    }
    
    public function getTicketEditDeny($id_ticket, ArtwPerfisUsuariosEquipeTickets $artwPerfisUsuariosEquipeTickets)
    {
        $ticket = \App\ArtwTickets::find($id_ticket);
        
        if ($ticket->id_status == 3) {
            $id_perfil = 2;
        } elseif ($ticket->id_status == 4) {
            $id_perfil = 1;
        } else {
            $id_perfil = 0;
        }

        $perfilUsuarioEquipeTicket = $artwPerfisUsuariosEquipeTickets->artwPerfilUsuarioEquipeTicket(Auth::user()->id, $id_perfil, $ticket->id);

        if ($perfilUsuarioEquipeTicket) {
            $perfilUsuarioTicket = $perfilUsuarioEquipeTicket->artwPerfisUsuariosTicket;
        } else {
            $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);
        }
        
        if (!$perfilUsuarioTicket)
        {
            return redirect()->route('site.artwork.ticket.edit.step1.get', ['ticket' => $ticket->id]);
        }
        
        if ( ($perfilUsuarioTicket->aprovado == 1 ) || ($perfilUsuarioTicket->aprovado == 2 && $perfilUsuarioTicket->ciclo >= $ticket->ciclo_atual()->numero) || ($perfilUsuarioTicket->em_reprovacao && $perfilUsuarioTicket->em_reprovacao_by != Auth::user()->id) )
        {
            return redirect()->route('site.artwork.ticket.edit.step1.get', ['ticket' => $ticket->id]);
        }
        
        $perfilUsuarioTicket->em_reprovacao = 1;
        $perfilUsuarioTicket->em_reprovacao_by = Auth::user()->id;
        $perfilUsuarioTicket->save();
        
        $layout = $ticket->ciclo_atual()->artwCiclosLayout->last();
        
        $tipo = 'ciclo';
        $id_arquivo = $layout->id;
        
        switch ($tipo) {
            case 'ciclo':
                //$caminho = 'cloudflow://RUBRUM_AMBEV_CICLO/';
                $caminho = config('app.cloudflow_ciclo');
                $file = $this->getCicloArquivo($id_arquivo);
                $arquivo = $caminho . rawurlencode($file);
                break;
            case 'versao':
                //$caminho = 'cloudflow://RUBRUM_AMBEV_VERSAO/';
                $caminho = config('app.cloudflow_versao');
                $file = $this->getVersaoArquivo($id_arquivo);
                $arquivo = $caminho . rawurlencode($file);
                break;
            default:
                break;
        }

        if (!$file) {
            abort('404');
        }
        
        $voltar = route('site.artwork.ticket.edit.deny.back.get', [$ticket->id]);
        
        $prosseguir = route('site.artwork.ticket.edit.deny.step2.get', [$ticket->id]);
        
        $ticketNumero = $ticket->numero;
        $ticketTitulo = $ticket->sku_nome_completo;
        
        $ticketLabel1 = 'CICLO ' . $ticket->ciclo_atual()->numero;
        
        $url = route('site.artwork.viewService', [$id_arquivo, $tipo, $id_ticket]);

        return view('site.api.iframe', compact('url', 'file', 'voltar', 'prosseguir', 'ticketNumero', 'ticketTitulo', 'ticketLabel1'));
    }
    
    public function getTicketEditDenyBack($ticket_id, ArtwPerfisUsuariosEquipeTickets $artwPerfisUsuariosEquipeTickets)
    {
        $ticket = ArtwTickets::find($ticket_id);

        if (!$ticket) {
            abort(404);
        }
        
        return redirect()->route('site.artwork.ticket.edit.step1.get', ['ticket' => $ticket->id]);
    }
    
    public function apagarNotas($ticket_id)
    {
        $ticket = ArtwTickets::find($ticket_id);

        if (!$ticket) {
            abort(404);
        }

        $this->session = $this->getSession();

        $cicloAtual = $ticket->ciclo_atual();
        
        $id_arquivo = $cicloAtual->artwCiclosLayout->last()->id;
        
        $caminho = config('app.cloudflow_ciclo');
        $file = $this->getCicloArquivo($id_arquivo);
        $arquivo = $caminho . rawurlencode($file);
        
        $this->session = $this->getSession();

        if (!isset($this->session->session))
        {
            return;
        }
        
        $options = [
                'include_notes' => 'Database'
            ];
        
        $response = $this->client->post('/', [
            'body' => json_encode(
                [
                    'session' => $this->session->session,
                    'method' => 'asset.get_notes_by_url',
                    'asset_url' => $arquivo,
                    'options' => $options
                ]
            ),
            'verify' => false
            ]
        );

        $body = $response->getBody()->getContents();
        $retorno = json_decode($body);
        
        if (isset($retorno->notes) && $retorno->notes)
        {
            foreach ($retorno->notes as $note)
            {
                if ($note->comments && Auth::user()->email == $note->comments[0]->email_address)
                {
                    //dd($retorno->notes, $note->comments[0]->email_address, Auth::user()->email);

                    //echo 'Apagar ' . $note->id . '<br>';

                    $response = $this->client->post('/', [
                        'body' => json_encode(
                            [
                                'session' => $this->session->session,
                                'method' => 'asset.delete_note_by_url',
                                'asset_url' => $arquivo,
                                'note' => $note,
                                'options' => []
                            ]
                        ),
                        'verify' => false
                        ]
                    );

                    $body = $response->getBody()->getContents();
                    $retorno = json_decode($body);

                    //dd($retorno);
                }
            }
        }
        
        //exit;
        
        if ($ticket->id_status == 3) {
            $id_perfil = 2;
        } elseif ($ticket->id_status == 4) {
            $id_perfil = 1;
        }

        /*
        $artwPerfisUsuariosEquipeTickets = new ArtwPerfisUsuariosEquipeTickets;
        
        $perfilUsuarioEquipeTicket = $artwPerfisUsuariosEquipeTickets->artwPerfilUsuarioEquipeTicket(Auth::user()->id, $id_perfil, $ticket->id);

        if ($perfilUsuarioEquipeTicket) {
            $perfilUsuarioTicket = $perfilUsuarioEquipeTicket->artwPerfisUsuariosTicket;
        } else {
            $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);
        }
        
        $perfilUsuarioTicket->em_reprovacao = 0;
        $perfilUsuarioTicket->save();
         * 
         */
        
        //return redirect()->route('site.artwork.ticket.edit.step1.get', ['ticket' => $ticket->id]);
    }

    public function compareService($id_arquivo1, $tipo1, $id_arquivo2, $tipo2, $id_ticket, ArtwPerfisUsuariosEquipeTickets $artwPerfisUsuariosEquipeTickets)
    {
        switch ($tipo1) {
            case 'ciclo':
                //$caminho = 'cloudflow://RUBRUM_AMBEV_CICLO/';
                $caminho = config('app.cloudflow_ciclo');
                $file1 = $this->getCicloArquivo($id_arquivo1);
                $arquivo1 = $caminho . rawurlencode($file1);
                break;
            case 'versao':
                //$caminho = 'cloudflow://RUBRUM_AMBEV_VERSAO/';
                $caminho = config('app.cloudflow_versao');
                $file1 = $this->getVersaoArquivo($id_arquivo1);
                $arquivo1 = $caminho . rawurlencode($file1);
                break;
            default:
                break;
        }

        switch ($tipo2) {
            case 'ciclo':
                //$caminho = 'cloudflow://RUBRUM_AMBEV_CICLO/';
                $caminho = config('app.cloudflow_ciclo');
                $file2 = $this->getCicloArquivo($id_arquivo2);
                $arquivo2 = $caminho . rawurlencode($file2);
                break;
            case 'versao':
                //$caminho = 'cloudflow://RUBRUM_AMBEV_VERSAO/';
                $caminho = config('app.cloudflow_versao');
                $file2 = $this->getVersaoArquivo($id_arquivo2);
                $arquivo2 = $caminho . rawurlencode($file2);
                break;
            default:
                break;
        }

        $url = $this->getCompare($arquivo1, $arquivo2, $id_ticket, $artwPerfisUsuariosEquipeTickets)->url;

        return redirect($url);
    }

    public function compareServiceFornecedorPdf($id_arquivo1, $id_arquivo2, ArtwPerfisUsuariosEquipeTickets $artwPerfisUsuariosEquipeTickets)
    {
        sleep(5);

        $fornecedorPdf = \App\ArtwFornecedorPdfs::find($id_arquivo1);

        if ($fornecedorPdf->artwTicket->id_status == 6){
            
            $caminho = config('app.cloudflow_versao');
            $file2 = $this->getVersaoArquivo($id_arquivo2);
            $arquivo2 = $caminho . rawurlencode($file2);
            
        } else {
            
            $caminho = config('app.cloudflow_ciclo');
            $file2 = $this->getCicloArquivo($id_arquivo2);
            $arquivo2 = $caminho . rawurlencode($file2);
            
        }
        
        $caminho = config('app.cloudflow_fornecedorpdf');
        $file1 = $this->getFornecedorPdf($id_arquivo1);
        $arquivo1 = $caminho . rawurlencode($file1);

        $url = $this->getCompare($arquivo2, $arquivo1, null, $artwPerfisUsuariosEquipeTickets)->url;

        return redirect($url);
    }

    public function compare($id_arquivo1, $tipo1, $id_arquivo2, $tipo2, $id_ticket)
    {
        if (config('app.costummer_id') == 16)
        {
            //return redirect('');
        }

        switch ($tipo1) {
            case 'ciclo':
                //$caminho = 'cloudflow://RUBRUM_AMBEV_CICLO/';
                $caminho = config('app.cloudflow_ciclo');
                $file1 = $this->getCicloArquivo($id_arquivo1);
                $arquivo1 = $caminho . rawurlencode($file1);
                
                $cicloLayoyt = \App\ArtwCiclosLayout::find($id_arquivo1);
                $ticket = $cicloLayoyt->artwCiclo->artwTicket;
                
                $ticketLabel1 = 'CICLO ' . $cicloLayoyt->artwCiclo->numero;
                
                $ticketLabel1 .= ($ticket->ciclo_atual()->numero == $cicloLayoyt->artwCiclo->numero) ? ' - ATUAL' : ' - REPROVADO';
                
                $ticketBadge1 = 'A';
                
                break;
            case 'versao':
                //$caminho = 'cloudflow://RUBRUM_AMBEV_VERSAO/';
                $caminho = config('app.cloudflow_versao');
                $file1 = $this->getVersaoArquivo($id_arquivo1);
                $arquivo1 = $caminho . rawurlencode($file1);
                
                $versao = \App\ArtwVersoes::find($id_arquivo1);
                $ticket = $versao->artwTicket;
                
                $ticketLabel1 = 'VERSÃO ' . $versao->numero;
                
                $ticketBadge1 = 'A';
                
                break;
            default:
                break;
        }

        switch ($tipo2) {
            case 'ciclo':
                //$caminho = 'cloudflow://RUBRUM_AMBEV_CICLO/';
                $caminho = config('app.cloudflow_ciclo');
                $file2 = $this->getCicloArquivo($id_arquivo2);
                $arquivo2 = $caminho . rawurlencode($file2);
                
                $cicloLayoyt = \App\ArtwCiclosLayout::find($id_arquivo2);
                $ticket2 = $cicloLayoyt->artwCiclo->artwTicket;
                
                $ticketLabel2 = 'CICLO ' . $cicloLayoyt->artwCiclo->numero;
                
                $ticketLabel2 .= ($ticket2->ciclo_atual()->numero == $cicloLayoyt->artwCiclo->numero) ? ' - ATUAL' : ' - REPROVADO';
                
                $ticketBadge2 = 'B';
                
                break;
            case 'versao':
                //$caminho = 'cloudflow://RUBRUM_AMBEV_VERSAO/';
                $caminho = config('app.cloudflow_versao');
                $file2 = $this->getVersaoArquivo($id_arquivo2);
                $arquivo2 = $caminho . rawurlencode($file2);
                
                $versao = \App\ArtwVersoes::find($id_arquivo2);
                $ticket2 = $versao->artwTicket;
                
                $ticketLabel2 = 'VERSÃO ' . $versao->numero . ' - MERCADO';
                
                $ticketBadge2 = 'B';
                
                break;
            default:
                break;
        }
        
        $ticketNumero = $ticket->numero;
        $ticketTitulo = $ticket->sku_nome_completo;

        $url = route('site.artwork.compareService', [$id_arquivo1, $tipo1, $id_arquivo2, $tipo2, $id_ticket]);

        return view('site.api.iframe', compact('url', 'file1', 'file2', 'ticketNumero', 'ticketTitulo', 'ticketLabel1', 'ticketLabel2', 'ticketBadge1', 'ticketBadge2'));
    }

    public function compareFornecedorPdf($id_pdf)
    {
        $fornecedorPdf = \App\ArtwFornecedorPdfs::find($id_pdf);

        if ($fornecedorPdf->artwTicket->id_status == 6){
            
            $layoutAprovado = $fornecedorPdf->artwTicket->artwVersoes->last();
            
        } else {
            
            $layoutAprovado = $fornecedorPdf->artwTicket->ciclo_atual()->artwCiclosLayout->last();
            
        }
        
        if (!$fornecedorPdf || !$layoutAprovado) {

            return 0;
            exit;
        }

        $caminho1 = config('app.cloudflow_fornecedorpdf');

        $file1 = $this->getFornecedorPdf($id_pdf);

        $arquivo1 = $caminho1 . rawurlencode($file1);

        
        if ($fornecedorPdf->artwTicket->id_status == 6){
            
            $caminho2 = config('app.cloudflow_versao');

            $file2 = $this->getVersaoArquivo($layoutAprovado->id);
            
        } else {
            
            $caminho2 = config('app.cloudflow_ciclo');

            $file2 = $this->getCicloArquivo($layoutAprovado->id);
            
        }

        $arquivo2 = $caminho2 . rawurlencode($file2);
        
        $ticketLabel2 = 'Pdf do Fornecedor';
        
        $ticketBadge1 = 'A';
        
        $ticket = $fornecedorPdf->artwTicket;
        
        if ($fornecedorPdf->artwTicket->id_status == 6){
            
            $ticketLabel1 = 'VERSÃO ' . $layoutAprovado->numero . ' - MERCADO';
            
        } else {
            
            $ticketLabel1 = 'CICLO ' . $layoutAprovado->artwCiclo->numero;

            $ticketLabel1 .= ($ticket->ciclo_atual()->numero == $layoutAprovado->artwCiclo->numero) ? ' - APROVADO' : ' - REPROVADO';
            
        }

        $ticketBadge2 = 'B';
        
        $ticketNumero = $ticket->numero;
        $ticketTitulo = $ticket->sku_nome_completo;

        $url = route('site.artwork.compareServiceFornecedorPdf', [$id_pdf, $layoutAprovado->id]);

        return view('site.api.iframe', compact('url', 'file2', 'file1', 'ticketNumero', 'ticketTitulo', 'ticketLabel2', 'ticketLabel1', 'ticketBadge2', 'ticketBadge1'));
    }

    public function ApiCompareIframe()
    {
        return view('site.api.iframe');
    }
}
