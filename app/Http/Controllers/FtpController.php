<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AdmUsuario;
use App\AdmFtps;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Http\Requests;

class FtpController extends Controller
{
    private $ftp;

    public function __construct(AdmFtps $ftp)
    {
    	$this->ftp = $ftp;
    }

    public function index()
    {
        $ftps = array();
    	$allFtps = $this->ftp->all();

        $total = count($allFtps);

    	foreach ($allFtps as $ftp) {
    		$ftp->admUsuarios;
    		$ftps[] = $ftp;
    	}

    	return view('admin.ftps.index', compact('ftps', 'total'));
    }

    public function search($name)
    {
        $ftps = array();
    	$allFtps = $this->ftp->where('nome', 'like', '%'.$name.'%')->orderBy('nome')->get();

        $total = count($allFtps);

        foreach ($allFtps as $ftp) {
    		$ftp->admUsuarios;
    		$ftps[] = $ftp;
    	}

    	return view('admin.ftps.index', compact('ftps', 'total'));
    }

    public function create(Request $request)
    {
        if(empty($request->nome) || empty($request->file)){
            return false;
        }

        $ftp = $this->ftp;
        $dados = $request->all();
        $file = isset($dados['file']) ? $dados['file'] : null;
        $filePath = '/files/ftp/';

        if(!is_null($file)){
            $fileName = $file->getClientOriginalName();
            $destinationPath = public_path().$filePath;
            $upload_success = $file->move($destinationPath, $fileName);
            $dados['path'] = $filePath.$fileName;
        }

        $dados['id_adm_usuario'] = Auth::user()->id;

        if($ftp->create($dados))
            return array('error' => false);
        else
            return array('error' => true, 'message' => 'Erro ao criar o arquivo. Tente novamente mais tarde.');
    }

    public function edit($id)
    {
        $editFtp = $this->ftp->find($id);
        return response(compact('editFtp'));
    }

    public function update($id, Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

        $ftp = $this->ftp->find($id);
        $dados = $request->all();

        $file = isset($dados['file']) ? $dados['file'] : null;
        $filePath = '/files/ftp/';

        if(!is_null($file)){
            if(File::isFile(public_path().$ftp->path)){
                File::delete(public_path().$ftp->path);
            }

            $fileName = $file->getClientOriginalName();
            $destinationPath = public_path().$filePath;
            $upload_success = $file->move($destinationPath, $fileName);
            $dados['path'] = $filePath.$fileName;
        }

        $ftp->update($dados);
    }

    public function remove($id)
    {
    	$removeFtp = $this->ftp->find($id);

        if(File::isFile(public_path().$removeFtp->path)){
            File::delete(public_path().$removeFtp->path);
        }

        $removeFtp->delete();
    }
}
