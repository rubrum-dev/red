<?php

namespace App\Http\Controllers;

use App\AdmPerfis;
use App\AdmUsuarios;
use App\Familias;
use App\Components\Common;
use Illuminate\Http\Request;

class PerfilController extends Controller
{
    private $perfil;

    public function __construct(AdmPerfis $perfil)
    {
    	$this->perfil = $perfil;
    }

    public function index()
    {
    	$profiles = $this->perfil->whereNotIn('id', [1,2])->get();
        $allFamilies = Familias::orderBy('nome')->get();
        $families = null;

        $total = count($profiles);

        foreach ($allFamilies as $value) {
            $families[$value->id] = $value->nome;
        }

    	return view('admin.perfis.index', compact('profiles', 'families', 'total'));
    }

    public function search($name)
    {
    	$profiles = $this->perfil->where('nome', 'like', '%'.$name.'%')->whereNotIn('id', [1,2])->get();
        $allFamilies = Familias::all();

        $total = count($profiles);

        foreach ($allFamilies as $value) {
            $families[$value->id] = $value->nome;
        }

    	return view('admin.perfis.index', compact('profiles', 'families', 'total'));
    }

    public function create(Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

    	$profile = $this->perfil;
        
        $data = $request->all();
        $data['status'] = ($request->input('status')) ? 1 : 0;
        
        if(!is_null($request->input('families')))
            $profile->create($data)->familias()->sync($request->input('families'));
        else
            $profile->create($data);

    	return redirect()->route("admin.profile");
    }

    public function edit($id)
    {
    	$editProfile = $this->perfil->find($id);
        $editProfile->familias = $editProfile->familias;

    	return response(compact('editProfile'));
    }

    public function update($id, Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

        if(!isset($request->gerencia_usuario)){
            $request['gerencia_usuario'] = 0;
        }

    	$profile = $this->perfil->find($id);
        $profile->familias()->sync($request->input('families'));
        
        $data = $request->all();
        $data['status'] = ($request->input('status')) ? 1 : 0;
        
        $profile->update($data);
    }

    public function remove($id)
    {
        $removeUser = new Common;
		$removeProfile = $this->perfil->find($id);

        foreach ($removeProfile->admUsuarios as $usuario) {
            $removeUser->removeUser($usuario->id);
        }

    	$removeProfile->familias()->detach();
    	$removeProfile->delete();
    }

}
