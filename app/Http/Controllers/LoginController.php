<?php

namespace App\Http\Controllers;

use Mail;
use Redis;
use Validator;
use App\AdmUsuarios;
use App\AdmUsuariosLogins;
use App\AdmUsuariosTokens;
use App\Sessions;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Components\Common;
use GuzzleHttp\Client;

class LoginController extends Controller
{
	public function index()
    {
    	if(Auth::check()){
	    	return redirect('dashboard');
	    }
    	return view('home');
    }

	public function getLogin()
    {
    	if(Auth::check()){
	    	return redirect('dashboard');
	    }
    	return view('login');
    }
    
    public function getLoginSso()
    {
        $url = 'https://login.microsoftonline.com/'
                .''.config('app.sso_tenant_id').'/oauth2/v2.0/authorize?'
                .'client_id='.config('app.sso_client_id')
                .'&response_type=token&'
                .'redirect_uri='.route('site.login.sso.post')
                .'&response_mode=form_post&scope=openid%20user.read';

        return redirect($url);
    }

    public function postLoginSso(Request $request)
    {
        $this->client = new Client([
            'base_uri' => 'https://graph.microsoft.com',
            'headers' => [
                'Content-Type' => 'application/json'
            ]
            ]
        );

        $response = $this->client->get('v1.0/me?$select=givenName,surname,mail,otherMails', [
            'headers' => [
                'Authorization' => 'Bearer ' . $request->input('access_token')
                ],
            'verify' => false
            ]
        );

        $contents = json_decode($response->getBody()->getContents());
        $contentsString = (string) $response->getBody();

        //dd($contents);
        //dd($contents, $emails);

        $usuario = AdmUsuarios::where('email', $contents->mail)->get()->first();

        if (!$usuario || $usuario->status == 0)
        {
            //dd('Usuário não cadastrado', $contents);

            return redirect('/')
            ->withErrors('O usuário com o e-mail '.$contents->mail.' ainda não foi cadastrado no Rubrum. Favor entrar em contato com o Administrador e solicitar o seu cadastro no sistema.')
            ->with('sso', false);
        }

        $usuario->nome = $contents->givenName;
        $usuario->sobrenome = $contents->surname;
        $usuario->access_token = $request->input('access_token');
        $usuario->expires_in = $request->input('expires_in');
        //$usuario->senha = bcrypt($request->input('access_token'));
        //$usuario->status = 1;
        $usuario->primeiro_acesso = 0;
        $usuario->expirado_em = null;
        $usuario->arquivado_em = null;
        $usuario->tem_token = 0;
        $usuario->save();

        $credentials = [
	        'email' => $usuario->email,
	        'password' => $usuario->access_token,
	        'status' => 1
	    ];

        // verifica se existem sessões abertas para esse usuário e deleta
        $sessions = Sessions::where('user_id', $usuario->id)->delete();
        Auth::loginUsingId($usuario->id);

        if(Auth::check()){
        //if(Auth::attempt($credentials, 0)){

            // log login
            Auth::user()->logged_at = date('Y-m-d H:i:s');
            Auth::user()->save();

            $usuarioLogin = new AdmUsuariosLogins();
            $usuarioLogin->id_usuario = Auth::user()->id;
            $usuarioLogin->email = Auth::user()->email;
            $usuarioLogin->type = 'sso';
            $usuarioLogin->access_token = Auth::user()->access_token;
            $usuarioLogin->client_ip = $request->getClientIp();
            $usuarioLogin->contents = $contentsString;
            $usuarioLogin->save();
            
			//$request->session()->put('news', 0);
	    	return redirect()->intended('dashboard');
	    }
	    else
        {
            //dd('Usuário não cadastrado', $contents);

            return redirect('/')
            ->withErrors('Você ainda não foi cadastrado no Rubrum. Favor entrar em contato com o Administrador e solicitar o seu cadastro no sistema.')
            ->with('sso', false);
	    }
        
    }
    
    public function getLoginToken($token, Request $request)
    {
        $usuarioToken = AdmUsuariosTokens::where('token', $token)->get()->first();

        if (!$usuarioToken)
        {
            Auth::logout();
            return redirect('/')
                ->withErrors('Token inválido.')
                ->with('sso', false);
        }

        if (!$usuarioToken->admUsuario)
        {
            Auth::logout();
            return redirect('/')
                ->withErrors('Token inválido.')
                ->with('sso', false);
        }

        if (!$usuarioToken->admUsuario->tem_token)
        {
            Auth::logout();
            return redirect('/')
                ->withErrors('token-expirado')
                ->with('sso', false)
                ->with('id_usuario', $usuarioToken->admUsuario->id);
        }

        if ($usuarioToken->expira_em <= date('Y-m-d H:i:s'))
        {
            Auth::logout();
            return redirect('/')
                ->withErrors('token-expirado')
                ->with('sso', false)
                ->with('id_usuario', $usuarioToken->admUsuario->id);
        }

        Auth::loginUsingId($usuarioToken->admUsuario->id);

        //$usuarioToken->admUsuario->tem_token = 0;
        //$usuarioToken->admUsuario->save();

        // log login
        Auth::user()->logged_at = date('Y-m-d H:i:s');
        Auth::user()->save();

        $usuarioLogin = new AdmUsuariosLogins();
        $usuarioLogin->id_usuario = Auth::user()->id;
        $usuarioLogin->email = Auth::user()->email;
        $usuarioLogin->type = 'token';
        $usuarioLogin->access_token = $token;
        $usuarioLogin->client_ip = $request->getClientIp();
        $usuarioLogin->save();
        
        $request->session()->put('news', 0);

        return redirect('first');
    }

	public function getAdminExterno($token)
    {
		$user = AdmUsuarios::where('email', Redis::get('email'))->select(array('id', 'nome'))->first();
		Auth::loginUsingId($user->id);
		if(Auth::check())
			return redirect('dashboard');
		else
			return view('home');
    }

    public function postLogin(Request $request)
    {
    	$rules = array(
			'email' => 'required|email',
			'senha' => 'required|min:6',
		);

		$messages = array(
	    	'email.required' => 'Digite o Email',
	    	'email.email' => 'Formato de Email Incorreto',
	    	'senha.required' => 'Senha Obrigatória',
	    	'senha.min' => 'A Senha deve conter no Minimo 6 Caracteres',
    	);

    	$validator = Validator::make($request->only('email', 'senha'), $rules, $messages);

        if (!$validator->passes()) {
            return redirect('/')
                        ->withErrors($validator)
                        ->withInput();
        }

    	$access = $request->all();

        if (str_contains($request->email, config('app.sso_dominio')) && config('app.sso_name'))
        {
            $dominio = strstr($access['email'], '@');

            return redirect('/')
            //->withErrors('Seu login deve ser feito na '.config('app.sso_name').'. Você será redirecionado.')
            ->withErrors('Seu domínio de e-mail <b>'.$dominio.'</b> só pode acessar o sistema através da opção <b>Acesso com Conta '.config('app.sso_name').'</b>. Favor escolher essa opção para proceder com o acesso.')
            ->with('sso', true);
        }

	    $credentials = [
	        'email' => $access['email'],
	        'password' => $access['senha'],
	        'status' => 1
	    ];

	    if(Auth::attempt($credentials, 0)){

            if (Auth::user()->expirado_em)
            {
                $id_usuario = Auth::user()->id;

                Auth::logout();

                return redirect('/')
                    ->withErrors('usuario-expirado')
                    ->with('alert', true)
                    ->with('id_usuario', $id_usuario);
            }

            // verifica se existem sessões abertas para esse usuário e deleta
            $sessions = Sessions::where('user_id', Auth::user()->id)->delete();

            //dd($sessions);

            // log login
            Auth::user()->logged_at = date('Y-m-d H:i:s');
            Auth::user()->tem_token = 0;
            Auth::user()->expirado_em = null;
            Auth::user()->arquivado_em = null;
            Auth::user()->senha = bcrypt($access['senha']);
            Auth::user()->save();

            $usuarioLogin = new AdmUsuariosLogins();
            $usuarioLogin->id_usuario = Auth::user()->id;
            $usuarioLogin->email = Auth::user()->email;
            $usuarioLogin->type = 'login';
            $usuarioLogin->access_token = Auth::user()->senha;
            $usuarioLogin->client_ip = $request->getClientIp();
            $usuarioLogin->save();
            
			$request->session()->put('news', 0);
	    	if (Auth::user()->primeiro_acesso == 0)
	    		return redirect()->intended('dashboard');
	    	else
	    		return redirect('first');
	    }
	    else{
	    	return redirect('/')
                ->withErrors('Usuário ou senha inválidos. Verifique os dados de acesso e tente novamente.')
                ->withInput();
	    }
    }

    public function getFirst()
    {
    	return view('primeiro_acesso');
    }

    public function postFirst(Request $request)
    {
    	$rules = array(
	        'password' => 'required|min:6|confirmed',
	        'password_confirmation' => 'required'
	    );

	    $messages = array(
		    'password.required' => 'Senha Obrigatória',
		    'password.min' => 'A Senha deve conter no Minimo 6 Caracteres',
		    'password.confirmed' => 'Valores Digitados não Conferem',
		    'password_confirmation.required' => 'Confirmação Obrigatória'
	    );

        $validator = Validator::make($request->all(), $rules, $messages);

        if (!$validator->passes()) {
            return redirect('first')
                        ->withErrors($validator)
                        ->withInput();
        }

        $data = array(
        	'_token' => $request->_token,
        	'senha' => bcrypt($request->password),
        	'primeiro_acesso' => 0
    	);

    	$user = AdmUsuarios::find(Auth::user()->id);

        $user->tem_token = 0;
        $user->expirado_em = null;
        $user->arquivado_em = null;
        $user->termos_aceitos_em = date('Y-m-d H:i:s');
        $user->data_senha_em = date('Y-m-d H:i:s');

        $user->save();

    	if($user->update($data)){
            return redirect('dashboard');
    	}
    }

    public function getForgot()
    {
		// dd(env('SES_KEY'), env('SES_SECRET'));
		if(Auth::check()){
	    	return redirect('dashboard');
	    }
        return view('lembrar_senha');
    }

    public function gerarToken($tipo, $id_usuario)
    {
        $user = AdmUsuarios::find($id_usuario);
        $update = [];
        
        if($user){
            
            $token = str_random(60);
            $update['tem_token'] = ($tipo == 'cadastro' ? 1 : 2);
            $update['primeiro_acesso'] = 1;

            $data = array(
                'nome' => htmlentities($user->nome_abreviado),
                'email' => $user->email,
                'token' => $token
            );

            if($user->update($update)){

                if ($tipo == 'cadastro')
                {
                    $template = 'emails.cadastro';
                    $subject = 'Cadastro no Sistema';
                }
                else
                {
                    $template = 'emails.nova_senha';
                    $subject = 'Recuperar Acesso';
                }

                $usuarioToken = new AdmUsuariosTokens();
                $usuarioToken->id_usuario = $user->id;
                $usuarioToken->token = $token;
                $usuarioToken->tipo = ($tipo == 'cadastro' ? 1 : 2);
                $usuarioToken->expira_em = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' +1 day'));
                $usuarioToken->save();

                Mail::send($template, $data, function ($message) use($data, $subject) {
                    $message->from(config('app.mail_from'), config('app.mail_name'));
                    $message->to($data['email'])->subject($subject);
                });
            }
        }

        return response()->json(['success' => 200, 'message' => 'Link enviado com sucesso.'], 200);
    }

    public function postForgot(Request $request)
    {
        if (str_contains($request->email, config('app.sso_dominio')) && config('app.sso_name'))
        {
            $dominio = strstr($request->email, '@');

            return redirect('/')
            ->withErrors('Seu domínio de e-mail <b>'.$dominio.'</b>  não é gerenciado pelo Rubrum e sim pelo <b>Departamento de TI '.config('app.sso_name').'</b>. Para resolver problemas com acesso e senha, favor entrar em contato com eles.')
            ->with('sso', true);

        }

        $newPass = new Common;
        $user = AdmUsuarios::where('email', $request->email)->where('status', 1)->first();
        $update = array();

        if($user){
            $token = str_random(60);
            $update['tem_token'] = 2;
            $update['primeiro_acesso'] = 1;

            $data = array(
                'nome' => htmlentities($user->nome_abreviado),
                'email' => $request->email,
                'token' => $token
            );

            if($user->update($update)){

                $usuarioToken = new AdmUsuariosTokens();
                $usuarioToken->id_usuario = $user->id;
                $usuarioToken->token = $token;
                $usuarioToken->tipo = 2;
                $usuarioToken->expira_em = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' +1 day'));
                $usuarioToken->save();

                Mail::send('emails.nova_senha', $data, function ($message) use($data) {
                    //$message->from('ambev@paintpackred.com', 'Ambev Red');
                    $message->from(config('app.mail_from'), config('app.mail_name'));
                    $message->to($data['email'])->subject('Recuperar Acesso');
                });
            }
        }
        else {
            return redirect()->route("site.get.forgot")->withErrors('Email não cadastrado. Verifique e tente novamente!');
        }

        return redirect()->route("site.login")->with('success', 'As instruções para recuperar seu acesso foram enviadas para o e-mail '.$request->email.'.')->with('email', $request->email);
    }
    
    public function getLogout()
    {
        Auth::logout();
        return redirect()->to('/');
    }
    
    public function manager()
    {
        Redis::set('email', Auth::user()->email);
        Redis::expire('email', 10);
        Auth::logout();
        return Redirect::to(getenv('ACESSO_MANAGER') . '/externo/' . getenv('ADMIN_TOKEN'));
    }

}

