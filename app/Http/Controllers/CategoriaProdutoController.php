<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CategoriasProdutos;
use App\Produtos;
use App\Logotipos;
use App\Components\Common;
use Illuminate\Support\Facades\File;

use App\Http\Requests;

class CategoriaProdutoController extends Controller
{
    private $categoria;

    public function __construct(CategoriasProdutos $categoria)
    {
    	$this->categoria = $categoria;
    }

    public function index()
    {
    	$categorias = $this->categoria->all();

        $total = count($categorias);

    	return view('admin.categorias-produtos.index', compact('categorias', 'total'));
    }

    public function search($name)
    {
    	$categorias = $this->categoria->where('nome', 'like', '%'.$name.'%')->get();

        $total = count($categorias);

    	return view('admin.categorias-produtos.index', compact('categorias', 'total'));
    }

    public function create(Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

    	$category = $this->categoria;
    	$category->create($request->all());

    	return redirect()->route("admin.product-category");
    }

    public function edit($id)
    {
    	$editCategory = $this->categoria->find($id);

    	return response(compact('editCategory'));
    }

    public function update($id, Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

    	$category = $this->categoria->find($id);
    	$category->update($request->all());
    }

    public function remove($id, Request $request)
    {
        $remove = new Common;
        $removeCategory = $this->categoria->find($id);

        foreach ($removeCategory->produtos as $produto){
            $remove->removeProduct($produto->id);
        }

    	$removeCategory->delete();
    }

}
