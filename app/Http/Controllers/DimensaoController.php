<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dimensoes;
use App\TiposEmbalagens;
use App\Components\Common;
use Illuminate\Support\Facades\File;

use App\Http\Requests;

class DimensaoController extends Controller
{
    private $dimensao;

    public function __construct(Dimensoes $dimensao)
    {
    	$this->dimensao = $dimensao;
    }

    public function index()
    {
    	$allDimensoes = $this->dimensao->orderBy('id_tipo_embalagem')->orderBy('nome')->get();
    	$allTiposEmbalagens = TiposEmbalagens::where('id', '<>' , '1')->where('status', 1)->orderBy('nome')->get();
        $paginate = new Common;
    	$dimensoes = null;
        $tiposEmbalagens = [];

        $total = count($allDimensoes);

    	foreach ($allDimensoes as $dimensao) {
    		$dimensao->tiposEmbalagens = $dimensao->tiposEmbalagens;
    		$dimensoes[] = $dimensao;
    	}

    	foreach ($allTiposEmbalagens as $value) {
    		$tiposEmbalagens[$value->id] = $value->nome;
    	}

        if(count($allDimensoes) == 0){
			$count = [];
		}
		else{
			foreach (range(1, count($allDimensoes)) as $number) {
			    $count[] = $number;
			}
		}

        //$dimensoes = $paginate->paginate($dimensoes);

    	return view('admin.dimensoes.index', compact('dimensoes', 'tiposEmbalagens', 'total', 'count'));
    }

    public function search($name)
    {
    	$allDimensoes = $this->dimensao->where('id', '<>' , '1')
            ->orderBy('id_tipo_embalagem')->orderBy('nome')->get();
    	$allTiposEmbalagens = TiposEmbalagens::where('id', '<>' , '1')->orderBy('nome')->get();
        $paginate = new Common;
        $dimensoes = null;

        $total = count($allDimensoes);

    	foreach ($allDimensoes as $dimensao) {
    		$dimensao->tiposEmbalagens = $dimensao->tiposEmbalagens;
    		$dimensoes[] = $dimensao;
    	}

    	foreach ($allTiposEmbalagens as $value) {
    		$tiposEmbalagens[$value->id] = $value->nome;
    	}

        $dimensoes = $paginate->paginate($dimensoes);

    	return view('admin.dimensoes.index', compact('dimensoes', 'tiposEmbalagens', 'total'));
    }

    public function create(Request $request)
    {
        if(empty($request->nome) || empty($request->id_tipo_embalagem)){
            return false;
        }

        header('Content-type: application/json');

    	$dimensions = $this->dimensao;
        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;
        $imagePath = '/images/uploads/dimensoes/';

        $check = $this->dimensao->where(array('nome' => $dados['nome'], 'id_tipo_embalagem' => $dados['id_tipo_embalagem']))->get()->all();

        if (!empty($check)) {
            echo json_encode([
                'error' => true,
                'message' => 'Já existe esse volume para esse tipo de embalagem'
            ]);
            die;
        }

        // Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $image];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;
        }
        
        //$dados['status'] = ($request->input('status')) ? 1 : 0;

    	$dimensions->create($dados);

    	return redirect()->route("admin.dimensions");
    }

    public function edit($id)
    {
    	$editDimension = $this->dimensao->find($id);

    	return response(compact('editDimension'));
    }

    public function update($id, Request $request)
    {
        if(empty($request->nome) || empty($request->id_tipo_embalagem)){
            return false;
        }

        $dimension = $this->dimensao->find($id);
        $dados = $request->all();

        $image = isset($dados['image']) ? $dados['image'] : null;
        $imagePath = '/images/uploads/dimensoes/';

        // Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $image];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;

            if(File::isFile(public_path().$dimension->thumb)){
                File::delete(public_path().$dimension->thumb);
            }
        }

        //$dados['status'] = ($request->input('status')) ? 1 : 0;
        
        //$dados['ordem'] = $dados['ordem'] + 1;

    	$dimension->update($dados);
    }

    public function remove($id, Request $request)
    {
        $removeSku = new Common;
    	$removeDimension = $this->dimensao->find($id);

        foreach ($removeDimension->skus as $sku) {
            $removeSku->removeSku($sku->id);
        }

    	$removeDimension->delete();
    }

}
