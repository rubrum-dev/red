<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TiposEmbalagens;
use App\NaturezaEmbalagens;
use App\Dimensoes;
use App\Components\Common;

use App\Http\Requests;

class TipoEmbalagemController extends Controller
{
    private $embalagem;

    public function __construct(TiposEmbalagens $embalagem)
    {
    	$this->embalagem = $embalagem;
    }

    public function index()
    {
    	$allEmbalagens = $this->embalagem
            ->where('id', '<>' , '1')
            ->orderBy('nome')
            ->get();
        
    	$allNatureza = NaturezaEmbalagens::where('id', '<>' , '1')->get();
        $embalagens = null;

        $total = count($allEmbalagens);

        if(count($allEmbalagens) == 0){
			$count = [];
		}
		else{
			foreach (range(1, count($allEmbalagens)) as $number) {
			    $count[] = $number;
			}
		}

    	foreach ($allEmbalagens as $embalagem) {
    		$embalagem->natureza = $embalagem->naturezaEmbalagens;
    		$embalagens[] = $embalagem;
    	}

    	foreach ($allNatureza as $value) {
    		$natureza[$value->id] = $value->nome;
    	}

    	return view('admin.embalagens.index', compact('embalagens', 'natureza', 'count', 'total'));
    }

    public function search($name)
    {
    	$allEmbalagens = $this->embalagem->where('nome', 'like', '%'.$name.'%')->where('id', '<>' , '1')->get();
    	$allNatureza = NaturezaEmbalagens::where('id', '<>' , '1')->get();
        $embalagens = null;

        $total = count($allEmbalagens);

        if(count($allEmbalagens) == 0){
			$count = [];
		}
		else{
			foreach (range(1, count($allEmbalagens)) as $number) {
			    $count[] = $number;
			}
		}

    	foreach ($allEmbalagens as $embalagem) {
    		$embalagem->natureza = $embalagem->naturezaEmbalagens;
    		$embalagens[] = $embalagem;
    	}

    	foreach ($allNatureza as $value) {
    		$natureza[$value->id] = $value->nome;
    	}

    	return view('admin.embalagens.index', compact('embalagens', 'natureza', 'count', 'total'));
    }

    public function create(Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

        $data = $request->all();
        $data['ordem'] = (int)$data['ordemCheck'] + 1;

        //$data['status'] = ($request->input('status')) ? 1 : 0;

		$data['id_natureza_embalagem'] = 2;

		//dd($data);
        
    	$package = $this->embalagem;
    	$package->create($data);

    	return redirect()->route("admin.package");
    }

    public function edit($id)
    {
    	$editPackage = $this->embalagem->find($id);
    	$editPackage->natureza = $editPackage->naturezaEmbalagens;

    	return response(compact('editPackage'));
    }

    public function update($id, Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

        $package = $this->embalagem->find($id);
        $data = $request->all();
        $data['ordem'] = $data['ordem'] + 1;
        
        //$data['status'] = ($request->input('status')) ? 1 : 0;

        // Organiza a ordem de exibição conforme selecionado no item atual
        /*
		$package->ordem = !is_null($package->ordem) ? $package->ordem : $this->embalagem->max('ordem') + 1;
		if($data['ordem'] != $package->ordem){
			if($data['ordem'] < $package->ordem){
				$ordemPackages = $this->embalagem->where('ordem', '>=', $data['ordem'])->where('ordem', '<', $package->ordem)->get();
				foreach ($ordemPackages as $value) {
					$novaOrdem = $value->ordem + 1;
					$value->update(['ordem' => $novaOrdem]);
				}
			}
			else{
				$ordemPackages = $this->embalagem->where('ordem', '<=', $data['ordem'])->where('ordem', '>', $package->ordem)->get();
				foreach ($ordemPackages as $value) {
					$novaOrdem = $value->ordem - 1;
					$value->update(['ordem' => $novaOrdem]);
				}
			}

		}
        */

    	$package->update($data);
    }

    public function remove($id, Request $request)
    {
        $removeSku = new Common;
        $removeDimension = new Dimensoes;
    	$removePackage = $this->embalagem->find($id);

        $ordemPackage = $removePackage->ordem;

        foreach ($removePackage->dimensoes as $dimensao) {
            $dimension = $removeDimension->find($dimensao->id);
            foreach ($dimension->skus as $sku) {
                $removeSku->removeSku($sku->id);
            }
            $dimension->delete();
        }

        $reordenarPackages = $this->embalagem->where('ordem', '>', $ordemPackage)->get();
		foreach ($reordenarPackages as $value) {
			$novaOrdem = $value->ordem - 1;
			$value->update(['ordem' => $novaOrdem]);
		}

    	$removePackage->delete();
    }

}
