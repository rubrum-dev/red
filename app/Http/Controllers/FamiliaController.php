<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Familias;
use App\TiposLogotipos;
use App\Logotipos;
use App\Produtos;
use App\Components\Common;
use Illuminate\Support\Facades\File;

class FamiliaController extends Controller
{
	private $familia;

    public function __construct(Familias $familia)
    {
    	$this->familia = $familia;
    }

    public function index()
    {
    	$familias = $this->familia->orderBy('nome')->get();
    	$tiposLogos = TiposLogotipos::all();
		$paginate = new Common;
		$count = null;

		$total = count($familias);

		if(count($familias) == 0){
			$count = [];
		}
		else{
			foreach (range(1, count($familias)) as $number) {
			    $count[] = $number;
			}
		}

		//$familias = $paginate->paginate($familias);

    	return view('admin.familias.index', compact('familias', 'tiposLogos', 'count', 'total'));
    }

    public function search($name)
    {
    	$familias = $this->familia->get();
		$allFamilias = $this->familia->where('status', '1')->get();
        $tiposLogos = TiposLogotipos::all();
		$paginate = new Common;
		$count = null;

		$total = count($familias);

		if(count($allFamilias) == 0){
			$count = [];
		}
		else{
			foreach (range(1, count($allFamilias)) as $number) {
			    $count[] = $number;
			}
		}

		//$familias = $paginate->paginate($familias);

    	return view('admin.familias.index', compact('familias', 'tiposLogos', 'count', 'total'));
    }

    public function create(Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

		$files = $request->all();

		$familia = $this->familia->where('nome', $files['nome'])->get();
		if(count($familia) > 0)
			return array('error' => true, 'message' => 'Já existe uma família com esse nome. Verifique e tente novamente.');

		$images = isset($files['images']) ? $files['images'] : null;
        $relativePath = '/images/uploads/logotipos/';

		$files['ordem'] = (int)$files['ordemCheck'] + 1;

		// Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $images];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

		$family = $this->familia;
        
        //$files['status'] = ($request->input('status')) ? 1 : 0;
        
		$family = $family->create($files);

        if(!is_null($images)){
            $logotipo = new Logotipos;
            $extension = $images->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $filename = $hash.".".$extension;
            $destinationPath = public_path().$relativePath;
            $upload_success = $images->move($destinationPath, $filename);

            $logo = array(
                'thumb' => $relativePath.$filename,
                'id_tipo_logotipo' => 1,
                'status' => 1
            );

			$family->logotipos()->attach($logotipo->create($logo));
        }

		$family->admPerfis()->attach(array(4));

    	return redirect()->route("admin.family");
    }

    public function edit($id)
    {
        $editFamily = $this->familia->find($id);
        $editFamily->logotipos;

        return response(compact('editFamily'));
    }

    public function update($id, Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

        $family = $this->familia->find($id);
        $logos = $family->logotipos;
        $files = $request->all();

		$familia = $this->familia->where('id', '<>', $files['id'])->where('nome', $files['nome'])->get();
		if(count($familia) > 0)
			return array('error' => true, 'message' => 'Já existe uma família com esse nome. Verifique e tente novamente.');

		$files['ordem'] = $files['ordem'] + 1;
        $images = isset($files['images']) ? $files['images'] : null;
        $relativePath = '/images/uploads/logotipos/';

		// Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $images];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

        if(!is_null($images)){
            $logotipo = new Logotipos;
            $extension = $images->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $filename = $hash.".".$extension;
            $destinationPath = public_path().$relativePath;
            $upload_success = $images->move($destinationPath, $filename);

            $logoThumbPath = isset($logos[0]->thumb) ? $logos[0]->thumb : null ;
            $logoRemoveId = isset($logos[0]->id) ? $logos[0]->id : null ;

            if($logoThumbPath && File::isFile(public_path().$logoThumbPath)){
                File::delete(public_path().$logoThumbPath);
            }

            $logo = array(
                'thumb' => $relativePath.$filename,
                'id_tipo_logotipo' => 1,
                'status' => 1
            );

			if($logoRemoveId){
				$family->logotipos()->detach($logoRemoveId);
				$removeLogo = $logotipo->find($logoRemoveId);
				$removeLogo->delete();
			}

			$family->logotipos()->attach($logotipo->create($logo));
        }

		// Organiza a ordem de exibição conforme selecionado no item atual
		$family->ordem = !is_null($family->ordem) ? $family->ordem : $this->familia->max('ordem') + 1;
		if($files['ordem'] != $family->ordem){
			if($files['ordem'] < $family->ordem){
				$ordemFamilias = $this->familia->where('ordem', '>=', $files['ordem'])->where('ordem', '<', $family->ordem)->get();
				foreach ($ordemFamilias as $value) {
					$novaOrdem = $value->ordem + 1;
					$value->update(['ordem' => $novaOrdem]);
				}
			}
			else{
				$ordemFamilias = $this->familia->where('ordem', '<=', $files['ordem'])->where('ordem', '>', $family->ordem)->get();
				foreach ($ordemFamilias as $value) {
					$novaOrdem = $value->ordem - 1;
					$value->update(['ordem' => $novaOrdem]);
				}
			}

		}

        //$files['status'] = ($request->input('status')) ? 1 : 0;
        
        $family->update($files);
    }

    public function remove($id)
    {
        $remove = new Common;
        $remove->removeFamily($id);
    }

}
