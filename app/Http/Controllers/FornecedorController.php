<?php

namespace App\Http\Controllers;

use App\Fornecedores;
use App\FornecedoresEmails;
use App\Components\SiteCommon;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class FornecedorController extends Controller
{
    public function __construct(SiteCommon $siteCommon)
    {
        $this->siteCommon = $siteCommon;
    }

    public function getFornecedores()
    {
        $fornecedores = Fornecedores::all();

        return view('site.fornecedores.gerenciar', compact('fornecedores'));
    }

    public function getFornecedoresAdicionar()
    {
        return view('site.fornecedores.adicionar');
    }

    public function getFornecedoresEditar(Fornecedores $fornecedores, $idFornecedor)
    {
        $fornecedor = $fornecedores->find($idFornecedor);

        $ids = [];
        $nomes = [];
        $emails = [];

        foreach ($fornecedor->fornecedoresEmails as $email) {
            $ids[] = $email->id;
            $nomes[] = $email->nome;
            $emails[] = $email->email;
        }

        return view('site.fornecedores.editar', compact('fornecedor', 'ids', 'nomes', 'emails'));
    }

    public function postFornecedoresAdicionar(Requests\PostFornecedorAdicionarForm $request)
    {
        DB::beginTransaction();

        $fornecedorNew = new Fornecedores();
        $fornecedorNew->nome = $request->input('nome');
        $fornecedorNew->telefone = $request->input('telefone');
        $fornecedorNew->status = $request->input('status');
        $fornecedorNew->save();

        foreach ($request->input('nomes') as $k => $nome) {
            $fornecedorEmail = new FornecedoresEmails();
            $fornecedorEmail->id_fornecedor = $fornecedorNew->id;
            $fornecedorEmail->email = $request->input('emails.' . $k);
            $fornecedorEmail->nome = $nome;
            $fornecedorEmail->save();
        }

        DB::commit();

        return redirect()
            ->route('site.fornecedores.get')
            ->with('success', ['Fornecedor salvo com sucesso.']);
    }

    public function postFornecedoresEditar(Requests\PostFornecedorEditarForm $request, Fornecedores $fornecedores, $idFornecedor)
    {
        $fornecedor = $fornecedores->find($idFornecedor);

        DB::beginTransaction();

        $fornecedor = $fornecedores->find($idFornecedor);
        $fornecedor->nome = $request->input('nome');
        $fornecedor->telefone = $request->input('telefone');
        $fornecedor->status = $request->input('status');
        $fornecedor->save();

        if ($request->input('excluidos')) {
            foreach ($request->input('excluidos') as $k => $excluido) {
                FornecedoresEmails::destroy($excluido);
            }
        }

        foreach ($request->input('nomes') as $k => $nome) {

            $fornecedorEmail = FornecedoresEmails::firstOrNew( ['id_fornecedor' => $fornecedor->id, 'email' => $request->input('emails.' . $k)] );
            $fornecedorEmail->nome = html_entity_decode($nome);
            $fornecedorEmail->save();
        }

        DB::commit();

        return redirect()
            ->route('site.fornecedores.get')
            ->with('success', ['Fornecedor salvo com sucesso.']);
    }

    public function getFornecedoresExcluir(Fornecedores $fornecedores, $idFornecedor)
    {
        $fornecedor = $fornecedores->find($idFornecedor);

        foreach ($fornecedor->fornecedoresEmails as $email)
        {
            $email->delete();
        }

        $fornecedor->delete();

        return redirect()
            ->route('site.fornecedores.get')
            ->with('success', ['Fornecedor excluído com sucesso.']);
    }
}
