<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Components\Common;

use App\AdsmartMidiaTipos;
use App\AdsmartCampanhaMidias;
use App\AdsmartPecas;
use App\AdsmartAprovadoresMidia;
use App\AdsmartCondicoesMidia;
use App\ArtwAprovacoes;
use App\ArtwCiclos;
use App\ArtwCiclosLayout;
use App\ArtwTickets;
use App\ArtwTktStatus;
use App\Familias;
use App\ArtwArquivos;
use Illuminate\Support\Facades\Storage;

class AdsmartServiceController extends Controller
{
    public function familias($id_categoria = null)
    {
        $familias = array();
        $allFamilias = Familias
            ::select('familias.id', 'familias.nome')
            ->distinct();

        if ($id_categoria) {
            $allFamilias = $allFamilias->where('produtos.id_categoria_produto', $id_categoria);
        }

        $allFamilias = $allFamilias->orderBy('familias.nome')->get();

        return $allFamilias;
    }


    public function ticketAcao(Request $request, Common $common)
    {
        if (!$request->input('id') || !$request->input('status')) {
            return response()->json(['error' => 400, 'message' => 'Parâmetros obrigatórios: id, status'], 404);
        }

        $ticket = ArtwTickets::find($request->input('id'));

        if (!$ticket) {
            return response()->json(['error' => 404, 'message' => 'Ticket Not found'], 404);
        }

        DB::beginTransaction();

        switch ($request->input('status')) {
            case 'aberto':
                //
            break;

            case 'anexacao':
            //
            break;

            case 'revisao':

                $id_perfil = 9;

                $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);

                if (!$perfilUsuarioTicket) {
                    return response()->json(['error' => 401, 'message' => 'Você não possui o perfil apropriado.'], 404);
                }

                $common->changeStatusTicket($ticket, '15');

            break;

            case 'aprovacao':

                $id_perfil = 10;

                $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);

                if (!$perfilUsuarioTicket) {
                    return response()->json(['error' => 401, 'message' => 'Você não possui o perfil apropriado.'], 404);
                }
                
                $common->changeStatusTicket($ticket, '16');

            break;

            case 'aprovar':
                
                $id_perfil = 11;

                $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);

                if (!$perfilUsuarioTicket) {
                    return response()->json(['error' => 401, 'message' => 'Você não possui o perfil apropriado.'], 404);
                }

                if ($perfilUsuarioTicket->aprovado == 2) {
                    $perfilUsuarioTicket->aprovado = 0;
                }

                $perfilUsuarioTicket->save();

                $pendentes = $ticket->participantesPendentes($id_perfil, $ticket->ciclo_atual()->numero);
                $reprovados = $ticket->participantesReprovados($id_perfil, $ticket->ciclo_atual()->numero);

                $libera_status = 0;

                $perfilUsuarioTicket->ciclo = $ticket->ciclo_atual()->numero;
                $perfilUsuarioTicket->ciclo_aprovacao = $ticket->ciclo_atual()->numero;
                $perfilUsuarioTicket->save();

                if ($pendentes->count() == 1) {
                    $libera_status = 1;
                }

                $artwAprovacao = ArtwAprovacoes::firstOrNew([
                    'id_ticket' => $ticket->id,
                    'id_usuario' => Auth::user()->id,
                    'ciclo' => $ticket->ciclo_atual()->numero,
                    'aprovado' => 1,
                    'id_perfil' => $id_perfil,
                    'descricao' => $request->input('descricao')
                ]);

                $artwAprovacao->save();

                if ($reprovados->count() > 0 && $pendentes->count() == 1 && $perfilUsuarioTicket->aprovado == 0) {

                    //dd('rotina de reprovar');

                    $common->reprovaTicketAdsmart($ticket, $id_perfil, $perfilUsuarioTicket);

                    DB::commit();

                    return response()->json(['success' => 200, 'message' => 'Ok'], 200);

                }

                if ($libera_status)
                {
                    $perfilUsuarioTicket->aprovado = 1;
                    $perfilUsuarioTicket->save();

                    $common->changeStatusTicket($ticket, '17');

                } else {

                    $perfilUsuarioTicket->aprovado = 1;
                    $perfilUsuarioTicket->save();

                    DB::commit();

                    return response()->json(['success' => 200, 'message' => 'Ok'], 200);

                }

            break;
        }

        DB::commit();

        return response()->json(['success' => 200, 'message' => 'Ok'], 200);
    }

    public function midiaTipos()
    {

        $midiaTipos = AdsmartMidiaTipos::where('status', 1)->orderBy('nome')->get();

        return $midiaTipos;

    }

    public function aprovadoresMidia()
    {
        $aprovadoresMidia = AdsmartAprovadoresMidia::orderBy('nome')->get();

        return $aprovadoresMidia;

    }

    public function condicoesMidia($tipo)
    {
        $aprovadoresMidia = AdsmartCondicoesMidia::orderBy('nome')->where('status', 1);

        if ($tipo == 'criar')
        {
            $aprovadoresMidia->where('abertura_ticket', 1);
        }

        if ($tipo == 'reprovar')
        {
            $aprovadoresMidia->where('reprovacao', 1);
        }

        return $aprovadoresMidia->get();

    }

    public function aprovadoresMidiaPorTipo($id_tipo_midia)
    {
        $aprovadoresMidia = AdsmartAprovadoresMidia
        ::select('adsmart_aprovadores_midia.*')
        ->join('adsmart_midia_tipos_aprovadores', 'adsmart_midia_tipos_aprovadores.id_aprovador_midia', '=', 'adsmart_aprovadores_midia.id')
        ->where('adsmart_midia_tipos_aprovadores.id_midia_tipo', $id_tipo_midia)
        ->orderBy('nome')->get();

        return $aprovadoresMidia;

    }

    public function midias($id_campanha)
    {
        $midiasAll = AdsmartCampanhaMidias
            ::select('adsmart_campanha_midias.*', 'adsmart_midia_tipos.nome')
            ->join('adsmart_midia_tipos', 'adsmart_campanha_midias.id_midia_tipo', '=', 'adsmart_midia_tipos.id')
            ->where('adsmart_campanha_midias.id_campanha', $id_campanha)
            ->get();

        $midias = [];

        foreach ($midiasAll as $midia)
        {
            $pecas = AdsmartPecas::where('id_campanha_midia', $midia->id)->where('status', 1)->orderBy('nome')->get();

            $arrayPecas = [];

            foreach ($pecas as $p => $peca)
            {
                if ($peca->artwTickets->last() && $peca->artwTickets->last()->id_status == 17)
                {
                    $ultimaAprovacao = $peca->artwTickets->last()->artwPerfisUsuariosTickets()->where('id_perfil', 11)->orderBy('updated_at')->get()->last();
                    $linkHistorico = route('site.adsmart.historico', $peca->artwTickets->last()->numero);

                } else {

                    $ultimaAprovacao = null;
                    $linkHistorico = null;

                }

                if (in_array('adsmart_criar_midia', Auth::user()->modulos))
                {
                    $permiteCriarMidia = true;
                } else {
                    $permiteCriarMidia = false;
                }

                if (in_array('adsmart_historico', Auth::user()->modulos))
                {
                    $permiteHistorico = true;
                } else {
                    $permiteHistorico = false;
                }

                if ($peca->artwTickets->last() && $peca->artwTickets->last()->id_status != 17 && ($peca->artwTickets->last()->checkUsuario(Auth::user()->id) || Auth::user()->admin || Auth::user()->gerente))
                {
                    $linkTicket = route('site.adsmart.ticket', $peca->artwTickets->last()->numero);

                } elseif ($peca->artwTickets->last() && $peca->artwTickets->last()->id_status != 17 && $peca->artwTickets->last()->id_usuario == Auth::user()->id)
                {
                    $linkTicket = route('site.adsmart.ticket.membros', $peca->artwTickets->last()->numero);

                } else {

                    $linkTicket = null;

                }

                $arrayPecas[$p]['artes'] = [];

                if ($peca->artwTickets->last()->id_status == 17)
                {
                    $cicloLayouts = $peca->artwTickets->last()->ciclo_atual()->artwCiclosLayout;

                    foreach ($cicloLayouts as $k => $cicloLayout)
                    {
                        if ($cicloLayout && $cicloLayout->path) {

                            $caminhoLayout = $cicloLayout->path . '?' . $cicloLayout->id;
            
                        } elseif ($cicloLayout) {
            
                            $caminhoLayout = route('site.adsmart.abrir', [$cicloLayout->id, $cicloLayout->nome_arquivo]);
            
                        }

                        $arrayPecas[$p]['artes'][$k]['linkAbrir'] = $caminhoLayout;
                        $arrayPecas[$p]['artes'][$k]['linkBaixar'] = route('site.adsmart.baixar', [$cicloLayout->id, $cicloLayout->nome_arquivo]);
                        $arrayPecas[$p]['artes'][$k]['numero'] = $k+1;
                    }

                }

                

                $arrayPecas[$p]['id'] = $peca->id;
                $arrayPecas[$p]['nome'] = $peca->nome;
                $arrayPecas[$p]['numero_ticket_aberto'] = ($peca->artwTickets->last() && $peca->artwTickets->last()->id_status != 17) ? $peca->artwTickets->last()->numero : null;
                $arrayPecas[$p]['ticket_status'] = ($peca->artwTickets->last()) ? $peca->artwTickets->last()->id_status : null;
                $arrayPecas[$p]['ultima_aprovacao'] = isset($ultimaAprovacao) ? $ultimaAprovacao->updated_at->format('d/m/Y') : null;
                //$arrayPecas[$p]['arte'] = ($cicloLayout && $peca->artwTickets->last()->id_status == 17) ? $caminhoLayout : null;
                $arrayPecas[$p]['link_ticket_aberto'] = isset($linkTicket) ? $linkTicket : null;
                $arrayPecas[$p]['link_historico'] = isset($linkHistorico) ? $linkHistorico : null;
                $arrayPecas[$p]['permite_criar_midia'] = isset($permiteCriarMidia) ? $permiteCriarMidia : null;
                $arrayPecas[$p]['permite_historico'] = isset($permiteHistorico) ? $permiteHistorico : null;
            }

            $midias[] = [
                'id' => $midia->id,
                'nome' => $midia->nome,
                'pecas' => $arrayPecas
            ];
        }

        return $midias;

    }

    public function postPecaCriar(Request $request)
    {
        DB::beginTransaction();

        $peca = AdsmartPecas::firstOrNew(['nome' => $request->input('nome'), 'id_campanha_midia' => $request->input('id_campanha_midia')], 'status', 1);

        if (!$peca->exists || ($peca->exists && $peca->status == 0)) {

            $peca->status = 0;

            $peca->save();

            DB::commit();

            $url = route('site.adsmart.ticket.criar', [$peca->id, $request->input('id_familia')]);

            return response()->json(['success' => 200, 'message' => 'Midia criada com sucesso.', 'url' => $url], 200);

        } else {

            $url = '';
            return response()->json(['success' => 201, 'message' => 'Midia já existe.', 'url' => $url], 200);

        }

        //$url = route("site.artwork.package", [$variacao->sku->produtos->familias->id, $variacao->sku->produtos->id]);
    }

    public function postMidiaExcluir(Request $request)
    {
        $midia = AdsmartCampanhaMidias::find($request->input('id_midia'));

        DB::beginTransaction();

        $midia->delete();

        DB::commit();

        return response()->json(['success' => 200, 'message' => 'Peça excluída com sucesso.'], 200);
    }

    public function postPecaExcluir(Request $request)
    {
        $peca = AdsmartPecas::find($request->input('id_peca'));

        DB::beginTransaction();

        $peca->delete();

        DB::commit();

        return response()->json(['success' => 200, 'message' => 'Peça excluída com sucesso.'], 200);
    }

    public function postPecaEditar(Request $request)
    {
        DB::beginTransaction();

        $peca = AdsmartPecas::find($request->input('id'));
        $peca->nome = $request->input('nome');
        $peca->save();
        DB::commit();
        $url = '';
        return response()->json(['success' => 200, 'message' => 'Midia criada com sucesso.', 'url' => $url], 200);
    }

    public function aprovadorMidia($id_grupo)
    {
        $grupo = AdsmartAprovadoresMidia::find($id_grupo);

        $usuarios = $grupo->usuarios;

        $selecionados = [];

        foreach ($usuarios as $usuario)
        {
            $selecionados[] = [
                'id' => $usuario->id,
                'nome' => $usuario->nome_abreviado,
                'departamento' => $usuario->admDepartamento->nome,
                'empresa' => $usuario->admEmpresa->nome,
            ];
        }

        return $selecionados;

    }

    public function uploadArquivo(Request $request, Common $common)
    {
        $ticket = ArtwTickets::find($request->input('id'));

        if (!$ticket) {
            return response()->json(['error' => 404, 'message' => 'Not found'], 404);
        }

        DB::beginTransaction();

        $files = $request->file('files');

        $relativePath = '/images/uploads/adsmart/arquivos/';

        foreach ($files as $file) {
            $filename = $file->getClientOriginalName();
            $filename = $common->verificaNomeArquivo($relativePath, $filename);

            $destinationPath = public_path() . $relativePath;
            $file->move($destinationPath, $filename);

            $arquivo = new ArtwArquivos();
            $arquivo->id_ticket = $ticket->id;
            $arquivo->id_usuario = Auth::user()->id;
            $arquivo->path = $relativePath . $filename;
            $arquivo->descricao = $filename;
            $arquivo->nome_original = $filename;
            $arquivo->save();
        }

        DB::commit();

        return response()->json(['success' => 200, 'message' => 'Ok'], 200);
    }

    public function upload(Request $request)
    {
        $file = $request->file('file1');

        $relativePath = '/images/uploads/adsmart/fotos/';
        $extension = $file->getClientOriginalExtension();
        $hash = md5(uniqid(rand(), true));
        $filename = $hash . "." . $extension;

        $destinationPath = public_path() . $relativePath;
        $file->move($destinationPath, $filename);

        $url = $relativePath . $filename;

        return response()->json(['url' => asset($url), 'success' => 200, 'message' => 'Ok'], 200);
    }

    public function uploadLayout(Request $request)
    {
        $ciclo = ArtwCiclos::find($request->input('id'));

        if (!$ciclo) {
            return response()->json(['error' => 404, 'message' => 'Not found'], 404);
        }

        DB::beginTransaction();

        $arquivos = $ciclo->artwCiclosLayoutAll;

        $file = $request->file('file0');

        $relativePath = '/images/uploads/adsmart/ciclo/';

        $extension = $file->getClientOriginalExtension();

        $nome_arquivo = $ciclo->artwTicket->adsmart_nome_completo . ' • ' . strtoupper($extension);
        $nome_arquivo2 = $ciclo->artwTicket->adsmart_nome_completo;

        $nome_arquivo .= ' - Ciclo ' . $ciclo->numero;
        $nome_arquivo2 .= ' - Ciclo ' . $ciclo->numero;

        $numero = (count($arquivos)) ? count($arquivos) + 1 : 1;

        $filename = $ciclo->artwTicket->numero . '-' . str_slug($nome_arquivo2) . "-peca-" . $numero . "." . $extension;

        //$destinationPath = public_path() . $relativePath;
        //$file->move($destinationPath, $filename);

        Storage::disk(config('app.storage'))->put(config('app.costummer_name') . '/adsmart/ciclo/' . $filename, file_get_contents($file));
        $size = Storage::disk(config('app.storage'))->size(config('app.costummer_name') . '/adsmart/ciclo/' . $filename);

        $cicloLayout = new ArtwCiclosLayout;
        $cicloLayout->id_ciclo = $ciclo->id;
        $cicloLayout->id_usuario = Auth::user()->id;
        $cicloLayout->nome = $nome_arquivo;
        $cicloLayout->nome_arquivo = $filename;
        //$cicloLayout->path = $relativePath . $filename;
        $cicloLayout->size = $size;
        $cicloLayout->storage = 1;
        $cicloLayout->save();

        /*
        if ($arquivos && $arquivos->last()) {

            if ($arquivos->last()->path && file_exists(public_path($arquivos->last()->path)))
            {
                unlink(public_path($arquivos->last()->path));
            }

            if (!$arquivos->last()->path && $arquivos->last()->nome_arquivo)
            {
                Storage::disk(config('app.storage'))->delete(config('app.costummer_name') . '/adsmart/ciclo/' . $arquivos->last()->nome_arquivo);
            }

            $arquivos->last()->delete();
        }
        */

        DB::commit();

        /*
        if (config('app.cloudflow')) {

            // Manda renderizar
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, route('api.render', ['ciclo', $cicloLayout->id]));
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_TIMEOUT_MS, 500);

            curl_exec($ch);

            if (curl_errno($ch)) {
                $error_message = curl_error($ch);
                //echo "<br>erreur:" . $error_message;
            }

            curl_close($ch);
        }
         * 
         */

        return response()->json(['success' => 200, 'message' => 'Ok'], 200);
    }

    public function deleteLayout(Request $request)
    {
        DB::beginTransaction();
        
        $arquivo = ArtwCiclosLayout::find($request->input('id'));

        if ($arquivo->path && file_exists(public_path($arquivo->path)))
        {
            unlink(public_path($arquivo->path));
        } else {
            Storage::disk(config('app.storage'))->delete(config('app.costummer_name') . '/adsmart/ciclo/' . $arquivo->nome_arquivo);
        }

        $arquivo->delete();

        DB::commit();

        return response()->json(['success' => 200, 'message' => 'Ok'], 200);
    }
    
}




