<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Icones;
use App\CategoriasIcones;
use Illuminate\Support\Facades\File;

use App\Http\Requests;

class CategoriaIconeController extends Controller
{
    private $categoria;

    public function __construct(CategoriasIcones $categoria)
    {
    	$this->categoria = $categoria;
    }

    public function index()
    {
    	$categorias = CategoriasIcones::all();

    	return view('admin.categorias-icones.index', compact('categorias'));
    }

    public function search($name)
    {
    	$categorias = $this->categoria->where('nome', 'like', '%'.$name.'%')->get();

    	return view('admin.categorias-icones.index', compact('categorias'));
    }

    public function create(Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

    	$category = $this->categoria;
    	$category->create($request->all());

    	return redirect()->route("admin.icon-category");
    }

    public function edit($id)
    {
    	$editCategory = $this->categoria->find($id);

    	return response(compact('editCategory'));
    }

    public function update($id, Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

    	$category = $this->categoria->find($id);
    	$category->update($request->all());
    }

    public function remove($id, Request $request)
    {
    	$icon = new Icones;
        $removeCategory = $this->categoria->find($id);

        foreach ($removeCategory->icones as $icone){
            $tmpIcon = $icon->find($icone->id);
            $tmpIcon->produtos()->detach();

            if(File::isFile(public_path().$icone->thumb)){
                File::delete(public_path().$icone->thumb);
            }
            if(File::isFile(storage_path().$icone->path_down)){
                File::delete(storage_path().$icone->path_down);
            }

        	$tmpIcon->delete();
        }

    	$removeCategory->delete();
    }
}
