<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produtos;
use App\TiposCores;
use App\NiveisCores;
use App\Components\ProductOutfit;
use App\Components\ProductColors;
use App\Components\ProductTypography;
use App\Components\ProductMisuse;
use App\Components\ProductApplications;
use App\Components\ProductVersions;
use App\Components\ProductProportions;
use App\Components\ProductIcons;

use App\Http\Requests;

class RelacaoProdutoController extends Controller
{
    private $outfit;
    private $color;
    private $typography;
    private $misuse;
    private $application;
    private $version;
    private $proportion;
    private $icon;

    public function __construct(ProductOutfit $outfit, ProductColors $color, ProductTypography $typography, ProductMisuse $misuse, ProductApplications $application, ProductVersions $version, ProductProportions $proportion, ProductIcons $icon)
    {
        $this->outfit = $outfit;
    	$this->color = $color;
        $this->typography = $typography;
        $this->misuse = $misuse;
        $this->application = $application;
        $this->version = $version;
        $this->proportion = $proportion;
        $this->icon = $icon;
    }

    public function getHome($attr, $id)
    {
    	$get = 'get'.ucfirst($attr);
    	return $this->$attr->$get($id);
    }

	public function postHome($attr, $id, Request $request)
    {
		$post = 'post'.ucfirst($attr);
    	return $this->$attr->$post($request);
    }

    public function create($attr, $prodId, Request $request)
    {
        return $this->$attr->create($request);
    }

    public function edit($attr, $id)
    {
        return $this->$attr->edit($id);
    }

    public function update($attr, $id, Request $request)
    {
        return $this->$attr->update($request);
    }

    public function remove($attr, $id)
    {
        return $this->$attr->remove($id);
    }
}
