<?php

namespace App\Http\Controllers;

use App\Promocoes;
use App\Produtos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

use App\Http\Requests;

class PromocaoController extends Controller
{
    private $promocao;

    public function __construct(Promocoes $promocao)
    {
    	$this->promocao = $promocao;
    }

    public function index()
    {
        $promocoes = array();
        $produtos = array();
        $allPromocoes = $this->promocao->orderBy('nome')->get();
        $allProdutos = Produtos::orderBy('nome')->get();
        foreach ($allPromocoes as $promocao) {
            $expiracao = date_create($promocao->dt_expiracao);
            $promocao->dt_expiracao = date_format($expiracao, 'd/m/Y');
            $promocoes[] = $promocao;
        }
        foreach ($allProdutos as $value) {
    		$produtos[$value->id] = $value->nome;
    	}
        $total = count($promocoes);
    	return view('admin.promocoes.index', compact('promocoes', 'produtos', 'total'));
    }

    public function search($name)
    {
        $promocoes = array();
        $produtos = array();
        $allPromocoes = $this->promocao->where('nome', 'like', '%'.$name.'%')->orderBy('nome')->get();
        $allProdutos = Produtos::orderBy('nome')->get();
        foreach ($allPromocoes as $promocao) {
            $expiracao = date_create($promocao->dt_expiracao);
            $promocao->dt_expiracao = date_format($expiracao, 'd/m/Y');
            $promocoes[] = $promocao;
        }
        foreach ($allProdutos as $value) {
    		$produtos[$value->id] = $value->nome;
    	}
        $total = count($promocoes);
    	return view('admin.promocoes.index', compact('promocoes', 'produtos', 'total'));
    }

    public function create(Request $request)
    {
        if(empty($request->nome) || empty($request->id_produto) || empty($request->image)){
            return false;
        }

        $promocao = $this->promocao;
        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;
        $imagePath = '/images/uploads/promocoes/';


        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;
        }

        $date = str_replace('/', '-', $dados['dt_expiracao']);
        $expiracao = date_create($date);
        $dados['dt_expiracao'] = date_format($expiracao, 'Y-m-d');

        if($promocao->create($dados))
            return array('error' => false);
        else
            return array('error' => true, 'message' => 'Erro ao criar o promoção. Tente novamente mais tarde.');
    }

    public function edit($id)
    {
        $editPromocao = $this->promocao->find($id);
        $expiracao = date_create($editPromocao->dt_expiracao);
        $editPromocao->dt_expiracao = date_format($expiracao, 'd/m/Y');
        return response(compact('editPromocao'));
    }

    public function update($id, Request $request)
    {
        if(empty($request->nome) || empty($request->id_produto) || empty($request->image)){
            return false;
        }

        $promocao = $this->promocao->find($id);
        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;
        $imagePath = '/images/uploads/promocoes/';

        $date = str_replace('/', '-', $dados['dt_expiracao']);
        $expiracao = date_create($date);
        $dados['dt_expiracao'] = date_format($expiracao, 'Y-m-d');

        if(!is_null($image)){
            if(File::isFile(public_path().$promocao->thumb)){
                File::delete(public_path().$promocao->thumb);
            }

            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;
        }

        $promocao->update($dados);
    }

    public function remove($id)
    {
    	$promocao = $this->promocao->find($id);
        $promocao->skus()->detach();        
        if(File::isFile(public_path().$promocao->path)){
            File::delete(public_path().$promocao->path);
        }
        $promocao->delete();
    }
}
