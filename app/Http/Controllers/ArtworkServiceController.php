<?php
namespace App\Http\Controllers;

use Illuminate\Support\Str;
use App\CategoriasProdutos;
use App\TiposEmbalagens;
use App\ArtwItemTipos;
use App\Dimensoes;
use App\Familias;
use App\Produtos;
use App\Campanhas;
use App\Skus;
use App\AdmUsuarios;
use App\ArtwTickets;
use App\ArtwComentarios;
use App\ArtwRespostas;
use App\ArtwSolicitacoes;
use App\ArtwArquivos;
use App\ArtwArtes;
use App\ArtwArtesEnvios;
use App\ArtwArtesEnviosEmails;
use App\ArtwCiclos;
use App\ArtwCiclosLayout;
use App\ArtwTktStatus;
use App\ArtwTicketsStatus;
use App\ArtwSolicitacoesStatus;
use App\ArtwSolicitacaoStatus;
use App\ArtwSolicitacaoTipos;
use App\ArtwVersoes;
use App\ArtwVariacoes;
use App\ArtwPerfisUsuariosEquipeTickets;
use App\ArtwPerfisUsuariosTickets;
use App\AdmPerfis;
use App\AdmUsuariosPerfis;
use App\ArtwAprovacoes;
use App\Fornecedores;
use App\ArtwItens;
use App\ArtwVariacaoItens;
use App\ArtwMercados;
use App\ArtwFornecedorPdfs;
use App\ArtwGruposPerfis;
use App\ArtwProjetos;
use Mail;
use Illuminate\Http\Request;
use App\Http\Requests\Artwork;
use Illuminate\Support\Facades\Auth;
use App\Components\Common;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Collection;
use Yajra\Datatables\Facades\Datatables;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\DNSCheckValidation;
use Egulias\EmailValidator\Validation\MultipleValidationWithAnd;
use Egulias\EmailValidator\Validation\RFCValidation;
use Illuminate\Support\Facades\Storage;

class ArtworkServiceController extends Controller
{

    public function getItem($id_item, $embalagens, Common $common)
    {
        $item = ArtwItens::find($id_item);

        $retorno = $item->toArray();

        $retorno['tipo'] = $item->artwItemTipo;
        $retorno['numero'] = $item->numero;

        $retorno['pai'] = null;

        if ($item->pai && $item->pai->artwVersoes->count())
        {
            if ($item->numero_ticket)
            {
                $ticket = ArtwTickets::where('numero', $item->numero_ticket)->get()->last();

                if ($ticket->cancelado_em)
                {

                    $linkHistorico = route('site.artwork.ticket.edit.step1.get', $ticket->numero);
                    $versaoNumero = "Cancelada";

                } else {

                    $versaoPai = $ticket->artwVersoes->last();
                    $linkHistorico = ($versaoPai) ? route('site.artwork.history', [$item->pai->artwVariacao->id, $versaoPai->id]) : null;
                    $versaoNumero = $versaoPai->numero;

                }

            } else {

                $versaoPai = $item->pai->artwVersoes->last();
                $linkHistorico = ($versaoPai) ? route('site.artwork.history', [$item->pai->artwVariacao->id, $versaoPai->id]) : null;
                $versaoNumero = $versaoPai->numero;

            }

            $retorno['pai']['id'] = $item->pai->id;
            $retorno['pai']['nome_original'] = $item->pai->artwVariacao->nome_original . ' • ' . $item->pai->artwItemTipo->nome;
            $retorno['pai']['versao'] = $versaoNumero;
            $retorno['pai']['link_historico'] = $linkHistorico;
        } else {

            if ($item->artwVersoes->count() && !$item->id_pai)
            {
                $versao = $item->artwVersoes->last();
                $linkHistorico = ($versao) ? route('site.artwork.history', [$item->artwVariacao->id, $versao->id]) : null;
                $versaoNumero = $versao->numero;

                $retorno['pai']['id'] = $item->id;
                $retorno['pai']['nome_original'] = $item->artwVariacao->nome_original . ' • ' . $item->artwItemTipo->nome;
                $retorno['pai']['versao'] = $versaoNumero;
                $retorno['pai']['link_historico'] = $linkHistorico;
            }

        }
        
        $compartilhadas = $common->getTodasEmbalagensPorItem($id_item);

        foreach ($compartilhadas as $compartilhada)
        {
            $retorno['compartilhadas'][] = [
                'id' => $compartilhada['id'],
                'nome_original' => $compartilhada['nome_original'],
            ];
        }

        //$retorno['compartilhadas'] = $common->getTodasEmbalagensPorItem($id_item);
        
        if ($embalagens) {
            
            $embalagens = $common->getTodasEmbalagens();

            foreach ($embalagens as $embalagem)
            {
                $retorno['embalagens'][] = [
                    'id' => $embalagem['id'],
                    'nome_original' => $embalagem['nome_original'],
                ];
            }
            
        } else {
            
            $retorno['embalagens'] = [];
            
        }
        
        if((in_array('artwork_compartilhar_item', Auth::user()->modulos))) {
            
            $retorno['permite_edicao'] = 1;
            
        } else {
            
            $retorno['permite_edicao'] = 0;
            
        }
        
        $ticketsAbertos = $item->ticketsAbertosSemCancelados();
        
        $retorno['id_variacao_ticket_aberto'] = ($ticketsAbertos->count()) ? $ticketsAbertos->last()->id_variacao : '';

        return $retorno;
    }

    public function postCompartilharItem(Request $request, Common $common)
    {
        $item = ArtwItens::find($request->input('id_item'));

        //dd($request->input('compartilhados'));

        DB::beginTransaction();

        // Verifica se o item nunca foi compartilhado para fazer o fluxo de compartilhamento
        if (!$item->compartilhado) {

            $ultimoCompartilhado = ArtwItens::whereNotNull('compartilhado')->orderBy('compartilhado', 'DESC')->limit(1)->get()->first();

            $novoNumero = ($ultimoCompartilhado) ? $ultimoCompartilhado->compartilhado + 1 : 1;

            $ultimaVersao = $item->artwVersoes->last();

            $ticketAberto = $item->ticketsAbertosSemCancelados()->last();


            // Cria um item novo compartilhado, igual anterior
            $novoItem = new ArtwItens;
            $novoItem->id_tipo = $item->id_tipo;
            $novoItem->id_pai = $item->id;
            $novoItem->numero_ticket = ($item->artwVersoes->last()) ? $item->artwVersoes->last()->artwTicket->numero : null;
            $novoItem->id_variacao_parent = $item->id_variacao_parent;
            $novoItem->compartilhado = $novoNumero;
            $novoItem->nome = $request->input('nome_caracteristica');
            $novoItem->status = 1;
            $novoItem->save();

            // Descontinua ou desabilita item anterior
            if ($ultimaVersao) {
            //if ($ultimaVersao) {
                
                $item->status = 2;
                $item->save();
                
            } else {
                
                $item->status = 0;
                $item->save();
                
            }
            

            // Caso tenha versão - Copia a último versão do item anterior para a versão 0 do item novo
            if ($ultimaVersao) {

                $ticket = new ArtwTickets();
                $ticket->id_sku = $ultimaVersao->artwTicket->id_sku;
                $ticket->id_variacao = $ultimaVersao->artwTicket->id_variacao;
                $ticket->id_item = $novoItem->id;
                $ticket->id_status = 6;
                $ticket->numero = $common->geraNumeroTicketFicticio();
                $ticket->descricao = 'Ticket aberto para auxiliar o compartilhamento.';
                $ticket->id_usuario = Auth::user()->id;
                $ticket->envio_autorizado = 1;
                $ticket->envio_finalizado = 1;
                $ticket->dt_envio_autorizado = date('Y-d-m H:i:s');
                $ticket->dt_envio_finalizado = date('Y-d-m H:i:s');
                $ticket->id_envio_autorizado = Auth::user()->id;
                $ticket->id_envio_finalizado = Auth::user()->id;
                $ticket->save();

                $ciclo = new ArtwCiclos();
                $ciclo->id_ticket = $ticket->id;
                $ciclo->id_status = 1;
                $ciclo->numero = 1;
                $ciclo->save();

                $path_original = $ultimaVersao->path;

                if (!file_exists(public_path($path_original))) {

                    dd($path_original);
                    exit;
                }

                $extension = pathinfo($path_original, PATHINFO_EXTENSION);

                $nome_arquivo = strtoupper($extension) . ' ' . $ticket->artwItem->nome_completo;

                $nome_arquivo .= ' • Versão 0';

                $path_novo = '/images/uploads/artes-finais/versoes/' . str_slug($nome_arquivo) . "." . $extension;

                File::copy(public_path() . $path_original, public_path() . $path_novo);

                $novaVersao = new ArtwVersoes();
                $novaVersao->id_sku = $ultimaVersao->artwTicket->id_sku;
                $novaVersao->id_variacao = $ultimaVersao->artwTicket->id_variacao;
                $novaVersao->id_item = $novoItem->id;
                $novaVersao->numero = 0;
                $novaVersao->indexado = 1;
                $novaVersao->path = $path_novo;
                $novaVersao->nome = $nome_arquivo;
                $novaVersao->id_usuario = Auth::user()->id;
                $novaVersao->id_ticket = $ticket->id;
                $novaVersao->save();

                $cicloLayout = ArtwCiclosLayout::firstOrNew([
                        'id_ciclo' => $ciclo->id
                ]);

                $cicloLayout->id_ciclo = $ciclo->id;
                $cicloLayout->id_usuario = Auth::user()->id;
                $cicloLayout->nome = $nome_arquivo;
                $cicloLayout->path = $path_novo;
                $cicloLayout->indexado = 1;
                $cicloLayout->updated_at = date('Y-m-d H:i:s');
                $cicloLayout->save();

                $ultimaArte = $ultimaVersao->artwTicket->artwArtes->last();

                if ($ultimaArte) {
                    
                    // Cria a arte
                    $arte = ArtwArtes::firstOrNew([
                            'id_ticket' => $ticket->id
                    ]);

                    $arte->id_usuario = Auth::user()->id;
                    $arte->path = $ultimaArte->path;
                    $arte->nome_arquivo = $ultimaArte->nome_arquivo;
                    $arte->nome = $novoItem->nome_completo;
                    $arte->save();
                    
                }

                //dd($arte);
            }

            // Caso tenha ticket - Atualiza o ticket com o item novo
            if ($ticketAberto) {

                //dd($ticketAberto, $novoItem);

                $ticketAberto->id_item = $novoItem->id;
                $ticketAberto->save();
            }


            //dd($novoNumero, $ultimaVersao, $ticketAberto);
            
            $item = $novoItem;
        } else {
            
            $item->nome = $request->input('nome_caracteristica');
            $item->save();
            
        }

        $compartilhados = [];

        foreach ($request->input('compartilhados') as $compartilhado) {

            $compartilhados[] = $compartilhado['id'];
        }

        $item->artwVariacoes()->sync($compartilhados);

        //dd($compartilhados, $request->all(), $item);

        DB::commit();

        return [];
    }

    public function renovarAprovacao(Request $request)
    {
        $participante = ArtwPerfisUsuariosTickets::find($request->input('id_participante'));
        $ticket = $participante->artwTicket;
        $id_perfil = $participante->id_perfil;

        DB::beginTransaction();

        $participante->id_membro_aprovador = null;
        $participante->aprovado = 0;
        $participante->ciclo = $participante->ciclo - 1;
        $participante->id_renovacao = Auth::user()->id;
        $participante->save();

        $artwAprovacao = ArtwAprovacoes::firstOrNew([
            'id_ticket' => $ticket->id,
            'id_usuario' => Auth::user()->id,
            'ciclo' => $ticket->ciclo_atual()->numero,
            'aprovado' => 0,
            'id_perfil' => $id_perfil,
            'descricao' => "Aprovação Renovada"
        ]);

        $artwAprovacao->save();

        DB::commit();

        return ['message' => 'Aprovação renovada com sucesso.'];
    }

    public function mercados()
    {
        $mercados = ArtwMercados::orderBy('nome')->get();

        $return = [];

        foreach ($mercados as $i => $mercado) {

            $return[$i] = $mercado;
            $return[$i]->title = $mercado->nome . ' (' . $mercado->sigla . ')';
        }

        return $mercados;
    }

    public function alteraRotulo($tipo)
    {
        switch ($tipo) {
            case 2:
                $novo_tipo = 48;
                break;
            case 9:
                $novo_tipo = 49;
                break;
            default:
                exit;
        }

        $rotulos = ArtwItens::select('artw_itens.*', 'artw_item_tipos.nome as item_tipo')
            ->join('artw_item_tipos', 'artw_itens.id_tipo', '=', 'artw_item_tipos.id')
            ->join('artw_variacoes', 'artw_itens.id_variacao', '=', 'artw_variacoes.id')
            ->join('skus', 'artw_variacoes.id_sku', '=', 'skus.id')
            ->join('dimensoes', 'skus.id_dimensao', '=', 'dimensoes.id')
            ->join('tipos_embalagens', 'dimensoes.id_tipo_embalagem', '=', 'tipos_embalagens.id')
            ->where('artw_item_tipos.id', 11)
            ->where('dimensoes.id_tipo_embalagem', $tipo)
            ->get();

        DB::beginTransaction();

        foreach ($rotulos as $rotulo) {
            echo $rotulo->id_tipo . ' - ' . $rotulo->item_tipo . ' - ' . @$rotulo->artwVariacao->sku_nome_completo . '<br>';

            $rotulo->id_tipo = $novo_tipo;
            $rotulo->save();
        }

        DB::commit();
    }

    public function addMercados()
    {
        $variacoes = ArtwVariacoes::withTrashed()->get();

        DB::beginTransaction();

        foreach ($variacoes as $variacao) {

            $variacaoMercado = \App\ArtwVariacaoMercados::firstOrNew(['id_mercado' => 1, 'id_variacao' => $variacao->id]);

            if (!$variacaoMercado->exists) {
                $variacaoMercado->save();
            }
        }

        DB::commit();
    }

    public function deletarPaintpack()
    {
        $itens = ArtwItens::where('id_tipo', 18)->get();

        DB::beginTransaction();

        foreach ($itens as $item) {

            $item->artwTickets()->forceDelete();
            $item->forceDelete();
        }

        DB::commit();

        dd($itens);
    }

    public function itemPadrao()
    {
        $itensBranco = ArtwItens::where('id_tipo', null)->get();

        DB::beginTransaction();

        foreach ($itensBranco as $item) {

            if (!$item->artwVariacaoAll || !$item->artwVariacaoAll->skuAll || !$item->artwVariacaoAll->skuAll->dimensoes || !$item->artwVariacaoAll->skuAll->dimensoes->tiposEmbalagens) {
                echo 'Erro no item: ' . $item->id . '<br>';
            } else {

                $embalagem = $item->artwVariacaoAll->skuAll->dimensoes->tiposEmbalagens->nome;

                echo $embalagem;

                $itemPadrao = \App\ItemPadrao::where('tipo', $embalagem)->where('principal', 1)->get()->first();

                if ($itemPadrao) {

                    echo ' - ' . $itemPadrao->item;
                } else {

                    echo ' - ERRO no Item Padrão';
                }

                $itemTipo = ArtwItemTipos::where('nome', $itemPadrao->item)->get()->first();

                if ($itemTipo) {

                    echo ' - ' . $itemTipo->nome . '<br>';

                    $item->id_tipo = $itemTipo->id;
                    $item->save();
                } else {

                    echo ' - Erro ao encontrar item' . '<br>';
                }
            }
        }

        DB::commit();
    }

    public function getArtes($id_variacao, $id_item = null)
    {
        $variacao = ArtwVariacoes::find($id_variacao);

        if (!$id_item) {
            $item = $variacao->artwItens->last();
        } else {
            $item = ArtwItens::find($id_item);
        }

        $versao = $item->artwVersoes->last();
        
        if ($versao) {

            unset($versao->error);
            
            if ($versao->path) {

                $versao->pdf = (object) [];

                $versao->pdf->path = $versao->path;
                $versao->pdf->filesize = $versao->filesize;
                
                $pdf = $versao->artwTicket->ciclo_atual()->artwCiclosLayout->last();

                $versao->pdf->nome_usuario = $pdf->admUsuario->nome_abreviado;
                $versao->pdf->data_criacao = $pdf->updated_at->format('H\hi \d\e d/m/Y');
            }
            
            if ($versao)
            {
                $ticket = $versao->artwTicket;

                if ($ticket->descricao == 'Ticket aberto para auxiliar o compartilhamento.' && $versao->numero == 0) {

                    $verificaTicket = ArtwTickets::where('id_item', $item->id)->where('descricao', 'Ticket aberto para auxiliar o upload manual de arte.')->get()->last();

                    if ($verificaTicket) {

                        $ticket = $verificaTicket;

                    }
                }
            }
            
            $versao->arte = ($ticket) ? $ticket->artwArtes->last() : null;
            $versao->sku_nome_completo = $item->sku_nome_completo;

            if (isset($versao->arte->path)) {
                $versao->arte->filesize = $versao->arte->filesize;
                //$versao->arte->path = asset($versao->arte->path);
                $versao->arte->nome_usuario = $versao->arte->admUsuario->nome_abreviado;
                $versao->arte->data_criacao = $versao->arte->updated_at->format('H\hi \d\e d/m/Y');

                unset($versao->arte->admUsuario);
                unset($versao->arte->created_at);
            }

            unset($ticket);
        } else {

            $versao = [
                'message' => 'Nenhuma versão',
                'sku_nome_completo' => $item->sku_nome_completo,
                'id_item' => $item->id,
                'id_variacao' => $variacao->id
            ];
        }

        return $versao;
    }

    public function projetos()
    {
        $allProjetos = ArtwProjetos::select()->orderBy('nome')->get();

        $projetos[0] = [
            'id' => 0,
            'nome' => 'Novo Projeto'
        ];

        foreach ($allProjetos as $p=>$projeto)
        {
            $projetos[$p+1] = $projeto;
        }

        return $projetos;
    }

    public function postArtes(Request $request, Common $common)
    {

        $arteArquivo = $request->file('arte');
        $pdf = $request->file('pdf');

        $variacao = ArtwVariacoes::find($request->input('id_variacao'));
        
        DB::beginTransaction();

        if ($request->input('id_item')) {

            $item = ArtwItens::find($request->input('id_item'));
        } else {

            $itemTipo = ArtwItemTipos::find($request->input('id_item_tipo'));

            $item = ArtwItens::firstOrNew([
                    'id_variacao_parent' => $variacao->id,
                    'id_tipo' => $itemTipo->id,
                    'compartilhado' => null
            ]);

            if (!$item->exists) {

                $item->save();
            }

            $item->status = 1;
            $item->save();

            $variacaoItem = ArtwVariacaoItens::firstOrNew([
                    'id_item' => $item->id,
                    'id_variacao' => $variacao->id
            ]);

            if (!$variacaoItem->exists) {

                $variacaoItem->save();
            }
        }

        $sku = $variacao->sku;

        $ticket = ArtwTickets::where('id_item', $item->id)->where('descricao', 'Ticket aberto para auxiliar o upload manual de arte.')->get()->last();
        
        if (!$ticket) {
            $ticket = new ArtwTickets();
            $ticket->id_sku = $sku->id;
            $ticket->id_variacao = $variacao->id;
            $ticket->id_item = $item->id;
            $ticket->id_status = 6;
            $ticket->numero = $common->geraNumeroTicketFicticio();
            $ticket->descricao = 'Ticket aberto para auxiliar o upload manual de arte.';
            $ticket->id_usuario = Auth::user()->id;
            $ticket->envio_autorizado = 1;
            $ticket->envio_finalizado = 1;
            $ticket->dt_envio_autorizado = date('Y-d-m H:i:s');
            $ticket->dt_envio_finalizado = date('Y-d-m H:i:s');
            $ticket->id_envio_autorizado = Auth::user()->id;
            $ticket->id_envio_finalizado = Auth::user()->id;
            $ticket->save();

            $ciclo = new ArtwCiclos();
            $ciclo->id_ticket = $ticket->id;
            $ciclo->id_status = 1;
            $ciclo->numero = 1;
            $ciclo->save();
        } else {

            $ciclo = $ticket->artwCiclos->last();
        }
        
        //dd($ciclo);

        // Faz a rotina de criação da arte
        if ($arteArquivo) {

            // Salva o arquivo no lovla correto
            //$relativePath = '/images/uploads/artwork/ticket/arte/';
            $filename = $arteArquivo->getClientOriginalName();

            // Cria um ticket fictício

            $nome_arquivo = $ticket->sku_nome_completo;
            $nome_arquivo .= ' - Ciclo ' . $ciclo->numero;
            
            // Cria a arte
            $arte = ArtwArtes::firstOrNew([
                    'id_ticket' => $ticket->id
            ]);

            if ($arte->exists)
            {
                $qtd = ArtwArtes::where('nome_arquivo', $arte->nome_arquivo)->count();

                if ($qtd == 1)
                {
                    Storage::disk(config('app.storage'))->delete(config('app.costummer_name') . '/artwork/artes-finais/' . $arte->nome_arquivo);
                }
            }

            if (Storage::disk(config('app.storage'))->exists(config('app.costummer_name') . '/artwork/artes-finais/' . $filename)) {

                $path_parts = pathinfo($filename);
    
                $filename = $path_parts['filename'] . '_' . time() . '.' . $path_parts['extension'];
            }

            //$filename = $common->verificaNomeArquivo($relativePath, $filename);
            //$destinationPath = public_path() . $relativePath;
            //$arteArquivo->move($destinationPath, $filename);

            Storage::disk(config('app.storage'))->put(config('app.costummer_name') . '/artwork/artes-finais/' . $filename, fopen($arteArquivo, 'r+'));
            $size = Storage::disk(config('app.storage'))->size(config('app.costummer_name') . '/artwork/artes-finais/' . $filename);

            //dd($arte);
            //$arte = new ArtwArtes();
            //if (!$arte->exists)
            //{
            //$arte->id_ticket = $ticket->id;
            $arte->id_usuario = Auth::user()->id;
            //$arte->path = $relativePath . $filename;
            $arte->nome_arquivo = $filename;
            $arte->nome = $ticket->sku_nome_completo;
            $arte->size = $size;
            $arte->storage = 1;
            $arte->save();
            //}

            $variacao->novo = 0;
            $variacao->save();
        }

        // Faz a rotina de criação da Versão
        if ($pdf) {

            $extension = $pdf->getClientOriginalExtension();

            $nome_arquivo = strtoupper($extension) . ' ' . $ticket->sku_nome_completo;
            $nome_arquivo .= ' - Versão 0';
            
            //dd($nome_arquivo);
            
            $relativePath = '/images/uploads/artes-finais/versoes/';
            $destinationPath = public_path() . $relativePath;

            $filename = str_slug($nome_arquivo) . "." . $extension;

            $pdf->move($destinationPath, $filename);

            $cicloLayout = ArtwCiclosLayout::firstOrNew([
                    'id_ciclo' => $ciclo->id
            ]);

            //if (!$cicloLayout->exists)
            //{
            //$cicloLayout = new ArtwCiclosLayout();
            $cicloLayout->id_ciclo = $ciclo->id;
            $cicloLayout->id_usuario = Auth::user()->id;
            $cicloLayout->nome = $nome_arquivo;
            $cicloLayout->path = $relativePath . $filename;
            $cicloLayout->updated_at = date('Y-m-d H:i:s');
            $cicloLayout->indexado = 1;
            $cicloLayout->save();
            //}
            //$versao = new ArtwVersoes();
            // Faz a Busca da Versão
            $versao = ArtwVersoes::firstOrNew([
                    'id_variacao' => $variacao->id,
                    'id_item' => $item->id
            ]);

            //if (!$versao->exists)
            //{
            $versao->id_sku = $sku->id;
            //$versao->id_variacao = $variacao->id;
            //$versao->id_item = $item->id;
            $versao->nome = $nome_arquivo;
            $versao->numero = 0;
            $versao->indexado = 1;
            $versao->path = $relativePath . $filename;
            $versao->id_usuario = Auth::user()->id;

            if (isset($ticket)) {
                $versao->id_ticket = $ticket->id;
            }

            $versao->save();
            //}

            $variacao->novo = 0;
            $variacao->save();
        }

        DB::commit();

        return [];
    }

    public function variacoes($id_produto, $id_dimensao, $id_campanha = null)
    {
        $sku = Skus
            ::where('id_produto', $id_produto)
            ->where('id_dimensao', $id_dimensao)
            ->get()
            ->first();

        if (!$sku) {
            return response()->json([]);
        }

        if ($id_campanha || $id_campanha == 0) {

            $variacoes = ArtwVariacoes
                ::where('id_sku', $sku->id)
                ->where('id_campanha', $id_campanha)
                ->get();
        } else {

            $variacoes = ArtwVariacoes
                ::where('id_sku', $sku->id)
                ->get();
        }

        $retorno = [];

        foreach ($variacoes as $variacao) {

            $itensAtivos = ArtwItens::where('id_variacao_parent', $variacao->id)->where('status', 1)->get();

            if ($itensAtivos->count() > 0) {
                $retorno[] = $variacao;
            }
        }

        return $retorno;
    }

    public function ticketAcao(Request $request, ArtwPerfisUsuariosEquipeTickets $artwPerfisUsuariosEquipeTickets, Common $common)
    {
        if (!$request->input('id') || !$request->input('status')) {
            return response()->json(['error' => 400, 'message' => 'Parâmetros obrigatórios: id, status'], 404);
        }

        $ticket = ArtwTickets::find($request->input('id'));

        if (!$ticket) {
            return response()->json(['error' => 404, 'message' => 'Ticket Not found'], 404);
        }

        $tkt_status = ArtwTktStatus::find($request->input('status'));

        // Verifica se a ação é do tipo autorizar_envio ou finalizar_envio
        if ($request->input('status') == 'finalizar_envio' || $request->input('status') == 'finalizar_envio_fornecedor') {

            if ($request->input('status') == 'finalizar_envio') {

                // notificações
                $qtdEnvioArtes = $ticket->artwPerfisUsuariosTickets()->where('id_perfil', 6)->get()->count();

                if ($qtdEnvioArtes) {

                    $common->enviarNotificacao($ticket, 'envio_autorizado', 'envio_arte');
                } else {

                    // Envia E-mail para o DONO finalizar o ticket
                    $this->changeStatusTicket($ticket, 12);
                    //$common->enviarNotificacao($ticket, 'finalizar_ticket', 'dono');

                    //$ticket->envio_finalizado = 1;
                    //$ticket->dt_envio_finalizado = date('Y-m-d H:i:s');
                    //$ticket->id_envio_finalizado = Auth::user()->id;
                }

                $ticket->envio_autorizado = 1;
                $ticket->dt_envio_autorizado = date('Y-m-d H:i:s');
                $ticket->id_envio_autorizado = Auth::user()->id;
                $ticket->save();
            }

            if ($request->input('status') == 'finalizar_envio_fornecedor') {
                
                // Verifica se existe a etapa PDF do Fornecedor
                $qtdPdfFornecedor = $ticket->artwPerfisUsuariosTickets()->where('id_perfil', 7)->get()->count();
                
                if ($qtdPdfFornecedor) {
                    
                    $common->changeStatusTicket($ticket, 11);
                    
                } else {
                    
                    // Envia E-mail para o DONO finalizar o ticket
                    $common->changeStatusTicket($ticket, 12);
                    //$common->enviarNotificacao($ticket, 'finalizar_ticket', 'dono');

                    $id_perfil = 6;
                    $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);

                    $perfilUsuarioTicket->aprovado = 1;
                    $perfilUsuarioTicket->save();
                    
                }
                
                $ticket->envio_finalizado = 1;
                $ticket->dt_envio_finalizado = date('Y-m-d H:i:s');
                $ticket->id_envio_finalizado = Auth::user()->id;
                $ticket->save();
                
            }

            return response()->json(['success' => 200, 'message' => 'Ok'], 200);
        }

        $libera_status = 1;

        DB::beginTransaction();

        switch ($tkt_status->id) {
            case 1:
                //
                break;
            case 2:
                $id_perfil = 5;
                $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);

                break;
            case 3:
                $id_perfil = 3;
                $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);

                break;
            case 4:
                
                \App::call('App\Http\Controllers\ApiController@apagarNotas', [$ticket->id]);
                
                $libera_status = 0;

                $id_perfil = 2;

                $perfilUsuarioEquipeTicket = $artwPerfisUsuariosEquipeTickets->artwPerfilUsuarioEquipeTicket(Auth::user()->id, $id_perfil, $ticket->id);

                if ($perfilUsuarioEquipeTicket) {
                    $perfilUsuarioTicket = $perfilUsuarioEquipeTicket->artwPerfisUsuariosTicket;
                } else {
                    $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);
                }

                if ($perfilUsuarioTicket->aprovado == 2) {
                    $perfilUsuarioTicket->aprovado = 0;
                    $perfilUsuarioTicket->save();
                }
                
                $perfilUsuarioTicket->ciclo_aprovacao = $ticket->ciclo_atual()->numero;
                
                $perfilUsuarioTicket->em_reprovacao = 0;
                $perfilUsuarioTicket->em_reprovacao_by = null;
                $perfilUsuarioTicket->save();

                $pendentes = $ticket->participantesPendentes($id_perfil, $ticket->ciclo_atual()->numero);
                $reprovados = $ticket->participantesReprovados($id_perfil, $ticket->ciclo_atual()->numero);

                if ($pendentes->count() == 1) {
                    $libera_status = 1;
                }

                if ($reprovados->count() > 0 && $pendentes->count() == 1 && $perfilUsuarioTicket->aprovado == 0) {

                    $common->reprovaTicket($ticket, $id_perfil, $perfilUsuarioTicket, $perfilUsuarioEquipeTicket);

                    DB::commit();

                    return response()->json(['success' => 200, 'message' => 'Ok'], 200);
                }

                $artwAprovacao = ArtwAprovacoes::firstOrNew([
                    'id_ticket' => $ticket->id,
                    'id_usuario' => Auth::user()->id,
                    'ciclo' => $ticket->ciclo_atual()->numero,
                    'aprovado' => 1,
                    'id_perfil' => $id_perfil
                ]);
    
                $artwAprovacao->save();

                break;
            case 5:
                
                \App::call('App\Http\Controllers\ApiController@apagarNotas', [$ticket->id]);
                
                $libera_status = 0;

                $id_perfil = 1;

                $perfilUsuarioEquipeTicket = $artwPerfisUsuariosEquipeTickets->artwPerfilUsuarioEquipeTicket(Auth::user()->id, $id_perfil, $ticket->id);

                if ($perfilUsuarioEquipeTicket) {
                    $perfilUsuarioTicket = $perfilUsuarioEquipeTicket->artwPerfisUsuariosTicket;
                } else {
                    $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);
                }

                if ($perfilUsuarioTicket->aprovado == 2) {
                    $perfilUsuarioTicket->aprovado = 0;
                    $perfilUsuarioTicket->save();
                }
                
                $perfilUsuarioTicket->ciclo_aprovacao = $ticket->ciclo_atual()->numero;
                
                $perfilUsuarioTicket->em_reprovacao = 0;
                $perfilUsuarioTicket->em_reprovacao_by = null;
                $perfilUsuarioTicket->save();

                $pendentes = $ticket->participantesPendentes($id_perfil, $ticket->ciclo_atual()->numero);
                $reprovados = $ticket->participantesReprovados($id_perfil, $ticket->ciclo_atual()->numero);

                if ($pendentes->count() == 1) {
                    $libera_status = 1;
                }

                if ($reprovados->count() > 0 && $pendentes->count() == 1 && $perfilUsuarioTicket->aprovado == 0) {

                    $common->reprovaTicket($ticket, $id_perfil, $perfilUsuarioTicket, $perfilUsuarioEquipeTicket);

                    DB::commit();

                    return response()->json(['success' => 200, 'message' => 'Ok'], 200);
                }

                $artwAprovacao = ArtwAprovacoes::firstOrNew([
                    'id_ticket' => $ticket->id,
                    'id_usuario' => Auth::user()->id,
                    'ciclo' => $ticket->ciclo_atual()->numero,
                    'aprovado' => 1,
                    'id_perfil' => $id_perfil
                ]);
    
                $artwAprovacao->save();

                break;
                
            case 11:
                $libera_status = 0;

                $id_perfil = 7;

                $perfilUsuarioEquipeTicket = $artwPerfisUsuariosEquipeTickets->artwPerfilUsuarioEquipeTicket(Auth::user()->id, $id_perfil, $ticket->id);

                if ($perfilUsuarioEquipeTicket) {
                    $perfilUsuarioTicket = $perfilUsuarioEquipeTicket->artwPerfisUsuariosTicket;
                } else {
                    $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);
                }
                
                $common->changeStatusTicket($ticket, 12);
                //$common->enviarNotificacao($ticket, 'finalizar_ticket', 'dono');
                
                break;
                
            case 6: // FINALIZA O TICKET

                if ($ticket->id_usuario != Auth::user()->id) {
                    return response()->json(['error' => 401, 'message' => 'Você não possui o perfil apropriado.'], 404);
                }

                $perfilUsuarioTicket = 'dono';

                // Verifica se o id_ticket já foi usada em alguma versão
                $check = ArtwVersoes::where('id_ticket', $ticket->id)->get()->count();

                if ($check == 0) {

                    $versaoAntiga = $ticket->artwItem->artwVersoes->last();

                    if ($versaoAntiga) {
                        $novoNumero = $versaoAntiga->numero + 1;
                    } else {
                        $novoNumero = 1;
                    }

                    $path_original = $ticket->ciclo_atual()->artwCiclosLayout->last()->path;

                    $extension = pathinfo($path_original, PATHINFO_EXTENSION);

                    $nome_arquivo = strtoupper($extension) . ' ' . $ticket->sku_nome_completo;

                    $nome_arquivo .= ' • Versão ' . $novoNumero;

                    $path_novo = '/images/uploads/artes-finais/versoes/' . str_slug($nome_arquivo) . "." . $extension;

                    File::copy(public_path() . $path_original, public_path() . $path_novo);

                    $novaVersao = new ArtwVersoes();
                    $novaVersao->id_sku = $ticket->artwItem->artwVariacao->sku->id;
                    $novaVersao->id_variacao = $ticket->artwVariacao->id;
                    $novaVersao->id_item = $ticket->artwItem->id;
                    $novaVersao->numero = $novoNumero;
                    $novaVersao->path = $path_novo;
                    $novaVersao->nome = $nome_arquivo;
                    $novaVersao->id_usuario = Auth::user()->id;
                    $novaVersao->id_ticket = $ticket->id;
                    $novaVersao->indexado = 1;
                    $novaVersao->save();

                    /*
                    if (config('app.cloudflow')) {

                        // Manda renderizar
                        $ch = curl_init();

                        curl_setopt($ch, CURLOPT_URL, route('api.render', ['versao', $novaVersao->id]));
                        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
                        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 500);

                        curl_exec($ch);

                        if (curl_errno($ch)) {
                            $error_message = curl_error($ch);
                        }

                        curl_close($ch);
                    }
                     * 
                     */

                    $ticket->artwVariacao->novo = 0;
                    $ticket->artwVariacao->save();

                    $ticket->artwItem->artwVariacao->sku->novo = 0;
                    $ticket->artwItem->artwVariacao->sku->save();

                    //$ticket->artwItem->novo = 0;
                    //$ticket->artwItem->save();
                }
                break;
            case 10:
                
                $id_perfil = 5;
                $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);
                
                break;
            default:
                $perfilUsuarioTicket = null;
                break;
        }

        if ($perfilUsuarioTicket !== 'dono') {

            if (!$perfilUsuarioTicket) {
                return response()->json(['error' => 401, 'message' => 'Você não possui o perfil apropriado.'], 404);
            }

            if (isset($perfilUsuarioEquipeTicket) && $perfilUsuarioEquipeTicket) {
                $perfilUsuarioTicket->id_membro_aprovador = $perfilUsuarioEquipeTicket->id;
            } else {
                $perfilUsuarioTicket->id_membro_aprovador = null;
            }

            $perfilUsuarioTicket->ciclo = $ticket->ciclo_atual()->numero;
            $perfilUsuarioTicket->aprovado = 1;
            $perfilUsuarioTicket->save();
        }

        if ($libera_status) {
            $status = $tkt_status->id;

            // Verifica se existe algum marketing
            if ($status === 3) {
                $count_marketing = $ticket->artwPerfisUsuariosTickets()->where('id_perfil', 2)->get()->count();

                if (!$count_marketing) {
                    $status = 4;
                }
            }

            $common->changeStatusTicket($ticket, $status);

            // Adiciona um registro para histórico na tabela tickets_status
            $solicitacoesStatus = new ArtwTicketsStatus();
            $solicitacoesStatus->id_ticket = $ticket->id;
            $solicitacoesStatus->id_ciclo = $ticket->ciclo_atual()->id;
            $solicitacoesStatus->id_usuario = Auth::user()->id;
            $solicitacoesStatus->id_status = $status;
            $solicitacoesStatus->save();
        }

        if (isset($checkReprovacao)) {
            
        }

        // Atualiza a timeline
        $common->ticketAtualizaTimeline($ticket);

        DB::commit();

        return response()->json(['success' => 200, 'message' => 'Ok'], 200);
    }

    public function ticketAcaoFornecedorPdf(Request $request)
    {
        
        $fornecedorPdf = ArtwFornecedorPdfs::findOrFail($request->input('id'));
     
        DB::beginTransaction();

        $fornecedorPdf->status = ($request->input('acao') == 'reprovar') ? 2 : 1;
        $fornecedorPdf->id_aprovacao = Auth::user()->id;
        $fornecedorPdf->dt_aprovacao = date('Y-m-d H:i:s');
        $fornecedorPdf->save();
        
        $envio = $fornecedorPdf->artwArteEnvio;

        if ($request->input('acao') == 'reprovar' && $envio) {

            $arte = $envio->artwArte;

            foreach ($envio->envioEmails as $email)
            {
                $data = [
                    'ticket' => $arte->artwTicket,
                    'sku' => $arte->artwTicket->sku,
                    'email' => $email->email,
                    'arte' => $arte,
                    'token' => $email->token
                ];

                // to-do

                Mail::send('emails.notificacoes.email_notificacao_45', $data, function ($message) use($data) {
                    $message->from(config('app.mail_from'), config('app.mail_name'));
                    $message->to($data['email'])->subject('PDF do Fornecedor Reprovado - ' . $data['ticket']->sku_nome_completo);
                });
            }

        }

        DB::commit();

        return response()->json([
            
            'success' => 200,
            'message' => ($request->input('acao') == 'reprovar') ? 'PDF do Fornecedor reprovado com sucesso.' : 'PDF do Fornecedor aprovado com sucesso.'
            
            ], 200);
    }

    public function ticketAcaoNotificarPdfAnexado(Request $request, Common $common)
    {
        
        $fornecedorPdf = ArtwFornecedorPdfs::findOrFail($request->input('id'));
     
        $fornecedorPdf->notificado = 1;
        $fornecedorPdf->save();

        $ticket = $fornecedorPdf->artwTicket;

        $fornecedor = $fornecedorPdf->fornecedor;

        $texto = 'O PDF do Fornecedor ' . $fornecedor->nome . ' foi anexado no ticket.';
        
        $common->enviarNotificacao($ticket, 'pdf_fornecedor_anexado', 'pdf-fornecedor', $texto, 1);
        
        return response()->json([
            
            'success' => 200,
            'message' => 'Notificação enviada com sucesso.'
            
            ], 200);
    }
    
    public function ticketExcluirFornecedorPdf(Request $request)
    {
        
        $fornecedorPdf = ArtwFornecedorPdfs::findOrFail($request->input('id'));
     
        DB::beginTransaction();

        $pdf = public_path($fornecedorPdf->path);
        
        if (is_file($pdf)){
            
            unlink($pdf);
            
        }

        $envio = $fornecedorPdf->artwArteEnvio;

        if ($envio)
        {
            $arte = $envio->artwArte;

            foreach ($envio->envioEmails as $email)
            {
                $data = [
                    'ticket' => $arte->artwTicket,
                    'sku' => $arte->artwTicket->sku,
                    'email' => $email->email,
                    'arte' => $arte,
                    'token' => $email->token
                ];

                // to-do

                Mail::send('emails.notificacoes.email_notificacao_46', $data, function ($message) use($data) {
                    $message->from(config('app.mail_from'), config('app.mail_name'));
                    $message->to($data['email'])->subject('PDF do Fornecedor Excluído - ' . $data['ticket']->sku_nome_completo);
                });
            }
        }
        
        $fornecedorPdf->delete();
        
        DB::commit();

        return response()->json([
            
            'success' => 200,
            'message' => 'PDF do Fornecedor deletado com sucesso.'
            
            ], 200);
    }
    
    public function ticketPausarCancelar(Request $request, Common $common)
    {
        if ($request->input('envia_notificacao') == 'true')
        {
            $enviarNotificacao = true;
        }
        elseif ($request->input('envia_notificacao') == 'false')
        {
            $enviarNotificacao = false;
        }
        else
        {
            $enviarNotificacao = true;
        }

        if (!$request->input('id')) {
            return response()->json(['error' => 400, 'message' => 'Parâmetros obrigatórios: id, status'], 404);
        }

        $ticket = ArtwTickets::find($request->input('id'));

        if (!$ticket) {
            return response()->json(['error' => 404, 'message' => 'Ticket Not found'], 404);
        }

        if ($request->input('acao') == 'cancelar') {

            DB::beginTransaction();

            // verifica se o item tem versão
            if ($ticket->artwItem && $ticket->artwItem->artwVersoes->count() == 0) {
                $ticket->artwItem->status = 0;
                $ticket->artwItem->save();
            }

            $ticket->pausado_em = null;
            $ticket->pausado_por = null;
            $ticket->cancelado_em = date('Y-m-d H:i:s');
            $ticket->cancelado_por = Auth::user()->id;
            $ticket->cancelado_motivo = $request->input('motivo');
            $ticket->cancelado_nome_original = ($ticket->modulo==1) ? $ticket->sku_nome_completo : $ticket->adsmart_nome_completo;
            $ticket->cancelado_campanha = ($ticket->artwItem && $ticket->artwItem->artwVariacao && $ticket->artwItem->artwVariacao->campanha) ? $ticket->artwItem->artwVariacao->campanha->nome : null;
            $ticket->cancelado_variacao = ($ticket->artwItem && $ticket->artwItem->artwVariacao) ? $ticket->artwItem->artwVariacao->nome : null;
            $ticket->cancelado_item = ($ticket->artwItem) ? $ticket->artwItem->artwItemTipo->nome : null;
            $ticket->save();

            if ($ticket->modulo==1)
            {
                // Cancela versão e envios ao fornecedor
                $versao = ArtwVersoes::where('id_ticket', $ticket->id)->get()->first();

                if ($versao)
                {
                    $versao->delete();
                }

                foreach ($ticket->artwArtesEnvios as $envio)
                {
                    $this->cancelarEnvioService($envio->id, $enviarNotificacao);
                }

                if ($request->input('tipo') == 'versao' && $enviarNotificacao)
                {
                    $common->enviarNotificacao($ticket, 'versao_cancelada', 'todos', '', true);
                }
                elseif ($request->input('tipo') != 'versao')
                {
                    $common->enviarNotificacao($ticket, 'ticket_cancelado', 'todos', '', true);
                }

            } else {

                $peca = $ticket->adsmartPeca;
                $peca->status = 0;
                $peca->save();

                // Notificações de Adsmart
                $common->enviarNotificacao($ticket, 'adsmart_ticket_cancelado', 'todos', '', true);

            }
            

            DB::commit();

            return response()->json(['success' => 200, 'message' => 'Ticket cancelado com sucesso.'], 200);
        }

        if ($request->input('acao') == 'pausar') {

            DB::beginTransaction();

            $ticket->pausado_em = date('Y-m-d H:i:s');
            $ticket->pausado_por = Auth::user()->id;
            $ticket->save();

            if ($ticket->modulo == 1)
            {
                // Notificações de Artwork
                $common->enviarNotificacao($ticket, 'ticket_pausado', 'todos', '', true);

            } else {
                
                // Notificações de Adsmart
                $common->enviarNotificacao($ticket, 'adsmart_ticket_pausado', 'todos', '', true);

            }

            DB::commit();

            return response()->json(['success' => 200, 'message' => 'Ticket pausado com sucesso.'], 200);
        }

        if ($request->input('acao') == 'despausar') {

            DB::beginTransaction();

            $ticket->pausado_em = null;
            $ticket->pausado_por = null;
            $ticket->save();

            if ($ticket->modulo == 1)
            {

                $common->enviarNotificacao($ticket, 'ticket_reativado', 'todos', '', true);

            } else {

                // Notificações de Adsmart
                $common->enviarNotificacao($ticket, 'adsmart_ticket_reativado', 'todos', '', true);

            }

            DB::commit();

            return response()->json(['success' => 200, 'message' => 'Ticket reativado com sucesso.'], 200);
        }

        // Atualiza a timeline
        $common->ticketAtualizaTimeline($ticket);
    }

    public function ticketSalvarParticipantes(Request $request, Common $common)
    {
        if (!$request->input('id_ticket')) {
            return response()->json(['error' => 400, 'message' => 'Parâmetros obrigatórios: id_ticket'], 404);
        }

        $ticket = ArtwTickets::find($request->input('id_ticket'));

        if (!$ticket) {
            return response()->json(['error' => 404, 'message' => 'Ticket Not found'], 404);
        }

        DB::beginTransaction();

        $perfis = $request->input('cbMembershipProfile');

        // Verifica se existem os três tipos de perfis necessários para prosseguir
        //if (!in_array(1, $perfis) || !in_array(3, $perfis) || !in_array(5, $perfis)) {
        //return back()->withErrors('Os três perfis obrigatórios não foram selecionados: Aprovador, Revisor e Arte-finalista.');
        //}
        // Trata as exclusões de membros
        foreach ((array) $request->input('membros_excluidos') as $id)
        {
            $participante = ArtwPerfisUsuariosTickets::find($id);
            
            if ($participante)
            {
                $participante->artwPerfisUsuariosEquipeTickets()->delete();
                $participante->delete();
            }
        }

        // Trata as exclusões de membros da equipe
        foreach ((array) $request->input('equipe_membros_excluidos') as $id) {
            ArtwPerfisUsuariosEquipeTickets::destroy($id);
        }

        //dd($request->input('cbMembershipName'), $request->input('cbMembershipAltName'), $perfis);

        foreach ($request->input('add_participante') as $k => $dados) {

            $participante = ArtwPerfisUsuariosTickets::withTrashed()->firstOrNew([
                    'id_ticket' => $ticket->id,
                    'id_usuario' => $dados['id_usuario'],
                    'id_perfil' => $dados['id_perfil']
            ]);
            
            $restore = false;
            
            if ($participante->trashed()) {
                $participante->restore();
                $participante->aprovado = 0;
                $participante->em_reprovacao = 0;
                $participante->em_reprovacao_by = 0;
                $restore = true;
            }
            
            if (isset($dados['id_participante_selected']) && $dados['id_usuario'] != $dados['id_participante_selected']) {

                $participante->id_usuario = $dados['id_participante_selected'];
            }

            if (!$participante->exists || $restore) {
                $participante->ciclo = 0;
                $enviar_notificacao = 1;
            } else {
                $enviar_notificacao = 0;
            }

            //$participante->recebe_email = ($request->input('chkNotificationsSwitch.' . $k)) ? 1 : 0;
            $participante->save();

            if ($enviar_notificacao) {

                if ($ticket->nova_embalagem) {

                    //$common->enviarNotificacao($ticket, 'primeiro_alteracao', $participante->id_usuario);
                    
                } elseif (!$ticket->nova_embalagem) {

                    //$common->enviarNotificacao($ticket, 'primeiro_embalagem', $participante->id_usuario);
                }

                //dd('PARA');
            }

            $equipe = (isset($dados['equipe_membros'])) ? $dados['equipe_membros'] : [];

            foreach ($equipe as $m => $membro) {
                $participanteEquipe = ArtwPerfisUsuariosEquipeTickets::withTrashed()->firstOrNew([
                        'id_parent' => $participante->id,
                        'id_usuario' => $membro['id_usuario']
                ]);
                
                if ($participanteEquipe->trashed()) {
                    $participanteEquipe->restore();
                }
                
                if (isset($membro['id_participante_selected']) && $membro['id_usuario'] != $membro['id_participante_selected']) {

                    $participanteEquipe->id_usuario = $membro['id_participante_selected'];
                }

                /*
                  if ($request->input('cbMembershipTeamAltName.' . $k . '.' . $m) && $membro != $request->input('cbMembershipTeamAltName.' . $k . '.' . $m)) {
                  $participanteEquipe->id_usuario = $request->input('cbMembershipTeamAltName.' . $k . '.' . $m);
                  }
                 * 
                 */

                //$participanteEquipe->recebe_email = ($request->input('chkNotificationsSwitchMember.' . $k . '.' . $m)) ? 1 : 0;

                $participanteEquipe->save();
            }
        }

        // Atualiza a timeline
        $common->ticketAtualizaTimeline($ticket);

        DB::commit();

        return response()->json(['success' => 200, 'message' => 'Participantes salvos com sucesso.'], 200);
    }

    public function solicitacaoAcao(Request $request)
    {
        if (!$request->input('id') || !$request->input('status')) {
            return response()->json(['error' => 400, 'message' => 'Parâmetros obrigatórios: id, status'], 404);
        }

        $solicitacao = ArtwSolicitacoes::find($request->input('id'));

        if (!$solicitacao) {
            return response()->json(['error' => 404, 'message' => 'Solicitação Not found'], 404);
        }

        $solicitacao_status = ArtwSolicitacaoStatus::find($request->input('status'));

        if (!$solicitacao_status) {
            return response()->json(['error' => 404, 'message' => 'Solicitação Status Not found'], 404);
        }

        DB::beginTransaction();

        // Salva o status na tabela de solicitação
        $solicitacao->id_status = $solicitacao_status->id;
        $solicitacao->save();

        // Adiciona um registro para histórico na tabela solicitacoes_status
        $solicitacoesStatus = new ArtwSolicitacoesStatus();
        $solicitacoesStatus->id_solicitacao = $solicitacao->id;
        $solicitacoesStatus->id_ciclo = $solicitacao->artwCiclo->id;
        $solicitacoesStatus->id_usuario = Auth::user()->id;
        $solicitacoesStatus->id_status = $solicitacao_status->id;
        $solicitacoesStatus->save();

        DB::commit();

        return response()->json(['success' => 200, 'message' => 'Ok'], 200);
    }

    public function ticketEnviarEmails(Request $request, Common $common)
    {
        $artes = ArtwArtes::where('id', $request->input('id'))->get();

        if (!$artes) {
            return response()->json(['error' => 404, 'message' => 'Arte Not found'], 404);
        }

        $validator = new EmailValidator();
        $multipleValidations = new MultipleValidationWithAnd([
            new RFCValidation(),
            new DNSCheckValidation()
        ]);

        // Envia os e-mails para todos
        $emails = array_map('trim', explode(',', $request->input('emails')));

        $emailsInvalidos = [];

        foreach ($emails as $email) {

            if (!$validator->isValid($email, $multipleValidations)) {

                $emailsInvalidos[] = $email;
            }
        }

        if (count($emailsInvalidos)) {

            return response()->json(['success' => 201, 'message' => 'E-mails inválidos: <br><br>' . implode('<br>', $emailsInvalidos)], 200);
        }

        $expiracao = date('Y-m-d H:i:s', strtotime("+30 days", strtotime(date('Y-m-d') . '23:59:59')));

        $arteEnvio = new ArtwArtesEnvios;
        $arteEnvio->emails = $request->input('emails');
        $arteEnvio->id_fornecedor = $request->input('fornecedor');
        $arteEnvio->mensagem = $request->input('msg_fornecedor');
        $arteEnvio->id_arte = $artes[0]->id;
        $arteEnvio->id_ticket = $artes[0]->artwTicket->id;
        $arteEnvio->id_usuario = Auth::user()->id;
        $arteEnvio->dt_expiracao = $expiracao;
        $arteEnvio->save();
        
        $item = $artes[0]->artwTicket->artwItem;
        
        if ($item->compartilhado) {
            
            $compartilhadas = $common->getTodasEmbalagensPorItem($item->id);
            
        } else {
            
            $compartilhadas = [];
            
        }
        
        $arte = $arteEnvio->artwArte;
        
        foreach ($emails as $email) {

            $token = sha1(bin2hex(random_bytes(8)));

            $arteEnvioEmail = new ArtwArtesEnviosEmails;
            $arteEnvioEmail->id_arte_envio = $arteEnvio->id;
            $arteEnvioEmail->email = $email;
            $arteEnvioEmail->token = $token;

            $data = [
                'ticket' => $artes[0]->artwTicket,
                'sku' => $artes[0]->artwTicket->sku,
                'email' => $email,
                'token' => $token,
                'compartilhadas' => $compartilhadas,
                'mensagem' => $request->input('msg_fornecedor'),
                'arte' => $arte
            ];

            $path = public_path($data['ticket']->ciclo_atual()->artwCiclosLayout->last()->path);

            $retorno = Mail::send('emails.notificacoes.email_notificacao_38', $data, function ($message) use($data, $path) {
                    $message->from(config('app.mail_from'), config('app.mail_name'));
                    $message->to($data['email'])->subject('Arte-final - ' . $data['ticket']->sku_nome_completo);
                });

            $arteEnvioEmail->envios = $retorno;
            $arteEnvioEmail->save();
        }
        
        // Envia e-mail para o responsável pelo envio
        $fornecedor = Fornecedores::find($request->input('fornecedor'));
        
        $token = sha1(bin2hex(random_bytes(8)));

        $arteEnvioEmail = new ArtwArtesEnviosEmails;
        $arteEnvioEmail->id_arte_envio = $arteEnvio->id;
        $arteEnvioEmail->email = Auth::user()->email;
        $arteEnvioEmail->token = $token;
        $arteEnvioEmail->save();
        
        $data = [
            'ticket' => $artes[0]->artwTicket,
            'sku' => $artes[0]->artwTicket->sku,
            'emails' => $request->input('emails'),
            'nome_to' => Auth::user()->nome_abreviado,
            'token' => $token,
            'mensagem' => $request->input('msg_fornecedor'),
            'fornecedor' => $fornecedor,
            'arte' => $arte,
            'data_envio' => $arteEnvio->created_at
        ];
        
        $path = public_path($data['ticket']->ciclo_atual()->artwCiclosLayout->last()->path);

        $retorno = Mail::send('emails.notificacoes.email_notificacao_40', $data, function ($message) use($data, $path) {
                $message->from(config('app.mail_from'), config('app.mail_name'));
                $message->to(Auth::user()->email)->subject('Arte-final Enviada - ' . $data['ticket']->sku_nome_completo . ' - Versão ' . $data['arte']->versao);
            });

        $arteEnvioEmail->envios = $retorno;
        $arteEnvioEmail->save();

        return response()->json(['success' => 200, 'message' => 'E-mails enviados com sucesso.'], 200);
    }

    public function renovarLink(Request $request)
    {
        $envio = ArtwArtesEnvios::find($request->input('id_arte_envio'));

        if (!$envio) {
            return response()->json(['error' => 404, 'message' => 'Not found'], 404);
        }

        $expiracao = date('Y-m-d H:i:s', strtotime("+30 days", strtotime(date('Y-m-d') . '23:59:59')));

        $envio->dt_expiracao = $expiracao;
        $envio->save();

        return response()->json(['success' => 200, 'message' => 'O link de acesso às artes foi renovado e a partir de hoje levará 30 dias para ser expirado.'], 200);
    }

    public function reenviarEmail(Request $request)
    {
        $arteEnvio = ArtwArtesEnvios::find($request->input('id_arte_envio'));

        $validator = new EmailValidator();
        $multipleValidations = new MultipleValidationWithAnd([
            new RFCValidation(),
            new DNSCheckValidation()
        ]);

        $emailsInvalidos = [];

        foreach ($arteEnvio->envioEmails as $envioEmail) {

            if (!$validator->isValid($envioEmail->email, $multipleValidations)) {

                $emailsInvalidos[] = $envioEmail->email;
            }
        }

        if (count($emailsInvalidos)) {

            return response()->json(['success' => 201, 'message' => 'E-mails inválidos: <br><br>' . implode('<br>', $emailsInvalidos)], 200);
        }

        $ticket = $arteEnvio->artwTicket;
        $sku = $ticket->sku;
        $arte = $arteEnvio->artwArte;

        foreach ($arteEnvio->envioEmails as $envioEmail) {

            $data = [
                'ticket' => $ticket,
                'sku' => $sku,
                'email' => $envioEmail->email,
                'token' => $envioEmail->token,
                'arte' => $arte
            ];

            $path = public_path($data['ticket']->ciclo_atual()->artwCiclosLayout->last()->path);

            $retorno = Mail::send('emails.notificacoes.email_notificacao_38', $data, function ($message) use($data, $path) {
                    $message->from(config('app.mail_from'), config('app.mail_name'));
                    $message->to($data['email'])->subject('Arte-final - ' . $data['ticket']->sku_nome_completo);
                });
        }

        return response()->json(['success' => 200, 'message' => 'O e-mail contendo o link de acesso às artes foi reenviado para os mesmos endereços deste envio.'], 200);
    }

    public function ticketEnviarLembrete(Request $request, Common $common)
    {
        $participanteEquipe = null;


        if ($request->input('id_participante')) {

            $participante = ArtwPerfisUsuariosTickets::find($request->input('id_participante'));
        } else {

            $participanteEquipe = ArtwPerfisUsuariosEquipeTickets::find($request->input('id_membro'));
            $participante = $participanteEquipe->artwPerfisUsuariosTicket;
        }

        $ticket = $participante->artwTicket;

        if (!$ticket->isOpen()) {
            return response()->json(['success' => 404, 'message' => 'Não pode ser enviado.'], 404);
        }

        $id_perfil = $participante->id_perfil;

        switch ($id_perfil) {
            case 1:
                $perfil = 'aprovadores';
                $modelo = 'lembrete_aprovador';
                break;
            case 2:
                $perfil = 'marketings';
                $modelo = 'lembrete_marketing';
                break;
            //case 3:
            //$perfil = 'revisores';
            //$modelo = 'lembrete_revisor';
            //break;
            //case 4:
            //$perfil = 'participantes';
            //return false;
            //break;
            //case 5:
            //$perfil = 'arte-finalistas';
            //return false;
            //break;
        }

        if (!isset($perfil)) {
            return response()->json(['success' => 404, 'message' => 'Não pode ser enviado.'], 404);
        }

        if ($request->input('todos')) {

            $common->enviarNotificacao($ticket, $modelo, $perfil, null, 1, null, 3);
        } else {
            if ($participanteEquipe) {
                $common->enviarNotificacao($ticket, $modelo, $participante->admUsuario->id, null, 1, $participanteEquipe->admUsuario->id);
            } else {
                $common->enviarNotificacao($ticket, $modelo, $participante->admUsuario->id, null, 1);
            }
        }

        return response()->json(['success' => 200, 'message' => 'Lembrete enviado com sucesso.'], 200);
    }

    public function ticketSeguir(Request $request)
    {
        $participante = ArtwPerfisUsuariosTickets
                ::where('id_usuario', $request->input('id_usuario'))
                ->where('id_ticket', $request->input('id_ticket'))
                ->get()->first();

        if (!$participante) {
            $participante = ArtwPerfisUsuariosEquipeTickets
                    ::select('artw_perfis_usuarios_equipe_tickets.*')
                    ->join('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
                    ->where('artw_perfis_usuarios_equipe_tickets.id_usuario', $request->input('id_usuario'))
                    ->where('artw_perfis_usuarios_tickets.id_ticket', $request->input('id_ticket'))
                    ->get()->first();
        }

        if ($participante) {
            $participante->recebe_email = ($request->input('recebe_email') === 'true') ? 1 : 0;
            $participante->save();
        } else {
            return response()->json(['error' => 404, 'message' => 'Você não é participante desse ticket.'], 200);
        }

        return response()->json(['success' => 200, 'message' => ($participante->recebe_email) ? 'Você agora está seguindo esse ticket.' : 'Você deixou de seguir esse ticket.'], 200);
    }

    public function ticketUploadArquivo(Request $request, Common $common)
    {
        $ticket = ArtwTickets::find($request->input('id'));

        if (!$ticket)
        {
            return response()->json(['error' => 404, 'message' => 'Not found'], 404);
        }

        DB::beginTransaction();

        $files = $request->file('files');

        $relativePath = '/images/uploads/artwork/ticket/arquivo/';

        foreach ($files as $file)
        {
            $filename = $file->getClientOriginalName();
            $filename = $common->verificaNomeArquivo($relativePath, $filename);

            $destinationPath = public_path() . $relativePath;
            $mover = $file->move($destinationPath, $filename);

            if ($mover && file_exists(public_path($relativePath . $filename)))
            {
                $arquivo = new ArtwArquivos();
                $arquivo->id_ticket = $ticket->id;
                $arquivo->id_usuario = Auth::user()->id;
                $arquivo->path = $relativePath . $filename;
                $arquivo->descricao = $filename;
                $arquivo->nome_original = $filename;
                $arquivo->save();
            }
            else
            {
                return response()->json(['error' => 404, 'message' => 'Not found'], 404);
            }   
        }

        DB::commit();

        return response()->json(['success' => 200, 'message' => 'Ok'], 200);
    }

    public function ticketRenomearArquivo(Request $request)
    {
        $arquivo = ArtwArquivos::find($request->input('id'));

        if (!$arquivo) {
            return response()->json(['error' => 404, 'message' => 'Not found'], 404);
        }

        DB::beginTransaction();

        $path_parts = pathinfo($arquivo->path);

        $arquivo->descricao = $request->input('novo_nome') . '.' . strtolower($path_parts['extension']);
        $arquivo->save();

        DB::commit();

        return response()->json(['success' => 200, 'message' => 'Ok'], 200);
    }

    public function ticketExcluirArquivo(Request $request)
    {
        $arquivo = ArtwArquivos::find($request->input('id'));

        if (!$arquivo) {
            return response()->json(['error' => 404, 'message' => 'Not found'], 404);
        }

        DB::beginTransaction();

        if (file_exists(public_path($arquivo->path)))
        {
            unlink(public_path($arquivo->path));
        }

        $arquivo->delete();

        DB::commit();

        return response()->json(['success' => 200, 'message' => 'Ok'], 200);
    }
    
    public function ticketCheckFornecedorPdf(Request $request)
    {
        $pdf = ArtwFornecedorPdfs
            ::where('id_ticket', $request->input('id_ticket'))
            ->where('id_fornecedor', $request->input('id_fornecedor'))
            ->whereIn('status', [0,1])
            ->count();
        
        if ($pdf > 0) {
            
            return response()->json(['error' => 201, 'message' => 'Já existe um PDF anexado atribuído a este fornecedor.'], 200);
            
        } else {
            
            return response()->json(['success' => 200, 'message' => 'Ok'], 200);
            
        }
        
    }
    
    public function ticketUploadFornecedorPdf(Request $request, Common $common)
    {
        $ticket = ArtwTickets::find($request->input('id'));

        if (!$ticket) {
            return response()->json(['error' => 404, 'message' => 'Not found'], 404);
        }

        $pdf = ArtwFornecedorPdfs::firstOrNew([
            
            'id_ticket' => $ticket->id,
            'id_fornecedor' => $request->input('fornecedor'),
            'status' => 0
                
                ]);
        
        if ($pdf->exists && $pdf->status != 2) {
            
            return response()->json(['error' => 201, 'message' => 'Já existe um PDF anexado atribuído a este fornecedor.'], 200);
            
        }
        
        DB::beginTransaction();

        $file = $request->file('pdf');

        $relativePath = '/images/uploads/artwork/ticket/fornecedorPdf/';
        
        $versao = $ticket->artwVersoes->last();
        
        if ($versao) {
            
            $numero = $versao->numero;
            
        } else {
            
            $versao_atual = $ticket->artwItem->artwVersoes->last();
            
            $numero = ($versao_atual) ? $versao_atual->numero+1 : 1;
            
        }
        
        $nome = $ticket->sku_nome_completo . ' • Versão ' . $numero;

        $filename = get_filename($file);
        $filename = $common->verificaNomeArquivo($relativePath, $filename);

        $destinationPath = public_path() . $relativePath;
        $file->move($destinationPath, $filename);
        
        //$pdf = new ArtwFornecedorPdfs();
        //$pdf->id_ticket = $ticket->id;
        $pdf->id_usuario = Auth::user()->id;
        $pdf->email_fornecedor = null;
        
        $pdf->path = $relativePath . $filename;
        $pdf->nome = $nome;
        $pdf->status = 0;
        $pdf->nome_arquivo = $filename;
        //$pdf->id_fornecedor = $request->input('fornecedor');
        $pdf->save();

        DB::commit();
        
        /*
        if (config('app.cloudflow')) {

            // Manda renderizar
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, route('api.render', ['fornecedorpdf', $pdf->id]));
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_TIMEOUT_MS, 500);

            curl_exec($ch);

            if (curl_errno($ch)) {
                $error_message = curl_error($ch);
                //echo "<br>erreur:" . $error_message;
            }

            curl_close($ch);
        }
         * 
         */
        
        
        return response()->json(['success' => 200, 'message' => 'Ok'], 200);
    }

    public function ticketUploadArte(Request $request, Common $common)
    {
        $ticket = ArtwTickets::find($request->input('id'));

        if (!$ticket) {
            return response()->json(['error' => 404, 'message' => 'Not found'], 404);
        }

        DB::beginTransaction();

        // deleta artes anteriores
        foreach ($ticket->artwArtes as $arte)
        {
            Storage::disk(config('app.storage'))->delete(config('app.costummer_name') . '/artwork/artes-finais/' . $arte->nome_arquivo);

            $arte->delete();
        }

        $file = $request->file('file0');

        //dd($file);

        //$relativePath = '/images/uploads/artwork/ticket/arte/';

        $nome_arquivo = $ticket->sku_nome_completo;

        if ($request->input('legenda')) {
            $nome_arquivo .= ' - ' . $request->input('legenda');
        }

        $filename = $file->getClientOriginalName();

        $arte = ArtwArtes::firstOrNew([
            'id_ticket' => $ticket->id
        ]);

        if ($arte->exists)
        {
            if (file_exists(public_path($arte->path))) {
                
                unlink(public_path($arte->path));

            }
        }

        // VOLTAR AQUI PRA NÃO DEIXAR SUBIR DUPLICADO
        //$filename = $common->verificaNomeArquivo($relativePath, $filename);

        //$destinationPath = public_path() . $relativePath;
        //$file->move($destinationPath, $filename);

        //dd(config('app.costummer_name') . '/artwork/artes-finais/' . $filename);

        if (Storage::disk(config('app.storage'))->exists(config('app.costummer_name') . '/artwork/artes-finais/' . $filename)) {

            $path_parts = pathinfo($filename);

            $filename = $path_parts['filename'] . '_' . time() . '.' . $path_parts['extension'];
        }

        //dd();

        //$diskTemp = Storage::disk('local');
        //$diskTemp->put($targetFile, file_get_contents($sourceFile));

        //$file = ;

        //dd($file);

        //Storage::disk(config('app.storage'))->put(config('app.costummer_name') . '/artwork/artes-finais/' . $filename, file_get_contents($request->file('file0')));
        Storage::disk(config('app.storage'))->put(config('app.costummer_name') . '/artwork/artes-finais/' . $filename, fopen($request->file('file0'), 'r+'));
        
        $size = Storage::disk(config('app.storage'))->size(config('app.costummer_name') . '/artwork/artes-finais/' . $filename);

        //$arte = new ArtwArtes();
        //$arte->id_ticket = $ticket->id;
        $arte->id_usuario = Auth::user()->id;
        //$arte->path = $relativePath . $filename;
        $arte->nome = $nome_arquivo;
        $arte->nome_arquivo = $filename;
        $arte->descricao = $request->input('legenda');
        $arte->size = $size;
        $arte->storage = 1;
        $arte->save();

        DB::commit();

        return response()->json(['success' => 200, 'message' => 'Ok'], 200);
    }

    public function ticketDeleteArte(Request $request)
    {
        $arte = ArtwArtes::find($request->input('id'));

        if (!$arte) {
            return response()->json(['error' => 404, 'message' => 'Not found'], 404);
        }

        if ($arte->path)
        {
            if (File::isFile(public_path() . $arte->path)) {
                File::delete(public_path() . $arte->path);
            }
        } else {
            Storage::disk(config('app.storage'))->delete(config('app.costummer_name') . '/artwork/artes-finais/' . $arte->nome_arquivo);
        }

        $arte->delete();

        return response()->json(['success' => 200, 'message' => 'Ok'], 200);
    }

    public function ticketUploadLayout(Request $request)
    {
        $ciclo = ArtwCiclos::find($request->input('id'));

        if (!$ciclo) {
            return response()->json(['error' => 404, 'message' => 'Not found'], 404);
        }

        DB::beginTransaction();

        $arquivos = $ciclo->artwCiclosLayoutAll;

        $file = $request->file('file0');

        $relativePath = '/images/uploads/artwork/ciclo/';

        $extension = $file->getClientOriginalExtension();

        $nome_arquivo = strtoupper($extension) . ' ' . $ciclo->artwTicket->sku_nome_completo;
        $nome_arquivo2 = $ciclo->artwTicket->sku_nome_completo;

        $nome_arquivo .= ' - Ciclo ' . $ciclo->numero;
        $nome_arquivo2 .= ' - Ciclo ' . $ciclo->numero;

        $numero = (count($arquivos)) ? count($arquivos) + 1 : 1;

        $filename = $ciclo->artwTicket->numero . '-' . str_slug($nome_arquivo2) . "-v" . $numero . "." . $extension;

        $destinationPath = public_path() . $relativePath;
        $file->move($destinationPath, $filename);

        $cicloLayout = new ArtwCiclosLayout;
        $cicloLayout->id_ciclo = $ciclo->id;
        $cicloLayout->id_usuario = Auth::user()->id;
        $cicloLayout->nome = $nome_arquivo;
        $cicloLayout->path = $relativePath . $filename;
        $cicloLayout->save();

        if ($arquivos && $arquivos->last()) {

            if (file_exists(public_path($arquivos->last()->path))) {
                unlink(public_path($arquivos->last()->path));
                $arquivos->last()->delete();
            }
        }

        DB::commit();

        /*
        if (config('app.cloudflow')) {

            // Manda renderizar
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, route('api.render', ['ciclo', $cicloLayout->id]));
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_TIMEOUT_MS, 500);

            curl_exec($ch);

            if (curl_errno($ch)) {
                $error_message = curl_error($ch);
                //echo "<br>erreur:" . $error_message;
            }

            curl_close($ch);
        }
         * 
         */

        return response()->json(['success' => 200, 'message' => 'Ok'], 200);
    }

    public function upload(Request $request)
    {
        $file = $request->file('file1');

        $relativePath = '/images/uploads/artwork/ticket/fotos/';
        $extension = $file->getClientOriginalExtension();
        $hash = md5(uniqid(rand(), true));
        $filename = $hash . "." . $extension;

        $destinationPath = public_path() . $relativePath;
        $file->move($destinationPath, $filename);

        $url = $relativePath . $filename;

        return response()->json(['url' => asset($url), 'success' => 200, 'message' => 'Ok'], 200);
    }

    public function comentar(Request $request, Common $common)
    {
        //dd($request->all());

        if (!$request->input('id') || !$request->input('texto')) {
            return response()->json(['error' => 400, 'message' => 'Parâmetros incorretos'], 400);
        }

        if ($request->input('tipo') == 'geral') {
            $ticket = ArtwTickets::find($request->input('id'));
        } else {
            $solicitacao = ArtwSolicitacoes::find($request->input('id'));

            if (!$solicitacao) {
                return response()->json(['error' => 404, 'message' => 'Not found'], 404);
            }

            $ticket = $solicitacao->artwCiclo->artwTicket;
        }

        $membros = ($request->input('enviar_membros')) ?: [];

        $array_envios = null;

        foreach ($membros as $membro) {
            if (isset($membro['selected']) && $membro['selected'] == 'true') {
                //$common->enviarNotificacao($ticket, 'comentario', 'dono', $request->input('texto'), 1);
                $common->enviarNotificacao($ticket, 'comentario', $membro['id_usuario'], $request->input('texto'), 1);

                $usuario = AdmUsuarios::find($membro['id_usuario']);

                $array_envios[] = [
                    'nome' => $usuario->nome_completo,
                    'depto' => ($usuario->admDepartamento) ? $usuario->admDepartamento->nome : null,
                    'empresa' => ($usuario->admEmpresa) ? $usuario->admEmpresa->nome : null,
                ];
            }

            if (isset($membro['equipe'])) {
                foreach ($membro['equipe'] as $equipe) {
                    if (isset($equipe['selected']) && $equipe['selected'] == 'true') {
                        $common->enviarNotificacao($ticket, 'comentario', $equipe['id_usuario'], $request->input('texto'), 1);

                        $usuario = AdmUsuarios::find($equipe['id_usuario']);

                        $array_envios[] = [
                            'nome' => $usuario->nome_completo,
                            'depto' => ($usuario->admDepartamento) ? $usuario->admDepartamento->nome : null,
                            'empresa' => ($usuario->admEmpresa) ? $usuario->admEmpresa->nome : null,
                        ];
                    }
                }
            }
        }

        $cores = [
            '#FF0000',
            '#33A552',
            '#4085F2',
            '#F9BA05',
            '#EA7233',
            '#EA4333'
        ];

        shuffle($cores);

        $comentario = new ArtwComentarios();
        $comentario->id_solicitacao = ($request->input('tipo') !== 'geral') ? $solicitacao->id : null;
        $comentario->id_ticket = ($request->input('tipo') === 'geral') ? $ticket->id : null;
        $comentario->id_usuario = Auth::user()->id;
        $comentario->texto = $request->input('texto');
        $comentario->cor = $cores[0];
        $comentario->array_envios = ($array_envios) ? json_encode($array_envios) : null;
        $comentario->save();

        return response()->json(['success' => 200, 'message' => 'Ok'], 200);
    }

    public function responder(Request $request, Common $common)
    {
        if (!$request->input('id') || !$request->input('texto')) {
            return response()->json(['error' => 400, 'message' => 'Parâmetros incorretos'], 400);
        }

        $comentario = ArtwComentarios::find($request->input('id'));

        if (!$comentario) {
            return response()->json(['error' => 404, 'message' => 'Not found'], 404);
        }

        if ($request->input('tipo') == 'geral') {
            $ticket = $comentario->artwTicket;
        } else {
            $ticket = $comentario->artwSolicitacao->artwCiclo->artwTicket;
        }

        $membros = ($request->input('enviar_membros')) ?: [];

        foreach ($membros as $membro) {
            if (isset($membro['selected']) && $membro['selected'] == 'true') {
                $common->enviarNotificacao($ticket, 'resposta', $membro['id_usuario'], $request->input('texto'), 1);

                $usuario = AdmUsuarios::find($membro['id_usuario']);

                $array_envios[] = [
                    'nome' => $usuario->nome_completo,
                    'depto' => ($usuario->admDepartamento) ? $usuario->admDepartamento->nome : null,
                    'empresa' => ($usuario->admEmpresa) ? $usuario->admEmpresa->nome : null,
                ];                
            }

            if (isset($membro['equipe'])) {
                foreach ($membro['equipe'] as $equipe) {
                    if (isset($equipe['selected']) && $equipe['selected'] == 'true') {
                        $common->enviarNotificacao($ticket, 'comentario', $equipe['id_usuario'], $request->input('texto'), 1);

                        $usuario = AdmUsuarios::find($equipe['id_usuario']);

                        $array_envios[] = [
                            'nome' => $usuario->nome_completo,
                            'depto' => ($usuario->admDepartamento) ? $usuario->admDepartamento->nome : null,
                            'empresa' => ($usuario->admEmpresa) ? $usuario->admEmpresa->nome : null,
                        ];
                    }
                }
            }
        }

        $cores = [
            '#FF0000',
            '#33A552',
            '#4085F2',
            '#F9BA05',
            '#EA7233',
            '#EA4333'
        ];

        shuffle($cores);

        $resposta = new ArtwRespostas();
        $resposta->id_comentario = $comentario->id;
        $resposta->id_usuario = Auth::user()->id;
        $resposta->texto = $request->input('texto');
        $resposta->cor = $cores[0];
        $resposta->array_envios = (isset($array_envios) && $array_envios) ? json_encode($array_envios) : null;
        $resposta->save();

        return response()->json(['success' => 200, 'message' => 'Ok'], 200);
    }

    public function ticketTimeline($etapa, $id_ticket)
    {
        //sleep(5);

        $ticket = ArtwTickets::find($id_ticket);

        $etapa_participantes = [];

        if ($etapa == 'aberto')
        {
            $statusUsuario = ArtwTicketsStatus::where('id_ciclo', $ticket->ciclo_atual()->id)->orderBy('created_at', 'ASC')->where('id_status', 1)->limit(1)->get()->first();

            $participantes[0] = [
                'nome' => $ticket->admUsuario->nome_abreviado,
                'empresa' => $ticket->admUsuario->admEmpresa->nome,
                'departamento' => $ticket->admUsuario->admDepartamento->nome,
                'icone' => ($ticket->id_status != 0 && $ticket->id_status != 9) ? 'fa-check' : 'fa-user',
                'dt_tarefa_concluida' => ($statusUsuario) ? $statusUsuario->created_at->format('H\hi \d\e d/m/Y') : null
            ];

            $etapa_participantes = [
                'titulo' => 'Aberto',
                'subtitulo' => 'Preencher informações iniciais',
                'icone' => 'fa-flag',
                'etapa_ativa' => ($ticket->id_status != 0 && $ticket->id_status != 9) ? 1 : 0,
                'participantes' => $participantes
            ];

        }
        elseif ($etapa == 'edicao')
        {
            $layout = $ticket->ciclo_atual()->artwCiclosLayout->last();

            $subetapa = ($ticket->id_status > 0 && $layout) ? 2 : 1;
            
            $ticketParticipantes = $ticket->participantesPorPerfilWithTrashed(5);

            $participantes = [];

            if ($subetapa == 1) {

                foreach ($ticketParticipantes as $ticketParticipante) {

                    $participantes[] = [
                        'nome' => $ticketParticipante->admUsuario->nome_abreviado,
                        'empresa' => $ticketParticipante->admUsuario->admEmpresa->nome,
                        'departamento' => $ticketParticipante->admUsuario->admDepartamento->nome,
                        'icone' => 'fa-user',
                        'dt_tarefa_concluida' => null
                    ];

                }

                $etapa_participantes = [
                    'titulo' => 'Edição',
                    'subtitulo' => 'Anexar layout editado',
                    'icone' => 'fa-paint-brush',
                    'etapa_ativa' => ($ticket->id_status != 0 && $ticket->id_status != 9 && $ticket->id_status >= 1) ? 1 : 0,
                    'participantes' => $participantes
                ];

            } else {

                $statusUsuario = ArtwTicketsStatus::where('id_ciclo', $ticket->ciclo_atual()->id)->orderBy('created_at', 'ASC')->where('id_status', 2)->limit(1)->get()->first();

                if ($statusUsuario) {

                    $participantes[0] = [
                        'nome' => $statusUsuario->admUsuario->nome_abreviado,
                        'empresa' => $statusUsuario->admUsuario->admEmpresa->nome,
                        'departamento' => $statusUsuario->admUsuario->admDepartamento->nome,
                        'icone' => 'fa-check',
                        'dt_tarefa_concluida' => ($statusUsuario) ? $statusUsuario->created_at->format('H\hi \d\e d/m/Y') : null
                    ];

                } else {

                    foreach ($ticketParticipantes as $ticketParticipante) {

                        $participantes[] = [
                            'nome' => $ticketParticipante->admUsuario->nome_abreviado,
                            'empresa' => $ticketParticipante->admUsuario->admEmpresa->nome,
                            'departamento' => $ticketParticipante->admUsuario->admDepartamento->nome,
                            'icone' => 'fa-user',
                            'dt_tarefa_concluida' => null
                        ];
    
                    }

                }

                $etapa_participantes = [
                    'titulo' => 'Edição',
                    'subtitulo' => 'Submeter à revisão',
                    'icone' => 'fa-paint-brush',
                    'etapa_ativa' => ($ticket->id_status != 0 && $ticket->id_status != 9 && $ticket->id_status >= 1) ? 1 : 0,
                    'participantes' => $participantes
                ];

            }

        }
        elseif ($etapa == 'revisao')
        {
            $naoRevisado = $ticket->ciclo_atual()->artwSolicitacoes()->where('id_status', 1)->get();

            $subetapa = ($naoRevisado->count() == 0) ? 2 : 1;

            $ticketParticipantes = $ticket->participantesPorPerfilWithTrashed(3);

            $participantes = [];

            if ($subetapa == 1) {

                foreach ($ticketParticipantes as $ticketParticipante) {

                    $participantes[] = [
                        'nome' => $ticketParticipante->admUsuario->nome_abreviado,
                        'empresa' => $ticketParticipante->admUsuario->admEmpresa->nome,
                        'departamento' => $ticketParticipante->admUsuario->admDepartamento->nome,
                        'icone' => 'fa-user',
                        'dt_tarefa_concluida' => null
                    ];

                }

                $etapa_participantes = [
                    'titulo' => 'Revisão',
                    'subtitulo' => 'Revisar layout editado',
                    'icone' => 'fa-eye',
                    'etapa_ativa' => ($ticket->id_status != 0 && $ticket->id_status != 9 && $ticket->id_status >= 2) ? 1 : 0,
                    'participantes' => $participantes
                ];

            } else {

                $statusUsuario = ArtwTicketsStatus::where('id_ciclo', $ticket->ciclo_atual()->id)->orderBy('created_at', 'ASC')->where('id_status', 3)->limit(1)->get()->first();

                if ($statusUsuario) {

                    $participantes[0] = [
                        'nome' => $statusUsuario->admUsuario->nome_abreviado,
                        'empresa' => $statusUsuario->admUsuario->admEmpresa->nome,
                        'departamento' => $statusUsuario->admUsuario->admDepartamento->nome,
                        'icone' => 'fa-check',
                        'dt_tarefa_concluida' => ($statusUsuario) ? $statusUsuario->created_at->format('H\hi \d\e d/m/Y') : null
                    ];

                } else {

                    foreach ($ticketParticipantes as $ticketParticipante) {

                        $participantes[] = [
                            'nome' => $ticketParticipante->admUsuario->nome_abreviado,
                            'empresa' => $ticketParticipante->admUsuario->admEmpresa->nome,
                            'departamento' => $ticketParticipante->admUsuario->admDepartamento->nome,
                            'icone' => 'fa-user',
                            'dt_tarefa_concluida' => null
                        ];
    
                    }

                }

                $etapa_participantes = [
                    'titulo' => 'Revisão',
                    'subtitulo' => 'Submeter à aprovação',
                    'icone' => 'fa-eye',
                    'etapa_ativa' => ($ticket->id_status != 0 && $ticket->id_status != 9 && $ticket->id_status >= 2) ? 1 : 0,
                    'participantes' => $participantes
                ];

            }

        }
        elseif ($etapa == 'primeira-aprovacao')
        {
            $ticketParticipantes = $ticket->participantesPorPerfilWithTrashed(2);

            $participantes = [];

            foreach ($ticketParticipantes as $ticketParticipante) {

                $participanteMembros = $ticketParticipante->artwPerfisUsuariosEquipeTickets;

                $membros = [];

                foreach ($participanteMembros as $participanteMembro) {

                    $membros[] = [
                        'nome' => $participanteMembro->admUsuario->nome_abreviado,
                        'empresa' => $participanteMembro->admUsuario->admEmpresa->nome,
                        'departamento' => $participanteMembro->admUsuario->admDepartamento->nome,
                    ];

                }

                if ($ticketParticipante->membroAprovador && ( $ticketParticipante->aprovado==1 || ($ticketParticipante->aprovado==2 && ( ($ticketParticipante->ciclo <= $ticket->ciclo_atual()->numero && $ticket->id_status < 3) || ($ticketParticipante->ciclo == $ticket->ciclo_atual()->numero && $ticket->id_status == 3) ) ) ) ) {

                    if (!$ticketParticipante->deleted_at || ($ticketParticipante->deleted_at && $ticketParticipante->ciclo >= $ticket->ciclo_atual()->numero) ){
                        $participantes[] = [
                            'nome' => $ticketParticipante->membroAprovador->admUsuario->nome_abreviado,
                            'empresa' => $ticketParticipante->membroAprovador->admUsuario->admEmpresa->nome,
                            'departamento' => $ticketParticipante->membroAprovador->admUsuario->admDepartamento->nome,
                            'icone' => ($ticketParticipante->aprovado==1)?'fa-check':
                                            ( ($ticketParticipante->aprovado==2 && $ticket->id_status != 3) || ($ticketParticipante->aprovado==2 && $ticketParticipante->ciclo >= $ticket->ciclo_atual()->numero ) ? 'fa-ban' :
                                            (($membros) ? 'fa-users' : 'fa-user')),
                            'dt_tarefa_concluida' => ( $ticketParticipante->aprovado==1 || ($ticketParticipante->aprovado==2 && $ticketParticipante->ciclo >= $ticket->ciclo_atual()->numero ) ) ? $ticketParticipante->updated_at->format('H\hi \d\e d/m/Y') : null,
                        ];
                    }

                } else {

                    if (!$ticketParticipante->deleted_at || ($ticketParticipante->deleted_at && $ticketParticipante->ciclo >= $ticket->ciclo_atual()->numero) ){
                        $participantes[] = [
                            'nome' => $ticketParticipante->admUsuario->nome_abreviado,
                            'empresa' => $ticketParticipante->admUsuario->admEmpresa->nome,
                            'departamento' => $ticketParticipante->admUsuario->admDepartamento->nome,
                            'icone' => ($ticketParticipante->aprovado==1)?'fa-check':
                                            ( ($ticketParticipante->aprovado==2 && $ticket->id_status != 3) || ($ticketParticipante->aprovado==2 && $ticketParticipante->ciclo >= $ticket->ciclo_atual()->numero ) ? 'fa-ban' :
                                            (($membros) ? 'fa-users' : 'fa-user')),
                            'dt_tarefa_concluida' => ( $ticketParticipante->aprovado==1 || ($ticketParticipante->aprovado==2 && $ticketParticipante->ciclo >= $ticket->ciclo_atual()->numero ) ) ? $ticketParticipante->updated_at->format('H\hi \d\e d/m/Y') : null,
                            'membros' => ( $ticketParticipante->aprovado==1 || ($ticketParticipante->aprovado==2 && $ticketParticipante->ciclo >= $ticket->ciclo_atual()->numero ) ) ? [] : $membros
                        ];
                    }

                }

            }
            
            $etapa_participantes = [
                'titulo' => 'Primeira Aprovação',
                'subtitulo' => 'Aprovar',
                'icone' => 'fa-thumbs-up',
                'etapa_ativa' => ($ticket->id_status != 0 && $ticket->id_status != 9 && $ticket->id_status >= 3) ? 1 : 0,
                'participantes' => $participantes
            ];

        }
        elseif ($etapa == 'demais-aprovacoes')
        {
            $ticketParticipantes = $ticket->participantesPorPerfilWithTrashed(1);

            $participantes = [];

            foreach ($ticketParticipantes as $ticketParticipante) {

                $participanteMembros = $ticketParticipante->artwPerfisUsuariosEquipeTickets;

                $membros = [];

                foreach ($participanteMembros as $participanteMembro) {

                    $membros[] = [
                        'nome' => $participanteMembro->admUsuario->nome_abreviado,
                        'empresa' => $participanteMembro->admUsuario->admEmpresa->nome,
                        'departamento' => $participanteMembro->admUsuario->admDepartamento->nome,
                    ];

                }

                if ($ticketParticipante->membroAprovador && ( $ticketParticipante->aprovado==1 || ($ticketParticipante->aprovado==2 && ( ($ticketParticipante->ciclo <= $ticket->ciclo_atual()->numero && $ticket->id_status < 4) || ($ticketParticipante->ciclo == $ticket->ciclo_atual()->numero && $ticket->id_status == 4) ) ) ) ) {

                    if (!$ticketParticipante->deleted_at || ($ticketParticipante->deleted_at && $ticketParticipante->ciclo >= $ticket->ciclo_atual()->numero) ){
                        $participantes[] = [
                            'nome' => $ticketParticipante->membroAprovador->admUsuario->nome_abreviado,
                            'empresa' => $ticketParticipante->membroAprovador->admUsuario->admEmpresa->nome,
                            'departamento' => $ticketParticipante->membroAprovador->admUsuario->admDepartamento->nome,
                            'icone' => ($ticketParticipante->aprovado==1)?'fa-check':
                                            ( ($ticketParticipante->aprovado==2 && $ticket->id_status != 4) || ($ticketParticipante->aprovado==2 && $ticketParticipante->ciclo >= $ticket->ciclo_atual()->numero ) ? 'fa-ban' :
                                            (($membros) ? 'fa-users' : 'fa-user')),
                            'dt_tarefa_concluida' => ( $ticketParticipante->aprovado==1 || ($ticketParticipante->aprovado==2 && $ticketParticipante->ciclo >= $ticket->ciclo_atual()->numero ) ) ? $ticketParticipante->updated_at->format('H\hi \d\e d/m/Y') : null,
                        ];
                    }

                } else {

                    if (!$ticketParticipante->deleted_at || ($ticketParticipante->deleted_at && $ticketParticipante->ciclo >= $ticket->ciclo_atual()->numero) ){
                        $participantes[] = [
                            'nome' => $ticketParticipante->admUsuario->nome_abreviado,
                            'empresa' => $ticketParticipante->admUsuario->admEmpresa->nome,
                            'departamento' => $ticketParticipante->admUsuario->admDepartamento->nome,
                            'icone' => ($ticketParticipante->aprovado==1)?'fa-check':
                                            ( ($ticketParticipante->aprovado==2 && $ticket->id_status != 4) || ($ticketParticipante->aprovado==2 && $ticketParticipante->ciclo >= $ticket->ciclo_atual()->numero ) ? 'fa-ban' :
                                            (($membros) ? 'fa-users' : 'fa-user')),
                            'dt_tarefa_concluida' => ( $ticketParticipante->aprovado==1 || ($ticketParticipante->aprovado==2 && $ticketParticipante->ciclo >= $ticket->ciclo_atual()->numero ) ) ? $ticketParticipante->updated_at->format('H\hi \d\e d/m/Y') : null,
                            'membros' => ( $ticketParticipante->aprovado==1 || ($ticketParticipante->aprovado==2 && $ticketParticipante->ciclo >= $ticket->ciclo_atual()->numero ) ) ? [] : $membros
                        ];
                    }
                }

            }

            $etapa_participantes = [
                'titulo' => 'Demais Aprovações',
                'subtitulo' => 'Aprovar',
                'icone' => 'fa-gavel',
                'etapa_ativa' => ($ticket->id_status != 0 && $ticket->id_status != 9 && $ticket->id_status >= 4) ? 1 : 0,
                'participantes' => $participantes
            ];

        }
        elseif ($etapa == 'finalizacao')
        {
            $arte = $ticket->artwArtes->last();

            $subetapa = ($arte) ? 2 : 1;

            $ticketParticipantes = $ticket->participantesPorPerfilWithTrashed(5);

            $participantes = [];

            if ($subetapa == 1) {

                foreach ($ticketParticipantes as $ticketParticipante) {

                    $participantes[] = [
                        'nome' => $ticketParticipante->admUsuario->nome_abreviado,
                        'empresa' => $ticketParticipante->admUsuario->admEmpresa->nome,
                        'departamento' => $ticketParticipante->admUsuario->admDepartamento->nome,
                        'icone' => 'fa-user',
                        'dt_tarefa_concluida' => null
                    ];

                }

                $etapa_participantes = [
                    'titulo' => 'Finalização',
                    'subtitulo' => 'Anexar arte-final',
                    'icone' => 'fa-paint-brush',
                    'etapa_ativa' => ($ticket->id_status != 0 && $ticket->id_status != 9 && $ticket->id_status >= 5) ? 1 : 0,
                    'participantes' => $participantes
                ];

            } else {

                $usuarioAutorizador = $ticket->usuarioAutorizador;

                if ($usuarioAutorizador) {

                    $participantes[0] = [
                        'nome' => $usuarioAutorizador->nome_abreviado,
                        'empresa' => $usuarioAutorizador->admEmpresa->nome,
                        'departamento' => $usuarioAutorizador->admDepartamento->nome,
                        'icone' => 'fa-check',
                        'dt_tarefa_concluida' => ($usuarioAutorizador) ? $ticket->dt_envio_autorizado->format('H\hi \d\e d/m/Y') : null
                    ];

                } else {

                    foreach ($ticketParticipantes as $ticketParticipante) {

                        $participantes[] = [
                            'nome' => $ticketParticipante->admUsuario->nome_abreviado,
                            'empresa' => $ticketParticipante->admUsuario->admEmpresa->nome,
                            'departamento' => $ticketParticipante->admUsuario->admDepartamento->nome,
                            'icone' => 'fa-user',
                            'dt_tarefa_concluida' => null
                        ];
    
                    }

                }

                $etapa_participantes = [
                    'titulo' => 'Finalização',
                    'subtitulo' => 'Finalizar anexação de arte-final',
                    'icone' => 'fa-paint-brush',
                    'etapa_ativa' => ($ticket->id_status != 0 && $ticket->id_status != 9 && $ticket->id_status >= 5) ? 1 : 0,
                    'participantes' => $participantes
                ];

            }

        }
        elseif ($etapa == 'envio-arte')
        {
            $arteEnvios = $ticket->artwArtesEnvios;

            $subetapa = ($arteEnvios->count()) ? 2 : 1;

            $ticketParticipantes = $ticket->participantesPorPerfilWithTrashed(6);

            $participantes = [];

            if ($subetapa == 1) {

                foreach ($ticketParticipantes as $ticketParticipante) {

                    $participantes[] = [
                        'nome' => $ticketParticipante->admUsuario->nome_abreviado,
                        'empresa' => $ticketParticipante->admUsuario->admEmpresa->nome,
                        'departamento' => $ticketParticipante->admUsuario->admDepartamento->nome,
                        'icone' => 'fa-user',
                        'dt_tarefa_concluida' => null
                    ];

                }

                $etapa_participantes = [
                    'titulo' => 'Envio da Arte',
                    'subtitulo' => 'Enviar arte-final para o fornecedor',
                    'icone' => 'fa-paper-plane',
                    'etapa_ativa' => ($ticket->id_status != 0 && $ticket->id_status != 9 && $ticket->id_status >= 10) ? 1 : 0,
                    'participantes' => $participantes
                ];

            } else {

                $usuarioFinalizador = $ticket->usuarioFinalizador;

                if ($usuarioFinalizador) {

                    $participantes[0] = [
                        'nome' => $usuarioFinalizador->nome_abreviado,
                        'empresa' => $usuarioFinalizador->admEmpresa->nome,
                        'departamento' => $usuarioFinalizador->admDepartamento->nome,
                        'icone' => 'fa-check',
                        'dt_tarefa_concluida' => ($usuarioFinalizador) ? $ticket->dt_envio_finalizado->format('H\hi \d\e d/m/Y') : null
                    ];

                } else {

                    foreach ($ticketParticipantes as $ticketParticipante) {

                        $participantes[] = [
                            'nome' => $ticketParticipante->admUsuario->nome_abreviado,
                            'empresa' => $ticketParticipante->admUsuario->admEmpresa->nome,
                            'departamento' => $ticketParticipante->admUsuario->admDepartamento->nome,
                            'icone' => 'fa-user',
                            'dt_tarefa_concluida' => null
                        ];
    
                    }

                }

                $etapa_participantes = [
                    'titulo' => 'Envio da Arte',
                    'subtitulo' => 'Declarar arte-final como enviada',
                    'icone' => 'fa-paper-plane',
                    'etapa_ativa' => ($ticket->id_status != 0 && $ticket->id_status != 9 && $ticket->id_status >= 10) ? 1 : 0,
                    'participantes' => $participantes
                ];

            }

        }
        elseif ($etapa == 'pdf-fornecedor')
        {
            $fornecedorPdfsAprovados = $ticket->fornecedorPdfs()->whereIn('status', [1]);

            $subetapa = ($fornecedorPdfsAprovados->count()) ? 2 : 1;

            $ticketParticipantes = $ticket->participantesPorPerfilWithTrashed(7);

            $participantes = [];

            if ($subetapa == 1) {

                foreach ($ticketParticipantes as $ticketParticipante) {

                    $participantes[] = [
                        'nome' => $ticketParticipante->admUsuario->nome_abreviado,
                        'empresa' => $ticketParticipante->admUsuario->admEmpresa->nome,
                        'departamento' => $ticketParticipante->admUsuario->admDepartamento->nome,
                        'icone' => 'fa-user',
                        'dt_tarefa_concluida' => null
                    ];

                }

                $etapa_participantes = [
                    'titulo' => 'PDF do Fornecedor',
                    'subtitulo' => 'Anexar e aprovar o PDF do Fornecedor',
                    'icone' => 'fa-industry',
                    'etapa_ativa' => ($ticket->id_status != 0 && $ticket->id_status != 9 && $ticket->id_status >= 11) ? 1 : 0,
                    'participantes' => $participantes
                ];

            } else {

                $fornecedorPdf = $ticket->artwPerfisUsuariosTickets()->where('id_perfil', 7)->where('aprovado', 1)->get()->first();

                if ($fornecedorPdf && $ticket->id_status != 11) {

                    $participantes[0] = [
                        'nome' => $fornecedorPdf->admUsuario->nome_abreviado,
                        'empresa' => $fornecedorPdf->admUsuario->admEmpresa->nome,
                        'departamento' => $fornecedorPdf->admUsuario->admDepartamento->nome,
                        'icone' => 'fa-check',
                        'dt_tarefa_concluida' => ($fornecedorPdf) ? $fornecedorPdf->updated_at->format('H\hi \d\e d/m/Y') : null
                    ];

                } else {

                    foreach ($ticketParticipantes as $ticketParticipante) {

                        $participantes[] = [
                            'nome' => $ticketParticipante->admUsuario->nome_abreviado,
                            'empresa' => $ticketParticipante->admUsuario->admEmpresa->nome,
                            'departamento' => $ticketParticipante->admUsuario->admDepartamento->nome,
                            'icone' => 'fa-user',
                            'dt_tarefa_concluida' => null
                        ];
    
                    }

                }

                $etapa_participantes = [
                    'titulo' => 'PDF do Fornecedor',
                    'subtitulo' => 'Finalizar o PDF do Fornecedor',
                    'icone' => 'fa-industry',
                    'etapa_ativa' => ($ticket->id_status != 0 && $ticket->id_status != 9 && $ticket->id_status >= 11) ? 1 : 0,
                    'participantes' => $participantes
                ];

            }

        }
        elseif ($etapa == 'encerramento')
        {
            $statusUsuario = ArtwTicketsStatus::where('id_ciclo', $ticket->ciclo_atual()->id)->orderBy('created_at', 'ASC')->where('id_status', 6)->limit(1)->get()->first();

            $participantes[0] = [
                'nome' => $ticket->admUsuario->nome_abreviado,
                'empresa' => $ticket->admUsuario->admEmpresa->nome,
                'departamento' => $ticket->admUsuario->admDepartamento->nome,
                'icone' => ($ticket->id_status == 6) ? 'fa-check' : 'fa-user',
                'dt_tarefa_concluida' => ($statusUsuario) ? $statusUsuario->created_at->format('H\hi \d\e d/m/Y') : null
            ];

            $etapa_participantes = [
                'titulo' => 'Encerramento',
                'subtitulo' => 'Encerrar ticket',
                'icone' => 'fa-flag-checkered',
                'etapa_ativa' => ($ticket->id_status == 9) ? 1 : 0,
                'participantes' => $participantes
            ];

        }
        
        return $etapa_participantes;
    }

    public function getFornecedorPdfToken(Request $request)
    {

        $token = $request->input('token');

        $arteEnvioEmail = ArtwArtesEnviosEmails::where('token', $token)->get()->first();

        $ticket = $arteEnvioEmail->artwArteEnvio->artwArte->artwTicket;

        $fornecedor = $arteEnvioEmail->artwArteEnvio->fornecedor;

        $fornecedorPdfs = ArtwFornecedorPdfs::where('id_ticket', $ticket->id)
                    ->where('id_fornecedor', $fornecedor->id)
                    ->whereIn('status', [0,1,3])
                    ->get();
        
        $response = [];

        foreach ($fornecedorPdfs as $k => $pdf) {
            
            $response['fornecedorPdfs'][$k]['id'] = $pdf->id;
            $response['fornecedorPdfs'][$k]['nome_usuario'] = ($pdf->admUsuario) ? $pdf->admUsuario->nome_abreviado : $pdf->email_fornecedor;
            $response['fornecedorPdfs'][$k]['data_criacao'] = $pdf->created_at->format('H\hi \d\e d/m/Y');
            $response['fornecedorPdfs'][$k]['id_aprovacao'] = ($pdf->usuarioAprovacao) ? $pdf->usuarioAprovacao->id : '';
            $response['fornecedorPdfs'][$k]['nome_aprovacao'] = ($pdf->usuarioAprovacao) ? $pdf->usuarioAprovacao->nome_abreviado : '';
            $response['fornecedorPdfs'][$k]['data_aprovacao'] = ($pdf->dt_aprovacao) ? $pdf->dt_aprovacao->format('H\hi \d\e d/m/Y') : '';
            //$response['fornecedorPdfs'][$k]['etapa'] = ($versao && $pdf->created_at > $versao->created_at) ? 2 : 1;
            $response['fornecedorPdfs'][$k]['nome'] = $pdf->nome;
            //$response['fornecedorPdfs'][$k]['link_comparar'] = ($versao_atual) ? route('site.artwork.compareFornecedorPdf', $pdf->id) : '';
            //$response['fornecedorPdfs'][$k]['link_comparar'] = route('site.artwork.compareFornecedorPdf', $pdf->id);
            $response['fornecedorPdfs'][$k]['status'] = $pdf->status;
            $response['fornecedorPdfs'][$k]['fornecedor'] = $pdf->fornecedor->nome;
            $response['fornecedorPdfs'][$k]['nome_arquivo'] = $pdf->nome_arquivo;
            $response['fornecedorPdfs'][$k]['filesize'] = $pdf->filesize;
            //$response['fornecedorPdfs'][$k]['path'] = asset($pdf->path);
            $response['fornecedorPdfs'][$k]['path'] = route('downloadAbrirPdfFornecedor', $pdf->id) . '?token=' . $token;
            //$response['fornecedorPdfs'][$k]['baixarPdf'] = route('site.artwork.baixarFornecedorPdf', $pdf->id);
            
        }

        return $response;

    }

    public function postEnviarFornecedorPdfCliente(Request $request, Common $common)
    {

        $token = $request->input('token');

        $arteEnvioEmail = ArtwArtesEnviosEmails::where('token', $token)->get()->first();

        $ticket = $arteEnvioEmail->artwArteEnvio->artwArte->artwTicket;

        $fornecedor = $arteEnvioEmail->artwArteEnvio->fornecedor;

        $fornecedorPdf = ArtwFornecedorPdfs::where('id_ticket', $ticket->id)
                    ->where('id_fornecedor', $fornecedor->id)
                    ->whereIn('status', [0,1,3])
                    ->get()->first();

        if ($fornecedorPdf->status != 3) {

            return response()->json(['success' => 201, 'message' => 'PDF do Fornecedor já enviado'], 201);    

        }
        
        $fornecedorPdf->status = 0;
        $fornecedorPdf->save();

        $texto = 'O Fornecedor ' . $fornecedor->nome . ' anexou o PDF no ticket.';

        if ($ticket->id_status == 6)
        {

            // to-do enviar
            $common->enviarNotificacao($ticket, 'fornecedor_subiu_pdf', $arteEnvioEmail->artwArteEnvio->id_usuario, $texto, 1);

        } else {

            $common->enviarNotificacao($ticket, 'fornecedor_subiu_pdf', 'pdf-fornecedor', $texto, 1);

        }

        return response()->json(['success' => 200, 'message' => 'Ok'], 200);

    }

    public function postExcluirFornecedorPdfCliente(Request $request)
    {

        $token = $request->input('token');

        $arteEnvioEmail = ArtwArtesEnviosEmails::where('token', $token)->get()->first();

        $ticket = $arteEnvioEmail->artwArteEnvio->artwArte->artwTicket;

        $fornecedor = $arteEnvioEmail->artwArteEnvio->fornecedor;

        $fornecedorPdf = ArtwFornecedorPdfs::where('id_ticket', $ticket->id)
                    ->where('id_fornecedor', $fornecedor->id)
                    ->whereIn('status', [0,1,3])
                    ->get()->first();

        if ($fornecedorPdf->status != 3) {

            return response()->json(['success' => 201, 'message' => 'PDF do Fornecedor já enviado'], 201);    

        }
        
        $fornecedorPdf->delete();

        return response()->json(['success' => 200, 'message' => 'Ok'], 200);

    }

    public function postUploadFornecedorPdf(Request $request, Common $common)
    {

        $token = $request->input('token');

        $arteEnvioEmail = ArtwArtesEnviosEmails::where('token', $token)->get()->first();

        $ticket = $arteEnvioEmail->artwArteEnvio->artwArte->artwTicket;

        $fornecedor = $arteEnvioEmail->artwArteEnvio->fornecedor;

        $pdf = ArtwFornecedorPdfs::firstOrNew([
            
            'id_ticket' => $ticket->id,
            'id_fornecedor' => $fornecedor->id,                
            ]);
        
        if ($pdf->exists && ($pdf->status == 0 || $pdf->status == 1)) {
            
            return response()->json(['error' => 201, 'message' => 'Já existe um PDF anexado.'], 200);
            
        }
        
        DB::beginTransaction();

        $file = $request->file('pdf');

        $relativePath = '/images/uploads/artwork/ticket/fornecedorPdf/';
        
        $versao = $ticket->artwVersoes->last();
        
        if ($versao) {
            
            $numero = $versao->numero;
            
        } else {
            
            $versao_atual = $ticket->artwItem->artwVersoes->last();
            
            $numero = ($versao_atual) ? $versao_atual->numero+1 : 1;
            
        }
        
        $nome = $ticket->sku_nome_completo . ' • Versão ' . $numero;

        $filename = get_filename($file);
        $filename = $common->verificaNomeArquivo($relativePath, $filename);

        $destinationPath = public_path() . $relativePath;
        $file->move($destinationPath, $filename);
        
        //$pdf = new ArtwFornecedorPdfs();
        //$pdf->id_ticket = $ticket->id;
        $pdf->id_usuario = null;
        $pdf->email_fornecedor = $arteEnvioEmail->email;
        
        $pdf->path = $relativePath . $filename;
        $pdf->nome = $nome;
        $pdf->status = 3;
        $pdf->id_envio = $arteEnvioEmail->artwArteEnvio->id;
        $pdf->nome_arquivo = $filename;
        //$pdf->id_fornecedor = $request->input('fornecedor');
        $pdf->save();

        DB::commit();
        
        return response()->json(['success' => 200, 'message' => 'Ok'], 200);

    }

    public function ticket($id_ticket, ArtwPerfisUsuariosEquipeTickets $artwPerfisUsuariosEquipeTickets)
    {
        $common = new Common();

        $ticket = ArtwTickets::find($id_ticket);

        if (!$ticket) {
            return response()->json(['error' => 404, 'message' => 'Not found'], 404);
        }
        
        $idsPerfis = [];

        foreach ($ticket->artwPerfisUsuariosTickets as $participante) {
            
            if (!$participante->admUsuario->deleted_at)
            {
                $idsPerfis[] = $participante->id_perfil;
            }
            
        }
        
        $artesEnvios = $ticket->artwArtesEnvios;
        
        $fornecedorPdfs = $ticket->fornecedorPdfs()->whereIn('status', [0,1])->get();
        
        $fornecedorPdfsAprovados = $ticket->fornecedorPdfs()->whereIn('status', [1])->count();
        
        // Tratamento pra quando o ticket estiver em digitação
        if ($ticket->id_status == 9 && $idsPerfis) {
            
            $common->changeStatusTicket($ticket, 0);
            
        }
        
        // Tratamento pra quando o ticket estiver em aberto ou digitação
        if (($ticket->id_status == 0 || $ticket->id_status == 9) && in_array(5, $idsPerfis)) {

            $common->changeStatusTicket($ticket, 1);

            if ($ticket->artwItem->status == 0) {
                $item = $ticket->artwItem;
                $item->status = 1;
                $item->save();
            }
        } else {

            //$common->changeStatusTicket($ticket, 0);
        }
        
        // Tratamento pra quando estiver no Envio da Arte
        if ($ticket->id_status == 10)
        {
            $id_perfil = 6;
            $proximo_status = 11;
            
            $pendentes = $ticket->participantesPendentes($id_perfil, $ticket->ciclo_atual()->numero);
            
            //if ($pendentes->count() == 0 && $artesEnvios->count() == 0)
            if ($pendentes->count() == 0)
            {
                $common->changeStatusTicket($ticket, $proximo_status);
            }
        }
        
        // Tratamento pra quando estiver no PDF do Fornecedor
        if ($ticket->id_status == 11)
        {
            $id_perfil = 7;
            $proximo_status = 12;
            
            $pendentes = $ticket->participantesPendentes($id_perfil, $ticket->ciclo_atual()->numero);
            
            //if ($pendentes->count() == 0 && $fornecedorPdfsAprovados == 0)
            if ($pendentes->count() == 0)
            {
                $common->changeStatusTicket($ticket, $proximo_status);
            }
        }
        
        // Tratamento pra quando o ticket estiver em aprovação
        if ($ticket->id_status == 3 || $ticket->id_status == 4) {
            
            $id_perfil = ($ticket->id_status == 3) ? 2 : 1;
            $proximo_status = ($ticket->id_status == 3) ? 4 : 5;
            
            $check = ($ticket->id_status == 3) ? in_array(2, $idsPerfis) : in_array(1, $idsPerfis);
            
            // Avança caso tiver excluído o Marketing
            if (!$check && $ticket->id_status == 3)
            {
                $common->changeStatusTicket($ticket, 4);
            }
            
            $pendentes = $ticket->participantesPendentes($id_perfil, $ticket->ciclo_atual()->numero);
            
            if ($pendentes->count() == 0 && $check) {

                $reprovados = $ticket->participantesReprovados($id_perfil, $ticket->ciclo_atual()->numero);

                if ($reprovados->count() == 0) {

                    $common->changeStatusTicket($ticket, $proximo_status);
                } else {

                    $perfilUsuarioEquipeTicket = $artwPerfisUsuariosEquipeTickets->artwPerfilUsuarioEquipeTicket(Auth::user()->id, $id_perfil, $ticket->id);

                    if ($perfilUsuarioEquipeTicket) {
                        $perfilUsuarioTicket = $perfilUsuarioEquipeTicket->artwPerfisUsuariosTicket;
                    } else {
                        $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);
                    }

                    //dd($perfilUsuarioTicket);

                    $common->reprovaTicket($ticket, $id_perfil, $perfilUsuarioTicket, $perfilUsuarioEquipeTicket, 'ticketService');
                }
            }
        }

        $dono = $ticket->admUsuario;

        $response = $ticket->toArray();

        $response['projeto_nome'] = ($ticket->projeto) ? $ticket->projeto->nome : null;

        if ($ticket->id_status == 17 && $ticket->modulo == 2)
        {
            $ultimaAprovacao = $ticket->artwPerfisUsuariosTickets()->where('id_perfil', 11)->orderBy('updated_at')->get()->last();

            $response['data_lancamento'] = ($ultimaAprovacao) ? $ultimaAprovacao->updated_at->format('d/m/Y') : null;
        }
        
        if ($ticket->modulo == 1) {
            $response['id_item_compartilhado'] = $ticket->artwItem->compartilhado;
        }
        
        if((in_array('artwork_compartilhar_item', Auth::user()->modulos))) {
            
            $response['permite_compartilhar'] = 1;
            
        } else {
            
            $response['permite_compartilhar'] = 0;
            
        }

        //if ($qtdEnvioArtes && $ticket->)


        if ($ticket->cancelado_em || $ticket->pausado_em) {
            $response['libera_ticket'] = 0;
        } else {
            $response['libera_ticket'] = 1;
        }

        if ($ticket->cancelado_em) {
            $response['cancelado_por_nome'] = $ticket->canceladoPor->nome_abreviado;
            $response['cancelado_em'] = $ticket->cancelado_em->format('H\hi \d\e d/m/Y');
            $response['cancelado_motivo'] = nl2br($ticket->cancelado_motivo);
        }

        if ($ticket->pausado_em) {
            $response['pausado_por_nome'] = $ticket->pausadoPor->nome_abreviado;
            $response['pausado_em'] = $ticket->pausado_em->format('H\hi \d\e d/m/Y');
        }

        $response['data_envio_autorizado'] = ($ticket->dt_envio_autorizado) ? $ticket->dt_envio_autorizado->format('H\hi \d\e d/m/Y') : null;
        $response['data_envio_finalizado'] = ($ticket->dt_envio_finalizado) ? $ticket->dt_envio_finalizado->format('H\hi \d\e d/m/Y') : null;

        $response['descricao'] = $ticket->descricao_html;

        //dd($response);

        $comentarios = $ticket->artwComentarios;

        $response['id_usuario_logado'] = Auth::user()->id;
        $response['id_adm_empresa'] = Auth::user()->id_adm_empresa;

        $response['comentarios'] = [];

        foreach ($comentarios as $i => $comentario) {
            $response['comentarios'][$i] = $comentario->toArray();
            $response['comentarios'][$i]['nome_usuario'] = $comentario->admUsuario->nome_abreviado;
            $response['comentarios'][$i]['data_criacao'] = $comentario->created_at->format('H\hi \d\e d/m/Y');
            $response['comentarios'][$i]['array_envios'] = json_decode($comentario->array_envios);

            $response['comentarios'][$i]['respostas'] = [];

            //$respostas = $comentario->artwRespostas;
            $respostas = ArtwRespostas::where('id_comentario', $comentario->id)->orderBy('created_at', 'ASC')->get();

            foreach ($respostas as $n => $resposta) {
                $response['comentarios'][$i]['respostas'][$n] = $resposta->toArray();
                $response['comentarios'][$i]['respostas'][$n]['nome_usuario'] = $resposta->admUsuario->nome_abreviado;
                $response['comentarios'][$i]['respostas'][$n]['data_criacao'] = $resposta->created_at->format('H\hi \d\e d/m/Y');
                $response['comentarios'][$i]['respostas'][$n]['array_envios'] = json_decode($resposta->array_envios);
            }
        }

        $response['finalizador'] = [];
        $response['autorizador'] = [];

        if ($ticket->usuarioFinalizador) {
            $response['finalizador']['id'] = $ticket->usuarioFinalizador->id;
            $response['finalizador']['nome_usuario'] = $ticket->usuarioFinalizador->nome_abreviado;
            $response['finalizador']['data'] = $ticket->dt_envio_finalizado->format('H\hi \d\e d/m/Y');
        }

        if ($ticket->usuarioAutorizador) {
            $response['autorizador']['id'] = $ticket->usuarioAutorizador->id;
            $response['autorizador']['nome_usuario'] = $ticket->usuarioAutorizador->nome_abreviado;
            $response['autorizador']['data'] = $ticket->dt_envio_autorizado->format('H\hi \d\e d/m/Y');
        }

        $cicloAtual = $ticket->ciclo_atual();
        
        // CICLO ATUAL
        $cicloAtualDetalhes = $common->ciclo_detalhe($cicloAtual, 1);
        $response['ciclo_atual'] = $cicloAtualDetalhes;
        
        $versao_atual = ($ticket->modulo == 1) ? $ticket->artwItem->artwVersoes->last() : null;

        $response['versao_atual'] = [];

        if ($versao_atual) {
            $response['versao_atual'] = $versao_atual->toArray();

            $response['versao_atual']['nome_usuario'] = ($versao_atual->admUsuario) ? $versao_atual->admUsuario->nome_abreviado : null;
            $response['versao_atual']['data_criacao'] = $versao_atual->created_at->format('H\hi \d\e d/m/Y');
            //$response['versao_atual']['nome'] = $versao_atual->nome;
            
            $response['versao_atual']['filesize'] = $versao_atual->filesize;

            if (config('app.cloudflow')) {

                $response['versao_atual']['path'] = route('site.artwork.workflow.view', [$versao_atual->id, 'versao']);
            }

            $response['versao_atual']['baixarPdf'] = route('site.artwork.workflow.baixarPdf', [$versao_atual->id, 'versao']);
        } else {
            $response['versao_atual'] = null;
        }

        $versao = $ticket->artwVersoes->last();

        $response['versao'] = [];
        
        if ($versao) {
            $response['versao'] = $versao->toArray();

            $response['versao']['nome_usuario'] = ($cicloAtualDetalhes['layout']['nome_usuario']) ? $cicloAtualDetalhes['layout']['nome_usuario'] : null;
            $response['versao']['data_criacao'] = $cicloAtualDetalhes['layout']['data_criacao'];
            $response['versao']['nome'] = str_replace(' - ', ' • ', $versao->nome);

            if (config('app.cloudflow')) {

                $response['versao']['inspector'] = route('site.artwork.workflow.view', [$versao->id, 'versao', $ticket->id]);
                
            } else {

                $response['versao']['inspector'] = null;
            }
            
            $response['versao']['path'] = route('site.artwork.abrirPdf', [$versao->id, 'versao']);

            $response['versao']['baixarPdf'] = route('site.artwork.workflow.baixarPdf', [$versao->id, 'versao']);
        } else {
            $response['versao'] = null;
        }
        
        $arquivosComparacao = [];

        if (config('app.cloudflow')) {
            
            if ($versao)
            {
                $arquivosComparacao[0]['id'] = $versao->id;
                $arquivosComparacao[0]['nome'] = $versao->nome;
                $arquivosComparacao[0]['path'] = $versao->path;
                $arquivosComparacao[0]['tipo'] = 'versao';
                
                $n = 1;
            }
            else
            {
                $n = 0;
            }
            
            if ($ticket->id_status == 6)
            {
                $ciclos = ArtwCiclos
                    ::where('id_ticket', $ticket->id)
                    ->where('id', '<>', $cicloAtual->id)
                    ->orderBy('id', 'DESC')
                    ->get();
            }
            else
            {
                $ciclos = ArtwCiclos
                    ::where('id_ticket', $ticket->id)
                    ->orderBy('id', 'DESC')
                    ->get();
            }

            foreach ($ciclos as $ciclo) {

                $layout = $ciclo->artwCiclosLayout->last();

                if ($layout) {
                    $arquivosComparacao[$n]['id'] = $layout->id;
                    $arquivosComparacao[$n]['nome'] = $layout->nome;
                    $arquivosComparacao[$n]['path'] = $layout->path;
                    $arquivosComparacao[$n]['tipo'] = 'ciclo';
                    $n++;
                }
            }

            $versoes = ArtwVersoes::where('id_item', $ticket->id_item)->orderBy('id', 'DESC')->get();

            foreach ($versoes as $versao) {

                //if ($versao->numero > 0) {

                    $arquivosComparacao[$n]['id'] = $versao->id;
                    $arquivosComparacao[$n]['nome'] = $versao->nome;
                    $arquivosComparacao[$n]['path'] = $versao->path;
                    $arquivosComparacao[$n]['tipo'] = 'versao';
                    $n++;
                //}
            }
        }

        $response['comparacao'] = $arquivosComparacao;
        
        //dd($response['comparacao']);

        $response['count_marketing'] = $ticket->artwPerfisUsuariosTickets()->where('id_perfil', 2)->get()->count();
        $response['libera_status'] = 0;
        $response['libera_edicao'] = 0;
        $response['libera_upload_layout'] = 0;
        $response['participante'] = 0;
        $response['revisor'] = 0;
        $response['arteFinalista'] = 0;
        $response['envioFornecedorPdf'] = 0;
        $response['aprovador'] = 0;
        $response['marketing'] = 0;
        $response['participanteEnvio'] = 0;
        $response['link_editar_embalagem'] = ($ticket->id_variacao && $ticket->id_item) ? route('site.artwork.package.edit.get', [$ticket->id_variacao, $ticket->id_item]) : null;
        $response['qtde_revisores'] = $ticket->participantesPorCategoria('revisores')->count();
        $response['qtde_marketings'] = $ticket->participantesPorCategoria('marketings')->count();
        $response['qtde_aprovadores'] = $ticket->participantesPorCategoria('aprovadores')->count();
        $response['adsmart_qtde_aprovadores'] = $ticket->participantesPorCategoria('adsmart-aprovadores')->count();

        $perfilUsuarioTicketEnvioFornecedorPdf = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, 7);

        if ($perfilUsuarioTicketEnvioFornecedorPdf) {
            $response['participante'] = 0;
            $response['envioFornecedorPdf'] = 1;
        }
        
        $perfilUsuarioTicketParticipante = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, 4);

        if ($perfilUsuarioTicketParticipante) {
            $response['participante'] = 1;
        }

        $perfilUsuarioTicketEnvio = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, 6);

        if ($perfilUsuarioTicketEnvio) {
            $response['participante'] = 0;
            $response['participanteEnvio'] = 1;
        }

        $perfilUsuarioTicketArteFinalista = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, 5);

        if ($perfilUsuarioTicketArteFinalista) {
            $response['participante'] = 0;
            $response['arteFinalista'] = 1;
        }

        $perfilUsuarioTicketAnexador = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, 9);

        if ($perfilUsuarioTicketAnexador) {
            $response['anexador'] = 1;
        }

        $perfilUsuarioTicketRevisor = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, 3);

        if ($perfilUsuarioTicketRevisor) {
            $response['participante'] = 0;
            $response['revisor'] = 1;
        }

        $perfilUsuarioTicketAprovador = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, 1);
        $perfilUsuarioEquipeTicketAprovador = $artwPerfisUsuariosEquipeTickets->artwPerfilUsuarioEquipeTicket(Auth::user()->id, 1, $ticket->id);

        if ($perfilUsuarioTicketAprovador || $perfilUsuarioEquipeTicketAprovador) {
            $response['participante'] = 0;
            $response['aprovador'] = 1;
        }

        $perfilUsuarioTicketMarketing = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, 2);
        $perfilUsuarioEquipeTicketMarketing = $artwPerfisUsuariosEquipeTickets->artwPerfilUsuarioEquipeTicket(Auth::user()->id, 2, $ticket->id);

        if ($perfilUsuarioTicketMarketing || $perfilUsuarioEquipeTicketMarketing) {
            $response['participante'] = 0;
            $response['marketing'] = 1;
        }

        switch ($ticket->id_status) {
            case 1:
                $id_perfil = 5;

                $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);

                if ($perfilUsuarioTicket) {
                    $response['libera_edicao'] = 1;
                    $response['libera_status'] = 1;
                }

                break;
            case 2:
                $id_perfil = 3;

                $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);

                if ($perfilUsuarioTicket) {
                    $response['libera_status'] = 1;
                    $response['libera_edicao'] = 1;
                }

                break;

            case 3:
                $id_perfil = 2;

                $perfilUsuarioEquipeTicket = $artwPerfisUsuariosEquipeTickets->artwPerfilUsuarioEquipeTicket(Auth::user()->id, $id_perfil, $ticket->id);

                if ($perfilUsuarioEquipeTicket) {
                    $perfilUsuarioTicket = $perfilUsuarioEquipeTicket->artwPerfisUsuariosTicket;
                } else {
                    $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);
                }

                //dd($perfilUsuarioTicket);

                if ($perfilUsuarioTicket && ($perfilUsuarioTicket->aprovado === 0 || ($perfilUsuarioTicket->aprovado == 2 && $perfilUsuarioTicket->ciclo != $ticket->ciclo_atual()->numero))) {
                    $response['libera_status'] = 1;
                }
                
                if ($perfilUsuarioTicket)
                {
                    $response['em_reprovacao'] = $perfilUsuarioTicket->em_reprovacao;
                    $response['em_reprovacao_by'] = $perfilUsuarioTicket->em_reprovacao_by;
                    $response['em_reprovacao_nome'] = ($perfilUsuarioTicket->emReprovacaoPor) ? $perfilUsuarioTicket->emReprovacaoPor->nome_abreviado : null;
                }

               break;

            case 4:
                $id_perfil = 1;

                $perfilUsuarioEquipeTicket = $artwPerfisUsuariosEquipeTickets->artwPerfilUsuarioEquipeTicket(Auth::user()->id, $id_perfil, $ticket->id);

                if ($perfilUsuarioEquipeTicket) {
                    $perfilUsuarioTicket = $perfilUsuarioEquipeTicket->artwPerfisUsuariosTicket;
                } else {
                    $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);
                }

                if ($perfilUsuarioTicket && ($perfilUsuarioTicket->aprovado === 0 || ($perfilUsuarioTicket->aprovado == 2 && $perfilUsuarioTicket->ciclo != $ticket->ciclo_atual()->numero))) {
                    $response['libera_status'] = 1;
                }
                
                if ($perfilUsuarioTicket)
                {
                    $response['em_reprovacao'] = $perfilUsuarioTicket->em_reprovacao;
                    $response['em_reprovacao_by'] = $perfilUsuarioTicket->em_reprovacao_by;
                    $response['em_reprovacao_nome'] = ($perfilUsuarioTicket->emReprovacaoPor) ? $perfilUsuarioTicket->emReprovacaoPor->nome_abreviado : null;
                }

                break;

            case 5:
                $id_perfil_revisor = 3;
                $id_perfil_artefinalista = 5;
                $id_perfil_envio = 6;

                $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil_artefinalista);

                if ($perfilUsuarioTicket) {
                    $response['libera_status'] = 0;
                    $response['libera_edicao'] = 1;
                }

                $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil_envio);

                if ($perfilUsuarioTicket) {
                    $response['libera_status'] = 0;
                    $response['libera_edicao'] = 1;
                }

                $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil_revisor);

                if ($perfilUsuarioTicket && !$ticket->envio_finalizado) {
                    $response['libera_status'] = 1;
                    $response['libera_edicao'] = 1;
                }

                if ($ticket->id_usuario == Auth::user()->id && $ticket->envio_finalizado) {

                    $response['libera_status'] = 1;
                    $response['libera_edicao'] = 1;
                }

                break;
            
            case 10:
                
                $response['libera_edicao'] = 1;
                
                break;
            case 11:
                $id_perfil = 7;

                $perfilUsuarioEquipeTicket = $artwPerfisUsuariosEquipeTickets->artwPerfilUsuarioEquipeTicket(Auth::user()->id, $id_perfil, $ticket->id);

                if ($perfilUsuarioEquipeTicket) {
                    $perfilUsuarioTicket = $perfilUsuarioEquipeTicket->artwPerfisUsuariosTicket;
                } else {
                    $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);
                }

                if ($perfilUsuarioTicket && ($perfilUsuarioTicket->aprovado === 0 || ($perfilUsuarioTicket->aprovado == 2 && $perfilUsuarioTicket->ciclo != $ticket->ciclo_atual()->numero))) {
                    $response['libera_status'] = 1;
                    $response['libera_edicao'] = 1;
                }

                break;
            case 14:
                $id_perfil = 9;

                $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);

                if ($perfilUsuarioTicket && ($perfilUsuarioTicket->aprovado === 0 || ($perfilUsuarioTicket->aprovado == 2 && $perfilUsuarioTicket->ciclo != $ticket->ciclo_atual()->numero))) {
                    $response['libera_status'] = 1;
                    $response['libera_edicao'] = 1;
                }
                
                if ($perfilUsuarioTicket)
                {
                    $response['em_reprovacao'] = $perfilUsuarioTicket->em_reprovacao;
                    $response['em_reprovacao_by'] = $perfilUsuarioTicket->em_reprovacao_by;
                    $response['em_reprovacao_nome'] = ($perfilUsuarioTicket->emReprovacaoPor) ? $perfilUsuarioTicket->emReprovacaoPor->nome_abreviado : null;
                }

                break;
            case 15:
                $id_perfil = 10;

                $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);

                if ($perfilUsuarioTicket && ($perfilUsuarioTicket->aprovado === 0 || ($perfilUsuarioTicket->aprovado == 2 && $perfilUsuarioTicket->ciclo != $ticket->ciclo_atual()->numero))) {
                    $response['libera_status'] = 1;
                    $response['libera_edicao'] = 1;
                }
                
                if ($perfilUsuarioTicket)
                {
                    $response['em_reprovacao'] = $perfilUsuarioTicket->em_reprovacao;
                    $response['em_reprovacao_by'] = $perfilUsuarioTicket->em_reprovacao_by;
                    $response['em_reprovacao_nome'] = ($perfilUsuarioTicket->emReprovacaoPor) ? $perfilUsuarioTicket->emReprovacaoPor->nome_abreviado : null;
                }

                break;
            case 16:
                $id_perfil = 11;

                $perfilUsuarioTicket = $ticket->artwPerfilUsuarioTicket(Auth::user()->id, $id_perfil);

                if ($perfilUsuarioTicket && ($perfilUsuarioTicket->aprovado === 0 || ($perfilUsuarioTicket->aprovado == 2 && $perfilUsuarioTicket->ciclo != $ticket->ciclo_atual()->numero))) {
                    $response['libera_status'] = 1;
                    $response['libera_edicao'] = 1;
                }
                
                if ($perfilUsuarioTicket)
                {
                    $response['em_reprovacao'] = $perfilUsuarioTicket->em_reprovacao;
                    $response['em_reprovacao_by'] = $perfilUsuarioTicket->em_reprovacao_by;
                    $response['em_reprovacao_nome'] = ($perfilUsuarioTicket->emReprovacaoPor) ? $perfilUsuarioTicket->emReprovacaoPor->nome_abreviado : null;
                }

                break;
        }

        if ($ticket->id_status == 1) {
            $perfil_tarefa = 5;
        } elseif ($ticket->id_status == 2) {
            $perfil_tarefa = 3;
        } elseif ($ticket->id_status == 3) {
            $perfil_tarefa = 2;
        } elseif ($ticket->id_status == 4) {
            $perfil_tarefa = 1;
        } elseif ($ticket->id_status == 5) {
            $perfil_tarefa = 5;
        } elseif ($ticket->id_status == 10 && !$ticket->envio_finalizado) {
            $perfil_tarefa = 5;
        } elseif ($ticket->id_status == 10 && $ticket->envio_finalizado && !$ticket->envio_autorizado) {
            $perfil_tarefa = 3;
        } elseif ($ticket->id_status == 10 && $ticket->envio_finalizado && $ticket->envio_autorizado) {
            $perfil_tarefa = 5;
        } elseif ($ticket->id_status == 11) {
            $perfil_tarefa = 7;
        } else {
            $perfil_tarefa = 0;
        }

        $response['perfis'] = [];
        $response['participantes'] = [];
        $response['participantesUnicos'] = [];
        

        $participantes = $ticket->participantes();
        
        //echo $participantes;
        //exit;
        
        $participantesUnicos = $ticket->participantesUnicos();

        //dd($participantesUnicos);
        
        //$participantes = [];
        //$participantesUnicos = [];
        
        //$participantes = $ticket->participantes();
        
        //echo $participantesUnicos;
        //exit;

        $response['dono'] = [
            'nome_usuario' => @$dono->nome_abreviado,
            'data_criacao' => $ticket->created_at->format('H\hi \d\e d/m/Y'),
            'data_atualizacao' => $ticket->updated_at->format('H\hi \d\e d/m/Y'),
            'departamento' => @$dono->admDepartamento,
            'empresa' => @$dono->admEmpresa
        ];

        $todosStatus = ArtwTktStatus::all();

        $response['todosStatus'] = [];

        foreach ($todosStatus as $i => $status)
        {
            $response['todosStatus'][$i] = $status->toArray();
            
            if ($status->id == 1)
            {
                //$primeiroUsuario = ArtwTicketsStatus::where('id_ciclo', $cicloAtual->id)->orderBy('created_at', 'ASC')->where('id_status', $status->id)->limit(1)->get()->first();
                $primeiroUsuario = ArtwTicketsStatus::where('id_ticket', $ticket->id)->orderBy('created_at', 'ASC')->where('id_status', $status->id)->limit(1)->get()->first();

                $response['todosStatus'][$i]['primeiroUsuario'] = ($primeiroUsuario) ? $primeiroUsuario->toArray() : [];
                $response['todosStatus'][$i]['primeiroUsuario']['data_criacao'] = ($primeiroUsuario) ? $primeiroUsuario->created_at->format('H\hi \d\e d/m/Y') : null;
                $response['todosStatus'][$i]['primeiroUsuario']['nome_usuario'] = ($primeiroUsuario && $primeiroUsuario->admUsuario) ? $primeiroUsuario->admUsuario->nome_abreviado : null;
                $response['todosStatus'][$i]['primeiroUsuario']['departamento'] = ($primeiroUsuario && $primeiroUsuario->admUsuario) ? $primeiroUsuario->admUsuario->admDepartamento : [];
                $response['todosStatus'][$i]['primeiroUsuario']['empresa'] = ($primeiroUsuario && $primeiroUsuario->admUsuario) ? $primeiroUsuario->admUsuario->admEmpresa : [];
            }
            else
            {
                $ultimoUsuario = ArtwTicketsStatus::where('id_ciclo', $cicloAtual->id)->orderBy('created_at', 'DESC')->where('id_status', $status->id)->limit(1)->get()->first();

                $response['todosStatus'][$i]['ultimoUsuario'] = ($ultimoUsuario) ? $ultimoUsuario->toArray() : [];
                $response['todosStatus'][$i]['ultimoUsuario']['data_criacao'] = ($ultimoUsuario) ? $ultimoUsuario->created_at->format('H\hi \d\e d/m/Y') : null;
                $response['todosStatus'][$i]['ultimoUsuario']['nome_usuario'] = ($ultimoUsuario && $ultimoUsuario->admUsuario) ? $ultimoUsuario->admUsuario->nome_abreviado : null;
                $response['todosStatus'][$i]['ultimoUsuario']['departamento'] = ($ultimoUsuario && $ultimoUsuario->admUsuario) ? $ultimoUsuario->admUsuario->admDepartamento : [];
                $response['todosStatus'][$i]['ultimoUsuario']['empresa'] = ($ultimoUsuario && $ultimoUsuario->admUsuario) ? $ultimoUsuario->admUsuario->admEmpresa : [];
            }
        }

        foreach ($participantes as $k => $participante) {

            if (!isset($response['nr_perfis'][$participante->artwPerfisUsuario->id]['qtd'])) {
                $response['nr_perfis'][$participante->artwPerfisUsuario->id]['qtd'] = 0;
                $response['nr_perfis'][$participante->artwPerfisUsuario->id]['nome'] = str_plural($participante->artwPerfisUsuario->nome);
            }

            // Incrementa a quantidade de participantes daquele perfil
            $response['nr_perfis'][$participante->artwPerfisUsuario->id]['qtd'] ++;

            if ($response['libera_ticket']) {

                $response['participantes'][$k]['envia_notificacao'] = (Auth::user()->id != $participante->admUsuario->id && $perfil_tarefa != 5 && $perfil_tarefa == $participante->artwPerfisUsuario->id && ($participante->aprovado == 0 || ($participante->aprovado == 2 && $participante->ciclo < $ticket->ciclo_atual()->numero ))) ? 1 : 0;
            } else {

                $response['participantes'][$k]['envia_notificacao'] = 0;
            }

            $response['participantes'][$k]['enviadas'] = $participante->admUsuario->artwNotificacoesTo()->whereIn('id_tipo', [17, 18, 19])->where('id_ticket', $ticket->id)->where('created_at', '>', \Carbon\Carbon::now()->subHours(24)->toDateTimeString())->get()->count();

            $response['participantes'][$k]['id_participante'] = $participante->id;
            $response['participantes'][$k]['id_usuario'] = $participante->admUsuario->id;
            $response['participantes'][$k]['aprovado'] = $participante->aprovado;
            $response['participantes'][$k]['ciclo'] = $participante->ciclo;
            $response['participantes'][$k]['ciclo_aprovacao'] = $participante->ciclo_aprovacao;
            $response['participantes'][$k]['membro_aprovador'] = $participante->id_membro_aprovador;
            $response['participantes'][$k]['deleted_at'] = $participante->deleted_at;
            $response['participantes'][$k]['data_atualizacao'] = $participante->updated_at->format('H\hi \d\e d/m/Y');
            $response['participantes'][$k]['nome_usuario'] = $participante->admUsuario->nome_abreviado;
            $response['participantes'][$k]['id_perfil'] = $participante->artwPerfisUsuario->id;
            $response['participantes'][$k]['nome_perfil'] = $participante->artwPerfisUsuario->nome;
            $response['participantes'][$k]['departamento'] = $participante->admUsuario->admDepartamento;
            $response['participantes'][$k]['empresa'] = $participante->admUsuario->admEmpresa;

            $response['participantes'][$k]['aprovador'] = [];

            if ($participante->id_membro_aprovador) {

                $membroAprovador = $participante->membroAprovador;
                
                $response['participantes'][$k]['aprovador']['data_atualizacao'] = $membroAprovador->updated_at->format('H\hi \d\e d/m/Y');
                $response['participantes'][$k]['aprovador']['nome_usuario'] = $membroAprovador->admUsuario->nome_abreviado;
                $response['participantes'][$k]['aprovador']['departamento'] = $membroAprovador->admUsuario->admDepartamento;
                $response['participantes'][$k]['aprovador']['empresa'] = $membroAprovador->admUsuario->admEmpresa;
                $response['participantes'][$k]['aprovador']['id_usuario'] = $membroAprovador->admUsuario->id;
                $response['participantes'][$k]['aprovador']['deleted_at'] = $membroAprovador->deleted_at;
            }

            if ($participante->id_renovacao) {

                $response['participantes'][$k]['participante_renovacao_nome'] = $participante->participanteRenovador->nome;
            } else {

                $response['participantes'][$k]['participante_renovacao_nome'] = null;
            }

            $response['participantes'][$k]['equipe'] = [];

            $equipe = $participante->equipe();
            
            foreach ($equipe as $m => $membro) {
                $response['participantes'][$k]['equipe'][$m]['id_membro'] = $membro->id;
                $response['participantes'][$k]['equipe'][$m]['id_usuario'] = $membro->admUsuario->id;
                $response['participantes'][$k]['equipe'][$m]['aprovado'] = $membro->aprovado;
                $response['participantes'][$k]['equipe'][$m]['deleted_at'] = $membro->deleted_at;
                $response['participantes'][$k]['equipe'][$m]['data_atualizacao'] = (isset($membro->updated_at)) ? $membro->updated_at->format('H\hi \d\e d/m/Y') : null;
                $response['participantes'][$k]['equipe'][$m]['nome_usuario'] = $membro->admUsuario->nome_abreviado;
                $response['participantes'][$k]['equipe'][$m]['departamento'] = $membro->admUsuario->admDepartamento;
                $response['participantes'][$k]['equipe'][$m]['empresa'] = $membro->admUsuario->admEmpresa;

                $response['participantes'][$k]['equipe'][$m]['enviadas'] = $membro->admUsuario->artwNotificacoesTo()->whereIn('id_tipo', [17, 18, 19])->where('id_ticket', $ticket->id)->where('created_at', '>', \Carbon\Carbon::now()->subHours(24)->toDateTimeString())->get()->count();

                // Incrementa a quantidade de participantes daquele perfil
                $response['nr_perfis'][$participante->artwPerfisUsuario->id]['qtd'] ++;
            }

            $ignorarExcluidos = [];
            
            //dd($participante);
            
            if ( (!in_array($participante->artwPerfisUsuario->id, $ignorarExcluidos)) || (in_array($participante->artwPerfisUsuario->id, $ignorarExcluidos) && !$participante->deleted_at) )
            {
                $response['perfis'][$participante->artwPerfisUsuario->id][] = $response['participantes'][$k];
            }
            
        }

        $response['artes'] = [];

        $arte = $ticket->artwArtes->last();

        $proximaVersao = ($ticket->artwItem && $ticket->artwItem->artwVersoes->last()) ? $ticket->artwItem->artwVersoes->last()->numero+1 : 1;

        if ($ticket->id_status == 6 && !$ticket->cancelado_em && $arte)
        {
            $arteNome = $arte->nome . ' • Versão ' . $ticket->artwVersoes->last()->numero;

        } elseif ($arte) {

            $arteNome = $arte->nome . ' • Versão ' . $proximaVersao;

        } else {
            $arteNome = '';
        }

        if ($arte) {
            $response['artes'][0]['id_usuario'] = $arte->admUsuario->id;
            $response['artes'][0]['nome_usuario'] = $arte->admUsuario->nome_abreviado;
            $response['artes'][0]['data_criacao'] = $arte->created_at->format('H\hi \d\e d/m/Y');
            $response['artes'][0]['id'] = $arte->id;
            $response['artes'][0]['nome'] = $arteNome;
            $response['artes'][0]['nome_original'] = $arte->nome_arquivo;
            $response['artes'][0]['descricao'] = $arte->descricao;
            $response['artes'][0]['filesize'] = $arte->filesize;
            $response['artes'][0]['path'] = asset($arte->path);
            $response['artes'][0]['path'] = ( (in_array('artwork_baixar_arte_final', Auth::user()->modulos) && $ticket->id_status == 6) || $ticket->id_status != 6 ) ? route('site.artwork.workflow.baixarArteFinal', $arte->id) : '';
            $response['artes'][0]['path_info'] = pathinfo(asset($arte->path));
        }

        $response['artes_envios'] = [];
        
        foreach ($artesEnvios as $k => $envio) {

            $response['artes_envios'][$k]['id'] = $envio->id;
            $response['artes_envios'][$k]['nome_arte'] = $envio->artwArte->nome;
            $response['artes_envios'][$k]['nome_usuario_arte'] = $envio->artwArte->admUsuario->nome_abreviado;
            $response['artes_envios'][$k]['data_criacao_arte'] = $envio->artwArte->created_at->format('H\hi \d\e d/m/Y');
            $response['artes_envios'][$k]['data_exclusao_arte'] = ($envio->artwArte->deleted_at) ? $envio->artwArte->deleted_at->format('H\hi \d\e d/m/Y') : null;
            $response['artes_envios'][$k]['id_usuario'] = $envio->admUsuario->id;
            $response['artes_envios'][$k]['nome_usuario'] = $envio->admUsuario->nome_abreviado;
            $response['artes_envios'][$k]['data_criacao'] = $envio->created_at->format('H\hi \d\e d/m/Y');
            $response['artes_envios'][$k]['dt_expiracao'] = ($envio->dt_expiracao) ? $envio->dt_expiracao->format('d/m/Y') : null;
            $response['artes_envios'][$k]['cancelado_em'] = ($envio->cancelado_em) ? $envio->cancelado_em->format('H\hi \d\e d/m/Y') : null;
            $response['artes_envios'][$k]['cancelado_por'] = ($envio->cancelado_por) ? $envio->canceladoPor->nome_abreviado : null;
            $response['artes_envios'][$k]['expirado'] = ($envio->dt_expiracao) ? ( $envio->dt_expiracao->format('Y-m-d H:i:s') < date('Y-m-d H:i:s') ) : null;
            $response['artes_envios'][$k]['status'] = $envio->status;
            $response['artes_envios'][$k]['fornecedor'] = ($envio->fornecedor) ? $envio->fornecedor->nome : '';
            $response['artes_envios'][$k]['emails'] = $envio->emails;
            $response['artes_envios'][$k]['mensagem'] = nl2br($envio->mensagem);
            $response['artes_envios'][$k]['downloads'] = $envio->downloads;
        }

        if ($ticket->modulo == 2 && $ticket->id_status != 9)
        {
            $usuario = $ticket->artwPerfisUsuariosTickets->where('id_perfil', 8)->first();

            // Etapas ADSMART
            $response['adsmartEtapas'] = [
                1 => 0,
                2 => 0,
                3 => 0,
                4 => 0,
                5 => 0,
                6 => 0,
                7 => 0,
            ];

            if ($ticket->id_status != 0 && $ticket->id_status != 9) {
                $response['adsmartEtapas'][1] = 1;
            }

            if ($ticket->id_status >= 13 && $ticket->id_status != 9) {
                $response['adsmartEtapas'][2] = 1;
            }

            if ($ticket->id_status >= 13 && $ticket->id_status != 9 && $cicloAtualDetalhes['layout']) {
                $response['adsmartEtapas'][3] = 1;
            }

            if ($ticket->id_status >= 15 && $ticket->id_status != 9) {
                $response['adsmartEtapas'][4] = 1;
            }

            // Verifica as solicitações
            $solicitacoesPendentes = $ticket->ciclo_atual()->artwSolicitacoes->where('id_status', 1)->count();

            if ($ticket->id_status >= 15 && $ticket->id_status != 9 && $solicitacoesPendentes == 0) {
                $response['adsmartEtapas'][5] = 1;
            }

            if ($ticket->id_status >= 16 && $ticket->id_status != 9) {
                $response['adsmartEtapas'][6] = 1;
            }

            if ($ticket->id_status == 17) {
                $response['adsmartEtapas'][7] = 1;
            }

            // Tarefas ADSMART
            $response['adsmartTarefas']['aberto_preencher'] = [

                'nome_usuario' => $usuario->admUsuario->nome_abreviado,
                'departamento' => ($usuario->admUsuario->admDepartamento) ? $usuario->admUsuario->admDepartamento->nome : null,
                'empresa' => ($usuario->admUsuario->admEmpresa) ? $usuario->admUsuario->admEmpresa->nome : null,
                'data' => $usuario->created_at->format('H\hi \d\e d/m/Y'),
                'status' => 1

            ];

            $usuario = $ticket->artwPerfisUsuariosTickets->where('id_perfil', 9)->first();

            $response['adsmartTarefas']['anexacao_anexar'] = [

                'nome_usuario' => $usuario->admUsuario->nome_abreviado,
                'departamento' => ($usuario->admUsuario->admDepartamento) ? $usuario->admUsuario->admDepartamento->nome : null,
                'empresa' => ($usuario->admUsuario->admEmpresa) ? $usuario->admUsuario->admEmpresa->nome : null,
                'data' => ($ticket->ciclo_atual()->artwCiclosLayout->last() && $response['adsmartEtapas'][2]) ? $ticket->ciclo_atual()->artwCiclosLayout->last()->created_at->format('H\hi \d\e d/m/Y') : 0,
                'status' => ($ticket->ciclo_atual()->artwCiclosLayout->last() && $response['adsmartEtapas'][2]) ? 1 : 0,

            ];

            $status = $ticket->artwTicketsStatus->where('id_status', 15)->last();

            $response['adsmartTarefas']['anexacao_submeter'] = [

                'nome_usuario' => $usuario->admUsuario->nome_abreviado,
                'departamento' => ($usuario->admUsuario->admDepartamento) ? $usuario->admUsuario->admDepartamento->nome : null,
                'empresa' => ($usuario->admUsuario->admEmpresa) ? $usuario->admUsuario->admEmpresa->nome : null,
                'data' => ($status && $response['adsmartEtapas'][3]) ? $status->created_at->format('H\hi \d\e d/m/Y') : null,
                'status' => ($status && $response['adsmartEtapas'][3]) ? 1 : 0,

            ];

            $solicitacoesAbertas = $ticket->ciclo_atual()->artwSolicitacoes->where('id_status', 1)->count();
            $statusEtapa = ($solicitacoesAbertas == 0 && $response['adsmartEtapas'][4]) ? 1 : 0;

            if ($statusEtapa == 0)
            {
                $usuarios = $ticket->artwPerfisUsuariosTickets->where('id_perfil', 10);

                foreach ($usuarios as $u=>$usuario)
                {
                    $response['adsmartTarefas']['revisao_revisar'][] = [

                        'nome_usuario' => $usuario->admUsuario->nome_abreviado,
                        'departamento' => ($usuario->admUsuario->admDepartamento) ? $usuario->admUsuario->admDepartamento->nome : null,
                        'empresa' => ($usuario->admUsuario->admEmpresa) ? $usuario->admUsuario->admEmpresa->nome : null,
                        'status' => $statusEtapa
                    ];
                }
            }
            else
            {
                //$usuario = $ticket->artwPerfisUsuariosTickets->where('id_perfil', 10)->first();

                $ultimaSolicitacaoRevisada = $ticket->ciclo_atual()->artwSolicitacoes()->where('id_status', 2)->orderBy('updated_at')->get()->last();

                $usuario = $ultimaSolicitacaoRevisada->artwSolicitacoesStatus->last();

                $response['adsmartTarefas']['revisao_revisar'][] = [

                    'nome_usuario' => $usuario->admUsuario->nome_abreviado,
                    'departamento' => ($usuario->admUsuario->admDepartamento) ? $usuario->admUsuario->admDepartamento->nome : null,
                    'empresa' => ($usuario->admUsuario->admEmpresa) ? $usuario->admUsuario->admEmpresa->nome : null,
                    'data' => ($solicitacoesAbertas == 0 && $response['adsmartEtapas'][4]) ? $ultimaSolicitacaoRevisada->updated_at->format('H\hi \d\e d/m/Y') : null,
                    'status' => $statusEtapa
                ];
            }

            $status = $ticket->artwTicketsStatus->where('id_status', 16)->last();
            $statusEtapa = ($status && $response['adsmartEtapas'][5]) ? 1 : 0;

            if ($statusEtapa == 0)
            {
                $usuarios = $ticket->artwPerfisUsuariosTickets->where('id_perfil', 10);

                foreach ($usuarios as $u=>$usuario)
                {
                    $response['adsmartTarefas']['revisao_submeter'][] = [

                        'nome_usuario' => $usuario->admUsuario->nome_abreviado,
                        'departamento' => ($usuario->admUsuario->admDepartamento) ? $usuario->admUsuario->admDepartamento->nome : null,
                        'empresa' => ($usuario->admUsuario->admEmpresa) ? $usuario->admUsuario->admEmpresa->nome : null,
                        'status' => $statusEtapa,
                    ];
                }
            }
            else
            {
                $response['adsmartTarefas']['revisao_submeter'][] = [

                    'nome_usuario' => $status->admUsuario->nome_abreviado,
                    'departamento' => ($status->admUsuario->admDepartamento) ? $status->admUsuario->admDepartamento->nome : null,
                    'empresa' => ($status->admUsuario->admEmpresa) ? $status->admUsuario->admEmpresa->nome : null,
                    'data' => ($status && $response['adsmartEtapas'][5]) ? $status->created_at->format('H\hi \d\e d/m/Y') : null,
                    'status' => $statusEtapa,
                ];
            }

            $usuarios = $ticket->artwPerfisUsuariosTickets->where('id_perfil', 11);

            $response['adsmartTarefas']['aprovacao_aprovar'] = [];

            foreach ($usuarios as $usuario)
            {
                $response['adsmartTarefas']['aprovacao_aprovar'][] = [

                    'nome_usuario' => $usuario->admUsuario->nome_abreviado,
                    'departamento' => ($usuario->admUsuario->admDepartamento) ? $usuario->admUsuario->admDepartamento->nome : null,
                    'empresa' => ($usuario->admUsuario->admEmpresa) ? $usuario->admUsuario->admEmpresa->nome : null,
                    'data' => ($usuario->aprovado != 0) ? $usuario->updated_at->format('H\hi \d\e d/m/Y') : null,
                    'status' => ($usuario->aprovado != 0) ? 1 : 0,
                    'ciclo' => $usuario->ciclo,
                    'aprovado' => $usuario->aprovado,

                ];
            }
        }

        

        // ETAPAS
        $response['etapas'] = [
            0 => 0,
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            '5b' => 0,
            6 => 0,
            7 => 0,
            8 => 0,
            9 => 0,
            10 => 0,
            11 => 0,
            12 => 0,
            13 => 0,
            14 => 0,
            15 => 0
        ];
        
        if ($ticket->id_status != 9) {
            
            $response['etapas'][0] = 1;
            
        }

        if ($ticket->id_status != 9 && $ticket->id_status != 0) {
            $response['etapas'][1] = 1;
            
            if ($ticket->pausado_em) {
                //$response['pausado_em'] = 1;
            }
            
            if ($ticket->cancelado_em) {
                //$response['cancelado_em'] = 1;
            }
        }

        if ($ticket->id_status > 0 && $ticket->id_status != 9) {
            $response['etapas'][2] = 1;
        }

        if ($ticket->id_status > 0 && $cicloAtualDetalhes['layout']) {
            $response['etapas'][3] = 1;
        }

        if ($ticket->id_status > 1 && $ticket->id_status != 9) {
            $response['etapas'][4] = 1;
        }

        //dd($response);

        if ($ticket->id_status > 1 && $cicloAtualDetalhes['tudoRevisado']) {
            $response['etapas'][5] = 1;
        }
        
        if ($ticket->id_status > 2 && $cicloAtualDetalhes['tudoRevisado']) {
            $response['etapas']['5b'] = 1;
        }

        if ($ticket->id_status > 2 && $ticket->id_status != 9) {
            $response['etapas'][6] = 1;
        }

        if ($ticket->id_status > 3 && $ticket->id_status != 9) {
            $response['etapas'][7] = 1;
        }

        if ($ticket->id_status > 4 && $ticket->id_status != 9) {
            $response['etapas'][8] = 1;
        }

        if ($ticket->id_status > 4 && $arte) {
            $response['etapas'][9] = 1;
        }

        // Etapa envio ao Fornecedor autorizado
        if ($ticket->id_status > 4 && $arte && $ticket->envio_autorizado) {
            $response['etapas'][10] = 1;
        }
        
        $fornecedorPdfsTotal = count($fornecedorPdfs);
        
        $perfilFornecedorPdfAprovado = $ticket->artwPerfisUsuariosTickets()->where('id_perfil', 7)->where('aprovado', 1)->get();

        // Etapa PDF do Fornecedor enviado
        if ( ($perfilFornecedorPdfAprovado->count() && $fornecedorPdfsTotal && $fornecedorPdfsAprovados)  || (!isset($response['perfis'][6]) && !isset($response['perfis'][7] ) && $response['etapas'][10] ) ) {
            $response['etapas'][14] = 1;
        }
        
        $enviosDeArteNaoExcluidos = [];
        
        if (isset($response['perfis'][6])){
            $enviosDeArteNaoExcluidos = array_filter($response['perfis'][6], function ($var) {
                return (!$var['deleted_at']);
            });
        }
        
        $response['qtdEnviosDeArteNaoExcluidos'] = count($enviosDeArteNaoExcluidos);
        
        // Etapa envio ao Fornecedor enviado
        if ( ($ticket->id_status > 4 && $arte && $ticket->envio_autorizado && count($response['artes_envios']) ) || (!$enviosDeArteNaoExcluidos && $ticket->id_status != 9) ) {
            $response['etapas'][11] = 1;
        }

        // Etapa envio ao Fornecedor enviado e finalizado
        if ( ($ticket->id_status > 4 && $ticket->envio_autorizado && $ticket->envio_finalizado && count($response['artes_envios'])) ) {
            $response['etapas'][12] = 1;
        }
        
        // Etapa Ticket Encerrado
        if ($ticket->id_status == 6) {
            $response['etapas'][13] = 1;
        }
        
        $pdfDoFornecedorNaoExcluido = [];
        
        if (isset($response['perfis'][7])){
            $pdfDoFornecedorNaoExcluido = array_filter($response['perfis'][7], function ($var) {
                return (!$var['deleted_at']);
            });
        }
        
        $response['QtdPdfDoFornecedorNaoExcluido'] = count($pdfDoFornecedorNaoExcluido);
       
        //dd($pdfDoFornecedorNaoExcluido);
        
        //dd( ($enviosDeArteNaoExcluidos && $response['etapas'][14] && !$pdfDoFornecedorNaoExcluido ) );
        
        // Etapa PDF do Fornecedor finalizado
        if ( ($ticket->id_status == 12 || $ticket->id_status == 6) || ($perfilFornecedorPdfAprovado && $perfilFornecedorPdfAprovado->count()) || ( !count($response['artes_envios']) && !$enviosDeArteNaoExcluidos && !$pdfDoFornecedorNaoExcluido && $response['etapas'][10] ) || ($enviosDeArteNaoExcluidos && $response['etapas'][14] && !$pdfDoFornecedorNaoExcluido ) ) {
            $response['etapas'][15] = 1;
        }
        
        if ($response['etapas'][15] && $ticket->id_status != 6 && $ticket->id_usuario == Auth::user()->id) {
            
            $response['libera_status'] = 1;
            
        }

        foreach ($participantesUnicos as $k => $participante) {

            //$response['participantesUnicos'][$k]['id_participante'] = $participante->id;
            $response['participantesUnicos'][$k]['id_usuario'] = $participante->id;
            //$response['participantesUnicos'][$k]['aprovado'] = $participante->aprovado;
            //$response['participantesUnicos'][$k]['membro_aprovador'] = $participante->id_membro_aprovador;
            //$response['participantesUnicos'][$k]['data_atualizacao'] = $participante->updated_at->format('H\hi \d\e d/m/Y');
            $response['participantesUnicos'][$k]['nome_usuario'] = $participante->nome_abreviado2;
            $response['participantesUnicos'][$k]['empresa'] = ($participante->admEmpresa) ? $participante->admEmpresa->nome : null;
            $response['participantesUnicos'][$k]['departamento'] = ($participante->admDepartamento) ? $participante->admDepartamento->nome : null;

            $response['participantesUnicos'][$k]['equipe'] = [];

            /*
            foreach ($participante->artwPerfisUsuariosEquipeTickets as $m => $membro) {
                $response['participantesUnicos'][$k]['equipe'][$m]['id_membro'] = $membro->id;
                $response['participantesUnicos'][$k]['equipe'][$m]['id_usuario'] = $membro->admUsuario->id;
                $response['participantesUnicos'][$k]['equipe'][$m]['aprovado'] = $membro->aprovado;
                $response['participantesUnicos'][$k]['equipe'][$m]['data_atualizacao'] = $membro->updated_at->format('H\hi \d\e d/m/Y');
                $response['participantesUnicos'][$k]['equipe'][$m]['nome_usuario'] = $membro->admUsuario->nome_abreviado;
                $response['participantesUnicos'][$k]['equipe'][$m]['empresa'] = ($membro->admUsuario->admEmpresa) ? $participante->admUsuario->admEmpresa->nome : null;
                $response['participantesUnicos'][$k]['equipe'][$m]['departamento'] = ($membro->admUsuario->admDepartamento) ? $membro->admUsuario->admDepartamento->nome : null;
            }
            */
        }

        if ($ticket->id_status == 6) {

            $response['participantes'][] = [
                'id_usuario' => $ticket->id_usuario,
                'nome_usuario' => ($ticket->admUsuario) ? $ticket->admUsuario->nome_abreviado : '',
                'id_perfil' => null,
                'nome_perfil' => 'Dono',
                'aprovado' => 1,
                'finalizado_em' => $ticket->updated_at->format('H\hi \d\e d/m/Y')
            ];
        }

        $response['ciclos_anteriores'] = [];

        $ciclos_anteriores = ArtwCiclos
            ::where('id', '!=', $ticket->ciclo_atual()->id)
            ->where('id_ticket', $ticket->id)
            ->where('id_status', '>=', 1)
            ->orderBy('id', 'DESC')
            ->get();

        foreach ($ciclos_anteriores as $ciclo) {
            $response['ciclos_anteriores'][] = $common->ciclo_detalhe($ciclo);
        }

        $response['ciclos_anteriores'] = collect($response['ciclos_anteriores']);

        $response['ciclo_anterior'] = $response['ciclos_anteriores']->first();
        
        $proximoCiclo = ArtwCiclos
            ::where('id', '!=', $ticket->ciclo_atual()->id)
            ->where('id_ticket', $ticket->id)
            ->where('id_status', '=', 0)
            ->where('numero', '>', $ticket->ciclo_atual()->numero)
            ->get()
            ->last();
        
        $response['proximo_ciclo'] = ($proximoCiclo) ? $common->ciclo_detalhe($proximoCiclo) : null;

        $response['arquivos'] = [];
        
        foreach ($ticket->artwArquivos as $k => $arquivo) {

            $path_parts = pathinfo($arquivo->descricao);
            
            $response['arquivos'][$k]['id'] = $arquivo->id;
            $response['arquivos'][$k]['id_usuario'] = $arquivo->id_usuario;
            $response['arquivos'][$k]['nome_usuario'] = $arquivo->admUsuario->nome_abreviado;
            $response['arquivos'][$k]['data_criacao'] = $arquivo->created_at->format('H\hi \d\e d/m/Y');
            $response['arquivos'][$k]['descricao'] = $arquivo->descricao;
            $response['arquivos'][$k]['nome_original'] = $arquivo->nome_original;
            $response['arquivos'][$k]['nome_renomeavel'] = $path_parts['filename'];
            $response['arquivos'][$k]['filesize'] = $arquivo->filesize;
            //$response['arquivos'][$k]['path'] = asset($arquivo->path);
            $response['arquivos'][$k]['path'] = ($ticket->modulo == 1) ? route('site.artwork.abrirArquivo', [$arquivo->id, rawurlencode($arquivo->descricao)]) : route('site.adsmart.abrirArquivo', [$arquivo->id, $arquivo->descricao]);
            //$response['arquivos'][$k]['path'] = route('site.artwork.abrirArquivo', [$arquivo->id, str_replace('%','_',basename(public_path($arquivo->path)))]);
        }
        
        $response['fornecedorPdfs'] = [];
        $response['fornecedorPdfsAprovados'] = [];
        
        $pdfsPendentes = 0;
        
        foreach ($fornecedorPdfs as $k => $pdf) {
            
            if ($pdf->status == 0){
                
                $pdfsPendentes++;
                
            }
            
            $response['fornecedorPdfs'][$k]['id'] = $pdf->id;
            $response['fornecedorPdfs'][$k]['nome_usuario'] = ($pdf->admUsuario) ? $pdf->admUsuario->nome_abreviado : $pdf->email_fornecedor;
            $response['fornecedorPdfs'][$k]['data_criacao'] = $pdf->created_at->format('H\hi \d\e d/m/Y');
            $response['fornecedorPdfs'][$k]['id_aprovacao'] = ($pdf->usuarioAprovacao) ? $pdf->usuarioAprovacao->id : '';
            $response['fornecedorPdfs'][$k]['nome_aprovacao'] = ($pdf->usuarioAprovacao) ? $pdf->usuarioAprovacao->nome_abreviado : '';
            $response['fornecedorPdfs'][$k]['data_aprovacao'] = ($pdf->dt_aprovacao) ? $pdf->dt_aprovacao->format('H\hi \d\e d/m/Y') : '';
            $response['fornecedorPdfs'][$k]['etapa'] = ($versao && $pdf->created_at > $versao->created_at) ? 2 : 1;
            $response['fornecedorPdfs'][$k]['nome'] = $pdf->nome;
            //$response['fornecedorPdfs'][$k]['link_comparar'] = ($versao_atual) ? route('site.artwork.compareFornecedorPdf', $pdf->id) : '';
            $response['fornecedorPdfs'][$k]['link_comparar'] = route('site.artwork.compareFornecedorPdf', $pdf->id);
            $response['fornecedorPdfs'][$k]['status'] = $pdf->status;
            $response['fornecedorPdfs'][$k]['fornecedor'] = $pdf->fornecedor->nome;
            $response['fornecedorPdfs'][$k]['nome_arquivo'] = $pdf->nome_arquivo;
            $response['fornecedorPdfs'][$k]['filesize'] = $pdf->filesize;
            $response['fornecedorPdfs'][$k]['notificado'] = $pdf->notificado;
            //$response['fornecedorPdfs'][$k]['path'] = asset($pdf->path);
            $response['fornecedorPdfs'][$k]['path'] = route('site.artwork.abrirFornecedorPdf', $pdf->id);
            $response['fornecedorPdfs'][$k]['baixarPdf'] = route('site.artwork.baixarFornecedorPdf', $pdf->id);
            
            if ($pdf->status == 1) {
                
                $response['fornecedorPdfsAprovados'][] = $response['fornecedorPdfs'][$k];
                
            }
            
        }
        
        $usuarioAprovacao = $perfilFornecedorPdfAprovado->first();
        
        $response['fornecedorPdfsAprovacao']['id_aprovacao'] = ($usuarioAprovacao) ? $usuarioAprovacao->admUsuario->id : '';
        $response['fornecedorPdfsAprovacao']['nome_aprovacao'] = ($usuarioAprovacao) ? $usuarioAprovacao->admUsuario->nome_abreviado : '';
        $response['fornecedorPdfsAprovacao']['data_aprovacao'] = ($usuarioAprovacao) ? $usuarioAprovacao->updated_at->format('H\hi \d\e d/m/Y') : '';
        
        $response['fornecedorPdfsPendentes'] = $pdfsPendentes;
        $response['fornecedorPdfsTotal'] = count($response['fornecedorPdfs']);
        
        $response['arquivosBaixar'] = route('site.artwork.ticket.baixarArquivos', $ticket->numero);

        $recebe = ArtwPerfisUsuariosTickets
                ::select('artw_perfis_usuarios_tickets.recebe_email as recebe_email1', 'artw_perfis_usuarios_equipe_tickets.recebe_email as recebe_email2')
                ->leftjoin('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
                ->where('artw_perfis_usuarios_tickets.id_ticket', $ticket->id)
                ->where(function($query) {
                    $query->orWhere('artw_perfis_usuarios_tickets.id_usuario', Auth::user()->id);
                    $query->orWhere('artw_perfis_usuarios_equipe_tickets.id_usuario', Auth::user()->id);
                })
                ->get()->first();

        $recebe_email = false;

        if ($recebe && ($recebe->recebe_email1 || $recebe->recebe_email2)) {
            $recebe_email = true;
        }

        $response['recebe_email'] = $recebe_email;

        if (Auth::user()->gerente || Auth::user()->gerente_usuario || Auth::user()->admin) {

            $response['exibe_cancelar_pausar'] = true;
        } else {

            $response['exibe_cancelar_pausar'] = false;
        }

        if ( ($response['libera_ticket'] && Auth::user()->gerente) || ( ($response['id_status'] < 4 || $response['id_status'] >= 9) && !$response['participante'] ) || Auth::user()->id == $ticket->id_usuario) {
            $response['link_editar'] = route('site.artwork.ticket.editNew.get', $ticket->id);
        }

        if ( $ticket->modulo == 2 && ((Auth::user()->gerente) || ( ($response['id_status'] < 16 || $response['id_status'] != 9) && !$response['participante'] ) || Auth::user()->id == $ticket->id_usuario) ) {

            $response['link_editar'] = route('site.adsmart.ticket.edicao', $ticket->numero);
        }

        $members = [];
        $membersAll = [];
        
        
        $allMembers = AdmUsuarios
            ::select('adm_usuarios.*', 'adm_empresas.nome as nome_empresa', 'adm_departamentos.nome as nome_departamento')
            ->orderBy('nome')
            ->leftJoin('adm_empresas', 'adm_usuarios.id_adm_empresa', '=', 'adm_empresas.id')
            ->leftJoin('adm_departamentos', 'adm_usuarios.id_departamento', '=', 'adm_departamentos.id')
            ->withTrashed()
            ->get();

        foreach ($allMembers as $k => $member) {

            $membersAll[$k]['id'] = $member->id;
            $membersAll[$k]['nome'] = $member->nome_abreviado;
            $membersAll[$k]['empresa'] = ($member->nome_empresa) ? $member->nome_empresa : '';
            $membersAll[$k]['departamento'] = ($member->nome_departamento) ? $member->nome_departamento : '';

            if ($member->status == 1 && !$member->deleted_at) {
                $members[] = $membersAll[$k];
            }
        }

        $response['membros'] = $members;
        $response['membrosAll'] = $membersAll;

        return $response;
    }

    public function setStatusEnvio(Request $request)
    {
        $envio = ArtwArtesEnvios::find($request->input('id_arte_envio'));

        if (!$envio) {
            return response()->json(['error' => 404, 'message' => 'Not found'], 404);
        }

        $envio->status = ($envio->status) ? 0 : 1;
        $envio->save();

        $mensagem = (($envio->status)) ? 'O link para download dos arquivos de arte agora está ativo. Caso queira desativá-lo, basta clicar na chave novamente.' : 'O link para download dos arquivos de arte agora está inativo. Caso queira reativá-lo, basta clicar na chave novamente.';

        return response()->json(['success' => 200, 'message' => $mensagem], 200);
    }

    public function cancelarEnvioService($id_envio)
    {
        $envio = ArtwArtesEnvios::find($id_envio);

        if (!$envio) {
            return response()->json(['error' => 404, 'message' => 'Not found'], 404);
        }

        $arte = $envio->artwArte;

        $item = $arte->artwTicket->artwItem;

        foreach ($envio->envioEmails as $email)
        {
            $data = [
                'ticket' => $arte->artwTicket,
                'sku' => $arte->artwTicket->sku,
                'email' => $email->email,
                'arte' => $arte
            ];

            Mail::send('emails.notificacoes.email_notificacao_41', $data, function ($message) use($data) {
                $message->from(config('app.mail_from'), config('app.mail_name'));
                $message->to($data['email'])->subject('Envio Cancelado da Arte-final - ' . $data['ticket']->sku_nome_completo);
            });
        }
        
        //$envio->status = 2;
        $envio->cancelado_em = date('Y-m-d H:i:s');
        $envio->cancelado_por = Auth::user()->id;
        $envio->save();
    }

    public function cancelarEnvio(Request $request)
    {
        $this->cancelarEnvioService($request->input('id_arte_envio'));

        $mensagem = 'Envio cancelado com sucesso.';

        return response()->json(['success' => 200, 'message' => $mensagem], 200);
    }

    public function members(Request $request)
    {
        //sleep(10);

        $ignore = [];

        if ($request->input('participantes')) {
            foreach ($request->input('participantes') as $participante) {
                $ignore[] = $participante['id_usuario'];

                if (isset($participante['equipe_membros'])) {
                    foreach ($participante['equipe_membros'] as $membro) {
                        $ignore[] = $membro['id_usuario'];
                    }
                }
            }
        }

        $members = [];
        $membersAll = [];

        $allMembers = AdmUsuarios::orderBy('nome')
            ->withTrashed()
            ->get();

        foreach ($allMembers as $k => $member) {

            $membersAll[$k]['id'] = $member->id;
            $membersAll[$k]['nome'] = $member->nome_abreviado;
            $membersAll[$k]['empresa'] = ($member->admEmpresa) ? $member->admEmpresa->nome : '';
            $membersAll[$k]['departamento'] = ($member->admDepartamento) ? $member->admDepartamento->nome : '';
            $membersAll[$k]['ignore'] = in_array($member->id, $ignore);

            if ($member->status == 1 && !$member->deleted_at) {
                $members[] = $membersAll[$k];
            }
        }

        return ['members' => $members, 'membersAll' => $membersAll];
    }

    public function usuarios()
    {

        $usuarios = [];

        $users = AdmUsuarios::orderBy('nome')
            //->withTrashed()
            ->get();

        foreach ($users as $k => $user) {

            $usuarios[$k]['id'] = $user->id;
            $usuarios[$k]['nome'] = $user->nome_abreviado;
            $usuarios[$k]['empresa'] = ($user->admEmpresa) ? $user->admEmpresa->nome : '';
            $usuarios[$k]['departamento'] = ($user->admDepartamento) ? $user->admDepartamento->nome : '';

        }

        return ['usuarios' => $usuarios];
    }

    public function ticket_members($id_ticket)
    {
        $ticket = ArtwTickets::find($id_ticket);

        if (!$ticket) {
            return response()->json(['error' => 404, 'message' => 'Not found'], 404);
        }

        $response = [];
        
        $participantes = ArtwPerfisUsuariosTickets
                ::select('artw_perfis_usuarios_tickets.*', 'artw_perfis_usuarios.ordem')
                ->join('artw_perfis_usuarios', 'artw_perfis_usuarios.id', '=', 'artw_perfis_usuarios_tickets.id_perfil')
                ->join('adm_usuarios', 'artw_perfis_usuarios_tickets.id_usuario', '=', 'adm_usuarios.id')
                ->orderBy('artw_perfis_usuarios.ordem')
                ->orderBy('adm_usuarios.nome')
                ->orderBy('adm_usuarios.sobrenome')
                ->where('artw_perfis_usuarios_tickets.id_ticket', $ticket->id)
                ->get();
        
        //dd($participantes);
        
        foreach ($participantes as $k => $member) {
            $response[$k] = $member->toArray();
            $response[$k]['id_usuario_logado'] = Auth::user()->id;

            $response[$k]['equipe_membros'] = [];
            
            $participantesEquipe = ArtwPerfisUsuariosEquipeTickets
                ::select('artw_perfis_usuarios_equipe_tickets.*')
                ->join('adm_usuarios', 'artw_perfis_usuarios_equipe_tickets.id_usuario', '=', 'adm_usuarios.id')
                ->where('id_parent', $member->id)
                ->get();
            
            foreach ($participantesEquipe as $equipe) {
                $response[$k]['equipe_membros'][] = [
                    'id' => $equipe->id,
                    'id_usuario' => $equipe->id_usuario,
                    'recebe_email' => $equipe->recebe_email
                ];
            }
        }

        return $response;
    }

    public function categories()
    {
        $categorias = array();
        $allCategorias = CategoriasProdutos::select('id', 'nome')->orderBy('nome')->get();

        return $allCategorias;
    }

    public function families($id_categoria = null)
    {
        $familias = array();
        $allFamilias = Familias
            ::select('familias.id', 'familias.nome')
            ->distinct()
            ->join('produtos', 'produtos.id_familia', '=', 'familias.id');

        if ($id_categoria) {
            $allFamilias = $allFamilias->where('produtos.id_categoria_produto', $id_categoria);
        }

        $allFamilias = $allFamilias->orderBy('familias.nome')->get();

        return $allFamilias;
    }

    public function products($id_familia = null)
    {
        $produtos = [];

        $allProdutos = Produtos
                ::select('produtos.id', 'produtos.nome', 'familias.nome AS familia', 'categorias_produtos.nome AS categoria')
                ->join('familias', 'produtos.id_familia', '=', 'familias.id')
                ->join('categorias_produtos', 'produtos.id_categoria_produto', '=', 'categorias_produtos.id')
                ->where('produtos.status', 1)
                ->orderBy('produtos.nome')->get();

        return $allProdutos;
    }

    public function typesPackage()
    {
        $tipos = array();
        $allTipos = TiposEmbalagens::select('id', 'nome')->orderBy('nome')->where('id', '<>', '1')->get();

        return $allTipos;
    }

    public function typesItems()
    {
        $allTipos = ArtwItemTipos::select('id', 'nome')->orderBy('nome')->get();

        $retorno = [];

        foreach ($allTipos as $i => $tipo) {

            $retorno[$i] = $tipo;
            $retorno[$i]->id = (int) $tipo->id;
        }

        return $allTipos;
    }

    public function typesRequest()
    {
        $allTipos = ArtwSolicitacaoTipos::all();

        return $allTipos;
    }

    public function sizesPackage($type)
    {
        $volumes = array();
        $allVolumes = Dimensoes::select('id', 'nome')->orderBy('nome')->where('id_tipo_embalagem', $type)->get();

        return $allVolumes;
    }

    public function promotions($product)
    {
        $promos = array();
        $allPromos = Campanhas
            ::select('campanhas.*')
            ->distinct()
            ->join('artw_variacoes', 'artw_variacoes.id_campanha', '=', 'campanhas.id')
            ->join('skus', 'skus.id', '=', 'artw_variacoes.id_sku')
            ->where('skus.id_produto', $product)
            ->orderBy('campanhas.nome', 'DESC')
            ->get();

        return $allPromos;
    }

    public function perfis(AdmPerfis $admPerfis)
    {
        $perfis = $admPerfis->orderBy('nome')->get();

        return $perfis;
    }

    public function fornecedores(Fornecedores $artwFornecedores)
    {
        $fornecedores = $artwFornecedores->where('status', 1)->orderBy('nome')->get();

        $return = [];

        foreach ($fornecedores as $k => $fornecedor) {

            $return[$k] = $fornecedor->toArray();

            $emails = [];

            foreach ($fornecedor->fornecedoresEmails as $email) {
                $emails[] = $email->email;
            }

            $return[$k]['emails'] = implode(', ', $emails);
        }

        return $return;
    }

    public function alterarItem(Request $request)
    {
        $tipo = ArtwItemTipos::find($request->input('id_tipo'));
        $item = ArtwItens::find($request->input('id'));

        $check = ArtwItens
            ::join('artw_variacao_itens', 'artw_variacao_itens.id_item', '=', 'artw_itens.id')
            ->where('artw_variacao_itens.id_variacao', $item->id_variacao_parent)
            ->where('artw_itens.id_tipo', $request->input('id_tipo'))
            ->whereIn('artw_itens.status', [1, 2])
            ->get();

        //dd($check);

        if ($check->count() == 0) {

            $item->id_tipo = $tipo->id;
            $item->save();

            return response()->json(['success' => 200, 'message' => 'Item alterado com sucesso.'], 200);
        } else {

            return response()->json(['error' => 400, 'message' => 'Item já existente.'], 200);
        }
    }

    public function checkItem(Request $request)
    {
        $check = ArtwItens
            ::join('artw_variacao_itens', 'artw_variacao_itens.id_item', '=', 'artw_itens.id')
            ->where('artw_variacao_itens.id_variacao', $request->input('id'))
            ->where('artw_itens.id_tipo', $request->input('id_tipo'))
            ->whereNull('artw_itens.compartilhado')
            ->whereIn('artw_itens.status', [1, 2])
            ->get();

        if ($check->count() > 0) {

            $item = $check->last();

            return response()->json([
                    'error' => 400,
                    'message' => 'Item já existente.',
                    'status' => $item->status,
                    'id_item' => $item->id
                    ], 200);
        } else {

            return response()->json(['success' => 200, 'message' => 'Ok.'], 200);
        }
    }

    public function cadastrarItem(Request $request)
    {
        $tipo = ArtwItemTipos::find($request->input('id_tipo'));

        $item = ArtwItens::firstOrNew(['id_tipo' => $tipo->id, 'id_variacao' => $request->input('id_variacao')]);

        if (!$item->exists) {
            $item->novo = 1;
            $item->save();
        }
    }

    public function getEmbalagensPorNatureza($id_produto, $id_natureza)
    {
        $embalagens = ArtwVariacoes::select(
            'artw_variacoes.*', 'artw_variacao_tipos.nome as tipo', 'produtos.nome as produto_nome', 'tipos_embalagens.nome as tipo_embalagem', 'dimensoes.nome as dimensao_nome', 'campanhas.nome as campanha'
        )
        ->distinct()
        ->leftjoin('artw_variacao_itens', 'artw_variacao_itens.id_variacao', '=', 'artw_variacoes.id')
        ->leftjoin('artw_itens', 'artw_variacao_itens.id_item', '=', 'artw_itens.id')
        ->join('skus', 'artw_variacoes.id_sku', '=', 'skus.id')
        ->join('produtos', 'skus.id_produto', '=', 'produtos.id')
        ->join('dimensoes', 'skus.id_dimensao', '=', 'dimensoes.id')
        ->join('tipos_embalagens', 'dimensoes.id_tipo_embalagem', '=', 'tipos_embalagens.id')
        ->leftJoin('campanhas', 'campanhas.id', '=', 'artw_variacoes.id_campanha')
        ->leftJoin('artw_variacao_tipos', 'artw_variacao_tipos.id', '=', 'artw_variacoes.id_tipo')
        ->where('skus.id_produto', $id_produto)
        ->where('artw_variacoes.id_natureza_embalagem', $id_natureza)
        ->orderBy('produtos.nome')
        ->orderBy('tipos_embalagens.nome')
        ->orderBy('dimensoes.nome')
        ->orderBy('artw_variacoes.nome_original')
        ->get();

        $retorno = [];

        foreach ($embalagens as $e => $embalagem) {

            $nome = '';
            $nome = $embalagem->produto_nome
                . ' ' .
                $embalagem->tipo_embalagem
                . ' ' .
                $embalagem->dimensao_nome;
            if ($embalagem->campanha) {
                $nome .= ' • ' . $embalagem->campanha;
            }
            if ($embalagem->principal !== 1 && $embalagem->nome) {
                $nome .= ' • ' . $embalagem->nome;
            }

            $retorno[] = [
                'id' => $embalagem->id,
                'nome' => $nome
            ];

        }

        return $retorno;

    }

    public function getEmbalagens($id_produto, Common $common)
    {
        $retorno = [
            'ativos' => $common->getEmbalagens($id_produto, 1),
            'inativos' => $common->getEmbalagens($id_produto, 2),
        ];

        $produto = Produtos::find($id_produto);

        if ($produto->replicar_logo_marca)
        {

            $logo = (isset($produto->logotipos)) ? $produto->familias->logotipos->first() : null;
            $cor_hexa = ($produto->cor_hexa) ? $produto->cor_hexa : $produto->familias->cor_hexa;

        } else {

            $logo = (isset($produto->logotipos)) ? $produto->logotipos->where('id_tipo_logotipo', 2)->first() : null;
            $cor_hexa = $produto->cor_hexa;

        }

        $thumb = ($logo) ? $logo->thumb : '/images/layout/ticket-no-image-rubrum.png';

        $retorno['thumb'] = $thumb;
        $retorno['cor_hexa'] = $cor_hexa;
        $retorno['nome_produto'] = $produto->nome;
        $retorno['replicar_logo_marca'] = $produto->replicar_logo_marca;
        $retorno['marca'] = $produto->familias->nome;
        $retorno['categoria'] = $produto->categorias->nome;

        $retorno['item_tipos'] = ArtwItemTipos::orderBy('nome')->where('status', 1)->get();

        return $retorno;
    }

    public function getTodasEmbalagens(Common $common)
    {
        return $common->getTodasEmbalagens();
    }

    public function getTicketsHistorico()
    {
        $ids = [6];

        //DB::enableQueryLog();

        $queryTickets = ArtwTickets
            ::select(
                'artw_tickets.id',
                'artw_tickets.id_item',
                'artw_tickets.id_peca',
                'artw_tickets.modulo',
                'artw_tickets.numero',
                'artw_tickets.ciclo',
                'artw_tickets.codigo_cliente',
                'artw_tickets.id_status',
                'artw_tickets.timeline_etapas',
                'artw_tickets.timeline_subetapas',
                'artw_tickets.pausado_em',
                'artw_tickets.cancelado_em',
                'artw_tickets.dt_ini',
                'artw_tickets.dt_fim',
                'artw_projetos.nome as projeto_nome',
                'f1.nome as familia_nome_artwork',
                'f2.nome as familia_nome_adsmart',
                'artw_itens.compartilhado as item_compartilhado',
                'produtos.nome as produto_nome',
                'tipos_embalagens.nome as tipo_embalagem',
                'dimensoes.nome as dimensao_nome',
                'campanhas.nome as campanha',
                'artw_variacoes.nome as variacao_nome',
                'artw_variacoes.codigo_ean',
                'artw_variacoes.principal as variacao_principal',
                'artw_item_tipos.nome as item_tipo',
                'artw_tkt_status.nome as status_nome',
                'artw_tkt_status.ordem as status_ordem',

                'adsmart_pecas.nome as adsmart_peca_nome',
                'adsmart_campanhas.nome as adsmart_campanha_nome',
                'adsmart_midia_tipos.nome as adsmart_midia_tipo'
            )
            ->distinct()
            ->leftJoin('artw_projetos', 'artw_projetos.id', '=', 'artw_tickets.id_projeto')
            ->leftJoin('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
            ->leftJoin('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
            ->leftJoin('artw_itens', 'artw_tickets.id_item', '=', 'artw_itens.id')
            ->leftJoin('artw_item_tipos', 'artw_itens.id_tipo', '=', 'artw_item_tipos.id')
            ->leftJoin('artw_variacao_itens', 'artw_variacao_itens.id_item', '=', 'artw_itens.id')
            ->leftJoin('artw_variacoes', 'artw_variacao_itens.id_variacao', '=', 'artw_variacoes.id')
            ->leftJoin('skus', 'artw_variacoes.id_sku', '=', 'skus.id')
            ->leftJoin('produtos', 'produtos.id', '=', 'skus.id_produto')
            ->leftJoin('familias as f1', 'f1.id', '=', 'produtos.id_familia')
            ->leftJoin('familias as f2', 'f2.id', '=', 'artw_tickets.id_familia')
            ->leftJoin('dimensoes', 'dimensoes.id', '=', 'skus.id_dimensao')
            ->leftJoin('tipos_embalagens', 'tipos_embalagens.id', '=', 'dimensoes.id_tipo_embalagem')
            ->leftJoin('campanhas', 'campanhas.id', '=', 'artw_variacoes.id_campanha')
            ->leftJoin('artw_tkt_status', 'artw_tickets.id_status', '=', 'artw_tkt_status.id')

            ->leftJoin('adsmart_pecas', 'artw_tickets.id_peca', '=', 'adsmart_pecas.id')
            ->leftJoin('adsmart_campanha_midias', 'adsmart_pecas.id_campanha_midia', '=', 'adsmart_campanha_midias.id')

            ->leftJoin('adsmart_campanhas', 'adsmart_campanha_midias.id_campanha', '=', 'adsmart_campanhas.id')
            ->leftJoin('adsmart_midia_tipos', 'adsmart_campanha_midias.id_midia_tipo', '=', 'adsmart_midia_tipos.id')

            ->groupBy('artw_tickets.id')
            ->where(function ($query) use ($ids) {

                // primeira condição do where, é pegar os IDs corretos
                $query->whereIn('id_status', $ids);

                $query->where(function ($query) {

                    $query->orwhere('artw_tickets.id_item', '!=', null);
                    $query->orwhere('artw_tickets.id_peca', '!=', null);

                });

                $query->where('numero', 'not like', '100%');

                $query->whereIn('produtos.id_familia', Auth::user()->familias);

            })
            ->get();
            //->toSql();

            //dd($queryTickets);
            
        $data = [];

        foreach ($queryTickets as $k => $ticket) {

            if ($ticket->item_compartilhado && $ticket->modulo == 1) {

                // nova query
                $nome = $ticket->artwitem->nomeCompleto;

            } elseif ($ticket->modulo == 1) {

                $nome = $ticket->produto_nome
                    . ' ' .
                    $ticket->tipo_embalagem
                    . ' ' .
                    $ticket->dimensao_nome;
                if ($ticket->campanha) {
                    $nome .= ' • ' . $ticket->campanha;
                }
                if ($ticket->variacao_principal !== 1 && $ticket->variacao_nome) {
                    $nome .= ' • ' . $ticket->variacao_nome;
                }

                if ($ticket->item_tipo) {
                    $nome .= ' • ' . $ticket->item_tipo;
                }
            } elseif ($ticket->modulo == 2)
            {

                $nome = $ticket->adsmart_campanha_nome . ' ' . $ticket->adsmart_midia_tipo . ' • ' . $ticket->adsmart_peca_nome;

            }

            $data[$k] = $ticket;

            $data[$k]->timeline_etapas = '';

            $data[$k]->timeline_subetapas = '';

            // ícone fa-ticket - Não compartihado, em dia
            if ($ticket->dt_fim && $ticket->dt_fim->format('Y-m-d H:i:s') >= date('Y-m-d H:i:s') && !$ticket->pausado_em && !$ticket->item_compartilhado) {
                $data[$k]->icone = 'fa-ticket';
            }

            // ícone fa-share-alt-square - Compartilhado, em dia
            if ($ticket->dt_fim && $ticket->dt_fim->format('Y-m-d H:i:s') >= date('Y-m-d H:i:s') && !$ticket->pausado_em && $ticket->item_compartilhado) {
                $data[$k]->icone = 'fa-share-alt-square';
            }

            // fa-bomb - atrasado
            if ($ticket->dt_fim && $ticket->dt_fim->format('Y-m-d H:i:s') < date('Y-m-d H:i:s')) {
                $data[$k]->icone = 'fa-bomb';
            }

            // ícone fa-pause - pausado
            if ($ticket->pausado_em) {
                $data[$k]->icone = 'fa-pause';
            }

            $data[$k]->nome_completo = $ticket->numero . ' • ' . $nome;
            $data[$k]->compartilhado = $ticket->item_compartilhado;
            //$data[$k]->nome_completo = $ticket->numero . ' ' . $ticket->sku_nome_completo;
            $data[$k]->dt_inicio = ($ticket->dt_ini) ? $ticket->dt_ini->format('d/m/y') : null;
            $data[$k]->ts_inicio = ($ticket->dt_ini) ? $ticket->dt_ini->timestamp : null;
            $data[$k]->dt_final = ($ticket->dt_fim) ? $ticket->dt_fim->format('d/m/y') : null;
            $data[$k]->ts_final = ($ticket->dt_fim) ? $ticket->dt_fim->timestamp : null;
            //$data[$k]->prazo = $ticket->dt_fim->format('d/m/Y');
            //$data[$k]->prazo_timestamp = $ticket->dt_fim->timestamp;
            //$data[$k]->atrasado = ($ticket->dt_fim->format('Y-m-d H:i:s') < date('Y-m-d H:i:s')) ? 1 : 0;

            if ($ticket->modulo == 1) {
                $data[$k]->familia_nome = $ticket->familia_nome_artwork;
            } elseif ($ticket->modulo == 2)  {
                $data[$k]->familia_nome = $ticket->familia_nome_adsmart;
            }

            if ($ticket->modulo == 1)
            {
                $data[$k]->link = ($ticket->id_status == 9) ? route('site.artwork.ticket.members.get', $ticket->id) : route('site.artwork.ticket.edit.step1.get', $ticket->numero);

            } else {

                $data[$k]->link = ($ticket->id_status == 9) ? route('site.adsmart.ticket.membros', $ticket->numero) : route('site.adsmart.ticket', $ticket->numero);

            }
            

            if ($ticket->cancelado_em) {

                $data[$k]->status_nome = 'Cancelado';

            } elseif ($ticket->pausado_em) {

                $data[$k]->status_nome = $ticket->status_nome . ' - Pausado';

            } elseif ($ticket->dt_fim && $ticket->dt_fim->format('Y-m-d H:i:s') < date('Y-m-d H:i:s')) {

                $data[$k]->status_nome = $ticket->status_nome . ' - Atrasado';
                
            } else {

                $data[$k]->status_nome = $ticket->status_nome;
                
            }

            //$ticket->artwTarefa;

            //return $data;
        }

        $tickets = new Collection($data);

        return Datatables::of($tickets)->make(true);
    }

    public function getTickets($tipo = 'todos')
    {
        $ids = [0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16];
        $idsCancelados = [0, 1, 2, 3, 4, 5, 6, 9, 10, 11, 12, 14, 15, 16];

        //DB::enableQueryLog();

        $queryTickets = ArtwTickets
            ::select(
                'artw_tickets.id',
                'artw_tickets.id_item',
                'artw_tickets.id_peca',
                'artw_tickets.modulo',
                'artw_tickets.numero',
                'artw_tickets.ciclo',
                'artw_tickets.codigo_cliente',
                'artw_tickets.id_status',
                'artw_tickets.timeline_etapas',
                'artw_tickets.timeline_subetapas',
                'artw_tickets.pausado_em',
                'artw_tickets.cancelado_em',
                'artw_tickets.dt_ini',
                'artw_tickets.dt_fim',
                'artw_variacoes.codigo_ean',
                'artw_projetos.nome as projeto_nome',
                'f1.nome as familia_nome_artwork',
                'f2.nome as familia_nome_adsmart',
                'artw_itens.compartilhado as item_compartilhado',
                'produtos.nome as produto_nome',
                'tipos_embalagens.nome as tipo_embalagem',
                'dimensoes.nome as dimensao_nome',
                'campanhas.nome as campanha',
                'artw_variacoes.nome as variacao_nome',
                'artw_variacoes.principal as variacao_principal',
                'artw_item_tipos.nome as item_tipo',
                'artw_tkt_status.nome as status_nome',
                'artw_tkt_status.ordem as status_ordem',

                'adsmart_pecas.nome as adsmart_peca_nome',
                'adsmart_campanhas.nome as adsmart_campanha_nome',
                'adsmart_midia_tipos.nome as adsmart_midia_tipo'
            )
            ->distinct()
            ->leftJoin('artw_projetos', 'artw_projetos.id', '=', 'artw_tickets.id_projeto')
            ->leftJoin('artw_perfis_usuarios_tickets', 'artw_perfis_usuarios_tickets.id_ticket', '=', 'artw_tickets.id')
            ->leftJoin('artw_perfis_usuarios_equipe_tickets', 'artw_perfis_usuarios_equipe_tickets.id_parent', '=', 'artw_perfis_usuarios_tickets.id')
            ->leftJoin('artw_itens', 'artw_tickets.id_item', '=', 'artw_itens.id')
            ->leftJoin('artw_item_tipos', 'artw_itens.id_tipo', '=', 'artw_item_tipos.id')
            ->leftJoin('artw_variacao_itens', 'artw_variacao_itens.id_item', '=', 'artw_itens.id')
            ->leftJoin('artw_variacoes', 'artw_variacao_itens.id_variacao', '=', 'artw_variacoes.id')
            ->leftJoin('skus', 'artw_variacoes.id_sku', '=', 'skus.id')
            ->leftJoin('produtos', 'produtos.id', '=', 'skus.id_produto')
            ->leftJoin('familias as f1', 'f1.id', '=', 'produtos.id_familia')
            ->leftJoin('familias as f2', 'f2.id', '=', 'artw_tickets.id_familia')
            ->leftJoin('dimensoes', 'dimensoes.id', '=', 'skus.id_dimensao')
            ->leftJoin('tipos_embalagens', 'tipos_embalagens.id', '=', 'dimensoes.id_tipo_embalagem')
            ->leftJoin('campanhas', 'campanhas.id', '=', 'artw_variacoes.id_campanha')
            ->leftJoin('artw_tkt_status', 'artw_tickets.id_status', '=', 'artw_tkt_status.id')

            ->leftJoin('adsmart_pecas', 'artw_tickets.id_peca', '=', 'adsmart_pecas.id')
            ->leftJoin('adsmart_campanha_midias', 'adsmart_pecas.id_campanha_midia', '=', 'adsmart_campanha_midias.id')

            ->leftJoin('adsmart_campanhas', 'adsmart_campanha_midias.id_campanha', '=', 'adsmart_campanhas.id')
            ->leftJoin('adsmart_midia_tipos', 'adsmart_campanha_midias.id_midia_tipo', '=', 'adsmart_midia_tipos.id')

            ->groupBy('artw_tickets.id')
            ->where(function ($query) use ($ids, $idsCancelados, $tipo) {

                // primeira condição do where, é pegar os IDs corretos
                if ($tipo == 'cancelados') {
                    $query->whereIn('id_status', $idsCancelados);
                } else {
                    $query->whereIn('id_status', $ids);
                }

                $query->where(function ($query) {

                    $query->orwhere('artw_tickets.id_item', '!=', null);
                    $query->orwhere('artw_tickets.id_peca', '!=', null);

                });                

                // Se não for o admin, trazer somente os tickets que ele estiver participando
                if ( (Auth::user()->admin !== 1 && Auth::user()->gerente !== 1 && Auth::user()->acesso_tickets !== 1) || $tipo == 'minhas_tarefas' ) {

                    $query->where(function ($query) {

                        $query->orwhere(function($query) {
                            
                            $query->where('artw_perfis_usuarios_tickets.id_usuario', Auth::user()->id);
                            $query->where('artw_perfis_usuarios_tickets.deleted_at', null);

                        });

                        $query->orwhere(function($query) {
                            
                            $query->where('artw_perfis_usuarios_equipe_tickets.id_usuario', Auth::user()->id);
                            $query->where('artw_perfis_usuarios_equipe_tickets.deleted_at', null);

                        });

                        

                        

                    });
                }

                if ($tipo == 'atrasadas') {

                    $query->where('dt_fim', '<=', date('Y-m-d'));
                }

                if ($tipo == 'minhas_tarefas') {

                    $query->where(function($query) {

                        $query->orwhere(function($query) {

                            $query->where('artw_perfis_usuarios_tickets.id_perfil', 5)
                            ->whereIn('artw_tickets.id_status', [1])
                            ->where('artw_perfis_usuarios_tickets.aprovado', 0)
                            ->where('artw_perfis_usuarios_tickets.deleted_at', null);
                        });

                        $query->orwhere(function($query) {

                            $query->where('artw_perfis_usuarios_tickets.id_perfil', 3)
                            ->whereIn('artw_tickets.id_status', [2])
                            ->where('artw_perfis_usuarios_tickets.aprovado', 0)
                            ->where('artw_perfis_usuarios_tickets.deleted_at', null);
                        });

                        $query->orwhere(function($query) {

                            $query->whereIn('artw_perfis_usuarios_tickets.id_perfil', [5])
                            ->whereNull('artw_tickets.envio_autorizado')
                            ->whereIn('artw_tickets.id_status', [5])
                            ->where('artw_perfis_usuarios_tickets.deleted_at', null);
                        });

                        $query->orwhere(function($query) {

                            $query
                            ->where('artw_tickets.id_usuario', Auth::user()->id)
                            ->where('artw_tickets.envio_finalizado', 1)
                            ->whereIn('artw_tickets.id_status', [10]);
                        });

                        $query->orwhere(function($query) {

                            $query->whereIn('artw_perfis_usuarios_tickets.id_perfil', [6])
                            ->where('artw_tickets.envio_finalizado', null)
                            ->whereIn('artw_tickets.id_status', [10])
                            ->where('artw_perfis_usuarios_tickets.deleted_at', null);
                        });

                        $query->orwhere(function($query) {

                            $query->where('artw_perfis_usuarios_tickets.id_perfil', 2)
                            ->whereIn('artw_tickets.id_status', [3])
                            ->whereIn('artw_perfis_usuarios_tickets.aprovado', [0])
                            ->where('artw_perfis_usuarios_tickets.deleted_at', null);
                        });
                        
                        $query->orwhere(function($query) {

                            $query->where('artw_perfis_usuarios_tickets.id_perfil', 2)
                            ->whereIn('artw_tickets.id_status', [3])
                            ->whereIn('artw_perfis_usuarios_tickets.aprovado', [2])
                            ->whereRaw('artw_perfis_usuarios_tickets.ciclo < artw_tickets.ciclo')
                            ->where('artw_perfis_usuarios_tickets.deleted_at', null);
                        });
                        
                        $query->orwhere(function($query) {

                            $query->where('artw_perfis_usuarios_tickets.id_perfil', 7)
                            ->whereIn('artw_tickets.id_status', [11])
                            ->whereIn('artw_perfis_usuarios_tickets.aprovado', [0, 2])
                            ->where('artw_perfis_usuarios_tickets.deleted_at', null);
                        });
                        
                        $query->orwhere(function($query) {

                            //$query->where('artw_perfis_usuarios_tickets.id_perfil', 7)
                            //->where('artw_tickets.id_usuario', Auth::user()->id)
                            $query->whereIn('artw_tickets.id_status', [12])
                                ->where('artw_tickets.id_usuario', Auth::user()->id);
                                //->where('artw_perfis_usuarios_tickets.id_perfil', 7);
                            //->whereIn('artw_perfis_usuarios_tickets.aprovado', [1]);
                            
                        });

                        $query->orwhere(function($query) {

                            $query->where('artw_perfis_usuarios_tickets.id_perfil', 1)
                            ->whereIn('artw_tickets.id_status', [4])
                            ->whereIn('artw_perfis_usuarios_tickets.aprovado', [0])
                            ->where('artw_perfis_usuarios_tickets.deleted_at', null);
                        });
                        
                        $query->orwhere(function($query) {

                            $query->where('artw_perfis_usuarios_tickets.id_perfil', 1)
                            ->whereIn('artw_tickets.id_status', [4])
                            ->whereIn('artw_perfis_usuarios_tickets.aprovado', [2])
                            ->whereRaw('artw_perfis_usuarios_tickets.ciclo < artw_tickets.ciclo')
                            ->where('artw_perfis_usuarios_tickets.deleted_at', null);
                        });

                        $query->orwhere(function($query) {

                            $query->where('artw_perfis_usuarios_tickets.id_perfil', 9)
                            ->whereIn('artw_tickets.id_status', [14])
                            ->whereIn('artw_perfis_usuarios_tickets.aprovado', [0])
                            ->where('artw_perfis_usuarios_tickets.deleted_at', null);
                        });

                        $query->orwhere(function($query) {

                            $query->where('artw_perfis_usuarios_tickets.id_perfil', 10)
                            ->whereIn('artw_tickets.id_status', [15])
                            ->whereIn('artw_perfis_usuarios_tickets.aprovado', [0])
                            ->where('artw_perfis_usuarios_tickets.deleted_at', null);
                        });

                        $query->orwhere(function($query) {

                            $query->where('artw_perfis_usuarios_tickets.id_perfil', 11)
                            ->whereIn('artw_tickets.id_status', [16])
                            ->whereIn('artw_perfis_usuarios_tickets.aprovado', [0])
                            ->where('artw_perfis_usuarios_tickets.deleted_at', null);
                        });

                        $query->orwhere(function($query) {

                            $query->where('artw_perfis_usuarios_tickets.id_perfil', 11)
                            ->whereIn('artw_tickets.id_status', [16])
                            ->whereIn('artw_perfis_usuarios_tickets.aprovado', [2])
                            ->whereRaw('artw_perfis_usuarios_tickets.ciclo < artw_tickets.ciclo')
                            ->where('artw_perfis_usuarios_tickets.deleted_at', null);
                        });
                        
                    });
                }

                if ($tipo == 'meus') {

                    $query->where('artw_tickets.id_usuario', Auth::user()->id);
                }

                if ($tipo == 'cancelados') {

                    $query->whereNotNull('cancelado_em');
                    
                    $query->where('artw_tickets.id_status', '!=', 9);
                    
                } else {

                    $query->whereNull('cancelado_em');
                }

                //dd($tipo);

                /*
                if ($tipo == 'pausados') {

                    $query->whereNotNull('pausado_em');
                } elseif ($tipo !== 'todos') {

                    $query->whereNull('pausado_em');
                }
                */
            })
            ->orwhere(function ($query) use ($tipo) {

                if ($tipo !== 'cancelados' && $tipo !== 'pausados') {

                    $query->where('artw_tickets.id_usuario', Auth::user()->id);
                    $query->whereNull('artw_tickets.cancelado_em');
                    $query->where('artw_tickets.id_status', 9);
                    $query->whereNull('cancelado_em');
                }
            })
            ->orwhere(function ($query) use ($tipo) {
                if ((Auth::user()->id_adm_perfil == 1 || Auth::user()->gerente == 1) && $tipo !== 'cancelados' && $tipo !== 'pausados') {
                    $query->where('artw_tickets.id_status', 0)
                        ->whereNull('artw_tickets.cancelado_por');
                }
            })
            ->get();
            //->toSql();

            //dd($queryTickets);
            
        $data = [];

        foreach ($queryTickets as $k => $ticket) {

            if ($ticket->item_compartilhado && $ticket->modulo == 1) {

                // nova query
                $nome = $ticket->artwitem->nomeCompleto;

            } elseif ($ticket->modulo == 1) {

                $nome = $ticket->produto_nome
                    . ' ' .
                    $ticket->tipo_embalagem
                    . ' ' .
                    $ticket->dimensao_nome;
                if ($ticket->campanha) {
                    $nome .= ' • ' . $ticket->campanha;
                }
                if ($ticket->variacao_principal !== 1 && $ticket->variacao_nome) {
                    $nome .= ' • ' . $ticket->variacao_nome;
                }

                if ($ticket->item_tipo) {
                    $nome .= ' • ' . $ticket->item_tipo;
                }
            } elseif ($ticket->modulo == 2)
            {

                $nome = $ticket->adsmart_campanha_nome . ' ' . $ticket->adsmart_midia_tipo . ' • ' . $ticket->adsmart_peca_nome;

            }

            $data[$k] = $ticket;

            $etapas = (array) json_decode($ticket->timeline_etapas);

            $timeline_etapas = [];

            foreach ($etapas as $etapa){

                /*
                $data[$k]->timeline_etapas .= '<div class="dropdown dropdown-task">
                    <div ng-click="dropdown_tarefas('.$ticket->id.', \''.$etapa->id.'\')" class="task fa '.$etapa->iconClass.' '.$etapa->class.'"></div>
                    <div class="dropdown-list dropdown-task-list dropdown-list-auto"><a href="javascript:;" class="arrow-up fa fa-angle-up"></a><a href="javascript:;" class="arrow-down fa fa-angle-down"></a></div>
                </div>';
                */

                $timeline_etapas[$etapa->id] = [
                    'html' => '<div ng-click="dropdown_tarefas('.$ticket->id.', \''.$etapa->id.'\')" class="task fa '.$etapa->iconClass.' '.$etapa->class.'"></div>',
                    'nome' => $etapa->nome
                ];

                //echo $data[$k]->timeline_etapas;
                //exit;

            }

            $data[$k]->timeline_etapas = ($ticket->modulo == 1 && !$ticket->cancelado_em) ? $timeline_etapas : null;

            $subEtapas = (array) json_decode($ticket->timeline_subetapas);

            $data[$k]->timeline_subetapas = '';

            foreach ($subEtapas as $subetapa){

                switch ($subetapa) {
                    case 1:
                        $class = 'current';
                        break;
                    case 2:
                        $class = 'disapproved';
                        break;
                    default:
                        $class = 'enabled';
                        break;
                }

                $data[$k]->timeline_subetapas .= '<li class="bubble '.$class.'"></li>';

            }

            // ícone fa-ticket - Não compartihado, em dia
            if ($ticket->dt_fim->format('Y-m-d H:i:s') >= date('Y-m-d H:i:s') && !$ticket->pausado_em && !$ticket->item_compartilhado) {
                $data[$k]->icone = 'fa-ticket';
            }

            // ícone fa-share-alt-square - Compartilhado, em dia
            if ($ticket->dt_fim->format('Y-m-d H:i:s') >= date('Y-m-d H:i:s') && !$ticket->pausado_em && $ticket->item_compartilhado) {
                $data[$k]->icone = 'fa-share-alt-square';
            }

            // fa-bomb - atrasado
            if ($ticket->dt_fim->format('Y-m-d H:i:s') < date('Y-m-d H:i:s')) {
                $data[$k]->icone = 'fa-bomb';
            }

            // ícone fa-pause - pausado
            if ($ticket->pausado_em) {
                $data[$k]->icone = 'fa-pause';
            }

            $data[$k]->nome_completo = $ticket->numero . ' • ' . $nome;
            $data[$k]->compartilhado = $ticket->item_compartilhado;
            //$data[$k]->nome_completo = $ticket->numero . ' ' . $ticket->sku_nome_completo;
            $data[$k]->dt_inicio = $ticket->dt_ini->format('d/m/y');
            $data[$k]->ts_inicio = $ticket->dt_ini->timestamp;
            $data[$k]->dt_final = $ticket->dt_fim->format('d/m/y');
            $data[$k]->ts_final = $ticket->dt_fim->timestamp;
            //$data[$k]->prazo = $ticket->dt_fim->format('d/m/Y');
            //$data[$k]->prazo_timestamp = $ticket->dt_fim->timestamp;
            //$data[$k]->atrasado = ($ticket->dt_fim->format('Y-m-d H:i:s') < date('Y-m-d H:i:s')) ? 1 : 0;

            if ($ticket->modulo == 1) {
                $data[$k]->familia_nome = $ticket->familia_nome_artwork;
            } elseif ($ticket->modulo == 2)  {
                $data[$k]->familia_nome = $ticket->familia_nome_adsmart;
            }

            if ($ticket->modulo == 1)
            {
                $data[$k]->link = ($ticket->id_status == 9) ? route('site.artwork.ticket.members.get', $ticket->id) : route('site.artwork.ticket.edit.step1.get', $ticket->numero);

            } else {

                $data[$k]->link = ($ticket->id_status == 9) ? route('site.adsmart.ticket.membros', $ticket->numero) : route('site.adsmart.ticket', $ticket->numero);

            }
            

            if ($ticket->cancelado_em) {

                $data[$k]->status_nome = 'Cancelado';

            } elseif ($ticket->pausado_em) {

                $data[$k]->status_nome = $ticket->status_nome . ' - Pausado';

            } elseif ($ticket->dt_fim->format('Y-m-d H:i:s') < date('Y-m-d H:i:s')) {

                $data[$k]->status_nome = $ticket->status_nome . ' - Atrasado';
                
            } else {

                $data[$k]->status_nome = $ticket->status_nome;
                
            }

            //$ticket->artwTarefa;

            //return $data;
        }

        $tickets = new Collection($data);

        return Datatables::of($tickets)->make(true);
    }

    public function descontinuarItem(Request $request)
    {
        $item = ArtwItens::find($request->input('id_item'));
        $item->status = 2;
        $item->save();

        return response()->json(['success' => 200, 'message' => 'Item descontinuado com sucesso.'], 200);
    }

    public function retornarItem(Request $request)
    {
        $item = ArtwItens::find($request->input('id_item'));
        $item->status = 1;
        $item->save();

        return response()->json(['success' => 200, 'message' => 'Item atualizado com sucesso.'], 200);
    }

    public function excluirItem(Request $request)
    {
        $item = ArtwItens::find($request->input('id_item'));

        if ($item->ticketsGerais()) {
            return response()->json(['success' => 400, 'message' => 'Item não pode ser exluído!'], 200);
        }

        $item->delete();

        return response()->json(['success' => 200, 'message' => 'Item exluído com sucesso.'], 200);
    }

    public function excluirEmbalagem(Request $request)
    {
        $variacao = ArtwVariacoes::withTrashed()->find($request->input('id_embalagem'));

        if ($variacao->ticketsGerais()) {
            return response()->json(['success' => 400, 'message' => 'Variação não pode ser exluído!'], 200);
        }
        
        DB::beginTransaction();
        
        $variacao->deletar();
        
        DB::commit();

        return response()->json(['success' => 200, 'message' => 'Embalagem excluída com sucesso.'], 200);
    }

    public function postPackageCreate(Artwork\PostPackageCreateForm $request)
    {
        //dd($request->all());

        DB::beginTransaction();

        if ($request->input('cbPckType') == 0) {
            $embalagem = TiposEmbalagens::firstOrNew(['nome' => $request->input('packagePackingName')]);

            if (!$embalagem->exists) {
                $embalagem->nome = $request->input('packagePackingName');
                $embalagem->status = 0;
                $embalagem->id_natureza_embalagem = $request->input('natureza');
                $embalagem->save();
            }
        } elseif ($request->input('cbPckType')) {
            $embalagem = TiposEmbalagens::find($request->input('cbPckType'));
        }

        //echo '::Embalagem: ' . $embalagem->nome . '<br>';
        // Volume
        if ($request->input('cbPckContentSize') == 0) {

            $dimensao = Dimensoes::firstOrNew(['nome' => $request->input('packageContentSpec'), 'id_tipo_embalagem' => $embalagem->id]);

            if (!$dimensao->exists) {
                $dimensao->nome = $request->input('packageContentSpec');
                $dimensao->status = 0;
                //$dimensao->id_tipo_embalagem = $embalagem->id;
                $dimensao->save();
            }
        } elseif ($request->input('cbPckContentSize')) {
            $dimensao = Dimensoes::find($request->input('cbPckContentSize'));
        }

        //echo '::Volume: ' . $dimensao->nome . '<br>';

        if ($request->input('cbPckProduct') == 0) {
            $produto = Produtos::firstOrNew(['nome' => $request->input('tfNewProductName')]);

            if (!$produto->exists) {
                $produto->nome = $request->input('tfNewProductName');
                $produto->status = 0;
                $produto->artwork = 1;
                //$produto->id_familia = $familia->id;
                //$produto->id_categoria_produto = $categoria->id;
                $produto->save();
            }
        } elseif ($request->input('cbPckProduct')) {
            $produto = Produtos::find($request->input('cbPckProduct'));
        }

        $familia = $produto->familias;

        if ($request->input('packageType') == 'Sim' && $request->input('cbPckCampaign') == 0) {
            $campanha = Campanhas::firstOrNew(['nome' => $request->input('packageCampaignName')]);

            if (!$campanha->exists) {
                $campanha->save();
            }
        } elseif ($request->input('cbPckCampaign')) {
            $campanha = Campanhas::find($request->input('cbPckCampaign'));
        }

        //echo (isset($campanha)) ? '::Campanha_id: ' . $campanha->id . '<br>' : '';
        //echo (isset($campanha)) ? '::Campanha_nome: ' . $campanha->nome . '<br>' : '';
        // Verifica o SKU no banco
        $sku = Skus::firstOrNew(['id_produto' => $produto->id, 'id_dimensao' => $dimensao->id]);

        if (!$sku->exists) {
            $sku->nome = $produto->nome . ' - ' . $embalagem->nome . ' - ' . $dimensao->nome;
            //$sku->status = 0;
            //$sku->novo = 1;
            $sku->promocional = ($request->input('packageType') == 'Promocional') ? 1 : 0;
            $sku->save();
        }

        if ($request->input('variacaoName') && $request->input('packageHasVariation') !== 'Nao') {
            $variacao_nome = $request->input('variacaoName');
            $principal = 0;
        } else {
            $variacao_nome = 'Original';
            $principal = 1;
        }

        $id_campanha = (isset($campanha) && $campanha) ? $campanha->id : null;

        $variacao = ArtwVariacoes::firstOrNew(['id_sku' => $sku->id, 'nome' => $variacao_nome, 'id_campanha' => $id_campanha]);

        //dd($variacao_nome, $variacao);

        if ($variacao->exists) {
            $itensAtivos = ArtwItens::where('id_variacao_parent', $variacao->id)->where('status', 1)->get();
            $itensInativos = ArtwItens::where('id_variacao_parent', $variacao->id)->where('status', 2)->get();

            if ($itensAtivos->count() == 0 && $itensInativos->count() > 0) {
                return response()->json([
                        'success' => 201,
                        'message' => 'A embalagem que você tentou criar já existe porém encontra-se Descontinuada, deseja Retornar para Mercado?',
                        'id_variacao' => $variacao->id
                        ], 200);
            }
        }

        $variacao->id_tipo = ($request->input('packageType') == 'Promocional') ? 2 : 1;

        $variacao->id_natureza_embalagem = $request->input('cbPckNatureza');

        $variacao->tem_codigo_ean = ($request->input('pckEANCode') == '1') ? 0 : 1;
        $variacao->codigo_ean = ($request->input('txtPckEANCode') && $request->input('pckEANCode') == 3) ? $request->input('txtPckEANCode') : null;

        if ($request->input('txtPckEANCode'))
        {
            $variacao->codigo_ean_alteracao = date('Y-m-d H:i:s');
        }

        $variacao->principal = $principal;
        $variacao->novo = 1;

        $variacao->nome_original = $variacao->sku_nome_completo;

        $variacao->save();

        //Verificam os Itens
        $itensAtivos = ArtwItens::where('id_variacao_parent', $variacao->id)->where('status', 1)->get();

        $mercados = ($request->input('cbPckMarket')) ? $request->input('cbPckMarket') : [];

        $variacao->mercados()->sync($mercados);

        $variacoesRelacionadas = [];
        
        foreach ((array) $request->input('embalagensRelacionadas') as $relacionada)
        {
            if ($variacao->id != $relacionada['id'])
            {
                $variacoesRelacionadas[]['id_variacao_relacionada'] = $relacionada['id'];
            }
        }
        
        $variacao->variacoesRelacionadas()->sync($variacoesRelacionadas);

        DB::commit();

        $url = route("site.artwork.package", [$variacao->sku->produtos->familias->id, $variacao->sku->produtos->id]);

        return response()->json(['success' => 200, 'message' => 'Embalagem criada com sucesso.', 'url' => $url], 200);
    }

    public function postPackageEdit(Artwork\PostPackageCreateForm $request)
    {
        //dd($request->all());

        $id_variacao = $request->input('id_variacao');

        $variacao = ArtwVariacoes::find($id_variacao);

        $id_campanha_original = $variacao->id_campanha;

        if ($request->input('cbPckType') == 0) {
            $embalagem = TiposEmbalagens::firstOrNew(['nome' => $request->input('packagePackingName')]);

            if (!$embalagem->exists) {
                $embalagem->nome = $request->input('packagePackingName');
                $embalagem->status = 0;
                $embalagem->id_natureza_embalagem = $request->input('natureza');
                $embalagem->save();
            }
        } elseif ($request->input('cbPckType')) {
            $embalagem = TiposEmbalagens::find($request->input('cbPckType'));
        }

        // Volume
        if ($request->input('cbPckContentSize') == 0) {
            $dimensao = Dimensoes::firstOrNew(['nome' => $request->input('packageContentSpec'), 'id_tipo_embalagem' => $embalagem->id]);

            if (!$dimensao->exists) {
                $dimensao->nome = $request->input('packageContentSpec');
                $dimensao->status = 0;
                //$dimensao->id_tipo_embalagem = $embalagem->id;
                $dimensao->save();
            }
        } elseif ($request->input('cbPckContentSize')) {
            $dimensao = Dimensoes::find($request->input('cbPckContentSize'));
        }

        if ($request->input('cbPckProduct') == 0) {
            $produto = Produtos::firstOrNew(['nome' => $request->input('tfNewProductName')]);

            if (!$produto->exists) {
                $produto->nome = $request->input('tfNewProductName');
                $produto->status = 0;
                $produto->artwork = 1;
                //$produto->id_familia = $familia->id;
                //$produto->id_categoria_produto = $categoria->id;
                $produto->save();
            }
        } elseif ($request->input('cbPckProduct')) {
            $produto = Produtos::find($request->input('cbPckProduct'));
        }

        $familia = $produto->familias;

        if ($request->input('packageType') == 'Sim' && $request->input('cbPckCampaign') == 0) {
            $campanha = Campanhas::firstOrNew(['nome' => $request->input('packageCampaignName')]);

            if (!$campanha->exists) {
                $campanha->save();
            }
        } elseif ($request->input('cbPckCampaign')) {
            $campanha = Campanhas::find($request->input('cbPckCampaign'));
        }

        // Verifica o SKU no banco
        $sku = Skus::firstOrNew(['id_produto' => $produto->id, 'id_dimensao' => $dimensao->id]);

        if (!$sku->exists) {
            $sku->nome = $produto->nome . ' - ' . $embalagem->nome . ' - ' . $dimensao->nome;
            //$sku->status = 0;
            //$sku->novo = 1;
            $sku->promocional = ($request->input('packageType') == 'Promocional') ? 1 : 0;
            $sku->save();
        }

        if ($request->input('variacaoName') && $request->input('packageHasVariation') == 'Sim') {
            $variacao_nome = $request->input('variacaoName');
            $principal = 0;
        } else {
            $variacao_nome = 'Original';
            $principal = 1;
        }


        $id_campanha = (isset($campanha) && $campanha) ? $campanha->id : null;

        $checkVariacao = ArtwVariacoes::select('*')
            ->where('id', '!=', $id_variacao)
            ->where('id_sku', $sku->id)
            ->where('nome', $variacao_nome)
            ->where('id_campanha', $id_campanha)
            //->where('id_sku', $sku->id)
            ->get();

        //dd($id_campanha, $id_campanha_original);

        if ($checkVariacao->count() && $variacao->nome !== $variacao_nome) {

            //return back()->with('warning', 'O nome selecionado para esta embalagem já está sendo utilizado. É possível cadastrar essa embalagem como uma variação, ou então selecionar um nome diferente para ela.');
            return response()->json(['success' => 202, 'message' => 'O nome selecionado para esta embalagem já está sendo utilizado. É possível cadastrar essa embalagem como uma variação, ou então selecionar um nome diferente para ela.'], 200);
        }

        if ($checkVariacao->count() && $id_campanha_original !== $id_campanha) {

            //return back()->with('warning', 'O nome selecionado para esta embalagem já está sendo utilizado. É possível cadastrar essa embalagem como uma variação, ou então selecionar um nome diferente para ela.');
            return response()->json(['success' => 202, 'message' => 'O nome selecionado para esta embalagem já está sendo utilizado. É possível cadastrar essa embalagem como uma variação, ou então selecionar um nome diferente para ela.'], 200);
        }

        $variacao->id_sku = $sku->id;
        $variacao->nome = $variacao_nome;
        $variacao->id_campanha = $id_campanha;
        $variacao->id_tipo = ($request->input('packageType') == 'Promocional') ? 2 : 1;
        $variacao->id_natureza_embalagem = $request->input('cbPckNatureza');
        $variacao->tem_codigo_ean = ($request->input('pckEANCode') == '1') ? 0 : 1;

        $codigo_ean_antigo = $variacao->codigo_ean;

        $variacao->codigo_ean = ($request->input('txtPckEANCode') && $request->input('pckEANCode') == 3) ? $request->input('txtPckEANCode') : null;

        if ($codigo_ean_antigo != $variacao->codigo_ean)
        {
            $variacao->codigo_ean_alteracao = date('Y-m-d H:i:s');
        }

        $variacao->principal = $principal;
        //$variacao->novo = 1;

        $variacao->nome_original = $variacao->sku_nome_completo;

        $variacao->save();

        $mercados = ($request->input('cbPckMarket')) ? $request->input('cbPckMarket') : [];

        $variacao->mercados()->sync($mercados);

        $variacoesRelacionadas = [];
        
        foreach ((array) $request->input('embalagensRelacionadas') as $relacionada)
        {
            if ($variacao->id != $relacionada['id'])
            {
                $variacoesRelacionadas[]['id_variacao_relacionada'] = $relacionada['id'];
            }
        }
        
        $variacao->variacoesRelacionadas()->sync($variacoesRelacionadas);

        $url = route("site.artwork.package", [$variacao->sku->produtos->familias->id, $variacao->sku->produtos->id]);

        return response()->json(['success' => 200, 'message' => 'Embalagem salva com sucesso.', 'url' => $url], 200);

        //return redirect()->route("site.artwork.package", [$familia->id, $produto->id])->with('message', 'Embalagem salva com sucesso.');
    }

    public function retornarEmbalagemMercado(Request $request)
    {
        $variacao = ArtwVariacoes::withTrashed()->find($request->input('id_variacao'));

        if (!$variacao) {
            return response()->json(['success' => 404, 'message' => 'Ocorreu um erro!'], 404);
        }

        DB::beginTransaction();

        foreach ($variacao->artwItens as $item) {
            $item->status = 1;
            $item->save();
        }

        DB::commit();

        $url = route("site.artwork.package", [$variacao->sku->produtos->familias->id, $variacao->sku->produtos->id]);

        return response()->json(['success' => 200, 'message' => 'Embalagem Retornada para Mercado com sucesso.', 'url' => $url], 200);
    }
    
    public function gruposDeAprovacao($global)
    {
        $grupos = ArtwGruposPerfis
            ::where('global', $global);
            //->with(['artwGruposPerfisUsuarios', 'artwGruposPerfisUsuarios.artwGruposPerfisUsuariosEquipe']);
        
        if (!$global) {
            
            $grupos = $grupos->where('id_usuario', Auth::user()->id);
            
        }
        
        $grupos = $grupos->get();
        
        $retorno = [];
        
        foreach ($grupos as $g=>$grupo)
        {
            $retorno[$g] = $grupo->toArray();
            
            $retorno[$g]['artw_grupos_perfis_usuarios'][0] = [
                
                'id_grupo_perfil' => $grupo->id,
                'id_usuario' => Auth::user()->id,
                'id_perfil' => null,
                'artw_grupos_perfis_usuarios_equipe' => []
                
            ];
            
            foreach ($grupo->artwGruposPerfisUsuarios as $p=>$participante)
            {
                $excluir = null;
                
                if ($participante->id_usuario == Auth::user()->id && !$retorno[$g]['artw_grupos_perfis_usuarios'][0]['id_perfil'])
                {
                    $retorno[$g]['artw_grupos_perfis_usuarios'][0]['id_perfil'] = $participante->id_perfil;
                    $retorno[$g]['artw_grupos_perfis_usuarios'][0]['artw_grupos_perfis_usuarios_equipe'] = $participante->artwGruposPerfisUsuariosEquipe;
                }
                else
                {
                    $retorno[$g]['artw_grupos_perfis_usuarios'][$p+1] = $participante->toArray();
                    $retorno[$g]['artw_grupos_perfis_usuarios'][$p+1]['artw_grupos_perfis_usuarios_equipe'] = [];
                
                    foreach ($participante->artwGruposPerfisUsuariosEquipe as $m=>$membro)
                    {
                        if ($membro->id_usuario == Auth::user()->id && !$retorno[$g]['artw_grupos_perfis_usuarios'][0]['id_perfil'])
                        {
                            $retorno[$g]['artw_grupos_perfis_usuarios'][0]['id_perfil'] = $participante->id_perfil;
                            
                            
                            $excluir = $p+1;
                        }
                        else
                        {
                            $retorno[$g]['artw_grupos_perfis_usuarios'][$p+1]['artw_grupos_perfis_usuarios_equipe'][$m] = $membro;
                        }
                    }
                    
                    if ($excluir)
                    {
                        $primeiroEquipe[0] = [
                            
                            'id_usuario' => $retorno[$g]['artw_grupos_perfis_usuarios'][$p+1]['id_usuario']
                            
                        ];
                        
                        if (isset($retorno[$g]['artw_grupos_perfis_usuarios'][$p+1]['artw_grupos_perfis_usuarios_equipe']) && count($retorno[$g]['artw_grupos_perfis_usuarios'][$p+1]['artw_grupos_perfis_usuarios_equipe']))
                        {
                            foreach ($retorno[$g]['artw_grupos_perfis_usuarios'][$p+1]['artw_grupos_perfis_usuarios_equipe'] as $e=>$equipe)
                            {
                                $equipes[$e+1] = $equipe;
                            }
                            
                            $retorno[$g]['artw_grupos_perfis_usuarios'][0]['artw_grupos_perfis_usuarios_equipe'] = array_merge($primeiroEquipe, $equipes);
                        }
                        else
                        {
                            $retorno[$g]['artw_grupos_perfis_usuarios'][0]['artw_grupos_perfis_usuarios_equipe'] = $primeiroEquipe;
                        }
                         
                        unset($retorno[$g]['artw_grupos_perfis_usuarios'][$p+1]);
                    }
                }
            }
        }
        
        return $retorno;
    }
    
    public function getGruposDeAprovacao()
    {
        /*
        $locaisAll = ArtwGruposPerfis
            ::where('global', 0)
            ->where('id_usuario', Auth::user()->id)
            ->with(['artwGruposPerfisUsuarios', 'artwGruposPerfisUsuarios.artwGruposPerfisUsuariosEquipe'])
            ->get();
        
        
        $globaisAll = ArtwGruposPerfis
            ::where('global', 1)
            ->with(['artwGruposPerfisUsuarios', 'artwGruposPerfisUsuarios.artwGruposPerfisUsuariosEquipe'])
            ->get();
         * 
         */
        
        $retorno = [
            'locais' => $this->gruposDeAprovacao(0),
            'globais' => $this->gruposDeAprovacao(1),
        ];
        
        return $retorno;
    }
    
    public function postGruposDeAprovacao(Request $request)
    {
        $global = ($request->input('grupo_global') == 'true') ? 1 : 0;

        if ($global)
        {
            $grupoPerfil = ArtwGruposPerfis::firstOrNew([
            
                'nome' => $request->input('grupo_nome'),
                'global' => $global
                
            ]);
        }
        else
        {
            $grupoPerfil = ArtwGruposPerfis::firstOrNew([
            
                'nome' => $request->input('grupo_nome'),
                'global' => $global,
                'id_usuario' => Auth::user()->id
                
            ]);
        }

        if ($grupoPerfil->exists)
        {
            return response()->json(['success' => 201, 'message' => 'Já existe um Grupo de Trabalho com este nome. Escolha um nome diferente.'], 200);
        }
        
        $grupoPerfil->global = $global;
        $grupoPerfil->id_usuario = Auth::user()->id;
        
        $grupoPerfil->save();
        
        $grupoPerfil->artwGruposPerfisUsuarios()->delete();
        
        $salvar = [];
        
        $participantes = $request->input('participantes');
        
        foreach($participantes as $p=>$participante)
        {
            $grupoPerfilUsuario = new \App\ArtwGruposPerfisUsuarios;
            $grupoPerfilUsuario->id_grupo_perfil = $grupoPerfil->id;
            $grupoPerfilUsuario->id_usuario = $participante['id_usuario'];
            $grupoPerfilUsuario->id_perfil = $request->input('cbMembershipProfile.' . $p);
            
            $grupoPerfilUsuario->save();
            
            
            if (isset($participante['equipe_membros']) && $participante['equipe_membros']) {
                
                foreach ($participante['equipe_membros'] as $m=>$membro) {
                    
                    $grupoPerfilUsuarioEquipe = new \App\ArtwGruposPerfisUsuariosEquipe;
                    $grupoPerfilUsuarioEquipe->id_usuario = $membro['id_usuario'];
                    $grupoPerfilUsuarioEquipe->id_parent = $grupoPerfilUsuario->id;
                    $grupoPerfilUsuarioEquipe->save();
                    
                }
                
            }
        }
        
        return response()->json(['success' => 200, 'message' => 'Ok'], 200);
    }
    
    public function postSalvarGruposDeAprovacao(Request $request)
    {
        $global = ($request->input('grupo_global') == 'true') ? 1 : 0;

        if ($global)
        {
            $exists = ArtwGruposPerfis
                ::where('id', '!=', $request->input('grupo_id'))
                ->where('global', $global)
                ->where('nome', $request->input('grupo_nome'))
                ->count();
        }
        else
        {
            $exists = ArtwGruposPerfis
                ::where('id', '!=', $request->input('grupo_id'))
                ->where('global', $global)
                ->where('nome', $request->input('grupo_nome'))
                ->where('id_usuario', Auth::user()->id)
                ->count();
        }

        if ($exists)
        {
            return response()->json(['success' => 201, 'message' => 'Já existe um Grupo de Trabalho com este nome. Escolha um nome diferente.'], 200);
        }

        $grupoPerfil = ArtwGruposPerfis::find($request->input('grupo_id'));
        $grupoPerfil->nome = $request->input('grupo_nome');
        $grupoPerfil->global = $global;
        
        $grupoPerfil->save();
        
        return [];
    }
    
    public function postExcluirGruposDeAprovacao(Request $request)
    {
        $grupoPerfil = ArtwGruposPerfis::find($request->input('grupo_id'));
        $grupoPerfil->delete();
        
        return [];
    }
    
}
