<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

use App\TdTechdraws;
use App\TdVariacoes;
use App\TdVersoes;
use App\TdItens;
use App\ArtwItemTipos;
use App\Components\Common;

class TechdrawServiceController extends Controller
{

    public function checkVariacoes($id_dimensao)
    {
        $techdraw = TdTechdraws
            ::where('id_dimensao', $id_dimensao)
            ->get()
            ->first();

        if (!$techdraw) {

            $variacoes = response()->json([]);
        } else {

            $variacoes = $techdraw->variacoes;
        }

        return $variacoes;
    }

    public function getPlantas($id_tipo)
    {
        $variacoes = TdVariacoes::select('td_variacoes.*')
            ->join('td_techdraws', 'td_techdraws.id', '=', 'td_variacoes.id_techdraw')
            ->join('dimensoes', 'dimensoes.id', '=', 'td_techdraws.id_dimensao')
            ->join('tipos_embalagens', 'tipos_embalagens.id', '=', 'dimensoes.id_tipo_embalagem')
            ->orderBy('tipos_embalagens.nome')
            ->orderBy('dimensoes.nome')
            ->orderBy('td_variacoes.nome')
            ->where('dimensoes.id_tipo_embalagem', $id_tipo)
            ->get();

        $plantas = [];

        foreach ($variacoes as $i => $planta) {

            $plantas[$i] = $planta->toArray();
            $plantas[$i]['nome_completo'] = $planta->nome_completo;
            $plantas[$i]['link_edicao'] = route('site.techdraw.technicalPlan.edit', [$planta->id]);

            $allItens = TdItens::select('td_itens.*', 'artw_item_tipos.nome')
                ->join('artw_item_tipos', 'artw_item_tipos.id', '=', 'td_itens.id_tipo')
                ->where('td_itens.id_variacao', $planta->id)
                ->orderBy('artw_item_tipos.nome')
                ->get();
            
            $itens = [];
            
            foreach ($allItens as $n => $item) {
                
                $ultimaVersao = $item->versoes->last();
                
                $itens[$n] = $item->toArray();
                $itens[$n]['versao'] = $ultimaVersao;
                //$itens[$n]['versao']['patch'] = $item->patch;
                $itens[$n]['versao']['path'] = ($ultimaVersao) ? route('site.techdraw.abrirPdf', $item->versoes->last()->id) : '';
                
            }
            
            $plantas[$i]['itens'] = $itens;
        }
        
        $itemTipo = \App\TiposEmbalagens::find($id_tipo);
        
        $retorno['nome_tipo'] = $itemTipo->nome;
        $retorno['plantas'] = $plantas;
        $retorno['item_tipos'] = ArtwItemTipos::orderBy('nome')->get();

        return $retorno;
    }

    public function criarItem(Request $request)
    {
        $tdItens = TdItens::firstOrNew([
                'id_tipo' => $request->input('id_tipo'),
                'id_variacao' => $request->input('id')
        ]);

        if (!$tdItens->exists) {

            $tdItens->save();

            return response()->json(['success' => 200, 'message' => 'Item adicionado com sucesso.'], 200);
        } else {

            return response()->json(['error' => 400, 'message' => 'Item já existente.'], 200);
        }
    }

    public function alterarItem(Request $request)
    {
        $tipo = ArtwItemTipos::find($request->input('id_tipo'));
        $item = TdItens::find($request->input('id'));

        $check = TdItens::where('id_variacao', $item->id_variacao)->where('id_tipo', $tipo->id)->where('status', 1)->get();

        if ($check->count() == 0) {

            $item->id_tipo = $tipo->id;
            $item->save();

            return response()->json(['success' => 200, 'message' => 'Item alterado com sucesso.'], 200);
        } else {

            return response()->json(['error' => 400, 'message' => 'Item já existente.'], 200);
        }
    }

    public function getArtes($id_item)
    {
        $item = TdItens::find($id_item);

        $versao = $item->versoes->last();

        $retorno['id_item'] = $item->id;
        $retorno['nome_completo'] = $item->nome_completo;

        if ($versao) {

            $v['nome'] = $versao->nome;
            $v['path'] = asset($versao->path);
            $v['nome_usuario'] = $versao->admUsuario->nome_abreviado;
            $v['data_criacao'] = $versao->created_at->format('H\hi \d\e d/m/Y');
            $v['filesize'] = $versao->filesize;

            $retorno['versao'] = $v;
        } else {

            $retorno['versao'] = [];
        }

        return $retorno;
    }

    public function postArtes(Request $request, Common $common)
    {
        $item = TdItens::find($request->input('id_item'));

        $pdf = $request->file('pdf');
        
        $nome_arquivo = $pdf->getClientOriginalName();
        //$extension = $pdf->getClientOriginalExtension();
        $relativePath = '/images/uploads/techdraw/versoes/';
        $destinationPath = public_path() . $relativePath;
        $filename = $nome_arquivo;
        $filename = $common->verificaNomeArquivo($relativePath, $filename);
        $pdf->move($destinationPath, $filename);
        
        $versao = new TdVersoes();
        $versao->nome = $filename;
        $versao->id_item = $item->id;
        $versao->numero = 1;
        $versao->path = $relativePath . $filename;
        $versao->id_usuario = Auth::user()->id;
        
        $versao->save();
        
        return response()->json(['success' => 200, 'message' => 'Upload efetuado com sucesso.'], 200);
    }
    
    public function removerItem(Request $request)
    {
        $item = TdItens::find($request->input('id'));
        
        $item->versoes()->delete();
        $item->delete();
        
        return response()->json(['success' => 200, 'message' => 'Item deletado com sucesso.'], 200);
    }
    
    public function removerPlanta(Request $request)
    {
        $planta = TdVariacoes::find($request->input('id'));
        
        $planta->delete();
        
        return response()->json(['success' => 200, 'message' => 'Item deletado com sucesso.'], 200);
    }
    
}
