<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Skus;

use App\Components\SkuArtesFinais;

use App\Http\Requests;

class RelacaoSkuController extends Controller
{
	private $finalart;

    public function __construct(SkuArtesFinais $finalart)
    {
        $this->finalart = $finalart;
    }

    public function getHome($attr, $id)
    {
    	$get = 'get'.ucfirst($attr);
    	return $this->$attr->$get($id);
    }

    public function create($attr, $skuId, Request $request)
    {
        return $this->$attr->create($request);
    }

    public function edit($attr, $id)
    {
        return $this->$attr->edit($id);
    }

    public function update($attr, $id, Request $request)
    {
        return $this->$attr->update($request);
    }

    public function remove($attr, $id)
    {
        return $this->$attr->remove($id);
    }

	public function removeSkuFile($attr, $type, $id)
    {
        return $this->$attr->removeSkuFile($type, $id);
    }
}
