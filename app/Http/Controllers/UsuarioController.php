<?php

namespace App\Http\Controllers;

use Mail;
use Validator;
use Illuminate\Http\Request;
use App\AdmUsuarios;
use App\AdmPerfis;
use App\AdmModulos;
use App\AdmEmpresas;
use App\Components\Common;

class UsuarioController extends Controller
{
	private $usuario;

    public function __construct(AdmUsuarios $usuario)
    {
    	$this->usuario = $usuario;
    }

    public function index()
    {
    	$allusers = AdmUsuarios::whereNotIn('id_adm_perfil', [1,2])->get();
    	$retornoPerfis = AdmPerfis::whereNotIn('id', [1,2])->get();
    	$retornoModulos = AdmModulos::all();
        $retornoEmpresas = AdmEmpresas::all();
        $users = null;

		$total = count($allusers);

    	foreach ($allusers as $user) {
    		$user->profile = $user->admPerfis;
            $user->empresa = $user->admEmpresas;
    		$users[] = $user;
    	}

    	foreach ($retornoPerfis as $value) {
    		$perfis[$value->id] = $value->nome;
    	}

    	foreach ($retornoModulos as $value) {
    		$modulos[$value->id] = $value->nome;
    	}

        foreach ($retornoEmpresas as $value) {
            $empresas[$value->id] = $value->nome;
        }

    	return view('admin.usuarios.index', compact('users', 'perfis', 'modulos', 'empresas', 'total'));
    }

    public function search($name)
    {
    	$allusers = $this->usuario->where('nome', 'like', '%'.$name.'%')->whereNotIn('id_adm_perfil', [1,2])->get();
    	$retornoPerfis = AdmPerfis::whereNotIn('id', [1,2])->get();
    	$retornoModulos = AdmModulos::all();
        $retornoEmpresas = AdmEmpresas::all();
        $users = null;

		$total = count($allusers);

        foreach ($allusers as $user) {
            $user->profile = $user->admPerfis;
            $user->empresa = $user->admEmpresas;
            $users[] = $user;
        }

        foreach ($retornoPerfis as $value) {
            $perfis[$value->id] = $value->nome;
        }

        foreach ($retornoModulos as $value) {
            $modulos[$value->id] = $value->nome;
        }

        foreach ($retornoEmpresas as $value) {
            $empresas[$value->id] = $value->nome;
        }

        return view('admin.usuarios.index', compact('users', 'perfis', 'modulos', 'empresas', 'total'));
    }

    public function create(Request $request)
    {
        $newPass = new Common;

        if(empty($request->nome) || empty($request->sobrenome) || empty($request->email) || empty($request->id_adm_perfil)){
            return false;
        }

        header('Content-type: application/json');

        $rules = array(
            'email' => 'unique:adm_usuarios,email'
        );

        $validator = Validator::make($request->only('email'), $rules);

        if ($validator->fails()) {
            echo json_encode([
                'error' => true,
                'message' => 'O email informado já está cadastrado no sistema.'
            ]);
            die;
        }

        $pass = $newPass->gerarSenha(8);

        $data = array(
            'nome' => htmlentities($request->nome),
            'email' => $request->email,
            'senha' => $pass
        );

        $request['senha'] = bcrypt($pass);

		if(!isset($request->token_guide))
			$request['token_guide'] = '0';

        $user = $this->usuario;

        if(!is_null($request->input('modulos'))) {
            if($user->create($request->all())->admModulos()->sync($request->input('modulos'))){
                Mail::send('emails.cadastro', $data, function ($message) use($data) {
                    //$message->from('ambev@paintpackred.com', 'Ambev Red');
                    $message->from(config('app.mail_from'), config('app.mail_name'));
                    $message->to($data['email'])->subject('Cadastro de Usuário');
                });
            }
        }
        else {
            if($user->create($request->all())){
                Mail::send('emails.cadastro', $data, function ($message) use($data) {
                    //$message->from('ambev@paintpackred.com', 'Ambev Red');
                    $message->from(config('app.mail_from'), config('app.mail_name'));
                    $message->to($data['email'])->subject('Cadastro de Usuário');
                });
            }
        }

        echo json_encode(['error' => false]);
        die;
    }

    public function edit($id)
    {
    	$editUser = $this->usuario->find($id);
    	$editUser->modules = $editUser->admModulos;

    	return response(compact('editUser'));
    }

	public function update($id, Request $request)
    {
        if(empty($request->nome) || empty($request->sobrenome) || empty($request->id_adm_perfil)){
            return false;
        }

		if(!isset($request->token_guide))
			$request['token_guide'] = '0';

    	$user = $this->usuario->find($id);
        if(isset($request->modulos))
    	   $user->admModulos()->sync($request->input('modulos'));
        else
            $user->admModulos()->detach();
    	$user->update($request->all());
    }

    public function remove($id)
    {
		$removeUser = new Common;
		$removeUser->removeUser($id);
    }

}
