<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tipografias;
use App\TiposTipografias;
use App\Components\Common;
use Illuminate\Support\Facades\File;

use App\Http\Requests;

class TipografiaController extends Controller
{
    private $tipografia;

    public function __construct(Tipografias $tipografia)
    {
    	$this->tipografia = $tipografia;
    }

    public function index()
    {
    	$allTipografias = Tipografias::orderBy('nome')->get();
    	$allTipos = TiposTipografias::all();
        $tipografias = null;

        $total = count($allTipografias);

    	foreach ($allTipografias as $tipografia) {
    		$tipografia->tipo = $tipografia->tiposTipografias;
    		$tipografias[] = $tipografia;
    	}

    	foreach ($allTipos as $value) {
    		$tipos[$value->id] = $value->nome;
    	}

    	return view('admin.tipografias.index', compact('tipografias', 'tipos', 'total'));
    }

    public function search($name)
    {
    	$allTipografias = $this->tipografia->where('nome', 'like', '%'.$name.'%')->orderBy('nome')->get();
        $allTipos = TiposTipografias::all();
        $tipografias = null;

        $total = count($allTipografias);

        foreach ($allTipografias as $tipografia) {
            $tipografia->tipo = $tipografia->tiposTipografias;
            $tipografias[] = $tipografia;
        }

    	foreach ($allTipos as $value) {
    		$tipos[$value->id] = $value->nome;
    	}

    	return view('admin.tipografias.index', compact('tipografias', 'tipos', 'total'));
    }

    public function create(Request $request)
    {
        if(empty($request->nome) || empty($request->id_tipo_tipografia)){
            return false;
        }

        $typography = $this->tipografia;
        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;
        $file = isset($dados['file']) ? $dados['file'] : null;
        $imagePath = '/images/uploads/thumbs/';
        $filePath = '/files/uploads/tipografias/';

        // Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $image];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

        // Validação de Upload de Arquivo
        $data = ['file' => $file];
        $rules = ['file' => 'mimes:zip'];
        $message = 'Formato de arquivo invalido. Utilize um arquivo ZIP';
        $validate->validaUpload($data, $rules, $message);

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;
        }

        if(!is_null($file)){
            $extension = $file->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $fileName = $hash.".".$extension;
            $destinationPath = storage_path().$filePath;
            $upload_success = $file->move($destinationPath, $fileName);
            $dados['path_down'] = $filePath.$fileName;
        }

        $typography->create($dados);
    }

    public function edit($id)
    {
        $editTypography = $this->tipografia->find($id);
        return response(compact('editTypography'));
    }

    public function update($id, Request $request)
    {
    	if(empty($request->nome) || empty($request->id_tipo_tipografia)){
            return false;
        }

        $typography = $this->tipografia->find($id);
        $dados = $request->all();

        $image = isset($dados['image']) ? $dados['image'] : null;
        $file = isset($dados['file']) ? $dados['file'] : null;
        $imagePath = '/images/uploads/thumbs/';
        $filePath = '/files/uploads/tipografias/';

        // Validação de Upload de Imagem
        $validate = new Common;
        $data = ['file' => $image];
        $rules = ['file' => 'mimes:jpeg,bmp,png'];
        $message = 'Formato de imagem invalida';
        $validate->validaUpload($data, $rules, $message);

        // Validação de Upload de Arquivo
        $data = ['file' => $file];
        $rules = ['file' => 'mimes:zip'];
        $message = 'Formato de arquivo invalido. Utilize um arquivo ZIP';
        $validate->validaUpload($data, $rules, $message);

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;

            if(File::isFile(public_path().$typography->thumb)){
                File::delete(public_path().$typography->thumb);
            }
        }

        if(!is_null($file)){
            $extension = $file->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $fileName = $hash.".".$extension;
            $destinationPath = storage_path().$filePath;
            $upload_success = $file->move($destinationPath, $fileName);
            $dados['path_down'] = $filePath.$fileName;

            if(File::isFile(storage_path().$typography->path_down)){
                File::delete(storage_path().$typography->path_down);
            }
        }

        $typography->update($dados);
    }

    public function remove($id)
    {
    	$removeTypography = $this->tipografia->find($id);

    	if(File::isFile(public_path().$removeTypography->thumb)){
            File::delete(public_path().$removeTypography->thumb);
        }

        if(File::isFile(storage_path().$removeTypography->path_down)){
            File::delete(storage_path().$removeTypography->path_down);
        }

        $removeTypography->produtos()->detach();
        $removeTypography->delete();
    }
}
