<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\ArtesFinais;
use App\ArtwVariacoes;
use App\CodigosEan;
use App\ApiPackshelf;
use App\ArtwVariacoesRelacionadas;
use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Support\Facades\DB;

class ExternalApiController extends Controller
{
    public function importarPlanilha()
    {
        $linhas = ApiPackshelf::all();

        //DB::beginTransaction();

        foreach ($linhas as $linha)
        {
            if ( ($linha->ean_secundaria && $linha->id_secundaria) || ($linha->ean_primaria && $linha->id_primaria) )
            {
                // Atualiza EAN da Secundária
                $variacao_secundaria = ArtwVariacoes::find($linha->id_secundaria);

                if ($variacao_secundaria)
                {
                    $variacao_secundaria->codigo_ean = $linha->ean_secundaria;
                    $variacao_secundaria->codigo_ean_alteracao = date('Y-m-d H/i/s');
                    $variacao_secundaria->tem_codigo_ean = 1;
                    $variacao_secundaria->save();

                    echo 'Atualiza EAN Secundário ' . $variacao_secundaria->id . ' - ' . $linha->ean_secundaria . '<br>';
                }

                // Atualiza EAN da Primária
                $variacao_primaria = ArtwVariacoes::find($linha->id_primaria);

                if ($variacao_primaria)
                {
                    $variacao_primaria->codigo_ean = $linha->ean_primaria;
                    $variacao_primaria->codigo_ean_alteracao = date('Y-m-d H/i/s');
                    $variacao_primaria->tem_codigo_ean = 1;
                    $variacao_primaria->save();

                    echo 'Atualiza EAN Primário ' . $variacao_primaria->id . ' - ' . $linha->ean_primaria . '<br>';
                }

                // Relaciona a secundária com primária
                if ($linha->id_secundaria && $linha->id_primaria)
                {
                    if ($variacao_secundaria && $variacao_primaria)
                    {
                        $relacionamento = ArtwVariacoesRelacionadas::firstOrNew([
            
                            'id_variacao' => $variacao_secundaria->id,
                            'id_variacao_relacionada' => $variacao_primaria->id,
                            
                        ]);

                        $relacionamento->save();
    
                        echo 'Relaciona a secundária com primária ' . $variacao_secundaria->id . ' -> ' . $variacao_primaria->id . '<br>';
                    }
                }

            }

        }

        //DB::commit();
    }

    public function importarEans()
    {
        $eans = ArtwVariacoes::select('codigos_ean.id_embalagem', 'codigos_ean.cod_ean AS codigo_ean_novo', 'artw_variacoes.codigo_ean AS codigo_ean_atual', 'artw_variacoes.id')
            ->join('codigos_ean', 'artw_variacoes.id', '=', 'codigos_ean.id_embalagem')
            ->get();
        
        foreach ($eans as $ean) {
            
            //dd($ean);
            
            if ( ($ean->codigo_ean_novo != $ean->codigo_ean_atual) && ($ean->id_embalagem == $ean->id) ){
                
                echo 'Atualiza ' . $ean->id_embalagem . ' - ' . $ean->id . '<br>';
                
                $ean->codigo_ean = $ean->codigo_ean_novo;
                $ean->codigo_ean_alteracao = date('Y-m-d H/i/s');
                $ean->save();
                
            }
            
        }
    }
    
    function getImages(Request $request, $ean = null) {
        
        $startDate = $request->get('startDate');
        
        $embalagens = ArtwVariacoes::select('artw_variacoes.id as id_embalagem', 'artw_variacoes.id_sku', 'artw_variacoes.id_natureza_embalagem', 'artw_variacoes.codigo_ean', 'artw_variacoes.codigo_ean_alteracao as atualizado_em', 'artw_variacoes.nome as variacao_nome', 'artw_variacoes.nome_original as variacao_nome_original')
            //->selectRaw('count(artes_finais.id) as qtdFotos')
            ->distinct()
            ->join('skus', 'artw_variacoes.id_sku', '=', 'skus.id')
            ->leftJoin('artes_finais', 'artes_finais.id_sku', '=', 'skus.id')
            ->leftJoin('artw_variacoes_relacionadas AS avr1', 'avr1.id_variacao', '=', 'artw_variacoes.id')
            ->leftJoin('artw_variacoes_relacionadas AS avr2', 'avr2.id_variacao_relacionada', '=', 'artw_variacoes.id')
            //->groupBy('skus.id')
            ->where('artw_variacoes.codigo_ean', '!=', null);
            //->where('artw_variacoes.id', 1309);
            //->where('artw_variacoes.id_natureza_embalagem', 3);
            
            if ($ean){
                
                $embalagens->where('artw_variacoes.codigo_ean', $ean);
                
            }
            
            if ($startDate) {

                $gmt = (!$request->get('tz')) ? 'GMT' : $request->get('tz');


                $tz = new DateTimeZone($gmt);

                $startDateObject = Carbon::createFromFormat('Y-m-d H:i:s', $startDate, $tz)
                    ->setTimezone('America/Sao_Paulo');
                
                $embalagens->where(function($query) use ($startDateObject){
                
                    $query->orWhere('artw_variacoes.codigo_ean_alteracao', '>=', $startDateObject->format('Y-m-d H:i:s'));
                    $query->orWhere('artw_variacoes.updated_at', '>=', $startDateObject->format('Y-m-d H:i:s'));
                    $query->orWhere('artes_finais.updated_at', '>=', $startDateObject->format('Y-m-d H:i:s'));

                    $query->orWhere('avr2.updated_at', '>=', $startDateObject->format('Y-m-d H:i:s'));
                    $query->orWhere('avr1.updated_at', '>=', $startDateObject->format('Y-m-d H:i:s'));

                });
                
            }
            
            $embalagens = $embalagens->get();
        
        $return = [];

        $i = 0;

        foreach($embalagens as $embalagem){

            $isMultipack = (strpos(strtolower($embalagem->variacao_nome_original), 'multipack') === false) ? false : true;
            $isPromokit = (strpos(strtolower($embalagem->variacao_nome_original), 'promokit') === false) ? false : true;

            //dd($isMultipack, $embalagem->variacao_nome, strpos(strtolower($embalagem->variacao_nome), 'multipack'));

            // Faz o tratamento das imagens
            $images = [];

            if ($embalagem->id_natureza_embalagem == 2)
            {
                $artes = ArtesFinais::where('id_sku', $embalagem->id_sku)->get();
            
                foreach ($artes as $n=>$arte) {
                    
                    $images[$n]['thumb'] = url($arte->thumb);
                    $images[$n]['cmyk'] = url($arte->path_cmyk);
                    $images[$n]['principal'] = $arte->arte_exibicao;
                    $images[$n]['status'] = $arte->status;
                    $images[$n]['atualizado_em'] = $arte->updated_at->format('Y-d-m H-i-s');
                    
                }
            }
            elseif ($embalagem->id_natureza_embalagem == 3 && !$isMultipack && !$isPromokit)
            {
                $primaria = ArtwVariacoesRelacionadas::where('id_variacao', $embalagem->id_embalagem)->get()->first();

                if ($primaria)
                {
                    $id_sku = ($primaria->variacao) ? $primaria->variacao->id_sku : null;

                    $artes = ArtesFinais::where('id_sku', $id_sku)->get();

                    foreach ($artes as $n=>$arte) {
                        
                        $images[$n]['thumb'] = url($arte->thumb);
                        $images[$n]['cmyk'] = url($arte->path_cmyk);
                        $images[$n]['principal'] = $arte->arte_exibicao;
                        $images[$n]['status'] = $arte->status;
                        $images[$n]['atualizado_em'] = $arte->updated_at->format('Y-d-m H-i-s');
                        
                    }
                }
            }
            elseif ($embalagem->id_natureza_embalagem == 3 && ($isMultipack || $isPromokit))
            {
                $artes = ArtesFinais::where('id_sku', $embalagem->id_sku)->get();

                //dd($artes);

                foreach ($artes as $n=>$arte) {
                    
                    $images[$n]['thumb'] = url($arte->thumb);
                    $images[$n]['cmyk'] = url($arte->path_cmyk);
                    $images[$n]['principal'] = $arte->arte_exibicao;
                    $images[$n]['status'] = $arte->status;
                    $images[$n]['atualizado_em'] = $arte->updated_at->format('Y-d-m H-i-s');
                    
                }
            }

            if (count($images))
            {
                $return[$i] = $embalagem->toArray();
                
                $return[$i]['original'] = ($embalagem->variacao_nome == 'Original') ? 1 : 0;
                $return[$i]['variacao_nome'] = ($embalagem->variacao_nome == 'Original') ? '' : $embalagem->variacao_nome;

                $return[$i]['qtdFotos'] = count($images);
                
                $return[$i]['images'] = $images;

                $i++;
            }
            
        }
        
        
        return $return;
    }
    
    
}
