<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Skus;
use App\ArtwVariacoes;
use App\ArtwItens;
use App\Produtos;
use App\Dimensoes;
use App\Campanhas;
use App\ArtesFinais;
use App\Components\Common;
use Illuminate\Support\Facades\File;
use App\Http\Requests;
use App\NaturezaEmbalagens;
use Illuminate\Support\Facades\DB;

class SkuController extends Controller
{

    private $sku;

    public function __construct(Skus $sku)
    {
        $this->sku = $sku;
    }

    public function getFinalartAction($id, $action)
    {
        $arteFinal = ArtesFinais::find($id);
        
        if ($action == 'principal') {
            
            ArtesFinais::where('id_sku', $arteFinal->id_sku)->update(['arte_exibicao' => 0]);
            
            $arteFinal->arte_exibicao = 1;
            $arteFinal->save();
            
        } else {
            
            $arteFinal->status = ($arteFinal->status) ? 0 : 1;
            
            /*
            if ($arteFinal->status == 0) {
                
                $arteFinal->arte_exibicao = 0;
                
            }
             * 
             */
            
            $arteFinal->save();
            
        }
        
        return back();
    }
    
    public function index()
    {
        // Dimensões de ID 1 são as promocionais e não devem ser listadas
        $paginate = new Common;
        $allSkus = $this->sku->orderBy('nome')->get();
        $allProdutos = Produtos::orderBy('nome')->get();
        $allNatureza = NaturezaEmbalagens::where('id', '<>' , '1')->get();
        
        $allDimensoes = Dimensoes
            ::select('dimensoes.*')
            ->where('id', '<>', '1')
            ->where('status', 1)
            ->join('tipos_embalagens', 'dimensoes.id_tipo_embalagem', 'tipos_embalagens.id')
            ->orderBy('nome')
            ->get();
        
        
        $skus = array();
        $produtos = array();
        $dimensoes = array();

        $total = count($skus);

        foreach ($allSkus as $sku) {
            $sku->nome = $sku->produtos->nome . ' - ' . $sku->dimensoes->tiposEmbalagens->nome . ' - ' . $sku->dimensoes->nome;
            $skus[] = $sku;
        }

        foreach ($allProdutos as $value) {
            $produtos[$value->id] = $value->nome;
        }

        foreach ($allDimensoes as $value) {
            $dimensoes[$value->id] = $value->tiposEmbalagens->nome . ' - ' . $value->nome;
        }

        foreach ($allNatureza as $value) {
    		$natureza[$value->id] = $value->nome;
    	}

        asort($dimensoes);
        $skus = $paginate->paginate($skus);

        return view('admin.skus.index', compact('skus', 'produtos', 'dimensoes', 'total', 'natureza'));
    }

    public function indexV2()
    {
        $todasVariacoes = ArtwVariacoes
            ::select(
                'artw_variacoes.*',
                'artw_variacao_tipos.nome as tipo',
                'produtos.nome as produto_nome',
                'tipos_embalagens.nome as tipo_embalagem',
                'dimensoes.nome as dimensao_nome',
                'campanhas.nome as campanha'
                )
            ->leftJoin('skus', 'artw_variacoes.id_sku', '=', 'skus.id')
            ->join('produtos', 'produtos.id', '=', 'skus.id_produto')
            ->join('dimensoes', 'dimensoes.id', '=', 'skus.id_dimensao')
            ->join('tipos_embalagens', 'tipos_embalagens.id', '=', 'dimensoes.id_tipo_embalagem')
            ->leftJoin('artw_variacao_tipos', 'artw_variacao_tipos.id', '=', 'artw_variacoes.id_tipo')
            ->leftJoin('campanhas', 'campanhas.id', '=', 'artw_variacoes.id_campanha')
            ->where('produtos.status', 1)
            ->orderBy('skus.nome')
            ->orderBy('artw_variacoes.id_campanha')
            ->orderBy('artw_variacoes.principal', 'DESC')
            ->orderBy('artw_variacoes.nome')
            ->get();

        $variacoes = [];

        foreach ($todasVariacoes as $n => $variacao) {

            $variacoes[$n] = $variacao;

            $nome = '';
            $nome = $variacao->produto_nome
            . ' ' .
            $variacao->tipo_embalagem
            . ' ' .
            $variacao->dimensao_nome;
            if ($variacao->campanha) {
                $nome .= ' - ' . $variacao->campanha;
            }
            if ($variacao->principal !== 1 && $variacao->nome) {
                $nome .= ' - ' . $variacao->nome;
            }

            $variacoes[$n]->nome_completo = $nome;
        }

        $total = count($variacoes);

        return view('admin.skus.index_v2', compact('variacoes', 'produtos', 'dimensoes', 'total'));
    }

    public function fotos($all = null)
    {

        if (!$all)
        {
            $todosProdutos = Skus
                ::select(
                    'skus.*',
                    'produtos.nome as produto_nome',
                    'tipos_embalagens.nome as tipo_embalagem',
                    'dimensoes.nome as dimensao_nome'
                    )
                ->distinct()
                ->join('produtos', 'produtos.id', '=', 'skus.id_produto')
                ->join('artw_variacoes', 'artw_variacoes.id_sku', '=', 'skus.id')
                ->join('dimensoes', 'dimensoes.id', '=', 'skus.id_dimensao')
                ->join('tipos_embalagens', 'tipos_embalagens.id', '=', 'dimensoes.id_tipo_embalagem')
                ->where('produtos.status', 1)
                ->orderBy('skus.nome')
                ->get();
        }
        else
        {
            $todosProdutos = Skus
                ::select(
                    'skus.*',
                    'produtos.nome as produto_nome',
                    'tipos_embalagens.nome as tipo_embalagem',
                    'dimensoes.nome as dimensao_nome'
                    )
                ->distinct()
                ->join('produtos', 'produtos.id', '=', 'skus.id_produto')                
                ->join('dimensoes', 'dimensoes.id', '=', 'skus.id_dimensao')
                ->join('tipos_embalagens', 'tipos_embalagens.id', '=', 'dimensoes.id_tipo_embalagem')
                ->where('produtos.status', 1)
                ->orderBy('skus.nome')
                ->get();
        }

        

        $produtos = [];

        foreach ($todosProdutos as $n => $produto) {

            $produtos[$n] = $produto;

            $nome = '';
            $nome = $produto->produto_nome
            . ' ' .
            $produto->tipo_embalagem
            . ' ' .
            $produto->dimensao_nome;
            
            $produtos[$n]->nome_completo = $nome;
        }

        $total = count($produtos);

        return view('admin.skus.fotos', compact('produtos', 'total'));
    }

    public function search($name)
    {
        //dd($name);

        $paginate = new Common;

        $variacoes = ArtwVariacoes
            ::select('artw_variacoes.*')
            ->join('skus', 'artw_variacoes.id_sku', '=', 'skus.id')
            ->orwhere('artw_variacoes.nome', 'like', '%' . $name . '%')
            ->orwhere('artw_variacoes.nome_original', 'like', '%' . $name . '%')
            ->orwhere('skus.nome', 'like', '%' . $name . '%')
            ->orderBy('skus.nome')
            ->orderBy('artw_variacoes.id_campanha')
            ->orderBy('artw_variacoes.principal', 'DESC')
            ->orderBy('artw_variacoes.nome')
            ->get();

        $produtos = Produtos
            ::orderBy('nome')
            ->get()
            ->pluck('nome', 'id');

        $dimensoes = Dimensoes
            ::where('id', '<>', '1')
            ->where('status', 1)
            ->orderBy('nome')
            ->get()
            ->pluck('nome', 'id');

        $total = count($variacoes);

        $variacoes = $paginate->paginate($variacoes);

        return view('admin.skus.index_v2', compact('variacoes', 'produtos', 'dimensoes', 'total'));
    }

    public function byId($id)
    {
        $paginate = new Common;

        $variacoes = ArtwVariacoes
            ::select('artw_variacoes.*')
            ->join('skus', 'artw_variacoes.id_sku', '=', 'skus.id')
            ->where('artw_variacoes.id', $id)
            ->orderBy('skus.nome')
            ->orderBy('artw_variacoes.id_campanha')
            ->orderBy('artw_variacoes.principal', 'DESC')
            ->orderBy('artw_variacoes.nome')
            ->get();

        $produtos = Produtos
            ::orderBy('nome')
            ->get()
            ->pluck('nome', 'id');

        $dimensoes = Dimensoes
            ::where('id', '<>', '1')
            ->where('status', 1)
            ->orderBy('nome')
            ->get()
            ->pluck('nome', 'id');

        $total = count($variacoes);

        $variacoes = $paginate->paginate($variacoes);

        return view('admin.skus.index_v2', compact('variacoes', 'produtos', 'dimensoes', 'total'));
    }

    public function create(Requests\CreateSku $request)
    {
        //dd($request->all());

        DB::beginTransaction();

        $produto = Produtos::find($request->input('id_produto'));
        $dimensao = Dimensoes::find($request->input('id_dimensao'));
        $embalagem = $dimensao->tiposEmbalagens;

        $sku = Skus::firstOrNew(['id_produto' => $request->input('id_produto'), 'id_dimensao' => $request->input('id_dimensao')]);

        if (!$sku->exists) {
            $sku->nome = $produto->nome . ' - ' . $embalagem->nome . ' - ' . $dimensao->nome;
            //$sku->status = $request->input('status');
            $sku->novo = 0;
            $sku->promocional = $request->input('promocional');
            $sku->save();
        }

        if ($request->input('promocional') && $request->input('id_campanha') == 0) {
            $campanha = Campanhas::firstOrNew(['nome' => $request->input('campanha')]);

            if (!$campanha->exists) {
                $campanha->save();
            }
        } elseif ($request->input('id_campanha')) {
            $campanha = Campanhas::find($request->input('id_campanha'));
        }

        $id_campanha = (isset($campanha) && $campanha) ? $campanha->id : null;

        if ($request->input('possuiVariacao')) {
            $variacao_nome = $request->input('variacao');
            $principal = 0;
        } else {
            $variacao_nome = 'Original';
            $principal = 1;
        }

        $variacao = ArtwVariacoes::firstOrNew(['id_sku' => $sku->id, 'nome' => $variacao_nome, 'id_campanha' => $id_campanha]);

        if (!$variacao->exists) {
            $variacao->id_tipo = ($request->input('promocional')) ? 2 : 1;
            $variacao->id_natureza_embalagem = $request->input('id_natureza');
            $variacao->principal = $principal;
            $variacao->novo = 0;
            $variacao->dt_expiracao = $request->input('dt_expiracao');
            $variacao->nome_original = $variacao->sku_nome_completo;
            $variacao->tem_codigo_ean = ($request->input('eanCode') == '1') ? 0 : 1;
            $variacao->codigo_ean = ($request->input('txtEANCode') && $request->input('eanCode') == 3) ? $request->input('txtEANCode') : null;

            if ($request->input('txtEANCode'))
            {
                $variacao->codigo_ean_alteracao = date('Y-m-d H:i:s');
            }

            $variacao->save();
        } else {
            return ['error' => true, 'message' => 'Essa variação já está cadastrada no sistema.'];
        }

        $embalagens_selecionadas = [];

        foreach ((array) $request->input('embalagens_selecionadas') as $selecionada)
        {
            if ($variacao->id != $selecionada)
            {
                $embalagens_selecionadas[] = $selecionada;
            }
        }

        $variacao->variacoesRelacionadas()->sync($embalagens_selecionadas);

        DB::commit();

        //$item = ArtwItens::firstOrNew(['id_variacao' => $variacao->id]);
        //if (!$item->exists) {
        //$item->save();
        //}

        return ['message' => 'Dados salvos com sucesso!'];
    }

    public function edit($id)
    {
        $editVariacao = ArtwVariacoes::find($id);

        $editSku = Skus::withTrashed()->find($editVariacao->id_sku);

        $editSku->id_produto = (string) $editSku->id_produto;
        $editSku->id_dimensao = (string) $editSku->id_dimensao;
        $editSku->id_variacao = (string) $editVariacao->id;
        //$editSku->status = $editVariacao->status;
        $editSku->ordem = $editVariacao->ordem;
        $editSku->promocional = ($editVariacao->id_campanha) ? '1' : '0';
        $editSku->sku_nome_completo = $editVariacao->sku_nome_completo;
        $editSku->id_campanha = $editVariacao->id_campanha;
        $editSku->id_natureza_embalagem = $editVariacao->id_natureza_embalagem;
        $editSku->possuiVariacao = ($editVariacao->principal == 0) ? '1' : '0';
        $editSku->variacao = $editVariacao->nome;

        $embalagens_selecionadas = [];

        foreach ($editVariacao->relacionadas as $relacionada)
        {
            $embalagens_selecionadas[] = [
                'id' => $relacionada->variacao->id,
                'nome' => $relacionada->variacao->sku_nome_completo,
            ];
        }

        $editSku->embalagens_selecionadas = $embalagens_selecionadas;

        if ($editVariacao->tem_codigo_ean === 0) {

            $eanCode = '1';

        } elseif ($editVariacao->tem_codigo_ean === 1 && !$editVariacao->codigo_ean) {

            $eanCode = '2';

        } elseif ($editVariacao->tem_codigo_ean === 1 && $editVariacao->codigo_ean) {
            
            $eanCode = '3';

        } else {

            $eanCode = '';

        }

        $editSku->eanCode = $eanCode;

        $editSku->txtEANCode = $editVariacao->codigo_ean;
        $editSku->id_natureza_embalagem = $editVariacao->id_natureza_embalagem;

        $editSku->dt_expiracao_ = ($editVariacao->dt_expiracao) ? $editVariacao->dt_expiracao->format('d/m/Y') : null;
        //$editSku->artes_finais = $editVariacao->artesFinais;
        $count = null;

        $skuPorEmbalagem = Skus
            ::join('dimensoes', 'skus.id_dimensao', '=', 'dimensoes.id')
            ->join('tipos_embalagens', 'dimensoes.id_tipo_embalagem', '=', 'tipos_embalagens.id')
            ->join('artw_variacoes', 'artw_variacoes.id_sku', '=', 'skus.id')
            ->where('dimensoes.id_tipo_embalagem', '=', $editSku->dimensoes->id_tipo_embalagem)
            ->where('skus.id_produto', '=', $editSku->id_produto)
            ->get();

        foreach (range(1, count($skuPorEmbalagem)) as $number) {
            $count[] = $number;
        }

        return response(compact('editSku', 'count'));
    }

    public function produtos()
    {
        $produtos = Produtos
            ::orderBy('nome')
            ->where('produtos.status', 1)
            ->get();

        return $produtos;
    }

    public function naturezas()
    {
        $naturezas = NaturezaEmbalagens::all();

        return $naturezas;
    }

    public function embalagens()
    {
        $dimensoes = Dimensoes
            ::select('dimensoes.*', 'tipos_embalagens.nome as tipo')
            //->select('tipos_embalagens.nome as tipo')
            ->join('tipos_embalagens', 'tipos_embalagens.id', '=', 'dimensoes.id_tipo_embalagem')
            ->where('dimensoes.id', '<>', '1')
            ->where('dimensoes.status', 1)
            ->orderBy('tipos_embalagens.nome')
            ->orderBy('dimensoes.nome')
            ->get();

        return $dimensoes;
    }

    public function campanhas($id_produto)
    {
        $retorno[] = [
            'id' => 0,
            'nome' => 'Cadastrar nova campanha...'
        ];
        
        $campanhas = Campanhas
            ::select('campanhas.*')
            ->distinct()
            ->join('artw_variacoes', 'artw_variacoes.id_campanha', '=', 'campanhas.id')
            ->join('skus', 'skus.id', '=', 'artw_variacoes.id_sku')
            ->where('skus.id_produto', $id_produto)
            ->orderBy('campanhas.nome')
            ->get();
        
        foreach ($campanhas as $campanha)
        {
            $retorno[] = [
                'id' => $campanha->id,
                'nome' => $campanha->nome
            ];
        }
        
        return $retorno;
    }

    public function update($id_variacao, Requests\CreateSku $request)
    {
        //dd($request->all());
        //echo 1;
        //exit;

        $produto = Produtos::find($request->input('id_produto'));
        $dimensao = Dimensoes::find($request->input('id_dimensao'));
        $embalagem = $dimensao->tiposEmbalagens;

        $sku = Skus::firstOrNew(['id_produto' => $request->input('id_produto'), 'id_dimensao' => $request->input('id_dimensao')]);

        if (!$sku->exists) {
            $sku->nome = $produto->nome . ' - ' . $embalagem->nome . ' - ' . $dimensao->nome;
            //$sku->status = $request->input('status');
            $sku->novo = 0;
            $sku->promocional = $request->input('promocional');
            $sku->save();
        }

        if ($request->input('promocional') && $request->input('id_campanha') == 0) {
            $campanha = Campanhas::firstOrNew(['nome' => $request->input('campanha')]);

            if (!$campanha->exists) {
                $campanha->save();
            }
        } elseif ($request->input('id_campanha')) {
            $campanha = Campanhas::find($request->input('id_campanha'));
        }

        $id_campanha = (isset($campanha) && $campanha) ? $campanha->id : null;

        if (trim($request->input('variacao')) && $request->input('possuiVariacao')) {
            $variacao_nome = $request->input('variacao');
            $principal = 0;
        } else {
            $variacao_nome = 'Original';
            $principal = 1;
        }

        $variacao = ArtwVariacoes::find($request->input('id'));

        $check = ArtwVariacoes
            ::where('id_sku', $sku->id)
            ->where('nome', $variacao_nome)
            ->where('id_campanha', $id_campanha)
            ->where('id', '!=', $variacao->id)
            ->get()
            ->first();

        if (!$check) {
            $variacao->id_sku = $sku->id;
            $variacao->nome = $variacao_nome;
            $variacao->id_campanha = $id_campanha;
            $variacao->id_tipo = ($request->input('promocional')) ? 2 : 1;
            $variacao->id_natureza_embalagem = $request->input('id_natureza');
            $variacao->principal = $principal;
            $variacao->novo = 0;
            $variacao->dt_expiracao = $request->input('dt_expiracao');
            $variacao->ordem = $request->input('ordem');
            //$variacao->status = $request->input('status');
            $variacao->nome_original = $variacao->sku_nome_completo;
            $variacao->tem_codigo_ean = ($request->input('eanCode') == '1') ? 0 : 1;

            $codigo_ean_antigo = $variacao->codigo_ean;

            $variacao->codigo_ean = ($request->input('txtEANCode') && $request->input('eanCode') == 3) ? $request->input('txtEANCode') : null;

            if ($codigo_ean_antigo != $variacao->codigo_ean)
            {
                $variacao->codigo_ean_alteracao = date('Y-m-d H:i:s');
            }

            $variacao->save();

            $embalagens_selecionadas = [];

            foreach ((array) $request->input('embalagens_selecionadas') as $selecionada)
            {
                if ($variacao->id != $selecionada)
                {
                    $embalagens_selecionadas[] = $selecionada;
                }
            }

            $variacao->variacoesRelacionadas()->sync($embalagens_selecionadas);

        } else {
            return ['error' => true, 'message' => 'Essa variação já está cadastrada no sistema.'];
        }

        /*
        $item = ArtwItens::firstOrNew(['id_variacao' => $variacao->id]);

        if (!$item->exists) {
            $item->save();
        }
         * 
         */

        return ['message' => 'Dados salvos com sucesso!'];
    }

    public function remove($id)
    {
        $variacao = ArtwVariacoes::find($id);

        $variacao->delete();

        return ['message' => 'Variação excluída com sucesso!'];
    }
}
