<?php
// Rotas para consumo da API
Route::get('/api/render/{tipo}/{id}', array('as' => 'api.render', 'uses' => 'ApiController@render'));
Route::get('/api/reRender/{tipo}/{id}', array('as' => 'api.reRender', 'uses' => 'ApiController@reRender'));
Route::get('/api/statusRender', array('as' => 'api.statusRender', 'uses' => 'ApiController@statusRender'));

Route::get('/api/importarPlanilha', array('as' => 'api.importarEans', 'uses' => 'ExternalApiController@importarPlanilha'));
Route::get('/api/importarEans', array('as' => 'api.importarEans', 'uses' => 'ExternalApiController@importarEans'));

Route::get('/api/images', array('as' => 'api.eans', 'uses' => 'ExternalApiController@getImages'));
Route::get('/api/images/{ean}', array('as' => 'api.eans', 'uses' => 'ExternalApiController@getImages'));

Route::get('/atualizaArquivos', array('as' => 'api.atualizaArtesFinais', 'uses' => 'ArtworkController@atualizaArquivos'));

// Rota de criação e edição de usuário pelo Admin Geral
Route::post('externo/novo/{token}', array('as' => 'admin.externo.novo', 'uses' => 'ServicesController@newUserExterno'))->middleware('externo');
Route::post('externo/edit/{token}', array('as' => 'admin.externo.edit', 'uses' => 'ServicesController@editUserExterno'))->middleware('externo');
Route::post('externo/remove/{token}', array('as' => 'admin.externo.remove', 'uses' => 'ServicesController@removeUserExterno'))->middleware('externo');

Route::get('/download', [
    'as' => 'download',
    'uses' => 'ArtworkController@downloadArteFinal',
    'middleware' => 'tokenArteDownload'
]);

Route::get('/download/arte', [
    'as' => 'downloadArte',
    'uses' => 'ArtworkController@downloadArte',
    'middleware' => 'tokenArteDownload'
]);

Route::get('/download/pdf', [
    'as' => 'downloadPdf',
    'uses' => 'ArtworkController@downloadPdf',
    'middleware' => 'tokenArteDownload'
]);

Route::get('/download/abrirPdfFornecedor/{id_arquivo}', [
    'as' => 'downloadAbrirPdfFornecedor',
    'uses' => 'ArtworkController@abrirFornecedorPdf',
    'middleware' => 'tokenArteDownload'
]);

Route::get('/download/getFornecedorPdf', [
    'as' => 'getFornecedorPdf',
    'uses' => 'ArtworkServiceController@getFornecedorPdfToken',
    'middleware' => 'tokenArteDownload'
]);

Route::post('/download/postEnviarFornecedorPdfCliente', [
    'as' => 'postEnviarFornecedorPdfCliente',
    'uses' => 'ArtworkServiceController@postEnviarFornecedorPdfCliente',
    'middleware' => 'tokenArteDownload'
]);

Route::post('/download/postExcluirFornecedorPdfCliente', [
    'as' => 'postExcluirFornecedorPdfCliente',
    'uses' => 'ArtworkServiceController@postExcluirFornecedorPdfCliente',
    'middleware' => 'tokenArteDownload'
]);

Route::post('/download/postUploadFornecedorPdf', [
    'as' => 'postUploadFornecedorPdf',
    'uses' => 'ArtworkServiceController@postUploadFornecedorPdf',
    'middleware' => 'tokenArteDownload'
]);

Route::group(['middleware' => 'web'], function () {
    Route::get('/', array('as' => 'site.home', 'uses' => 'LoginController@index'));

    Route::get('/login/gerarToken/{tipo}/{id_usuario}', array('as' => 'site.login.gerarToken', 'uses' => 'LoginController@gerarToken'));

    Route::get('/login', array('as' => 'site.login', 'uses' => 'LoginController@index'));
    Route::post('/login', array('as' => 'site.login', 'uses' => 'LoginController@postLogin'));

    Route::get('/login/sso', array('as' => 'site.login.sso.get', 'uses' => 'LoginController@getLoginSso'));
    Route::post('/login/sso', array('as' => 'site.login.sso.post', 'uses' => 'LoginController@postLoginSso'));

    Route::get('/login/token/{token}', array('as' => 'site.login.token.get', 'uses' => 'LoginController@getLoginToken'));

    Route::get('/forgot', array('as' => 'site.get.forgot', 'uses' => 'LoginController@getForgot'));
    Route::post('/forgot', array('as' => 'site.post.forgot', 'uses' => 'LoginController@postForgot'));

    Route::get('/logout', array('as' => 'site.get.logout', 'uses' => 'LoginController@getLogout'));

    // Rota de acesso pelo Admin Geral
    Route::get('externo/{token}', array('as' => 'admin.externo', 'uses' => 'LoginController@getAdminExterno'))->middleware('externo');

    // Rotas de Downloads Files SKU
    Route::get('files/uploads/view/{page}/{type}/{file}', [
        'as' => 'site.download.skuFile',
        'uses' => 'DownloadController@skuDownload',
        'middleware' => 'download',
    ]);

    Route::get('files/uploads/{page}/{type}/{file}', [
        'as' => 'site.download.sku',
        'uses' => 'DownloadController@skuDownloadHtml',
        'middleware' => 'download',
    ]);

    // Rotas de Downloads Guides e Enxovais
    Route::get('files/uploads/{page}/{file}', [
        'as' => 'site.download.guide',
        'uses' => 'DownloadController@guideDownload',
        'middleware' => 'download',
    ]);

    // Rotas de Usuários Autenticados
    Route::group(['middleware' => ['auth', 'token']], function () {

        // Rota de Primeiro Acesso
        Route::group(['middleware' => 'first'], function () {
            Route::get('first', array('as' => 'site.first', 'uses' => 'LoginController@getFirst'));
            Route::post('first', array('as' => 'site.first', 'uses' => 'LoginController@postFirst'));
        });

        // Rotas de BackEnd
        Route::group(['middleware' => 'pass'], function () {

            Route::get('/site/dashboard/services/getDashboard/{mes?}/{ano?}', array('as' => 'site.dashboard.services.getDashboard', 'uses' => 'DashboardServiceController@getDashboard'));
            Route::get('/site/dashboard/services/atualizaDashboard', array('as' => 'site.dashboard.services.atualizaDashboard', 'uses' => 'DashboardServiceController@atualizaDashboard'));
            Route::post('/site/dashboard/services/getRelatorio', array('as' => 'site.dashboard.services.getRelatorio', 'uses' => 'DashboardServiceController@getRelatorio'));
            Route::get('/site/dashboard/services/downloadRelatorio/{relatorio}', array('as' => 'site.dashboard.services.downloadRelatorio', 'uses' => 'DashboardServiceController@downloadRelatorio'));
            Route::get('/site/dashboard/services/downloadRelatorioAdmin/{relatorio}', array('as' => 'site.dashboard.services.downloadRelatorioAdmin', 'uses' => 'DashboardServiceController@downloadRelatorioAdmin'));

            Route::get('/site/guide', array('as' => 'site.guidenew', 'uses' => 'HomeController@guideNew'));
            Route::get('/site/guide/brahma', array('as' => 'site.guidenew.brahma', 'uses' => 'HomeController@guideNewBrahma'));

            Route::get('admin/sku/produtos', array('as' => 'admin.sku.produtos', 'uses' => 'SkuController@produtos'));
            Route::get('admin/sku/naturezas', array('as' => 'admin.sku.naturezas', 'uses' => 'SkuController@naturezas'));
            Route::get('admin/service/sku/embalagens', array('as' => 'admin.sku.embalagens', 'uses' => 'SkuController@embalagens'));
            Route::get('admin/sku/campanhas/{id_produto}', array('as' => 'admin.sku.campanhas', 'uses' => 'SkuController@campanhas'));
            
            Route::group(['prefix' => 'admin'], function () {

                Route::get('sku/search/{name}', array('as' => 'admin.sku.search', 'uses' => 'SkuController@search'));
                Route::get('sku/search/id/{id}', array('as' => 'admin.sku.id', 'uses' => 'SkuController@byId'));
                Route::post('sku/create', array('as' => 'admin.sku.create', 'uses' => 'SkuController@create'));
                Route::get('sku/edit/{id}', array('as' => 'admin.sku.edit', 'uses' => 'SkuController@edit'));
                Route::post('sku/update/{id}', array('as' => 'admin.sku.update', 'uses' => 'SkuController@update'));
                Route::get('sku/remove/{id}', array('as' => 'admin.sku.remove', 'uses' => 'SkuController@remove'));

                Route::get('sku/{attr}/edit/{id}', array('as' => 'admin.sku.relations.edit', 'uses' => 'RelacaoSkuController@edit'));
                Route::post('sku/{attr}/{skuId}/create', array('as' => 'admin.sku.relations.create', 'uses' => 'RelacaoSkuController@create'));
                Route::post('sku/{attr}/update/{id}', array('as' => 'admin.sku.relations.update', 'uses' => 'RelacaoSkuController@update'));
                Route::get('sku/{attr}/remove/{id}', array('as' => 'admin.sku.relations.remove', 'uses' => 'RelacaoSkuController@remove'));
                Route::get('sku/{attr}/remove-sku-file/{type}/{id}', array('as' => 'admin.sku.relations.remove.sku.file', 'uses' => 'RelacaoSkuController@removeSkuFile'));
            
            });    
            
            Route::group(['middleware' => 'admin'], function () {
                Route::group(['prefix' => 'admin'], function () {
                    Route::get('user', array('as' => 'admin.user', 'uses' => 'UsuarioController@index'));
                    Route::get('user/search/{name}', array('as' => 'admin.user.search', 'uses' => 'UsuarioController@search'));
                    Route::post('user/create', array('as' => 'admin.user.create', 'uses' => 'UsuarioController@create'));
                    Route::get('user/edit/{id}', array('as' => 'admin.user.edit', 'uses' => 'UsuarioController@edit'));
                    Route::post('user/update/{id}', array('as' => 'admin.user.update', 'uses' => 'UsuarioController@update'));
                    Route::get('user/remove/{id}', array('as' => 'admin.user.remove', 'uses' => 'UsuarioController@remove'));

                    Route::get('promotions', array('as' => 'admin.promotions', 'uses' => 'PromocaoController@index'));
                    Route::get('promotions/search/{name}', array('as' => 'admin.promotions.search', 'uses' => 'PromocaoController@search'));
                    Route::post('promotions/create', array('as' => 'admin.promotions.create', 'uses' => 'PromocaoController@create'));
                    Route::get('promotions/edit/{id}', array('as' => 'admin.promotions.edit', 'uses' => 'PromocaoController@edit'));
                    Route::post('promotions/update/{id}', array('as' => 'admin.promotions.update', 'uses' => 'PromocaoController@update'));
                    Route::get('promotions/remove/{id}', array('as' => 'admin.promotions.remove', 'uses' => 'PromocaoController@remove'));

                    Route::get('company', array('as' => 'admin.company', 'uses' => 'EmpresaController@index'));
                    Route::get('company/search/{name}', array('as' => 'admin.company.search', 'uses' => 'EmpresaController@search'));
                    Route::post('company/create', array('as' => 'admin.company.create', 'uses' => 'EmpresaController@create'));
                    Route::get('company/edit/{id}', array('as' => 'admin.company.edit', 'uses' => 'EmpresaController@edit'));
                    Route::post('company/update/{id}', array('as' => 'admin.company.update', 'uses' => 'EmpresaController@update'));
                    Route::get('company/remove/{id}', array('as' => 'admin.company.remove', 'uses' => 'EmpresaController@remove'));

                    Route::get('icon-category', array('as' => 'admin.icon-category', 'uses' => 'CategoriaIconeController@index'));
                    Route::get('icon-category/search/{name}', array('as' => 'admin.icon-category.search', 'uses' => 'CategoriaIconeController@search'));
                    Route::post('icon-category/create', array('as' => 'admin.icon-category.create', 'uses' => 'CategoriaIconeController@create'));
                    Route::get('icon-category/edit/{id}', array('as' => 'admin.icon-category.edit', 'uses' => 'CategoriaIconeController@edit'));
                    Route::post('icon-category/update/{id}', array('as' => 'admin.icon-category.update', 'uses' => 'CategoriaIconeController@update'));
                    Route::get('icon-category/remove/{id}', array('as' => 'admin.icon-category.remove', 'uses' => 'CategoriaIconeController@remove'));

                    Route::get('typography', array('as' => 'admin.typography', 'uses' => 'TipografiaController@index'));
                    Route::get('typography/search/{name}', array('as' => 'admin.typography.search', 'uses' => 'TipografiaController@search'));
                    Route::post('typography/create', array('as' => 'admin.typography.create', 'uses' => 'TipografiaController@create'));
                    Route::get('typography/edit/{id}', array('as' => 'admin.typography.edit', 'uses' => 'TipografiaController@edit'));
                    Route::post('typography/update/{id}', array('as' => 'admin.typography.update', 'uses' => 'TipografiaController@update'));
                    Route::get('typography/remove/{id}', array('as' => 'admin.typography.remove', 'uses' => 'TipografiaController@remove'));

                    //Route::get('sku', array('as' => 'admin.sku', 'uses' => 'SkuController@index'));

                    Route::get('ftp', array('as' => 'admin.ftp', 'uses' => 'FtpController@index'));
                    Route::get('ftp/search/{name}', array('as' => 'admin.ftp.search', 'uses' => 'FtpController@search'));
                    Route::post('ftp/create', array('as' => 'admin.ftp.create', 'uses' => 'FtpController@create'));
                    Route::get('ftp/edit/{id}', array('as' => 'admin.ftp.edit', 'uses' => 'FtpController@edit'));
                    Route::post('ftp/update/{id}', array('as' => 'admin.ftp.update', 'uses' => 'FtpController@update'));
                    Route::get('ftp/remove/{id}', array('as' => 'admin.ftp.remove', 'uses' => 'FtpController@remove'));

                    Route::get('token', array('as' => 'admin.token', 'uses' => 'TokenGuideController@index'));
                    Route::get('token/extend/{id}', array('as' => 'admin.token.extend', 'uses' => 'TokenGuideController@extend'));
                    Route::get('token/remove/{id}', array('as' => 'admin.token.remove', 'uses' => 'TokenGuideController@remove'));

                    //Rota de acesso ao Gerenciador de Sistemas Geral
                    Route::get('manager', array('as' => 'admin.manager', 'uses' => 'LoginController@manager'));

                    /*
                      Route::get('manager', function () {
                      Redis::set('email', Auth::user()->email);
                      Redis::expire('email', 10);
                      Auth::logout();
                      return Redirect::to(getenv('ACESSO_MANAGER') . '/externo/' . getenv('ADMIN_TOKEN'));
                      });
                     * 
                     */
                });
            });
        });

        // Rotas de FrontEnd com Autenticação
        Route::get('site/{family}/produtos/{product}/guide/{guide}', array('as' => 'site.guide', 'uses' => 'HomeController@guide'));

        Route::get('dashboard/', array('as' => 'site.dashboard', 'uses' => 'HomeController@dashboard'));

        Route::get('site/notificacoes', array('as' => 'site.notificacoes', 'uses' => 'HomeController@notificacoes'));

        Route::get('site/exportar', array('as' => 'site.exportaR', 'uses' => 'HomeController@exportar'));

        Route::get('site/notificacao/{id_notificacao}', array('as' => 'site.notificacao', 'uses' => 'HomeController@testarNotificacao'));

        //Route::get('site/user', array('as' => 'site.user', 'uses' => 'HomeController@getUser'));
        //Route::get('site/user/config', array('as' => 'site.user.config', 'uses' => 'HomeController@userConfig'));

        Route::get('site/user', array('as' => 'site.user', 'uses' => 'HomeController@userConfigV2'));

        Route::post('site/user', array('as' => 'site.user.post', 'uses' => 'HomeController@postUser'));
        Route::get('site/news', array('as' => 'site.more.news', 'uses' => 'HomeController@news'));
        Route::get('site/news/count', array('as' => 'site.news.count', 'uses' => 'ServicesController@countNews'));
        Route::get('site/news/viewed', array('as' => 'site.news.viewed', 'uses' => 'ServicesController@viewedNews'));
        Route::get('site/more/favorites', array('as' => 'site.more.favorites', 'uses' => 'HomeController@moreFavorites'));

        Route::get('site/more/favorites_new', array('as' => 'site.more.favorites_new', 'uses' => 'HomeController@manageFavorites'));

        // Rotas para Departamentos
        Route::get('admin/departamentos/', array('as' => 'admin.departamentos.gerenciar', 'uses' => 'HomeController@departmentsManager'));
        Route::get('admin/departamentos/adicionar', array('as' => 'admin.departamentos.adicionar', 'uses' => 'HomeController@addDepartment'));
        Route::get('admin/departamentos/editar/{id}', array('as' => 'admin.departamentos.editar', 'uses' => 'HomeController@editDepartment'));
        Route::post('admin/departamentos/salvar/{id?}', array('as' => 'admin.departamentos.criar', 'uses' => 'HomeController@saveDepartment'));
        Route::get('admin/departamentos/excluir/{id}', array('as' => 'admin.departamentos.excluir', 'uses' => 'HomeController@deleteDepartment'));
        // Rotas para Cargos
        Route::get('admin/cargos/', array('as' => 'admin.cargos.gerenciar', 'uses' => 'HomeController@positionsManager'));
        Route::get('admin/cargos/adicionar', array('as' => 'admin.cargos.adicionar', 'uses' => 'HomeController@addPosition'));
        Route::get('admin/cargos/editar/{id}', array('as' => 'admin.cargos.editar', 'uses' => 'HomeController@editPosition'));
        Route::post('admin/cargos/salvar/{id?}', array('as' => 'admin.cargos.criar', 'uses' => 'HomeController@savePosition'));
        Route::get('admin/cargos/excluir/{id}', array('as' => 'admin.cargos.excluir', 'uses' => 'HomeController@deletePosition'));
        // Rotas para Tipos de Instrução
        Route::get('admin/tipos-instrucao/', array('as' => 'admin.tipos-instrucao.gerenciar', 'uses' => 'HomeController@instructionTypesManager'));
        Route::get('admin/tipos-instrucao/adicionar', array('as' => 'admin.tipos-instrucao.adicionar', 'uses' => 'HomeController@addInstructionType'));
        Route::get('admin/tipos-instrucao/editar', array('as' => 'admin.tipos-instrucao.editar', 'uses' => 'HomeController@editInstructionType'));
        // Rotas para Itens de Embalagem
        Route::get('admin/itens-embalagem/', array('as' => 'admin.itens-embalagem.gerenciar', 'uses' => 'HomeController@packagingItemsManager'));
        Route::get('admin/itens-embalagem/adicionar', array('as' => 'admin.itens-embalagem.adicionar', 'uses' => 'HomeController@addPackagingItem'));
        Route::get('admin/itens-embalagem/editar/{id}', array('as' => 'admin.itens-embalagem.editar', 'uses' => 'HomeController@editPackagingItem'));
        Route::post('admin/itens-embalagem/salvar/{id?}', array('as' => 'admin.itens-embalagem.criar', 'uses' => 'HomeController@savePackagingItem'));
        Route::get('admin/itens-embalagem/excluir/{id}', array('as' => 'admin.itens-embalagem.excluir', 'uses' => 'HomeController@deletePackagingItem'));

        // Rotas de Gerenciamento de Usuários pelo Cliente
        Route::group(['middleware' => 'manager'], function () {
            
            Route::get('site/manager', array('as' => 'site.manager', 'uses' => 'HomeController@getManager'));
            Route::post('site/manager/save', array('as' => 'site.manager.save', 'uses' => 'HomeController@postManager'));
            Route::get('site/manager/edit/{id}', array('as' => 'site.manager.edit', 'uses' => 'ServicesController@editUser'));
            Route::get('site/manager/remove/{id}', array('as' => 'site.manager.remove', 'uses' => 'ServicesController@removeUser'));
        });
        
        // Rotas Gerenciamento de Fornecedores
        Route::group(['middleware' => 'gerenciaFornecedor'], function () {
            
            Route::get('site/fornecedores', array('as' => 'site.fornecedores.get', 'uses' => 'FornecedorController@getFornecedores'));
            Route::get('site/fornecedores/adicionar/', array('as' => 'site.fornecedores.adicionar.get', 'uses' => 'FornecedorController@getFornecedoresAdicionar'));
            Route::post('site/fornecedores/adicionar/', array('as' => 'site.fornecedores.adicionar.post', 'uses' => 'FornecedorController@postFornecedoresAdicionar'));
            Route::get('site/fornecedores/editar/{id_fornecedor}', array('as' => 'site.fornecedores.editar.get', 'uses' => 'FornecedorController@getFornecedoresEditar'));
            Route::post('site/fornecedores/editar/{id_fornecedor}', array('as' => 'site.fornecedores.editar.post', 'uses' => 'FornecedorController@postFornecedoresEditar'));
            Route::get('site/fornecedores/excluir/{id_fornecedor}', array('as' => 'site.fornecedores.excluir.get', 'uses' => 'FornecedorController@getFornecedoresExcluir'));
            
        });

        // Rotas Gerenciamento de Usuários
        Route::group(['middleware' => 'gerenciaUsuario'], function () {

            Route::get('admin/profile', array('as' => 'admin.profile', 'uses' => 'PerfilController@index'));
            Route::get('admin/profile/search/{name}', array('as' => 'admin.profile.search', 'uses' => 'PerfilController@search'));
            Route::post('admin/profile/create', array('as' => 'admin.profile.create', 'uses' => 'PerfilController@create'));
            Route::get('admin/profile/edit/{id}', array('as' => 'admin.profile.edit', 'uses' => 'PerfilController@edit'));
            Route::post('admin/profile/update/{id}', array('as' => 'admin.profile.update', 'uses' => 'PerfilController@update'));
            Route::get('admin/profile/remove/{id}', array('as' => 'admin.profile.remove', 'uses' => 'PerfilController@remove'));
            Route::get('site/usuarios/v2', array('as' => 'site.usuarios.gerenciar_v2', 'uses' => 'HomeController@manageUsers'));
            Route::get('site/usuarios/adicionar', array('as' => 'site.usuarios.adicionar', 'uses' => 'HomeController@getAddUser'));
            Route::post('site/usuarios/adicionar', array('as' => 'site.usuarios.adicionar.post', 'uses' => 'HomeController@postAddUser'));
            Route::get('site/usuarios/migrate', array('as' => 'site.usuarios.migrate', 'uses' => 'HomeController@migrate'));
            Route::get('site/usuarios/editar/{id}', array('as' => 'site.usuarios.editar', 'uses' => 'HomeController@getEditUser'));
            Route::post('site/usuarios/editar/{id}', array('as' => 'site.usuarios.editar.post', 'uses' => 'HomeController@postEditUser'));
            Route::get('site/usuarios/excluir/{id}', array('as' => 'site.usuarios.excluir', 'uses' => 'HomeController@getDeleteUser'));
            
        });

        // Rotas Gerenciamento de Embalagens
        Route::group(['middleware' => 'gerenciaEmbalagem'], function () {

            Route::get('admin/sku/embalagens', array('as' => 'admin.sku.embalagens', 'uses' => 'SkuController@indexV2'));

            Route::get('admin/dimensions', array('as' => 'admin.dimensions', 'uses' => 'DimensaoController@index'));
            Route::get('admin/dimensions/search/{name}', array('as' => 'admin.dimensions.search', 'uses' => 'DimensaoController@search'));
            Route::post('admin/dimensions/create', array('as' => 'admin.dimensions.create', 'uses' => 'DimensaoController@create'));
            Route::get('admin/dimensions/edit/{id}', array('as' => 'admin.dimensions.edit', 'uses' => 'DimensaoController@edit'));
            Route::post('admin/dimensions/update/{id}', array('as' => 'admin.dimensions.update', 'uses' => 'DimensaoController@update'));
            Route::get('admin/dimensions/remove/{id}', array('as' => 'admin.dimensions.remove', 'uses' => 'DimensaoController@remove'));

            Route::get('admin/package', array('as' => 'admin.package', 'uses' => 'TipoEmbalagemController@index'));
            Route::get('admin/package/search/{name}', array('as' => 'admin.package.search', 'uses' => 'TipoEmbalagemController@search'));
            Route::post('admin/package/create', array('as' => 'admin.package.create', 'uses' => 'TipoEmbalagemController@create'));
            Route::get('admin/package/edit/{id}', array('as' => 'admin.package.edit', 'uses' => 'TipoEmbalagemController@edit'));
            Route::post('admin/package/update/{id}', array('as' => 'admin.package.update', 'uses' => 'TipoEmbalagemController@update'));
            Route::get('admin/package/remove/{id}', array('as' => 'admin.package.remove', 'uses' => 'TipoEmbalagemController@remove'));

            Route::get('admin/product', array('as' => 'admin.product', 'uses' => 'ProdutoController@index'));
            Route::get('admin/product/search/{name}', array('as' => 'admin.product.search', 'uses' => 'ProdutoController@search'));
            Route::get('admin/product/search/id/{id}', array('as' => 'admin.product.id', 'uses' => 'ProdutoController@byId'));
            Route::post('admin/product/create', array('as' => 'admin.product.create', 'uses' => 'ProdutoController@create'));
            Route::get('admin/product/edit/{id}', array('as' => 'admin.product.edit', 'uses' => 'ProdutoController@edit'));
            Route::post('admin/product/update/{id}', array('as' => 'admin.product.update', 'uses' => 'ProdutoController@update'));
            Route::get('admin/product/remove/{id}', array('as' => 'admin.product.remove', 'uses' => 'ProdutoController@remove'));

            Route::get('admin/product/{attr}/{id}', array('as' => 'admin.product.get', 'uses' => 'RelacaoProdutoController@getHome'));
            Route::post('admin/product/{attr}/{id}', array('as' => 'admin.product.post', 'uses' => 'RelacaoProdutoController@postHome'));
            Route::get('admin/product/{attr}/edit/{id}', array('as' => 'admin.relations.edit', 'uses' => 'RelacaoProdutoController@edit'));
            Route::post('admin/product/{attr}/{prodId}/create', array('as' => 'admin.relations.create', 'uses' => 'RelacaoProdutoController@create'));
            Route::post('admin/product/{attr}/update/{id}', array('as' => 'admin.relations.update', 'uses' => 'RelacaoProdutoController@update'));
            Route::get('admin/product/{attr}/remove/{id}', array('as' => 'admin.relations.remove', 'uses' => 'RelacaoProdutoController@remove'));

            Route::get('admin/family', array('as' => 'admin.family', 'uses' => 'FamiliaController@index'));
            Route::get('admin/family/search/{name}', array('as' => 'admin.family.search', 'uses' => 'FamiliaController@search'));
            Route::get('admin/family/search/id/{id}', array('as' => 'admin.family.id', 'uses' => 'FamiliaController@byId'));
            Route::post('admin/family/create', array('as' => 'admin.family.create', 'uses' => 'FamiliaController@create'));
            Route::get('admin/family/edit/{id}', array('as' => 'admin.family.edit', 'uses' => 'FamiliaController@edit'));
            Route::post('admin/family/update/{id}', array('as' => 'admin.family.update', 'uses' => 'FamiliaController@update'));
            Route::get('admin/family/remove/{id}', array('as' => 'admin.family.remove', 'uses' => 'FamiliaController@remove'));

            Route::get('admin/product-category', array('as' => 'admin.product-category', 'uses' => 'CategoriaProdutoController@index'));
            Route::get('admin/product-category/search/{name}', array('as' => 'admin.product-category.search', 'uses' => 'CategoriaProdutoController@search'));
            Route::post('admin/product-category/create', array('as' => 'admin.product-category.create', 'uses' => 'CategoriaProdutoController@create'));
            Route::get('admin/product-category/edit/{id}', array('as' => 'admin.product-category.edit', 'uses' => 'CategoriaProdutoController@edit'));
            Route::post('admin/product-category/update/{id}', array('as' => 'admin.product-category.update', 'uses' => 'CategoriaProdutoController@update'));
            Route::get('admin/product-category/remove/{id}', array('as' => 'admin.product-category.remove', 'uses' => 'CategoriaProdutoController@remove'));

            // Rotas Gerenciamento de Grupos de Trabalho
            Route::get('admin/grupos-trabalho/', array('as' => 'admin.grupos-trabalho.gerenciar', 'uses' => 'HomeController@workgroups'));
            Route::get('admin/grupos-trabalho/adicionar', array('as' => 'admin.grupos-trabalho.adicionar', 'uses' => 'HomeController@workgroupAdd'));
            Route::get('admin/grupos-trabalho/editar', array('as' => 'admin.grupos-trabalho.editar', 'uses' => 'HomeController@workgroupEdit'));
            
        });

        Route::get('admin/sku/{attr}/{id}', array('as' => 'admin.sku.get', 'uses' => 'RelacaoSkuController@getHome'));
        Route::post('admin/sku/{attr}/{id}', array('as' => 'admin.sku.post', 'uses' => 'RelacaoSkuController@postHome'));

        // Rotas Gerenciamento de Fotos
        Route::group(['middleware' => 'gerenciaFoto'], function () {

            Route::get('admin/sku/fotos/{all?}', array('as' => 'admin.sku.fotos', 'uses' => 'SkuController@fotos'));
            Route::get('admin/sku/finalart/{id}/{acao}/action', array('as' => 'admin.sku.finalartaction.get', 'uses' => 'SkuController@getFinalartAction'));

        });

        // Rotas Gerenciamento de Mídias
        Route::group(['middleware' => 'gerenciaMidia'], function () {
            Route::get('site/adsmart/campanha/excluir/{id}', array('as' => 'site.adsmart.campanha.excluir', 'uses' => 'AdsmartController@campanhaExcluir'));

            Route::get('site/adsmart/tipos-midia', array('as' => 'site.adsmart.tipos_midia', 'uses' => 'AdsmartController@tiposMidia'));
            Route::get('site/adsmart/tipos-midia/criar', array('as' => 'site.adsmart.tipos_midia.criar', 'uses' => 'AdsmartController@tipoMidiaCriar'));
            Route::post('site/adsmart/tipos-midia/criar', array('as' => 'site.adsmart.tipos_midia.criar', 'uses' => 'AdsmartController@postTipoMidiaCriar'));
            Route::get('site/adsmart/tipos-midia/editar/{tipo_id}', array('as' => 'site.adsmart.tipos_midia.editar', 'uses' => 'AdsmartController@tipoMidiaEditar'));
            Route::post('site/adsmart/tipos-midia/editar/{tipo_id}', array('as' => 'site.adsmart.tipos_midia.editar', 'uses' => 'AdsmartController@postTipoMidiaEditar'));
            Route::get('site/adsmart/tipos-midia/excluir/{tipo_id}', array('as' => 'site.adsmart.tipos_midia.excluir', 'uses' => 'AdsmartController@tipoMidiaExcluir'));

            Route::get('site/adsmart/aprovadores-midia', array('as' => 'site.adsmart.aprovadores_midia', 'uses' => 'AdsmartController@aprovadoresMidia'));
            Route::get('site/adsmart/aprovadores-midia/criar', array('as' => 'site.adsmart.aprovadores_midia.criar', 'uses' => 'AdsmartController@aprovadoresMidiaCriar'));
            Route::post('site/adsmart/aprovadores-midia/criar', array('as' => 'site.adsmart.aprovadores_midia.criar.post', 'uses' => 'AdsmartController@postAprovadoresMidiaCriar'));
            Route::get('site/adsmart/aprovadores-midia/editar/{aprovador_id}', array('as' => 'site.adsmart.aprovadores_midia.editar', 'uses' => 'AdsmartController@aprovadoresMidiaEditar'));
            Route::post('site/adsmart/aprovadores-midia/editar/{aprovador_id}', array('as' => 'site.adsmart.aprovadores_midia.editar.post', 'uses' => 'AdsmartController@postAprovadoresMidiaEditar'));
            Route::get('site/adsmart/aprovadores-midia/exluir/{aprovador_id}', array('as' => 'site.adsmart.aprovadores_midia.excluir', 'uses' => 'AdsmartController@aprovadoresMidiaExcluir'));

            Route::get('site/adsmart/condicoes-midia', array('as' => 'site.adsmart.condicoes_midia', 'uses' => 'AdsmartController@condicoesMidia'));
            Route::get('site/adsmart/condicoes-midia/criar', array('as' => 'site.adsmart.condicao_midia.criar', 'uses' => 'AdsmartController@condicaoMidiaCriar'));
            Route::post('site/adsmart/condicoes-midia/criar', array('as' => 'site.adsmart.condicao_midia.criar.post', 'uses' => 'AdsmartController@postCondicoesMidiaCriar'));
            Route::get('site/adsmart/condicoes-midia/editar/{condicao_id}', array('as' => 'site.adsmart.condicao_midia.editar', 'uses' => 'AdsmartController@condicaoMidiaEditar'));
            Route::post('site/adsmart/condicoes-midia/editar/{condicao_id}', array('as' => 'site.adsmart.condicao_midia.editar.post', 'uses' => 'AdsmartController@postCondicoesMidiaEditar'));
            Route::get('site/adsmart/condicoes-midia/exluir/{condicao_id}', array('as' => 'site.adsmart.condicao_midia.excluir', 'uses' => 'AdsmartController@condicoesMidiaExcluir'));

        });

        Route::get('site/grupos', array('as' => 'site.grupos.gerenciar', 'uses' => 'HomeController@groupsManager'));
        Route::get('site/grupos/adicionar', array('as' => 'site.grupos.adicionar', 'uses' => 'HomeController@addGroups'));

        Route::get('site/news/novidades', array('as' => 'site.news.modal_novidades', 'uses' => 'HomeController@siteNews'));
        
        Route::get('site/pesquisa', array('as' => 'site.pesquisa.modal_pesquisa', 'uses' => 'HomeController@pesquisa'));

        Route::get('site/pesquisa_agradecimento', array('as' => 'site.pesquisa.modal_pesquisa_agradecimento', 'uses' => 'HomeController@pesquisaAgradecimento'));
        
        Route::post('site/news/getExibir/', array('as' => 'site.news.getNews', 'uses' => 'HomeController@siteNewsExibir'));

        // Rotas Adsmart
        Route::group(['prefix' => 'site/adsmart', 'as' => 'site.adsmart.'], function () {

            Route::group(['middleware' => 'adsmart'], function () {

                Route::group(['middleware' => 'adsmartCriarCampanha'], function () {
                    Route::get('campanha/criar', array('as' => 'campanha.criar.get', 'uses' => 'AdsmartController@getCampanhaCriar'));
                    Route::post('campanha/criar', array('as' => 'campanha.criar.post', 'uses' => 'AdsmartController@postCampanhaCriar'));
                });

                Route::group(['middleware' => 'adsmartCriarMidia'], function () {

                    Route::get('familia/{id_familia}/campanhas/{id_campanha}/midias/criar', array('as' => 'midias.criar', 'uses' => 'AdsmartController@midiaCriar'));
                    Route::post('midia/criar', array('as' => 'midia.criar.post', 'uses' => 'AdsmartController@postMidiaCriar'));

                    Route::get('ticket/criar/{id_peca}/{id_familia}', array('as' => 'ticket.criar', 'uses' => 'AdsmartController@ticketCriar'));
                    Route::post('ticket/criar/{id_peca}/{id_familia}', array('as' => 'ticket.criar.post', 'uses' => 'AdsmartController@ticketCriarPost'));

                });

                Route::group(['middleware' => 'adsmartHistorico'], function () {

                    Route::get('/historico/{numero}', array('as' => 'historico', 'uses' => 'AdsmartController@historico'));

                });

                Route::get('abrirArquivo/{id_arquivo}/{filename}', array('as' => 'abrirArquivo', 'uses' => 'AdsmartController@abrirArquivo'));

                Route::get('abrir/{id_arquivo}/{filename}', array('as' => 'abrir', 'uses' => 'AdsmartController@abrirLayout'));
                Route::get('baixar/{id_arquivo}/{filename}', array('as' => 'baixar', 'uses' => 'AdsmartController@baixarLayout'));

                Route::get('/', array('as' => 'familias', 'uses' => 'AdsmartController@familias'));

                Route::get('ticket/{numero}/membros', array('as' => 'ticket.membros', 'uses' => 'AdsmartController@ticketMembros'));

                Route::post('ticket/{numero}/membros', array('as' => 'ticket.membros.post', 'uses' => 'AdsmartController@ticketMembrosPost'));

                Route::get('ticket', array('as' => 'ticket', 'uses' => 'AdsmartController@ticketEstatico'));

                Route::get('/notificacao/{id_notificacao}', array('as' => 'notificacao', 'uses' => 'AdsmartController@testarNotificacao'));
                
                Route::get('ticket/{numero}', array('as' => 'ticket', 'uses' => 'AdsmartController@ticket'));

                Route::get('ticket/{numero}/edit', array('as' => 'ticket.edicao', 'uses' => 'AdsmartController@ticketEdicao'));
                Route::post('ticket/{numero}/edit', array('as' => 'ticket.edicao', 'uses' => 'AdsmartController@ticketEdicaoPost'));

                Route::get('ticket/{numero}/reprovar', array('as' => 'ticket.deny', 'uses' => 'AdsmartController@ticketReprovar'));
                Route::post('ticket/{numero}/reprovar', array('as' => 'ticket.deny.post', 'uses' => 'AdsmartController@ticketReprovarPost'));

                Route::get('ticket/edicao/reprovado', array('as' => 'ticket.edicao.reprovado', 'uses' => 'AdsmartController@ticketReprovado'));

                Route::get('familia/{id_familia}/campanhas', array('as' => 'campanhas', 'uses' => 'AdsmartController@campanhas'));

                Route::get('familia/{id_familia}/campanhas/{id_campanha}/midias', array('as' => 'midias', 'uses' => 'AdsmartController@midias'));
                

                //Route::get('familia/campanhas/midias', array('as' => 'midias.old', 'uses' => 'AdsmartController@midias'));

                //Route::get('familia/campanhas/midias/criar', array('as' => 'midias.criar', 'uses' => 'AdsmartController@midiaCriar'));


                

                // Serviços
                Route::group(['prefix' => 'services', 'as' => 'services'], function () {

                    Route::get('familias', array('as' => 'familias', 'uses' => 'AdsmartServiceController@familias'));
                    Route::get('midiaTipos', array('as' => 'midiaTipos', 'uses' => 'AdsmartServiceController@midiaTipos'));
                    Route::get('midias/{id_campanha}', array('as' => 'midiaTipos', 'uses' => 'AdsmartServiceController@midias'));
                    Route::post('midias/criarPeca', array('as' => 'midiaTipos', 'uses' => 'AdsmartServiceController@postPecaCriar'));
                    Route::post('midias/editarPeca', array('as' => 'midiaTipos', 'uses' => 'AdsmartServiceController@postPecaEditar'));
                    Route::post('midias/excluirPeca', array('as' => 'midiasExcluirPeca', 'uses' => 'AdsmartServiceController@postPecaExcluir'));
                    Route::post('midias/excluirMidia', array('as' => 'midiasExcluirPeca', 'uses' => 'AdsmartServiceController@postMidiaExcluir'));
                    Route::get('condicoesMidia/{tipo}', array('as' => 'condicoesMidia', 'uses' => 'AdsmartServiceController@condicoesMidia'));
                    Route::get('aprovadoresMidia', array('as' => 'aprovadoresMidia', 'uses' => 'AdsmartServiceController@aprovadoresMidia'));
                    Route::get('aprovadoresMidia/{id_tipo_midia}', array('as' => 'aprovadoresMidia', 'uses' => 'AdsmartServiceController@aprovadoresMidiaPorTipo'));
                    Route::get('aprovadorMidia/{id_grupo}', array('as' => 'aprovadorMidia', 'uses' => 'AdsmartServiceController@aprovadorMidia'));
                    Route::post('uploadLayout', array('as' => 'ticketUploadLayout', 'uses' => 'AdsmartServiceController@uploadLayout'));
                    Route::post('ticketAcao', array('as' => 'ticketAcao', 'uses' => 'AdsmartServiceController@ticketAcao'));
                    Route::post('upload', array('as' => 'upload', 'uses' => 'AdsmartServiceController@upload'));
                    Route::post('uploadArquivo', array('as' => 'uploadArquivo', 'uses' => 'AdsmartServiceController@uploadArquivo'));
                    Route::post('deleteLayout', array('as' => 'deleteLayout', 'uses' => 'AdsmartServiceController@deleteLayout'));
                });
            
            });

        });

        // Rotas Artwork
        Route::group(['prefix' => 'site/artwork', 'as' => 'site.artwork.'], function () {

            Route::get('apagaItemVazio', array('as' => 'apagaItemVazio', 'uses' => 'ArtworkController@apagaItemVazio'));
            
            Route::get('atualizaDepartamentoVazio', array('as' => 'atualizaDepartamentoVazio', 'uses' => 'ArtworkController@atualizaDepartamentoVazio'));

            Route::group(['middleware' => 'artwork'], function () {

                //Route::get('atualizaTicketsEncerramento', array('as' => 'atualizaTicketsEncerramento', 'uses' => 'ArtworkController@atualizaTicketsEncerramento'));
                Route::get('atualizaCicloTickets', array('as' => 'atualizaCicloTickets', 'uses' => 'ArtworkController@atualizaCicloTickets'));

                Route::get('baixarPdf/{id_arquivo}/{tipo}', array('as' => 'baixarPdf', 'uses' => 'ArtworkController@baixarPdf'));
                
                Route::get('abrirPdf/{id_arquivo}/{tipo}', array('as' => 'abrirPdf', 'uses' => 'ArtworkController@abrirPdf'));
                Route::get('abrirPdf/{id_arquivo}/{tipo}/{arquivo}', array('as' => 'abrirPdfNome', 'uses' => 'ArtworkController@abrirPdf'));
                
                Route::get('abrirArquivo/{id_arquivo}/{filename}', array('as' => 'abrirArquivo', 'uses' => 'ArtworkController@abrirArquivo'));
                Route::get('baixarArteFinal/{id_arquivo}', array('as' => 'baixarArteFinal', 'uses' => 'ArtworkController@baixarArteFinal'));
                Route::get('baixarFornecedorPdf/{id_arquivo}', array('as' => 'baixarFornecedorPdf', 'uses' => 'ArtworkController@baixarFornecedorPdf'));
                Route::get('abrirFornecedorPdf/{id_arquivo}', array('as' => 'abrirFornecedorPdf', 'uses' => 'ArtworkController@abrirFornecedorPdf'));

                // Acesso ao API
                Route::get('view/{id_arquivo}/{tipo}/{id_ticket?}', array('as' => 'view', 'uses' => 'ApiController@view'))->defaults(null, null, null);
                Route::get('compare/{id_arquivo}/{tipo}/{id_arquivo2}/{tipo2}/{id_ticket}', array('as' => 'compare', 'uses' => 'ApiController@compare'));
                Route::get('compareFornecedorPdf/{id_pdf}', array('as' => 'compareFornecedorPdf', 'uses' => 'ApiController@compareFornecedorPdf'));
                Route::get('viewService/{id_arquivo}/{tipo}/{id_ticket?}', array('as' => 'viewService', 'uses' => 'ApiController@viewService'));
                Route::get('compareService/{id_arquivo}/{tipo}/{id_arquivo2}/{tipo2}/{id_ticket}', array('as' => 'compareService', 'uses' => 'ApiController@compareService'));
                Route::get('compareServiceFornecedorPdf/{id_arquivo1}/{id_arquivo2}', array('as' => 'compareServiceFornecedorPdf', 'uses' => 'ApiController@compareServiceFornecedorPdf'));

                Route::get('compare/frame', array('as' => 'comparar', 'uses' => 'ApiController@ApiCompareIframe'));

                //Route::get('/', array('as' => 'category', 'uses' => 'ArtworkController@category'));
                //Route::get('{cat}/family/', array('as' => 'family', 'uses' => 'ArtworkController@family'));
                Route::get('/', array('as' => 'family', 'uses' => 'ArtworkController@family'));

                Route::get('/', array('as' => 'family', 'uses' => 'ArtworkController@family'));

                Route::get('family/{family}/product', array('as' => 'product', 'uses' => 'ArtworkController@product'));

                //Route::get('{cat}/family/{family}/product/{product}', array('as' => 'package', 'uses' => 'ArtworkController@package'));

                /*
                  Route::get('manager', array('as' => 'manager', 'uses' => 'ArtworkController@manager'));
                  Route::get('manager/{tipo?}', array('as' => 'manager', 'uses' => 'ArtworkController@manager'));
                  Route::get('managerV2/{tipo?}', array('as' => 'manager', 'uses' => 'ArtworkController@managerV2'));
                  Route::get('managerV3/{tipo?}', array('as' => 'managerV3', 'uses' => 'ArtworkController@managerV3'));
                 * 
                 */

                //Route::get('package/new', array('as' => 'package.create.get', 'uses' => 'ArtworkController@getPackageCreateV2'));
                Route::get('family/{family}/product/{product}', array('as' => 'package', 'uses' => 'ArtworkController@packageNew'));

                Route::get('family/{family}/product/{product}/v3', array('as' => 'package', 'uses' => 'ArtworkController@packageNewV3'));

                Route::get('family/{family}/product/{product}/admin', array('as' => 'packageAdmin', 'uses' => 'ArtworkController@packageAdmin'));

                Route::get('family/product/packages', array('as' => 'packages', 'uses' => 'ArtworkController@packageList'));

                Route::post('family/{family}/product/{product}', array('as' => 'package.post', 'uses' => 'ArtworkController@postPackageNew'));

                Route::get('/history/{id_variacao}/{id_versao}', array('as' => 'history', 'uses' => 'ArtworkController@history'));

                Route::get('/versions/{variacao}', array('as' => 'versions', 'uses' => 'ArtworkController@versions'));

                Route::get('/packages/manage', array('as' => 'packages.manage.get', 'uses' => 'ArtworkController@getPackageManager'));

                Route::get('package/edit/v3/{id_variacao}/{id_item?}', array('as' => 'package.edit.get', 'uses' => 'ArtworkController@getPackageEditV3'));
                Route::post('package/edit/v3/{id_variacao}/{id_item?}', array('as' => 'package.edit.post', 'uses' => 'ArtworkController@postPackageEditV3'));

                Route::get('ticket/{ticket}/edit/', array('as' => 'ticket.editNew.get', 'uses' => 'ArtworkController@getTicketEditNew'));
                Route::post('ticket/{ticket}/edit/', array('as' => 'ticket.editNew.post', 'uses' => 'ArtworkController@postTicketEditNew'));

                Route::get('ticket/baixarArquivos/{id_ticket}', array('as' => 'ticket.baixarArquivos', 'uses' => 'ArtworkController@ticketBaixarArquivos'));

                Route::get('ticket/{ticket}/', array('as' => 'ticket.edit.step1.get', 'uses' => 'ArtworkController@ticketEditStep1'));
                Route::get('ticket/{ticket}/edit/step2', array('as' => 'ticket.edit.step2.get', 'uses' => 'ArtworkController@ticketEditStep2'));
                Route::get('ticket/{ticket}/edit/step3', array('as' => 'ticket.edit.step3.get', 'uses' => 'ArtworkController@ticketEditStep3'));
                Route::get('ticket/{ticket}/edit/step4', array('as' => 'ticket.edit.step4.get', 'uses' => 'ArtworkController@ticketEditStep4'));
                Route::get('ticket/{ticket}/edit/step5', array('as' => 'ticket.edit.step5.get', 'uses' => 'ArtworkController@ticketEditStep5'));
                Route::get('ticket/{ticket}/edit/step6', array('as' => 'ticket.edit.step6.get', 'uses' => 'ArtworkController@ticketEditStep6'));
                
                //Route::get('ticket/{ticket}/edit/deny/step2', array('as' => 'ticket.edit.deny.get', 'uses' => 'ArtworkController@getTicketEditDeny'));
                //Route::post('ticket/{ticket}/edit/deny/step2', array('as' => 'ticket.edit.deny.post', 'uses' => 'ArtworkController@postTicketEditDeny'));
                
                Route::get('ticket/{ticket}/print', array('as' => 'ticket.print.get', 'uses' => 'ArtworkController@ticketPrint'));

                Route::group(['middleware' => 'artworkEnviarArte'], function () {
                    Route::get('/history/{variacao}/{item}/send', array('as' => 'send', 'uses' => 'ArtworkController@sendArtwork'));
                });
                
                Route::group(['middleware' => 'artworkFornecedorPdf'], function () {
                    Route::get('/history/{variacao}/{item}/fornecedorPdf', array('as' => 'link_fornecedorpdf', 'uses' => 'ArtworkController@fornecedorPdf'));
                });

                Route::group(['middleware' => 'artworkAbrirTicket'], function () {
                    Route::get('ticket/create/{ticket}/members', array('as' => 'ticket.members.get', 'uses' => 'ArtworkController@getTicketMembers'));
                    Route::post('ticket/create/{ticket}/members', array('as' => 'ticket.members.post', 'uses' => 'ArtworkController@postTicketMembers'));

                    Route::get('ticket/create/{variacao_id}/{item_id}/variacao', array('as' => 'ticket.create.get', 'uses' => 'ArtworkController@getTicketCreate'));
                    Route::get('ticket/create/{variacao_id}/{item_id}/item', array('as' => 'ticket.createItem.get', 'uses' => 'ArtworkController@getTicketCreateItem'));

                    Route::post('ticket/create/{variacao_id}/{item_id}/variacao', array('as' => 'ticket.create.post', 'uses' => 'ArtworkController@postTicketCreate'))->defaults('tipo', 'item');

                    Route::get('ticket/create/{ticket_id}/{variacao_id}', array('as' => 'ticket.edit.get', 'uses' => 'ArtworkController@getTicketEditNew'));
                    Route::post('ticket/create/{ticket_id}/{variacao_id}', array('as' => 'ticket.edit.post', 'uses' => 'ArtworkController@postTicketEditNew'));

                    Route::get('ticket/{ticket}/print', array('as' => 'ticket.print.get', 'uses' => 'ArtworkController@ticketPrint'));
                });

                Route::group(['middleware' => 'artworkCriarEmbalagem'], function () {
                    Route::get('package/create', array('as' => 'package.create.get', 'uses' => 'ArtworkController@getPackageCreateV2'));
                    Route::get('package/create/v3', array('as' => 'package.create.get', 'uses' => 'ArtworkController@getPackageCreateV3'));
                });
            });

            Route::group(['middleware' => 'workflow'], function () {

                // Acesso ao API
                Route::get('workflow/view/{id_arquivo}/{tipo}/{id_ticket?}', array('as' => 'workflow.view', 'uses' => 'ApiController@workflowView'))->defaults(null, null, null);
                Route::get('workflow/viewService/{id_arquivo}/{tipo}/{id_ticket?}', array('as' => 'workflow.viewService', 'uses' => 'ApiController@viewService'));
                Route::get('workflow/baixarArteFinal/{id_arquivo}', array('as' => 'workflow.baixarArteFinal', 'uses' => 'ArtworkController@baixarArteFinal'));
                Route::get('workflow/baixarPdf/{id_arquivo}/{tipo}', array('as' => 'workflow.baixarPdf', 'uses' => 'ArtworkController@baixarPdf'));

                Route::get('manager', array('as' => 'manager', 'uses' => 'ArtworkController@manager'));
                Route::get('manager/{tipo?}', array('as' => 'manager', 'uses' => 'ArtworkController@manager'));
                Route::get('managerV2/{tipo?}', array('as' => 'manager', 'uses' => 'ArtworkController@managerV2'));
                Route::get('managerV3/{tipo?}', array('as' => 'managerV3', 'uses' => 'ArtworkController@managerV3'));
                Route::get('managerV4/{tipo?}', array('as' => 'managerV4', 'uses' => 'ArtworkController@managerV4'));

                Route::get('managerV5/{tipo?}', array('as' => 'managerV5', 'uses' => 'ArtworkController@managerV5'));

                Route::get('ticket/{ticket}/edit/', array('as' => 'ticket.editNew.get', 'uses' => 'ArtworkController@getTicketEditNew'));
                Route::post('ticket/{ticket}/edit/', array('as' => 'ticket.editNew.post', 'uses' => 'ArtworkController@postTicketEditNew'));

                Route::get('ticket/baixarArquivos/{id_ticket}', array('as' => 'ticket.baixarArquivos', 'uses' => 'ArtworkController@ticketBaixarArquivos'));

                Route::get('ticket/{ticket}/', array('as' => 'ticket.edit.step1.get', 'uses' => 'ArtworkController@ticketEditStep1'));
                Route::get('ticket/{ticket}/edit/step2', array('as' => 'ticket.edit.step2.get', 'uses' => 'ArtworkController@ticketEditStep2'));
                Route::get('ticket/{ticket}/edit/step3', array('as' => 'ticket.edit.step3.get', 'uses' => 'ArtworkController@ticketEditStep3'));
                Route::get('ticket/{ticket}/edit/step4', array('as' => 'ticket.edit.step4.get', 'uses' => 'ArtworkController@ticketEditStep4'));
                Route::get('ticket/{ticket}/edit/step5', array('as' => 'ticket.edit.step5.get', 'uses' => 'ArtworkController@ticketEditStep5'));
                Route::get('ticket/{ticket}/edit/step6', array('as' => 'ticket.edit.step6.get', 'uses' => 'ArtworkController@ticketEditStep6'));
                
                Route::get('ticket/{ticket}/edit/deny', array('as' => 'ticket.edit.deny.get', 'uses' => 'ApiController@getTicketEditDeny'));
                
                Route::get('ticket/{ticket}/edit/deny/back', array('as' => 'ticket.edit.deny.back.get', 'uses' => 'ApiController@getTicketEditDenyBack'));
                
                Route::get('ticket/{ticket}/edit/deny/step2', array('as' => 'ticket.edit.deny.step2.get', 'uses' => 'ArtworkController@getTicketEditDeny'));
                Route::post('ticket/{ticket}/edit/deny/step2', array('as' => 'ticket.edit.deny.step2.post', 'uses' => 'ArtworkController@postTicketEditDeny'));
                
            });

            // Serviços Artwork
            Route::group(['prefix' => 'services/package'], function () {
                Route::get('usuarios', array('as' => 'artwork.services.usuarios', 'uses' => 'ArtworkServiceController@usuarios'));
                Route::post('members', array('as' => 'artwork.services.members', 'uses' => 'ArtworkServiceController@members'));
                Route::get('categories', array('as' => 'artwork.services.categories', 'uses' => 'ArtworkServiceController@categories'));
                Route::get('families/{category?}', array('as' => 'artwork.services.families', 'uses' => 'ArtworkServiceController@families'));
                Route::get('products', array('as' => 'artwork.services.products', 'uses' => 'ArtworkServiceController@products'));
                Route::get('variacoes/{id_produto}/{id_dimensao}/{id_campanha?}', array('as' => 'artwork.services.variacoes', 'uses' => 'ArtworkServiceController@variacoes'));
                Route::get('types', array('as' => 'artwork.services.types', 'uses' => 'ArtworkServiceController@typesPackage'));
                Route::get('projetos', array('as' => 'artwork.services.projetos', 'uses' => 'ArtworkServiceController@projetos'));
                Route::get('typesItems', array('as' => 'artwork.services.typesItems', 'uses' => 'ArtworkServiceController@typesItems'));
                Route::get('request/types', array('as' => 'artwork.services.typesRequest', 'uses' => 'ArtworkServiceController@typesRequest'));
                Route::get('types/{type}/sizes', array('as' => 'artwork.services.types', 'uses' => 'ArtworkServiceController@sizesPackage'));
                Route::get('promotions/{product}', array('as' => 'artwork.services.promotions', 'uses' => 'ArtworkServiceController@promotions'));
                Route::get('ticket_members/{id_ticket}', array('as' => 'artwork.services.ticket_members', 'uses' => 'ArtworkServiceController@ticket_members'));
                Route::get('ticket/{id_ticket}', array('as' => 'artwork.services.ticket', 'uses' => 'ArtworkServiceController@ticket'));
                Route::post('comentar', array('as' => 'artwork.services.comentar', 'uses' => 'ArtworkServiceController@comentar'));
                Route::post('responder', array('as' => 'artwork.services.responder', 'uses' => 'ArtworkServiceController@responder'));
                Route::post('upload', array('as' => 'artwork.services.upload', 'uses' => 'ArtworkServiceController@upload'));
                Route::post('ticket/uploadArquivo', array('as' => 'artwork.services.ticket.uploadArquivo', 'uses' => 'ArtworkServiceController@ticketUploadArquivo'));
                Route::post('ticket/renomearArquivo', array('as' => 'artwork.services.ticket.renomearArquivo', 'uses' => 'ArtworkServiceController@ticketRenomearArquivo'));
                Route::post('ticket/excluirArquivo', array('as' => 'artwork.services.ticket.excluirArquivo', 'uses' => 'ArtworkServiceController@ticketExcluirArquivo'));
                Route::post('ticket/uploadFornecedorPdf', array('as' => 'artwork.services.ticket.uploadFornecedorPdf', 'uses' => 'ArtworkServiceController@ticketUploadFornecedorPdf'));
                Route::post('ticket/checkFornecedorPdf', array('as' => 'artwork.services.ticket.checkFornecedorPdf', 'uses' => 'ArtworkServiceController@ticketCheckFornecedorPdf'));
                Route::post('ticket/uploadArte', array('as' => 'artwork.services.ticket.uploadArte', 'uses' => 'ArtworkServiceController@ticketUploadArte'));
                Route::post('ticket/deleteArte', array('as' => 'artwork.services.ticket.deleteArte', 'uses' => 'ArtworkServiceController@ticketDeleteArte'));
                Route::post('ticket/uploadLayout', array('as' => 'artwork.services.ticket.uploadLayout', 'uses' => 'ArtworkServiceController@ticketUploadLayout'));
                Route::post('ticket/acao', array('as' => 'artwork.services.ticket.acao', 'uses' => 'ArtworkServiceController@ticketAcao'));
                Route::post('ticket/enviarEmails', array('as' => 'artwork.services.ticket.enviarEmails', 'uses' => 'ArtworkServiceController@ticketEnviarEmails'));
                Route::post('ticket/enviarLembrete', array('as' => 'artwork.services.ticket.enviarLembrete', 'uses' => 'ArtworkServiceController@ticketEnviarLembrete'));
                Route::post('ticket/seguir', array('as' => 'artwork.services.ticket.seguir', 'uses' => 'ArtworkServiceController@ticketSeguir'));
                Route::post('ticket/setStatusEnvio', array('as' => 'artwork.services.ticket.setStatusEnvio', 'uses' => 'ArtworkServiceController@setStatusEnvio'));
                Route::post('ticket/cancelarEnvio', array('as' => 'artwork.services.ticket.cancelarEnvio', 'uses' => 'ArtworkServiceController@cancelarEnvio'));
                Route::post('ticket/renovarLink', array('as' => 'artwork.services.ticket.renovarLink', 'uses' => 'ArtworkServiceController@renovarLink'));
                Route::post('ticket/reenviarEmail', array('as' => 'artwork.services.ticket.reenviarEmail', 'uses' => 'ArtworkServiceController@reenviarEmail'));
                Route::post('solicitacao/acao', array('as' => 'artwork.services.solicitacao.acao', 'uses' => 'ArtworkServiceController@solicitacaoAcao'));
                Route::get('perfis', array('as' => 'perfis', 'uses' => 'ArtworkServiceController@perfis'));
                Route::get('fornecedores', array('as' => 'perfis', 'uses' => 'ArtworkServiceController@fornecedores'));
                Route::get('getArtes/{id_variacao}/{id_item?}', array('as' => 'artwork.services.getArtes', 'uses' => 'ArtworkServiceController@getArtes'));
                Route::post('postArtes', array('as' => 'artwork.services.postArtes', 'uses' => 'ArtworkServiceController@postArtes'));
                Route::get('getEmbalagens/{id_produto}', array('as' => 'artwork.embalagens.get', 'uses' => 'ArtworkServiceController@getEmbalagens'));
                Route::get('getEmbalagens', array('as' => 'artwork.todasEmbalagens.get', 'uses' => 'ArtworkServiceController@getTodasEmbalagens'));
                Route::get('getEmbalagensPorNatureza/{id_produto}/{id_natureza}', array('as' => 'artwork.embalagensPorNatureza.get', 'uses' => 'ArtworkServiceController@getEmbalagensPorNatureza'));
                Route::post('cadastrarItem', array('as' => 'artwork.cadastrarItem.post', 'uses' => 'ArtworkServiceController@cadastrarItem'));
                Route::post('alterarItem', array('as' => 'artwork.alterarItem.post', 'uses' => 'ArtworkServiceController@alterarItem'));
                Route::post('descontinuarItem', array('as' => 'artwork.descontinuarItem.post', 'uses' => 'ArtworkServiceController@descontinuarItem'));
                Route::post('retornarItem', array('as' => 'artwork.retornarItem.post', 'uses' => 'ArtworkServiceController@retornarItem'));
                Route::post('excluirItem', array('as' => 'artwork.excluirItem.post', 'uses' => 'ArtworkServiceController@excluirItem'));
                Route::post('excluirEmbalagem', array('as' => 'artwork.excluirEmbalagem.post', 'uses' => 'ArtworkServiceController@excluirEmbalagem'));
                Route::post('checkItem', array('as' => 'artwork.checkItem.post', 'uses' => 'ArtworkServiceController@checkItem'));
                Route::get('mercados', array('as' => 'artwork.deletarPaintpack.get', 'uses' => 'ArtworkServiceController@mercados'));
                Route::post('ticket/renovarAprovacao', array('as' => 'artwork.services.ticket.renovarAprovacao', 'uses' => 'ArtworkServiceController@renovarAprovacao'));
                Route::post('ticket/pausarCancelar', array('as' => 'artwork.services.ticket.pausarCancelar', 'uses' => 'ArtworkServiceController@ticketPausarCancelar'));
                Route::get('tickets/historico', array('as' => 'services.ticketsHistorico', 'uses' => 'ArtworkServiceController@getTicketsHistorico'));
                Route::get('tickets/{tipo?}', array('as' => 'services.tickets', 'uses' => 'ArtworkServiceController@getTickets'));
                Route::get('alteraRotulo/{tipo}', array('as' => 'artwork.alteraRotulo.get', 'uses' => 'ArtworkServiceController@alteraRotulo'));
                Route::post('create', array('as' => 'package.create.post', 'uses' => 'ArtworkServiceController@postPackageCreate'));
                Route::post('edit', array('as' => 'package.edit.post', 'uses' => 'ArtworkServiceController@postPackageEdit'));
                Route::post('retornarEmbalagemMercado', array('as' => 'package.retornarEmbalagemMercado.post', 'uses' => 'ArtworkServiceController@retornarEmbalagemMercado'));
                Route::post('ticketSalvarParticipantes', array('as' => 'ticketSalvarParticipantes.post', 'uses' => 'ArtworkServiceController@ticketSalvarParticipantes'));
                Route::get('getItem/{id_item}/{embalagens?}', array('as' => 'getItem.get', 'uses' => 'ArtworkServiceController@getItem'))->defaults('embalagens', 0);
                Route::post('postCompartilharItem', array('as' => 'postCompartilharItem.post', 'uses' => 'ArtworkServiceController@postCompartilharItem'));
                Route::post('ticket/acaoFornecedorPdf', array('as' => 'artwork.services.ticket.acaoFornecedorPdf', 'uses' => 'ArtworkServiceController@ticketAcaoFornecedorPdf'));

                Route::post('ticket/acaoNotificarPdfAnexado', array('as' => 'artwork.services.ticket.acaoNotificarPdfAnexado', 'uses' => 'ArtworkServiceController@ticketAcaoNotificarPdfAnexado'));

                Route::post('ticket/excluirFornecedorPdf', array('as' => 'artwork.services.ticket.excluirFornecedorPdf', 'uses' => 'ArtworkServiceController@ticketExcluirFornecedorPdf'));
                Route::get('ticket/getTimeline/{etapa}/{id_item}', array('as' => 'artwork.services.ticket.getTimeline', 'uses' => 'ArtworkServiceController@ticketTimeline'));
                
                // Grupos de Aprovação
                Route::get('getGruposDeAprovacao', array('as' => 'artwork.services.ticket.getGruposDeAprovacao', 'uses' => 'ArtworkServiceController@getGruposDeAprovacao'));
                Route::post('postGruposDeAprovacao', array('as' => 'artwork.services.ticket.postGruposDeAprovacao', 'uses' => 'ArtworkServiceController@postGruposDeAprovacao'));
                Route::post('postSalvarGruposDeAprovacao', array('as' => 'artwork.services.ticket.postSalvarGruposDeAprovacao', 'uses' => 'ArtworkServiceController@postSalvarGruposDeAprovacao'));
                Route::post('postExcluirGruposDeAprovacao', array('as' => 'artwork.services.ticket.postExcluirGruposDeAprovacao', 'uses' => 'ArtworkServiceController@postExcluirGruposDeAprovacao'));
                
            });
        });

        // Rotas TechDraw
        Route::group(['prefix' => 'site/techdraw', 'as' => 'site.techdraw.', 'middleware' => 'techdraw'], function () {
            //Route::get('/', array('as' => 'embalagens', 'uses' => 'TechdrawController@embalagens'));
            Route::get('/', array('as' => 'technicalPlan', 'uses' => 'TechdrawController@technicalPlan'));
            Route::get('/package/{id_tipo}', array('as' => 'technicalPlan.package', 'uses' => 'TechdrawController@package'));
            Route::get('/edit/{id_variacao}', array('as' => 'technicalPlan.edit.get', 'uses' => 'TechdrawController@getCreateNewPlanEdit'));
            
            Route::get('abrirPdf/{id_arquivo}', array('as' => 'abrirPdf', 'uses' => 'TechdrawController@abrirPdf'));
            
            Route::post('/edit/{id_variacao}', array('as' => 'technicalPlan.edit', 'uses' => 'TechdrawController@postCreateNewPlanEdit'));

            Route::group(['middleware' => 'techdrawCriarPlanta'], function () {
                Route::get('/create', array('as' => 'technicalPlan.create.get', 'uses' => 'TechdrawController@getCreateNewPlan'));
                Route::post('/create', array('as' => 'technicalPlan.create.post', 'uses' => 'TechdrawController@postCreateNewPlan'));
            });

            // Serviços TechDraw
            Route::group(['prefix' => 'services'], function () {
                Route::get('checkVariacoes/{id_dimensao}/', array('as' => 'techdraw.services.checkVariacoes', 'uses' => 'TechdrawServiceController@checkVariacoes'));
                Route::get('getPlantas/{id_tipo}/', array('as' => 'techdraw.services.getPlantas', 'uses' => 'TechdrawServiceController@getPlantas'));
                Route::post('criarItem', array('as' => 'techdraw.services.criarItem', 'uses' => 'TechdrawServiceController@criarItem'));
                Route::post('alterarItem', array('as' => 'techdraw.services.alterarItem', 'uses' => 'TechdrawServiceController@alterarItem'));
                Route::post('removerItem', array('as' => 'techdraw.services.removerItem', 'uses' => 'TechdrawServiceController@removerItem'));
                Route::post('removerPlanta', array('as' => 'techdraw.services.removerPlanta', 'uses' => 'TechdrawServiceController@removerPlanta'));
                Route::get('getArtes/{id_item}/', array('as' => 'techdraw.services.getArtes', 'uses' => 'TechdrawServiceController@getArtes'));
                Route::post('postArtes', array('as' => 'techdraw.services.postArtes', 'uses' => 'TechdrawServiceController@postArtes'));
            });
        });

        // Rotas de Gerenciamento de Tokens Temporários
        Route::get('site/token', array('as' => 'site.token', 'uses' => 'TokenGuideController@siteIndex'));
        Route::get('site/token/create/{produto}', array('as' => 'site.token.create', 'uses' => 'TokenGuideController@create'));
        Route::get('site/token/extend/{id}', array('as' => 'site.token.extend', 'uses' => 'TokenGuideController@siteExtend'));
        Route::get('site/token/remove/{id}', array('as' => 'site.token.remove', 'uses' => 'TokenGuideController@siteRemove'));
    });

    /*
      Route::get('/escalabrahma', function () {
      return redirect()->route("site.guidenew.brahma");
      });

      Route::get('/site/escalabrahma', function () {
      return redirect()->route("site.guidenew.brahma");
      });
     * 
     */

    Route::get('/escalabrahma', array('as' => 'site.escalabrahma', 'uses' => 'HomeController@escalabrahma'));
    Route::get('/site/escalabrahma', array('as' => 'site.escalabrahma', 'uses' => 'HomeController@escalabrahma'));
    
    Route::get('/site/politica-de-privacidade', array('as' => 'site.politica_de_privacidade.index', 'uses' => 'HomeController@politicaPrivacidade'));
    Route::get('/site/termos-de-uso', array('as' => 'site.termos_de_uso.index', 'uses' => 'HomeController@termosDeUso'));

    // Rotas de FrontEnd sem Autenticação
    Route::group(['prefix' => 'site'], function () {

        Route::group(['middleware' => 'packshelf'], function () {

            Route::get('/{modulo?}', array('as' => 'site.index', 'uses' => 'HomeController@index'))->defaults('modulo', 'sku');
        });

        Route::get('{family}', array('as' => 'site.search.family', 'uses' => 'HomeController@searchFamily'));
        Route::get('search/{search}', array('as' => 'site.search', 'uses' => 'HomeController@search'));
        Route::get('more/lasts', array('as' => 'site.more.lasts', 'uses' => 'HomeController@moreLasts'));

        Route::get('more/recents', array('as' => 'site.more.recents', 'uses' => 'HomeController@recentlyAddedItems'));

        Route::get('more/about', array('as' => 'site.footer.about', 'uses' => 'HomeController@about'));
        Route::get('more/suport', array('as' => 'site.footer.suport', 'uses' => 'HomeController@suport'));
        Route::group(['prefix' => '{family}/produtos'], function () {
            Route::get('/{modulo?}', array('as' => 'site.product', 'uses' => 'HomeController@product'))->defaults('modulo', 'sku');
            Route::get('{product}', array('as' => 'site.search.product', 'uses' => 'HomeController@searchProduct'));
            Route::get('{product}/favorite', array('as' => 'site.product.favorite', 'uses' => 'ServicesController@checkFavorite'));
            Route::get('{product}/sku/old', array('as' => 'site.sku.old', 'uses' => 'HomeController@sku'));
            Route::get('{product}/sku', array('as' => 'site.sku', 'uses' => 'HomeController@skuNew'))->defaults('expirados', false);
            Route::get('{product}/sku/expirados', array('as' => 'site.sku.expirados', 'uses' => 'HomeController@skuNew'))->defaults('expirados', true);
            Route::get('{product}/sku/promocional', array('as' => 'site.sku.promo', 'uses' => 'HomeController@sku'));
            Route::get('{product}/sku/modal/{id}', array('as' => 'site.sku.modal', 'uses' => 'ServicesController@loadSkuModal'));
        });
    });
});
