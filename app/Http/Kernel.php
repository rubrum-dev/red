<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        //\Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        Middleware\Manutencao::class,
        \GeneaLabs\LaravelCaffeine\Http\Middleware\LaravelCaffeineDripMiddleware::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
        ],

        'api' => [
            'throttle:60,1',
        ],
        'packshelf' => [
            \App\Http\Middleware\PackshelfVerify::class,
        ],
        'artwork' => [
            \App\Http\Middleware\ArtworkVerify::class,
        ],
        'adsmart' => [
            \App\Http\Middleware\AdsmartVerify::class,
        ],
        'adsmartCriarCampanha' => [
            \App\Http\Middleware\AdsmartCriarCampanhaVerify::class,
        ],
        'adsmartCriarMidia' => [
            \App\Http\Middleware\AdsmartCriarMidiaVerify::class,
        ],
        'adsmartHistorico' => [
            \App\Http\Middleware\AdsmartHistoricoVerify::class,
        ],
        'artworkEnviarArte' => [
            \App\Http\Middleware\ArtworkEnviarArteVerify::class,
        ],
        'artworkFornecedorPdf' => [
            \App\Http\Middleware\ArtworkFornecedorPdfVerify::class,
        ],
        'artworkAbrirTicket' => [
            \App\Http\Middleware\ArtworkAbrirTicketVerify::class,
        ],
        'artworkCriarEmbalagem' => [
            \App\Http\Middleware\ArtworkCriarEmbalagemVerify::class,
        ],
        'workflow' => [
            \App\Http\Middleware\WorkflowVerify::class,
        ],
        'techdraw' => [
            \App\Http\Middleware\TechdrawVerify::class,
        ],
        'techdrawCriarPlanta' => [
            \App\Http\Middleware\TechdrawCriarPlantaVerify::class,
        ],
        'gerenciaUsuario' => [
            \App\Http\Middleware\GerenciaUsuarioVerify::class,
        ],
        'gerenciaFornecedor' => [
            \App\Http\Middleware\GerenciaFornecedorVerify::class,
        ],
        'gerenciaEmbalagem' => [
            \App\Http\Middleware\GerenciaEmbalagemVerify::class,
        ],
        'gerenciaFoto' => [
            \App\Http\Middleware\GerenciaFotoVerify::class,
        ],
        'gerenciaMidia' => [
            \App\Http\Middleware\GerenciaMidiaVerify::class,
        ],
        
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'externo' => \App\Http\Middleware\Externo::class,
        'token' => \App\Http\Middleware\Token::class,
        'tokenArteDownload' => \App\Http\Middleware\TokenArteDownload::class,
        'first' => \App\Http\Middleware\FirstAccess::class,
        'auth' => \App\Http\Middleware\Authenticate::class,
        'admin' => \App\Http\Middleware\AdminVerify::class,
        'download' => \App\Http\Middleware\VerifyPermissionDownload::class,
        'pass' => \App\Http\Middleware\ChangePassVerify::class,
        'manager' => \App\Http\Middleware\CreateUserFront::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
    ];
}
