<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class CreateSku extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_produto' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'id_produto.required' => 'Selecione o produto.',
        ];
    }

    protected function formatErrors(Validator $validator)
    {
        return ['error' => true, 'message' => 'Verifique os campos preenchidos.', 'campos' => $validator->errors()->all()];
    }
}
