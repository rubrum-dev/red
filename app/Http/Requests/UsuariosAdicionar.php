<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;

class UsuariosAdicionar extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required',
            'sobrenome' => 'required',
            'email' => 'required',
            'email' => 'unique:adm_usuarios,email',
            
            'perfis' => 'required',
            'perfis.*' => 'required',
            
            'permissao' => 'required',
            'permissao.*' => 'required',
            
            'id_adm_empresa' => 'required',
            'empresa_nome' => 'required_if:id_adm_empresa,==,0',
            
            'id_departamento' => 'required',
            'departamento_nome' => 'required_if:id_departamento,==,0',
            
            //'id_cargo' => 'required',
            'cargo_nome' => 'required_if:id_cargo,==,0',
        ];
    }
    
    public function messages()
    {
        return [
            'nome.required' => 'Nome é obrigatório',
            'sobrenome.required' => 'Sobrenome é obrigatório',
            'email.required' => 'E-mail é obrigatório',
            'email.unique' => 'O e-mail informado já se encontra cadastrado no sistema.',
            
            'perfis.*.required' => 'Adicione o Perfil de Acesso clicando em Adicionar.',
            'permissao.*.required' => 'Marque ao menos uma Permissão aos Módulos.',
            'perfis.required' => 'Adicione o Perfil de Acesso clicando em Adicionar.',
            'permissao.required' => 'Marque ao menos uma Permissão aos Módulos.',
            
            'id_adm_empresa.required' => 'Empresa é obrigatório',
            'empresa_nome.required_if' => 'Digite o nome da Nova Empresa.',
            
            'id_departamento.required' => 'Departamento é obrigatório',
            'nome_departamento.required_if' => 'Digite o nome do Novo Departamento.',
            
            'id_cargo.required' => 'Cargo é obrigatório.',
            'nome_cargo.required_if' => 'Digite o nome do Novo Cargo.',
            
        ];
    }
}
