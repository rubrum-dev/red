<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PostFornecedorEditarForm extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required',
            //'telefone' => 'required',
            'status' => 'required',
            'nomes.*' => 'required',
            //'emails.*' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nome.required' => 'Razão Social é obrigatório',
            'nome.unique' => 'Já existe um Fornecedor com esse nome',
            //'telefone.required' => 'Telefone é obrigatório',
            'status.required' => 'Status é obrigatório',

            'nomes.*.required' => 'Adicione ao menos um nome e e-mail',
            //'emails.*.required' => 'Adicione ao menos um nome e e-mail'
        ];
    }

}
