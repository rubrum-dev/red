<?php

namespace App\Http\Requests\Artwork;

use App\Http\Requests\Request;

class PostTicketEditDenyForm extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cbAlterationType.*' => 'required',
            'txtDescriptionSpec.*' => 'required',
            'cbAlterationType' => 'required_without: cbAlterationType.*',
            'txtDescriptionSpec' => 'required_without: txtDescriptionSpec.*',
            'artworkFileUpload.*' => 'file',
            'tfArtworkFileLegend.*' => 'required_with:artworkFileUpload.*',
        ];
    }

    public function messages()
    {
        return [
            'cbAlterationType.*.required' => 'Tipo de alteração é obrigatório',
            'txtDescriptionSpec.*.required' => 'Descrição específica é obrigatório',

            'tfArtworkFileLegend.*.required_with' => 'Legenda do arquivo é obrigatório',

            'cbAlterationType.required_without' => 'Tipo de alteração é obrigatório',
            'txtDescriptionSpec.required_without' => 'Descrição específica é obrigatório',
        ];
    }

}
