<?php

namespace App\Http\Requests\Artwork;

use App\Http\Requests\Request;

class PostPackageNew extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_variacao' => 'required',
            'novoItemTipo' => 'required',
            'novoItemTipoAdicionar' => 'required_if:novoItemTipo,0',
            'editarItemTipo' => 'required_with:id_item_edicao',
            'editarItemTipoAdicionar' => 'required_if:editarItemTipo,0',
        ];
    }
    
    public function messages()
    {
        return [
            'id_variacao.required' => 'Erro na solicitação!',
            
            'novoItemTipo.required' => 'Selecione o nome do novo item',
            
            'novoItemTipoAdicionar.required_if' => 'Dê um nome ao novo item',
            
            'editarItemTipo.required_with' => 'Selecione o nome do item existente',
            
            'editarItemTipoAdicionar.required_if' => 'Dê um nome ao novo item existente',
        ];
    }
}
