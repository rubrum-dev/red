<?php

namespace App\Http\Requests\Artwork;

use App\Http\Requests\Request;

class PostTicketCreateForm extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtMainDescription' => 'required',
            'tfStartDate' => 'required|date_format:d/m/Y',
            'tfArtSendDate' => 'required|date_format:d/m/Y',
            'tfDeadlineDate' => 'required|date_format:d/m/Y',
            'cbAlterationType.*' => 'required',
            'txtDescriptionSpec.*' => 'required',
            'cbAlterationType' => 'required_without: cbAlterationType.*',
            'txtDescriptionSpec' => 'required_without: txtDescriptionSpec.*',
            'artworkFileUpload.*' => 'file',
            'tfArtworkFileLegend.*' => 'required_with:artworkFileUpload.*',
        ];
    }

    public function messages()
    {
        return [
            'txtMainDescription.required' => 'Descrição geral é obrigatório',
            
            'tfStartDate.required' => 'Data de início é obrigatório',
            'tfStartDate.date_format' => 'Data de início incorreta',
            
            'tfArtSendDate.required' => 'Data de Arte enviada é obrigatório',
            'tfArtSendDate.date_format' => 'Data de Arte enviada incorreta',
            
            'tfDeadlineDate.required' => 'Prazo final é obrigatório',
            'tfDeadlineDate.date_format' => 'Prazo final incorreto',
            
            'cbAlterationType.*.required' => 'Tipo de alteração é obrigatório',
            'txtDescriptionSpec.*.required' => 'Descrição específica é obrigatório',
            
            'tfArtworkFileLegend.*.required_with' => 'Legenda do arquivo é obrigatório',
            
            'cbAlterationType.required_without' => 'Tipo de alteração é obrigatório',
            'txtDescriptionSpec.required_without' => 'Descrição específica é obrigatório',
        ];
    }

}