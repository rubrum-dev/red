<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserConfigracao extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required',
            'sobrenome' => 'required',
            //'id_departamento' => 'required',
            'nome_departamento' => 'required_if:id_departamento,==,0',
            //'id_cargo' => 'required',
            'nome_cargo' => 'required_if:id_cargo,==,0',
            'password' => 'required_with:old_password|min:6|confirmed',
            'password_confirmation' => 'required_with:password|min:6'
        ];
    }

    public function messages()
    {
        return [
            'nome.required' => 'Nome é obrigatório',
            'sobrenome.required' => 'Sobrenome é obrigatório',
            'id_departamento.required' => 'Departamento é obrigatório',
            'nome_departamento.required_if' => 'Nome do departamento é obrigatório',
            'id_cargo.required' => 'Cargo é obrigatório',
            'nome_cargo.required_if' => 'Nome do cargo é obrigatório',
            
            'password.required_with' => 'Digite a nova senha.',
            'password.min' => 'A senha deve conter no mínimo 6 caracteres.',
            'password.confirmed' => 'Novas senhas digitadas não conferem.',
            'password_confirmation.required_with' => 'Digite a confirmação da nova senha.',
            'password_confirmation.min' => 'Confirmação deve conter no mínimo 6 caracteres.'
        ];
    }
}
