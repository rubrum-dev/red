<?php

// Techdraw - tipos
Breadcrumbs::register('techdraw.tipos', function ($breadcrumbs) {
    
    $breadcrumbs->push('TechDraw', route('site.techdraw.technicalPlan'));
    
});

Breadcrumbs::register('techdraw.nova', function ($breadcrumbs) {

    $breadcrumbs->parent('techdraw.tipos');

    $breadcrumbs->push('Nova Planta Técnica', route('site.techdraw.technicalPlan.create.get'));
});

Breadcrumbs::register('techdraw.plantas', function ($breadcrumbs, $id_tipo) {
    
    $itemTipo = \App\TiposEmbalagens::find($id_tipo);
    
    $breadcrumbs->parent('techdraw.tipos');

    $breadcrumbs->push($itemTipo->nome, route('site.techdraw.technicalPlan.package', $id_tipo));
    
});

Breadcrumbs::register('techdraw.editar', function ($breadcrumbs, $id_variacao) {
    
    $variacao = \App\TdVariacoes::find($id_variacao);
    
    $breadcrumbs->parent('techdraw.plantas', $variacao->techdraw->dimensao->id_tipo_embalagem);

    $breadcrumbs->push('Editar Planta Técnica', route('site.techdraw.technicalPlan.edit.get', $id_variacao));
});

// AdSmart
Breadcrumbs::register('adsmart.tipos_midia', function ($breadcrumbs) {
    
    $breadcrumbs->push('Tipos de Mídia', route('site.adsmart.tipos_midia'));
    
});

Breadcrumbs::register('adsmart.tipos_midia_criar', function ($breadcrumbs) {
    
    $breadcrumbs->parent('adsmart.tipos_midia');
    $breadcrumbs->push('Adicionar', route('site.adsmart.tipos_midia.criar'));
    
});

Breadcrumbs::register('adsmart.tipos_midia_alterar', function ($breadcrumbs) {
    
    $breadcrumbs->parent('adsmart.tipos_midia');
    $breadcrumbs->push('Alterar', route('site.adsmart.tipos_midia.editar', 0));
    
});

Breadcrumbs::register('adsmart.aprovadores_midia', function ($breadcrumbs) {
    
    $breadcrumbs->push('Aprovadores de Mídia', route('site.adsmart.aprovadores_midia'));
    
});

Breadcrumbs::register('adsmart.aprovadores_midia_criar', function ($breadcrumbs) {
    
    $breadcrumbs->parent('adsmart.aprovadores_midia');
    $breadcrumbs->push('Adicionar', route('site.adsmart.aprovadores_midia.criar'));
    
});

Breadcrumbs::register('adsmart.aprovadores_midia_alterar', function ($breadcrumbs) {
    
    $breadcrumbs->parent('adsmart.aprovadores_midia');
    $breadcrumbs->push('Alterar', route('site.adsmart.aprovadores_midia.editar', 0));
    
});

Breadcrumbs::register('adsmart.condicoes_midia', function ($breadcrumbs) {
    
    $breadcrumbs->push('Critérios de Avaliação', route('site.adsmart.condicoes_midia'));
    
});

Breadcrumbs::register('adsmart.condicoes_midia_criar', function ($breadcrumbs) {
    
    $breadcrumbs->parent('adsmart.condicoes_midia');
    $breadcrumbs->push('Adicionar', route('site.adsmart.aprovadores_midia.criar'));
    
});

Breadcrumbs::register('adsmart.condicoes_midia_alterar', function ($breadcrumbs) {
    
    $breadcrumbs->parent('adsmart.condicoes_midia');
    $breadcrumbs->push('Alterar', route('site.adsmart.aprovadores_midia.editar', 0));
    
});




Breadcrumbs::register('adsmart.familias', function ($breadcrumbs) {
    
    $breadcrumbs->push('AdSmart', route('site.adsmart.familias'));
    
});

Breadcrumbs::register('adsmart.campanhas', function ($breadcrumbs, $id_familia) {

    $familia = App\Familias::find($id_familia);

    $breadcrumbs->parent('adsmart.familias');

    $breadcrumbs->push($familia->nome, route('site.adsmart.campanhas', [$id_familia]));
});

Breadcrumbs::register('adsmart.midias', function ($breadcrumbs, $id_familia, $id_campanha) {

    $campanha = App\AdsmartCampanhas::find($id_campanha);

    $breadcrumbs->parent('adsmart.campanhas', $id_familia);

    $breadcrumbs->push($campanha->nome, route('site.adsmart.midias', [$id_familia, $id_campanha]));
});

Breadcrumbs::register('adsmart.midias.criar', function ($breadcrumbs, $id_familia, $id_campanha) {

    $breadcrumbs->parent('adsmart.midias', $id_familia, $id_campanha);

    $breadcrumbs->push('Nova mídia', route('site.adsmart.midias', [$id_familia, $id_campanha]));
});

Breadcrumbs::register('adsmart.campanha_criar', function ($breadcrumbs) {
    
    $breadcrumbs->parent('adsmart.familias');

    $breadcrumbs->push('Nova Campanha', route('site.adsmart.campanha.criar.get'));
    
});

Breadcrumbs::register('adsmart.ticket.criar', function ($breadcrumbs, $id_familia, $id_campanha) {

    if ($id_familia && $id_campanha)
    {
        $breadcrumbs->parent('adsmart.midias', $id_familia, $id_campanha);

        $breadcrumbs->push('Abrir Ticket', route('site.adsmart.midias', [$id_familia, $id_campanha]));
    }
    
});

Breadcrumbs::register('adsmart.historico', function ($breadcrumbs, $id_familia, $id_campanha) {

    if ($id_familia && $id_campanha)
    {
        $breadcrumbs->parent('adsmart.midias', $id_familia, $id_campanha);

        $breadcrumbs->push('Histórico', route('site.adsmart.midias', [$id_familia, $id_campanha]));
    }
    
});

// Artwork
Breadcrumbs::register('workflow.familias', function ($breadcrumbs) {
    
    $breadcrumbs->push('WorkFlow', route('site.artwork.manager', ['minhas_tarefas']));
    
});

Breadcrumbs::register('artwork.familias', function ($breadcrumbs) {
    
    $breadcrumbs->push('ArtWork', route('site.artwork.family'));
    
});

Breadcrumbs::register('artwork.produtos', function ($breadcrumbs, $id_familia) {
    $familia = App\Familias::find($id_familia);

    $breadcrumbs->parent('artwork.familias');

    $breadcrumbs->push($familia->nome, route('site.artwork.product', [$id_familia]));
});

Breadcrumbs::register('artwork.embalagens', function ($breadcrumbs, $id_familia, $id_produto) {
    $produto = App\Produtos::find($id_produto);

    $breadcrumbs->parent('artwork.produtos', $id_familia);

    $breadcrumbs->push($produto->nome, route('site.artwork.package', [$id_familia, $id_produto]));
});

Breadcrumbs::register('artwork.historico', function ($breadcrumbs, $id_variacao) {
    
    $variacao = App\ArtwVariacoes::find($id_variacao);

    $id_produto = $variacao->sku->produtos->id;
    $id_familia = $variacao->sku->produtos->id_familia;

    $breadcrumbs->parent('artwork.embalagens', $id_familia, $id_produto);

    $breadcrumbs->push('Histórico');
});

Breadcrumbs::register('artwork.enviar_arte', function ($breadcrumbs, $id_variacao) {
    
    $variacao = App\ArtwVariacoes::find($id_variacao);

    $id_produto = $variacao->sku->produtos->id;
    $id_familia = $variacao->sku->produtos->id_familia;

    $breadcrumbs->parent('artwork.embalagens', $id_familia, $id_produto);

    $breadcrumbs->push('Enviar Arte');
});

Breadcrumbs::register('artwork.fornecedor_pdf', function ($breadcrumbs, $id_variacao) {
    
    $variacao = App\ArtwVariacoes::find($id_variacao);

    $id_produto = $variacao->sku->produtos->id;
    $id_familia = $variacao->sku->produtos->id_familia;

    $breadcrumbs->parent('artwork.embalagens', $id_familia, $id_produto);

    $breadcrumbs->push('Anexar PDF do Fornecedor');
});

Breadcrumbs::register('artwork.versoes_anteriores', function ($breadcrumbs, $id_versao) {
    
    $versao = App\ArtwVersoes::find($id_versao);

    $id_produto = $versao->artwItem->artwVariacao->sku->produtos->id;
    $id_familia = $versao->artwItem->artwVariacao->sku->produtos->id_familia;

    $breadcrumbs->parent('artwork.embalagens', $id_familia, $id_produto);

    $breadcrumbs->push('Versões Anteriores');
    
    /*
    
    $variacao = App\ArtwVariacoes::find($id_variacao);

    $id_produto = $variacao->sku->produtos->id;
    $id_familia = $variacao->sku->produtos->id_familia;
    $id_categoria = $variacao->sku->produtos->id_categoria_produto;

    $breadcrumbs->parent('artwork.embalagens', $id_categoria, $id_familia, $id_produto);

    $breadcrumbs->push('Versões Anteriores');
     * 
     * 
     * 
     */
});

Breadcrumbs::register('artwork.solicitar_alteracao', function ($breadcrumbs, $id_variacao) {
    $variacao = App\ArtwVariacoes::withTrashed()->find($id_variacao);

    $id_produto = $variacao->sku->produtos->id;
    $id_familia = $variacao->sku->produtos->id_familia;
    $id_categoria = $variacao->sku->produtos->id_categoria_produto;

    $breadcrumbs->parent('artwork.embalagens', $id_familia, $id_produto);

    $breadcrumbs->push('Abrir Ticket');
});

Breadcrumbs::register('artwork.manager', function ($breadcrumbs) {
    //$breadcrumbs->parent('workflow.familias');

    $breadcrumbs->push('WorkFlow', route('site.artwork.managerV4',['minhas_tarefas']));
});

Breadcrumbs::register('artwork.nova_embalagem', function ($breadcrumbs) {
    $breadcrumbs->parent('artwork.familias');

    $breadcrumbs->push('Nova Embalagem', route('site.artwork.package.create.get'));
});

Breadcrumbs::register('artwork.ticket', function ($breadcrumbs, $id_ticket) {
    $ticket = App\ArtwTickets::find($id_ticket);

    $breadcrumbs->parent('artwork.manager');

    if ($ticket->modulo == 1)
    {
        $breadcrumbs->push('Ticket ' . $ticket->numero, route('site.artwork.ticket.edit.step1.get', [$ticket->id]));
    } else {
        $breadcrumbs->push('Ticket ' . $ticket->numero, route('site.adsmart.ticket', [$ticket->numero]));
    }

    

});

Breadcrumbs::register('artwork.ticket_reprovar', function ($breadcrumbs, $id_ticket) {
    $breadcrumbs->parent('artwork.ticket', $id_ticket);

    $breadcrumbs->push('Reprovar');
});

Breadcrumbs::register('artwork.ticket_editar', function ($breadcrumbs, $id_ticket) {
    $breadcrumbs->parent('artwork.ticket', $id_ticket);

    $breadcrumbs->push('Editar');
});

// Famílias de Produtos
Breadcrumbs::register('familias', function ($breadcrumbs) {
    if (!Request::route('guide')) {
        $modulo = Request::route('modulo');
    } else {
        $modulo = (Request::route('guide') == 'versoes') ? 'guide' : 'ads';
    }

    switch ($modulo) {
        case 'guide':
            $breadcrumbs->push('Guide', route('site.index', [$modulo]));
            break;
        case 'ads':
            $breadcrumbs->push('Ads', route('site.index', [$modulo]));
            break;
        default:
            $breadcrumbs->push('PackShelf', route('site.index', [$modulo]));
            break;
    }
});

// Famílias de Produtos > Favoritos
Breadcrumbs::register('busca', function ($breadcrumbs) {
    $breadcrumbs->parent('familias');
    $breadcrumbs->push('Busca');
});

// Famílias de Produtos > Saiba Mais
Breadcrumbs::register('about', function ($breadcrumbs) {
    $breadcrumbs->parent('familias');
    $breadcrumbs->push('Saiba Mais');
});

// Famílias de Produtos > Suporte
Breadcrumbs::register('suport', function ($breadcrumbs) {
    $breadcrumbs->parent('familias');
    $breadcrumbs->push('Suporte');
});

// Famílias de Produtos > Favoritos
Breadcrumbs::register('favoritos', function ($breadcrumbs) {
    $breadcrumbs->parent('familias');
    $breadcrumbs->push('Favoritos');
});

// Famílias de Produtos > Recentes
Breadcrumbs::register('recentes', function ($breadcrumbs) {
    $breadcrumbs->parent('familias');
    $breadcrumbs->push('Recentes');
});

Breadcrumbs::register('fornecedor.gerenciar', function ($breadcrumbs) {
    //$breadcrumbs->parent('familias');
    $breadcrumbs->push('Gerenciador de Fornecedores', route('site.fornecedores.get'));
});

Breadcrumbs::register('fornecedor.adicionar', function ($breadcrumbs) {
    $breadcrumbs->parent('fornecedor.gerenciar');
    $breadcrumbs->push('Novo Fornecedor');
});

Breadcrumbs::register('fornecedor.editar', function ($breadcrumbs) {
    $breadcrumbs->parent('fornecedor.gerenciar');
    $breadcrumbs->push('Editar Fornecedor');
});

// Configurações da Conta
Breadcrumbs::register('user', function ($breadcrumbs) {
    //$breadcrumbs->parent('familias');
    $breadcrumbs->push('Configurações da Conta');
});

// Gerenciamento de Tokens
Breadcrumbs::register('token', function ($breadcrumbs) {
    //$breadcrumbs->parent('familias');
    $breadcrumbs->push('Gerenciamento de Tokens');
});

// Famílias de Produtos > Produtos
Breadcrumbs::register('produtos', function ($breadcrumbs, $produtos) {
    $familia = App\Familias::find($produtos);
    $breadcrumbs->parent('familias');
    $family = !is_null($familia) ? $familia->id : 1;
    $breadcrumbs->push($familia->nome, route('site.product', $family));
});

// Produtos > SKU - Parent
Breadcrumbs::register('parent', function ($breadcrumbs, $produtos) {
    if (!Request::route('guide')) {
        $modulo = Request::route('modulo');
    } else {
        $modulo = (Request::route('guide') == 'versoes') ? 'guide' : 'ads';
    }

    $produto = App\Produtos::find($produtos);
    $breadcrumbs->parent('familias');
    $family = !is_null($produto) ? $produto->id_familia : 1;
    $breadcrumbs->push($produto->familias->nome, route('site.product', [$family, $modulo]));
});

// Famílias de Produtos > Produtos > SKU
Breadcrumbs::register('skus', function ($breadcrumbs, $skus) {
    $produto = App\Produtos::find($skus);
    $breadcrumbs->parent('parent', $skus);
    $breadcrumbs->push($produto->nome);
});

// Famílias de Produtos > Produtos > SKU Promocional
Breadcrumbs::register('promo', function ($breadcrumbs, $promos) {
    $produto = App\Produtos::find($promos);
    $breadcrumbs->parent('parent', $promos);
    $breadcrumbs->push($produto->nome);
});

// Famílias de Produtos > Produtos > Guide [Page]
Breadcrumbs::register('guides', function ($breadcrumbs, $page) {
    $produto = App\Produtos::find($page['id_prod']);
    $breadcrumbs->parent('parent', $page['id_prod']);
    $breadcrumbs->push($produto->nome);
});
