<?php

namespace App\Http\Middleware;

use Closure;
use App\Components\SiteCommon;

class VerifyPermissionDownload
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $checkUserRelations = new SiteCommon;
        $userPermissions = $checkUserRelations->checkUserRelations();

        // Verifica permissão do usuário para o módulo Guide (1), se não, direciona para tela de Login ou Home
        if(in_array($request->page, ['versoes', 'icones'])){
            if(!$userPermissions || (!in_array($userPermissions->id_adm_perfil, [1, 2]) && (!in_array(1, $userPermissions->modulos)))){
                if($request->page != 'versoes'){
                    if ($request->ajax() || $request->wantsJson()) {
                        return response('Unauthorized.', 401);
                    } else {
                        return redirect()->guest('/admin/user');
                    }                    
                }
            }
        }

        // Verifica permissão do usuário para o módulo Enxoval (4), se não, direciona para tela de Login ou Home
        if($request->page == 'enxovais'){
            if(!$userPermissions || (!in_array($userPermissions->id_adm_perfil, [1, 2]) && (!in_array(4, $userPermissions->modulos)))){
                if ($request->ajax() || $request->wantsJson()) {
                    return response('Unauthorized.', 401);
                } else {
                    return redirect()->guest('/admin/user');
                }
            }
        }

        // Verifica permissão do usuário para o módulo SKU File (2), se não, direciona para tela de Login ou Home
        if($request->page == 'artes-finais' && (in_array($request->type , ['zip', 'pdf']))){
            if(!$userPermissions || (!in_array($userPermissions->id_adm_perfil, [1, 2]) && (!in_array(2, $userPermissions->modulos)))){
                if ($request->ajax() || $request->wantsJson()) {
                    return response('Unauthorized.', 401);
                } else {
                    return redirect()->guest('/admin/user');
                }
            }
        }

        return $next($request);
    }
}
