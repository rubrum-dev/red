<?php
namespace App\Http\Middleware;

use Closure;
use App\ArtwArtesEnviosEmails;

class TokenArteDownload
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->input('token')) {

            abort('404');
        }

        $token = ArtwArtesEnviosEmails
            ::where('token', $request->input('token'))
            ->whereNull('artw_artes_envios.deleted_at')
            ->join('artw_artes_envios', 'artw_artes_envios.id', '=', 'artw_artes_envios_emails.id_arte_envio')
            ->get()
            ->first();

        if (!$token) {
            abort('403');
        }

        if ($token->cancelado_em) {
            return view('errors.link_cancelado', compact('titulo', 'texto'));
        }

        if ($token->status == 0) {
            //return view('errors.link_inativo', compact('titulo', 'texto'));
        }

        if ($token->dt_expiracao->format('Y-m-d H:i:s') < date('Y-m-d H:i:s')) {
            //return view('errors.link_expirado', compact('titulo', 'texto'));
        }

        return $next($request);
    }
}
