<?php

namespace App\Http\Middleware;

use Closure;
use App\AdmTokensGuides;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Token
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::guard($guard)->guest()){
            return $next($request);
        }

        if($request->route()->getName() == 'site.guide' && $request['token'] != null) {
            $tokenModel = new AdmTokensGuides;
            $token = $tokenModel->where('token', $request['token'])->get();

            if(count($token) > 0 && $token->first()->dt_expiracao > date("Y-m-d H:i:s"))
                return $next($request);

            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('/');
            }
        }

        return $next($request);
    }
}
