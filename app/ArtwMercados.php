<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwMercados extends Model
{

    protected $fillable = [
        'nome'
    ];
    
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
    
}
