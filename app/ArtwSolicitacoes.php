<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwSolicitacoes extends Model
{
    protected $fillable = [
        'id_ciclo',
        'id_tipo',
        'id_status',
        'id_usuario',
        'descricao'
    ];

    public function artwCiclo()
    {
        return $this->belongsTo('App\ArtwCiclos', 'id_ciclo');
    }

    public function artwSolicitacaoTipo()
    {
        return $this->belongsTo('App\ArtwSolicitacaoTipos', 'id_tipo');
    }

    public function adsmartCondicaoMidia()
    {
        return $this->belongsTo('App\AdsmartCondicoesMidia', 'id_condicao_midia');
    }

    public function artwSolicitacaoStatus()
    {
        return $this->belongsTo('App\ArtwSolicitacaoStatus', 'id_status');
    }

    public function admUsuario()
    {
        return $this->belongsTo('App\AdmUsuarios', 'id_usuario')->withTrashed();
    }

    public function artwComentarios()
    {
        return $this->hasMany('App\ArtwComentarios', 'id_solicitacao');
    }

    public function artwSolicitacoesStatus()
    {
        return $this->hasMany('App\ArtwSolicitacoesStatus', 'id_solicitacao');
    }
    
    public function getDescricaoHtmlAttribute()
    {
        return nl2br($this->descricao);
    }
    
    public function getDescricaoAttribute($value)
    {
        return str_replace('<br />', '', $value);
    }

}
