<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsosIncorretos extends Model
{
	protected $fillable = array(
		'id_tipo_uso_incorreto',
		'thumb',		
		'descricao',
		'status'
	);

	public function produtos()
	{
		return $this->belongsToMany('App\Produtos', 'produtos_usos_incorretos', 'id_uso_incorreto', 'id_produto');
	}

    public function tiposUsosIncorretos()
    {
    	return $this->belongsTo('App\TiposUsosIncorretos', 'id_tipo_uso_incorreto');
    }
}
