<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\TiposUsosAplicacoes
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UsosAplicacoes[] $usosAplicacoes
 * @mixin \Eloquent
 */
class TiposUsosAplicacoes extends Model
{
    public function usosAplicacoes()
    {
    	return $this->hasMany('App\UsosAplicacoes', 'id_tipo_uso_aplicacao');
    }
}
