<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmUsuariosModulos extends Model
{
    protected $fillable = array(
        'id_adm_usuario',
        'id_adm_modulo',
    );

    public $timestamps = false;
    
}
