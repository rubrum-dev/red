<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdsmartMidiaTipos extends Model
{
    protected $fillable = [
        'nome'
    ];

    public function aprovadores()
    {
        return $this->belongsToMany('App\AdsmartAprovadoresMidia', 'adsmart_midia_tipos_aprovadores', 'id_midia_tipo', 'id_aprovador_midia');
    }
}
