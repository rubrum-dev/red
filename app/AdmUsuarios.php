<?php
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Wildside\Userstamps\Userstamps;
use Carbon\Carbon;

class AdmUsuarios extends Authenticatable
{

    use SoftDeletes;
    use Userstamps;

    protected $fillable = array(
        'id_adm_perfil',
        'id_adm_empresa',
        'id_cargo',
        'id_departamento',
        'nome',
        'sobrenome',
        'email',
        'senha',
        'fone',
        'cargo',
        'depto',
        'status',
        'remember_token',
        'primeiro_acesso',
        'notificacao_favoritos',
        'token_guide',
        'updated_at',
        'tem_token'
    );

    protected $dates = [
        'logged_at',
        'expirado_em',
        'arquivado_em',
    ];

    public function getAuthPassword()
    {
        return $this->senha;
    }

    public function admEmpresas()
    {
        return $this->belongsTo('App\AdmEmpresas', 'id_adm_empresa');
    }

    public function admEmpresa()
    {
        return $this->belongsTo('App\AdmEmpresas', 'id_adm_empresa');
    }

    public function admCargo()
    {
        return $this->belongsTo('App\AdmCargos', 'id_cargo');
    }

    public function admDepartamento()
    {
        return $this->belongsTo('App\AdmDepartamentos', 'id_departamento');
    }

    public function admModulos()
    {
        return $this->belongsToMany('App\AdmModulos', 'adm_usuarios_modulos', 'id_adm_usuario', 'id_adm_modulo');
    }

    public function getModulosAttribute()
    {
        $modPermitidos = [];

        if ($this->admin) {

            $modulos = \App\AdmModulos::all();
        } else {

            $modulos = $this->admModulos;
        }

        foreach ($modulos as $modulo) {
            $modPermitidos[] = $modulo->nome;
        }

        return $modPermitidos;
    }

    public function getFamiliasAttribute()
    {
        $famPermitidas = [];
        
        $todasFamilias = AdmUsuariosPerfis::where('id_usuario', $this->id)->where('id_perfil', 4)->get()->last();
        
        if ($this->admin || $todasFamilias) {

            $familias = Familias::all();
        } else {

            $perfis = [];

            foreach ($this->admPerfis as $perfil) {

                $perfis[] = $perfil->id;
            }

            $familias = Familias::select('familias.*')
                ->distinct()
                ->join('adm_perfis_familias', 'adm_perfis_familias.id_familia', '=', 'familias.id')
                ->whereIn('adm_perfis_familias.id_adm_perfil', $perfis)
                ->get();
        }

        foreach ($familias as $familia) {
            $famPermitidas[] = $familia->id;
        }

        return $famPermitidas;
    }

    public function admPerfis()
    {
        return $this->belongsToMany('App\AdmPerfis', 'adm_usuarios_perfis', 'id_usuario', 'id_perfil');
    }

    public function produtos()
    {
        return $this->belongsToMany('App\Produtos', 'adm_usuarios_produtos', 'id_adm_usuario', 'id_produto');
    }

    public function admFtps()
    {
        return $this->hasMany('App\AdmFtps', 'id_adm_usuario');
    }

    public function admTokensGuides()
    {
        return $this->hasMany('App\AdmTokensGuides', 'id_adm_usuario');
    }

    public function artwPerfisUsuariosTickets()
    {
        return $this->hasMany('App\ArtwPerfisUsuariosTickets', 'id_usuario');
    }

    public function artwNovasEmbalagens()
    {
        return $this->hasMany('App\ArtwNovasEmbalagens', 'id_usuario');
    }

    public function getNomeCompletoAttribute()
    {
        return trim($this->nome . ' ' . $this->sobrenome);
    }

    public function getNomeAbreviadoAttribute()
    {
        $partesNome = explode(' ', trim($this->nome));
        $primeiroNome = array_shift($partesNome);

        $partesSobreNome = explode(' ', trim($this->sobrenome));
        $primeiroSobreNome = array_pop($partesSobreNome);
        
        if ($this->deleted_at || $this->status != 1){
            
            $primeiroSobreNome .= '*';
            
        }
        
        return trim($primeiroNome . ' ' . $primeiroSobreNome);
    }

    public function getNomeAbreviado2Attribute()
    {
        $partesNome = explode(' ', trim($this->usuario_nome));
        $primeiroNome = array_shift($partesNome);

        $partesSobreNome = explode(' ', trim($this->usuario_sobrenome));
        $primeiroSobreNome = array_pop($partesSobreNome);
        
        if ($this->deleted_at || $this->status != 1){
            
            $primeiroSobreNome .= '*';
            
        }
        
        return trim($primeiroNome . ' ' . $primeiroSobreNome);
    }

    public function artwNotificacoesTo()
    {
        return $this->hasMany('App\ArtwNotificacoes', 'id_to');
    }

    public function getStatusNomeAttribute()
    {
        $id_status = $this->status;

        switch ($id_status) {
            case 0:
                $status = 'Inativo';
                break;
            case 1:
                $status = 'Ativo';
                break;
            default:
                break;
        }

        return $status;
    }
    
    public function exibe_news()
    {
        $ultimaNews = News::all()->last();

        $news = News
            ::select('news.*', 'news_usuarios.id as id_news_usuarios')
            ->join('news_usuarios', 'news_usuarios.id_news', '=','news.id')
            ->where('news.id', $ultimaNews->id)
            ->where('news.id_tipo', 1)
            ->where('news_usuarios.id_usuario', Auth::user()->id)
            
            ->where(function($query) {
                    $query->orwhere('news_usuarios.session_id', request()->session()->getId());
                    $query->orWhere('news_usuarios.session_id', null);
            })
            
            ->get()
            ->last();
        
        if (!$news)
        {
            $newsAtual = News::all()->last();
            
            $ler = new NewsUsuarios;
            $ler->session_id = request()->session()->getId();
            $ler->id_news = $newsAtual->id;
            $ler->id_usuario = Auth::user()->id;
            $ler->save();
            
            return true;
            
        } else {
            
            return false;
            
        }
    }
}
