<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtwVariacoesRelacionadas extends Model
{
    protected $fillable = [
        'id_variacao',
        'id_variacao_relacionada'
    ];

    public function variacao()
    {
        return $this->belongsTo('App\ArtwVariacoes', 'id_variacao_relacionada');
    }
}
