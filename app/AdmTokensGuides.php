<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmTokensGuides extends Model
{
    protected $fillable = array(
        'id_produto',
		'id_adm_usuario',
		'token',
		'dt_expiracao',
		'status'
	);

    public function produtos()
    {
    	return $this->belongsTo('App\Produtos', 'id_produto');
    }

    public function admUsuarios()
    {
    	return $this->belongsTo('App\AdmUsuarios', 'id_adm_usuario');
    }
}
