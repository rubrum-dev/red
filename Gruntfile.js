module.exports = function (grunt) {
  require('jit-grunt')(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      "public/js/admin/jquery.validate.min.js": "public/js/admin/jquery.validate.js",  // destination file and source file
      "public/js/admin/jquery.maskedinput.min.js": "public/js/admin/jquery.maskedinput.js",
      "public/js/site/jquery.maskedinput.min.js": "public/js/site/jquery.maskedinput.js",
      "public/js/site/selectize.min.js": "public/js/site/selectize.js",
      "public/js/site/jquery-confirm.min.js": "public/js/site/jquery-confirm.js",
      "public/js/site/jquery-confirm-config.min.js": "public/js/site/jquery-confirm-config.js",
      "public/js/site/jquery.adaptive-backgrounds.min.js": "public/js/site/jquery.adaptive-backgrounds.js",
      "public/js/site/jquery.adaptive-backgrounds-init.min.js": "public/js/site/jquery.adaptive-backgrounds-init.js",
      "public/js/site/login.min.js": "public/js/site/login.js",
      "public/js/site/site.min.js": "public/js/site/site.js",
      "public/js/site/my-alert.min.js": "public/js/site/my-alert.js",
      "public/js/site/my-dialog.min.js": "public/js/site/my-dialog.js",
      "public/js/site/site-news.min.js": "public/js/site/site-news.js",
      "public/js/site/slide.min.js": "public/js/site/slide.js"
    },
    less: {
      development: {
        options: {
          compress: false,
          yuicompress: true,
          optimization: 2
        },
        files: {
          "./public/css/site/cliente_1/layout.css": "public/less/themes/cliente_1/layout-helper.less", // destination file and source file
          "./public/css/site/cliente_2/layout.css": "public/less/themes/cliente_2/layout-helper.less",
          "./public/css/site/cliente_3/layout.css": "public/less/themes/cliente_3/layout-helper.less",
          "./public/css/site/cliente_4/layout.css": "public/less/themes/cliente_4/layout-helper.less",
          "./public/css/site/cliente_5/layout.css": "public/less/themes/cliente_5/layout-helper.less",
          "./public/css/site/cliente_6/layout.css": "public/less/themes/cliente_6/layout-helper.less",
          "./public/css/site/cliente_7/layout.css": "public/less/themes/cliente_7/layout-helper.less",
          "./public/css/site/cliente_8/layout.css": "public/less/themes/cliente_8/layout-helper.less",
          "./public/css/site/cliente_9/layout.css": "public/less/themes/cliente_9/layout-helper.less",
          "./public/css/site/cliente_10/layout.css": "public/less/themes/cliente_10/layout-helper.less",
          "./public/css/site/cliente_11/layout.css": "public/less/themes/cliente_11/layout-helper.less",
          "./public/css/site/cliente_12/layout.css": "public/less/themes/cliente_12/layout-helper.less",
          "./public/css/site/cliente_14/layout.css": "public/less/themes/cliente_14/layout-helper.less",
          "./public/css/site/cliente_15/layout.css": "public/less/themes/cliente_15/layout-helper.less",
          "./public/css/site/cliente_16/layout.css": "public/less/themes/cliente_16/layout-helper.less",
          "./public/css/site/cliente_17/layout.css": "public/less/themes/cliente_17/layout-helper.less",
          "./public/css/site/cliente_18/layout.css": "public/less/themes/cliente_18/layout-helper.less",
          "./public/css/site/cliente_19/layout.css": "public/less/themes/cliente_19/layout-helper.less",
          "./public/css/site/cliente_20/layout.css": "public/less/themes/cliente_20/layout-helper.less",
          "./public/css/site/cliente_21/layout.css": "public/less/themes/cliente_21/layout-helper.less",
          "./public/css/site/cliente_22/layout.css": "public/less/themes/cliente_22/layout-helper.less",
          "./public/css/site/cliente_23/layout.css": "public/less/themes/cliente_23/layout-helper.less",
          "./public/css/site/cliente_24/layout.css": "public/less/themes/cliente_24/layout-helper.less",
          "./public/css/site/cliente_25/layout.css": "public/less/themes/cliente_25/layout-helper.less",
          "./public/css/site/cliente_27/layout.css": "public/less/themes/cliente_27/layout-helper.less",
          "./public/css/site/cliente_28/layout.css": "public/less/themes/cliente_28/layout-helper.less",
          "./public/css/site/cliente_26/layout.css": "public/less/themes/cliente_26/layout-helper.less",
          "./public/css/site/cliente_13/layout.css": "public/less/themes/cliente_13/layout-helper.less",
          "./public/css/site/cliente_dev/layout.css": "public/less/themes/cliente_dev/layout-helper.less",
          "./public/css/site/cliente_stg/layout.css": "public/less/themes/cliente_stg/layout-helper.less",
          "./public/css/site/cliente_1/compare.css": "public/less/themes/cliente_1/compare-helper.less",
          "./public/css/site/cliente_2/compare.css": "public/less/themes/cliente_2/compare-helper.less",
          "./public/css/site/cliente_3/compare.css": "public/less/themes/cliente_3/compare-helper.less",
          "./public/css/site/cliente_4/compare.css": "public/less/themes/cliente_4/compare-helper.less",
          "./public/css/site/cliente_5/compare.css": "public/less/themes/cliente_5/compare-helper.less",
          "./public/css/site/cliente_6/compare.css": "public/less/themes/cliente_6/compare-helper.less",
          "./public/css/site/cliente_7/compare.css": "public/less/themes/cliente_7/compare-helper.less",
          "./public/css/site/cliente_8/compare.css": "public/less/themes/cliente_8/compare-helper.less",
          "./public/css/site/cliente_9/compare.css": "public/less/themes/cliente_9/compare-helper.less",
          "./public/css/site/cliente_10/compare.css": "public/less/themes/cliente_10/compare-helper.less",
          "./public/css/site/cliente_11/compare.css": "public/less/themes/cliente_11/compare-helper.less",
          "./public/css/site/cliente_12/compare.css": "public/less/themes/cliente_12/compare-helper.less",
          "./public/css/site/cliente_14/compare.css": "public/less/themes/cliente_14/compare-helper.less",
          "./public/css/site/cliente_15/compare.css": "public/less/themes/cliente_15/compare-helper.less",
          "./public/css/site/cliente_16/compare.css": "public/less/themes/cliente_16/compare-helper.less",
          "./public/css/site/cliente_17/compare.css": "public/less/themes/cliente_17/compare-helper.less",
          "./public/css/site/cliente_18/compare.css": "public/less/themes/cliente_18/compare-helper.less",
          "./public/css/site/cliente_19/compare.css": "public/less/themes/cliente_19/compare-helper.less",
          "./public/css/site/cliente_20/compare.css": "public/less/themes/cliente_20/compare-helper.less",
          "./public/css/site/cliente_21/compare.css": "public/less/themes/cliente_21/compare-helper.less",
          "./public/css/site/cliente_22/compare.css": "public/less/themes/cliente_22/compare-helper.less",
          "./public/css/site/cliente_23/compare.css": "public/less/themes/cliente_23/compare-helper.less",
          "./public/css/site/cliente_24/compare.css": "public/less/themes/cliente_24/compare-helper.less",
          "./public/css/site/cliente_25/compare.css": "public/less/themes/cliente_25/compare-helper.less",
          "./public/css/site/cliente_27/compare.css": "public/less/themes/cliente_27/compare-helper.less",
          "./public/css/site/cliente_28/compare.css": "public/less/themes/cliente_28/compare-helper.less",
          "./public/css/site/cliente_26/compare.css": "public/less/themes/cliente_26/compare-helper.less",
          "./public/css/site/cliente_13/compare.css": "public/less/themes/cliente_13/compare-helper.less",
          "./public/css/site/cliente_dev/compare.css": "public/less/themes/cliente_dev/compare-helper.less",
          "./public/css/site/cliente_stg/compare.css": "public/less/themes/cliente_stg/compare-helper.less",
          "./public/css/site/cliente_1/home-v2.css": "public/less/themes/cliente_1/home-helper.less",
          "./public/css/site/cliente_2/home-v2.css": "public/less/themes/cliente_2/home-helper.less",
          "./public/css/site/cliente_3/home-v2.css": "public/less/themes/cliente_3/home-helper.less",
          "./public/css/site/cliente_4/home-v2.css": "public/less/themes/cliente_4/home-helper.less",
          "./public/css/site/cliente_5/home-v2.css": "public/less/themes/cliente_5/home-helper.less",
          "./public/css/site/cliente_6/home-v2.css": "public/less/themes/cliente_6/home-helper.less",
          "./public/css/site/cliente_7/home-v2.css": "public/less/themes/cliente_7/home-helper.less",
          "./public/css/site/cliente_8/home-v2.css": "public/less/themes/cliente_8/home-helper.less",
          "./public/css/site/cliente_9/home-v2.css": "public/less/themes/cliente_9/home-helper.less",
          "./public/css/site/cliente_10/home-v2.css": "public/less/themes/cliente_10/home-helper.less",
          "./public/css/site/cliente_11/home-v2.css": "public/less/themes/cliente_11/home-helper.less",
          "./public/css/site/cliente_12/home-v2.css": "public/less/themes/cliente_12/home-helper.less",
          "./public/css/site/cliente_14/home-v2.css": "public/less/themes/cliente_14/home-helper.less",
          "./public/css/site/cliente_15/home-v2.css": "public/less/themes/cliente_15/home-helper.less",
          "./public/css/site/cliente_16/home-v2.css": "public/less/themes/cliente_16/home-helper.less",
          "./public/css/site/cliente_17/home-v2.css": "public/less/themes/cliente_17/home-helper.less",
          "./public/css/site/cliente_18/home-v2.css": "public/less/themes/cliente_18/home-helper.less",
          "./public/css/site/cliente_19/home-v2.css": "public/less/themes/cliente_19/home-helper.less",
          "./public/css/site/cliente_20/home-v2.css": "public/less/themes/cliente_20/home-helper.less",
          "./public/css/site/cliente_21/home-v2.css": "public/less/themes/cliente_21/home-helper.less",
          "./public/css/site/cliente_22/home-v2.css": "public/less/themes/cliente_22/home-helper.less",
          "./public/css/site/cliente_23/home-v2.css": "public/less/themes/cliente_23/home-helper.less",
          "./public/css/site/cliente_24/home-v2.css": "public/less/themes/cliente_24/home-helper.less",
          "./public/css/site/cliente_25/home-v2.css": "public/less/themes/cliente_25/home-helper.less",
          "./public/css/site/cliente_27/home-v2.css": "public/less/themes/cliente_27/home-helper.less",
          "./public/css/site/cliente_28/home-v2.css": "public/less/themes/cliente_28/home-helper.less",
          "./public/css/site/cliente_26/home-v2.css": "public/less/themes/cliente_26/home-helper.less",
          "./public/css/site/cliente_13/home-v2.css": "public/less/themes/cliente_13/home-helper.less",
          "./public/css/site/cliente_dev/home-v2.css": "public/less/themes/cliente_dev/home-helper.less",
          "./public/css/site/cliente_stg/home-v2.css": "public/less/themes/cliente_stg/home-helper.less",
          "./public/css/site/cliente_1/admin.css": "public/less/themes/cliente_1/admin-helper.less",
          "./public/css/site/cliente_2/admin.css": "public/less/themes/cliente_2/admin-helper.less",
          "./public/css/site/cliente_3/admin.css": "public/less/themes/cliente_3/admin-helper.less",
          "./public/css/site/cliente_4/admin.css": "public/less/themes/cliente_4/admin-helper.less",
          "./public/css/site/cliente_5/admin.css": "public/less/themes/cliente_5/admin-helper.less",
          "./public/css/site/cliente_6/admin.css": "public/less/themes/cliente_6/admin-helper.less",
          "./public/css/site/cliente_7/admin.css": "public/less/themes/cliente_7/admin-helper.less",
          "./public/css/site/cliente_8/admin.css": "public/less/themes/cliente_8/admin-helper.less",
          "./public/css/site/cliente_9/admin.css": "public/less/themes/cliente_9/admin-helper.less",
          "./public/css/site/cliente_10/admin.css": "public/less/themes/cliente_10/admin-helper.less",
          "./public/css/site/cliente_11/admin.css": "public/less/themes/cliente_11/admin-helper.less",
          "./public/css/site/cliente_12/admin.css": "public/less/themes/cliente_12/admin-helper.less",
          "./public/css/site/cliente_14/admin.css": "public/less/themes/cliente_14/admin-helper.less",
          "./public/css/site/cliente_15/admin.css": "public/less/themes/cliente_15/admin-helper.less",
          "./public/css/site/cliente_16/admin.css": "public/less/themes/cliente_16/admin-helper.less",
          "./public/css/site/cliente_13/admin.css": "public/less/themes/cliente_13/admin-helper.less",
          "./public/css/site/cliente_17/admin.css": "public/less/themes/cliente_17/admin-helper.less",
          "./public/css/site/cliente_18/admin.css": "public/less/themes/cliente_18/admin-helper.less",
          "./public/css/site/cliente_19/admin.css": "public/less/themes/cliente_19/admin-helper.less",
          "./public/css/site/cliente_20/admin.css": "public/less/themes/cliente_20/admin-helper.less",
          "./public/css/site/cliente_21/admin.css": "public/less/themes/cliente_21/admin-helper.less",
          "./public/css/site/cliente_22/admin.css": "public/less/themes/cliente_22/admin-helper.less",
          "./public/css/site/cliente_23/admin.css": "public/less/themes/cliente_23/admin-helper.less",
          "./public/css/site/cliente_24/admin.css": "public/less/themes/cliente_24/admin-helper.less",
          "./public/css/site/cliente_25/admin.css": "public/less/themes/cliente_25/admin-helper.less",
          "./public/css/site/cliente_27/admin.css": "public/less/themes/cliente_27/admin-helper.less",
          "./public/css/site/cliente_28/admin.css": "public/less/themes/cliente_28/admin-helper.less",
          "./public/css/site/cliente_26/admin.css": "public/less/themes/cliente_26/admin-helper.less",
          "./public/css/site/cliente_dev/admin.css": "public/less/themes/cliente_dev/admin-helper.less",
          "./public/css/site/cliente_stg/admin.css": "public/less/themes/cliente_stg/admin-helper.less",
          "./public/css/site/cliente_1/placeholder-shimmer.css": "public/less/themes/cliente_1/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_2/placeholder-shimmer.css": "public/less/themes/cliente_2/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_3/placeholder-shimmer.css": "public/less/themes/cliente_3/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_4/placeholder-shimmer.css": "public/less/themes/cliente_4/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_5/placeholder-shimmer.css": "public/less/themes/cliente_5/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_6/placeholder-shimmer.css": "public/less/themes/cliente_6/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_7/placeholder-shimmer.css": "public/less/themes/cliente_7/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_8/placeholder-shimmer.css": "public/less/themes/cliente_8/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_9/placeholder-shimmer.css": "public/less/themes/cliente_9/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_10/placeholder-shimmer.css": "public/less/themes/cliente_10/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_11/placeholder-shimmer.css": "public/less/themes/cliente_11/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_12/placeholder-shimmer.css": "public/less/themes/cliente_12/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_13/placeholder-shimmer.css": "public/less/themes/cliente_13/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_14/placeholder-shimmer.css": "public/less/themes/cliente_14/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_15/placeholder-shimmer.css": "public/less/themes/cliente_15/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_16/placeholder-shimmer.css": "public/less/themes/cliente_16/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_20/placeholder-shimmer.css": "public/less/themes/cliente_20/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_21/placeholder-shimmer.css": "public/less/themes/cliente_21/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_22/placeholder-shimmer.css": "public/less/themes/cliente_22/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_23/placeholder-shimmer.css": "public/less/themes/cliente_23/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_24/placeholder-shimmer.css": "public/less/themes/cliente_24/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_25/placeholder-shimmer.css": "public/less/themes/cliente_25/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_27/placeholder-shimmer.css": "public/less/themes/cliente_27/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_28/placeholder-shimmer.css": "public/less/themes/cliente_28/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_26/placeholder-shimmer.css": "public/less/themes/cliente_26/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_17/placeholder-shimmer.css": "public/less/themes/cliente_17/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_18/placeholder-shimmer.css": "public/less/themes/cliente_18/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_19/placeholder-shimmer.css": "public/less/themes/cliente_19/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_dev/placeholder-shimmer.css": "public/less/themes/cliente_dev/placeholder-shimmer-helper.less",
          "./public/css/site/cliente_stg/placeholder-shimmer.css": "public/less/themes/cliente_stg/placeholder-shimmer-helper.less"
        }
      }
    },
    watch: {
      styles: {
        files: [
          './public/less/placeholder-shimmer.less',
          './public/less/jquery-confirm-custom.less',
          './public/less/angular-confirm-custom.less',
          './public/less/common.less',
          './public/less/compare.less',
          './public/less/artwork.less',
          './public/less/artwork-embalagens.less',
          './public/less/packshelf.less',
          './public/less/techdraw.less',
          './public/less/workflow.less',
          './public/less/manager.less',
          './public/less/dashboard.less',
          './public/less/layout.less',
          './public/less/home-v2.less',
          './public/less/admin.less'
          // which files to watch
        ],
        tasks: ['less'],
        options: {
          nospawn: true
        }
      }
    },
    copy: {
      main: {
        files: [
          { 
            expand: true,
            cwd: './public/less/themes/cliente_1', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_1' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_2', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_2' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_3', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_3' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_4', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_4' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_5', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_5' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_6', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_6' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_7', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_7' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_8', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_8' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_9', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_9' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_10', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_10' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_11', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_11' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_12', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_12' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_14', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_14' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_15', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_15' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_16', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_16' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_13', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_13' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_17', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_17' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_18', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_18' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_19', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_19' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_20', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_20' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_21', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_21' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_22', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_22' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_23', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_23' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_24', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_24' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_25', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_25' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_27', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_27' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_26', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_26' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_28', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_28' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_dev', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_dev' 
          },
          { 
            expand: true,
            cwd: './public/less/themes/cliente_stg', 
            src: ['**/*.{png,jpg,svg}'], 
            dest:'./public/images/layout/cliente_stg' 
          }
        ]
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-uglify', 'grunt-contrib-copy');
  grunt.registerTask('default', ['less', 'watch', 'copy']);
};