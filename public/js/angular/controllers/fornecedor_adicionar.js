var site_url = '/';
var app = angular.module('app', ['720kb.tooltips']);

app.controller('FornecedorAdicionarCtrl', function ($scope)
{
    $scope.emails = emails;

    $scope.add_email = '';
    $scope.add_nome = '';

    $scope.show_load = function()
    {
        $('.lds-css').show();
    }

    $scope.hide_load = function()
    {
        $('.lds-css').hide();
    }

    $scope.adicionar = function ()
    {
        var emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if ($scope.add_email && $scope.add_nome && emailRegex.test($scope.add_email))
        {
            
            for(var k = 0; k < $scope.emails.length; k ++) {
                if( ( $scope.emails[k] && $scope.add_email === $scope.emails[k].email ) || !$scope.emails[k] ) {
                    $('.col-responsavel input').removeClass('has-error');
                    $('.col-email-artwork input').addClass('has-error');
                    myAlert('Já existe um destinatário com este e-mail.');
                    return false;
                }
            }
            
            var template = {'email': $scope.add_email, 'nome': $scope.add_nome};

            $scope.emails.push(template);

            $scope.add_email = '';
            $scope.add_nome = '';

            $('.col-responsavel input').removeClass('has-error');
            $('.col-email-artwork input').removeClass('has-error');
        } else {
            $('.col-responsavel input').addClass('has-error');
            $('.col-email-artwork input').addClass('has-error');
            myAlert('Preencha o Nome do Responsável e Email para envio de artes-finais');
            return false;
        }        
    }

    $scope.remover = function (k)
    {
        $scope.emails.splice(parseInt(k), 1);
    }

    $scope.carregar_membros = function ()
    {
        $scope.show_load();

        $.get(site_url + 'site/artwork/services/package/members', function (data)
        {
            $scope.membros = data;

            $scope.$apply();
            $scope.hide_load();

        }, 'json');
    }

    $scope.carregar_participantes = function ()
    {
        $scope.show_load();

        $.get(site_url + 'site/artwork/services/package/ticket_members/' + id_ticket, function (data)
        {
            if (data.length)
            {
                $scope.participantes = data;

                angular.forEach($scope.participantes, function (participante, key) {
                    $scope.cbMembershipProfile.push('' + participante.id_perfil + '');
                });

                $scope.$apply();
                $scope.hide_load();
            }

        }, 'json');

    }
});
