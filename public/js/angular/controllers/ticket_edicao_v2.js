var customLoadingHTML = '<div class="line-wobble"></div>';
var site_url = '/';
var app = angular.module('app', [
    'vAccordion',
    'ngAnimate',
    'ngSanitize',
    'ngRoute',
    'cp.ngConfirm',
    'selectize',
    'ngFileUpload',
    '720kb.tooltips',
    'ticketResolveLoader',
    'compartilharItemMod',
    'grupoTrabalhoMod',
    'abaVisaoGeralMod',
    'abaAnexosMod',
    'abaParticipantesMod',
    'abaTarefasMod',
    'abaMensagensMod',
    'abaPdfFornecedorMod',
    'abaOpcoesMod'
]);

app.run(function ($rootScope, $templateCache, $http) {
    $http.get('/view/artwork/ticket_visao_geral.html', {
        cache: $templateCache,
        headers: {
            'Cache-Control': 'no-cache'
        }
    });
    
    $http.get('/view/artwork/ticket_anexos.html', {
        cache: $templateCache,
        headers: {
            'Cache-Control': 'no-cache'
        }
    });
    
    $http.get('/view/artwork/ticket_participantes.html', {
        cache: $templateCache,
        headers: {
            'Cache-Control': 'no-cache'
        }
    });
    
    $http.get('/view/artwork/ticket_tarefas.html', {
        cache: $templateCache,
        headers: {
            'Cache-Control': 'no-cache'
        }
    });
    
    $http.get('/view/artwork/ticket_mensagens.html', {
        cache: $templateCache,
        headers: {
            'Cache-Control': 'no-cache'
        }
    });
    
    $http.get('/view/artwork/ticket_pdf_fornecedor.html', {
        cache: $templateCache,
        headers: {
            'Cache-Control': 'no-cache'
        }
    });
    
    $http.get('/view/artwork/ticket_opcoes.html', {
        cache: $templateCache,
        headers: {
            'Cache-Control': 'no-cache'
        }
    });
});

app.config(function ($routeProvider, $locationProvider) {
    aba_padrao = (libera_fornecedor_pdf == 1) ? '/pdf-fornecedor' : '/visao-geral';
    
    $routeProvider
        .when('/visao-geral', {
            templateUrl: '/view/artwork/ticket_visao_geral.html',
            controller: 'abaVisaoGeralCtrl',
            resolve: {
                isOffset: function ($location, $rootScope) {
                    $rootScope.activetab = $location.path();
                },
                delay: function($q, $timeout){
                    var delay = $q.defer();
                    $timeout(delay.resolve, new Date().getMilliseconds());
                    return delay.promise;
                }
            }
        })
        .when('/anexos', {
            templateUrl: '/view/artwork/ticket_anexos.html',
            controller: 'abaAnexosCtrl',
            resolve: {
                isOffset: function ($location, $rootScope) {
                    $rootScope.activetab = $location.path();
                },
                delay: function($q, $timeout){
                    var delay = $q.defer();
                    $timeout(delay.resolve, new Date().getMilliseconds());
                    return delay.promise;
                }
            }
        })
        .when('/participantes', {
            templateUrl: '/view/artwork/ticket_participantes.html',
            controller: 'abaParticipantesCtrl',
            resolve: {
                isOffset: function ($location, $rootScope) {
                    $rootScope.activetab = $location.path();
                },
                delay: function($q, $timeout){
                    var delay = $q.defer();
                    $timeout(delay.resolve, new Date().getMilliseconds());
                    return delay.promise;
                }
            }
        })
        .when('/tarefas', {
            templateUrl: '/view/artwork/ticket_tarefas.html',
            controller: 'abaTarefasCtrl',
            resolve: {
                isOffset: function ($location, $rootScope) {
                    $rootScope.activetab = $location.path();
                },
                delay: function($q, $timeout){
                    var delay = $q.defer();
                    $timeout(delay.resolve, new Date().getMilliseconds());
                    return delay.promise;
                }
            }
        })
        .when('/mensagens', {
            templateUrl: '/view/artwork/ticket_mensagens.html',
            controller: 'abaMensagensCtrl',
            resolve: {
                isOffset: function ($location, $rootScope) {
                    $rootScope.activetab = $location.path();
                },
                delay: function($q, $timeout){
                    var delay = $q.defer();
                    $timeout(delay.resolve, new Date().getMilliseconds());
                    return delay.promise;
                }
            }
        })
        .when('/pdf-fornecedor', {
            templateUrl: '/view/artwork/ticket_pdf_fornecedor.html',
            controller: 'abaPdfFornecedorCtrl',
            resolve: {
                isOffset: function ($location, $rootScope) {
                    $rootScope.activetab = $location.path();
                },
                delay: function($q, $timeout){
                    var delay = $q.defer();
                    $timeout(delay.resolve, new Date().getMilliseconds());
                    return delay.promise;
                }
            }
        })
        .when('/opcoes', {
            templateUrl: '/view/artwork/ticket_opcoes.html',
            controller: 'abaOpcoesCtrl',
            resolve: {
                isOffset: function ($location, $rootScope) {
                    $rootScope.activetab = $location.path();
                },
                delay: function($q, $timeout){
                    var delay = $q.defer();
                    $timeout(delay.resolve, new Date().getMilliseconds());
                    return delay.promise;
                }
            }
        })
        .otherwise({
            redirectTo: aba_padrao
        });
});

app.directive('dropdown', function ($document) {
    return {
        restrict: 'C',
        link: function (scope, element, attr) {
            const dropdown = element.find('.dropdown-btn');

            dropdown.bind('click', function () {
                element.toggleClass('dropdown-active');
                element.addClass('active-recent');

                if (angular.element('.dropdown').hasClass('dropdown-active')) {
                    scope.interval_tickets_stop();
                } else {
                    scope.interval_tickets_start();
                }
            });

            dropdown.bind('blur', function () {
                if (element.hasClass('active-recent')) {
                    element.removeClass('dropdown-active');
                    scope.interval_tickets_start();
                }

                element.removeClass('active-recent');
            });
        }
    };
});

app.directive('validFile', function validFile($parse) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            ngModelCtrl.$validators.validFile = function () {
                element.on('change', function () {
                    var value = element.val(),
                        model = $parse(attrs.ngModel),
                        modelSetter = model.assign;

                    scope.uploadedFileType = null;

                    if (!value) {
                        modelSetter(scope, '');
                    } else {
                        var ext = value.substring(value.lastIndexOf('.') + 1).toLowerCase();

                        if (attrs.validFile.indexOf(ext) !== -1) {
                            scope.uploadedFileType = ext;
                            modelSetter(scope, element[0].files[0]);
                        } else {
                            myAlert('Formato de arquivo inválido.', 'warning');
                            scope.uploadedFileType = 'other';
                            modelSetter(scope, '');
                            scope.limparCamposUpload();
                        }
                    }
                });
            };
        }
    };
});

app.directive('fileUpload', function () {
    return {
        scope: true, //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                var uploadObj = el.context.name;
                //iterate files since 'multiple' may be specified on the element
                for (var i = 0; i < files.length; i++) {
                    //emit event upward
                    scope.$emit("fileSelected", {
                        file: files[i]
                    });
                }
            });
        }
    };
});

app.directive('contenteditable', [
    function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                function read() {
                    ngModel.$setViewValue(element.html());
                }

                ngModel.$render = function () {
                    element.html(ngModel.$viewValue || '');
                };

                element.bind('blur keyup change', function () {
                    scope.$apply(read);
                });
            }
        };
    }
]);

app.directive("mwConfirmClick", [
    function () {
        return {
            priority: -1,
            restrict: 'A',
            scope: {
                confirmFunction: "&mwConfirmClick"
            },
            link: function (scope, element, attrs) {
                element.bind('click', function (e) {
                    var message = attrs.mwConfirmClickMessage ? attrs.mwConfirmClickMessage : 'Tem certeza?';

                    if ($.confirm) {
                        $.confirm({
                            icon: 'icon fa fa-question',
                            title: 'Confirmar',
                            content: message,
                            closeIcon: true,
                            boxWidth: '910px',
                            scrollToPreviousElement: false,
                            useBootstrap: false,
                            closeIconClass: 'jconfirm-btn-close fa fa-times',
                            buttons: {
                                Confirmar: {
                                    keys: ['enter', 'space'],
                                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                                    btnClass: 'btn-confirm',
                                    action: function (Confirmar) {
                                        scope.confirmFunction();
                                    }
                                },
                                Cancelar: {
                                    keys: ['esc'],
                                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                                    btnClass: 'btn btn-cancel'
                                }
                            },
                            onOpenBefore: function () {
                                $('body').addClass('no-scroll');
                                $('.jconfirm').css('overflow', 'auto');
                                $('.jconfirm').overlayScrollbars({
                                    scrollbars: {
                                        autoHide: 'move'
                                    }
                                });
                            },
                            onClose: function () {
                                $('body').removeClass('no-scroll');
                                $('.jconfirm').css('overflow', 'hidden');
                            }
                        });
                    } else {
                        if (window.confirm(message)) {
                            scope.confirmFunction();
                        }
                    }
                });
            }
        };
    }
]);

app.filter('fileSize', function() {
    return function(size) {
        if (isNaN(size))
            size = 0;

        if (size < 1024)
            return size + ' Bytes';
        size /= 1024;

        if (size < 1024)
            return size.toFixed(2) + ' Kb';
        size /= 1024;

        if (size < 1024)
            return size.toFixed(2) + ' Mb';
        size /= 1024;

        if (size < 1024)
            return size.toFixed(2) + ' Gb';
        size /= 1024;

        return size.toFixed(2) + ' Tb';
    };
});

app.component('multiCheckboxContainer', {
    controller: function () {
        var ctrl = this;
        var checkboxes = [];
        var checkboxModels = [];
        var previousClickedCheckbox = null;

        ctrl.addCheckbox = addCheckbox;
        ctrl.onCheckboxClick = onCheckboxClick;

        function addCheckbox(checkbox, checkboxModelCtrl) {
            checkboxes.push(checkbox);
            checkboxModels.push(checkboxModelCtrl);
        }

        function onCheckboxClick(checkbox, shiftKey) {
            var start, end, i, checking;
            
            if (shiftKey && previousClickedCheckbox) {
                checking = checkbox.prop('checked')
                start = checkboxes.indexOf(previousClickedCheckbox);
                end = checkboxes.indexOf(checkbox);
                
                if (start > end) {
                    start = start + end;
                    end = start - end;
                    start = start - end;
                }
                
                for (i = start; i <= end; i++) {
                    checkboxes[i].prop('checked', checking);
                    checkboxModels[i].$setViewValue(checking);
                }
            }
            
            previousClickedCheckbox = checkbox;
        }
    }
});

app.directive('multiCheckbox', function () {
    return {
        restrict: 'A',
        require: ['^^multiCheckboxContainer', 'ngModel'],
        link: function (scope, element, attrs, controllers) {
            var containerCtrl = controllers[0];
            var ngModelCtrl = controllers[1];
            containerCtrl.addCheckbox(element, ngModelCtrl);

            element.on('click', function (ev) {
                containerCtrl.onCheckboxClick(element, ev.shiftKey);
            });
        }
    };
});

app.controller('TicketEdicaoCtrl', function ($scope, $interval, $rootScope, $ngConfirm, $timeout) {
    $scope.artw_tab = 1;
    $scope.tab = 1;
    $scope.id_ticket = id_ticket;
    $scope.mensagens = [];
    $scope.comentarios = [];
    $scope.enviar_comentarios = [];
    $scope.respostas = [];
    $scope.enviar_respostas = [];
    $scope.exibe_comentar = [];
    $scope.exibe_responder = [];
    $scope.exibir_responder_msg = [];
    $scope.exibe_nova_conversa = false;
    $scope.index_resposta = 0;
    $scope.interval_tickets = false;
    $scope.legenda = '';
    $scope.step3 = false;
    $scope.enviar_emails = [];
    $scope.files_arquivo = [];
    $scope.files_layout = [];
    $scope.files_upload_arte = [];
    $scope.files = [];
    $scope.fornecedores = [];
    $scope.fornecedorSelecionado = '';
    $scope.msgTicket = '';
    $scope.msg_fornecedor = '';
    $scope.data = {};
    $scope.memberList = [];
    $scope.modal_enviar = false;
    $scope.exibe_loading = false;

    $scope.myConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome', 'empresa', 'departamento'],
        placeholder: 'Selecionar membro',
        maxItems: 1,
        render: {
            option: function (data, escape) {
                if (data.ignore === true) {
                    return '<div style="display:none"></div>';
                } else if (data.departamento !== '') {
                    return '<div>' +
                        '   <span class="heading">' + escape(data.nome) + '</span>' +
                        '   <span class="caption">' + escape(data.empresa) + '</span>' +
                        '   <span class="slash">' + escape('/') + '</span>' +
                        '   <span class="caption">' + escape(data.departamento) + '</span>' +
                        '</div>';
                } else {
                    return '<div>' +
                        '   <span class="heading">' + escape(data.nome) + '</span>' +
                        '   <span class="caption">' + escape(data.empresa) + '</span>' +
                        '   <span class="slash">' + escape('/') + '</span>' +
                        '   <span class="caption">' + 'Departamento' + '</span>' +
                        '</div>';
                }
            }
        },
        onInitialize: function (selectize) {
            $('.selectize-dropdown').addClass('direction-up');
        },
        onDropdownOpen: function () {
            this.clear();
            this.focus();
        }
    };

    $scope.userLoginConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome', 'empresa', 'departamento'],
        placeholder: 'Selecionar membro',
        maxItems: 1,
        render: {
            option: function (data, escape) {
                if (data.ignore === true) {
                    return '<div style="display:none"></div>';
                } else if (data.departamento !== '') {
                    return '<div>' +
                        '   <span class="heading">' + escape(data.nome) + '</span>' +
                        '   <span class="caption">' + escape(data.empresa) + '</span>' +
                        '   <span class="slash">' + escape('/') + '</span>' +
                        '   <span class="caption">' + escape(data.departamento) + '</span>' +
                        '</div>';
                } else {
                    return '<div>' +
                        '   <span class="heading">' + escape(data.nome) + '</span>' +
                        '   <span class="caption">' + escape(data.empresa) + '</span>' +
                        '   <span class="slash">' + escape('/') + '</span>' +
                        '   <span class="caption">' + 'Departamento' + '</span>' +
                        '</div>';
                }
            }
        },
        onInitialize: function (selectize) {
            selectize.on('change', function () {
                if (this.options) {
                    angular.element('#participantes').find('.btn-group').removeClass('ng-hide');
                }
            });
        },
        onDropdownOpen: function () {
            this.clear();
            this.focus();
        }
    };

    $scope.solicitacoes = [];
    $scope.tipos = [];
    $scope.ticket = {};
    $scope.participantes = [];
    $scope.membros = {};
    $scope.cbMembershipProfile = [];
    $scope.cbMembershipDelete = [];
    $scope.cbMembershipEquipeDelete = [];
    $scope.prosseguir = '';
    $scope.itens = {};
    $scope.cancelamento_descricao = '';
    $scope.cancelado = false;
    $scope.pausado = true;
    $scope.link_ativo;
    $scope.link_expirado = true;
    $scope.memberList = [];

    $scope.model = {
        selectedMemberlList: []
    };

    $scope.atrw_arquivo_atual = null;
    $scope.atrw_arquivo_anterior = null;
    $scope.disable = true;

    $scope.artwFileCompareConfig = {
        create: false,
        persist: true,
        valueField: 'id',
        labelField: 'nome',
        delimiter: '|',
        placeholder: 'Selecione um PDF de comparação',
        dropdownParent: null,
        scrollDuration: 0,
        maxItems: 1,
        onInitialize: function (selectize) {
            $(selectize.$dropdown[0]).addClass('direction-up');
        },
        onDropdownOpen: function () {
            this.clear();
            this.focus();
        }
    };

    $scope.config_fornecedores = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        persist: false,
        hideSelected: true,
        maxItems: 1,
        placeholder: 'Selecione o Fornecedor',
        searchField: ['nome'],
        render: {
            item: function (data) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {
            this.clear();
            this.focus();
        }
    };

    $scope.config_enviar_emails = {
        plugins: ['restore_on_backspace'],
        openOnFocus: false,
        valueField: 'id',
        labelField: 'emails',
        persist: false,
        delimiter: '[,]',
        placeholder: 'Selecione os participantes',
        searchField: ['emails'],
        create: function (input) {
            return {
                id: input,
                emails: input
            };
        },
        render: {
            item: function (data, escape) {
                var emails = data.emails,
                    tmp = new Array();

                tmp = emails.split(',');
                
                return '<div class="item">' + escape(tmp) + '</div>';
            }
        },
        onDropdownOpen: function () {
            this.clear();
            this.focus();
        }
    };

    $scope.config_funcoes = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        delimiter: '|',
        maxItems: 1,
        placeholder: 'Selecione as funções',
        onInitialize: function (selectize) {}
    };

    $scope.participants_config = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        delimiter: '|',
        maxItems: 1,
        placeholder: 'Selecione os participantes',
        searchField: ['nome', 'empresa', 'departamento'],
        onInitialize: function (selectize) {},
        render: {
            item: function (item, escape) {
                return '<div>' +
                    (item.nome ? '<span class="heading">' + escape(item.nome) + '</span>' : '') + ' ' +
                    (item.empresa ? '<span class="caption">' + escape(item.empresa) + '</span>' : '') + '<span class="slash"> / </span>' +
                    (item.departamento ? '<span class="caption">' + escape(item.departamento) + '</span>' : '') +
                    '</div>';
            },
            option: function (item, escape) {
                var nome = item.nome || item.nome,
                    empresa = item.empresa ? item.empresa : null,
                    departamento = item.departamento ? item.departamento : null;

                return '<div>' +
                    '<span class="heading">' + escape(nome) + '</span>' +
                    (empresa ? '<span class="caption">' + escape(empresa) + '</span>' : '') + '<span class="slash"> / </span>' +
                    (departamento ? '<span class="caption">' + escape(departamento) + '</span>' : '') +
                    '</div>';
            }
        },
        onDropdownOpen: function () {
            this.clear();
            this.focus();
        }
    };

    $scope.user_login_config = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecione os participantes',
        maxItems: 1,
        render: {
            option: function (data, escape) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {
            this.clear();
            this.focus();
        }
    };

    $scope.team_member_config = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecionar membro',
        maxItems: 1,
        render: {
            option: function (data, escape) {
                if (data.ignore === true) {
                    return '<div style="display:none"></div>';
                } else {
                    return '<div class="item">' + data.nome + '</div>';
                }
            }
        },
        onDropdownOpen: function () {
            this.clear();
            this.focus();
        }
    };

    $scope.show_load = function () {
        $('.lds-css').show();
        
        $scope.exibe_loading = true;
    };

    $scope.hide_load = function () {
        $('.lds-css').hide();
        
        $scope.exibe_loading = false;
    };

    /* Modal que exibe a lista dos destinatários */
    $scope.exibe_destinatarios = function(array_envios) {

        $scope.array_envios = array_envios;

        $ngConfirm({
            useBootstrap: false,
            boxWidth: '910px',
            title: '',
            content: '<div class="ng-confirm-action-dialog margin-bottom-30">' +
                '   <div class="dialog-heading">' +
                '       <h4>Destinatários que receberam por e-mail</h4>' +
                '   </div>' +
                '   <div class="workflow-send-by-email block margin-top-20">' +
                '       <table cellpading="0" cellspacing="0" class="table-nested table-border">' +
                '           <thead>' +
                '               <tr>' +
                '                   <th><span class="txt-lighter txt-uppercase">Participante</span></th>' +
                '                   <th><span class="txt-lighter txt-uppercase">Empresa / Departamento</span></th>' +
                '               </tr>' +
                '           </thead>' +
                '           <tbody>' +
                /* Looping de membros */
                '               <tr class="table-group" ng-repeat="envio in array_envios | orderBy:'+"['empresa', 'depto']"+'">' +
                '                   <td colspan="2">' +
                '                       <table cellpading="0" cellspacing="0">' +
                '                           <tbody>' +
                '                               <tr>' +
                '                                   <td><span class="checkbox-txt txt-semibold">{{envio.nome}}</span></td>' + // TO-DO - Nome do Participante                
                '                                   <td><span class="member-profile-txt">{{envio.empresa}} / {{envio.depto}}</span></td>' + // TO-DO - Empresa / Departamento
                '                               </tr>' +
                /* Looping de equipe
                '                               <tr>' +
                '                                   <td><span class="member-profile-txt txt-semibold">Teste A</span></td>' + // TO-DO - Nome do Participante
                '                                   <td><span class="member-profile-txt">Teste A</span></td>' + // TO-DO - Empresa / Departamento
                '                               </tr>' +
                END Looping de equipe */
                '                           </tbody>' +
                '                       </table>' +
                '                   </td>' +
                '               </tr>' +
                /* END Looping de membros */
                '           </tbody>' +
                '       </table>' +
                '   </div>' +
                '</div>',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'ng-confirm-btn-close fa fa-times',
            backgroundDismiss: true,
            onOpenBefore: function() {
                setTimeout(function () {
                    $('.ng-confirm-content-pane').addClass('no-margin');
                    $('.ng-confirm .ng-confirm-scrollpane').overlayScrollbars({
                        scrollbars: {
                            autoHide: 'move'
                        }
                    });
                }, 0);
            },
            onClose: function() {
                $('.ng-confirm-content-pane').addClass('no-margin');
            }
        });
    };

    $scope.marcar_participantes_unicos = function(id_usuario) {
        $scope.memberList = $scope.ticket.participantesUnicos;

        for (var i = 0; i < $scope.memberList.length; ++i) {
            $scope.memberList[i].selected = false;
            
            if ($scope.memberList[i].id_usuario == id_usuario) {
                $scope.memberList[i].selected = true;
            }
        }
    };

    $scope.enviar_msg_email = function () {
        var message = 'Selecione os participantes que deseja que recebam uma cópia da mensagem por e-mail.';

        $ngConfirm({
            useBootstrap: false,
            boxWidth: '910px',
            title: '',
            content: '<form name="messageSenderDlg" class="ng-confirm-action-dialog margin-bottom-30">' +
                '   <div class="dialog-heading">' +
                '       <h4>Enviar mensagem por e-mail</h4>' +
                '   </div>' +
                '   <div class="text">' +
                '       <p class="txt-regular">' + message + '</p>' +
                '   </div>' +
                '   <multi-checkbox-container class="workflow-send-by-email block margin-top-20">' +
                '       <table cellpading="0" cellspacing="0" class="table-nested table-border">' +
                '           <thead>' +
                '               <tr>' +
                '                   <th><span class="txt-lighter txt-uppercase">Participante</span></th>' +
                '                   <th><span class="txt-lighter txt-uppercase">Empresa / Departamento</span></th>' +
                '               </tr>' +
                '           </thead>' +
                '           <tbody>' +
                '               <tr ng-repeat="member in memberList | orderBy:'+"['empresa', 'departamento']"+'" class="table-group" ng-class="'+"{'member-selected': member.selected === true}"+'">' +
                '                   <td colspan="2">' +
                '                       <table cellpading="0" cellspacing="0">' +
                '                           <tbody>' +
                '                               <tr>' +
                '                                   <td>' +
                '                                       <label class="checkbox-control">' +
                '                                           <input type="checkbox" multi-checkbox ng-checked="member.selected === true || isAllSelected" ng-model="member.selected" />' +
                '                                           <span class="checkmark"></span>' +
                '                                           <span class="checkbox-txt txt-semibold">{{ member.nome_usuario }}</span>' +
                '                                       </label>' +
                '                                   </td>' +
                '                                   <td><span class="member-profile-txt">{{ member.empresa }} / {{ member.departamento }}</span></td>' + // TO-DO - Empresa / Departamento
                '                               </tr>' +                               
                '                           </tbody>' +
                '                       </table>' +
                '                   </td>' +
                '               </tr>' +
                '           </tbody>' +
                '       </table>' +
                '       <div class="select-all clearfix">' +
                '           <label class="checkbox-control">' +
                '               <input type="checkbox" ng-init="isAllSelected = false" ng-click="$root.toggleAll()" ng-model="isAllSelected" />' +
                '               <span class="checkmark"></span>' +
                '               <span class="checkbox-txt txt-semibold txt-color-primary">Selecionar Todos</span>' +
                '           </label>' +
                '       </div>' +
                '   </multi-checkbox-container>' +
                '</form>',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'ng-confirm-btn-close fa fa-times',
            backgroundDismiss: true,
            onOpenBefore: function() {
                setTimeout(function () {
                    $scope.modal_enviar = true;
                    $('.ng-confirm-content-pane').addClass('no-margin');
                    $('.ng-confirm .ng-confirm-scrollpane').overlayScrollbars({
                        scrollbars: {
                            autoHide: 'move'
                        }
                    });
                }, 0);
            },
            onClose: function() {
                $('.ng-confirm-content-pane').addClass('no-margin');
            }
        });
    };

    $rootScope.toggleAll = function () {
        var toggleStatus = $scope.isAllSelected;

        angular.forEach($scope.memberList, function (itm) {
            itm.selected = toggleStatus;

            angular.forEach(itm.equipe, function (equipe) {
                equipe.selected = toggleStatus;
            });

        });
    };

    $scope.despausar_ticket_service = function () {
        $scope.show_load();
        $scope.interval_tickets_stop();

        params = {
            _token: csrf_token,
            id: $scope.ticket.id,
            acao: 'despausar'
        };

        $.post(site_url + 'site/artwork/services/package/ticket/pausarCancelar', params, function (data) {
                myAlert('Ticket reativado com sucesso.', 'success');
                $scope.carregar_ticket(1);
                $scope.interval_tickets_start();
            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });

        $scope.cancelamento_descricao = '';
    };

    $scope.despausar_ticket = function () {
        var message = 'Deseja reativar este ticket?';

        $.confirm({
            icon: 'fa fa-question',
            title: 'Confirmar',
            content: message,
            draggable: false,
            dragWindowBorder: false,
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            useBootstrap: false,
            boxWidth: '910px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        angular.element('.pause-ticket-switch input').prop('checked', false);
                        $scope.despausar_ticket_service();
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {
                        angular.element('.pause-ticket-switch input').prop('checked', true);
                    }
                }
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };

    //listen for the file selected event
    $scope.$on('fileSelected', function (event, args) {
        $scope.$apply(function () {
            //add the file object to the scope's files collection
            $scope.files.push(args.file);
        });
    });

    $scope.acao_ticket = function (status) {
        $scope.show_load();

        params = {
            _token: csrf_token,
            id: id_ticket,
            status: status
        };

        $.post(site_url + 'site/artwork/services/package/ticket/acao', params, function (data) {
                $scope.carregar_ticket(1,1);
            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
            });
    };

    $scope.interval_tickets_start = function () {
        if ($scope.interval_tickets === false) {
            $scope.Timer = $interval(function () {
                $scope.carregar_ticket(0);
            }, 10000);
            $scope.interval_tickets = true;
        }
    };

    $scope.interval_tickets_stop = function () {
        $interval.cancel($scope.Timer);
        $scope.interval_tickets = false;
    };

    $scope.carregar_ticket = function (exibe_loading, force) {
        if ($('.flex-rename-file').is(':visible') && exibe_loading == 0) {
            return;
        }

        if ($('.chat-comment-box').is(':visible') && exibe_loading == 0) {
            return;
        }

        if (angular.element('.va-header-hide').is(':visible') && exibe_loading == 0) {
            return;
        }

        if ($('.workflow-membership-table').find('.team-member-insert').is(':visible')  && !force) {
            return;
        }

        if ($('#cbSupplierChooser').is(':focus')  && !force) {
            return;
        }

        if ($('#cbSupplierChooser').val()  && !force) {
            return;
        }

        if ($('#tfEmailSupplier').is(':focus')  && !force) {
            return;
        }

        if ($('#msg_fornecedor').is(':focus')  && !force) {
            return;
        }

        if ($('#msg_fornecedor').val()  && !force) {
            return;
        }

        if ($('.dropdown').find('.dropdown-menu').is(':visible')  && !force) {
            return;
        }

        if ($('.dropdown').find('.dropdown-list').is(':visible')  && !force) {
            return;
        }

        if ($('.attach').find('.blob > span').is(':visible')  && !force) {
            return;
        }

        if ( ($scope.activetab == '/participantes')  && !force) {
            return;
        }

        if (exibe_loading == 1) {
            $scope.show_load();
        }
                
        window.addEventListener('focus', $scope.interval_tickets_start);
        
        $.get(site_url + 'site/artwork/services/package/ticket/' + id_ticket, function (data) {
                if ($scope.ticket.id_status && $scope.ticket.id_status != data.id_status) {
                    var topo = true;
                } else {
                    var topo = false;
                }
                
                $scope.ticket = data;
                
                $scope.ticket.dt_ini = $scope.ticket.dt_ini.replace(/(.+) (.+)/, "$1T$2Z");
                $scope.ticket.dt_envio = $scope.ticket.dt_envio.replace(/(.+) (.+)/, "$1T$2Z");
                $scope.ticket.dt_fim = $scope.ticket.dt_fim.replace(/(.+) (.+)/, "$1T$2Z");

                $scope.ticket.dt_ini = Date.parse($scope.ticket.dt_ini);
                $scope.ticket.dt_envio = Date.parse($scope.ticket.dt_envio);
                $scope.ticket.dt_fim = Date.parse($scope.ticket.dt_fim);

                if ($scope.ticket.comparacao.length >= 2 && exibe_loading) {

                    $scope.atrw_arquivo_atual = $scope.ticket.comparacao[0].id;
                    $scope.atrw_arquivo_anterior = $scope.ticket.comparacao[1].id;

                }

                var solicitacoes = (data.ciclo_atual.solicitacoes) ? data.ciclo_atual.solicitacoes : [];

                total = parseInt(solicitacoes.length);
                total_revisadas = 0;

                for (var i = 0; i < total; i++) {
                    if (solicitacoes[i].id_status === 2) {
                        total_revisadas++;
                    }
                }

                if (total === total_revisadas) {
                    $scope.step3 = true;
                } else {
                    $scope.step3 = false;
                }

                $scope.$apply();

            $scope.interval_tickets_start();
                if (exibe_loading == 1) {
                    $scope.hide_load();
                }
            }, 'json')
            .fail(function (data) {
                if (exibe_loading == 1) {
                    $scope.hide_load();
                }
            });
    };

    $scope.msg_file_upload_service = function (tipo, id) {
        var formData = new FormData();
        formData.append('file1', $rootScope.item);

        $scope.show_load();
        
        $.ajax({
            url: site_url + 'site/artwork/services/package/upload',
            headers: {
                'x-csrf-token': csrf_token
            },
            type: 'POST',
            data: formData,
            success: function (data) {
                var imageUrl = data.url,
                    imgURLIcon = ' <label class="attach" contenteditable="false">' +
                    '   <a class="blob icon-camera" href="' + imageUrl + '" target="blank">' +
                    '       <span>Clique aqui para visualizar</span>' +
                    '   </a>' +
                    '</label>&nbsp;';

                if (tipo == 'geral') {
                    $rootScope.msgTicket += imgURLIcon;
                } else if (tipo == 'respostas') {
                    $rootScope.respostas[id] += imgURLIcon;
                } else if (tipo == 'comentarios') {
                    $rootScope.comentarios[id] += imgURLIcon;
                } else {
                    alert('Não implementado');
                }

                $scope.hide_load();
                $rootScope.$apply();
            },
            error: function () {
                myAlert('Ocorreu um erro.', 'error');
            },
            contentType: false,
            processData: false
        });
    };

    $scope.msg_attach_file = function (element, tipo) {
        var fileInput = angular.element('.msgAttachment');

        if (tipo === undefined) {
            tipo = null;
        }

        $rootScope.item = element.files[0];

        if ($scope.item.type.indexOf("image") != -1) {
            $scope.msg_file_upload_service(tipo, element.getAttribute('data-id'));
            angular.element(fileInput).val(null);
        } else {
        
        }
    };

    $scope.msg_copy_paste_handler = function (e, tipo, id) {
        clipboardData = e.clipboardData || e.originalEvent.clipboardData;

        if (clipboardData.items === undefined) {
            return;
        }

        if (id === undefined) {
            id = null;
        }

        for (var i = 0; i < clipboardData.items.length; i++) {
            var item = clipboardData.items[i];

            if (item.type.indexOf('image') != -1) {
                $rootScope.item = item.getAsFile();
                $scope.msg_file_upload_service(tipo, id);
                e.preventDefault();
            } else {
            
            }
        }
    };

    $scope.oculta_nova_conversa_f = function() {
        $scope.exibe_nova_conversa = false;
    };

    $scope.exibe_nova_conversa_f = function() {
        for(i = 0; i < $scope.exibir_responder_msg.length; i++) {
            $scope.exibir_responder_msg[i] = '';
        }

        $scope.exibe_nova_conversa=!$scope.exibe_nova_conversa;
    };

    $scope.exibir_responder_msg_f = function ($id) {
        $scope.exibe_nova_conversa = false;
        
        for(i = 0; i < $scope.exibir_responder_msg.length; i++) {
            if($scope.exibir_responder_msg[i] && i !== $id)
                $scope.exibir_responder_msg[i] = '';
        }

        if ($scope.exibir_responder_msg[$id] === 1) {
            $scope.exibir_responder_msg[$id] = '';
            $scope.interval_tickets_start();
        } else {
            $scope.exibir_responder_msg[$id] = 1;
            $scope.interval_tickets_stop();
        }
    };

    $scope.exibe_comentar_f = function (s, index) {
        for(i = 0; i < $scope.exibe_comentar.length; i++) {
            if($scope.exibe_comentar[i] && i !== index)
                $scope.exibe_comentar[i] = '';
        }

        for(i = 0; i < $scope.exibe_responder.length; i++) {
            if($scope.exibe_responder[i] && i !== index)
                $scope.exibe_responder[i] = '';
        }

        if ($scope.exibe_comentar[s] === 1) {
            $scope.exibe_comentar[s] = '';
            $scope.interval_tickets_start();
        } else {
            $scope.exibe_comentar[s] = 1;
            $scope.interval_tickets_stop();
        }
    };

    $scope.exibe_responder_f = function ($id) {
        for(i = 0; i < $scope.exibe_comentar.length; i++) {
            $scope.exibe_comentar[i] = '';
        }

        for(i = 0; i < $scope.exibe_responder.length; i++) {
            if($scope.exibe_responder[i] && i !== $id)
                $scope.exibe_responder[i] = '';
        }

        if ($scope.exibe_responder[$id] === 1) {
            $scope.exibe_responder[$id] = '';
            $scope.interval_tickets_start();
        } else {
            $scope.exibe_responder[$id] = 1;
            $scope.interval_tickets_stop();
        }
    };

    $scope.comentar = function (id, s) {
        if ($rootScope.comentarios[s] === null || $rootScope.comentarios[s] === undefined || $rootScope.comentarios[s] === '') {
            $('.conversation').find('.textbox-fake').addClass('has-error');
            $scope.interval_tickets_stop();
            myAlert('Insira uma mensagem.', 'warning');
            return false;
        }

        $scope.show_load();

        params = {
            _token: csrf_token,
            id: id,
            texto: $rootScope.comentarios[s],
            enviar_membros: $scope.memberList,
            modal_enviar: $scope.modal_enviar
        };

        $.post(site_url + 'site/artwork/services/package/comentar', params, function (data) {
                $scope.exibe_comentar[s] = '';
                $rootScope.comentarios[s] = '';
                $scope.enviar_comentarios[s] = '';
                $scope.carregar_ticket(1);
                $scope.interval_tickets_start();
            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });

        $scope.modal_enviar = false;
    };

    $scope.responder = function (id, $id, tipo) {
        if (tipo == undefined) {
            tipo = null;
        }

        if ($rootScope.respostas[$id] === null || $rootScope.respostas[$id] === undefined || $rootScope.respostas[$id] === '') {
            $('.replies').find('.textbox-fake').addClass('has-error');
            $scope.interval_tickets_stop();
            myAlert('Insira uma resposta.', 'warning');
            return false;
        }

        $scope.show_load();

        params = {
            _token: csrf_token,
            id: id,
            texto: $rootScope.respostas[$id],
            enviar_membros: $scope.memberList,
            tipo: tipo,
            modal_enviar: $scope.modal_enviar
        };

        $.post(site_url + 'site/artwork/services/package/responder', params, function (data) {
                $('.replies').hide();
                $scope.exibe_responder[$id] = '';
                $rootScope.respostas[$id] = '';
                $scope.enviar_respostas[$id] = '';
                $scope.carregar_ticket(1);
                $scope.interval_tickets_start();

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });

        $scope.modal_enviar = false;
    };

    $scope.customMultiFileInputCtrl = function () {
        $('.files-control-multi').find('strong').text('Anexar arquivo com no máx. 100 MB');
        
        $('.files-control-multi').on('click', function () {
            $('.files-control-multi').each(function () {
                $(this).find('input[type="file"]').val('');
                $(this).find('.files-control-fake strong').text('Anexar arquivo com no máx. 100 MB');
            });
        });
            
        $('.files-control-multi input[type="file"]').on('change focus', function () {
            $('.files-control-multi input[type="file"]').each(function () {
                if ($('.files-control-multi input[type="file"]').val()) {
                    var names = [];

                    for (var i = 0; i < $(this).get(0).files.length; ++i) {
                        names.push($(this).get(0).files[i].name);
                    }

                    $('.files-control-fake').find('strong').text(names.join(', '));
                } else if (!$('.files-control-multi input[type="file"]').val()) {
                    $scope.limparCamposUpload();
                }
            });
        });
    };

    $scope.customFileInputCtrl = function () {
        angular.element('.files-control').find('strong').text('Anexar arquivo');
        angular.element('.files-control.attachPDF').find('strong').text('Anexar arquivo PDF com no máx. 50 MB');
        angular.element('.files-control.attach-artwork').find('strong').text('Anexar arquivo ZIP ou RAR');
        
        angular.element('.files-control, .files-control.attach-artwork').on('click', function () {
            angular.forEach(angular.element('.files-control'), function () {
                $scope.limparCamposUpload();
            });

            angular.forEach(angular.element('.files-control.attachPDF'), function () {
                $scope.limparCamposUpload();
            });

            angular.forEach(angular.element('.files-control.attach-artwork'), function () {
                $scope.limparCamposUpload();
            });
        });
        
        angular.element('.files-control input[type="file"], .files-control.attach-artwork input[type="file"], .files-control.attachPDF input[type="file"]').bind('change focus', function (event) {
            angular.forEach(angular.element(event.target), function () {
                if (angular.element(event.target).val()) {
                    var filePlaceholder = angular.element(event.target).val().split('\\').pop();
                    angular.element(event.target).next('.files-control-fake').find('strong').text(filePlaceholder);
                }
            });
        });
    };

    $scope.limparCamposUpload = function () {
        $('.files-control').find('input[type="file"]').val('');
        $('.files-control').find('.files-control-fake strong').text('Anexar arquivo');
        $('.files-control.attachPDF').find('.files-control-fake strong').text('Anexar arquivo PDF com no máx. 50 MB');
        $('.files-control.attach-artwork').find('.files-control-fake strong').text('Anexar arquivo ZIP ou RAR');
    };
    
    $scope.carregar_fornecedores = function () {
        //$rootScope.loading = true;
        //$rootScope.statechange = false;
        $.get(site_url + 'site/artwork/services/package/fornecedores', function (data) {
                $scope.fornecedores = data;
                //$rootScope.loading = false;
                //$rootScope.statechange = true;
                $scope.$apply();
            }, 'json')
            .fail(function (data) {
            });
    };
    
    $scope.carregar_fornecedores();
    
    $scope.carregar_ticket(1);
    
    $scope.$on('$destroy', function () {
        $scope.interval_tickets_stop();
    });
});