var site_url = '/';
var app = angular.module('app', []);

app.directive("mwConfirmClick", [
    function () {
        return {
            priority: -1,
            restrict: 'A',
            scope: {
                confirmFunction: "&mwConfirmClick"
            },
            link: function (scope, element, attrs) {
                element.bind('click', function (e) {
                    var message = attrs.mwConfirmClickMessage ? attrs.mwConfirmClickMessage : "Are you sure?";
                    
                    if (window.confirm(message)) {
                        scope.confirmFunction();
                    }
                });
            }
        };
    }
]);

app.controller('AdminSkusCtrl', function ($scope, $timeout)
{
    $scope.dados = {};
    $scope.count = [];

    $scope.produtos = [];
    $scope.embalagens = [];
    $scope.campanhas = [];

    $scope.campanha_nome = '';
    
    $scope.cbPckEmbalagens = '';

    $scope.embalagens_por_natureza = [];
        
    $scope.embalagens_selecionadas = [];

    $scope.show_load = function ()
    {
        $('.lds-css').show();
    };

    $scope.hide_load = function ()
    {
        $('.lds-css').hide();
    };

    //$scope.checkPromo = function () 
    //{
        /*
        if ($('input[name=promocional]:checked').val() == 0){
            $('#nome-promo').hide();
            $('#box-expiracao').hide();
            
            if ($('#editId').val()){
                //$('#box-nome').show();
                $('#box-ordem').show();
                $('#manage-attrs').show();
            }
            
            else{
                //$('#box-nome').hide();
                $('#box-ordem').hide();
                $('#manage-attrs').hide();
            }
            
            $('#listImage').find('input[type="file"]').val('');
        }
        
        else {
            $('#nome-promo').show();
            $('#box-expiracao').show();
            //$('#box-nome').hide();
        }
        */
    //};

    $scope.resetar = function ()
    {
        $scope.dados = {};
        $scope.dados.promocional = '0';
        $scope.dados.possuiVariacao = '0';
        $scope.dados.status = '1';
        $scope.eanCode = '0';
        //$scope.checkPromo();
    };
    
    $scope.carregar_naturezas = function ()
    {
        $scope.show_load();

        $.get(site_url + 'admin/sku/naturezas', function (data)
        {
            $scope.naturezas = data;

            $scope.$apply();
            
            $scope.hide_load();
        }, 'json')
                .fail(function (data) {
                    alert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
    };

    $scope.carregar_produtos = function ()
    {
        $scope.show_load();

        $.get(site_url + 'admin/sku/produtos', function (data)
        {
            $scope.produtos = data;

            $scope.$apply();
            
            $scope.hide_load();
        }, 'json')
                .fail(function (data) {
                    alert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
    };

    $scope.carregar_embalagens = function ()
    {
        $scope.show_load();

        $.get(site_url + 'admin/service/sku/embalagens', function (data)
        {
            $scope.embalagens = data;

            $scope.$apply();
            
            $scope.hide_load();
        }, 'json')
                .fail(function (data) {
                    alert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
    };

    $scope.carregar_campanhas = function (limpar_embalagens)
    {
        $scope.hide_load();

        $scope.processing = true;

        angular.element('.modal-loading').show();

        angular.element('.modalBtnGroup input[type="submit"]').prop('disabled', true);
        
        $.get(site_url + 'admin/sku/campanhas/' + $scope.dados.id_produto, function (data)
        {
            $scope.campanhas = data;

            $scope.$apply();
            $scope.hide_load();
            
            $timeout(function () {
                $scope.processing = false;
                angular.element('.modal-loading').hide();
                angular.element('.modalBtnGroup input[type="submit"]').prop('disabled', false);
            }, new Date().getMilliseconds());
        }, 'json')
                .fail(function (data) {
                    alert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });

        if (limpar_embalagens)
        {
            $scope.limpar_embalagens_selecionadas(1);
        }
    };

    $scope.carregar_variacao = function (id_variacao)
    {
        $scope.hide_load();

        angular.element('.modal-loading').show();
        angular.element('.modalBtnGroup input[type="submit"]').prop('disabled', true);
        
        $.get(site_url + 'admin/sku/edit/' + id_variacao, function (data)
        {
            $scope.dados = data.editSku;
            $scope.count = data.count;
            
            $scope.id_campanha = data.editSku.id_campanha;

            $scope.carregar_campanhas();
            
            $scope.carregar_embalagens_por_natureza();
    
            $scope.bsModalOpen();
                
            $scope.$apply();
            
            $scope.hide_load();
        
            angular.element('.modal-loading').hide();
        }, 'json')
                .fail(function (data) {
                    alert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });        
        //$scope.checkPromo();
    };

    $scope.carregar_sku = function (id_sku)
    {
        $scope.hide_load();

        $.get(site_url + 'admin/sku/edit/' + id_sku, function (data)
        {
            $scope.dados = data.editSku;
            $scope.count = data.count;
            
            $scope.id_campanha = data.editSku.id_campanha;

            $scope.carregar_campanhas();

            $scope.bsModalOpen();
                
            $scope.$apply();
            
            $scope.hide_load();
        }, 'json')
                .fail(function (data) {
                    alert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });        
        //$scope.checkPromo();
    };

    $scope.salvar = function ()
    {
        var validator = $( "form" ).validate();
                
        if(!$('form').valid()) {
            return false;
        } else {
            $('.modal').modal('hide');
            validator.resetForm();
        }
        
        if ($scope.dados.id_variacao) {
            var url = site_url + 'admin/sku/update/' + $scope.dados.id_variacao;
            var msg = 'Embalagem atualizada com sucesso.';
        } else {
            var url = site_url + 'admin/sku/create';
            var msg = 'Embalagem criada com sucesso.';
        }

        $scope.show_load();

        $.ajax({
            url: url,
            data: new FormData($("#form-sku")[0]),
            type: 'post',
            processData: false,
            contentType: false,
            start_time: new Date().getTime(),
            success: function (data) {
                if (data.error) {
                    angular.element('.lds-css').fadeOut('200', function() {
                        alert(data.message);
                        return false;
                    });
                } else {
                    angular.element('.lds-css').fadeOut('200', function() {
                        alert(msg);
                        location.reload();                       
                    });
                }
            },
            error: function (data) {
                angular.element('.lds-css').fadeOut('200', function() {
                    alert('Ocorreu um erro');
                });
            }
        });
    };
    
    $scope.excluir_variacao = function (id_variacao)
    {
        $scope.show_load();

        $.ajax({
            url: site_url + 'admin/sku/remove/' + id_variacao,
            type: 'get',
            processData: false,
            contentType: false,
            success: function (data) {
                if (data.error) {
                    angular.element('.lds-css').fadeOut('200', function() {
                        alert(data.message);
                        return false;
                    });
                } else {
                    angular.element('.lds-css').fadeOut('200', function() {
                        alert('Embalagem excluída com sucesso.');
                        location.reload();                       
                    });
                }
            },
            error: function (data) {
                angular.element('.lds-css').fadeOut('200', function() {
                    alert(data.responseJSON.message);
                });
            }
        });
    };

    $scope.validate = function()
    {
        $('#editExpiracao').mask('99/99/9999', { placeholder: 'dd/mm/aaaa' });

        $('#txtEANCode').mask('00000000000000', { placeholder: '00000000000000' });

        //$.validator.addMethod('dateBR', function(value, element) {
        //    if(value.length!=10) return false;
        //    var data   = value;
        //    var dia    = data.substr(0,2);
        //    var barra1 = data.substr(2,1);
        //    var mes    = data.substr(3,2);
        //    var barra2 = data.substr(5,1);
        //    var ano    = data.substr(6,4);
        //    if(data.length!=10||barra1!="/"||barra2!="/"||isNaN(dia)||isNaN(mes)||isNaN(ano)||dia>31||mes>12)return false;
        //    if((mes==4||mes==6||mes==9||mes==11) && dia==31)return false;
        //    if(mes==2  &&  (dia>29||(dia==29 && ano%4!=0)))return false;
        //    if(ano < 1900)return false;
        //    return true;
        //}, 'Informe uma data válida');

        jQuery.validator.addMethod('eanCodeCheck', function(value,element) {
            if(element.value.length !== 8 && element.value.length !== 13 && element.value.length < 12) {
                return false;
            } else { 
                return true;
            }
        }, 'Insira o código de barras com 8, 12, 13 ou 14 dígitos.'); 
            
        $('form').validate({
            rules: {
                id_produto: {
                    required: true
                },
                id_natureza: {
                    required: true
                },
                eanCode: {
                    required: true,
                },
                txtEANCode: {
                    required: true,
                    eanCodeCheck: true
                },
                ordem: {
                    required: true
                },
                id_dimensao: {
                    required: {
                        depends: function(){
                            return $('#editProduto').val() != '';
                        }
                    }
                },
                variacao: {
                    required: '#possuiVariacao_1:checked',
                    normalizer: function(value) {
                        // Note: the value of `this` inside the `normalizer` is the corresponding
                        // DOMElement. In this example, `this` reference the `username` element.
                        // Trim the value of the input
                        return $.trim(value);
                    }
                },
                id_campanha: {
                    required: {
                        depends: function(){
                            return $('input[name=promocional]:checked').val() == 1 ? true : false;
                        }
                    }
                },
                campanha: {
                    required: {
                        depends: function(){
                            return $('#id_campanha_2').val() == 0;
                        }
                    }
                }//,
                //dt_expiracao:{
                //    required:{
                //        depends: function(){
                //            return $('input[name=promocional]:checked').val() == 1 ? true : false;
                //        }
                //    },
                //    dateBR: true
                //}
            },
            messages: {
                id_produto: {
                    required: 'Produto é obrigatório.'
                },
                id_natureza: {
                    required: 'Natureza é obrigatório.'
                },
                eanCode: {
                    required: 'Marque pelo menos uma opção.'
                },
                txtEANCode: {
                    required: 'Código de Barras é obrigatório.'
                },
                ordem: {
                    required: 'Ordem de Exibição é obrigatório.'
                },
                id_dimensao: {
                    required: 'Volume é obrigarório.'
                },
                variacao: {
                    required: 'Variação é obrigatório.',
                },
                id_campanha: {
                    required: 'Campanha é obrigatório.'
                },
                campanha: {
                    required: 'Nome da Nova Campanha é obrigatório.'
                },
                dt_expiracao: {
                    required: 'Data de Expiração é obrigatório.'
                }
            },

            errorPlacement: function(error, element) {
                if (element.attr('name') == 'eanCode') {
                    error.insertAfter('.ean-code-group');
                } else {
                    error.insertAfter(element);
                }
            }
        });
    };


    $scope.getById = function (id, array) {
        for (i = 0; i < array.length; i++) {
            if (array[i].id == id) {
                return array[i];
            }
        }
    };

    /** Adiciona embalagem primária/secundária selecionada a lista **/
    $scope.add_embalagem_selecionada = function() {
        if($scope.getById($scope.cbPckEmbalagens, $scope.dados.embalagens_selecionadas)) {
            alert('Esta embalagem já foi adicionada.');

            return false;
        } else if($scope.cbPckEmbalagens === '') {
            alert('Não foi selecionada uma embalagem.');
            
            return false;
        } else {

            if($scope.cbPckEmbalagens !== '') {
                var i = $scope.getById($scope.cbPckEmbalagens, $scope.embalagens_por_natureza);
                
                $scope.dados.embalagens_selecionadas.push(i)
                $scope.cbPckEmbalagens = '';
            }
        
        }
    };

    /** Exclui embalagem primária/secundária selecionada a lista **/
    $scope.excluir_embalagem_selecionada = function(index) {
        $scope.dados.embalagens_selecionadas.splice(index, 1);
    };

    /** Limpa a lista de embalagens primárias/secundárias selecionadas **/
    $scope.limpar_embalagens_selecionadas = function(limpa_natureza) {

        if (limpa_natureza)
        {
            $scope.dados.id_natureza_embalagem = '';
        }

        $scope.cbPckEmbalagens = '';
        $scope.dados.embalagens_selecionadas = [];
        $scope.embalagens_por_natureza = [];
    };

    $scope.carregar_embalagens_por_natureza = function (limpar_embalagens) {

        if (limpar_embalagens)
        {
            $scope.limpar_embalagens_selecionadas();
        }

        id_natureza = $scope.dados.id_natureza_embalagem;

        if (id_natureza == 2)
        {
            id_natureza_requisicao = '';
        }
        else if (id_natureza == 3)
        {
            id_natureza_requisicao = 2;
        }
        else if (id_natureza == 4)
        {
            id_natureza_requisicao = 3;
        }

        if (id_natureza_requisicao)
        {
            //$scope.show_load();

            angular.element('.modal-loading').show();
            angular.element('.modalBtnGroup input[type="submit"]').prop('disabled', true);
                            
            $scope.processing = true;
        
            var url = site_url + 'site/artwork/services/package/getEmbalagensPorNatureza/' + $scope.dados.id_produto + '/' + id_natureza_requisicao;

            $.ajax({
                url: url,
                type: "get",
                contentType: false,
                processData: false,
                start_time: new Date().getTime(),
                success: function (data) {
                    if (data.error) {
                        myAlert(data.message, 'error');
                    } else {
                        $scope.embalagens_por_natureza = data;
                        $scope.$apply();
                        
                        $timeout(function () {
                            angular.element('.modal-loading').hide();
                            angular.element('.modalBtnGroup input[type="submit"]').prop('disabled', false);
                            $scope.processing = false;
                        }, start_time - new Date().getTime())
                    }
                },
                error: function () {
                    myAlert('Ocorreu um erro.', 'error');
                }
            });
        }
    };
    
    $scope.bsModalOpen = function() {
        $('.modal').on('show.bs.modal', function () {
            $('.modal').overlayScrollbars({
                autoUpdate: true
            });
        });
    };

    $scope.bsModalClose = function() {
        $('form').validate().resetForm();
	    $('.modal').modal('hide');
        
        $scope.id_campanha = '';
        $scope.campanha_nome = '';
    };
    
    $scope.validate();
    $scope.carregar_produtos();
    $scope.carregar_naturezas();
    $scope.carregar_embalagens();
});
