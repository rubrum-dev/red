var opcoes = angular.module("abaOpcoesMod", []);

opcoes.controller('abaOpcoesCtrl', function (
        $scope,
        $ngConfirm,
        $rootScope,
        $timeout
    ) {
    $rootScope.statechange = true;

    $scope.seguir = function (id_usuario, id_ticket) {
        $scope.interval_tickets_stop();
        $scope.show_load();

        params = {
            _token: csrf_token,
            id_usuario: id_usuario,
            id_ticket: id_ticket,
            recebe_email: $scope.ticket.recebe_email
        };

        $.post(site_url + 'site/artwork/services/package/ticket/seguir', params, function (data) {
            if (data.error) {
                myAlert(data.message, 'error');
                $scope.carregar_ticket(1);
            } else {
                myAlert(data.message, 'success');
                $scope.carregar_ticket(1);
            }

            $scope.interval_tickets_start();
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.interval_tickets_stop();
                    $scope.hide_load();
                });
    };

    $scope.cancelar_ticket_service = function () {
        $scope.show_load();
        $scope.interval_tickets_stop();

        params = {
            _token: csrf_token,
            id: $scope.ticket.id,
            motivo: $scope.cancelamento_descricao,
            acao: 'cancelar'
        };

        $.post(site_url + 'site/artwork/services/package/ticket/pausarCancelar', params, function (data) {
            myAlert('Ticket cancelado com sucesso.', 'success');
            $scope.cancelamento_descricao = '';
            $scope.carregar_ticket(1);
            $scope.interval_tickets_start();
            location.href = '#/tarefas';
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });

        $scope.cancelamento_descricao = '';
    };

    $scope.cancelar_ticket_confirm = function () {
        
        if ($scope.ticket.id_status == 0 || $scope.ticket.id_status == 9) {
            
            var message = 'Este ticket não possui participantes o suficiente para ser cancelado, portanto ele será excluído sem deixar registros. Deseja confirmar?';
            
        } else {
            
            var message = 'Cancelar o ticket é uma ação irreversível, tem certeza que deseja cancelar?';
            
        }
        
        $.confirm({
            icon: 'fa fa-question',
            title: 'Confirmar',
            content: message,
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            useBootstrap: false,
            boxWidth: '910px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        $scope.cancelar_ticket();
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {}
                }
            },
            onOpen: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };

    $scope.cancelar_ticket = function () {
        $ngConfirm({
            useBootstrap: false,
            boxWidth: '910px',
            title: '',
            contentUrl: '/view/modal_cancelar_ticket.html',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'ng-confirm-btn-close fa fa-times',
            backgroundDismiss: true,
            escapeKey: 'Sair',
            buttons: {
                Sair: {
                    keys: ['esc'],
                    text: 'Sair',
                    btnClass: 'btn-control btn-alt',
                    action: function (Sair) {}
                },
                Concluir: {
                    keys: ['enter', 'space'],
                    text: 'Concluir',
                    btnClass: 'btn-control btn-positive',
                    action: function (Concluir) {
                        var descricao = $scope.cancelamento_descricao;

                        if (descricao === '') {
                            $('#cancelamento_descricao').addClass('has-error');

                            myAlert('Informe o motivo do cancelamento do ticket.', 'warning');

                            return false;
                        }

                        $('#cancelamento_descricao').removeClass('has-error');

                        $scope.cancelar_ticket_service();
                    }
                }
            },
            onReady: function () {
                $timeout(function () {
                    $('.ng-confirm-box .line-wobble').remove();
                    $('.ng-confirm-content').css('visibility', 'visible');
                    $('.ng-confirm-buttons').show();
                }, new Date().getMilliseconds());
            },
            onOpenBefore: function() {
                setTimeout(function () {
                    $('.ng-confirm .ng-confirm-scrollpane').overlayScrollbars({
                        scrollbars: {
                            autoHide: 'move'
                        }
                    });
                }, 0);
            },
            onOpen: function () {
                $('.ng-confirm-content').css('visibility', 'hidden');
                $('body').addClass('no-scroll');
                $('.ng-confirm-buttons').hide();
                $('.ng-confirm-box').append(customLoadingHTML);
            }
        });

    };

    $scope.pausar_ticket_service = function () {
        $scope.show_load();
        $scope.interval_tickets_stop();

        params = {
            _token: csrf_token,
            id: $scope.ticket.id,
            acao: 'pausar'
        };

        $.post(site_url + 'site/artwork/services/package/ticket/pausarCancelar', params, function (data) {
            myAlert('Ticket pausado com sucesso.', 'success');
            $scope.carregar_ticket(1);
            $scope.interval_tickets_start();
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });

        $scope.cancelamento_descricao = '';
    };

    $scope.pausar_ticket = function () {
        var message = 'Deseja pausar este ticket?';

        $.confirm({
            icon: 'fa fa-question',
            title: 'Confirmar',
            content: message,
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            useBootstrap: false,
            boxWidth: '910px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        angular.element('.pause-ticket-switch input').prop('checked', true);
                        $scope.pausar_ticket_service();
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {
                        angular.element('.pause-ticket-switch input').prop('checked', false);
                    }
                }
            },
            onOpen: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };
});