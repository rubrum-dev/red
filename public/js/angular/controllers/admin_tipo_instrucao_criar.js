var site_url = '/';
var app = angular.module('app', []);
app.controller('TipoInstrucaoCriarCtrl', function ($scope) {
    $scope.nomeTipoInstrucao = '';
    $scope.chkTipoInstrucaoAtivo = false;
    $scope.show_load = function () {
        $('.master').addClass('blurred');
        $('.lds-css').show();
    };
    $scope.hide_load = function () {
        $('.master').removeClass('blurred');
        $('.lds-css').hide();
    };
    $scope.enviarFormulario = function (form) {
        $scope.show_load();
        
        params = {
            _token: csrf_token,
            nomeTipoInstrucao: $scope.nomeTipoInstrucao,
            chkTipoInstrucaoAtivo: $scope.chkTipoInstrucaoAtivo
        }
        $.post(site_url + 'site/tipos-instrucao/criar', params, function (data) {
            if (data.error) {
                myAlert(data.message, 'error');
            } else {
                $.confirm({
                    animationSpeed: 200,
                    animateFromElement: false,
                    draggable: false,
                    icon: 'fa fa-check',
                    title: 'Sucesso',
                    content: 'Tipo de instrução criado com sucesso.',
                    closeIcon: true,
                    boxWidth: '910px',
                    scrollToPreviousElement: false,
                    useBootstrap: false,
                    closeIconClass: 'jconfirm-btn-close fa fa-times',
                    buttons: {
                        Entendi: {
                            keys: ['esc', 'enter', 'space'],
                            text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                            btnClass: 'btn-confirm my-alert-confirm',
                            action: function (Todos) {
                                location = data.url;
                            }
                        }
                    },
                    onOpen: function () {
                        $('body').addClass('jconfirm-overlay');
                        $('body').addClass('no-scroll');
                        $('.master').addClass('blurred');
                        $('.jconfirm').css('overflow', 'auto');
                        $scope.hide_load();
                    },
                    onClose: function () {
                        $('body').removeClass('jconfirm-overlay');
                        $('body').removeClass('no-scroll');
                        $('.master').removeClass('blurred');
                        $('.jconfirm').css('overflow', 'hidden');
                        $scope.hide_load();
                    }
                });
            }
        }, 'json')
        .fail(function (data) {
            myAlert('Ocorreu um erro.', 'error');
        });
    };
});