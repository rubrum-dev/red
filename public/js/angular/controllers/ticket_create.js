var site_url = '/';
var app = angular.module('app', [
    'cp.ngConfirm',
    '720kb.tooltips',
    'ngAnimate', 
    'selectize', 
    'ngFileUpload',
    'compartilharItemMod'
]);

app.directive('fileUpload', function () {
    return {
        scope: true, //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                var uploadObj = el.context.name;
                //iterate files since 'multiple' may be specified on the element
                for (var i = 0; i < files.length; i++) {
                    //emit event upward
                    scope.$emit("fileSelected", {
                        file: files[i]
                    });
                }
            });
        }
    };
});

app.filter('fileSize', function () {
    return function (size) {
        if (isNaN(size))
            size = 0;

        if (size < 1024)
            return size + ' Bytes';
        size /= 1024;

        if (size < 1024)
            return size.toFixed(2) + ' Kb';
        size /= 1024;

        if (size < 1024)
            return size.toFixed(2) + ' Mb';
        size /= 1024;

        if (size < 1024)
            return size.toFixed(2) + ' Gb';
        size /= 1024;

        return size.toFixed(2) + ' Tb';
    };
});

app.directive('autoHeight', function () {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element = element[0];
            
            var resize = function() {
                element.style.height = 'auto';
                element.style.height = (element.scrollHeight)+'px';
            };
            
            element.addEventListener('change', resize, false);
            element.addEventListener('cut', resize, false);
            element.addEventListener('paste', resize, false);
            element.addEventListener('drop', resize, false);
            element.addEventListener('keydown', resize, false);

            setTimeout(resize, 0);
        }
    }
});

app.controller('TicketCreateCtrl', function ($scope, $http, $rootScope, $timeout, $ngConfirm, Upload) {
    $scope.arquivos = [];
    
    $scope.solicitacoes = solicitacoes;

    $scope.cbProjeto = '';
    
    $scope.tipos = [];

    $scope.projetoConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        maxItems: 1,
        render: {
            option: function (data, escape) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };

    $scope.show_load = function () {
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        $('.lds-css').hide();
    };

    $scope.carregar_tipos = function () {
        $scope.show_load();

        $.get(site_url + 'site/artwork/services/package/request/types', function (data) {
            $scope.tipos = data;

            $scope.$apply();
            $scope.hide_load();

        }, 'json');
    };

    $scope.projetos = [{ 
        id: 0, nome: 'Novo Projeto' 
    }];

    $scope.carregar_projetos = function () {
        $scope.show_load();

        $.get(site_url + 'site/artwork/services/package/projetos', function (data) {
            $scope.projetos = data;

            $scope.$apply();
            $scope.hide_load();

        }, 'json');
    };

    $scope.adicionar = function () {
        if ($scope.add_tipo && $scope.add_descricao) {
            var template = {
                'id': $scope.add_tipo,
                'descricao': $scope.add_descricao
            };
            
            $scope.solicitacoes.push(template);

            $scope.add_tipo = '';
            $scope.add_descricao = '';

            $('#alterationTypeWrap .add-instruction-col button').removeClass('has-error');
            $('.request-type .input-fields-group:last select').removeClass('has-error');
            $('.request-type .input-fields-group:last textarea').removeClass('has-error');
            $('.request-type .input-fields-group:last textarea').height(0);
        } else {
            $('.request-type .input-fields-group:last select').addClass('has-error');
            $('.request-type .input-fields-group:last textarea').addClass('has-error');

            myAlert('Selecione o tipo de alteração e infome a descrição específica.', 'warning');
        }
    };

    $scope.remover = function (k) {
        $scope.solicitacoes.splice(parseInt(k), 1);
    };
    /* Função responsável por definir extenção de arquivo no box de upload */
    //$scope.fileExtensionClass = function (arquivo) {
    //    var ext = arquivo.descricao.substring(arquivo.descricao.lastIndexOf('.') + 1)
    //    switch (ext) {
    //        case 'jpg':
    //        case 'jpeg':
    //        case 'png':
    //        case 'bmp':
    //        case 'gif':
    //        case 'tif':
    //        case 'tiff':
    //            return 'fa-file-image-o';
    //            break;
    //        case 'pdf':
    //            return 'fa-file-pdf-o';
    //            break;
    //        case 'doc':
    //        case 'docx':
    //            return 'fa-file-word-o';
    //            break;
    //        case 'xls':
    //        case 'xlsx':
    //            return 'fa-file-excel-o';
    //            break;
    //        case 'ppt':
    //        case 'pptx':
    //            return 'fa-file-powerpoint-o';
    //            break;
    //        case 'zip':
    //        case 'rar':
    //        case '7z':
    //            return 'fa-file-archive-o';
    //            break;
    //        case 'txt':
    //            return 'fa-file-text-o';
    //        default:
    //            return 'fa-file-o';
    //            break;
    //    }
    //};
    /* Adicionar arquivo */
    /*$scope.add_arquivo = function (arquivo) {
        $scope.arquivo = document.getElementById('arquivo').files[0];

        if (!$scope.arquivo) {
            angular.element('#fileAttachWrap').find('.files-control-fake').addClass('has-error');

            myAlert('Não foi anexado um aquivo.', 'warning');
            return false;
        }

        if ($scope.arquivo.size > 52428800) {
            $('.workflow-file-upload').find('.files-control-fake').addClass('has-error');
            myAlert('O tamanho máximo permitido para um arquivo é 50MB.', 'warning');
            return false;
        }

        arquivo = {
            descricao: $scope.arquivo.name,
            data_criacao: new Date().toLocaleDateString(),
            filesize: $scope.arquivo.size
        }

        $scope.arquivos.push(arquivo);

        $scope.limparCamposUpload();

        angular.element('#fileAttachWrap').find('.files-control-fake').removeClass('has-error');
    };*/
    /* Remover arquivo */
    /*$scope.remover_arquivo = function ($index) {
        $scope.arquivos.splice($index, 1)
    };*/

    $scope.validar = function ($event) {
        if (!edicao) {
            if (angular.element('#txtMainDescription').val() === '') {
                angular.element('#txtMainDescription').focus();
                angular.element('#txtMainDescription').addClass('has-error');
                
                myAlert('Descreva resumidamente o conteúdo deste ticket em Descrição Geral.', 'Atenção', 'warning');

                $event.preventDefault();

                return false;
            } else if ($scope.cbProjeto === '0' && angular.element('#tfNomeProjeto').val() === '') {
                angular.element('#tfNomeProjeto').focus();
                angular.element('#tfNomeProjeto').addClass('has-error');
                
                myAlert('Digite o nome do projeto.', 'Atenção', 'warning');

                $event.preventDefault();

                return false;
            } else if (angular.element('#tfArtSendDate').val() === '') {
                angular.element('#txtMainDescription').removeClass('has-error');
                angular.element('#tfNomeProjeto').removeClass('has-error');
                angular.element('#tfArtSendDate').focus();
                angular.element('#tfArtSendDate').addClass('has-error');
                
                myAlert('Informe a data para que a arte seja enviada para aprovação.', 'warning');
            
                $event.preventDefault();

                return false;
            } else if (angular.element('#tfDeadlineDate').val() === '') {
                angular.element('#tfArtSendDate').removeClass('has-error');
                angular.element('#tfDeadlineDate').focus();
                angular.element('#tfDeadlineDate').addClass('has-error');
                
                myAlert('Informe a data que a arte deve ser enviada para o fornecedor.', 'warning');
            
                $event.preventDefault();

                return false;
            } else if (!$scope.add_tipo && $scope.solicitacoes.length === 0) {
                angular.element('#tfDeadlineDate').removeClass('has-error');
                angular.element('#alterationTypeWrap .input-fields-group select:last').focus();
                angular.element('#alterationTypeWrap .input-fields-group select:last').addClass('has-error');
                
                myAlert('Selecione o Tipo de Alteração.', 'warning');
                
                $event.preventDefault();

                return false;
            } else if (!$scope.add_descricao && $scope.solicitacoes.length === 0) {
                angular.element('#alterationTypeWrap .input-fields-group select').removeClass('has-error');
                angular.element('#alterationTypeWrap .input-fields-group textarea:last').focus();
                angular.element('#alterationTypeWrap .input-fields-group textarea:last').addClass('has-error');
                
                myAlert('Descreva com detalhes a alteração.', 'warning');
                
                $event.preventDefault();

                return false;
            } else if ($scope.add_tipo || $scope.add_descricao) {
                angular.element('#alterationTypeWrap .input-fields-group select:last').removeClass('has-error');
                angular.element('#alterationTypeWrap .input-fields-group textarea:last').removeClass('has-error');
                angular.element('#alterationTypeWrap .add-instruction-col button').addClass('has-error');
                
                myAlert('Clique em Adicionar para incluir a alteração no ticket.', 'warning');
                
                $event.preventDefault();

                return false;
            } else if (angular.element('#fileAttachWrap input[type="file"]:last').val() !== '' && angular.element('#fileAttachWrap input[type="text"]:last').length <= 1 || angular.element('#fileAttachWrap input[type="file"]:last').val() !== '') {
                $('#fileAttachWrap .add-files-submit-col .btn-add-fields').addClass('has-error');
                
                myAlert('Clique em Adicionar para incluir o arquivo anexo no ticket.', 'warning');
                
                $event.preventDefault();

                return false;
            }

        } 
    };
    
    /*$scope.limparCamposUpload = function () {
        $('.files-control').find('input[type="file"]').val('');
        $('.files-control').find('.files-control-fake strong').text('Anexar arquivo');
    };*/
    
    $scope.carregar_tipos();
    $scope.carregar_projetos();
});