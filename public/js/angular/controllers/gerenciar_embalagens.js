var site_url = '/';
var app = angular.module('app', ['cp.ngConfirm', 'ngAnimate']);

app.controller('GerenciarEmbalagensCtrl', function ($scope, $ngConfirm) {
    $scope.embalagens = [
        {
            'name': 'Skol Pilsen Lata 269ml',
            'language': 'BR, AG'
        },
        {
            'name': 'Skol Pilsen Lata 350ml',
            'language': 'BR, AG, PY'
        },
        {
            'name': 'Skol Pilsen Garrafa 355ml',
            'language': 'BR, AG'
        },
        {
            'name': 'Skol Pilsen Garrafa 600ml',
            'language': 'BR, AG'
        },
        {
            'name': 'Skol Pilsen Garrafa 1L',
            'language': 'BR, AG, PT'
        }
    ]
        
    $scope.show_load = function () {
        $('.master').addClass('blurred');
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        $('.master').removeClass('blurred');
        $('.lds-css').hide();
    };
});

