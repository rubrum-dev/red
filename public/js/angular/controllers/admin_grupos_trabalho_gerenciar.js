var site_url = window.location.origin;
var dataTableToolbarBtnDOM = '<div class="flex flex-auto">' +
    '<a href="' + site_url + '/admin/grupos-trabalho/adicionar" class="btn-function"><i class="icon fa fa-handshake-o"></i> Adicionar Grupo de Trabalho</a>' +
    '</div>';
var app = angular.module('app', ['ngAnimate', 'ngSanitize', 'ngResource', 'ngRoute', 'datatables', 'selectize', 'cp.ngConfirm', 'adminGruposTrabalhoResolveLoader', 'admGruposTrabalhoEditar']);

app.run(function ($templateCache, $http) {
    $http.get('/view/adm_grupo_trabalho_gerenciar.html', {
        cache: $templateCache,
        headers: {
            'Cache-Control': 'no-cache'
        }
    });
    $http.get('/view/adm_grupo_trabalho_editar.html', {
        cache: $templateCache,
        headers: {
            'Cache-Control': 'no-cache'
        }
    });
});

app.run(initDT);

function initDT(DTDefaultOptions) {
    DTDefaultOptions.setLoadingTemplate('');
}

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/view/adm_grupo_trabalho_gerenciar.html',
            controller: 'AdmGruposTrabalhoCtrl',
            resolve: {
                isOffset: function ($location, $rootScope) {}
            },
            delay: function($q, $timeout){
                var delay = $q.defer();
                $timeout(delay.resolve, new Date().getMilliseconds());
                return delay.promise;
            }
        })
        .when('/editar/:id', {
            templateUrl: '/view/adm_grupo_trabalho_editar.html',
            controller: 'AdmGruposTrabalhoEditarCtrl',
            resolve: {
                isOffset: function ($location, $rootScope) {}
            },
            delay: function($q, $timeout){
                var delay = $q.defer();
                $timeout(delay.resolve, new Date().getMilliseconds());
                return delay.promise;
            }
        })
        .otherwise({
            redirectTo: '/'
        });
});

app.controller('AdmGruposTrabalhoCtrl', function (
    DTOptionsBuilder,
    DTColumnDefBuilder,
    $scope,
    $rootScope,
    $compile,
    $resource,
    $http,
    $routeParams,
    $timeout,
    $ngConfirm
) {
    $rootScope.loaded = false;

    $rootScope.grupos = {
        locais: [],
        globais: []
    };

    $rootScope.grupo = {};

    $rootScope.participantes = [];
    $scope.cbMembershipProfile = [];

    $scope.dtOptions = {
        dom: '<"datatable-toolbar flexbox-container clearfix"' +
            '<"datatable-filter-container flex flex-3-large"f>' +
            '>' +
            '<"clearfix"' +
            '<tr>' +
            '>' +
            '<"datatable-pagination flexbox-container flex-align-center clearfix"' +
            '<"flex"<"select-control"l>><"flex flex-auto"p><"flex"i>' +
            '>',
        bAutoWidth: false,
        processing: false,
        serverSide: false,
        stateSave: true,
        pageLength: 25,
        lengthMenu: [
            [10, 25, 50, -1],
            ['Exibir 10 por página', 'Exibir 25 por página', 'Exibir 50 por página', 'Exibir todos']
        ],
        columnDefs: [{
            orderable: false,
            targets: 2
        }],
        language: {
            "url": "/js/datatables/language/Portuguese-Brasil.json"
        },
        initComplete: function () {
            $('.datatable-filter-container .dataTables_filter input[type="search"]').attr('placeholder', 'Busca');

            var dataTables_filter_cancel = '<i></i>';
            var dataTables_filter_search = '<i></i>';

            $('.datatable-filter-container .dataTables_filter').append(dataTables_filter_search);
            $('.datatable-filter-container .dataTables_filter').append(dataTables_filter_cancel);
            $('.datatable-filter-container .dataTables_filter i').addClass('dataTables_filter_search');
            $('.datatable-filter-container .dataTables_filter i + i').addClass('dataTables_filter_cancel');

            function dataTablesFilterCancelShowHide() {
                if ($('.datatable-filter-container .dataTables_filter input[type="search"]').val() !== '') {
                    $('.dataTables_filter_search').hide();
                    $('.dataTables_filter_cancel').show();

                } else {
                    $('.dataTables_filter_search').show();
                    $('.dataTables_filter_cancel').hide();
                    $('#workgroupsManagerTable').dataTable().api().search(jQuery.fn.DataTable.ext.type.search.string(this.value)).draw();
                }
            }

            $(document).on('click', '.datatable-filter-container .dataTables_filter_cancel', function () {
                $('#workgroupsManagerTable_filter input[type="search"]').val('');

                dataTablesFilterCancelShowHide();
            });

            // Remove accented character from search input as well
            $(document).on('keyup', '#workgroupsManagerTable_filter input[type="search"]', function () {
                dataTablesFilterCancelShowHide();
            });
            
            $('.dataTable thead').show();
            $('.dataTables_filter').show();
            $('.datatable-toolbar').append(dataTableToolbarBtnDOM);
        },
        fnDrawCallback: function (oSettings) {
            if (oSettings.aiDisplay.length <= 0) {
                $('.dataTable thead th').hide();
                $('.datatable-pagination').hide();
                $('.dataTables_empty').addClass('fa-handshake-o');
                $('.dataTables_empty').text('Nenhum grupo de trabalho encontrado.');
            } else {
                $('.dataTable thead th').show();
                $('.datatable-pagination').show();
                $('.dataTables_empty').removeClass('fa-handshake-o');
                $('.dataTables_empty').text('');
            }
        }
    };

    $scope.show_load = function () {
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        $('.lds-css').hide();
    };

    $scope.carregar_grupos = function () {
        $rootScope.loaded = false;
        
        $http.get(site_url + 'site/artwork/services/package/getGruposDeAprovacao').success(function (data) {
            $rootScope.grupos = data;
            $rootScope.loaded = true;
        });
    };

    $scope.selecionar_grupo = function (grupo) {
        $rootScope.participantes = [];

        angular.forEach(grupo.artw_grupos_perfis_usuarios, function (participante, key) {
            objeto = {
                'id_usuario': participante.id_usuario,
                'id_perfil': participante.id_perfil,
                'recebe_email': '',
                'adiciona_equipe': 0,
                'add_participante': '',
                'equipe_membros': participante.artw_grupos_perfis_usuarios_equipe
            };

            $rootScope.participantes.push(objeto);
        });

        localStorage.setItem('participantes', JSON.stringify($rootScope.participantes));

        $scope.$apply();
    };

    $scope.editar_grupo_trabalho = function (grupo) {
        localStorage.clear();
        localStorage.setItem('grupo', JSON.stringify(grupo));
        localStorage.setItem('grupo_nome', grupo.nome);
        localStorage.setItem('grupo_global', grupo.global);
        localStorage.setItem('grupo_id', grupo.id);

        $scope.grupo_nome = grupo.nome;
        $scope.grupo_id = grupo.id;
        $scope.grupo_global = (grupo.global == 1) ? true : false;

        $timeout(function () {
            $scope.selecionar_grupo(grupo);
        }, 100);
    };

    $scope.excluir_grupo_trabalho_service = function (grupo_id) {
        $scope.show_load();

        params = {
            _token: csrf_token,
            grupo_id: grupo_id
        };

        url = site_url + 'site/artwork/services/package/postExcluirGruposDeAprovacao';

        $.post(url, params, function (data) {
                myAlert('Grupo de Trabalho excluído com sucesso.', 'success');
                $scope.$apply();
                $scope.hide_load();
                $scope.carregar_grupos();
            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro!', 'error');
                $scope.hide_load();
            });
    };

    $scope.excluir_grupo_trabalho = function (grupo_id) {
        $scope.deleteWorkGroupModal = $.confirm({
            icon: 'fa fa-question',
            title: 'Confirmar',
            content: 'Deseja excluir esse grupo de trabalho?',
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            useBootstrap: false,
            boxWidth: '910px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        $scope.excluir_grupo_trabalho_service(grupo_id);
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {}
                }
            },
            onOpenBefore: function () {
                //$('body').addClass('jconfirm-overlay');
                $('body').addClass('no-scroll');
                //$('.master').addClass('blurred');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                //$('body').removeClass('jconfirm-overlay');
                $('body').removeClass('no-scroll');
                //$('.master').removeClass('blurred');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };

    $scope.carregar_grupos();
});