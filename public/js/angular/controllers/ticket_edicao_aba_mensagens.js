var mensagens = angular.module("abaMensagensMod", []);

mensagens.controller('abaMensagensCtrl', function (
        $scope,
        $rootScope
    ) {
    $rootScope.statechange = true;
    
    $scope.adicionar_mensagem = function () {
        console.log($rootScope.msgTicket);
        
        if ($rootScope.msgTicket === null || $rootScope.msgTicket === undefined || $rootScope.msgTicket === '') {
            $('.conversation').find('.textbox-fake').addClass('has-error');
            $scope.interval_tickets_stop();
            myAlert('Insira uma mensagem.', 'warning');
            return false;
        }

        $scope.show_load();

        params = {
            _token: csrf_token,
            id: $scope.ticket.id,
            texto: $rootScope.msgTicket,
            tipo: 'geral',
            enviar_membros: $scope.memberList,
            modal_enviar: $scope.modal_enviar
        };

        $.post(site_url + 'site/artwork/services/package/comentar', params, function (data) {
            $rootScope.msgTicket = '';
            $scope.carregar_ticket(1);
            $scope.interval_tickets_start();
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
        
        $scope.modal_enviar = false;
            
        $('.conversation').find('.textbox-fake').removeClass('has-error');
    };
});