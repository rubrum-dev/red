var site_url = '/';
var app = angular.module('app', [
    'cp.ngConfirm',
    '720kb.tooltips',
    'ngAnimate',
    'selectize'
]);

app.directive('autoHeight', function () {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element = element[0];
            
            var resize = function() {
                element.style.height = 'auto';
                element.style.height = (element.scrollHeight)+'px';
            };
            
            element.addEventListener('change', resize, false);
            element.addEventListener('cut', resize, false);
            element.addEventListener('paste', resize, false);
            element.addEventListener('drop', resize, false);
            element.addEventListener('keydown', resize, false);

            setTimeout(resize, 0);
        }
    }
});

app.controller('TicketReprovarCtrl', function ($scope) {

    $scope.solicitacoes = solicitacoes;

    $scope.tipos = [];

    $scope.ticket = {};

    $scope.data = {};

    //$scope.id_ticket = id_ticket;

    $scope.show_load = function () {
        $('.lds-css').show();
    }

    $scope.hide_load = function () {
        $('.lds-css').hide();
    }

    $scope.adicionar = function () {
        if ($scope.add_tipo && $scope.add_descricao) {
            var template = {
                'id': $scope.add_tipo,
                'descricao': $scope.add_descricao,
                'nome': $scope.add_titulo,
                'obrigatoria': $scope.obrigatoria
            };

            $scope.solicitacoes.push(template);

            $scope.add_tipo = '';
            $scope.add_descricao = '';
            $scope.add_titulo = '';
            $scope.obrigatoria = '';

            $('#alterationTypeWrap .add-instruction-col button').removeClass('has-error');
            $('.request-type .input-fields-group:last select').removeClass('has-error');
            $('.request-type .input-fields-group:last textarea').removeClass('has-error');
            $('.request-type .input-fields-group:last textarea').height(0);
        } else {
            $('.request-type .input-fields-group:last select').addClass('has-error');
            $('.request-type .input-fields-group:last textarea').addClass('has-error');

            myAlert('Selecione o critério de avaliação e infome a descrição específica.', 'warning');
        }
    }

    $scope.validar = function ($event) {
        if (!edicao) {
            if (!$scope.add_tipo && $scope.solicitacoes.length === 0) {
                angular.element('#tfDeadlineDate').removeClass('has-error');
                angular.element('#alterationTypeWrap .input-fields-group select:last').focus();
                angular.element('#alterationTypeWrap .input-fields-group select:last').addClass('has-error');

                myAlert('Selecione o Critério de Avaliação.', 'warning');

                $event.preventDefault();

                return false;
            } else if (!$scope.add_descricao && $scope.solicitacoes.length === 0) {
                angular.element('#alterationTypeWrap .input-fields-group select').removeClass('has-error');
                angular.element('#alterationTypeWrap .input-fields-group textarea:last').focus();
                angular.element('#alterationTypeWrap .input-fields-group textarea:last').addClass('has-error');

                myAlert('Descreva com detalhes o critério de avaliação.', 'warning');

                $event.preventDefault();

                return false;
            } else if ($scope.add_tipo || $scope.add_descricao) {
                angular.element('#alterationTypeWrap .input-fields-group select:last').removeClass('has-error');
                angular.element('#alterationTypeWrap .input-fields-group textarea:last').removeClass('has-error');
                angular.element('#alterationTypeWrap .add-instruction-col button').addClass('has-error');

                myAlert('Clique em Adicionar para incluir o critério de avaliação no ticket.', 'warning');

                $event.preventDefault();

                return false;
            }
        }
    };

    $scope.remover = function (k) {
        $scope.solicitacoes.splice(parseInt(k), 1);
    };

    $scope.carregar_ticket = function (exibe_loading) {
        if (exibe_loading == 1) {
            $scope.show_load();
        }
        
        $.get(site_url + 'site/artwork/services/package/ticket/' + id_ticket, function (data) {
                $scope.ticket = data;
                
                $scope.$apply();
                
                if (exibe_loading == 1) {
                    $scope.hide_load();
                }
            }, 'json')
            .fail(function (data) {
                if (exibe_loading == 1) {
                    $scope.hide_load();
                }
            });
    };

    $scope.carregar_ticket(1);

    $scope.carregar_condicoes_midia = function () {

        $scope.show_load();

        $.get(site_url + 'site/adsmart/services/condicoesMidia/reprovar', function (data) {

                $scope.condicoesMidia = data;

                angular.forEach($scope.condicoesMidia, function(condicao, key){
                    
                    if (condicao.obrigatoria)
                    {
                         //$scope.add_tipo = condicao.id;
                         //$scope.add_descricao = condicao.texto;
                         //$scope.add_titulo = condicao.nome;
                         //$scope.obrigatoria = condicao.obrigatoria;

                         //$scope.adicionar();
                    }

                });

                $scope.$apply();

                $scope.hide_load();

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
    };

    $scope.carregar_condicoes_midia();

    $scope.getById = function (id, array) {
        for (i = 0; i < array.length; i++) {
            if (array[i].id == id) {
                return array[i];
            }
        }
    };

    $scope.addTipo = function () {

        var condicao = $scope.getById($scope.add_tipo, $scope.condicoesMidia);

        $scope.add_descricao = (condicao) ? condicao.texto : '';
        $scope.add_titulo = (condicao) ? condicao.nome : '';

        $('#alterationTypeWrap .add-instruction-col button').removeClass('has-error');
        $('.request-type .input-fields-group:last select').removeClass('has-error');
        $('.request-type .input-fields-group:last textarea').removeClass('has-error');
    };

});