var site_url = '/';
var app = angular.module('app', ['selectize', '720kb.tooltips']);

app.controller('PackageCreateCtrl', function ($scope)
{
    $scope.packageType = packageType;
    
    $scope.packageIsNewProd = '';

    $scope.familias = [];
    
    $scope.cbPckFamily = cbPckFamily;
    
    $scope.produtos = [];
    
    $scope.cbPckProduct = cbPckProduct;
    
    $scope.tipos = [];
    
    $scope.tiposItens = [];

    $scope.tamanhos = [];
    
    $scope.categorias = [];
    
    $scope.variacoes = [];

    $scope.cbNewCategory = cbNewCategory;
    
    $scope.cbPckType = cbPckType;
    
    $scope.cbPckTypeItem = cbPckTypeItem;

    $scope.cbPckNatureza = cbPckNatureza;

    $scope.pckEANCode = pckEANCode;
    $scope.txtPckEANCode = txtPckEANCode;
    
    $scope.campanhas = [{
        'id': '0',
        'nome': 'Cadastrar nova campanha'
    }];

    $scope.cbPckCampaign = cbPckCampaign;

    $scope.naturezas = [{
        id: 2,
        nome: 'Primária'
    }, {
        id: 3,
        nome: 'Secundária'
    }, {
        id: 4,
        nome: 'Terciária'
    }];

    $scope.embalagens_primarias = [{
        id: 1,
        nome: 'Skol Garrafa Vidro 600ml'
    }, {
        id: 2,
        nome: 'Skol Garrafa Vidro 600ml • OW'
    }, {
        id: 3,
        nome: 'Skol Garrafa Vidro 600ml • Carnaval 2019'
    }, {
        id: 4,
        nome: 'Skol Garrafa Vidro 600ml • Rótulo Quadrado'
    }];
    
    $scope.embalagens_secundarias = [{
        id: 1,
        nome: 'Skol Garrafa Vidro 600ml'
    }, {
        id: 2,
        nome: 'Skol Garrafa Vidro 600ml • OW'
    }, {
        id: 3,
        nome: 'Skol Garrafa Vidro 600ml • Carnaval 2019'
    }, {
        id: 4,
        nome: 'Skol Garrafa Vidro 600ml • Rótulo Quadrado'
    }];

    $scope.primaryPackageConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Indique as embalagens primárias contidas',
        maxItems: 1,
        render: {
            option: function (data) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };

    $scope.secondaryPackageConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Indique as embalagens secundárias contidas',
        maxItems: 1,
        render: {
            option: function (data) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };


    $scope.cbPckContentSize = cbPckContentSize;

    $scope.variacaoName = variacao_edicao;
    $scope.variacao_edicao = variacao_edicao;
    
    $scope.packageHasVariation = (!variacao_edicao) ? 'Nao' : 'Sim';
    
    $scope.cbPckMarket = mercados;
            
    $scope.myConfig = {
        create: false,
        valueField: 'id',
        labelField: 'title',
        searchField: ['title'],
        options: '',
        onInitialize: function (selectize) {},
        onDropdownOpen: function () {}
    };
    
    $scope.productsConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecione o produto',
        maxItems: 1,
        render: {
            option: function (data) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };
    
    $scope.packageTypeConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecione o tipo de embalagem',
        maxItems: 1,
        render: {
            option: function (data) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };

    $scope.packageNaturezaConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecione',
        maxItems: 1,
        render: {
            option: function (data) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };
    
    $scope.contentsConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecione o volume',
        maxItems: 1,
        render: {
            option: function (data, escape) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };

    $scope.campaignConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecione a campanha',
        maxItems: 1,
        options: {
            id: 0,
            nome: 'Cadastrar nova campanha'
        },
        render: {
            option: function (data, escape) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };

    $scope.marketConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecione os mercados',
        options: {
            id: 0,
            nome: 'Cadastrar novo mercado'
        },
        render: {
            option: function (data, escape) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };
    
    $scope.show_load = function ()
    {
        $('.lds-css').show();
    };

    $scope.hide_load = function ()
    {
        $('.lds-css').hide();
    };

    $scope.uncheckFirstRadioIfApplicable = function() {
        if($scope.pckHasEANCode !== '') {
            $scope.pckEANCode = '';
        }
    };

    $scope.checkFirstRadioIfNotApplicable = function() {
        $scope.pckHasEANCode = '';
    };

    $scope.carregar_produtos = function (cbPckProduct)
    {
        cbPckProduct = (typeof cbPckProduct !== 'undefined') ? cbPckProduct : '';

            $scope.show_load();

            $.get(site_url + 'site/artwork/services/package/products/', function (data)
            {
                $scope.produtos = data;
                $scope.cbPckProduct = cbPckProduct;

                if (!cbPckProduct) {
                    $scope.cbPckType = '';
                    $scope.cbPckContentSize = '';
                    $scope.tamanhos = [];
                }

                $scope.$apply();
                $scope.hide_load();

            }, 'json')
                    .fail(function (data) {
                        myAlert('Ocorreu um erro.', 'error');
                        $scope.hide_load();
                    });
    };

    $scope.carregar_tipos = function (cbPckType, manterNatureza)
    {
        cbPckType = (typeof cbPckType !== 'undefined') ? cbPckType : '';
        
        if ($scope.cbPckProduct != '')
        {
            $.get(site_url + 'site/artwork/services/package/types', function (data)
            {
                $scope.show_load();

                $scope.tipos = data;
                //$scope.cbPckType = cbPckType;

                if (!cbPckType) {
                    //$scope.tamanhos = [];
                    $scope.cbPckContentSize = '';
                }

                $scope.$apply();
                $scope.hide_load();

            }, 'json')
                    .fail(function (data) {
                        myAlert('Ocorreu um erro.', 'error');
                        $scope.hide_load();
                    });
        } else {
            $scope.tamanhos = [];
            $scope.cbPckType = '';
            $scope.cbPckContentSize = '';
        }

        $scope.limpar_embalagens_selecionadas(1);

        if (manterNatureza){
            $scope.cbPckNatureza = cbPckNatureza;
        }
    };

    $scope.carregar_tipos_itens = function (cbPckTypeItem)
    {
        cbPckTypeItem = (typeof cbPckTypeItem !== 'undefined') ? cbPckTypeItem : '';

        if ($scope.cbPckContentSize != '')
        {
            $.get(site_url + 'site/artwork/services/package/typesItems', function (data)
            {
                $scope.show_load();

                $scope.tiposItens = data;
                $scope.cbPckTypeItem = cbPckTypeItem;

                $scope.$apply();
                $scope.hide_load();

            }, 'json')
                    .fail(function (data) {
                        myAlert('Ocorreu um erro.', 'error');
                        $scope.hide_load();
                    });
        } else {
            $scope.tiposItens = [];
            $scope.cbPckTypeItem = '';
    }
    };

    $scope.getMercadoById = function(id) {
        
        for(i = 0; i < $scope.mercados.length; i++) {
            if($scope.mercados[i].id == id) {
                return $scope.mercados[i];
            }
        }
    };

    $scope.carregar_campanhas = function (cbPckCampaign)
    {
        cbPckCampaign = (typeof cbPckCampaign !== 'undefined') ? cbPckCampaign : '';
        
        if ($scope.cbPckProduct != '' && $scope.cbPckProduct != 0)
        {
            $scope.show_load();

            $.get(site_url + 'site/artwork/services/package/promotions/' + $scope.cbPckProduct, function (data)
            {
                $scope.campanhas = data;
                $scope.cbPckCampaign = cbPckCampaign;
                
                if (!cbPckCampaign) {
                }

                $scope.campanhas = [{
                    'id': '0',
                    'nome': 'Cadastrar nova campanha'
                }];
                
                $scope.campanhas.push(data);
                $scope.packageCampaignName = '';
                $scope.$apply();
                $scope.hide_load();
            }, 'json')
                    .fail(function (data) {
                        myAlert('Ocorreu um erro.', 'error');
                        $scope.hide_load();
                    });
        } else {
            $scope.campanhas = [{
                        'id': '0',
                        'nome': 'Cadastrar nova campanha'
                    }];
            $scope.tamanhos = [];
            $scope.cbPckType = '';
            $scope.cbPckContentSize = '';
        }
    };

    $scope.carregar_tamanhos = function (cbPckContentSize)
    {
        cbPckContentSize = (typeof cbPckContentSize !== 'undefined') ? cbPckContentSize : '';

        if ($scope.cbPckType != '' && $scope.cbPckType != 0)
        {
            $scope.show_load();

            $.get(site_url + 'site/artwork/services/package/types/' + $scope.cbPckType + '/sizes', function (data)
            {
                $scope.tamanhos = data;
                $scope.cbPckContentSize = cbPckContentSize;

                $scope.$apply();
                
                $scope.hide_load();

            }, 'json')
                    .fail(function (data) {
                        myAlert('Ocorreu um erro.', 'error');
                        $scope.hide_load();
                    });
        } else {
            $scope.tamanhos = [];
            $scope.cbPckContentSize = '';
    }
    };

    $scope.carregar_variacoes = function ()
    {
        if ($scope.cbPckContentSize != '' && $scope.cbPckContentSize != 0)
        {
            $scope.show_load();

            $.get(site_url + 'site/artwork/services/package/variacoes/' + $scope.cbPckProduct + '/' + $scope.cbPckContentSize + '/' + $scope.cbPckCampaign, function (data)
            {
                $scope.variacoes = data;
                $scope.$apply();
                $scope.hide_load();

            }, 'json')
                    .fail(function (data) {
                        myAlert('Ocorreu um erro.', 'error');
                        $scope.hide_load();
                    });
        }
    };
    
    $scope.carregar_mercados = function () {
        $scope.show_load();
        $.get(site_url + 'site/artwork/services/package/mercados', function (data) {
                $scope.mercados = data;
                $scope.$apply();
                $scope.hide_load();

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
    };
    
    $scope.retornar_embalagem_mercado = function (id_variacao) {

        params = {
            _token: csrf_token,
            id_variacao: id_variacao
        };

        $.post(site_url + 'site/artwork/services/package/retornarEmbalagemMercado', params, function (data) {

            if (data.error) {
                myAlert(data.message, 'error');
            } else {
                
                $.confirm({
                        animationSpeed: 200,
                        animateFromElement: false,
                        draggable: false,
                        icon: 'fa fa-exclamation',
                        title: 'Confirmar',
                        content: data.message,
                        closeIcon: true,
                        boxWidth: '910px',
                        scrollToPreviousElement: false,
                        useBootstrap: false,
                        closeIconClass: 'jconfirm-btn-close fa fa-times',
                        buttons: {
                            Entendi: {
                                keys: ['esc', 'enter', 'space'],
                                text: '<span><i class="fa fa-check"></i>Entendi</span>',
                                btnClass: 'btn-confirm',
                                action: function (Todos) {
                                    location = data.url;
                                }
                            }
                        },
                        onOpen: function () {
                            $('body').addClass('no-scroll');
                            $('.jconfirm').css('overflow', 'auto');
                        },
                        onClose: function () {
                            $('body').removeClass('no-scroll');
                            $('.jconfirm').css('overflow', 'hidden');
                        }
                    });
                
            }

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                });
    };
    
    $scope.enviarFormulario = function (form) {
        
        $scope.show_load();
        
        params = {
            _token: csrf_token,
            id_variacao: id_variacao,
            cbPckProduct: $scope.cbPckProduct,
            cbPckType: $scope.cbPckType,
            cbPckContentSize: $scope.cbPckContentSize,
            packageType: $scope.packageType,
            cbPckCampaign: $scope.cbPckCampaign,
            packageCampaignName: $scope.packageCampaignName,
            cbPckMarket: $scope.cbPckMarket,
            variacaoName: $scope.variacaoName,
            packageHasVariation: $scope.packageHasVariation,
            cbPckNatureza: $scope.cbPckNatureza,
            pckEANCode: $scope.pckEANCode,
            txtPckEANCode: $scope.txtPckEANCode,
            embalagensRelacionadas: $scope.embalagens_selecionadas
        };
        
        $.post(site_url + 'site/artwork/services/package/edit', params, function (data) {

            if (data.error) {
                
                myAlert(data.message, 'error');
                
            } else {
                
                if (data.success == 200){
                    
                    $.confirm({
                        animationSpeed: 200,
                        animateFromElement: false,
                        draggable: false,
                        icon: 'fa fa-check',
                        title: 'Sucesso',
                        content: data.message,
                        closeIcon: true,
                        boxWidth: '910px',
                        scrollToPreviousElement: false,
                        useBootstrap: false,
                        closeIconClass: 'jconfirm-btn-close fa fa-times',
                        buttons: {
                            Entendi: {
                                keys: ['esc', 'enter', 'space'],
                                text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                                btnClass: 'btn btn-confirm',
                                action: function (Todos) {
                                    location = data.url;
                                }
                            }
                        },
                        onOpen: function () {
                            $('body').addClass('no-scroll');
                            $('.jconfirm').css('overflow', 'auto');
                            $scope.hide_load();
                        },
                        onClose: function () {
                            $('body').removeClass('no-scroll');
                            $('.jconfirm').css('overflow', 'hidden');
                            $scope.hide_load();
                        }
                    });
                    
                } else if (data.success == 202){
                    
                    $.confirm({
                        animationSpeed: 200,
                        animateFromElement: false,
                        draggable: false,
                        icon: 'fa fa-exclamation',
                        title: 'Atenção',
                        content: data.message,
                        closeIcon: true,
                        boxWidth: '910px',
                        scrollToPreviousElement: false,
                        useBootstrap: false,
                        closeIconClass: 'jconfirm-btn-close fa fa-times',
                        buttons: {
                            Entendi: {
                                keys: ['esc', 'enter', 'space'],
                                text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                                btnClass: 'btn btn-confirm',
                                action: function (Todos) {
                                }
                            }
                        },
                        onOpen: function () {
                            $('body').addClass('no-scroll');
                            $('.jconfirm').css('overflow', 'auto');
                            $scope.hide_load();
                        },
                        onClose: function () {
                            $('body').removeClass('no-scroll');
                            $('.jconfirm').css('overflow', 'hidden');
                            $scope.hide_load();
                        }
                    });
                    
                } else {
                    
                    $.confirm({
                        animationSpeed: 200,
                        animateFromElement: false,
                        draggable: false,
                        icon: 'fa fa-question',
                        title: 'Confirmar',
                        content: data.message,
                        closeIcon: true,
                        boxWidth: '910px',
                        scrollToPreviousElement: false,
                        useBootstrap: false,
                        closeIconClass: 'jconfirm-btn-close fa fa-times',
                        buttons: {
                            Renovar: {
                                keys: ['enter', 'space'],
                                text: '<span><i class="icon fa fa-check"></i>Retornar para Mercado</span>',
                                btnClass: 'btn btn-confirm',
                                action: function (Todos) {
                                    
                                    $scope.retornar_embalagem_mercado(data.id_variacao);
                                    
                                }
                            },
                            Cancelar: {
                                keys: ['esc'],
                                text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                                btnClass: 'btn btn-cancel'
                            }
                        },
                        onOpen: function () {
                            $('body').addClass('no-scroll');
                            $('.jconfirm').css('overflow', 'auto');
                        },
                        onClose: function () {
                            $('body').removeClass('no-scroll');
                            $('.jconfirm').css('overflow', 'hidden');
                        }
                    });
                    
                }
                
            };

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                });
        
    };

    $scope.getById = function (id, array) {
        for (i = 0; i < array.length; i++) {
            if (array[i].id == id) {
                return array[i];
            }
        }
    };

    /** Adiciona embalagem primária/secundária selecionada a lista **/
    $scope.add_embalagem_selecionada = function() {
        
        if( $scope.getById($scope.cbPckEmbalagens, $scope.embalagens_selecionadas) ) {
            myAlert('Esta embalagem já foi adicionada.', 'warning');

            return false;
        } else if($scope.cbPckEmbalagens === '') {
            myAlert('Não foi selecionada uma embalagem.', 'warning');
            
            return false;
        } else {

            if($scope.cbPckEmbalagens !== '') {
                var i = $scope.getById($scope.cbPckEmbalagens, $scope.embalagens_por_natureza);
                
                $scope.embalagens_selecionadas.push(i)
                $scope.cbPckPrimarias = '';
            }
        
        }
    
    };

    /** Exclui embalagem primária/secundária selecionada a lista **/
    $scope.excluir_embalagem_selecionada = function(index) {
        $scope.embalagens_selecionadas.splice(index, 1);
    };

    /** Limpa a lista de embalagens primárias/secundárias selecionadas **/
    $scope.limpar_embalagens_selecionadas = function(limpa_natureza) {

        if (limpa_natureza)
        {
            $scope.cbPckNatureza = '';
        }

        $scope.embalagens_por_natureza = [];
        $scope.embalagens_selecionadas = [];
        $scope.cbPckEmbalagens = '';
        //$scope.cbPckSecundarias = '';
    };

    $scope.carregar_embalagens_por_natureza = function () {

        $scope.limpar_embalagens_selecionadas();

        id_natureza = $scope.cbPckNatureza;

        if (id_natureza == 2)
        {
            id_natureza_requisicao = '';
        }
        else if (id_natureza == 3)
        {
            id_natureza_requisicao = 2;
        }
        else if (id_natureza == 4)
        {
            id_natureza_requisicao = 3;
        }

        if (id_natureza_requisicao)
        {
            $scope.show_load();

            var url = site_url + 'site/artwork/services/package/getEmbalagensPorNatureza/' + $scope.cbPckProduct + '/' + id_natureza_requisicao;

            $.ajax({
                url: url,
                type: "get",
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.error) {
                        myAlert(data.message, 'error');
                    } else {
                        $scope.embalagens_por_natureza = data;
                        $scope.$apply();
                    }

                    $scope.hide_load();
                },
                error: function () {
                    myAlert('Ocorreu um erro.', 'error');
                }
            });
        }
    };

    if ($scope.packageType == 'Sim') {
        $scope.carregar_campanhas(cbPckCampaign);
    }

    $scope.carregar_produtos(cbPckProduct);

    if ($scope.cbPckProduct) {
        $scope.carregar_tipos(cbPckType, 1);
    }

    if ($scope.cbPckType) {
        $scope.carregar_tamanhos(cbPckContentSize);
    }

    if ($scope.cbPckContentSize) {
        $scope.carregar_tipos_itens(cbPckTypeItem);
    }

    if ($scope.cbPckNatureza) {
        $scope.carregar_embalagens_por_natureza();
    }
    
    $scope.carregar_mercados();
    
    if ($scope.packageHasVariation == 'Sim') {
        $scope.carregar_variacoes();
    }

    $scope.embalagens_selecionadas = embalagens_selecionadas;
    
});
