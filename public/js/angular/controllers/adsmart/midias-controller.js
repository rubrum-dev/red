var site_url = '/';
var app = angular.module('app', ['ngAnimate', 'midiasResolveLoader', 'selectize', '720kb.tooltips', 'cp.ngConfirm']);

app.animation('.slide-toggle-js', function () {
    return {
        enter: function (element, done) {
            $(element).slideDown(200, function ($scope) {
                done();
            });
        },
        leave: function (element, done) {
            $(element).slideUp(200, function () {
                done();
            });
        }
    };
});

app.controller('MidiasController', function ($scope, $rootScope, $ngConfirm, $timeout) {
    var start_time = new Date().getTime();

    $scope.listagem = {};

    $scope.midias = [];

    $scope.peca_arquivos = [{
        id: 0,
        nome: 'Teste 1'
    }, {
        id: 1,
        nome: 'Teste 2'
    }, {
        id: 2,
        nome: 'Teste 3'
    }];

    $scope.listagem.stateJS = [];
    $scope.exibe_loading = false;

    $scope.show_load = function () {
        $('.lds-css').show();
        $scope.exibe_loading = true;
    };

    $scope.hide_load = function () {
        $('.lds-css').hide();
        $scope.exibe_loading = false;
    };

    /*$scope.carregar_midias = function () {
        $scope.show_load();
        $rootScope.loaded =  false;
        
        $.get(site_url + 'site/adsmart/services/midias/' + id_campanha, function (data) {
                $scope.midias = data;
                $scope.$apply();
                $scope.hide_load();
                
                $timeout(function () {
                    $rootScope.loaded =  true;                
                }, new Date().getTime() - start_time);            
            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
    };*/

    $scope.carregar_midias = function () {
        var url = site_url + 'site/adsmart/services/midias/' + id_campanha;
        
        $scope.show_load();
        $rootScope.loaded =  false;
        
        $.ajax({
            url: url,
            type: "get",
            contentType: false,
            processData: false,
            start_time: new Date().getTime(),
            success: function (data, status) {
                $scope.midias = data;
                $scope.$apply();
                $scope.hide_load();
                
                $timeout(function () {
                    $rootScope.loaded =  true;                
                }, new Date().getTime() - this.start_time);            
            },
            error: function () {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            }
        });
    };

    $scope.exibeEditarPeca = function (midia, peca) {

        for (i = 0; i < midia.pecas.length; i++) {
            
            if (midia.pecas[i].exibir_editar_peca == true) {
                
                midia.pecas[i].exibir_editar_peca = false;
            
            }
        
        }
        
        midia.exibir_formulario = false;
        
        peca.exibir_editar_peca = !peca.exibir_editar_peca;

    };

    $scope.exibeNovaPeca = function (midia, acao) {

        if (acao) {

            for (i = 0; i < midia.pecas.length; i++) {
                
                if (midia.pecas[i].exibir_editar_peca == true) {
                    
                    midia.pecas[i].exibir_editar_peca = false;
                
                }

            }

        }

        midia.exibir_formulario = acao;

    };

    $scope.criar_peca_old = function (midia) {
        
        if (!midia.nome_nova_peca) {
            $('body').find('.artwork-container .input-required').addClass('has-error');
            $scope.hide_load();
            myAlert('Digite o nome da peça.', 'warning');

            return;
        }

        // Verifica se já existe 
        for(i = 0; i < midia.pecas.length; i++) {
            if(midia.pecas[i].nome === midia.nome_nova_peca) {
                $('body').find('.artwork-container .input-required').addClass('has-error');
                $scope.hide_load();
                myAlert('Essa peça já existe.', 'warning');

                return; 
            }
        }

        $scope.midia = midia;

        $scope.criarPeca = $ngConfirm({
            animation: 'scale',
            closeAnimation: 'scale',
            boxWidth: '910px',
            useBootstrap: false,
            title: '',
            contentUrl: '/view/adsmart/modal_cadastrar_peca.html',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'ng-confirm-btn-close fa fa-times',
            onOpen: function () {
                $('body').addClass('no-scroll');
                $('.ng-confirm-buttons').addClass('no-padding');
                $('.ng-confirm-buttons').hide();
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
            },
            onScopeReady: function () {
                $('body').find('.artwork-container .input-required').removeClass('has-error');
            }
        });

    };

    $scope.adsmartFileUpload = function () {
        
        $scope.criarPeca.close();

        $scope.artwUpload = $ngConfirm({
            boxWidth: '910px',
            useBootstrap: false,
            title: '',
            contentUrl: '/view/adsmart/modal_upload_arquivo.html?v=3.0',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'ng-confirm-btn-close fa fa-times',
            escapeKey: 'Cancelar',
            buttons: {
                Cancelar: {
                    text: 'Cancelar',
                    btnClass: 'btn-control btn-alt'
                },
                Carregar: {
                    text: 'Concluir',
                    btnClass: 'btn-control btn-positive',
                    keys: ['enter', 'space'],
                    action: function (Confirmar) {
                        $scope.salvar_arquivos();
                        //prevent close
                        return false;
                    }
                }
            },
            onScopeReady: function () {
                
            },
            onReady: function () {
                $scope.fileAttachmentCtrl();
            },
            onClose: function () {
                $scope.dados = {};
                $scope.id_item_tipo_novo = '';
            }
        });
    };

    $scope.criar_peca = function (midia) {
        if (!midia.nome_nova_peca) {
            $('body').find('.artwork-container .input-required').addClass('has-error');
            $scope.hide_load();
            myAlert('Digite o nome da peça.', 'warning');

            return;
        }

        // Verifica se já existe 
        for(i = 0; i < midia.pecas.length; i++) {
            if(midia.pecas[i].nome === midia.nome_nova_peca) {
                $('body').find('.artwork-container .input-required').addClass('has-error');
                $scope.hide_load();
                myAlert('Essa peça já existe.', 'warning');

                return; 
            }
        }

        $('body').find('.artwork-container .input-required').removeClass('has-error');
        
        params = {
            _token: csrf_token,
            id_campanha_midia: midia.id,
            nome: midia.nome_nova_peca,
            id_familia: id_familia
            //data_lancamento: new Date(Date.now()).toLocaleString().slice(0, 10)
        };
        
        $.confirm({
            animateFromElement: false,
            icon: 'fa fa-question',
            title: 'Confirmar',
            content: 'Para prosseguir é necessário abrir um ticket para a Peça passar por um fluxo de aprovação. Deseja abrir o ticket?',
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {

                        $scope.show_load();

                        $.post(site_url + 'site/adsmart/services/midias/criarPeca', params, function (data) {

                            if (data.error) {
                                myAlert(data.message, 'error');
                                $scope.hide_load();
                            } else {

                                if (data.success == 200){
                                    
                                    location = data.url;

                                } else {

                                    myAlert(data.message, 'error');

                                }

                                
                            }
            
                        }, 'json')
                        .fail(function (data) {
                            myAlert('Ocorreu um erro.', 'error');
                            $scope.hide_load();
                        });

                        //myAlert('Peça adicionada com sucesso!', 'success');
                        //midia.pecas.push(peca);
                        //midia.nome_nova_peca = '';
                        //midia.exibir_formulario = false;
                        
                        //$scope.$apply();
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {
                        midia.nome_nova_peca = '';
                        midia.exibir_formulario = false;
                        
                        $scope.$apply();
                    }
                }
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');

                midia.nome_nova_peca = '';
                midia.exibir_formulario = false;

                $scope.$apply();
            }
        })
         
    };

    $scope.alterar_peca = function (peca) {
        if (!peca.nome_original) {
            $('body').find('.artwork-container .input-required').addClass('has-error');
            $scope.hide_load();
            myAlert('Digite o nome da peça.', 'warning');

            return;
        }
        
        if (peca.nome == peca.nome_original) {
            peca.exibir_editar_peca = false;
            return;
        }

        // Verifica se já existe 
        for(i = 0; i < $scope.midias.length; i++) {
            
            for(j = 0; j < $scope.midias[i].pecas.length; j++) {
        
                if($scope.midias[i].pecas[j].nome === peca.nome_original) {
                    $('body').find('.artwork-container .input-required').addClass('has-error');
                    $scope.hide_load();
                    myAlert('Essa peça já existe.', 'warning');

                    return; 
                }
            
            }
        
        }

        $scope.show_load();

        params = {
            _token: csrf_token,
            id: peca.id,
            nome: peca.nome_original
            //data_lancamento: new Date(Date.now()).toLocaleString().slice(0, 10)
        };

        $.post(site_url + 'site/adsmart/services/midias/editarPeca', params, function (data) {

            if (data.error) {
                myAlert(data.message, 'error');
                $scope.hide_load();
            } else {

                if (data.success == 200){
                    
                    myAlert(data.message, 'success');

                    $('body').find('.artwork-container .input-required').removeClass('has-error');

                    $scope.carregar_midias();
                    $rootScope.loaded =  true;
                    $scope.hide_load();

                } else {

                    myAlert(data.message, 'error');

                }
                
            }

        }, 'json')
        .fail(function (data) {
            myAlert('Ocorreu um erro.', 'error');
            $scope.hide_load();
        });

    };

    $scope.excluir_midia_service = function (midia) {

        $scope.show_load();

        params = {
            _token: csrf_token,
            id_midia: midia.id
        };

        $.post(site_url + 'site/adsmart/services/midias/excluirMidia', params, function (data) {

                if (data.error) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                } else {
                    myAlert('Mídia excluída com sucesso.', 'success');
                    $scope.carregar_midias();
                    //$scope.hide_load();
                }

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });

    };

    $scope.excluir_midia = function (midia) {
        $.confirm({
            icon: 'fa fa-question',
            title: 'Confirmar',
            content: 'Deseja excluir essa mídia?',
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        $scope.excluir_midia_service(midia);
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel'
                }
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        })
    };

    $scope.excluir_peca_service = function (peca) {

        $scope.show_load();

        params = {
            _token: csrf_token,
            id_peca: peca.id
        };

        $.post(site_url + 'site/adsmart/services/midias/excluirPeca', params, function (data) {

                if (data.error) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                } else {
                    myAlert('Peça excluída com sucesso.', 'success');
                    $scope.carregar_midias();
                    //$scope.hide_load();
                }

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });

    };

    $scope.excluir_peca = function (peca) {
        $.confirm({
            icon: 'fa fa-question',
            title: 'Confirmar',
            content: 'Deseja excluir essa peça?',
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        $scope.excluir_peca_service(peca);
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel'
                }
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        })
    };

    $scope.carregar_midias();
});