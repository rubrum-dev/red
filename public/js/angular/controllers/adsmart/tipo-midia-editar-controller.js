var app = angular.module('app', ['selectize', '720kb.tooltips']);

app.controller('TipoMidiaCriarController', function ($scope) {
    $scope.tipoMidiaNome = tipoMidiaNome;

    $scope.cbAprovadoresMidia = '';

    $scope.aprovadores_midia = [];

    $scope.chkTipoMidiaAtivo = chkTipoMidiaAtivo;
    $scope.chkBloquarAutoAprovacao = chkBloquarAutoAprovacao;

    var site_url = '/';
    $scope.aprovadores_midia_selecionados = aprovadores_midia_selecionados;

    $scope.aprovadoresMidiaConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecione os aprovadores de mídia',
        delimiter: ',',
        maxItems: 1,
        render: {
            option: function (data) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };

    $scope.show_load = function () {
        //$('.master').addClass('blurred');
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        //$('.master').removeClass('blurred');
        $('.lds-css').hide();
    };

    $scope.getById = function (id, array) {
        for (i = 0; i < array.length; i++) {
            if (array[i].id == id) {
                return array[i];
            }
        }
    };

    $scope.carregar_aprovadores = function () {

        //$scope.show_load();
        $.get(site_url + 'site/adsmart/services/aprovadoresMidia', function (data) {
                $scope.aprovadores_midia = data;
                $scope.$apply();
                //$scope.hide_load();
            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                //$scope.hide_load();
            });

    };

    $scope.carregar_aprovadores();

    $scope.add_aprovador_midia_selecionado = function () {

        if( $scope.getById($scope.cbAprovadoresMidia, $scope.aprovadores_midia_selecionados) ) {
            myAlert('Estes Aprovadores de Mídia já estão adicionados.', 'warning');

            return false;
        } else if($scope.cbAprovadoresMidia === '' || $scope.cbAprovadoresMidia === undefined) {
            myAlert('Não foi selecionado um aprovador.', 'warning');
            
            return false;
        } else {

            if($scope.cbAprovadoresMidia !== '') {
                var i = $scope.getById($scope.cbAprovadoresMidia, $scope.aprovadores_midia);
                
                $scope.aprovadores_midia_selecionados.push(i);

                $scope.cbAprovadoresMidia = '';
            }
        
        }

    };

    $scope.excluir_aprovador_midia_selecionado = function(index) {
        $scope.aprovadores_midia_selecionados.splice(index, 1);
    };

    $scope.enviarFormulario = function (form) {
        
        $scope.show_load();
        
        params = {
            _token: csrf_token,
            tipoMidiaNome: $scope.tipoMidiaNome,
            chkTipoMidiaAtivo: $scope.chkTipoMidiaAtivo,
            chkBloquarAutoAprovacao: $scope.chkBloquarAutoAprovacao,
            aprovadores_midia_selecionados: $scope.aprovadores_midia_selecionados
        };
        
        $.post(site_url + 'site/adsmart/tipos-midia/editar/' + id, params, function (data) {

            if (data.error) {
                
                myAlert(data.message, 'error');
                
            } else {
                
                if (data.success == 200){
                    
                    $.confirm({
                        animationSpeed: 200,
                        animateFromElement: false,
                        draggable: false,
                        icon: 'fa fa-check',
                        title: 'Sucesso',
                        content: data.message,
                        closeIcon: true,
                        boxWidth: '910px',
                        scrollToPreviousElement: false,
                        useBootstrap: false,
                        closeIconClass: 'jconfirm-btn-close fa fa-times',
                        buttons: {
                            Entendi: {
                                keys: ['esc', 'enter', 'space'],
                                text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                                btnClass: 'btn-confirm my-alert-confirm',
                                action: function (Todos) {
                                    $('.lds-css').show(200, function () {
                                        location = data.url;
                                    });
                                }
                            }
                        },
                        onOpen: function () {
                            //$('body').addClass('jconfirm-overlay');
                            $('body').addClass('no-scroll');
                            //$('.master').addClass('blurred');
                            $('.jconfirm').css('overflow', 'auto');
                            $scope.hide_load();
                        },
                        onClose: function () {
                            //$('body').removeClass('jconfirm-overlay');
                            $('body').removeClass('no-scroll');
                            //$('.master').removeClass('blurred');
                            $('.jconfirm').css('overflow', 'hidden');
                            $scope.hide_load();
                        }
                    });
                    
                } else {
                    
                    $.confirm({
                        animationSpeed: 200,
                        animateFromElement: false,
                        draggable: false,
                        icon: 'fa fa-check',
                        title: 'Sucesso',
                        content: data.message,
                        closeIcon: true,
                        boxWidth: '910px',
                        scrollToPreviousElement: false,
                        useBootstrap: false,
                        closeIconClass: 'jconfirm-btn-close fa fa-times',
                        buttons: {
                            Entendi: {
                                keys: ['esc', 'enter', 'space'],
                                text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                                btnClass: 'btn-confirm my-alert-confirm'
                            }
                        },
                        onOpen: function () {
                            //$('body').addClass('jconfirm-overlay');
                            $('body').addClass('no-scroll');
                            //$('.master').addClass('blurred');
                            $('.jconfirm').css('overflow', 'auto');
                            $scope.hide_load();
                        },
                        onClose: function () {
                            //$('body').removeClass('jconfirm-overlay');
                            $('body').removeClass('no-scroll');
                            //$('.master').removeClass('blurred');
                            $('.jconfirm').css('overflow', 'hidden');
                            $scope.hide_load();
                        }
                    });

                }
                
            };

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                });
        
    };

});