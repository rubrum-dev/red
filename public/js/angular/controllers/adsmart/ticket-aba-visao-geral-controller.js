var visaoGeral = angular.module("abaVisaoGeralMod", []);

visaoGeral.controller('abaVisaoGeralCtrl', function (
        $scope,
        $rootScope,
        $ngConfirm,
        $window,
        Upload,
        $timeout
    ) {
    $rootScope.statechange = true;

    $scope.acao_solicitacao = function (id_solicitacao, status) {
        $scope.show_load();

        params = {
            _token: csrf_token,
            id: id_solicitacao,
            status: status
        };

        $.post(site_url + 'site/artwork/services/package/solicitacao/acao', params, function (data) {
            $scope.carregar_ticket(1);
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                });
        
        angular.element(document).ajaxStop(function () {
            $scope.hide_load();
            angular.element(this).unbind('ajaxStop'); // to stop this event repeating further    
        });
    };

    $scope.excluirArquivo = function(index) {
        $.confirm({
            icon: 'icon fa fa-question',
            title: 'Confirmar',
            content: 'Tem certeza que deseja excluir o Arquivo?',
            closeIcon: true,
            boxWidth: '910px',
            scrollToPreviousElement: false,
            useBootstrap: false,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn-confirm',
                    action: function (Confirmar) {
                        $scope.show_load();

                        params = {
                            _token: csrf_token,
                            id: $scope.ticket.ciclo_atual.layout[index].id,
                        };

                        $.post(site_url + 'site/adsmart/services/deleteLayout', params, function (data) {
                            $scope.carregar_ticket(1);
                            $scope.interval_tickets_start();    
                        }, 'json')
                                .fail(function (data) {
                                    myAlert('Ocorreu um erro.', 'error');
                                    $scope.hide_load();
                                });

                        angular.element(document).ajaxStop(function () {
                            angular.element(this).unbind('ajaxStop'); // to stop this event repeating further    
                            
                            if(!$scope.ticket.ciclo_atual.layout.length) {
                                angular.forEach($scope.ticket.ciclo_atual.solicitacoes, function (solicitacao, s) {
                                    $scope.acao_solicitacao($scope.ticket.ciclo_atual.solicitacoes[s].id, 1);
                                });
                            }
                        });
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel'
                }
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };
    
    $scope.customFileInputCtrl();
});