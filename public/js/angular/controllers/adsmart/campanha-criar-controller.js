var site_url = '/';
var app = angular.module('app', ['selectize', '720kb.tooltips']);

app.controller('CampanhaCriarCtrl', function ($scope) {
    $scope.familias = [];
    $scope.familias_selecionadas = [];

    $scope.familiasConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecione a marca',
        delimiter: ',',
        maxItems: 1,
        render: {
            option: function (data) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };
    
    $scope.show_load = function () {
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        $('.lds-css').hide();
    };

    $scope.carregar_familias = function () {
        if ($scope.cbNewCategory != '' && $scope.cbNewCategory != 0) {
            $scope.show_load();
            $.get(site_url + 'site/adsmart/services/familias', function (data) {
                    $scope.familias = data;
                    $scope.$apply();
                    $scope.hide_load();
                }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
        } else {
            $scope.familias = [];
        }
    };
    
    $scope.enviarFormulario = function (form) {
        
        $scope.show_load();
        
        params = {
            _token: csrf_token,
            campanhaNome: $scope.campanhaNome,
            familiasSelecionadas: $scope.familias_selecionadas
        };
        
        $.post(site_url + 'site/adsmart/campanha/criar', params, function (data) {

            if (data.error) {
                
                myAlert(data.message, 'error');
                
            } else {
                
                if (data.success == 200){
                    
                    $.confirm({
                        animationSpeed: 200,
                        animateFromElement: false,
                        draggable: false,
                        icon: 'fa fa-check',
                        title: 'Sucesso',
                        content: data.message,
                        closeIcon: true,
                        boxWidth: '910px',
                        scrollToPreviousElement: false,
                        useBootstrap: false,
                        closeIconClass: 'jconfirm-btn-close fa fa-times',
                        buttons: {
                            Entendi: {
                                keys: ['esc', 'enter', 'space'],
                                text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                                btnClass: 'btn-confirm my-alert-confirm',
                                action: function (Todos) {
                                    location = data.url;
                                }
                            }
                        },
                        onOpen: function () {
                            $('body').addClass('no-scroll');
                            $('.jconfirm').css('overflow', 'auto');
                            $scope.hide_load();
                        },
                        onClose: function () {
                            $('body').removeClass('no-scroll');
                            $('.jconfirm').css('overflow', 'hidden');
                            $scope.hide_load();
                        }
                    });
                    
                } else {
                    
                    $.confirm({
                        animationSpeed: 200,
                        animateFromElement: false,
                        draggable: false,
                        icon: 'fa fa-question',
                        title: 'Confirmar',
                        content: data.message,
                        closeIcon: true,
                        boxWidth: '910px',
                        scrollToPreviousElement: false,
                        useBootstrap: false,
                        closeIconClass: 'jconfirm-btn-close fa fa-times',
                        buttons: {
                            Renovar: {
                                keys: ['enter', 'space'],
                                text: '<span><i class="icon fa fa-check"></i>Continuar e editar a campanha existente</span>',
                                btnClass: 'btn-confirm',
                                action: function (Todos) {
                                    
                                    location = data.url;
                                    
                                }
                            },
                            Cancelar: {
                                keys: ['esc'],
                                text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                                btnClass: 'btn btn-cancel'
                            }
                        },
                        onOpen: function () {
                            $('body').addClass('no-scroll');
                            $('.jconfirm').css('overflow', 'auto');
                            $scope.hide_load();
                        },
                        onClose: function () {
                            $('body').removeClass('no-scroll');
                            $('.jconfirm').css('overflow', 'hidden');
                            $scope.hide_load();
                        }
                    });
                    
                }
                
            };

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                });
        
    };
    
    $scope.getById = function (id, array) {
        for (i = 0; i < array.length; i++) {
            if (array[i].id == id) {
                return array[i];
            }
        }
    };
    
    /** Adiciona embalagem primária/secundária selecionada a lista **/
    $scope.add_familia_selecionada = function() {
        
        if( $scope.getById($scope.cbFamilias, $scope.familias_selecionadas) ) {
            angular.element('.col-add-marca .selectize-input').addClass('has-error');
            
            myAlert('Esta marca já foi adicionada.', 'warning');

            return false;
        } else if($scope.cbFamilias === '' || $scope.cbFamilias === undefined) {
            angular.element('.col-add-marca .selectize-input').addClass('has-error');

            myAlert('Não foi selecionada uma marca.', 'warning');
            
            return false;
        } else {

            if($scope.cbFamilias !== '') {
                var i = $scope.getById($scope.cbFamilias, $scope.familias);
                
                $scope.familias_selecionadas.push(i);
                $scope.cbFamilias = '';
                
                angular.element('.col-add-marca .selectize-input').removeClass('has-error');
            }
        
        }
    
    };

    /** Exclui embalagem primária/secundária selecionada a lista **/
    $scope.excluir_familia_selecionada = function(index) {
        $scope.familias_selecionadas.splice(index, 1);
    };

    /** Limpa a lista de embalagens primárias/secundárias selecionadas **/
    $scope.limpar_familias_selecionadas = function() {

        $scope.familias_selecionadas = [];

    };
    
    $scope.carregar_familias();
});