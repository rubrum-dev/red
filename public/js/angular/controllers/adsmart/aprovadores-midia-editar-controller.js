var site_url = '/';
var app = angular.module('app', ['selectize', '720kb.tooltips']);

app.controller('AprovadoresMidiaCriarController', function ($scope) {
    $scope.aprovadoresMidiaNome = '';

    $scope.cbUsuario = '';

    $scope.usuarios = [];

    $scope.usuarios_selecionados = usuarios_selecionados;

    $scope.aprovadoresMidiaNome = aprovadoresMidiaNome;

    $scope.usuarioConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome', 'empresa', 'departamento'],
        placeholder: 'Selecione usuário',
        delimiter: ',',
        maxItems: 1,
        render: {
            option: function (data) {
                //return '<div class="item">' + data.nome + '</div>';

                if (data.departamento !== '') {
                    return '<div>' +
                        '   <span class="heading">' + data.nome + '</span>' +
                        '   <span class="caption">' + data.empresa + '</span>' +
                        '   <span class="slash">' + escape('/') + '</span>' +
                        '   <span class="caption">' + data.departamento + '</span>' +
                        '</div>';
                } else {
                    return '<div>' +
                        '   <span class="heading">' + data.nome + '</span>' +
                        '   <span class="caption">' + data.empresa + '</span>' +
                        '   <span class="slash">' + escape('/') + '</span>' +
                        '   <span class="caption">' + 'Departamento' + '</span>' +
                        '</div>';
                }
            }
        },
        onDropdownOpen: function () {}
    };

    $scope.show_load = function () {
        //$('.master').addClass('blurred');
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        //$('.master').removeClass('blurred');
        $('.lds-css').hide();
    };

    $scope.getById = function (id, array) {
        for (i = 0; i < array.length; i++) {
            if (array[i].id == id) {
                return array[i];
            }
        }
    };

    $scope.carregar_usuarios = function () {

        //$scope.show_load();
        $.get(site_url + 'site/artwork/services/package/usuarios', function (data) {
                $scope.usuarios = data.usuarios;
                $scope.$apply();
                //$scope.hide_load();
            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                //$scope.hide_load();
            });

    };

    $scope.add_usuario_selecionado = function () {

        if( $scope.getById($scope.cbUsuario, $scope.usuarios_selecionados) ) {
            myAlert('Este usuário já foi adicionado.', 'warning');

            return false;
        } else if($scope.cbUsuario === '' || $scope.cbUsuario === undefined) {
            myAlert('Não foi selecionado um usuário.', 'warning');
            
            return false;
        } else {

            if($scope.cbUsuario !== '') {
                var i = $scope.getById($scope.cbUsuario, $scope.usuarios);
                
                $scope.usuarios_selecionados.push(i);

                $scope.cbUsuario = '';
            }
        
        }

    };

    $scope.excluir_usuario_selecionado = function(index) {
        $scope.usuarios_selecionados.splice(index, 1);
    };

    $scope.enviarFormulario = function (form) {
        
        $scope.show_load();
        
        params = {
            _token: csrf_token,
            aprovadoresMidiaNome: $scope.aprovadoresMidiaNome,
            usuarios_selecionados: $scope.usuarios_selecionados
        };
        
        $.post(site_url + 'site/adsmart/aprovadores-midia/editar/' + id, params, function (data) {

            if (data.error) {
                
                myAlert(data.message, 'error');
                
            } else {
                
                if (data.success == 200){
                    
                    $.confirm({
                        animationSpeed: 200,
                        animateFromElement: false,
                        draggable: false,
                        icon: 'fa fa-check',
                        title: 'Sucesso',
                        content: 'Aprovadores de Mídia editado com sucesso.',
                        closeIcon: true,
                        boxWidth: '910px',
                        scrollToPreviousElement: false,
                        useBootstrap: false,
                        closeIconClass: 'jconfirm-btn-close fa fa-times',
                        buttons: {
                            Entendi: {
                                keys: ['esc', 'enter', 'space'],
                                text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                                btnClass: 'btn-confirm my-alert-confirm',
                                action: function (Todos) {
                                    $('.lds-css').show(200, function () {
                                        location = data.url;
                                    });
                                }
                            }
                        },
                        onOpen: function () {
                            //$('body').addClass('jconfirm-overlay');
                            $('body').addClass('no-scroll');
                            //$('.master').addClass('blurred');
                            $('.jconfirm').css('overflow', 'auto');
                            $scope.hide_load();
                        },
                        onClose: function () {
                            //$('body').removeClass('jconfirm-overlay');
                            $('body').removeClass('no-scroll');
                            //$('.master').removeClass('blurred');
                            $('.jconfirm').css('overflow', 'hidden');
                            $scope.hide_load();
                        }
                    });
                    
                } else {
                    
                    $.confirm({
                        animationSpeed: 200,
                        animateFromElement: false,
                        draggable: false,
                        icon: 'fa fa-check',
                        title: 'Sucesso',
                        content: data.message,
                        closeIcon: true,
                        boxWidth: '910px',
                        scrollToPreviousElement: false,
                        useBootstrap: false,
                        closeIconClass: 'jconfirm-btn-close fa fa-times',
                        buttons: {
                            Entendi: {
                                keys: ['esc', 'enter', 'space'],
                                text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                                btnClass: 'btn-confirm my-alert-confirm'
                            }
                        },
                        onOpen: function () {
                            //$('body').addClass('jconfirm-overlay');
                            $('body').addClass('no-scroll');
                            //$('.master').addClass('blurred');
                            $('.jconfirm').css('overflow', 'auto');
                            $scope.hide_load();
                        },
                        onClose: function () {
                            //$('body').removeClass('jconfirm-overlay');
                            $('body').removeClass('no-scroll');
                            //$('.master').removeClass('blurred');
                            $('.jconfirm').css('overflow', 'hidden');
                            $scope.hide_load();
                        }
                    });

                }
                
            };

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                });
        
    };

    $scope.carregar_usuarios();
});