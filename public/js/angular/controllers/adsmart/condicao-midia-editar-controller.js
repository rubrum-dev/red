var site_url = '/';
app = angular.module('app', []);

app.directive('autoHeight', function () {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element = element[0];
            
            var resize = function() {
                element.style.height = 'auto';
                element.style.height = (element.scrollHeight)+'px';
            };
            
            element.addEventListener('load', resize, false);
            element.addEventListener('change', resize, false);
            element.addEventListener('cut', resize, false);
            element.addEventListener('paste', resize, false);
            element.addEventListener('drop', resize, false);
            element.addEventListener('keyup', resize, false);
            element.addEventListener('keydown', resize, false);

            setTimeout(resize, 0);
        }
    }
});

app.controller('CondicaoMidiaCriarController', function ($scope) {
    $scope.condicaoMidiaNome = condicaoMidiaNome;
    $scope.txtCondicaoMidia = txtCondicaoMidia;
    $scope.chkCondicaoMidiaTipo_1 = chkCondicaoMidiaTipo_1;
    $scope.chkCondicaoMidiaTipo_2 = chkCondicaoMidiaTipo_2;
    $scope.chkCondicaoMidiaTipo_3 = chkCondicaoMidiaTipo_3;
    $scope.chkCondicaoMidiaTipo_4 = chkCondicaoMidiaTipo_4;
    $scope.chkCondicaoMidiaTipo_5 = chkCondicaoMidiaTipo_5;

    $scope.show_load = function () {
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        $('.lds-css').hide();
    };

    $scope.aberturaTicketUncheck = function() {
        if ($scope.chkCondicaoMidiaTipo_2 === false) {
            $scope.chkCondicaoMidiaTipo_3 = true;
        } 
    };

    $scope.reprovacaoTicketUncheck = function() {
        if ($scope.chkCondicaoMidiaTipo_3 === false) {
            $scope.chkCondicaoMidiaTipo_2 = true;
        } 
    };

    $scope.enviarFormulario = function (form) {
        
        $scope.show_load();
        
        params = {
            _token: csrf_token,
            condicaoMidiaNome: $scope.condicaoMidiaNome,
            txtCondicaoMidia: $scope.txtCondicaoMidia,
            chkCondicaoMidiaTipo_1: $scope.chkCondicaoMidiaTipo_1,
            chkCondicaoMidiaTipo_2: $scope.chkCondicaoMidiaTipo_2,
            chkCondicaoMidiaTipo_3: $scope.chkCondicaoMidiaTipo_3,
            chkCondicaoMidiaTipo_4: $scope.chkCondicaoMidiaTipo_4,
            chkCondicaoMidiaTipo_5: $scope.chkCondicaoMidiaTipo_5
        };
        
        $.post(site_url + 'site/adsmart/condicoes-midia/editar/' + id, params, function (data) {

            if (data.error) {
                
                myAlert(data.message, 'error');
                
            } else {
                
                if (data.success == 200){
                    
                    $.confirm({
                        animationSpeed: 200,
                        animateFromElement: false,
                        draggable: false,
                        icon: 'fa fa-check',
                        title: 'Sucesso',
                        content: 'Critério de Avaliação editado com sucesso.',
                        closeIcon: true,
                        boxWidth: '910px',
                        scrollToPreviousElement: false,
                        useBootstrap: false,
                        closeIconClass: 'jconfirm-btn-close fa fa-times',
                        buttons: {
                            Entendi: {
                                keys: ['esc', 'enter', 'space'],
                                text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                                btnClass: 'btn-confirm my-alert-confirm',
                                action: function (Todos) {
                                    location = data.url;
                                }
                            }
                        },
                        onOpen: function () {
                            $('body').addClass('no-scroll');
                            $('.jconfirm').css('overflow', 'auto');
                            $scope.hide_load();
                        },
                        onClose: function () {
                            $('body').removeClass('no-scroll');
                            $('.jconfirm').css('overflow', 'hidden');
                            $scope.hide_load();
                        }
                    });
                    
                } else {
                    
                    $.confirm({
                        animationSpeed: 200,
                        animateFromElement: false,
                        draggable: false,
                        icon: 'fa fa-check',
                        title: 'Sucesso',
                        content: data.message,
                        closeIcon: true,
                        boxWidth: '910px',
                        scrollToPreviousElement: false,
                        useBootstrap: false,
                        closeIconClass: 'jconfirm-btn-close fa fa-times',
                        buttons: {
                            Entendi: {
                                keys: ['esc', 'enter', 'space'],
                                text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                                btnClass: 'btn-confirm my-alert-confirm'
                            }
                        },
                        onOpen: function () {
                            $('body').addClass('no-scroll');
                            $('.jconfirm').css('overflow', 'auto');
                            $scope.hide_load();
                        },
                        onClose: function () {
                            $('body').removeClass('no-scroll');
                            $('.jconfirm').css('overflow', 'hidden');
                            $scope.hide_load();
                        }
                    });

                }
                
            };

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                });
        
    };
});