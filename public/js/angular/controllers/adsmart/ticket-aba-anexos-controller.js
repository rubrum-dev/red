var anexos = angular.module("abaAnexosMod", []);

anexos.controller('abaAnexosCtrl', function (
    $scope,
    $rootScope,
    Upload,
    $timeout
) {
    $rootScope.statechange = true;

    /* Função responsável por definir extenção de arquivo no box de upload em Anexos */
    $scope.attachedFileExtensionClass = function (arquivo) {
        var ext = arquivo.descricao.substring(arquivo.descricao.lastIndexOf('.') + 1)

        switch (ext) {
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'bmp':
            case 'gif':
            case 'tif':
            case 'tiff':
                return 'fa-file-image-o';
                break;
            case 'pdf':
                return 'fa-file-pdf-o';
                break;
            case 'doc':
            case 'docx':
                return 'fa-file-word-o';
                break;
            case 'xls':
            case 'xlsx':
                return 'fa-file-excel-o';
                break;
            case 'ppt':
            case 'pptx':
                return 'fa-file-powerpoint-o';
                break;
            case 'zip':
            case 'rar':
            case '7z':
                return 'fa-file-archive-o';
                break;
            case 'txt':
                return 'fa-file-text-o';
            case 'mp3':
            case 'wav':
                return 'fa-file-audio-o';
                break;
            case 'mp4':
            case 'mpeg':
            case 'avi':
            case 'mov':
                return 'fa-file-video-o';
                break;
            default:
                return 'fa-file-o';
                break;
        }
    };

    $scope.setArquivo = function (element) {
        $scope.$apply(function (scope) {
            // Turn the FileList object into an Array
            for (var i = 0; i < element.files.length; i++) {
                $rootScope.files_arquivo.push(element.files[i]);
            }
        });
    };

    $scope.uploadArquivoAnexos = function (id) {
        if (!$scope.arquivos || !$scope.arquivos.length) {
            $('.workflow-file-upload').find('.files-control-fake').addClass('has-error');
            myAlert('Anexe um arquivo.', 'warning');
            return false;
        } else if($scope.arquivos[0].size > 105255200) {
            $('body').find('.attach-file .files-control-fake').addClass('has-error');
            myAlert('O tamanho máximo permitido para um arquivo é 100 MB.', 'warning');
            return false;
        }

        $scope.arquivos.progress = 0;
        $scope.interval_tickets_stop();
        $scope.limparCamposUpload();

        $('.workflow-file-upload').find('.files-control-fake').removeClass('has-error');

        $scope.arquivos.upload = Upload.upload({
            url: site_url + 'site/adsmart/services/uploadArquivo',
            data: {
                id: id,
                files: $scope.arquivos
            }
        });

        $scope.arquivos.upload.then(function (response) {
            $timeout(function () {
                $scope.arquivos = '';
                $scope.carregar_ticket(1);
                $scope.limparCamposUpload();
            });
        }, function (response) {
            if (response.status > 0) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.arquivos.progress = 0;
                $scope.interval_tickets_start();
            }
        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            $scope.arquivos.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });

    };

    $scope.exibeRenomearAnexo = function(arquivo) {
        for (i = 0; i < $scope.ticket.arquivos.length; i++) {
            if ($scope.ticket.arquivos[i].exibir_renomear == true) {
                $scope.ticket.arquivos[i].exibir_renomear = false;
            }
        }

        arquivo.novo_nome = arquivo.nome_renomeavel;
        arquivo.exibir_renomear = !arquivo.exibir_renomear;

        angular.element('.flex-rename-file input').removeClass('has-error');
    };

    $scope.getFileExtension = function(filename) {
        return filename
            .split('.')    // Split the string on every period
            .slice(-1)[0]; // Get the last item from the split
    }

    $scope.renomearAnexoSalvar = function(arquivo, id) {
        var input = arquivo.novo_nome,
            //format = /[!@#$%^`&*()+\=\[\]{};':"\\|,.<>\/?]/;
            format = /["^*:\\|<>\/?]/;

        if(format.test(input)) {
            myAlert('Os caracteres &Hat; &sol; &verbar; &bsol; &lt; &gt; &quest; &ast; &colon; &quot; não são permitidos em nomes de arquivo.', 'warning');
            angular.element('.flex-rename-file input').addClass('has-error');
            return false;
        }

        angular.element('.flex-rename-file input').removeClass('has-error');

        params = {
            _token: csrf_token,
            id: arquivo.id,
            novo_nome: input
        };

        $scope.show_load();

        $.post(site_url + 'site/artwork/services/package/ticket/renomearArquivo', params, function (data) {
            arquivo.novo_nome = '';
            arquivo.exibir_renomear = false;
            $scope.carregar_ticket(1);
            $scope.interval_tickets_start();
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });

        /*arquivo.upload = Upload.upload({
            url: site_url + 'site/artwork/services/package/ticket/renomearArquivo',
            data: {
                id: id,
                file: arquivo,
                name: Upload.rename(arquivo, arquivo.novo_nome + '.' + $scope.getFileExtension(arquivo.descricao))
            }
        });*/
        
        arquivo.novo_nome = '';
        arquivo.exibir_renomear = false;
    };

    $scope.excluirAnexo = function(arquivo) {
        $.confirm({
            icon: 'icon fa fa-question',
            title: 'Confirmar',
            content: 'Tem certeza que deseja excluir o anexo?',
            closeIcon: true,
            boxWidth: '910px',
            scrollToPreviousElement: false,
            useBootstrap: false,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn-confirm',
                    action: function (Confirmar) {
                        params = {
                            _token: csrf_token,
                            id: arquivo.id,
                        };

                        $scope.show_load()
                
                        $.post(site_url + 'site/artwork/services/package/ticket/excluirArquivo', params, function (data) {
                            $scope.carregar_ticket(1);
                            $scope.interval_tickets_start();
                        }, 'json')
                                .fail(function (data) {
                                    myAlert('Ocorreu um erro.', 'error');
                                    $scope.hide_load();
                                });
                                
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel'
                }
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };

    $scope.customMultiFileInputCtrl();
});