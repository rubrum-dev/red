var site_url = '/';
var app = angular.module('app', ['selectize', 'cp.ngConfirm']);

app.controller('MidiaCriarController', function($scope) {
    $scope.cbTipoMidia = '';

    $scope.tipos_midia = [];

    $scope.aprovadores_midia = [];

    $scope.tiposMidiaConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecione o tipo de mídia',
        delimiter: ',',
        maxItems: 1,
        render: {
            option: function (data) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };

    $scope.show_load = function () {
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        $('.lds-css').hide();
    };

    $scope.carregar_aprovadores = function () {

        //$scope.show_load();
        $.get(site_url + 'site/adsmart/services/aprovadoresMidia/' + $scope.cbTipoMidia, function (data) {
                $scope.aprovadores_midia = data;
                $scope.$apply();
                //$scope.hide_load();
            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                //$scope.hide_load();
            });

    };

    $scope.enviarFormulario = function (form) {
        
        params = {
            _token: csrf_token,
            id_midia_tipo: $scope.cbTipoMidia,
            id_familia: id_familia,
            id_campanha: id_campanha
        };
        
        $.post(site_url + 'site/adsmart/midia/criar', params, function (data) {

            if (data.error) {
                
                myAlert(data.message, 'error');
                
            } else {
                
                if (data.success == 200){
                    
                    $.confirm({
                        animationSpeed: 200,
                        animateFromElement: false,
                        draggable: false,
                        icon: 'fa fa-check',
                        title: 'Sucesso',
                        content: data.message,
                        closeIcon: true,
                        boxWidth: '910px',
                        scrollToPreviousElement: false,
                        useBootstrap: false,
                        closeIconClass: 'jconfirm-btn-close fa fa-times',
                        buttons: {
                            Entendi: {
                                keys: ['esc', 'enter', 'space'],
                                text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                                btnClass: 'btn-confirm my-alert-confirm',
                                action: function (Todos) {
                                    location = data.url;
                                }
                            }
                        },
                        onOpen: function () {
                            $('body').addClass('jconfirm-overlay');
                            $('body').addClass('no-scroll');
                            $('.master').addClass('blurred');
                            $('.jconfirm').css('overflow', 'auto');
                            $scope.hide_load();
                        },
                        onClose: function () {
                            $('body').removeClass('jconfirm-overlay');
                            $('body').removeClass('no-scroll');
                            $('.master').removeClass('blurred');
                            $('.jconfirm').css('overflow', 'hidden');
                            $scope.hide_load();
                        }
                    });
                    
                } else {
                    
                    myAlert('Tipo de mídia já cadastrada.', 'error');
                    
                }
                
            };

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                });
        
    };

    $scope.carregar_midia_tipos = function () {
        $.get(site_url + 'site/adsmart/services/midiaTipos', function (data) {
                $scope.tipos_midia = data;
                $scope.$apply();

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
            });
    };

    $scope.carregar_midia_tipos();

});