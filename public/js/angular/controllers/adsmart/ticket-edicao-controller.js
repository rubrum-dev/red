var site_url = '/';
var app = angular.module('app', [
    'cp.ngConfirm',
    '720kb.tooltips',
    'ngAnimate',
    'selectize'
]);

/*app.directive('fileUpload', function () {
    return {
        scope: true, //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                var uploadObj = el.context.name;
                //iterate files since 'multiple' may be specified on the element
                for (var i = 0; i < files.length; i++) {
                    //emit event upward
                    scope.$emit("fileSelected", {
                        file: files[i]
                    });
                }
            });
        }
    };
});*/

/*app.directive('validFile', [function () {
    return {
        require: 'ngModel',
        scope: {
            format: '@',
            upload: '&upload'
        },
        link: function (scope, el, attrs, ngModel) {
            // change event is fired when file is selected
            el.bind('change', function (event) {
                console.log(event.target.files[0]);
                scope.upload({
                    file: event.target.files[0]
                });
                scope.$apply(function () {
                    ngModel.$setViewValue(el.val());
                    ngModel.$render();
                });
            })
        }
    }
}]);*/

/*app.filter('fileSize', function () {
    return function (size) {
        if (isNaN(size))
            size = 0;

        if (size < 1024)
            return size + ' Bytes';
        size /= 1024;

        if (size < 1024)
            return size.toFixed(2) + ' Kb';
        size /= 1024;

        if (size < 1024)
            return size.toFixed(2) + ' Mb';
        size /= 1024;

        if (size < 1024)
            return size.toFixed(2) + ' Gb';
        size /= 1024;

        return size.toFixed(2) + ' Tb';
    };
});*/

app.directive('autoHeight', function () {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element = element[0];
            
            var resize = function() {
                element.style.height = 'auto';
                element.style.height = (element.scrollHeight)+'px';
            };
            
            element.addEventListener('change', resize, false);
            element.addEventListener('cut', resize, false);
            element.addEventListener('paste', resize, false);
            element.addEventListener('drop', resize, false);
            element.addEventListener('keydown', resize, false);

            setTimeout(resize, 0);
        }
    }
});

app.controller('TicketEdicaoCtrl', function ($scope, $http, $rootScope, $timeout, $ngConfirm) {
    //$scope.arquivos = [];
    $scope.ticket = {};
    $scope.solicitacoes = [];
    $scope.tipos = [];

    $scope.show_load = function () {
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        $('.lds-css').hide();
    };

    $scope.carregar_condicoes_midia = function () {

        $scope.show_load();

        $.get(site_url + 'site/adsmart/services/condicoesMidia/criar', function (data) {

                $scope.condicoesMidia = data;

                $scope.condicoesMidia

                $scope.$apply();

                $scope.hide_load();

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
    };

    $scope.carregar_condicoes_midia();

    $scope.getById = function (id, array) {
        for (i = 0; i < array.length; i++) {
            if (array[i].id == id) {
                return array[i];
            }
        }
    };

    $scope.addTipo = function () {

        var condicao = $scope.getById($scope.add_tipo, $scope.condicoesMidia);

        $scope.add_descricao = (condicao) ? condicao.texto : '';
        $scope.add_titulo = (condicao) ? condicao.nome : '';

        $('#alterationTypeWrap .add-instruction-col button').removeClass('has-error');
        $('.request-type .input-fields-group:last select').removeClass('has-error');
        $('.request-type .input-fields-group:last textarea').removeClass('has-error');
    };

    $scope.adicionar = function () {
        if ($scope.add_tipo && $scope.add_descricao) {
            var template = {
                'id_tipo': $scope.add_tipo,
                'descricao': $scope.add_descricao,
                'nome': $scope.add_titulo,
                'obrigatoria': $scope.obrigatoria
            };

            $scope.solicitacoes.push(template);

            $scope.add_tipo = '';
            $scope.add_descricao = '';
            $scope.add_titulo = '';
            $scope.obrigatoria = '';

            $('#alterationTypeWrap .add-instruction-col button').removeClass('has-error');
            $('.request-type .input-fields-group:last select').removeClass('has-error');
            $('.request-type .input-fields-group:last textarea').removeClass('has-error');
            $('.request-type .input-fields-group:last textarea').height(0);
        } else {
            $('.request-type .input-fields-group:last select').addClass('has-error');
            $('.request-type .input-fields-group:last textarea').addClass('has-error');

            myAlert('Selecione o critério de avaliação e infome a descrição específica.', 'warning');
        }
    };

    $scope.remover = function (k) {
        $scope.solicitacoes.splice(parseInt(k), 1);
    };

    $scope.carregar_ticket = function () {
        $scope.show_load();

        $.get(site_url + 'site/artwork/services/package/ticket/' + id_ticket, function (data) {
                $scope.ticket = data;

                $scope.solicitacoes = (data.ciclo_atual.solicitacoes) ? data.ciclo_atual.solicitacoes : [];

                $scope.$apply();
                $scope.hide_load();

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro!');
                $scope.hide_load();
            });
    };

    /* Função responsável por definir extenção de arquivo no box de upload */
    //$scope.fileExtensionClass = function (arquivo) {
    //    var ext = arquivo.descricao.substring(arquivo.descricao.lastIndexOf('.') + 1)

    //    switch (ext) {
    //        case 'jpg':
    //        case 'jpeg':
    //        case 'png':
    //        case 'gif':
    //            return 'fa-file-image-o';
    //            break;
    //        case 'pdf':
    //            return 'fa-file-pdf-o';
    //            break;
    //        case 'ppt':
    //        case 'pptx':
    //            return 'fa-file-powerpoint-o';
    //            break;
    //        case 'mp3':
    //        case 'wav':
    //            return 'fa-file-audio-o';
    //            break;
    //        case 'mp4':
    //        case 'mpeg':
    //        case 'avi':
    //        case 'mov':
    //            return 'fa-file-video-o';
    //            break;
    //        default:
    //            return 'fa-file-o';
    //            break;
    //    }
    //};

    /* Adicionar arquivo */
    //$scope.add_arquivo = function (arquivo) {
    //    $scope.arquivo = document.getElementById('arquivo').files[0];

    //    if (!$scope.arquivo) {
    //        angular.element('#fileAttachWrap').find('.files-control-fake').addClass('has-error');

    //        myAlert('Não foi anexado um aquivo.', 'warning');
    //        return false;
    //    }

    //    arquivo = {
    //        descricao: $scope.arquivo.name,
    //        data_criacao: new Date().toLocaleDateString(),
    //        filesize: $scope.arquivo.size
    //    }

    //    $scope.arquivos.push(arquivo);

    //    $scope.limparCamposUpload();

    //    angular.element('#fileAttachWrap').find('.files-control-fake').removeClass('has-error');
    //};

    /* Remover arquivo */
    //$scope.remover_arquivo = function ($index) {
    //    $scope.arquivos.splice($index, 1)
    //};

    $scope.validar = function ($event) {
        if (edicao) {
            if (angular.element('#txtMainDescription').val() === '') {
                angular.element('#txtMainDescription').focus();
                angular.element('#txtMainDescription').addClass('has-error');

                myAlert('Descreva resumidamente o conteúdo deste ticket em Descrição Geral.', 'Atenção', 'warning');

                $event.preventDefault();

                return false;
            } else if (angular.element('#tfArtSendDate').val() === '') {
                angular.element('#txtMainDescription').removeClass('has-error');
                angular.element('#tfArtSendDate').focus();
                angular.element('#tfArtSendDate').addClass('has-error');

                myAlert('Informe a data para que a arte seja enviada para aprovação.', 'warning');

                $event.preventDefault();

                return false;
            } else if (!$scope.add_tipo && $scope.solicitacoes.length === 0) {
                angular.element('#tfDeadlineDate').removeClass('has-error');
                angular.element('#alterationTypeWrap .input-fields-group select:last').focus();
                angular.element('#alterationTypeWrap .input-fields-group select:last').addClass('has-error');

                myAlert('Selecione o Critério de Avaliação.', 'warning');

                $event.preventDefault();

                return false;
            } else if (!$scope.add_descricao && $scope.solicitacoes.length === 0) {
                angular.element('#alterationTypeWrap .input-fields-group select').removeClass('has-error');
                angular.element('#alterationTypeWrap .input-fields-group textarea:last').focus();
                angular.element('#alterationTypeWrap .input-fields-group textarea:last').addClass('has-error');

                myAlert('Descreva com detalhes o critéro de avaliação.', 'warning');

                $event.preventDefault();

                return false;
            } else if ($scope.add_tipo || $scope.add_descricao) {
                angular.element('#alterationTypeWrap .input-fields-group select:last').removeClass('has-error');
                angular.element('#alterationTypeWrap .input-fields-group textarea:last').removeClass('has-error');
                angular.element('#alterationTypeWrap .add-instruction-col button').addClass('has-error');

                myAlert('Clique em Adicionar para incluir o critério de avaliação no ticket.', 'warning');

                $event.preventDefault();

                return false;
            }

        }
    };

    $scope.limparCamposUpload = function () {
        $('.files-control').find('input[type="file"]').val('');
        $('.files-control').find('.files-control-fake strong').text('Anexar arquivo');
    };

    $scope.carregar_ticket();
});
