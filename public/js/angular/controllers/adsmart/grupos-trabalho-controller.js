var site_url = '/';

var grupoTrabalho = angular.module('gruposTrabalhoMod',
    [
        'cp.ngConfirm',
        '720kb.tooltips',
        'ngAnimate'
    ]);

grupoTrabalho.controller('GruposTrabalhoCtrl', function (
    $scope,
    $timeout,
    $rootScope,
    $ngConfirm,
    $http
) {
    $scope.grupo_nome = '';
    $scope.grupo_global = false;
    $scope.editar_grupo = false;
    $scope.disabled = false;

    $scope.grupos = {

        locais: [],
        globais: []

    };

    $scope.carregar_grupos = function () {
        $http.get(site_url + 'site/adsmart/services/getGruposDeAprovacao').success(function (data) {
            $scope.grupos = data;
        });
    };

    $scope.criar_grupo_trabalho = function () {
        $scope.workGroupModal = $ngConfirm({
            title: '',
            contentUrl: '/view/modal_grupo_trabalho.html',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'ng-confirm-btn-close fa fa-times',
            backgroundDismiss: false,
            boxWidth: '910px',
            useBootstrap: false,
            onOpen: function () {

                $('body').addClass('no-scroll');
                $('.ng-confirm-buttons').addClass('no-padding');
                $('.ng-confirm-buttons').hide();
            },
            onScopeReady: function () {}
        });
    };

    $scope.utilizar_grupo_trabalho = function () {

        $scope.workGroupModal.close();

        $scope.useWorkGroupModal = $ngConfirm({
            title: '',
            contentUrl: '/view/modal_utilizar_grupo_trabalho.html',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'ng-confirm-btn-close fa fa-times',
            backgroundDismiss: false,
            boxWidth: '910px',
            useBootstrap: false,
            onScopeReady: function () {

                $scope.carregar_grupos();

            }
        });
    };

    $scope.editar_grupo_trabalho = function (grupo) {

        if (grupo === undefined) {
            $scope.grupo_nome = '';
            $scope.editar_grupo = false;
            $scope.grupo_global = false;
            $scope.disabled = false;
            $scope.grupo_id = null;
            $scope.workGroupModal.close();
        } else {
            $scope.workGroupModal.close();
            $scope.useWorkGroupModal.close();
            $scope.editar_grupo = true;
        }

        if (grupo) {
            $scope.grupo_nome = grupo.nome;
            $scope.grupo_id = grupo.id;
            $scope.grupo_global = (grupo.global == 1) ? true : false;
            $scope.disabled = (grupo.global == 1) ? true : false;
        }

        $scope.editWorkGroupModal = $ngConfirm({
            title: '',
            contentUrl: '/view/modal_editar_grupo_trabalho.html',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'ng-confirm-btn-close fa fa-times',
            backgroundDismiss: false,
            boxWidth: '910px',
            useBootstrap: false,
            buttons: {
                Cancelar: {
                    keys: ['esc'],
                    text: 'Cancelar',
                    btnClass: 'btn-control btn-alt',
                    action: function (Cancelar) {}
                },
                Salvar: {
                    keys: ['enter', 'space'],
                    text: 'Salvar',
                    btnClass: 'btn-control btn-positive',
                    action: function (Salvar) {
                        $scope.salvar_grupo_trabalho(0, grupo);
                        return false;
                    }
                }
            },
            onScopeReady: function () {
                $scope.carregar_grupos();
            }
        });

    };

    $scope.salvar_grupo_trabalho_confirm = function () {

        $scope.editWorkGroupModal.close();

        $.confirm({
            animationSpeed: 200,
            icon: 'fa fa-question',
            title: 'Confirmar',
            content: 'Já existe um Grupo de Trabalho com este nome. Deseja substituí-lo?',
            draggable: false,
            dragWindowBorder: false,
            animateFromElement: false,
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            useBootstrap: false,
            boxWidth: '910px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        $scope.salvar_grupo_trabalho(1);
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {}
                }
            },
            onOpen: function () {
                $('body').addClass('jconfirm-overlay');
                $('body').addClass('no-scroll');
                $('.master').addClass('blurred');
                $('.jconfirm').css('overflow', 'auto');
            },
            onClose: function () {
                $('body').removeClass('jconfirm-overlay');
                $('body').removeClass('no-scroll');
                $('.master').removeClass('blurred');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };

    $scope.selecionar_grupo_confirm = function (grupo) {

        $scope.useWorkGroupModal.close();

        $.confirm({
            animationSpeed: 200,
            icon: 'fa fa-question',
            title: 'Confirmar',
            content: 'Todos os participantes do ticket serão substituídos pelo Grupo de Trabalho.<br>Deseja confirmar?',
            draggable: false,
            dragWindowBorder: false,
            animateFromElement: false,
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            useBootstrap: false,
            boxWidth: '910px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        $scope.show_load();

                        setTimeout(function () {
                            $scope.selecionar_grupo(grupo);
                        }, 100);

                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {}
                }
            },
            onOpen: function () {
                $('body').addClass('jconfirm-overlay');
                $('body').addClass('no-scroll');
                $('.master').addClass('blurred');
                $('.jconfirm').css('overflow', 'auto');
            },
            onClose: function () {
                $('body').removeClass('jconfirm-overlay');
                $('body').removeClass('no-scroll');
                $('.master').removeClass('blurred');
                $('.jconfirm').css('overflow', 'hidden');
            },
            onDestroy: function () {}
        });
    };

    $scope.selecionar_grupo = function (grupo) {

        $rootScope.participantes = [];

        angular.forEach(grupo.artw_grupos_perfis_usuarios, function (participante, key) {

            objeto = {
                'id_usuario': participante.id_usuario,
                'id_perfil': participante.id_perfil,
                'recebe_email': '',
                'adiciona_equipe': 0,
                'add_participante': '',
                'equipe_membros': participante.artw_grupos_perfis_usuarios_equipe
            };

            $rootScope.participantes.push(objeto);

        });

        $rootScope.$apply();

        $scope.hide_load();
    };

    $scope.getGrupoByName = function (name) {

        for (i = 0; i < $scope.grupos.locais.length; i++) {
            if ($scope.grupos.locais[i].nome == name) {
                return $scope.grupos.locais[i];
            }
        }

        for (i = 0; i < $scope.grupos.globais.length; i++) {
            if ($scope.grupos.globais[i].nome == name) {
                return $scope.grupos.globais[i];
            }
        }
    };

    $scope.salvar_grupo_trabalho = function (substituir, grupo) {

        if (!$scope.grupo_nome) {
            $scope.hide_load();
            angular.element('#workGroupName').addClass('has-error');
            myAlert('Digite o Nome do Grupo de Trabalho.', 'warning');
            return;
        }

        if (((grupo && $scope.grupo_nome != grupo.nome) || !grupo) && $scope.getGrupoByName($scope.grupo_nome) && !substituir) {
            $scope.hide_load();
            angular.element('#workGroupName').addClass('has-error');
            myAlert('Já existe um Grupo de Trabalho com este nome. Escolha um nome diferente.', 'warning');

            return;
        }

        $scope.editWorkGroupModal.close();
        $scope.show_load();

        if (!substituir) {
            substituir == 0;
        }

        if ($scope.grupo_id) {

            params = {
                _token: csrf_token,
                grupo_nome: $scope.grupo_nome,
                grupo_nome: $scope.grupo_nome,
                grupo_global: $scope.grupo_global,
                grupo_id: $scope.grupo_id
            };

            url = site_url + 'site/adsmart/services/postSalvarGruposDeAprovacao';

        } else {

            params = {
                _token: csrf_token,
                grupo_nome: $scope.grupo_nome,
                grupo_global: $scope.grupo_global,
                participantes: $rootScope.participantes,
                cbMembershipProfile: $scope.cbMembershipProfile
            };

            url = site_url + 'site/adsmart/services/postGruposDeAprovacao';
        }

        $.post(url, params, function (data) {
                $scope.$apply();
                $scope.hide_load();

                myAlert('Grupo de Trabalho salvo com sucesso.', 'success');
                $scope.editWorkGroupModal.close();
            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro!', 'error');
                $scope.editWorkGroupModal.close();
                $scope.hide_load();
            });
    };

    $scope.excluir_grupo_trabalho_service = function (grupo_id) {

        $scope.show_load();

        params = {
            _token: csrf_token,
            grupo_id: grupo_id
        };

        url = site_url + 'site/adsmart/services/postExcluirGruposDeAprovacao';


        $.post(url, params, function (data) {
                myAlert('Grupo de Trabalho excluído com sucesso.', 'success');

                $scope.$apply();
                $scope.hide_load();
                $scope.carregar_grupos();
                $scope.deleteWorkGroupModal.close();
            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro!', 'error');
                $scope.deleteWorkGroupModal.close();
                $scope.hide_load();
            });
    };

    $scope.excluir_grupo_trabalho = function (grupo_id) {

        $scope.deleteWorkGroupModal = $.confirm({
            animationSpeed: 200,
            icon: 'fa fa-question',
            title: 'Confirmar',
            content: 'Deseja excluir esse grupo de trabalho?',
            draggable: false,
            dragWindowBorder: false,
            animateFromElement: false,
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            useBootstrap: false,
            boxWidth: '910px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        $scope.excluir_grupo_trabalho_service(grupo_id);
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {}
                }
            },
            onOpen: function () {
                $('body').addClass('jconfirm-overlay');
                $('body').addClass('no-scroll');
                $('.master').addClass('blurred');
                $('.jconfirm').css('overflow', 'auto');
            },
            onClose: function () {
                $('body').removeClass('jconfirm-overlay');
                $('body').removeClass('no-scroll');
                $('.master').removeClass('blurred');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };
});