var site_url = '/';
var app = angular.module('app', [
    'cp.ngConfirm',
    'selectize',
    '720kb.tooltips',
    'ngAnimate',
]);

app.controller('TicketMembrosCtrl', function ($scope, $http, $rootScope, $timeout, $ngConfirm) {
    $scope.chkAutoReprovacao = false;
    $scope.chkAprovadoresMidia = false;
    $scope.id_usuario_logado = id_usuario_logado;
    $scope.participante_inicio = id_usuario_logado;
    //$scope.participante_anexacao = id_usuario_logado;
    //$scope.participante_revisao = id_usuario_logado;
    //$scope.participante_aprovacao = id_usuario_logado;
    $scope.aprovador_midia = '';
    $scope.aprovador_midia_id = '';
    $scope.grupo_aprovacao_has_selected = false;
    $scope.participante_has_selected = false;

    $scope.aprovadoresMidiaConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecione os aprovadores de mídia',
        delimiter: ',',
        maxItems: 1,
        render: {
            option: function (data) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () { }
    };

    $scope.participantesConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecione o participante',
        maxItems: 1,
        options: '',
        render: {
            option: function (data) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () { }
    };

    $scope.participantes = [];
    $scope.aprovadores_midia = [];
    $scope.usuarios_selecionados = [];
    $scope.usuarios_anexacao_selecionados = [];
    $scope.usuarios_revisao_selecionados = [];

    $scope.show_load = function () {
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        $('.lds-css').hide();
    };

    $scope.getById = function (id, array) {
        for (i = 0; i < array.length; i++) {
            if (array[i].id == id) {
                return array[i];
            }
        }
    };

    $scope.carregar_usuarios = function () {
        $scope.show_load();
        $.get(site_url + 'site/artwork/services/package/usuarios', function (data) {
            $scope.participantes = data.usuarios;
            $scope.$apply();
            $scope.hide_load();
        }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
    };

    $scope.carregar_usuarios();

    $scope.condicoes_uncheck = function () {
        $scope.limpar_tarefas_participantes();
    };

    $scope.auto_aprovacao_check = function () {
        $scope.condicoes_uncheck();

        if ($scope.chkAutoReprovacao === true) {
            $scope.participantes_anexacao = id_usuario_logado;
            $scope.participantes_revisao = id_usuario_logado;
            $scope.participantes_aprovacao = id_usuario_logado;
            $scope.chkAprovadoresMidia = false;
        }
    };

    $scope.aprovadores_midia_check = function () {
        $scope.condicoes_uncheck();

        if ($scope.chkAprovadoresMidia === true) {
            $scope.chkAutoReprovacao = false;
        }
    };

    $scope.carregar_aprovadores = function () {
        $scope.show_load();
        $.get(site_url + 'site/adsmart/services/aprovadoresMidia/' + id_tipo, function (data) {
            $scope.aprovadores_midia = data;
            $scope.$apply();
            $scope.hide_load();
        }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
    };

    $scope.carregar_aprovadores_midia = function () {
        $scope.grupo_aprovacao_has_selected = false;    
        
        $scope.show_load();
        
        if ($scope.aprovador_midia) {
            $.get(site_url + 'site/adsmart/services/aprovadorMidia/' + $scope.aprovador_midia, function (data) {
                $scope.usuarios_selecionados = data;
                $scope.$apply();
                $scope.hide_load();
            }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
        } else if (!$scope.aprovador_midia) {
            $scope.usuarios_selecionados = [];
            $scope.participantes_aprovacao = [];
            $scope.grupo_aprovacao_has_selected = false;
            $scope.hide_load();
            return;
        }
    };

    $scope.add_participante_anexacao = function () {
        var i = $scope.getById($scope.participantes_anexacao, $scope.participantes);

        if ($scope.getById($scope.participantes_anexacao, $scope.usuarios_anexacao_selecionados)) {
            myAlert('Este participante já foi adicionado.', 'warning');
            return false;
        } else if ($scope.participantes_anexacao === '' || $scope.participantes_anexacao === undefined) {
            myAlert('Não foi selecionado um participante.', 'warning');
            return false;
        } else {
            $scope.usuarios_anexacao_selecionados.push(i);
            $scope.participantes_anexacao = '';
            
            if ($scope.usuarios_anexacao_selecionados.length > 1) {
                $scope.usuarios_anexacao_selecionados.splice(1, 1);
                myAlert('Já foi adicionando um participante para esta tarefa.', 'warning');
                return;
            }
        }
    };

    $scope.add_participante_revisao = function () {
        var i = $scope.getById($scope.participantes_revisao, $scope.participantes);

        if ($scope.getById($scope.participantes_revisao, $scope.usuarios_revisao_selecionados)) {
            myAlert('Este participante já foi adicionado.', 'warning');
            return false;
        } else if ($scope.participantes_revisao === '' || $scope.participantes_revisao === undefined) {
            myAlert('Não foi selecionado um participante.', 'warning');
            return false;
        } else {
            $scope.usuarios_revisao_selecionados.push(i);
            $scope.participantes_revisao = '';
        }
    };

    $scope.add_participante_aprovacao = function () {
        var i = $scope.getById($scope.participantes_aprovacao, $scope.participantes);

        if ($scope.getById($scope.participantes_aprovacao, $scope.usuarios_selecionados)) {
            myAlert('Este participante já foi adicionado.', 'warning');
            return false;
        } else if ($scope.participante_aprovacao === '' || $scope.participantes_aprovacao === undefined) {
            myAlert('Não foi selecionado um participante.', 'warning');
            return false;
        } else {
            $scope.usuarios_selecionados.push(i);
            $scope.participantes_aprovacao = '';
            $scope.participante_has_selected = true;
        }
    };

    $scope.add_aprovadores_midia = function () {
        $scope.aprovador_midia_id = $scope.aprovador_midia;
        $scope.aprovador_midia = '';
        $scope.grupo_aprovacao_has_selected = true;
    };

    $scope.excluir_participante_anexacao = function (u) {
        $scope.usuarios_anexacao_selecionados.splice(u, 1);
    };

    $scope.excluir_participante_revisao = function (u) {
        $scope.usuarios_revisao_selecionados.splice(u, 1);
    };

    $scope.excluir_participante_aprovacao = function (index) {
        $scope.usuarios_selecionados.splice(index, 1);

        if($scope.usuarios_selecionados.length === 0) {
            $scope.participante_has_selected = false;    
        }
    };

    $scope.excluir_aprovadores_midia = function () {
        $scope.aprovador_midia = '';
        $scope.usuarios_selecionados = [];
        $scope.grupo_aprovacao_has_selected = false;
    };

    $scope.enviar_formulario = function ($event) {
        if (!$scope.chkAutoReprovacao && !$scope.chkAprovadoresMidia) {
            myAlert('Para concluir a abertura do ticket é necessário selecionar uma das opções em Condições de Aprovação.', 'warning');
            $event.preventDefault();
            return false;
        }
    };

    $scope.limpar_tarefas_participantes = function () {
        $scope.participantes_anexacao = '';
        $scope.participantes_revisao = '';
        $scope.participantes_aprovacao = '';
        $scope.aprovador_midia = '';
        $scope.usuarios_selecionados = [];
        $scope.usuarios_anexacao_selecionados = [];
        $scope.usuarios_revisao_selecionados = [];
        $scope.grupo_aprovacao_has_selected = false;
        $scope.participante_has_selected = false;    
    };

    $scope.carregar_aprovadores();
});