var tarefas = angular.module("abaTarefasMod", []);

tarefas.controller('abaTarefasCtrl', function (
        $scope,
        $location,
        $rootScope
    ) {
    $rootScope.statechange = true;

    $rootScope.activetab = $location.path();



    $scope.renovarAprovacaoService = function (participante) {
        $scope.interval_tickets_stop();
        $scope.show_load();

        params = {
            _token: csrf_token,
            id_participante: participante.id_participante
        };

        $.post(site_url + 'site/artwork/services/package/ticket/renovarAprovacao', params, function (data) {

            if (data.error) {
                myAlert(data.message, 'error');
                $scope.carregar_ticket(1);
            } else {
                myAlert(data.message, 'success');
                $scope.carregar_ticket(1);
            }

            $scope.interval_tickets_start();

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
    };

    $scope.renovarAprovacao = function (participante, nomeMembro) {
        
        if (nomeMembro) {
            
            var message = 'Renovar a aprovação de ' + nomeMembro + '?';
            
        } else {
            
            var message = 'Renovar a aprovação de ' + participante.nome_usuario + '?';
            
        }
        
        

        $.confirm({
            icon: 'icon fa fa-question',
            title: 'Confirmar',
            content: message,
            closeIcon: true,
            boxWidth: '910px',
            useBootstrap: false,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            buttons: {
                Renovar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Renovar Aprovação</span>',
                    btnClass: 'btn-confirm',
                    action: function (Todos) {
                        $scope.renovarAprovacaoService(participante);
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel'
                }
            },
            onOpenBefore: function () {
                $('body').addClass('jconfirm-overlay');
                $('body').addClass('no-scroll');
                $('.master').addClass('blurred');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('jconfirm-overlay');
                $('body').removeClass('no-scroll');
                $('.master').removeClass('blurred');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };
    

    
    $scope.enviarLembreteService = function (id_participante, id_perfil, todos, tipo) {
        $scope.show_load();

        if (tipo == 'id_membro') {
            
            params = {
                _token: csrf_token,
                id_membro: id_participante,
                id_perfil: id_perfil,
                todos: todos
            };
            
        } else {
            
            params = {
                _token: csrf_token,
                id_participante: id_participante,
                id_perfil: id_perfil,
                todos: todos
            };
            
        }

        $.post(site_url + 'site/artwork/services/package/ticket/enviarLembrete', params, function (data) {
            myAlert(data.message, 'success');
            $scope.carregar_ticket(1);
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.interval_tickets_stop();
                    $scope.hide_load();
                });
    };

    $scope.enviarLembrete = function (id_participante, id_perfil, enviadas, tipo) {
        if (enviadas) {
            myAlert('Notificação já enviada recentemente, você poderá enviar uma nova notificação dentro de 24h.', 'alert');
            return false;
        }

        if ($scope.ticket.nr_perfis[id_perfil].qtd > 1) {
            var message = 'O lembrete pode ser enviado para todos os Aprovadores e seus respectivos membros de equipe ou apenas para o Aprovador individual selecionado. Deseja enviar para:';

            $.confirm({
                icon: 'icon fa fa-question',
                title: 'Confirmar',
                content: message,
                closeIcon: true,
                closeIconClass: 'jconfirm-btn-close fa fa-times',
                useBootstrap: false,
                boxWidth: '910px',
                buttons: {
                    Participantes: {
                        text: '<span><i class="icon fa fa-check-square-o"></i>Apenas o Selecionado</span>',
                        btnClass: 'btn btn-common btn-confirm',
                        action: function (Participantes) {
                            $scope.enviarLembreteService(id_participante, id_perfil, 0, tipo);
                        }
                    },
                    Todos: {
                        keys: ['enter', 'space'],
                        text: '<span><i class="icon fa fa-globe"></i>Todos os Aprovadores</span>',
                        btnClass: 'btn btn-common btn-confirm',
                        action: function (Todos) {
                            $scope.enviarLembreteService(id_participante, id_perfil, 1, tipo);
                        }
                    },
                    Cancelar: {
                        keys: ['esc'],
                        text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                        btnClass: 'btn btn-cancel',
                        action: function (Cancelar) {}
                    }
                },
                onOpenBefore: function () {
                    $('body').addClass('jconfirm-overlay');
                    $('body').addClass('no-scroll');
                    $('.master').addClass('blurred');
                    $('.jconfirm').css('overflow', 'auto');
                    $('.jconfirm').overlayScrollbars({
                        scrollbars: {
                            autoHide: 'move'
                        }
                    });
                },
                onClose: function () {
                    $('body').removeClass('jconfirm-overlay');
                    $('body').removeClass('no-scroll');
                    $('.master').removeClass('blurred');
                    $('.jconfirm').css('overflow', 'hidden');
                }
            });
        } else {
            $scope.enviarLembreteService(id_participante, id_perfil, 0, tipo);
        }
    };
    
});
