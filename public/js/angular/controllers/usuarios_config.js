var site_url = '/';
var app = angular.module('app', ['ui.mask']);

app.directive('maskChange', function () {
    return {
        restrict: 'A',
        scope: {
            maskChange: "="
        },
        require: '?ngModel',
        link: function (scope, elem, attrs, ngModel) {

            var novoTel, flag = false,
                val;

            elem.off('keyup');
            elem.on('keyup', function (ev) {

                if (/^\d+$/.test(ev.key) || ev.key == 'Backspace' || ev.key == 'Delete') {

                    novoTel = String(ngModel.$viewValue).replace(/[\(\)\_\-/\s]/g, '')

                    if (novoTel.length == 10 && !flag) {
                        flag = true;
                        scope.maskChange = "(99)9999-9999";
                        scope.$apply();
                    } else if (novoTel.length == 10 && flag) {
                        flag = false;
                        scope.maskChange = "(99)9?9999-9999";

                        scope.$apply();
                        ngModel.$viewValue += ev.key
                        ngModel.$render();

                    } else if (novoTel.length < 10) {
                        flag = false;
                    }
                }
            });
        }

    };
});

app.controller('UsuariosConfigCtrl', function ($scope) {
    $scope.nome = '';
    $scope.sobrenome = '';
    $scope.email = '';
    $scope.telefone = '';
    $scope.phoneMask = '(99)9999-9999';
    $scope.id_departamento = '';
    $scope.departamento_nome = '';
    $scope.id_cargo = '';
    $scope.cargo_nome = '';
    $scope.old_password = '';
    $scope.password = '';
    $scope.password_confirmation = '';

    $scope.show_load = function () {
        $('.lds-css').show();
    }

    $scope.hide_load = function () {
        $('.lds-css').hide();
    }

    $scope.add_usuario_submit = function ($event) {
        var emailRegex = /^[a-z0-9][a-z0-9-_\.]+@([a-z]|[a-z0-9]?[a-z0-9-]+[a-z0-9])\.[a-z0-9]{2,10}(?:\.[a-z]{2,10})?$/;
    
        if ($scope.nome === '') {
            angular.element('#editNome').addClass('has-error');
            myAlert('Nome é obrigatório.', 'error');
            $event.preventDefault();
            return false;
        } else {
            angular.element('#editNome').removeClass('has-error');
        }

        if ($scope.sobrenome === '') {
            angular.element('#editSobrenome').addClass('has-error');
            myAlert('Sobrenome é obrigatório.', 'error');
            $event.preventDefault();
            return false;
        } else {
            angular.element('#editSobrenome').removeClass('has-error');
        }

        if ($scope.email === '' || !emailRegex.test($scope.email)) {
            angular.element('#editEmail').addClass('input-error');
            myAlert('Email é obrigatório.', 'error');
            $event.preventDefault();
            return false;
        } else {
            angular.element('#editEmail').removeClass('has-error');
        }
        
        if ($scope.old_password !== '' || $scope.password !== '' || $scope.password_confirmation !== '') {
            if ($scope.old_password.length < 6) {
                angular.element('#inputOldPwd').addClass('has-error');
                myAlert('A Senha Atual deve conter no mínimo 6 caracteres.', 'warning');
                $event.preventDefault();
                return false;
            } else {
                angular.element('#inputOldPwd').removeClass('has-error');
            }
            
            if ($scope.password === '') {
                angular.element('#inputNewPwd').addClass('has-error');
                myAlert('Digite sua Nova Senha.', 'warning');
                $event.preventDefault();
                return false;
            } else {
                angular.element('#inputNewPwd').removeClass('has-error');
            }

            /* Critérios da nova senha 
                - Deve conter letras maiúsculas 
                - Deve conter letras minúsculas
                - Deve conter números
                - Deve conter no mínimo 6 caracteres 
            */
            if(!$scope.password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
                angular.element('#inputNewPwd').focus();
                angular.element('#inputNewPwd').addClass('has-error');
                myAlert('A Nova Senha deve conter letras maiúsculas e minúsculas.', 'warning');
                $event.preventDefault();
                return false;
            } else {
                angular.element('#inputNewPwd').removeClass('has-error');
            }

            if(!$scope.password.match(/([0-9])/)) {
                angular.element('#inputNewPwd').focus();
                angular.element('#inputNewPwd').addClass('has-error');
                myAlert('A Nova Senha deve conter números.', 'warning');
                $event.preventDefault();
                return false;
            } else {
                angular.element('#inputNewPwd').removeClass('has-error');
            }

            if ($scope.password.length < 6) {
                angular.element('#inputNewPwd').addClass('has-error');
                myAlert('A Nova Senha deve conter no mínimo 6 caracteres.', 'warning');
                $event.preventDefault();
                return false;
            } else {
                angular.element('#inputNewPwd').removeClass('has-error');
            }

            if ($scope.password_confirmation === '') {
                angular.element('#inputConfNewPwd').addClass('has-error');
                myAlert('Digite novamente sua Nova Senha.', 'warning');
                $event.preventDefault();
                return false;
            } else {
                angular.element('#inputConfNewPwd').removeClass('has-error');
            }

            if ($scope.password_confirmation !== $scope.password) {
                angular.element('#inputConfNewPwd').addClass('has-error');
                myAlert('As duas senhas não se coincidem.', 'warning');
                $event.preventDefault();
                return false;
            } else {
                angular.element('#inputConfNewPwd').removeClass('has-error');
            }
        }
    };
});