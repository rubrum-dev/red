var visaoGeral = angular.module("abaVisaoGeralMod", []);

visaoGeral.controller('abaVisaoGeralCtrl', function (
        $scope,
        $rootScope,
        $ngConfirm,
        $window,
        Upload,
        $timeout
    ) {
    $rootScope.statechange = true;

    $scope.acao_solicitacao = function (id_solicitacao, status) {
        $scope.show_load();

        params = {
            _token: csrf_token,
            id: id_solicitacao,
            status: status
        };

        $.post(site_url + 'site/artwork/services/package/solicitacao/acao', params, function (data) {
            $scope.carregar_ticket(1);
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                });
    };
    
    $scope.renovarAprovacaoService = function (participante) {
        $scope.interval_tickets_stop();
        $scope.show_load();

        params = {
            _token: csrf_token,
            id_participante: participante.id_participante
        };

        $.post(site_url + 'site/artwork/services/package/ticket/renovarAprovacao', params, function (data) {

            if (data.error) {
                myAlert(data.message, 'error');
                $scope.carregar_ticket(1);
            } else {
                myAlert(data.message, 'success');
                $scope.carregar_ticket(1);
            }

            $scope.interval_tickets_start();

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
    };

    $scope.renovarAprovacao = function (participante) {
        var message = 'Renovar a aprovação de ' + participante.nome_usuario + '?';

        $.confirm({
            icon: 'icon icon-question',
            title: 'Confirmar',
            content: message,
            closeIcon: true,
            boxWidth: '850px',
            useBootstrap: false,
            closeIconClass: 'jconfirm-btn-close icon-cross',
            buttons: {
                Renovar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon icon-check"></i>Renovar Aprovação</span>',
                    btnClass: 'btn-confirm',
                    action: function (Todos) {
                        $scope.renovarAprovacaoService(participante);
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon icon-ban-circle"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel'
                }
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };

    $scope.cancelarEnvioService = function (id_arte_envio) {
        $scope.interval_tickets_stop();
        $scope.show_load();

        params = {
            _token: csrf_token,
            id_arte_envio: id_arte_envio
        };

        $.post(site_url + 'site/artwork/services/package/ticket/cancelarEnvio', params, function (data) {

            if (data.error) {
                myAlert(data.message, 'error');
                $scope.carregar_ticket(1);
            } else {
                myAlert(data.message, 'success');
                $scope.carregar_ticket(1);
            }

            $scope.interval_tickets_start();
            $scope.hide_load();

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
    };

    $scope.cancelarEnvio = function (id_arte_envio) {
        var message = 'Deseja mesmo cancelar esse envio? Caso prossiga o fornecedor será notificado por e-mail para que não prossiga com o processo de produção desta arte-final.';

        $.confirm({
            icon: 'icon fa fa-question',
            title: 'Confirmar',
            content: message,
            closeIcon: true,
            boxWidth: '910px',
            useBootstrap: false,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            buttons: {
                Renovar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Cancelar Envio</span>',
                    btnClass: 'btn-confirm',
                    action: function (Todos) {
                        $scope.cancelarEnvioService(id_arte_envio);
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel'
                }
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };

    $scope.setStatusEnvio = function (id_arte_envio) {
        $scope.interval_tickets_stop();
        $scope.show_load();

        params = {
            _token: csrf_token,
            id_arte_envio: id_arte_envio
        };

        $.post(site_url + 'site/artwork/services/package/ticket/setStatusEnvio', params, function (data) {
            if (data.error) {
                myAlert(data.message, 'error');
                $scope.carregar_ticket(1);
            } else {
                myAlert(data.message, 'success');
                $scope.carregar_ticket(1);
            }

            $scope.interval_tickets_start();
            $scope.hide_load();
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.carregar_ticket(1);
                });
    };

    $scope.renovarLink = function (id_arte_envio) {
        $scope.interval_tickets_stop();
        $scope.show_load();

        params = {
            _token: csrf_token,
            id_arte_envio: id_arte_envio
        };

        $.post(site_url + 'site/artwork/services/package/ticket/renovarLink', params, function (data) {
            if (data.error) {
                myAlert(data.message, 'error');
            } else {
                myAlert(data.message, 'success');
                $scope.carregar_ticket(1);
            }

            $scope.interval_tickets_start();
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.carregar_ticket(1);
                });
    };

    $scope.reenviarEmail = function (id_arte_envio) {
        $scope.interval_tickets_stop();
        $scope.show_load();

        params = {
            _token: csrf_token,
            id_arte_envio: id_arte_envio
        };

        $.post(site_url + 'site/artwork/services/package/ticket/reenviarEmail', params, function (data) {
            if (data.error) {
                myAlert(data.message, 'error');
                $scope.carregar_ticket(1);
            } else {
                myAlert(data.message, 'success');
                $scope.carregar_ticket(1);
            }

            $scope.interval_tickets_start();

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.carregar_ticket(1);
                });
    };

    $scope.enviarLembreteService = function (id_participante, id_perfil, todos) {
        $scope.show_load();

        params = {
            _token: csrf_token,
            id_participante: id_participante,
            id_perfil: id_perfil,
            todos: todos
        };

        $.post(site_url + 'site/artwork/services/package/ticket/enviarLembrete', params, function (data) {
            myAlert(data.message, 'success');
            $scope.carregar_ticket(1);
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.interval_tickets_stop();
                    $scope.hide_load();
                });
    };

    $scope.enviarLembrete = function (id_participante, id_perfil, enviadas) {
        if (enviadas) {
            myAlert('Notificação já enviada recentemente, você poderá enviar uma nova notificação dentro de 24h.', 'alert');
            return false;
        }

        if ($scope.ticket.nr_perfis[id_perfil].qtd > 1) {
            var message = 'O lembrete pode ser enviado para todos os Aprovadores ou apenas para o participante selecionado.<br /> Deseja enviar para:';

            $.confirm({
                icon: 'icon icon-question',
                title: 'Confirmar',
                content: message,
                closeIcon: true,
                closeIconClass: 'jconfirm-btn-close icon-cross',
                useBootstrap: false,
                boxWidth: '910px',
                buttons: {
                    Participantes: {
                        text: '<span>Apenas o Selecionado</span>',
                        btnClass: 'btn btn-common btn-confirm',
                        action: function (Participantes) {
                            $scope.enviarLembreteService(id_participante, id_perfil, 0);
                        }
                    },
                    Todos: {
                        keys: ['enter', 'space'],
                        text: '<span>Todos os Aprovadores</span>',
                        btnClass: 'btn btn-common btn-confirm',
                        action: function (Todos) {
                            $scope.enviarLembreteService(id_participante, id_perfil, 1);
                        }
                    },
                    Cancelar: {
                        keys: ['esc'],
                        text: '<span><i class="icon icon-ban-circle"></i>Cancelar</span>',
                        btnClass: 'btn btn-cancel',
                        action: function (Cancelar) {}
                    }
                },
                onOpenBefore: function () {
                    $('body').addClass('no-scroll');
                    $('.jconfirm').css('overflow', 'auto');
                    $('.jconfirm').overlayScrollbars({
                        scrollbars: {
                            autoHide: 'move'
                        }
                    });
                },
                onClose: function () {
                    $('body').removeClass('no-scroll');
                    $('.jconfirm').css('overflow', 'hidden');
                }
            });
        } else {
            $scope.enviarLembreteService(id_participante, id_perfil, 0);
        }
    };

    $scope.verMsgFornecedor = function(mensagem) {
        
        mensagem = (mensagem) ? mensagem : 'Nenhuma mensagem foi incluída no envio da arte para o fornecedor.';
        
        $ngConfirm({
            useBootstrap: false,
            boxWidth: '910px',
            title: '',
            content: '<div class="ng-confirm-action-dialog margin-bottom-25">' +
                '<div class="dialog-heading">' +
                    '<h4>Mensagem enviada</h4>' +
                '</div>' +
                '<div class="text no-margin">' +    
                    '<p ng-if="!msg_fornecedor" class="no-margin">'+mensagem+'</p>' +
                '</div>' +
            '</div>',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'ng-confirm-btn-close fa fa-times',
            backgroundDismiss: true,
            onOpenBefore: function() {
                setTimeout(function () {
                    $('.ng-confirm .ng-confirm-scrollpane').overlayScrollbars({
                        scrollbars: {
                            autoHide: 'move'
                        }
                    });
                }, 0);
            },
            onOpen: function () {
                $('body').addClass('no-scroll');
            }
        });    
    };

    $scope.getArquivoById = function (id) {
        for (i = 0; i < $scope.ticket.comparacao.length; i++) {
            if ($scope.ticket.comparacao[i].id == id) {
                return $scope.ticket.comparacao[i];
            }
        }
    };

    $scope.enviarCompararArtes = function (e) {
        if (!$scope.atrw_arquivo_atual) {
            $('#artwFileCurrent ~ .selectize-control > .selectize-input').addClass('input-error');

            myAlert('Selecione um arquivo para o ciclo atual.');
            e.preventDefault()
        } else {
            $('#artwFileCurrent ~ .selectize-control > .selectize-input').removeClass('input-error');
        }

        if (!$scope.atrw_arquivo_anterior) {
            $('#artwFilePrevious ~ .selectize-control > .selectize-input').addClass('input-error');

            myAlert('Selecione um arquivo para o ciclo anterior.');
            e.preventDefault()
        } else {
            $('#artwFilePrevious ~ .selectize-control > .selectize-input').removeClass('input-error');
        }

        if ($scope.atrw_arquivo_atual && $scope.atrw_arquivo_anterior) {
            var arquivo1 = $scope.getArquivoById($scope.atrw_arquivo_atual);
            var arquivo2 = $scope.getArquivoById($scope.atrw_arquivo_anterior);

            $window.open('/site/artwork/compare/' + arquivo1.id + '/' + arquivo1.tipo + '/' + arquivo2.id + '/' + arquivo2.tipo + '/' + $scope.ticket.id);
        } else {
            return false;
        }
    };

    $scope.comparar_artes = function () {
        $ngConfirm({
            useBootstrap: false,
            boxWidth: '910px',
            title: '',
            contentUrl: '/view/modal_comparar_artes.html',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'ng-confirm-btn-close fa fa-times',
            backgroundDismiss: true,
            escapeKey: 'Cancelar',
            buttons: {
                Cancelar: {
                    keys: ['esc'],
                    text: 'Cancelar',
                    btnClass: 'btn-control btn-alt',
                    action: function (Cancelar) {}
                },
                Salvar: {
                    keys: ['enter', 'space'],
                    text: 'Comparar',
                    btnClass: 'btn-control btn-positive',
                    action: function (Salvar) {
                        $scope.enviarCompararArtes();
                    }
                }
            },
            onOpenBefore: function () {
                setTimeout(function () {
                    $('.ng-confirm .ng-confirm-scrollpane').overlayScrollbars({
                        scrollbars: {
                            autoHide: 'move'
                        }
                    });
                }, 0);
            },
            onReady: function () {
                $timeout(function () {
                    $('.ng-confirm-box .line-wobble').remove();
                    $('.ng-confirm-content').css('visibility', 'visible');
                    $('.ng-confirm-buttons').show();
                }, new Date().getMilliseconds());
            },
            onOpen: function () {
                $('.ng-confirm-content').css('visibility', 'hidden');
                $('body').addClass('no-scroll');
                $('.ng-confirm-buttons').hide();
                $('.ng-confirm-box').append(customLoadingHTML);
                $scope.interval_tickets_stop();
            },
            onClose: function () {
                $scope.interval_tickets_start();
            }
        });

    };
    
    $scope.comparar_artes_historico = function () {
        $ngConfirm({
            useBootstrap: false,
            boxWidth: '910px',
            title: '',
            contentUrl: '/view/modal_comparar_artes_historico.html',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'ng-confirm-btn-close fa fa-times',
            backgroundDismiss: true,
            escapeKey: 'Cancelar',
            buttons: {
                Cancelar: {
                    keys: ['esc'],
                    text: 'Cancelar',
                    btnClass: 'btn-control btn-alt',
                    action: function (Cancelar) {}
                },
                Salvar: {
                    keys: ['enter', 'space'],
                    text: 'Comparar',
                    btnClass: 'btn-control btn-positive',
                    action: function (Salvar) {
                        $scope.enviarCompararArtes();
                    }
                }
            },
            onReady: function () {
                $timeout(function () {
                    $('.ng-confirm-box .line-wobble').remove();
                    $('.ng-confirm-content').css('visibility', 'visible');
                    $('.ng-confirm-buttons').show();
                }, new Date().getMilliseconds());
            },
            onOpenBefore: function() {
                setTimeout(function () {
                    $('.ng-confirm .ng-confirm-scrollpane').overlayScrollbars({
                        scrollbars: {
                            autoHide: 'move'
                        }
                    });
                }, 0);
            },
            onOpen: function () {
                $('.ng-confirm-content').css('visibility', 'hidden');
                $('body').addClass('no-scroll');
                $('.ng-confirm-buttons').hide();
                $('.ng-confirm-box').append(customLoadingHTML);

                $scope.interval_tickets_stop();
            },
            onClose: function () {
                $scope.interval_tickets_start();
            }
        });

    };
    
    $scope.enviar_emails = function (a, id_arte) {
        if (!$scope.enviar_emails[a]) {
            $('body').find('.artw-sending-group select').addClass('has-error');
            $('body').find('.artw-sending-group input').addClass('has-error');

            myAlert('Selecione o Fornecedor ou digite o e-mail de um destinatário específico.', 'warning');

            return false;
        }

        $('body').find('.artw-sending-group select').removeClass('has-error');
        $('body').find('.artw-sending-group input').removeClass('has-error');

        $scope.show_load();

        if ($scope.ticket.artes[a].fornecedorSelecionado) {
            var fornecedor = $scope.fornecedores[$scope.ticket.artes[a].fornecedorSelecionado];
            fornecedor = fornecedor.id;
        } else {
            var fornecedor = null;
        }

        params = {
            _token: csrf_token,
            id: id_arte,
            emails: $scope.enviar_emails[0],
            msg_fornecedor: $scope.ticket.artes[a].msg_fornecedor,
            fornecedor: fornecedor
        };

        $.post(site_url + 'site/artwork/services/package/ticket/enviarEmails', params, function (data) {
            if (data.success === 201) {
                myAlert(data.message, 'alert');
                $scope.hide_load();
            } else {
                myAlert(data.message, 'success');

                $scope.enviar_emails[a] = '';
                $scope.ticket.artes[a].msg_fornecedor = '';

                $('#cbSupplierChooser').val('');
                $('#cbSupplierChooser').blur();
                $('#msg_fornecedor').val('');
                $('#msg_fornecedor').blur();

                $scope.hide_load();
                $scope.carregar_ticket(1);
            }
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
    };

    $scope.setUploadArte = function (element) {
        $scope.$apply(function (scope) {
            // Turn the FileList object into an Array
            for (var i = 0; i < element.files.length; i++) {
                $scope.files_upload_arte.push(element.files[i]);
            }
        });
    };

    $scope.setLayout = function (element) {
        $scope.$apply(function (scope) {
            // Turn the FileList object into an Array
            for (var i = 0; i < element.files.length; i++) {
                $scope.files_layout.push(element.files[i]);
            }
        });
    };

    $scope.uploadArte = function (id) {
        if (!$scope.arte) {
            $('body').find('.artwork-upload-section .file-attachment-container .file-attachment-control').css('borderColor', '#EA4334');
            $('body').find('.artwork-upload-section .file-attachment-container .file-attachment-control').addClass('input-error');
            myAlert('Anexe um Arquivo.', 'warning');
            return false;
        }

        $scope.arte.progress = 0;
        $scope.interval_tickets_stop();

        $('body').find('.artwork-upload-section .file-attachment-container .file-attachment-control').css('borderColor', '#ACACAC');
        $('body').find('.artwork-upload-section .file-attachment-container .file-attachment-control').removeClass('input-error');
        $('body').find('.artwork-upload-section .file-attachment-container input[type="text"]').removeClass('input-error');

        $scope.arte.upload = Upload.upload({
            url: site_url + 'site/artwork/services/package/ticket/uploadArte',
            data: {
                id: id,
                file0: $scope.arte
            }
        });

        $scope.arte.upload.then(function (response) {
            $timeout(function () {
                $scope.arte = '';
                $scope.carregar_ticket(1);
                $scope.limparCamposUpload();
            });
        }, function (response) {
            if (response.status > 0) {

                myAlert('Ocorreu um erro.', 'error');
                $scope.arte.progress = 0;
                $scope.interval_tickets_start();

            }
        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            $scope.arte.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });

    };

    $scope.uploadLayout = function (id) {
        if (!$scope.layout) {
            $('body').find('.attachPDF .files-control-fake').addClass('has-error');
            myAlert('Anexe um Layout.', 'warning');
            return false;
        }

        if ($scope.layout.size > 52428800) {
            $('body').find('.attachPDF .files-control-fake').addClass('has-error');
            myAlert('O tamanho máximo permitido para um arquivo é 50 MB.', 'warning');
            return false;
        }
    
        $scope.layout.progress = 0;
        $scope.interval_tickets_stop();

        $('body').find('.attachPDF .files-control-fake').removeClass('has-error');

        $scope.layout.upload = Upload.upload({
            url: site_url + 'site/artwork/services/package/ticket/uploadLayout',
            data: {
                id: id,
                file0: $scope.layout
            }
        });

        $scope.layout.upload.then(function (response) {
            $timeout(function () {
                $scope.layout = '';
                myAlert('Layout salvo com sucesso.', 'success');
                $scope.carregar_ticket(1);
                $scope.limparCamposUpload();
            });
        }, function (response) {
            if (response.status > 0) {

                myAlert('Ocorreu um erro.', 'error');
                $scope.layout.progress = 0;
                $scope.interval_tickets_start();

            }

        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            $scope.layout.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });

    };

    $scope.deleteArte = function (id) {
        $scope.show_load();

        params = {
            _token: csrf_token,
            id: id
        };

        $.post(site_url + 'site/artwork/services/package/ticket/deleteArte', params, function (data) {
            $scope.carregar_ticket(1);
            $scope.interval_tickets_start();
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
    };

    $scope.notificarPDFAnexado = function(id) {
        // TO-DO
        $scope.show_load();
        
        params = {
            _token: csrf_token,
            pdf_notificacao: $scope.pdf_notificacao
        };
        
        $.post(site_url + 'site/artwork/services/package/ticket/acaoNotificarPdfAnexado', params, function (data) {
                myAlert('Voce está sendo notificado por esse PDF anexado.', 'success');
            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
    };

    $scope.setar_fornecedor = function (arte) {
        var fornecedor = $scope.fornecedores[$scope.ticket.artes[arte].fornecedorSelecionado];

        if (fornecedor && fornecedor.emails) {
            $scope.enviar_emails[arte] = fornecedor.emails;
        } else {
            $scope.enviar_emails[arte] = null;
        }
    };

    $scope.carregar_fornecedores = function () {
        //$rootScope.loading = true;
        //$rootScope.statechange = false;
                
        $.get(site_url + 'site/artwork/services/package/fornecedores', function (data) {
            $scope.fornecedores = data;
            //$rootScope.loading = false;
            //$rootScope.statechange = true;
            $scope.$apply();
        }, 'json')
                .fail(function (data) {
                });
    };

    $scope.customFileInputCtrl();
});