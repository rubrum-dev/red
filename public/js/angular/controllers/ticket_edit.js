var site_url = '/';
var app = angular.module('app', []);

app.controller('TicketEditCtrl', function ($scope)
{
    $scope.solicitacoes = [
        //{'id': '1', 'descricao': 'Teste 1'},
        //{'id': '2', 'descricao': 'Teste 2'}
    ];
    $scope.tipos = [];
    $scope.ticket = {};

    $scope.show_load = function()
    {
        //$('.master').addClass('blurred');
        $('.lds-css').show();
    }

    $scope.hide_load = function()
    {
        //$('.master').removeClass('blurred');
        $('.lds-css').hide();
    }

    $scope.carregar_ticket = function ()
    {
        $scope.show_load();

        $.get(site_url + 'site/artwork/services/package/ticket/' + id_ticket, function (data)
        {
            $scope.ticket = data;

            $scope.solicitacoes = (data.ciclo_atual.solicitacoes) ? data.ciclo_atual.solicitacoes : [];

            $scope.$apply();
            $scope.hide_load();

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro!', 'error');
                    $scope.hide_load();
                });
    }

    $scope.carregar_tipos = function ()
    {
        $.get(site_url + 'site/artwork/services/package/request/types', function (data)
        {
            $scope.tipos = data;

            $scope.$apply();

        }, 'json');
    }

    $scope.adicionar = function ()
    {
        if ($scope.add_tipo && $scope.add_descricao)
        {
            var template = {'id_tipo': $scope.add_tipo, 'descricao': $scope.add_descricao};

            //console.log(template);

            $scope.solicitacoes.push(template);

            $scope.add_tipo = '';
            $scope.add_descricao = '';
        }
    }

    $scope.remover = function (k)
    {
        $scope.solicitacoes.splice(parseInt(k), 1);

        //console.log(parseInt(k));
        //console.log($scope.participantes);
    }

    $scope.carregar_tipos();
    $scope.carregar_ticket();

});
