var site_url = '/';
var app = angular.module('app', ['selectize', '720kb.tooltips']);

app.controller('PackageCreateCtrl', function ($scope) {
    $scope.familias = [];
    $scope.produtos = [];
    $scope.tipos = [];
    $scope.tamanhos = [];
    $scope.categorias = [];
        
    $scope.campanhas = [{
        'id': '0',
        'nome': 'Cadastrar nova campanha'
    }];
    
    $scope.cbPckFamily = '11';
    $scope.cbNewCategory = '2';
    $scope.cbPckProduct = '';
    $scope.cbPckType = '';
    $scope.cbPckContentSize = '';
    $scope.packageType = '';
    $scope.cbPckCampaign = '';
    $scope.packageCampaignName = '';

    $scope.cbPckNatureza = '';

    $scope.naturezas = [{
        id: 2,
        nome: 'Primária'
    }, {
        id: 3,
        nome: 'Secundária'
    }, {
        id: 4,
        nome: 'Terciária'
    }];
    $scope.embalagens_primarias = [{
        id: 1,
        nome: 'Skol Garrafa Vidro 600ml'
    }, {
        id: 2,
        nome: 'Skol Garrafa Vidro 600ml • OW'
    }, {
        id: 3,
        nome: 'Skol Garrafa Vidro 600ml • Carnaval 2019'
    }, {
        id: 4,
        nome: 'Skol Garrafa Vidro 600ml • Rótulo Quadrado'
    }];
    $scope.embalagens_secundarias = [{
        id: 1,
        nome: 'Skol Garrafa Vidro 600ml'
    }, {
        id: 2,
        nome: 'Skol Garrafa Vidro 600ml • OW'
    }, {
        id: 3,
        nome: 'Skol Garrafa Vidro 600ml • Carnaval 2019'
    }, {
        id: 4,
        nome: 'Skol Garrafa Vidro 600ml • Rótulo Quadrado'
    }];

    $scope.embalagens_selecionadas = [];

    $scope.cbPckMarket = '';
    $scope.cbPckLanguage = '';
    $scope.mercados = [];
    $scope.languages = [];
    $scope.variacoes = [];

    $scope.mercados = [];

    $scope.myConfig = {
        create: false,
        valueField: 'id',
        labelField: 'title',
        searchField: ['title'],
        options: '',
        onInitialize: function (selectize) {},
        onDropdownOpen: function () {}
    };

    $scope.productsConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecione o produto',
        delimiter: ',',
        maxItems: 1,
        render: {
            option: function (data) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };

    $scope.packageNaturezaConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecione',
        maxItems: 1,
        render: {
            option: function (data) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };

    $scope.primaryPackageConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Indique as embalagens primárias contidas',
        maxItems: 1,
        render: {
            option: function (data) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };

    $scope.secondaryPackageConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Indique as embalagens secundárias contidas',
        maxItems: 1,
        render: {
            option: function (data) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };

    $scope.packageTypeConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecione o tipo de embalagem',
        maxItems: 1,
        render: {
            option: function (data) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };

    $scope.contentsConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecione o volume',
        maxItems: 1,
        render: {
            option: function (data, escape) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };

    $scope.campaignConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecione a campanha',
        maxItems: 1,
        options: {
            id: 0,
            nome: 'Cadastrar nova campanha'
        },
        render: {
            option: function (data, escape) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };

    $scope.marketConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecione os mercados',
        options: {
            id: 0,
            nome: 'Cadastrar novo mercado'
        },
        render: {
            option: function (data, escape) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };

    $scope.show_load = function () {
        $('.master').addClass('blurred');
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        $('.master').removeClass('blurred');
        $('.lds-css').hide();
    };

    $scope.carregar_categorias = function () {
        $scope.show_load();
        $.get(site_url + 'site/artwork/services/package/categories', function (data) {
                $scope.categorias = data;
                $scope.$apply();
                $scope.hide_load();

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
    };

    $scope.carregar_mercados = function () {
        $scope.show_load();
        $.get(site_url + 'site/artwork/services/package/mercados', function (data) {
                $scope.mercados = data;
                $scope.$apply();
                $scope.hide_load();

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
    };
    
    $scope.getMercadoById = function(id) {
        
        for(i = 0; i < $scope.mercados.length; i++) {
            if($scope.mercados[i].id == id) {
                return $scope.mercados[i];
            }
        }
    };

    $scope.carregar_familias = function () {
        if ($scope.cbNewCategory != '' && $scope.cbNewCategory != 0) {
            $scope.show_load();
            $.get(site_url + 'site/artwork/services/package/families', function (data) {
                    $scope.familias = data;
                    $scope.$apply();
                    $scope.hide_load();
                }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
        } else {
            $scope.familias = [];
        }
    };

    $scope.carregar_produtos = function () {
        if ($scope.cbPckProduct == 0) {
            $scope.show_load();
            $.get(site_url + 'site/artwork/services/package/products', function (data) {
                    $scope.produtos = data;
                    $scope.tamanhos = [];
                    $scope.cbPckProduct = '';
                    $scope.cbPckType = '';
                    $scope.cbPckContentSize = '';
                    $scope.$apply();
                    $scope.hide_load();
                }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
        } else {
            $scope.produtos = [];
            $scope.tamanhos = [];
            $scope.cbPckProduct = '';
            $scope.cbPckType = '';
            $scope.cbPckContentSize = '';
        }
    };

    $scope.carregar_tipos = function () {
        if ($scope.cbPckProduct != '') {
            $scope.show_load();

            $.get(site_url + 'site/artwork/services/package/types', function (data) {
                    
                    $scope.tipos = data;
                    $scope.cbPckContentSize = '';
                    $scope.$apply();
                    $scope.hide_load();

                }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
        } else {
            $scope.tamanhos = [];
            $scope.cbPckType = '';
            $scope.cbPckContentSize = '';
        }

        $scope.limpar_embalagens_selecionadas(1);
    };

    $scope.carregar_tamanhos = function () {
        if ($scope.cbPckType != '' && $scope.cbPckType != 0) {
            $scope.show_load();

            $.get(site_url + 'site/artwork/services/package/types/' + $scope.cbPckType + '/sizes', function (data) {
                    $scope.tamanhos = data;
                    $scope.cbPckContentSize = '';
                    $scope.$apply();
                    $scope.hide_load();

                }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
        } else {
            $scope.tamanhos = [];
            $scope.cbPckContentSize = '';
        }
    };

    $scope.carregar_campanhas = function () {
        if ($scope.cbPckProduct != '' && $scope.cbPckProduct != 0 && $scope.packageType === 'Sim') {
            $scope.show_load();
            $.get(site_url + 'site/artwork/services/package/promotions/' + $scope.cbPckProduct, function (data) {
                    $scope.campanhas = [{
                        'id': '0',
                        'nome': 'Cadastrar nova campanha'
                    }];
                    $scope.campanhas.push(data);
                    $scope.variacoes = [];
                    $scope.cbPckCampaign = '';
                    $scope.packageCampaignName = '';
                    $scope.$apply();
                    $scope.hide_load();
                }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
        } else {
            $scope.campanhas = [{
                        'id': '0',
                        'nome': 'Cadastrar nova campanha'
                    }];
            $scope.cbPckCampaign = '';
            $scope.packageCampaignName = '';
        }
    };
    
    $scope.carregar_variacoes = function ()
    {
        if ($scope.cbPckContentSize != '' && $scope.cbPckContentSize != 0)
        {
            $scope.show_load();
            
            var url = 'site/artwork/services/package/variacoes/' + $scope.cbPckProduct + '/' + $scope.cbPckContentSize;
            
            if ($scope.packageType == 'Sim') {
                url += '/' + $scope.cbPckCampaign;
            }
            
            $.get(site_url + url, function (data)
            {
                $scope.variacoes = data;

                $scope.$apply();
                $scope.hide_load();

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
        }
    };

    $scope.cadastrar_campanha = function () {
        if ($scope.cbPckCampaign === '') {
            $scope.packageCampaignName = '';
        }
    };

    $scope.cadastrar_variacao = function () {
        if ($scope.packageHasVariation === '' || $scope.packageHasVariation === 'Nao') {
            $scope.variacaoName = '';
        }
    };
    
    $scope.retornar_embalagem_mercado = function (id_variacao) {

        $scope.show_load();

        params = {
            _token: csrf_token,
            id_variacao: id_variacao
        };

        $.post(site_url + 'site/artwork/services/package/retornarEmbalagemMercado', params, function (data) {

            if (data.error) {
                myAlert(data.message, 'error');
            } else {
                
                $.confirm({
                    animationSpeed: 200,
                    animateFromElement: false,
                    draggable: false,
                    icon: 'fa fa-check',
                    title: 'Confirmar',
                    content: data.message,
                    closeIcon: true,
                    boxWidth: '910px',
                    scrollToPreviousElement: false,
                    useBootstrap: false,
                    closeIconClass: 'jconfirm-btn-close fa fa-times',
                    buttons: {
                        Entendi: {
                            keys: ['esc', 'enter', 'space'],
                            text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                            btnClass: 'btn-confirm my-alert-confirm',
                            action: function (Todos) {
                                location = data.url;
                            }
                        }
                    },
                    onOpen: function () {
                        $('body').addClass('jconfirm-overlay');
                        $('body').addClass('no-scroll');
                        $('.master').addClass('blurred');
                        $('.jconfirm').css('overflow', 'auto');
                        $scope.hide_load();
                    },
                    onClose: function () {
                        $('body').removeClass('jconfirm-overlay');
                        $('body').removeClass('no-scroll');
                        $('.master').removeClass('blurred');
                        $('.jconfirm').css('overflow', 'hidden');
                        $scope.hide_load();
                    }
                });
                
            }

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                });
    };

    $scope.enviarFormularioConfirm = function() {

        if($scope.cbPckNatureza === '3' && $scope.embalagens_selecionadas.length < 1 || $scope.cbPckNatureza === '4' && $scope.embalagens_selecionadas.length < 1) {
            $scope.hide_load();

            $.confirm({
                animationSpeed: 200,
                animateFromElement: false,
                draggable: false,
                icon: 'fa fa-question',
                title: 'Confirmar',
                content: 'Embalagens secundárias e terciárias devem possuir a indicação das embalagens que são contidas dentro delas. Tem certeza que deseja deixar em branco?',
                closeIcon: true,
                boxWidth: '910px',
                scrollToPreviousElement: false,
                useBootstrap: false,
                closeIconClass: 'jconfirm-btn-close fa fa-times',
                buttons: {
                    Confirmar: {
                        keys: ['enter', 'space'],
                        text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                        btnClass: 'btn-confirm',
                        action: function (Confirmar) {
                            $scope.enviarFormulario();
                        }
                    },
                    Cancelar: {
                        keys: ['esc'],
                        text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                        btnClass: 'btn-cancel'
                    }
                },
                onOpen: function () {
                    $('body').addClass('jconfirm-overlay');
                    $('body').addClass('no-scroll');
                    $('.master').addClass('blurred');
                    $('.jconfirm').css('overflow', 'auto');
                },
                onClose: function () {
                    $('body').removeClass('jconfirm-overlay');
                    $('body').removeClass('no-scroll');
                    $('.master').removeClass('blurred');
                    $('.jconfirm').css('overflow', 'hidden');
                }
            });

        } else {
            $scope.enviarFormulario();
        }

    };
    
    $scope.enviarFormulario = function (form) {
        
        $scope.show_load();
        
        params = {
            _token: csrf_token,
            cbPckProduct: $scope.cbPckProduct,
            cbPckType: $scope.cbPckType,
            cbPckContentSize: $scope.cbPckContentSize,
            packageType: $scope.packageType,
            cbPckCampaign: $scope.cbPckCampaign,
            packageCampaignName: $scope.packageCampaignName,
            cbPckMarket: $scope.cbPckMarket,
            variacaoName: $scope.variacaoName,
            packageHasVariation: $scope.packageHasVariation,
            cbPckNatureza: $scope.cbPckNatureza,
            pckEANCode: $scope.pckEANCode,
            txtPckEANCode: $scope.txtPckEANCode,
            embalagensRelacionadas: $scope.embalagens_selecionadas
        };
        
        $.post(site_url + 'site/artwork/services/package/create', params, function (data) {

            if (data.error) {
                
                myAlert(data.message, 'error');
                
            } else {
                
                if (data.success == 200){
                    
                    $.confirm({
                        animationSpeed: 200,
                        animateFromElement: false,
                        draggable: false,
                        icon: 'fa fa-check',
                        title: 'Sucesso',
                        content: data.message,
                        closeIcon: true,
                        boxWidth: '910px',
                        scrollToPreviousElement: false,
                        useBootstrap: false,
                        closeIconClass: 'jconfirm-btn-close fa fa-times',
                        buttons: {
                            Entendi: {
                                keys: ['esc', 'enter', 'space'],
                                text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                                btnClass: 'btn-confirm my-alert-confirm',
                                action: function (Todos) {
                                    location = data.url;
                                }
                            }
                        },
                        onOpen: function () {
                            $('body').addClass('jconfirm-overlay');
                            $('body').addClass('no-scroll');
                            $('.master').addClass('blurred');
                            $('.jconfirm').css('overflow', 'auto');
                            $scope.hide_load();
                        },
                        onClose: function () {
                            $('body').removeClass('jconfirm-overlay');
                            $('body').removeClass('no-scroll');
                            $('.master').removeClass('blurred');
                            $('.jconfirm').css('overflow', 'hidden');
                            $scope.hide_load();
                        }
                    });
                    
                } else {
                    
                    $.confirm({
                        animationSpeed: 200,
                        animateFromElement: false,
                        draggable: false,
                        icon: 'fa fa-question',
                        title: 'Confirmar',
                        content: data.message,
                        closeIcon: true,
                        boxWidth: '910px',
                        scrollToPreviousElement: false,
                        useBootstrap: false,
                        closeIconClass: 'jconfirm-btn-close fa fa-times',
                        buttons: {
                            Renovar: {
                                keys: ['enter', 'space'],
                                text: '<span><i class="icon fa fa-check"></i>Retornar para Mercado</span>',
                                btnClass: 'btn-confirm',
                                action: function (Todos) {
                                    
                                    $scope.retornar_embalagem_mercado(data.id_variacao);
                                    
                                }
                            },
                            Cancelar: {
                                keys: ['esc'],
                                text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                                btnClass: 'btn btn-cancel'
                            }
                        },
                        onOpen: function () {
                            $('body').addClass('jconfirm-overlay');
                            $('body').addClass('no-scroll');
                            $('.master').addClass('blurred');
                            $('.jconfirm').css('overflow', 'auto');
                            $scope.hide_load();
                        },
                        onClose: function () {
                            $('body').removeClass('jconfirm-overlay');
                            $('body').removeClass('no-scroll');
                            $('.master').removeClass('blurred');
                            $('.jconfirm').css('overflow', 'hidden');
                            $scope.hide_load();
                        }
                    });
                    
                }
                
            };

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                });
        
    };
    
    $scope.getById = function (id, array) {
        for (i = 0; i < array.length; i++) {
            if (array[i].id == id) {
                return array[i];
            }
        }
    };
    
    /** Adiciona embalagem primária/secundária selecionada a lista **/
    $scope.add_embalagem_selecionada = function() {
        
        if( $scope.getById($scope.cbPckEmbalagens, $scope.embalagens_selecionadas) ) {
            myAlert('Esta embalagem já foi adicionada.', 'warning');

            return false;
        } else if($scope.cbPckEmbalagens === '') {
            myAlert('Não foi selecionada uma embalagem.', 'warning');
            
            return false;
        } else {

            if($scope.cbPckEmbalagens !== '') {
                var i = $scope.getById($scope.cbPckEmbalagens, $scope.embalagens_por_natureza);
                
                $scope.embalagens_selecionadas.push(i)
                $scope.cbPckPrimarias = '';
            }
        
        }
    
    };

    /** Exclui embalagem primária/secundária selecionada a lista **/
    $scope.excluir_embalagem_selecionada = function(index) {
        $scope.embalagens_selecionadas.splice(index, 1);
    };

    /** Limpa a lista de embalagens primárias/secundárias selecionadas **/
    $scope.limpar_embalagens_selecionadas = function(limpa_natureza) {

        if (limpa_natureza)
        {
            $scope.cbPckNatureza = '';
        }

        $scope.embalagens_por_natureza = [];
        $scope.embalagens_selecionadas = [];
        $scope.cbPckEmbalagens = '';
        //$scope.cbPckSecundarias = '';
    };

    $scope.carregar_embalagens_por_natureza = function () {

        $scope.limpar_embalagens_selecionadas();

        id_natureza = $scope.cbPckNatureza;

        if (id_natureza == 2)
        {
            id_natureza_requisicao = '';
        }
        else if (id_natureza == 3)
        {
            id_natureza_requisicao = 2;
        }
        else if (id_natureza == 4)
        {
            id_natureza_requisicao = 3;
        }

        if (id_natureza_requisicao)
        {
            $scope.show_load();

            var url = site_url + 'site/artwork/services/package/getEmbalagensPorNatureza/' + $scope.cbPckProduct + '/' + id_natureza_requisicao;

            $.ajax({
                url: url,
                type: "get",
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.error) {
                        myAlert(data.message, 'error');
                    } else {
                        $scope.embalagens_por_natureza = data;
                        $scope.$apply();
                    }

                    $scope.hide_load();
                },
                error: function () {
                    myAlert('Ocorreu um erro.', 'error');
                }
            });
        }
    };
    
    $scope.carregar_mercados();
});