var site_url = '/';
var app = angular.module('app', [
    'dashboardResolveLoader',
    'ngAnimate',
    'ngRoute', 
    'selectize', 
    'relatorio1Mod', 
    'relatorio2Mod'
]);

app.run(function ($templateCache, $http) {
    $http.get('/view/dashboard/meus_indicadores.html', {
        cache: $templateCache,
        headers: {
            'Cache-Control': 'no-cache'
        }
    });

    $http.get('/view/dashboard/indicadores_gerais.html', {
        cache: $templateCache,
        headers: {
            'Cache-Control': 'no-cache'
        }
    });
    
    $http.get('/view/dashboard/relatorios.html', {
        cache: $templateCache,
        headers: {
            'Cache-Control': 'no-cache'
        }
    });
    
    $http.get('/view/dashboard/relatorios/relatorio_1.html', {
        cache: $templateCache,
        headers: {
            'Cache-Control': 'no-cache'
        }
    });

    $http.get('/view/dashboard/relatorios/relatorio_2.html', {
        cache: $templateCache,
        headers: {
            'Cache-Control': 'no-cache'
        }
    });
});

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/view/dashboard/meus_indicadores.html',
            resolve: {
                isOffset: function ($location, $rootScope) {
                    $rootScope.activetab = $location.path();
                    $rootScope.activenav = '';
                }
            }
        })
        .when('/indicadores-gerais', {
            templateUrl: '/view/dashboard/indicadores_gerais.html',
            resolve: {
                isOffset: function ($location, $rootScope) {
                    $rootScope.aba_indicadores_gerais = aba_indicadores_gerais;
                    $rootScope.activetab = $location.path();
                    $rootScope.activenav = '';

                    if(!$rootScope.aba_indicadores_gerais) {
                        $location.path('');    
                    }
                }
            }
        })
        .when('/relatorios', {
            templateUrl: '/view/dashboard/relatorios.html',
            resolve: {
                isOffset: function ($location, $rootScope) {
                    $rootScope.aba_relatorios = aba_relatorios;
                    $rootScope.activetab = $location.path();
                    $rootScope.activenav = '';
                    
                    if(!$rootScope.aba_relatorios) {
                        $location.path('');    
                    }
                }
            }
        })
        .when('/relatorios/tickets-encerrados-por-participante-e-funcao', {
            templateUrl: '/view/dashboard/relatorios/relatorio_1.html',
            controller: 'Relatorio1Controller',
            resolve: {
                isOffset: function ($location, $rootScope) {
                    $rootScope.aba_relatorios = aba_relatorios;
                    $rootScope.activenav = $location.path();

                    if(!$rootScope.aba_relatorios) {
                        $location.path('');    
                    }
                }
            }
        })
        .when('/relatorios/tickets-em-andamento-por-participante-e-funcao', {
            templateUrl: '/view/dashboard/relatorios/relatorio_2.html',
            controller: 'Relatorio2Controller',
            resolve: {
                isOffset: function ($location, $rootScope) {
                    $rootScope.aba_relatorios = aba_relatorios;
                    $rootScope.activenav = $location.path();
                    
                    if(!$rootScope.aba_relatorios) {
                        $location.path('');    
                    }
                }
            }
        })
        .otherwise({
            redirectTo: '/meus-indicadores'
        });
});

app.controller('DashboardController', function($scope, $rootScope, $location, $interval, $http) {
    $scope.tab = 1;
    $rootScope.loading = false;

    $scope.tooltipsTicketsEmAndamento = [
        { nome: 'Tickets', texto: 'Total de tickets em andamento no sistema.' },
        { nome: 'No prazo', texto: 'Tickets que estão dentro dos prazos de Aprovação e de Encerramento.' },
        { nome: 'Atrasados', texto: 'Tickets que passaram dos prazos de Aprovação ou de Encerramento.' },
        { nome: 'Pausados', texto: 'Tickets que estão Pausados.' },
        { nome: 'Novos Itens', texto: 'Tickets abertos para novos Itens de Embalagem dentro do mês.' },
        { nome: 'Alteração', texto: 'Tickets abertos para alterações de Itens de Embalagem dentro do mês.' },
        { nome: 'Cancelados', texto: 'Tickets Cancelados dentro do mês.' }
    ];

    $scope.tooltipsTicketsEncerradosPorCiclo = [
        { nome: 'Tickets', texto: 'Total de tickets Encerrados dentro do mês.' },
        { nome: 'Ciclo 1', texto: 'Tickets Encerrados no Ciclo 1 dentro do mês.' },
        { nome: 'Ciclo 2', texto: 'Tickets Encerrados no Ciclo 2 dentro do mês.' },
        { nome: 'Ciclo 3', texto: 'Tickets Encerrados no Ciclo 3 dentro do mês.' },
        { nome: 'Ciclo 4', texto: 'Tickets Encerrados no Ciclo 4 dentro do mês.' },
        { nome: 'Ciclo 5+', texto: 'Tickets Encerrados no Ciclo 5 ou mais dentro do mês.' }
    ];

    $scope.tooltipsEmbalagens = [
        { nome: 'Embalagens', texto: 'Total de Embalagens cadastradas no sistema.' },
        { nome: 'Itens de embalagem', texto: 'Total de Itens de Embalagem cadastrados no sistema.' },
        { nome: 'Itens de alteração', texto: 'Itens de Embalagem em alteração com ticket em andamento.' },
        { nome: 'Itens descontinuados', texto: 'Total de Itens de Embalagem descontinuados.' }
    ];

    $scope.tooltipsUsuarios = [
        { nome: 'Usuários', texto: 'Total de Usuários cadastrados no sistema.' },
        { nome: 'Gerentes', texto: 'Total de Gerentes (Embalagens, Fotos, Mídias, Usuários, Fornecedores) cadastrados no sistema.' },
        { nome: 'Administradores', texto: 'Total de Administradores cadastrados no sistema.' },
        { nome: 'Inativos', texto: 'Total de Usuários Inativos cadastrados no sistema.' }
    ];
    
    $scope.tooltipsTicketsEmAndamentoUsuario = [
        { nome: 'Tickets', texto: 'Total de tickets em andamento que eu participo.' },
        { nome: 'Sou Líder',  texto: 'Tickets em andamento nos quais sou Líder.' },
        { nome: 'No Prazo', texto: 'Tickets que estão dentro dos prazos de Aprovação e de Encerramento que eu participo.' },
        { nome: 'Atrasados', texto: 'Tickets que passaram dos prazos de Aprovação ou de Encerramento que eu participo.' },
        { nome: 'Pausados', texto: 'Tickets que estão Pausados que eu participo.' }
    ];

    $scope.tooltipsMinhasFuncoesTicketsAndamentoAtrasado = [
        { nome: 'Líder', texto: 'Tickets em andamento atrasados (que passaram dos prazos de Aprovação ou de Encerramento)  nos quais sou Líder.' },
        { nome: 'Edição e Finalização', texto: 'Tickets em andamento atrasados nos quais sou Edição e Finalização.' },
        { nome: 'Revisão', texto: 'Tickets em andamento atrasados nos quais sou Revisão.' },
        { nome: 'Aprovações', texto: 'Tickets em andamento atrasados nos quais sou Primeira Aprovação ou Demais Aprovações.' },
        { nome: 'Envio da Arte', texto: 'Tickets em andamento atrasados nos quais sou Envio da Arte.' },
        { nome: 'PDF do Fornecedor', texto: 'Tickets em andamento atrasados nos quais sou PDF do Fornecedor.' }
    ];

    $scope.tooltipsEtapaAtrasoTicketsEncerradosUsuario = [
        { nome: 'Atrasos', texto: '' },
        { nome: 'Edição', texto: '' },
        { nome: 'Revisão', texto: '' },
        { nome: 'Aprovações', texto: '' },
        { nome: 'Finalização', texto: '' },
        { nome: 'Envio da Arte', texto: '' },
        { nome: 'PDF do Fornecedor', texto: '' }
    ];

    $scope.tooltipsTicketsEncerradosPorCicloUsuario = [
        { nome: 'Tickets', texto: 'Total de tickets Encerrados que eu participo dentro do mês.' },
        { nome: 'Ciclo 1', texto: 'Tickets Encerrados no Ciclo 1 que eu participo dentro do mês.' },
        { nome: 'Ciclo 2', texto: 'Tickets Encerrados no Ciclo 2 que eu participo dentro do mês.' },
        { nome: 'Ciclo 3', texto: 'Tickets Encerrados no Ciclo 3 que eu participo dentro do mês.' },
        { nome: 'Ciclo 4', texto: 'Tickets Encerrados no Ciclo 4 que eu participo dentro do mês.' },
        { nome: 'Ciclo 5+', texto: 'Tickets Encerrados no Ciclo 5 ou mais que eu participo dentro do mês.' }
    ];
    
    $scope.carregar_dashboard = function () {

        var url = site_url + 'site/dashboard/services/getDashboard';

        $rootScope.loading = true;

        $http({
            method: 'GET',
            url: url
        }).then(function successCallback (response) {
            console.log(response);
            $scope.dashboard = response.data;
            $rootScope.loading = false;
        }, function errorCallback (response) {
            myAlert('Ocorreu um erro.', 'error');
            $rootScope.loading = false;
        });
    };

    $scope.carregar_dashboard();

    $scope.Timer = $interval(function () {
        //$scope.carregar_dashboard();
    }, 5000);

    $scope.atualizaDashboard = function () {
        var url = site_url + 'site/dashboard/services/atualizaDashboard';

        $http({
            method: 'GET',
            url: url
        }).then(function successCallback (response) {
            console.log(response);            
            $scope.carregar_dashboard();
        }, function errorCallback (response) {
            myAlert('Ocorreu um erro.', 'error');
        });
    };
});