var relatorio2 = angular.module("relatorio2Mod", ['cp.ngConfirm', 'selectize']);

relatorio2.controller('Relatorio2Controller', function($scope, $rootScope, $ngConfirm, $window, $timeout, $http) {
    $scope.cbFuncoes = '';
    
    $scope.funcoesConfig = {
        create: false,
        persist: true,
        valueField: 'id',
        labelField: 'nome',
        delimiter: '|',
        maxItems: 1,
        onInitialize: function (selectize) {},
        onDropdownOpen: function () {
            this.clear();
            this.focus();
        }
    };

    $scope.funcoes = [
        {id: 5, nome: 'Edição e Finalização'},
        {id: 3, nome: 'Revisão'},
        {id: 2, nome: 'Primeira Aprovação'},
        {id: 1, nome: 'Demais Aprovações'},
        {id: 6, nome: 'Envio da Arte'},
        {id: 7, nome: 'PDF do Fornecedor'},
        {id: 4, nome: 'Participante'}
    ];

    $scope.funcoes_selecionadas = [];

    $scope.show_load = function () {
        $('.master').addClass('blurred');
        $('.lds-css').show();
        
        $scope.exibe_loading = true;
    };

    $scope.hide_load = function () {
        $('.master').removeClass('blurred');
        $('.lds-css').hide();
        
        $scope.exibe_loading = false;
    };

    $scope.getById = function (id, array) {
        for (i = 0; i < array.length; i++) {
            if (array[i].id == id) {
                return array[i];
            }
        }
    };

    $scope.gerar_relatorio = function() {

        params = {
            _token: csrf_token,
            funcoes_selecionadas: $scope.funcoes_selecionadas,
            tipo: 'ticketsEmAndamentoPorParticipanteFuncao'
        };

        var url = site_url + 'site/dashboard/services/getRelatorio';

        /*$.post(url, params, function (data) {

            if (data.error) {
                myAlert(data.message, 'error');
            } else {
                
                myAlert(data.message, 'success');

                window.open(data.urlDownload, "_blank");

            }

        }, 'json')
        .fail(function (data) {
            myAlert('Ocorreu um erro.', 'error');
        });*/

        $scope.show_load();

        $http.post(url, params).then(function (data) {
            console.log(data);

            if (data.data.error) {
                $scope.hide_load();
                myAlert(data.data.message, 'error');
            } else {
                $scope.hide_load();
                myAlert(data.data.message, 'success');
                window.open(data.data.urlDownload, "_blank");
            }
        }, function(data) {
            $scope.hide_load();
            myAlert('Ocorreu um erro.', 'error');
        });
    };

    $scope.add_funcao_selecionada = function() {
        if($scope.getById($scope.cbFuncoes, $scope.funcoes_selecionadas)) {
            angular.element('.col-add-funcao .selectize-input').addClass('has-error');
            
            myAlert('Esta função já foi adicionada.', 'warning');

            return false;
        } else if($scope.cbFuncoes === '' || $scope.cbFuncoes === undefined) {
            angular.element('.col-add-funcao .selectize-input').addClass('has-error');

            myAlert('Não foi selecionada uma função.', 'warning');
            
            return false;
        } else {
            if($scope.cbFuncoes !== '') {
                var i = $scope.getById($scope.cbFuncoes, $scope.funcoes);
                    
                $scope.funcoes_selecionadas.push(i);
                $scope.cbFuncoes = '';
            
                angular.element('.col-add-funcao .selectize-input').removeClass('has-error');
            }
        }
    };

    $scope.excluir_funcao_selecionada = function(index) {
        $scope.funcoes_selecionadas.splice(index, 1);
    };
});