var site_url = '/';
var app = angular.module('app', []);

app.controller('PackageCreateCtrl', function ($scope)
{
    $scope.packageType = '';
    //$scope.packageType = 'Regular';

    $scope.packageIsNewProd = '';

    $scope.familias = [];
    //$scope.familias = [{"id": 17, "nome": "Adriática"}, {"id": 1, "nome": "Antarctica"}, {"id": 21, "nome": "Antarctica Original"}, {"id": 39, "nome": "Beck's"}, {"id": 2, "nome": "Bohemia"}, {"id": 3, "nome": "Brahma"}, {"id": 44, "nome": "Bucanero Fuerte"}, {"id": 4, "nome": "Budweiser"}, {"id": 18, "nome": "Caracu"}, {"id": 30, "nome": "Cervejaria Bohemia"}, {"id": 32, "nome": "Cervejaria Colorado"}, {"id": 26, "nome": "Corona Extra"}, {"id": 33, "nome": "Franziskaner"}, {"id": 34, "nome": "Goose Island"}, {"id": 40, "nome": "Hertog Jan"}, {"id": 35, "nome": "Hoegaarden"}, {"id": 36, "nome": "Leffe"}, {"id": 41, "nome": "Norteña"}, {"id": 37, "nome": "Patagonia"}, {"id": 19, "nome": "Polar Export"}, {"id": 42, "nome": "Quilmes"}, {"id": 23, "nome": "Serramalte"}, {"id": 11, "nome": "Skol"}, {"id": 13, "nome": "Stella Artois"}, {"id": 31, "nome": "Três Fidalgas"}, {"id": 24, "nome": "Wäls"}];

    $scope.cbPckFamily = '';
    //$scope.cbPckFamily = '33';

    $scope.produtos = [];
    //$scope.produtos = [{"id": 130, "nome": "Franziskaner Dunkel"}, {"id": 131, "nome": "Franziskaner Hell"}, {"id": 132, "nome": "Franziskaner Kristall-Klar"}, {"id": 149, "nome": "Franziskaner Naturtrüb"}];

    $scope.cbPckProduct = '';
    //$scope.cbPckProduct = '130';

    $scope.tipos = [];
    //$scope.tipos = [{"id": 13, "nome": "Acessório"}, {"id": 9, "nome": "Allubottle"}, {"id": 10, "nome": "Caixa"}, {"id": 16, "nome": "Cartucho"}, {"id": 3, "nome": "Garrafa"}, {"id": 26, "nome": "Garrafeira"}, {"id": 2, "nome": "Lata"}, {"id": 7, "nome": "Pack Garrafa"}, {"id": 4, "nome": "Pack Lata"}, {"id": 5, "nome": "PET"}, {"id": 15, "nome": "Pillow Bag"}, {"id": 8, "nome": "Shrink Garrafa"}, {"id": 6, "nome": "Shrink Lata"}, {"id": 12, "nome": "Sleeve"}, {"id": 14, "nome": "Tetra Pak"}];

    $scope.tamanhos = [];
    //$scope.tamanhos = [{"id": 67, "nome": "250ml"}, {"id": 27, "nome": "269ml"}, {"id": 20, "nome": "310ml"}, {"id": 59, "nome": "340ml"}, {"id": 2, "nome": "350ml"}, {"id": 3, "nome": "473ml"}, {"id": 4, "nome": "550ml"}];

    $scope.categorias = [];
    //$scope.categorias = [{"id": 6, "nome": "Água"}, {"id": 5, "nome": "Bebida Mista"}, {"id": 10, "nome": "Cereal"}, {"id": 2, "nome": "Cerveja"}, {"id": 8, "nome": "Chá"}, {"id": 12, "nome": "Chopp"}, {"id": 3, "nome": "Energético"}, {"id": 11, "nome": "Garrafeira"}, {"id": 4, "nome": "Isotônico"}, {"id": 1, "nome": "Refrigerante"}, {"id": 9, "nome": "Suco"}];

    $scope.variacoes = [];

    $scope.cbNewCategory = '';
    //$scope.cbNewCategory = '2';

    $scope.cbPckType = '';
    //$scope.cbPckType = '2';

    $scope.campanhas = [];
    $scope.cbPckCampaign = '';

    $scope.cbPckContentSize = '';
    //$scope.cbPckContentSize = '67';

    $scope.variacaoName = '';

    $scope.show_load = function ()
    {
        //$('.master').addClass('blurred');
        $('.lds-css').show();
    };

    $scope.hide_load = function ()
    {
        //$('.master').removeClass('blurred');
        $('.lds-css').hide();
    };

    $scope.carregar_categorias = function ()
    {
        $scope.show_load();

        $.get(site_url + 'site/artwork/services/package/categories', function (data)
        {
            $scope.categorias = data;
            //$scope.familias = [];
            //$scope.produtos = [];
            //$scope.cbNewCategory = '';
            //$scope.cbPckFamily = '';
            //$scope.cbPckProduct = '';

            $scope.$apply();
            $scope.hide_load();

        }, 'json')
        .fail(function (data) {
            myAlert('Ocorreu um erro.', 'error');
            $scope.hide_load();
        });
    };

    $scope.carregar_familias = function ()
    {
        if ($scope.cbNewCategory != '' && $scope.cbNewCategory != 0)
        {
            $scope.show_load();

            $.get(site_url + 'site/artwork/services/package/families', function (data)
            {
                $scope.familias = data;
                $scope.produtos = [];
                $scope.tamanhos = [];
                $scope.cbPckFamily = '';
                $scope.cbPckProduct = '';
                $scope.cbPckType = '';
                $scope.cbPckContentSize = '';

                $scope.$apply();
                $scope.hide_load();

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
        } else {
            $scope.familias = [];
            $scope.produtos = [];
            $scope.tamanhos = [];
            $scope.cbPckFamily = '';
            $scope.cbPckProduct = '';
            $scope.cbPckType = '';
            $scope.cbPckContentSize = '';
        }
    };

    $scope.carregar_produtos = function ()
    {
        if ($scope.cbPckFamily != '' && $scope.cbPckFamily != 0)
        {
            $scope.show_load();

            $.get(site_url + 'site/artwork/services/package/products/' + $scope.cbPckFamily, function (data)
            {
                $scope.produtos = data;
                $scope.tamanhos = [];
                $scope.cbPckProduct = '';
                $scope.cbPckType = '';
                $scope.cbPckContentSize = '';

                $scope.$apply();
                $scope.hide_load();

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
        } else {
            $scope.produtos = [];
            $scope.tamanhos = [];
            $scope.cbPckProduct = '';
            $scope.cbPckType = '';
            $scope.cbPckContentSize = '';
        }
    };

    $scope.carregar_tipos = function ()
    {
        if ($scope.cbPckProduct != '')
        {
            $.get(site_url + 'site/artwork/services/package/types', function (data)
            {
                $scope.show_load();

                $scope.tipos = data;
                $scope.tamanhos = [];
                $scope.cbPckType = '';
                $scope.cbPckContentSize = '';

                $scope.$apply();
                $scope.hide_load();

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
        } else {
            $scope.tamanhos = [];
            $scope.cbPckType = '';
            $scope.cbPckContentSize = '';
        }
    };

    $scope.carregar_campanhas = function ()
    {
        if ($scope.cbPckProduct != '' && $scope.cbPckProduct != 0)
        {
            $scope.show_load();

            $.get(site_url + 'site/artwork/services/package/promotions/' + $scope.cbPckProduct, function (data)
            {
                $scope.campanhas = data;
                $scope.tamanhos = [];
                $scope.cbPckType = '';
                $scope.cbPckContentSize = '';

                $scope.$apply();
                $scope.hide_load();

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
        } else {
            $scope.campanhas = [];
            $scope.tamanhos = [];
            $scope.cbPckType = '';
            $scope.cbPckContentSize = '';
        }
    };

    $scope.carregar_tamanhos = function ()
    {
        if ($scope.cbPckType != '' && $scope.cbPckType != 0)
        {
            $scope.show_load();

            $.get(site_url + 'site/artwork/services/package/types/' + $scope.cbPckType + '/sizes', function (data)
            {
                $scope.tamanhos = data;
                $scope.cbPckContentSize = '';

                $scope.$apply();
                $scope.hide_load();

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
        } else {
            $scope.tamanhos = [];
            $scope.cbPckContentSize = '';
        }
    };

    $scope.carregar_variacoes = function ()
    {
        if ($scope.cbPckContentSize != '' && $scope.cbPckContentSize != 0)
        {
            $scope.show_load();
            
            var url = 'site/artwork/services/package/variacoes/' + $scope.cbPckProduct + '/' + $scope.cbPckContentSize;
            
            if ($scope.cbPckCampaign) {
                url += '/' + $scope.cbPckCampaign;
            }
            
            $.get(site_url + url, function (data)
            {
                $scope.variacoes = data;

                $scope.$apply();
                $scope.hide_load();

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
        }
    };

});
