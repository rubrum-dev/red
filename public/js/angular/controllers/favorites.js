var app = angular.module('app', []);

app.controller('favoritesCtrl', function ($scope, $http) {
    var btn = $(this).attr('id'),
        family = $('#' + btn).data('family-id'),
        product = $('#' + btn).data('prod-id');

    window.path = window.location.origin;
    
    $scope.deleteFavoriteConfirm = function (message) {
        $.confirm({
            title: '',
            content: '<div class="custom-alert-heading confirmation">' +
                '   <span class="icon icon-question"></span>' +
                '   <h4>Confirmar</h4>' +
                '</div>' +
                '<div class="custom-alert-contents">' +
                '   <p>' + message + '</p>' +
                '</div>',
            closeIcon: true,
            closeIconClass: 'custom-alert-close',
            buttons: {
                Confirmar: {
                    btnClass: 'custom-btn-confirm-yes',
                    action: function (Confirmar) {
                        $.ajax({
                            type: 'get',
                            url: window.path + '/site/' + family + '/produtos/' + product + '/favorite',
                            context: document.body
                        }).done(function (data) {
                            $scope.deleteFavoriteSuccess();                        
                            location.reload();
                        });
                    }
                },
                Cancelar: {
                    btnClass: 'custom-btn-confirm-cancel',
                }
            }
        });
    };

    $scope.deleteFavoriteSuccess = function(message) {
        
        $.alert({
            title: '',
            content: '<div class="custom-alert-heading success">' +
                '   <span class="icon icon-success"></span>' +
                '   <h4>Sucesso</h4>' +
                '</div>' +
                '<div class="custom-alert-contents">' +
                '   <p>' + message + '</p>' +
                '</div>',
            closeIcon: true,
            closeIconClass: 'custom-alert-close',
            buttons: {
                Entendi: {
                    btnClass: 'custom-btn-primary',
                    action: function (Entendi) {
                        
                    }
                }
            }
        });
    };

    $scope.deleteFavoriteConfirm('Deseja excluir esse favorito?');
});