var site_url = '/';
var app = angular.module('app', ['ngSanitize', 'selectize']);

app.controller('TicketEditCtrl', function ($scope) {
    $scope.myConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        //delimiter: '|',
        searchField: ['nome'],
        placeholder: 'Selecionar membro',
        maxItems: 1,
        render: { 
            option: function(data, escape){
                //receives the selectize object as an argument
                //console.log(data);
                //console.log(escape);
                //return escape(data.nome);
                if(data.ignore === true) {
                    //console.log('caiu');
                    return '<div style="display:none"></div>';
                } else {
                    return '<div class="item">'+ data.nome +'</div>';
                }
            }
        },
    };
    
    $scope.userLoginConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        //delimiter: '|',
        searchField: ['nome'],
        placeholder: 'Selecionar membro',
        maxItems: 1,
        render: { 
            option: function(data, escape){
                //receives the selectize object as an argument
                //console.log(data);
                //console.log(escape);
                //return escape(data.nome);
                return '<div class="item">'+ data.nome +'</div>';
            }
        }
    };
    
    $scope.solicitacoes = [
        //{'id': '1', 'descricao': 'Teste 1'},
        //{'id': '2', 'descricao': 'Teste 2'}
    ];
    $scope.tipos = [];
    $scope.ticket = {};

    $scope.participantes = [
        //{'id_usuario': id_usuario, 'id_perfil': '', 'recebe_email': ''},
        //{'id_usuario': '2', 'id_perfil': '3', 'recebe_email': '1'},
        //{'id_usuario': '3', 'id_perfil': '1', 'recebe_email': '1'},
    ];

    $scope.membros = {};
    $scope.cbMembershipProfile = [];
    $scope.cbMembershipDelete = [];
    $scope.cbMembershipEquipeDelete = [];
    $scope.prosseguir = '';
    $scope.itens = {};

    $scope.show_load = function () {
        //$('.master').addClass('blurred');
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        //$('.master').removeClass('blurred');
        $('.lds-css').hide();
    };
    
    $scope.verificar_participante = function()
    {
        var n = 0;
        
        for(i = 0; i < $scope.participantes.length; i++) {
            
            if($scope.participantes[i].id_usuario == $scope.add_participante && ($scope.participantes[i].id_perfil == $scope.add_perfil || $scope.cbMembershipProfile[i] ==  $scope.add_perfil)) {
                
                n++;
                
            }
            
        }
        
        //console.log(n);
        
        if (n > 0) {
            
            return true;
            
        } else {
            
            return false;
            
        }
        
    };

    $scope.adicionar_membro = function () {
        
        if ($scope.verificar_participante())
        {
            
            $('.input-fields-group:last .select-control .selectize-input').addClass('input-error');
            $('.input-fields-group:last .select-control select').addClass('input-error');
            
            myAlert('O Perfil de Participação selecionado já foi atribuído à este Participante neste ticket.', 'warning');
            
            return false;
        }
        
        if ($scope.add_participante && $scope.add_perfil) {
            var template = {
                'id_usuario': $scope.add_participante,
                'id_perfil': $scope.add_perfil,
                'recebe_email': $scope.add_email,
                'equipe_membros': []
            };

            $scope.participantes.push(template);

            $scope.add_participante = '';
            $scope.add_perfil = '';
            $scope.add_email = '';
            //$scope.carregar_membros();

            $('.input-fields-group:last .select-control .selectize-input').removeClass('input-error');
            $('.input-fields-group:last .select-control select').removeClass('input-error');
        } else {
            $('.input-fields-group:last .select-control .selectize-input').addClass('input-error');
            $('.input-fields-group:last .select-control select').addClass('input-error');
            
            myAlert('Selecione o Participante e seu respectivo Perfil de Participação neste ticket.', 'warning');
        }
    };

    $scope.adicionar_membro_equipe = function (k)
    {
        //console.log($scope.participantes[k]);

        if ($scope.participantes[k].add_participante && $scope.cbMembershipProfile[k])
        {
            var template = {'id_usuario': $scope.participantes[k].add_participante, 'recebe_email': ''};

            $scope.participantes[k].equipe_membros.push(template);

            $scope.participantes[k].add_participante = '';
            $scope.participantes[k].adiciona_equipe = 0;
            $scope.carregar_membros();
        
            $('.team-member-group .select-control .selectize-input').removeClass('input-error');
            $('.team-member-group .select-control select').removeClass('input-error');
        } else {
            
            $('.team-member-group .select-control .selectize-input').addClass('input-error');
            $('.team-member-group .select-control select').addClass('input-error');
        
            myAlert('Selecione o Participante e seu respectivo Perfil de Participação neste ticket.', 'warning');
            
        }
    };

    $scope.remover_membro = function (k) {

        if ($scope.participantes[k].id){
            $scope.cbMembershipDelete.push($scope.participantes[k].id);
        }

        $scope.cbMembershipProfile.splice(parseInt(k), 1);

        $scope.participantes.splice(parseInt(k), 1);

        $scope.carregar_membros();
    };

    $scope.remover_membro_equipe = function (k, e)
    {
        //$scope.cbMembershipProfile[k].equipe_membros.splice(parseInt(e), 1);
        if ($scope.participantes[k].equipe_membros[e].id){
            $scope.cbMembershipEquipeDelete.push($scope.participantes[k].equipe_membros[e].id);
        }

        $scope.participantes[k].equipe_membros.splice(parseInt(e), 1);

        $scope.carregar_membros();
    };

    $scope.carregar_membros = function () {
        $scope.show_load();

        params = {
            _token: csrf_token
        };

        $.post(site_url + 'site/artwork/services/package/members', params, function (data) {
            $scope.membros = data;

            $scope.$apply();
            $scope.hide_load();
            
        }, 'json');
    };

    $scope.carregar_participantes = function () {
        $scope.show_load();
        
        $.get(site_url + 'site/artwork/services/package/ticket_members/' + id_ticket, function (data) {
            if (data.length) {
                $scope.participantes = data;

                angular.forEach($scope.participantes, function (participante, key) {
                    $scope.cbMembershipProfile.push('' + participante.id_perfil + '');
                });

                $scope.$apply();

                $scope.carregar_membros();

                $scope.hide_load();
            }
        }, 'json');

    };

    $scope.carregar_ticket = function () {
        $scope.show_load();

        $.get(site_url + 'site/artwork/services/package/ticket/' + id_ticket, function (data) {
                $scope.ticket = data;

                $scope.solicitacoes = (data.ciclo_atual.solicitacoes) ? data.ciclo_atual.solicitacoes : [];

                $scope.$apply();
                $scope.hide_load();

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro!');
                $scope.hide_load();
            });
    };

    $scope.carregar_tipos = function () {
        $.get(site_url + 'site/artwork/services/package/request/types', function (data) {
            $scope.tipos = data;

            $scope.$apply();

        }, 'json');
    };

    $scope.adicionar = function () {
        if ($scope.add_tipo && $scope.add_descricao) {
            var template = {
                'id_tipo': $scope.add_tipo,
                'descricao': $scope.add_descricao,
                'descricao_texto': $scope.add_descricao
            };

            console.log(template);

            $scope.solicitacoes.push(template);

            $scope.add_tipo = '';
            $scope.add_descricao = '';
        
            $('.request-type .input-fields-group:last select').css('border', '1px solid #686868');
            $('.request-type .input-fields-group:last textarea').css('border', '1px solid #686868');
        } else {
            $('.request-type .input-fields-group:last select').css('border', '2px dashed #EA4334');
            $('.request-type .input-fields-group:last textarea').css('border', '2px dashed #EA4334');
            
            myAlert('Selecione o tipo de alteração e infome a descrição específica.', 'warning');
        }
    };

    $scope.remover = function (k) {
        $scope.solicitacoes.splice(parseInt(k), 1);

        //console.log(parseInt(k));
        //console.log($scope.participantes);
    };

    $scope.carregar_tipos();
    $scope.carregar_ticket();
    $scope.carregar_membros();
    $scope.carregar_participantes();
    //$scope.carregar_membros();

});