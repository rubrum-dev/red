var site_url = '/';
var app = angular.module('app', []);

app.controller('adminItensEmbalagemCtrl', function($scope) {
    $scope.show_load = function () {
        //$('.master').addClass('blurred');
        $('.lds-css').show();
    };
    $scope.hide_load = function () {
        //$('.master').removeClass('blurred');
        $('.lds-css').hide();
    };

    // Função de excluir item de embalagem
    $scope.excluir_item_embalagem = function (id) {
        // Chama o modal de confirmação perguntando se o usuário deseja excluir 
        $scope.deletePackageItemModal = $.confirm({
            icon: 'fa fa-question',
            title: 'Confirmar',
            content: 'Deseja excluir esse item de embalagem?',
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            useBootstrap: false,
            boxWidth: '910px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    // Caso sim, Dispara serviço de excluir item de embalagem
                    action: function (Confirmar) {
                        $scope.show_load();
        
                        // Serviço de excluir item de embalagem
                        $.get(site_url + 'admin/itens-embalagem/excluir/' + id, function (data) {
                            if (data.error) {
                                myAlert(data.message, 'error');
                                $scope.hide_load();
                            } else {
                                if (data.success == 201){
                                    myAlert(data.message, 'sucess');
                                    $scope.hide_load();
                                    return;
                                }

                                $.confirm({
                                    animationSpeed: 200,
                                    animateFromElement: false,
                                    draggable: false,
                                    icon: 'fa fa-check',
                                    title: 'Sucesso',
                                    content: 'Item de embalagem excluído com sucesso.',
                                    closeIcon: true,
                                    boxWidth: '910px',
                                    scrollToPreviousElement: false,
                                    useBootstrap: false,
                                    closeIconClass: 'jconfirm-btn-close fa fa-times',
                                    buttons: {
                                        Entendi: {
                                            keys: ['esc', 'enter', 'space'],
                                            text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                                            btnClass: 'btn-confirm my-alert-confirm',
                                            action: function (Todos) {
                                                $('.lds-css').show(200, function () {
                                                    location = data.url;
                                                });
                                            }
                                        }
                                    },
                                    onOpen: function () {
                                        //$('body').addClass('jconfirm-overlay');
                                        $('body').addClass('no-scroll');
                                        //$('.master').addClass('blurred');
                                        $('.jconfirm').css('overflow', 'auto');
                                        
                                        $scope.hide_load();
                                    },
                                    onClose: function () {
                                        //$('body').removeClass('jconfirm-overlay');
                                        $('body').removeClass('no-scroll');
                                        //$('.master').removeClass('blurred');
                                        $('.jconfirm').css('overflow', 'hidden');
                                        
                                        $scope.hide_load();
                                    }
                                });
                            }
                        }, 'json')
                        .fail(function (data) {
                            myAlert('Ocorreu um erro.', 'error');
                            $scope.hide_load();
                        });
                    }
                },
                // Caso não, não executa nenhuma função
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {}
                }
            },
            onOpenBefore: function () {
                //$('body').addClass('jconfirm-overlay');
                $('body').addClass('no-scroll');
                //$('.master').addClass('blurred');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                //$('body').removeClass('jconfirm-overlay');
                $('body').removeClass('no-scroll');
                //$('.master').removeClass('blurred');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };
})