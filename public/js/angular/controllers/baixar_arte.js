var site_url = '/';

var app = angular.module('app', ['ngFileUpload']);

app.filter('bytes', function() {
	return function(bytes, precision) {
		if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
		
        if (typeof precision === 'undefined') precision = 1;
		
        var units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB'],
			number = Math.floor(Math.log(bytes) / Math.log(1024));
		
            return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  ' ' + units[number];
	}
});

app.controller('BaixarArteCtrl', function ($scope, Upload, $http, $interval, $timeout) {
    $scope.loaded = false;
    $scope.interval_tickets = false;
    $scope.id_fornecedor = id_fornecedor;
    $scope.fornecedorPdfs = [];

    $scope.show_load = function () {
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        $('.lds-css').hide();
    };

    $scope.artwItemSharing = function (id_item) {
        $scope.itemSelecionado = [];
        
        $http.get(site_url + 'site/artwork/services/package/getItem/' + id_item + '/1').success(function(data){
            $scope.itemSelecionado = data;
        });
        
    };

    $scope.carregar_pdfs = function () {
        if ($scope.id_fornecedor) {
            $scope.loaded = false;
            $scope.show_load();
            
            $http.get(site_url + 'download/getFornecedorPdf?token=' + token).success(function (data){
                $scope.fornecedorPdfs = data['fornecedorPdfs'];
                //console.log($scope.fornecedorPdfs);
                $scope.loaded = true;
                $scope.hide_load();
            });
        } else {
            $interval.cancel($scope.Timer);
            $scope.hide_load();
        }
    };

    $scope.carregar_pdfs();
    
    $scope.upload_arquivo = function($event) {
        var file = document.getElementById('pdfFileUpload').files[0];
        
        $scope.pdf = file;
        
        if (!$scope.pdf) {
            $('.col-files-attach').find('.files-control-fake').addClass('has-error');
            $('.col-select-supplier').find('select').addClass('has-error');
            
            myAlert('Anexe um arquivo PDF.', 'warning');
            
            return false;
        } 
        
        if ($scope.pdf.size > 52428800) {
            $('.col-files-attach').find('.files-control-fake').addClass('has-error');
            
            myAlert('O tamanho máximo permitido para um arquivo é 50 MB.', 'warning');
            
            return false;
        }

        $('.col-files-attach').find('.files-control-fake').removeClass('has-error');
        $('.col-select-supplier').find('select').removeClass('has-error');        
        
        params = {
            _token: csrf_token,
            token: token,
            pdf: file
        };

        $scope.pdf.progress = 0;

        $scope.pdf.upload = Upload.upload({
            url: site_url + 'download/postUploadFornecedorPdf',
            data: params
        });

        $scope.pdf.upload.then(function (response) {
            console.log(params);
            if (response.data.error)
            {
                myAlert(response.data.message);
                $scope.pdf = '';
                $scope.limparCamposUpload();
            } else {
                $scope.pdf = '';
                $scope.limparCamposUpload();
                $scope.carregar_pdfs();
            }
        }, function (response) {
            if (response.status > 0) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.pdf.progress = 0;
            }
        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            $scope.pdf.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
    };

    $scope.excluir = function()  {
        $.confirm({
            icon: 'icon fa fa-question',
            title: 'Confirmar',
            content: 'Deseja excluir esse PDF?',
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            useBootstrap: false,
            boxWidth: '910px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        $scope.excluir_pdf_service();
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {}
                }
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };

    $scope.excluir_pdf_service = function() {
        // TO-DO
        params = {
            _token: csrf_token,
            token: token
        };
        
        $.post(site_url + 'download/postExcluirFornecedorPdfCliente', params, function (data) {
            if (data.success == 200) {
                myAlert('PDF excluído com sucesso.', 'success');
                $scope.carregar_pdfs();
                
            } else {
                myAlert(data.message, 'alert');
                $scope.carregar_pdfs();
            }
        }, 'json')
        .fail(function (data) {
            myAlert('Ocorreu um erro.', 'error');
        });
    };

    $scope.enviar_para_cliente = function() {
        $.confirm({
            icon: 'icon fa fa-question',
            title: 'Confirmar',
            content: 'Deseja enviar o PDF anexado para o cliente?',
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            useBootstrap: false,
            boxWidth: '910px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        $scope.enviar_para_cliente_service();
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {}
                }
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };

    $scope.enviar_para_cliente_service = function() {
        // TO-DO
        params = {
            _token: csrf_token,
            token: token
        };
        
        $.post(site_url + 'download/postEnviarFornecedorPdfCliente', params, function (data) {
            if (data.success == 200) {
                myAlert('PDF enviado com sucesso.', 'success');
                $scope.carregar_pdfs();
            } else {
                myAlert(data.message, 'alert');
                $scope.carregar_pdfs();
            }
        }, 'json')
        .fail(function (data) {
            myAlert('Ocorreu um erro.', 'error');
        });
    };

    $scope.Timer = $interval(function () {
        $scope.carregar_pdfs();
        $scope.hide_load();
        $scope.loaded = true;
    }, 5000);
    
    $scope.limparCamposUpload = function () {
        $('.files-control.attachPDF').find('.files-control-fake strong').text('Anexar arquivo PDF com no máx. 50 MB');
    };

    angular.element('.files-control, .files-control.attach-artwork').on('click', function () {
        angular.forEach(angular.element('.files-control.attachPDF'), function () {
            $scope.limparCamposUpload();
        });
    });
});