var site_url = '/';
var app = angular.module('app', []);

app.controller('UsuariosGerenciarCtrl', function ($scope) {
    $scope.show_load = function () {
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        $('.lds-css').hide();
    };

    $scope.enviar_novo_link = function(id_usuario, tipo) {

        $.confirm({
            icon: 'icon fa fa-question',
            title: 'Confirmar',
            content: 'Deseja enviar novo link de acesso por e-mail para o usuário?',
            closeIcon: true,
            boxWidth: '910px',
            scrollToPreviousElement: false,
            useBootstrap: false,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Enviar Novo Link</span>',
                    btnClass: 'btn-confirm',
                    action: function (Confirmar) {
                        $scope.reenviar_token(id_usuario, tipo);
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel'
                }
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');

                if ($('.ng-confirm').length) {
                    $('.master').addClass('blurred');
                }
            }
        });
    };

    $scope.reenviar_token = function(id_usuario, tipo) {
        $scope.show_load();

        $.get(site_url + 'login/gerarToken/'+tipo+'/'+id_usuario, function (data) {
            $scope.hide_load();
            myAlert('Link enviado com sucesso.', 'success');
        }, 'json')
        .fail(function (data) {
            $scope.hide_load();
            myAlert('Ocorreu um erro.', 'error');
        });
    };
});