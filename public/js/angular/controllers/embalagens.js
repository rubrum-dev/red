var site_url = '/';
var app = angular.module('app', ['cp.ngConfirm', 'ngAnimate']);

app.animation('.slide-toggle-js', function() {
    return {
        enter: function(element, done) {
            $(element).hide().slideDown(200, function ($scope) {
                done();
            });
        },
        leave: function(element, done) {
            $(element).slideUp(200, function () {
                done();
            });
        }
    };
});

app.controller('EmbalagensCtrl', function ($scope, $ngConfirm) {
    $scope.dados = {};
    $scope.files_pdf = [];
    $scope.files_arte = [];
    $scope.activeTabs = [];
    $scope.embalagens = [
        { 
            'name': 'Skol Pilsen Lata 269ml',
            'language': 'BR, AG',
            'itens': [] 
        },
        { 
            'name': 'Skol Pilsen Lata 350ml',
            'language': 'BR, AG, PY',
            'itens': [ 
                { 
                    'name': 'Front',
                    'version': 'Versão 2' 
                },
                { 
                    'name': 'Back',
                    'version': 'Novo' 
                },
                { 
                    'name': 'Foil',
                    'version': 'Versão 0'
                }
            ]
        },
        { 
            'name': 'Skol Pilsen Garrafa 355ml',
            'language': 'BR, AG',
            'itens': [] 
        },
        { 
            'name': 'Skol Pilsen Garrafa 600ml',
            'language': 'BR, AG',
            'itens': [ 
                { 
                    'name': 'Back',
                    'version': 'Novo'
                },
                { 
                    'name': 'Rolha Metállica',
                    'version': 'Versão 3' 
                }
            ]
        },
        {
            'name': 'Skol Pilsen Garrafa 1L',
            'language': 'BR, AG, PT',
            'itens': [
                { 
                    'name': 'Front',
                    'version': 'Versão 1'
                },
                { 
                    'name': 'Back',
                    'version': 'Novo'
                },
                { 
                    'name': 'Rolha Metállica',
                    'version': 'Versão 0' 
                }
            ] 
        }
    ];

    $scope.tab = 1;
    
    $scope.isOpenTab = function(tab) {
        if($scope.activeTabs.indexOf(tab) > -1) {
            return true;
        } else {
            return false;
        }
    };

    $scope.openTab = function(tab) {
        if($scope.isOpenTab(tab)) {
            $scope.activeTabs.splice($scope.activeTabs.indexOf(tab), 1);
        } else {
            $scope.activeTabs.push(tab);
        }
    };
    
    $scope.show_load = function () {
        $('.master').addClass('blurred');
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        $('.master').removeClass('blurred');
        $('.lds-css').hide();
    };
    
    $scope.setUploadPdf = function (element) {
        $scope.$apply(function (scope) {
            // Turn the FileList object into an Array
            for (var i = 0; i < element.files.length; i++) {
                $scope.files_pdf.push(element.files[i]);
            }
        });
    };
    
    $scope.setUploadArte = function (element) {
        $scope.$apply(function (scope) {
            // Turn the FileList object into an Array
            for (var i = 0; i < element.files.length; i++) {
                $scope.files_arte.push(element.files[i]);
            }
        });
    };
    
    $scope.salvar_arquivos = function() {
        if (!$scope.files_pdf.length) {
            myAlert('Anexe o PDF.', 'warning');
            return;
        }
        
        $scope.show_load();
                
        var formData = new FormData();
        formData.append("id_item", $scope.dados.id_item);
        formData.append("id_variacao", $scope.dados.id_variacao);

        for (var i = 0; i < $scope.files_pdf.length; i++) {
            //add each file to the form data and iteratively name them
            formData.append("pdf", $scope.files_pdf[i]);
        }
        
        for (var i = 0; i < $scope.files_arte.length; i++) {
            //add each file to the form data and iteratively name them
            formData.append("arte", $scope.files_arte[i]);
        }
        
        $.ajax({
            url: site_url + 'site/artwork/services/package/postArtes',
            headers: {
                'x-csrf-token': csrf_token
            },
            type: 'POST',
            data: formData,
            success: function (data) {
                var ok = $.alert({
                    title: '',
                    content: '<div class="custom-alert-contents success">' +
                    '   <span class="icon icon-check"></span>' +
                    '   <div class="custom-alert-heading">' +
                    '       <h4>Sucesso</h4>' +
                    '   </div>' +    
                    '   <p>Arquivos enviados com sucesso</p>' +
                    '</div>',
                    closeIcon: true,
                    scrollToPreviousElement: false,
                    closeIconClass: 'custom-alert-close',
                    escapeKey: true,
                    buttons: {
                        Ok: {
                            keys: ['enter', 'space', 'esc'],
                            text: 'Ok',
                            btnClass: 'custom-btn-confirm-save',
                            action: function (Confirmar) {
                                window.location.reload(true);
                                $scope.files_pdf = [];
                                $scope.files_arte = [];
                            }
                        }
                    }
                });
            },
            error: function () {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            },
            contentType: false,
            processData: false
        });
    };
    
    $scope.fileAttachmentCtrl = function (element) {
        $('.file-attachment-control span.file-placeholder').text('Procurar...');
        $('.file-attachment-control > input').on('change', function () {
            $(this).each(function () {
                var filePlaceholder = $(this).val().split('\\').pop();
                $(this).parents('.file-attachment-control').find('.file-placeholder').text(filePlaceholder);
            });
        });
    };

    $scope.artwFileUpload = function (id_variacao, id_item) {
        if (id_item === undefined) {
            id_item = '';
        }

        var artwUpload = $ngConfirm({
            boxWidth: '503px',
            useBootstrap: false,
            title: '',
            contentUrl: '/view/modal_upload_arquivo.html',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'custom-alert-close',
            buttons: {
                Cancelar: {
                    text: 'Cancelar',
                    btnClass: 'custom-btn-confirm-cancel'
                },
                Carregar: {
                    text: 'Carregar',
                    btnClass: 'custom-btn-confirm-upload',
                    keys: ['enter', 'space', 'esc'],
                    action: function (Confirmar) {
                        $scope.salvar_arquivos();
                    }
                }
            },
            onScopeReady: function () {
                var url = site_url + 'site/artwork/services/package/getArtes/' + id_variacao;
        
                if (id_item) {
                    url += '/' + id_item;
                }

                $.get(url, function (data) {

                    if (data.error) {
                        myAlert(data.message, 'error');
                    } else {
                        $scope.dados = data;
                        $scope.$apply();
                    }

                }, 'json')
                        .fail(function (data) {
                            myAlert('Ocorreu um erro.', 'error');
                        });
            },
            onReady: function (scope) {
                $scope.fileAttachmentCtrl();
            },
            onClose: function (scope) {
                $scope.dados = {};
                $('.master').removeClass('blurred');
            }
        });
    };
});

