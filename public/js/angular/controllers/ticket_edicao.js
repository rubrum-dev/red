var customLoadingHTML = '<div class="line-wobble"></div>';
var site_url = '/';
var app = angular.module('app', [
    'vAccordion', 
    'ngAnimate', 
    'ngSanitize', 
    'cp.ngConfirm', 
    'selectize', 
    'ngFileUpload', 
    '720kb.tooltips',
    'enviarArteResolveLoader',
    'pdfFornecedorResolveLoader', 
    'abaVisaoGeralMod',
    'compartilharItemMod',
    'abaPdfFornecedorMod'
]);

app.directive('dropdown', function($document) {
    return {
        restrict: 'C',
        link: function(scope, element, attr) {
            const dropdown = element.find('.dropdown-btn');


            dropdown.bind('click', function() {
                element.toggleClass('dropdown-active');
                element.addClass('active-recent');

                if(angular.element('.dropdown').hasClass('dropdown-active')) {
                    scope.interval_tickets_stop();
                } else {
                    scope.interval_tickets_start();
                } 
            });

            dropdown.bind('blur', function() {
                if (element.hasClass('active-recent')) {
                    element.removeClass('dropdown-active');
                    scope.interval_tickets_start();
                }

                element.removeClass('active-recent');
            });
        }
    }
});

app.directive('validFile', function validFile($parse) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            ngModelCtrl.$validators.validFile = function () {
                element.on('change', function () {
                    var value = element.val(),
                        model = $parse(attrs.ngModel),
                        modelSetter = model.assign;

                    scope.uploadedFileType = null;

                    if (!value) {
                        modelSetter(scope, '');
                    } else {
                        var ext = value.substring(value.lastIndexOf('.') + 1).toLowerCase();

                        if (attrs.validFile.indexOf(ext) !== -1) {
                            scope.uploadedFileType = ext;
                            modelSetter(scope, element[0].files[0]);
                        } else {
                            myAlert('Formato de arquivo inválido.', 'warning');
                            scope.uploadedFileType = 'other';
                            modelSetter(scope, '');
                            scope.limparCamposUpload();
                        }
                    }
                });
            };
        }
    };
});

app.directive('fileUpload', function () {
    return {
        scope: true, //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                var uploadObj = el.context.name;

                //iterate files since 'multiple' may be specified on the element
                for (var i = 0; i < files.length; i++) {
                    //emit event upward
                    scope.$emit("fileSelected", {
                        file: files[i]
                    });
                }
            });
        }
    };
});

app.directive('contenteditable', [
    function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                function read() {
                    ngModel.$setViewValue(element.html());
                }

                ngModel.$render = function () {
                    element.html(ngModel.$viewValue || '');
                };

                element.bind('blur keyup change', function () {
                    scope.$apply(read);
                });
            }
        };
    }
]);

app.directive("mwConfirmClick", [
    function () {
        return {
            priority: -1,
            restrict: 'A',
            scope: {
                confirmFunction: "&mwConfirmClick"
            },
            link: function (scope, element, attrs) {
                element.bind('click', function (e) {
                    var message = attrs.mwConfirmClickMessage ? attrs.mwConfirmClickMessage : 'Are you sure?';

                    if ($.confirm) {
                        $.confirm({
                            icon: 'icon icon-question',
                            title: 'Confirmar',
                            content: message,
                            closeIcon: true,
                            boxWidth: '850px',
                            scrollToPreviousElement: false,
                            useBootstrap: false,
                            closeIconClass: 'jconfirm-btn-close icon-cross',
                            buttons: {
                                Confirmar: {
                                    keys: ['enter', 'space'],
                                    text: '<span><i class="icon icon-check"></i>Confirmar</span>',
                                    btnClass: 'btn-confirm',
                                    action: function (Confirmar) {
                                        scope.confirmFunction();
                                    }
                                },
                                Cancelar: {
                                    keys: ['esc'],
                                    text: '<span><i class="icon icon-ban-circle"></i>Cancelar</span>',
                                    btnClass: 'btn btn-cancel'
                                }
                            },
                            onOpenBefore: function () {
                                //$('body').addClass('jconfirm-overlay');
                                $('body').addClass('no-scroll');
                                //$('.master').addClass('blurred');
                                $('.jconfirm').css('overflow', 'auto');
                                $('.jconfirm').overlayScrollbars({
                                    scrollbars: {
                                        autoHide: 'move'
                                    }
                                });
                            },
                            onClose: function () {
                                //$('body').removeClass('jconfirm-overlay');
                                $('body').removeClass('no-scroll');
                                //$('.master').removeClass('blurred');
                                $('.jconfirm').css('overflow', 'hidden');
                            }
                        });
                    } else {
                        if (window.confirm(message)) {
                            scope.confirmFunction();
                        }
                    }
                });
            }
        };
    }
]);

app.controller('TicketEdicaoCtrl', function ($scope, $rootScope, $http, $interval, $ngConfirm, $window, Upload, $timeout) {
    $scope.artw_tab = 1;
    $scope.tab = 1;
    $scope.id_ticket = id_ticket;
    $scope.ticket = {};
    $scope.mensagens = [];
    $scope.exibir_responder_msg = [];
    $scope.comentarios = [];
    $scope.enviar_comentarios = [];
    $scope.respostas = [];
    $scope.enviar_respostas = [];
    $scope.exibe_comentar = [];
    $scope.exibe_responder = [];
    $scope.index_resposta = 0;
    $scope.interval_tickets = false;
    $scope.legenda = '';
    $scope.step3 = false;
    $scope.enviar_emails = [];
    $scope.files_arquivo = [];
    $scope.files_layout = [];
    $scope.files_upload_arte = [];
    $scope.files = [];
    $scope.fornecedores = [];
    $scope.fornecedorSelecionado = '';
    $scope.msgTicket = '';
    $scope.data = {};
    $scope.modal_enviar = false;
    $scope.exibe_loading = false;

    $scope.cancelamento_descricao = '';
    
    $scope.cancelado = false;
    $scope.pausado = true;

    $scope.link_ativo;
    $scope.link_expirado = true;
    
    $scope.memberList = [];

    $scope.model = {
        selectedMemberlList: []
    };

    $scope.atrw_arquivo_atual = null;
    $scope.atrw_arquivo_anterior = null;

    $scope.disable = true;

    $scope.artwFileCompareConfig = {
        create: false,
        persist: false,
        valueField: 'id',
        labelField: 'nome',
        delimiter: '|',
        placeholder: '',
        dropdownParent: 'body',
        scrollDuration: 0,
        maxItems: 1,
        onInitialize: function (selectize) {},
        onDropdownOpen: function () {
            this.clear();
            this.focus();
        }
    };

    $scope.renovarAprovacaoService = function (participante) {
        $scope.interval_tickets_stop();
        $scope.show_load();

        params = {
            _token: csrf_token,
            id_participante: participante.id_participante
        };

        $.post(site_url + 'site/artwork/services/package/ticket/renovarAprovacao', params, function (data) {

            if (data.error) {
                myAlert(data.message, 'error');
                $scope.carregar_ticket(1);
            } else {
                myAlert(data.message, 'success');
                $scope.carregar_ticket(1);
            }

            $scope.interval_tickets_start();

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
    };

    $scope.renovarAprovacao = function (participante) {
        var message = 'Renovar a aprovação de ' + participante.nome_usuario + '?';

        $.confirm({
            icon: 'icon icon-question',
            title: 'Confirmar',
            content: message,
            closeIcon: true,
            boxWidth: '850px',
            scrollToPreviousElement: false,
            useBootstrap: false,
            closeIconClass: 'jconfirm-btn-close icon-cross',
            buttons: {
                Renovar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon icon-check"></i>Renovar Aprovação</span>',
                    btnClass: 'btn-confirm',
                    action: function (Todos) {
                        $scope.renovarAprovacaoService(participante);
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon icon-ban-circle"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel'
                }
            },
            onOpenBefore: function () {
                //$('body').addClass('jconfirm-overlay');
                $('body').addClass('no-scroll');
                //$('.master').addClass('blurred');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                //$('body').removeClass('jconfirm-overlay');
                $('body').removeClass('no-scroll');
                //$('.master').removeClass('blurred');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };

    $scope.seguir = function (id_usuario, id_ticket) {
        $scope.interval_tickets_stop();
        $scope.show_load();

        params = {
            _token: csrf_token,
            id_usuario: id_usuario,
            id_ticket: id_ticket,
            recebe_email: $scope.ticket.recebe_email
        };

        $.post(site_url + 'site/artwork/services/package/ticket/seguir', params, function (data) {

            if (data.error) {
                myAlert(data.message, 'error');
                $scope.carregar_ticket(1);
            } else {
                myAlert(data.message, 'success');
                $scope.carregar_ticket(1);
            }

            $scope.interval_tickets_start();

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.interval_tickets_stop();
                    $scope.hide_load();
                });
    };
    
    $scope.setStatusEnvio = function (id_arte_envio) {
        $scope.interval_tickets_stop();
        $scope.show_load();

        params = {
            _token: csrf_token,
            id_arte_envio: id_arte_envio
        };

        $.post(site_url + 'site/artwork/services/package/ticket/setStatusEnvio', params, function (data) {

            if (data.error) {
                myAlert(data.message, 'error');
                $scope.carregar_ticket(1);
            } else {
                myAlert(data.message, 'success');
                $scope.carregar_ticket(1);
            }

            $scope.interval_tickets_start();

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.carregar_ticket(1);
                });
    };
    
    $scope.renovarLink = function (id_arte_envio) {
        $scope.interval_tickets_stop();
        $scope.show_load();

        params = {
            _token: csrf_token,
            id_arte_envio: id_arte_envio
        };

        $.post(site_url + 'site/artwork/services/package/ticket/renovarLink', params, function (data) {

            if (data.error) {
                myAlert(data.message, 'error');
            } else {
                myAlert(data.message, 'success');
                $scope.carregar_ticket(1);
            }

            $scope.interval_tickets_start();

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.carregar_ticket(1);
                });
    };
    
    $scope.reenviarEmail = function (id_arte_envio) {
        $scope.interval_tickets_stop();
        $scope.show_load();

        params = {
            _token: csrf_token,
            id_arte_envio: id_arte_envio
        };

        $.post(site_url + 'site/artwork/services/package/ticket/reenviarEmail', params, function (data) {

            if (data.error) {
                myAlert(data.message, 'error');
                $scope.carregar_ticket(1);
            } else {
                myAlert(data.message, 'success');
                $scope.carregar_ticket(1);
            }

            $scope.interval_tickets_start();

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.carregar_ticket(1);
                });
    };

    $scope.enviarLembreteService = function (id_participante, id_perfil, todos) {
        $scope.show_load();

        params = {
            _token: csrf_token,
            id_participante: id_participante,
            id_perfil: id_perfil,
            todos: todos
        };

        $.post(site_url + 'site/artwork/services/package/ticket/enviarLembrete', params, function (data) {
            myAlert(data.message, 'success');
            $scope.carregar_ticket(1);
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.interval_tickets_stop();
                    $scope.hide_load();
                });
    };

    $scope.enviarLembrete = function (id_participante, id_perfil, enviadas) {
        if (enviadas) {
            myAlert('Notificação já enviada recentemente, você poderá enviar uma nova notificação dentro de 24h.', 'alert');
            return false;
        }

        if ($scope.ticket.nr_perfis[id_perfil].qtd > 1) {
            var message = 'O lembrete pode ser enviado para todos os Aprovadores ou apenas para o participante selecionado.<br /> Deseja enviar para:';

            $.confirm({
                icon: 'icon icon-question',
                title: 'Confirmar',
                content: message,
                closeIcon: true,
                closeIconClass: 'jconfirm-btn-close icon-cross',
                useBootstrap: false,
                boxWidth: '850px',
                buttons: {
                    Participantes: {
                        text: '<span>Apenas o Selecionado</span>',
                        btnClass: 'btn btn-common btn-confirm',
                        action: function (Participantes) {
                            $scope.enviarLembreteService(id_participante, id_perfil, 0);
                        }
                    },
                    Todos: {
                        keys: ['enter', 'space'],
                        text: '<span>Todos os Aprovadores</span>',
                        btnClass: 'btn btn-common btn-confirm',
                        action: function (Todos) {
                            $scope.enviarLembreteService(id_participante, id_perfil, 1);
                        }
                    },
                    Cancelar: {
                        keys: ['esc'],
                        text: '<span><i class="icon icon-ban-circle"></i>Cancelar</span>',
                        btnClass: 'btn btn-cancel',
                        action: function (Cancelar) {}
                    }
                },
                onOpenBefore: function () {
                    //$('body').addClass('jconfirm-overlay');
                    $('body').addClass('no-scroll');
                    //$('.master').addClass('blurred');
                    $('.jconfirm').css('overflow', 'auto');
                    $('.jconfirm').overlayScrollbars({
                        scrollbars: {
                            autoHide: 'move'
                        }
                    });
                },
                onClose: function () {
                    //$('body').removeClass('jconfirm-overlay');
                    $('body').removeClass('no-scroll');
                    //$('.master').removeClass('blurred');
                    $('.jconfirm').css('overflow', 'hidden');
                }
            });
        } else {
            $scope.enviarLembreteService(id_participante, id_perfil, 0);
        }
    };

    $scope.toggleAll = function () {
        var toggleStatus = $scope.isAllSelected;

        angular.forEach($scope.memberList, function (itm) {

            itm.selected = toggleStatus;

            angular.forEach(itm.equipe, function (equipe) {

                equipe.selected = toggleStatus;

            });

        });
    };

    $scope.enviar_msg_email = function (id_usuario) {

        $scope.memberList = $scope.ticket.participantesUnicos;

        var message = 'Selecione os particpantes que deseja que recebam uma cópia da mensagem por e-mail.';

        $ngConfirm({
            useBootstrap: false,
            boxWidth: '850px',
            title: '',
            content: '<div class="custom-alert-contents custom-alert-contents-center">' +
                    '   <div class="custom-alert-heading custom-alert-heading-center confirmation">' +
                    '       <h4 class="custom-alert-title">Enviar mensagem por e-mail</h4>' +
                    '   </div>' +
                    '   <div class="content-participantes">' +
                    '       <div class="custom-alert-text-block">' +
                    '           <p>' + message + '</p>' +
                    '       </div>' +
                    '       <div class="custom-alert-checkbox-group">' +
                    '           <div ng-show="member.id_usuario != ticket.id_usuario_logado" ng-repeat="member in memberList" class="row clearfix">' +
                    '               <div class="row-sub clearfix">' +
                    '                   <label class="checkbox-col">' +
                    '                       <input ng-init="member.selected = (member.id_usuario == ' + id_usuario + ') ? true : false" type="checkbox" ng-model="member.selected" class="fake-checkbox" />' +
                    '                       <span>{{ member.nome_usuario }}</span>' +
                    '                   </label>' +
                    '                   <label class="profile-type-label">{{ member.nome_perfil }}</label>' +
                    '               </div>' +
                    '               <div ng-show="equipe.id_usuario != ticket.id_usuario_logado" ng-repeat="equipe in member.equipe" class="row-sub clearfix">' +
                    '                   <label class="checkbox-col">' +
                    '                       <input ng-init="equipe.selected = (equipe.id_usuario == ' + id_usuario + ') ? true : false" type="checkbox" ng-model="equipe.selected" class="fake-checkbox" />' +
                    '                       <span>{{ equipe.nome_usuario }}</span>' +
                    '                   </label>' +
                    '                   <label class="profile-type-label">{{ member.nome_perfil }}</label>' +
                    '               </div>' +
                    '           </div>' +
                    '       </div>' +
                    '       <div class="row checkbox-select-all clearfix">' +
                    '           <label>' +
                    '               <input type="checkbox" ng-click="toggleAll()" ng-model="isAllSelected" class="fake-checkbox" />' +
                    '               <span>Selecionar Todos</span>' +
                    '           </label>' +
                    '       </div>' +
                    '   </div>' +
                    '</div>',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'custom-alert-close',
            backgroundDismiss: true,
            escapeKey: 'Cancelar',
            buttons: {
                Cancelar: {
                    keys: ['esc'],
                    text: '<i class="icon icon-ban-circle"></i>Cancelar',
                    btnClass: 'custom-ng-btn-confirm-cancel',
                    action: function (Cancelar) {
                        $scope.memberList = [];
                        $scope.modal_enviar = false;
                    }
                },
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<i class="icon icon-check"></i>Confirmar',
                    btnClass: 'custom-ng-btn-confirm-save',
                    action: function (Confirmar) {
                        $scope.modal_enviar = true;
                    }
                }
            }
        });
    };
    
    $scope.getArquivoById = function(id) {
        
        for(i = 0; i < $scope.ticket.comparacao.length; i++) {
            if($scope.ticket.comparacao[i].id == id) {
                return $scope.ticket.comparacao[i];
            }
        }
    };
    
    $scope.enviarCompararArtes = function (e) {
        if (!$scope.atrw_arquivo_atual) {
            $('#artwFileCurrent ~ .selectize-control > .selectize-input').addClass('input-error');
            
            myAlert('Selecione um arquivo para o ciclo atual.');
            e.preventDefault()
        } else {
            $('#artwFileCurrent ~ .selectize-control > .selectize-input').removeClass('input-error');
        }

        if (!$scope.atrw_arquivo_anterior) {
            $('#artwFilePrevious ~ .selectize-control > .selectize-input').addClass('input-error');

            myAlert('Selecione um arquivo para o ciclo anterior.');
            
            e.preventDefault()
        } else {
            $('#artwFilePrevious ~ .selectize-control > .selectize-input').removeClass('input-error');
        }
                
        if ($scope.atrw_arquivo_atual && $scope.atrw_arquivo_anterior) {
            
            var arquivo1 = $scope.getArquivoById($scope.atrw_arquivo_atual);
            var arquivo2 = $scope.getArquivoById($scope.atrw_arquivo_anterior);
            
            $window.open('/site/artwork/compare/' + arquivo1.id + '/' + arquivo1.tipo + '/' + arquivo2.id + '/' + arquivo2.tipo);
            
        } else {
            
            return false;
            
        }
    };
    
    $scope.comparar_artes = function () {
        $ngConfirm({
            useBootstrap: false,
            boxWidth: '850px',
            title: '',
            contentUrl: '/view/modal_comparar_artes.html',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'custom-alert-close',
            backgroundDismiss: true,
            escapeKey: 'Cancelar',
            buttons: {
                Cancelar: {
                    keys: ['esc'],
                    text: '<i class="icon icon-ban-circle"></i>Cancelar',
                    btnClass: 'custom-ng-btn-confirm-cancel',
                    action: function (Cancelar) {
                    }
                },
                Salvar: {
                    keys: ['enter', 'space'],
                    text: '<i class="icon icon-check"></i>Comparar',
                    btnClass: 'custom-ng-btn-confirm-save',
                    action: function (Salvar) {
                        $scope.enviarCompararArtes();
                    }
                }
            },
            onOpenBefore: function () {
                $scope.interval_tickets_stop();
            },
            onClose: function () {
                $scope.interval_tickets_start();
            }
        });

    };

    //listen for the file selected event
    $scope.$on("fileSelected", function (event, args) {
        $scope.$apply(function () {
            //add the file object to the scope's files collection
            $scope.files.push(args.file);
        });
    });

    $scope.show_load = function () {
        //$('.master').addClass('blurred');
        $('.lds-css').show();
        $scope.exibe_loading = true;
    };

    $scope.hide_load = function () {
        //$('.master').removeClass('blurred');
        $('.lds-css').hide();
        $scope.exibe_loading = false;
    };
   
    /* Enviar Arte-final - V2 */
    /*$scope.enviar_emails = function (a, id_arte) {
        if (!$scope.enviar_emails[a]) {
            $('body').find('.artw-sending-group select').addClass('has-error');
            $('body').find('.artw-sending-group input').addClass('has-error');

            myAlert('Selecione o Fornecedor ou digite o e-mail de um destinatário específico.', 'warning');

            return false;
        }

        $('body').find('.artw-sending-group select').removeClass('has-error');
        $('body').find('.artw-sending-group input').removeClass('has-error');

        $scope.show_load();

        if ($scope.ticket.artes[a].fornecedorSelecionado) {
            var fornecedor = $scope.fornecedores[$scope.ticket.artes[a].fornecedorSelecionado];
            fornecedor = fornecedor.id;
        } else {
            var fornecedor = null;
        }

        params = {
            _token: csrf_token,
            id: id_arte,
            emails: $scope.enviar_emails[0],
            msg_fornecedor: $scope.ticket.artes[a].msg_fornecedor,
            fornecedor: fornecedor
        };

        $.post(site_url + 'site/artwork/services/package/ticket/enviarEmails', params, function (data) {
            if (data.success === 201) {
                myAlert(data.message, 'alert');
                $scope.hide_load();
            } else {
                myAlert(data.message, 'success');
                $scope.enviar_emails[a] = '';
                $scope.ticket.artes[a].msg_fornecedor = '';

                $('#cbSupplierChooser').val('');
                $('#cbSupplierChooser').blur();

                $scope.carregar_ticket(1);
            }
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
    };*/

    /* Ver Mensagem */
    /*$scope.verMsgFornecedor = function(mensagem) {
        
        mensagem = (mensagem) ? mensagem : 'Nenhuma mensagem foi incluída no envio da arte para o fornecedor.';
        
        $ngConfirm({
            useBootstrap: false,
            boxWidth: '910px',
            title: '',
            content: '<div class="ng-confirm-action-dialog">' +
                '<div class="dialog-heading">' +
                    '<h4>Mensagem enviada</h4>' +
                '</div>' +
                '<div class="text no-margin">' +    
                    '<p ng-if="!msg_fornecedor" class="no-margin">'+mensagem+'</p>' +
                '</div>' +
            '</div>',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'ng-confirm-btn-close fa fa-times',
            backgroundDismiss: true
        });    
    };*/

    /* Enviar Arte-final - V1 */
    /*$scope.enviar_emails = function (a, id_arte) {
        if (!$scope.enviar_emails[a]) {
            $('body').find('.send-artwork-group .col-supplier').addClass('input-error');
            myAlert('Selecione o Fornecedor ou digite o e-mail de um destinatário específico.', 'warning');
            return false;
        }

        $('body').find('.send-artwork-group .col-supplier').removeClass('input-error');

        $scope.show_load();

        if ($scope.ticket.artes[a].fornecedorSelecionado) {
            var fornecedor = $scope.fornecedores[$scope.ticket.artes[a].fornecedorSelecionado];
            fornecedor = fornecedor.id;
        } else {
            var fornecedor = null;
        }

        params = {
            _token: csrf_token,
            id: id_arte,
            emails: $scope.enviar_emails[a],
            fornecedor: fornecedor
        };

        $.post(site_url + 'site/artwork/services/package/ticket/enviarEmails', params, function (data) {
            
            if (data.success === 201)
            {
                
                myAlert(data.message, 'alert');
                $scope.hide_load();
                
            }
            else
            {
                
                myAlert(data.message, 'success');
                $scope.enviar_emails[a] = '';
                
                $('.suppliers').val('');
                $('.suppliers').blur();
                
                $scope.carregar_ticket(1);
                
            }
            

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
    };*/

    $scope.acao_solicitacao = function (id_solicitacao, status) {

        $scope.show_load();

        params = {
            _token: csrf_token,
            id: id_solicitacao,
            status: status
        };

        $.post(site_url + 'site/artwork/services/package/solicitacao/acao', params, function (data) {
            $scope.carregar_ticket(1);
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                });
    };

    $scope.acao_ticket = function (status) {
        $scope.show_load();

        params = {
            _token: csrf_token,
            id: id_ticket,
            status: status
        };

        $.post(site_url + 'site/artwork/services/package/ticket/acao', params, function (data) {
            $scope.carregar_ticket(1);
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                });
    };

    $scope.customFileInputCtrl = function () {
        angular.element('.files-control').find('strong').text('Anexar arquivo');
        angular.element('.files-control.attachPDF').find('strong').text('Anexar arquivo PDF com no máx. 50 MB');

        angular.element('.files-control').on('click', function () {
            angular.forEach(angular.element('.files-control'), function () {
                $scope.limparCamposUpload();
            });
        });

        angular.element('.files-control input[type="file"], .files-control.attachPDF input[type="file"]').bind('change focus', function (event) {
            angular.forEach(angular.element(event.target), function () {
                if (angular.element(event.target).val()) {
                    var filePlaceholder = angular.element(event.target).val().split('\\').pop();
                    angular.element(event.target).next('.files-control-fake').find('strong').text(filePlaceholder);
                } else if (!angular.element(event.target).val()) {
                    $scope.limparCamposUpload();
                }
            });
        });
    };

    $scope.limparCamposUpload = function () {
        $('body').find('input[type="file"]').val('');
        $('body').find('.files-control-fake strong').text('Anexar arquivo');
        $('body').find('.files-control.attachPDF strong').text('Anexar arquivo PDF com no máx. 50 MB');
    };

    $scope.setArquivo = function (element) {
        $scope.$apply(function (scope) {
            // Turn the FileList object into an Array
            for (var i = 0; i < element.files.length; i++) {
                $scope.files_arquivo.push(element.files[i]);
            }
        });
    };

    $scope.setUploadArte = function (element) {
        $scope.$apply(function (scope) {
            // Turn the FileList object into an Array
            for (var i = 0; i < element.files.length; i++) {
                $scope.files_upload_arte.push(element.files[i]);
            }
        });
    };

    $scope.setLayout = function (element) {
        $scope.$apply(function (scope) {
            // Turn the FileList object into an Array
            for (var i = 0; i < element.files.length; i++) {
                $scope.files_layout.push(element.files[i]);
            }
        });
    };

    $scope.uploadArquivoOLD = function (id) {
        if (!$scope.files_arquivo.length) {
            $('body').find('.file-attachment-container .file-attachment-control').css('borderColor', 'dashed', '#EA4334');
            $('body').find('.file-attachment-container .file-attachment-control').addClass('input-error');
            myAlert('Anexe um arquivo.', 'warning');
            return false;
        }

        $('body').find('.file-attachment-container .file-attachment-control').css('borderColor', '#ACACAC');
        $('body').find('.file-attachment-container .file-attachment-control').removeClass('input-error');
        $('body').find('input[type="text"]').removeClass('input-error');

        $scope.show_load();

        var formData = new FormData();
        formData.append("legenda", $scope.legenda);
        formData.append("id", id);

        for (var i = 0; i < $scope.files_arquivo.length; i++) {
            //add each file to the form data and iteratively name them
            formData.append("file" + i, $scope.files_arquivo[i]);
        }

        $.ajax({
            url: site_url + 'site/artwork/services/package/ticket/uploadArquivo',
            headers: {
                'x-csrf-token': csrf_token
            },
            type: 'POST',
            data: formData,
            success: function (data) {
                $scope.legenda = '';
                $scope.files_arquivo = [];
                $scope.carregar_ticket(1);
                $scope.limparCamposUpload();
            },
            error: function () {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            },
            contentType: false,
            processData: false
        });
    };

    $scope.uploadArquivo = function (id) {
        
        if (!$scope.arquivos) {
            $('body').find('.file-attachment-container .file-attachment-control').css('borderColor', 'dashed', '#EA4334');
            $('body').find('.file-attachment-container .file-attachment-control').addClass('input-error');
            myAlert('Anexe um arquivo.', 'warning');
            return false;
        }

        $scope.arquivos.progress = 0;
        $scope.interval_tickets_stop();
        
        $('body').find('.file-attachment-container .file-attachment-control').css('borderColor', '#ACACAC');
        $('body').find('.file-attachment-container .file-attachment-control').removeClass('input-error');
        $('body').find('input[type="text"]').removeClass('input-error');

        $scope.arquivos.upload = Upload.upload({
            url: site_url + 'site/artwork/services/package/ticket/uploadArquivo',
            data: {id: id, file0: $scope.arquivos}
        });

        $scope.arquivos.upload.then(function (response) {
            $timeout(function () {
                $scope.arquivos = '';
                $scope.carregar_ticket(1);
                $scope.limparCamposUpload();
            });
        }, function (response) {
            if (response.status > 0) {
                
                myAlert('Ocorreu um erro.', 'error');
                $scope.arquivos.progress = 0;
                $scope.interval_tickets_start();
                
            }
            
        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            $scope.arquivos.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
        
    };

    $scope.uploadArteOLD = function (id) {
        if (!$scope.files_upload_arte.length) {
            $('body').find('.artwork-upload-section .file-attachment-container .file-attachment-control').css('borderColor', '#EA4334');
            $('body').find('.artwork-upload-section .file-attachment-container .file-attachment-control').addClass('input-error');
            myAlert('Anexe um Arquivo.', 'warning');
            return false;
        }

        $('body').find('.artwork-upload-section .file-attachment-container .file-attachment-control').css('borderColor', '#ACACAC');
        $('body').find('.artwork-upload-section .file-attachment-container .file-attachment-control').removeClass('input-error');
        $('body').find('.artwork-upload-section .file-attachment-container input[type="text"]').removeClass('input-error');

        $scope.show_load();

        var formData = new FormData();
        formData.append("id", id);

        for (var i = 0; i < $scope.files_upload_arte.length; i++) {
            formData.append("file" + i, $scope.files_upload_arte[i]);
        }

        $.ajax({
            url: site_url + 'site/artwork/services/package/ticket/uploadArte',
            headers: {
                'x-csrf-token': csrf_token
            },
            type: 'POST',
            data: formData,
            success: function (data) {
                $scope.files_upload_arte = [];
                $scope.carregar_ticket(1);
                $scope.limparCamposUpload();
            },
            error: function () {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            },
            contentType: false,
            processData: false
        });
    };
    
    $scope.uploadArte = function (id) {
        
        if (!$scope.arte) {
            $('body').find('.artwork-upload-section .file-attachment-container .file-attachment-control').css('borderColor', '#EA4334');
            $('body').find('.artwork-upload-section .file-attachment-container .file-attachment-control').addClass('input-error');
            myAlert('Anexe um Arquivo.', 'warning');
            return false;
        }

        $scope.arte.progress = 0;
        $scope.interval_tickets_stop();
        
        $('body').find('.artwork-upload-section .file-attachment-container .file-attachment-control').css('borderColor', '#ACACAC');
        $('body').find('.artwork-upload-section .file-attachment-container .file-attachment-control').removeClass('input-error');
        $('body').find('.artwork-upload-section .file-attachment-container input[type="text"]').removeClass('input-error');

        $scope.arte.upload = Upload.upload({
            url: site_url + 'site/artwork/services/package/ticket/uploadArte',
            data: {id: id, file0: $scope.arte}
        });

        $scope.arte.upload.then(function (response) {
            $timeout(function () {
                $scope.arte = '';
                $scope.carregar_ticket(1);
                $scope.limparCamposUpload();
            });
        }, function (response) {
            if (response.status > 0) {
                
                myAlert('Ocorreu um erro.', 'error');
                $scope.arte.progress = 0;
                $scope.interval_tickets_start();
                
            }
            
        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            $scope.arte.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
        
    };
    
    $scope.uploadLayout = function (id) {
        
        if (!$scope.layout) {
            $('body').find('.updated-version-container .file-attachment-control').css('borderColor', '#EA4334');
            $('body').find('.updated-version-container .file-attachment-control').addClass('input-error');
            myAlert('Anexe um Layout.', 'warning');
            return false;
        }

        $scope.layout.progress = 0;
        $scope.interval_tickets_stop();
        
        $('body').find('.updated-version-container .file-attachment-control').css('borderColor', '#ACACAC');
        $('body').find('.updated-version-container .file-attachment-control').removeClass('input-error');

        $scope.layout.upload = Upload.upload({
            url: site_url + 'site/artwork/services/package/ticket/uploadLayout',
            data: {id: id, file0: $scope.layout}
        });

        $scope.layout.upload.then(function (response) {
            $timeout(function () {
                $scope.layout = '';
                myAlert('Layout salvo com sucesso.', 'success');
                $scope.carregar_ticket(1);
                $scope.limparCamposUpload();
            });
        }, function (response) {
            if (response.status > 0) {
                
                myAlert('Ocorreu um erro.', 'error');
                $scope.layout.progress = 0;
                $scope.interval_tickets_start();
                
            }
            
        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            $scope.layout.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
        
    };

    $scope.deleteArte = function (id) {

        $scope.show_load();

        params = {
            _token: csrf_token,
            id: id
        };

        $.post(site_url + 'site/artwork/services/package/ticket/deleteArte', params, function (data) {
            $scope.carregar_ticket(1);
            $scope.interval_tickets_start();

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
    };

    $scope.interval_tickets_start = function () {
        if ($scope.interval_tickets === false) {
            $scope.Timer = $interval(function () {
                $scope.carregar_ticket(0);
            }, 4000);
            $scope.interval_tickets = true;
        }
    };

    $scope.interval_tickets_stop = function () {
        $interval.cancel($scope.Timer);
        $scope.interval_tickets = false;
    };

    $scope.carregar_ticket = function (exibe_loading) {
        if ($('.chat-comment-box').is(':visible')) {
            return;
        }
    
        if ($('.dropdown').find('.dropdown-menu').is(':visible')) {
            return;
        }

        if ($('.dropdown').find('.dropdown-list').is(':visible')) {
            return;
        }
                        
        if (exibe_loading == 1) {
            $scope.show_load();
        }
        
        $.get(site_url + 'site/artwork/services/package/ticket/' + id_ticket, function (data) {
            if ($scope.ticket.id_status && $scope.ticket.id_status != data.id_status) {
                var topo = true;
            } else {
                var topo = false;
            }
            
            $scope.ticket = data;
                        
            if ($scope.ticket.comparacao.length >= 2 && exibe_loading) {
                
                $scope.atrw_arquivo_atual = $scope.ticket.comparacao[0].id;
                $scope.atrw_arquivo_anterior = $scope.ticket.comparacao[1].id;
                
            }
            
            var solicitacoes = (data.ciclo_atual.solicitacoes) ? data.ciclo_atual.solicitacoes : [];

            total = parseInt(solicitacoes.length);
            total_revisadas = 0;

            for (var i = 0; i < total; i++) {
                if (solicitacoes[i].id_status === 2) {
                    total_revisadas++;
                }
            }
            
            if (total === total_revisadas) {
                $scope.step3 = true;
            } else {
                $scope.step3 = false;
            }

            $scope.$apply();

            if (topo) {
                location = '#';
            }

            if (exibe_loading == 1) {
                $scope.hide_load();
                
                $timeout(function () {
                    $rootScope.loaded = true;
                }, new Date().getMilliseconds())
            }
        }, 'json')
                .fail(function (data) {

                    if (exibe_loading == 1) {
                        $rootScope.loaded = true;
                        $scope.hide_load();
                    }

                });
    };

    $scope.carregar_fornecedores = function () {
        $scope.show_load();
        $rootScope.loaded = false;
        
        $.get(site_url + 'site/artwork/services/package/fornecedores', function (data) {
            $scope.fornecedores = data;
            $scope.$apply();
            $rootScope.loaded = true;
        }, 'json')
                .fail(function (data) {
                    $scope.hide_load();
                    $rootScope.loaded = true;
                });
    };

    $scope.setar_fornecedor = function (arte) {
        var fornecedor = $scope.fornecedores[$scope.ticket.artes[arte].fornecedorSelecionado];

        if (fornecedor && fornecedor.emails) {
            $scope.enviar_emails[arte] = fornecedor.emails;
        } else {
            $scope.enviar_emails[arte] = null;
        }
    };

    $scope.adicionar_mensagem = function () {
        if ($scope.msgTicket === null || $scope.msgTicket === undefined || $scope.msgTicket === '') {
            $('.textbox-mensagens').addClass('input-error');
            $scope.interval_tickets_stop();
            myAlert('Insira uma mensagem.', 'warning');
            return false;
        }

        $scope.show_load();

        params = {
            _token: csrf_token,
            id: $scope.ticket.id,
            texto: $scope.msgTicket,
            tipo: 'geral',
            enviar_membros: $scope.memberList,
            modal_enviar: $scope.modal_enviar
        };

        $.post(site_url + 'site/artwork/services/package/comentar', params, function (data) {
            $scope.msgTicket = '';
            $scope.carregar_ticket(1);
            $scope.interval_tickets_start();
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });


        $scope.msgTicket = '';
        $scope.modal_enviar = false;

        $('.textbox-mensagens').removeClass('input-error');
    };

    $scope.msg_file_upload_service = function (tipo, id) {

        var formData = new FormData();
        formData.append("file1", $scope.item);
        
        $.ajax({
            url: site_url + 'site/artwork/services/package/upload',
            headers: {
                'x-csrf-token': csrf_token
            },
            type: 'POST',
            data: formData,
            success: function (data) {
                var imageUrl = data.url;
                var imgURLIcon = '';
                
                imgURLIcon += ' ';
                imgURLIcon += '<label class="attach" contenteditable="false">';
                imgURLIcon += '<a class="blob icon-camera" href="' + imageUrl + '" target="blank">';
                imgURLIcon += '<span>Clique aqui para visualizar</span>';
                imgURLIcon += '</a>';
                imgURLIcon += '</label>';
                imgURLIcon += ' ';

                if (tipo == 'geral') {
                    $scope.msgTicket += imgURLIcon;
                } else if (tipo == 'respostas') {
                    $scope.respostas[id] += imgURLIcon;
                } else if (tipo == 'comentarios') {
                    $scope.comentarios[id] += imgURLIcon;
                } else {
                    alert('não implementado');
                }

                $scope.$apply();
            },
            error: function () {
                myAlert('Ocorreu um erro.', 'error');
            },
            contentType: false,
            processData: false
        });
    };

    $scope.msg_attach_file = function (element, tipo) {
        var fileInput = angular.element('.msgAttachment');

        if (tipo === undefined) {
            tipo = null;
        }

        $scope.item = element.files[0];

        if ($scope.item.type.indexOf("image") != -1) {
            $scope.msg_file_upload_service(tipo, element.getAttribute("data-id"));
            angular.element(fileInput).val(null);
        } else {
        }
    };

    $scope.msg_copy_paste_handler = function (e, tipo, id) {

        clipboardData = e.clipboardData || e.originalEvent.clipboardData;

        if (clipboardData.items === undefined) {
            return;
        }

        if (id === undefined) {
            id = null;
        }

        for (var i = 0; i < clipboardData.items.length; i++) {
            var item = clipboardData.items[i];

            if (item.type.indexOf("image") != -1) {
                $scope.item = item.getAsFile();
                $scope.msg_file_upload_service(tipo, id);
                e.preventDefault();
            } else {
            }
        }
    };

    $scope.comments_file_upload_service = function (s) {
        var imageUrl = 'http://www.google.com.br',
                s = [],
                imgURLIcon = '';

        imgURLIcon += ' ';
        imgURLIcon += '<label class="attach" contenteditable="false">';
        imgURLIcon += '<a class="blob icon-camera" href="' + imageUrl + '" target="blank" contenteditable="false">';
        imgURLIcon += '<span>Clique aqui para visualizar</span>';
        imgURLIcon += '</a>';
        imgURLIcon += '</label>';
        imgURLIcon += ' ';

        $scope.comentarios[s] += imgURLIcon;
    };
    
    $scope.exibir_responder_msg = function ($id) {
        if ($scope.exibir_responder_msg[$id] === 1) {
            $scope.exibir_responder_msg[$id] = '';
            $scope.interval_tickets_start();
        } else {
            $scope.exibir_responder_msg[$id] = 1;
            $scope.interval_tickets_stop();
        }
    };

    $scope.exibe_comentar = function (s) {
        if ($scope.exibe_comentar[s] === 1) {
            $scope.exibe_comentar[s] = '';
            $scope.interval_tickets_start();
        } else {
            $scope.exibe_comentar[s] = 1;
            $scope.interval_tickets_stop();
        }
    };

    $scope.exibe_responder = function ($id) {
        if ($scope.exibe_responder[$id] === 1) {
            $scope.exibe_responder[$id] = '';
            $scope.interval_tickets_start();
        } else {
            $scope.exibe_responder[$id] = 1;
            $scope.interval_tickets_stop();
        }
    };

    $scope.comentar = function (id, s) {
        if ($scope.comentarios[s] === null || $scope.comentarios[s] === undefined || $scope.comentarios[s] === '') {
            $('.chat-comment-box').addClass('input-error');
            $scope.interval_tickets_stop();
            myAlert('Insira uma mensagem.', 'warning');
            return false;
        }

        $scope.show_load();

        params = {
            _token: csrf_token,
            id: id,
            texto: $scope.comentarios[s],
            enviar_membros: $scope.memberList,
            modal_enviar: $scope.modal_enviar
        };

        $.post(site_url + 'site/artwork/services/package/comentar', params, function (data) {
            //myAlert('Comentário adicionado com sucesso.');
            $('.chat-comment-box').hide();
            $scope.exibe_comentar[s] = '';
            $scope.comentarios[s] = '';
            $scope.enviar_comentarios[s] = '';
            $scope.carregar_ticket(1);
            $scope.interval_tickets_start();
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });

        $scope.modal_enviar = false;
    };

    $scope.responder = function (id, $id, tipo) {

        if (tipo == undefined) {
            tipo = null;
        }

        if ($scope.respostas[$id] === null || $scope.respostas[$id] === undefined || $scope.respostas[$id] === '') {
            $('.chat-comment-box').addClass('input-error');
            $scope.interval_tickets_stop();
            myAlert('Insira uma resposta.', 'warning');
            return false;
        }

        $scope.show_load();

        params = {
            _token: csrf_token,
            id: id,
            texto: $scope.respostas[$id],
            enviar_membros: $scope.memberList,
            tipo: tipo,
            modal_enviar: $scope.modal_enviar
        };

        $.post(site_url + 'site/artwork/services/package/responder', params, function (data) {
            //myAlert('Resposta adicionada com sucesso.');
            $('.chat-comment-box').hide();
            $scope.exibe_responder[$id] = '';
            $scope.respostas[$id] = '';
            $scope.enviar_respostas[$id] = '';
            $scope.carregar_ticket(1);
            $scope.interval_tickets_start();

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });

        $scope.modal_enviar = false;
    };

    $scope.cancelar_ticket_service = function () {

        $scope.show_load();
        $scope.interval_tickets_stop();
        
        params = {
            _token: csrf_token,
            id: $scope.ticket.id,
            motivo: $scope.cancelamento_descricao,
            acao: 'cancelar'
        };

        $.post(site_url + 'site/artwork/services/package/ticket/pausarCancelar', params, function (data) {
            myAlert('Ticket cancelado com sucesso.', 'success');
            $scope.cancelamento_descricao = '';
            $scope.carregar_ticket(1);
            $scope.interval_tickets_start();
            $scope.tab = 1;
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });

        $scope.cancelamento_descricao = '';
    };

    $scope.cancelar_ticket_confirm = function () {
        var message = 'Cancelar o ticket é uma ação irreversível, tem certeza que deseja cancelar?';
        
        $.confirm({
            icon: 'icon icon-question',
            title: 'Confirmar',
            content: message,
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close icon-cross',
            useBootstrap: false,
            boxWidth: '850px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon icon-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        $scope.cancelar_ticket();
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon icon-ban-circle"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {}
                }
            },
            onOpenBefore: function () {
                //$('body').addClass('jconfirm-overlay');
                $('body').addClass('no-scroll');
                //$('.master').addClass('blurred');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                //$('body').removeClass('jconfirm-overlay');
                $('body').removeClass('no-scroll');
                //$('.master').removeClass('blurred');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    }

    $scope.cancelar_ticket = function () {
        $ngConfirm({
            useBootstrap: false,
            boxWidth: '850px',
            title: '',
            contentUrl: '/view/modal_cancelar_ticket.html',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'custom-alert-close',
            backgroundDismiss: true,
            escapeKey: 'Sair',
            buttons: {
                Sair: {
                    keys: ['esc'],
                    text: '<i class="icon icon-ban-circle"></i>Sair',
                    btnClass: 'custom-ng-btn-confirm-cancel',
                    action: function (Sair) {
                    }
                },
                Concluir: {
                    keys: ['enter', 'space'],
                    text: '<i class="icon icon-check"></i>Concluir',
                    btnClass: 'custom-ng-btn-confirm-save',
                    action: function (Concluir) {
                        var descricao = $scope.cancelamento_descricao; 

                        if (descricao === '') {
                            $('#cancelamento_descricao').addClass('input-error');

                            myAlert('Informe o motivo do cancelamento do ticket.', 'warning');
                            
                            return false;
                        }

                        $('#cancelamento_descricao').removeClass('input-error');
                        
                        $scope.cancelar_ticket_service();
                    }
                }
            }
        });

    };
    
    $scope.pausar_ticket_service = function () {

        $scope.show_load();
        $scope.interval_tickets_stop();
        
        params = {
            _token: csrf_token,
            id: $scope.ticket.id,
            acao: 'pausar'
        };

        $.post(site_url + 'site/artwork/services/package/ticket/pausarCancelar', params, function (data) {
            myAlert('Ticket pausado com sucesso.', 'success');
            $scope.carregar_ticket(1);
            $scope.interval_tickets_start();
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });

        $scope.cancelamento_descricao = '';
    };
    
    $scope.pausar_ticket = function() {
        var message = 'Deseja pausar este ticket?';

        $.confirm({
            icon: 'icon icon-question',
            title: 'Confirmar',
            content: message,
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close icon-cross',
            useBootstrap: false,
            boxWidth: '850px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon icon-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        $scope.pausar_ticket_service();
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon icon-ban-circle"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {}
                }
            },
            onOpenBefore: function () {
                //$('body').addClass('jconfirm-overlay');
                $('body').addClass('no-scroll');
                //$('.master').addClass('blurred');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                //$('body').removeClass('jconfirm-overlay');
                $('body').removeClass('no-scroll');
                //$('.master').removeClass('blurred');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };
    
    $scope.despausar_ticket_service = function () {

        $scope.show_load();
        $scope.interval_tickets_stop();
        
        params = {
            _token: csrf_token,
            id: $scope.ticket.id,
            acao: 'despausar'
        };

        $.post(site_url + 'site/artwork/services/package/ticket/pausarCancelar', params, function (data) {
            myAlert('Ticket reativado com sucesso.', 'success');
            $scope.carregar_ticket(1);
            $scope.interval_tickets_start();
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });

        $scope.cancelamento_descricao = '';
    };

    $scope.despausar_ticket = function() {
        var message = 'Deseja reativar este ticket?';

        $.confirm({
            icon: 'icon icon-question',
            title: 'Confirmar',
            content: message,
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close icon-cross',
            useBootstrap: false,
            boxWidth: '850px',
            buttons: {
                Ok: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon icon-check"></i>Ok</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Ok) {
                        $scope.despausar_ticket_service();
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon icon-ban-circle"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {}
                }
            },
            onOpenBefore: function () {
                //$('body').addClass('jconfirm-overlay');
                $('body').addClass('no-scroll');
                //$('.master').addClass('blurred');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                //$('body').removeClass('jconfirm-overlay');
                $('body').removeClass('no-scroll');
                //$('.master').removeClass('blurred');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };
        
    $scope.carregar_ticket(1);
    $scope.carregar_fornecedores(1);
    $scope.interval_tickets_start();
        
    $scope.$on('$destroy', function () {
        $scope.interval_tickets_stop();
    });
});