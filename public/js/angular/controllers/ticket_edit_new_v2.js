var site_url = '/';
var app = angular.module('app', [
    'cp.ngConfirm', 
    'ngSanitize',
    'selectize', 
    '720kb.tooltips', 
    'ngAnimate', 
    'compartilharItemMod'
]);

app.directive('autoHeight', function () {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element = element[0];
            
            var resize = function() {
                element.style.height = 'auto';
                element.style.height = (element.scrollHeight)+'px';
            };
            
            element.addEventListener('change', resize, false);
            element.addEventListener('cut', resize, false);
            element.addEventListener('paste', resize, false);
            element.addEventListener('drop', resize, false);
            element.addEventListener('keydown', resize, false);

            setTimeout(resize, 0);
        }
    }
});

app.controller('TicketEditCtrl', function ($scope, $http, $rootScope, $timeout, $ngConfirm) {
    $scope.myConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecionar membro',
        maxItems: 1,
        render: { 
            option: function(data, escape){
                if(data.ignore === true) {
                    return '<div style="display:none"></div>';
                } else {
                    return '<div class="item">'+ data.nome +'</div>';
                }
            }
        },
    };
    
    $scope.userLoginConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        placeholder: 'Selecionar membro',
        maxItems: 1,
        render: { 
            option: function(data, escape){
                return '<div class="item">'+ data.nome +'</div>';
            }
        }
    };

    $scope.projetoConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        maxItems: 1,
        render: {
            option: function (data, escape) {
                return '<div class="item">' + data.nome + '</div>';
            }
        },
        onDropdownOpen: function () {}
    };

    $scope.projetos = [{ 
        id: 0, nome: 'Novo Projeto' 
    }];

    $scope.carregar_projetos = function () {
        $scope.show_load();

        $.get(site_url + 'site/artwork/services/package/projetos', function (data) {
            $scope.projetos = data;

            $scope.$apply();
            $scope.hide_load();

        }, 'json');
    };
    
    $scope.solicitacoes = [];
    $scope.tipos = [];
    $scope.ticket = {};

    $scope.participantes = [];

    $scope.membros = {};
    $scope.cbMembershipProfile = [];
    $scope.cbMembershipDelete = [];
    $scope.cbMembershipEquipeDelete = [];
    $scope.prosseguir = '';
    $scope.itens = {};

    $scope.show_load = function () {
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        $('.lds-css').hide();
    };

    $scope.verificar_participante = function()
    {
        var n = 0;
        
        for(i = 0; i < $scope.participantes.length; i++) {
            
            if($scope.participantes[i].id_usuario == $scope.add_participante && ($scope.participantes[i].id_perfil == $scope.add_perfil || $scope.cbMembershipProfile[i] ==  $scope.add_perfil)) {
                
                n++;
                
            }
            
        }
                
        if (n > 0) {
            
            return true;
            
        } else {
            
            return false;
            
        }
        
    };

    $scope.adicionar_membro = function () {
        
        if ($scope.verificar_participante())
        {
            
            $('.input-fields-group:last .select-control .selectize-input').addClass('input-error');
            $('.input-fields-group:last .select-control select').addClass('input-error');
            
            myAlert('O Perfil de Participação selecionado já foi atribuído à este Participante neste ticket.', 'warning');
            
            return false;
        }
        
        if ($scope.add_participante && $scope.add_perfil) {
            var template = {
                'id_usuario': $scope.add_participante,
                'id_perfil': $scope.add_perfil,
                'recebe_email': $scope.add_email,
                'equipe_membros': []
            };

            $scope.participantes.push(template);

            $scope.add_participante = '';
            $scope.add_perfil = '';
            $scope.add_email = '';
            
            $('.input-fields-group:last .select-control .selectize-input').removeClass('has-error');
            $('.input-fields-group:last .select-control select').removeClass('has-error');
        } else {
            $('.input-fields-group:last .select-control .selectize-input').addClass('has-error');
            $('.input-fields-group:last .select-control select').addClass('has-error');
            
            myAlert('Selecione o Participante e seu respectivo Perfil de Participação neste ticket.', 'warning');
        }
    };

    $scope.adicionar_membro_equipe = function (k)
    {
        
        if ($scope.participantes[k].add_participante && $scope.cbMembershipProfile[k])
        {
            var template = {'id_usuario': $scope.participantes[k].add_participante, 'recebe_email': ''};

            $scope.participantes[k].equipe_membros.push(template);

            $scope.participantes[k].add_participante = '';
            $scope.participantes[k].adiciona_equipe = 0;
            $scope.carregar_membros();
        
            $('.team-member-group .select-control .selectize-input').removeClass('has-error');
            $('.team-member-group .select-control select').removeClass('has-error');
        } else {
            
            $('.team-member-group .select-control .selectize-input').addClass('has-error');
            $('.team-member-group .select-control select').addClass('has-error');
        
            myAlert('Selecione o Participante e seu respectivo Perfil de Participação neste ticket.', 'warning');
            
        }
    };

    $scope.remover_membro = function (k) {

        if ($scope.participantes[k].id){
            $scope.cbMembershipDelete.push($scope.participantes[k].id);
        }

        $scope.cbMembershipProfile.splice(parseInt(k), 1);

        $scope.participantes.splice(parseInt(k), 1);

        $scope.carregar_membros();
    };

    $scope.remover_membro_equipe = function (k, e)
    {
        if ($scope.participantes[k].equipe_membros[e].id){
            $scope.cbMembershipEquipeDelete.push($scope.participantes[k].equipe_membros[e].id);
        }

        $scope.participantes[k].equipe_membros.splice(parseInt(e), 1);

        $scope.carregar_membros();
    };

    $scope.carregar_membros = function () {
        $scope.show_load();

        params = {
            _token: csrf_token
        };

        $.post(site_url + 'site/artwork/services/package/members', params, function (data) {
            $scope.membros = data;

            $scope.$apply();
            $scope.hide_load();
            
        }, 'json');
    };

    $scope.carregar_participantes = function () {
        $scope.show_load();
        
        $.get(site_url + 'site/artwork/services/package/ticket_members/' + id_ticket, function (data) {
            if (data.length) {
                $scope.participantes = data;

                angular.forEach($scope.participantes, function (participante, key) {
                    $scope.cbMembershipProfile.push('' + participante.id_perfil + '');
                });

                $scope.$apply();

                $scope.carregar_membros();

                $scope.hide_load();
            }
        }, 'json');

    };

    $scope.carregar_ticket = function () {
        $scope.show_load();

        $.get(site_url + 'site/artwork/services/package/ticket/' + id_ticket, function (data) {
                $scope.ticket = data;

                $scope.solicitacoes = (data.ciclo_atual.solicitacoes) ? data.ciclo_atual.solicitacoes : [];

                $scope.$apply();
                $scope.hide_load();

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro!');
                $scope.hide_load();
            });
    };

    $scope.carregar_tipos = function () {
        $.get(site_url + 'site/artwork/services/package/request/types', function (data) {
            $scope.tipos = data;

            $scope.$apply();

        }, 'json');
    };

    $scope.adicionar = function () {
        if ($scope.add_tipo && $scope.add_descricao) {
            var template = {
                'id_tipo': $scope.add_tipo,
                'descricao': $scope.add_descricao,
                'descricao_texto': $scope.add_descricao
            };
            
            $scope.solicitacoes.push(template);

            $scope.add_tipo = '';
            $scope.add_descricao = '';

            angular.element('.add-instruction-group textarea').height(0);
        
            $('.request-type .input-fields-group:last select').removeClass('has-error');
            $('.request-type .input-fields-group:last textarea').css('has-error');
        } else {
            $('.request-type .input-fields-group:last select').addClass('has-error');
            $('.request-type .input-fields-group:last textarea').addClass('has-error');
            
            myAlert('Selecione o tipo de alteração e infome a descrição específica.', 'warning');
        }
    };

    $scope.remover = function (k) {
        $scope.solicitacoes.splice(parseInt(k), 1);
    };

    $scope.validar = function ($event) {
        if (edicao) {
            if (angular.element('#txtMainDescription').val() === '') {
                angular.element('#txtMainDescription').focus();
                angular.element('#txtMainDescription').addClass('has-error');
                
                myAlert('Descreva resumidamente o conteúdo deste ticket em Descrição Geral.', 'Atenção', 'warning');

                $event.preventDefault();

                return false;
            
            } else if ($scope.cbProjeto === '0' && angular.element('#tfNomeProjeto').val() === '') {
                angular.element('#tfNomeProjeto').focus();
                angular.element('#tfNomeProjeto').addClass('has-error');
                
                myAlert('Digite o nome do projeto.', 'Atenção', 'warning');

                $event.preventDefault();

                return false;

            } else if (angular.element('#tfArtSendDate').val() === '') {
                angular.element('#txtMainDescription').removeClass('has-error');
                angular.element('#tfArtSendDate').focus();
                angular.element('#tfArtSendDate').addClass('has-error');
                
                myAlert('Informe a data para que a arte seja enviada para aprovação.', 'warning');
            
                $event.preventDefault();

                return false;
            } else if (angular.element('#tfDeadlineDate').val() === '') {
                angular.element('#tfArtSendDate').removeClass('has-error');
                angular.element('#tfDeadlineDate').focus();
                angular.element('#tfDeadlineDate').addClass('has-error');
                
                myAlert('Informe a data que a arte deve ser enviada para o fornecedor.', 'warning');
            
                $event.preventDefault();

                return false;
            } else if ($scope.solicitacoes.length === 0) {
                if (!$scope.add_tipo) {
                    angular.element('#tfDeadlineDate').removeClass('has-error');
                    angular.element('#alterationTypeWrap .input-fields-group select:last').focus();
                    angular.element('#alterationTypeWrap .input-fields-group select:last').addClass('has-error');
                    
                    myAlert('Selecione o Tipo de Alteração.', 'warning');
                    
                    $event.preventDefault();
    
                    return false;
                } else if (!$scope.add_descricao) {
                    angular.element('#alterationTypeWrap .input-fields-group select').removeClass('has-error');
                    angular.element('#alterationTypeWrap .input-fields-group textarea:last').focus();
                    angular.element('#alterationTypeWrap .input-fields-group textarea:last').addClass('has-error');
                    
                    myAlert('Descreva com detalhes a alteração.', 'warning');
                    
                    $event.preventDefault();
    
                    return false;
                }
            } else if ($scope.solicitacoes.length > 0) {
                if ($scope.add_tipo || $scope.add_descricao) {
                    angular.element('#alterationTypeWrap .input-fields-group select:last').removeClass('has-error');
                    angular.element('#alterationTypeWrap .input-fields-group textarea:last').removeClass('has-error');
                    angular.element('#alterationTypeWrap .add-instruction-col button').addClass('has-error');
                    
                    myAlert('Clique em Adicionar para incluir a alteração no ticket.', 'warning');
                    
                    $event.preventDefault();
    
                    return false;
                }
            }
        }
    };

    $scope.carregar_projetos();
    $scope.carregar_tipos();
    $scope.carregar_ticket();
    //$scope.carregar_membros();
    //$scope.carregar_participantes();
});