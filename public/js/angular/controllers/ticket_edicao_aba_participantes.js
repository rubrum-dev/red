var participantes = angular.module("abaParticipantesMod", []);

participantes.controller('abaParticipantesCtrl', function (
        $scope,
        $http,
        $rootScope
    ) {
    $rootScope.statechange = true;
    
    $scope.verificar_participante_old = function () {
        var n = 0;

        for (i = 0; i < $scope.participantes.length; i++) {

            if ($scope.participantes[i].id_usuario == $scope.add_participante && ($scope.participantes[i].id_perfil == $scope.add_perfil || $scope.cbMembershipProfile[i] == $scope.add_perfil)) {

                n++;

            }

        }

        if (n > 0) {

            return true;

        } else {

            return false;

        }

    };

    $scope.verificar_participante_service = function(id_usuario, id_perfil)
    {
        var n = 0;
                
        for(i = 0; i < $scope.participantes.length; i++)
        {
            if($scope.participantes[i].id_usuario == id_usuario && ($scope.participantes[i].id_perfil == id_perfil || $scope.cbMembershipProfile[i] ==  id_perfil))
            {
                n++;
            }
            
            if ($scope.participantes[i].equipe_membros)
            {
                for(m = 0; m < $scope.participantes[i].equipe_membros.length; m++)
                {
                    if($scope.participantes[i].equipe_membros[m].id_usuario == id_usuario && ($scope.participantes[i].id_perfil == id_perfil || $scope.cbMembershipProfile[i] ==  id_perfil))
                    {
                        n++;
                    }
                }
            }
        }
                
        if (n > 0)
        {
            return n;
        }
        else
        {
            return false;
        }
    };

    $scope.selecionar_primeiro = function(id_usuario, id_perfil)
    {
        verifica = $scope.verificar_participante_service(id_usuario, id_perfil);
        
        if (verifica > 1)
        {
            myAlert('O Perfil de Participação selecionado já foi atribuído à este Participante neste ticket.', 'warning');
            
            $scope.participantes[0].id_perfil = '';
            $scope.cbMembershipProfile[0] = '';
            
            return false;
        }
    };

    $scope.adicionar_membro = function () {
        if ($scope.verificar_participante_service($scope.add_participante, $scope.add_perfil))
        {
            myAlert('O Perfil de Participação selecionado já foi atribuído à este Participante neste ticket.', 'warning');
            
            return false;
        }
                
        if ($scope.add_participante && $scope.add_perfil) {
            var template = {
                'id_usuario': $scope.add_participante,
                'id_perfil': $scope.add_perfil,
                'recebe_email': $scope.add_email,
                'equipe_membros': []
            };

            $scope.participantes.push(template);

            $scope.add_participante = '';
            $scope.add_perfil = '';
            $scope.add_email = '';

            $('.workflow-members-group .selectize-input').removeClass('has-error');
            $('.workflow-members-group .select-control select').removeClass('has-error');

            $scope.has_changed = true;
        } else {
            $('.workflow-members-group .selectize-input').addClass('has-error');
            $('.workflow-members-group .select-control select').addClass('has-error');

            myAlert('Selecione o Participante e seu respectivo Perfil de Participação neste ticket.', 'warning');
        }
    };

    $scope.adicionar_membro_equipe = function (k) {
        if ($scope.verificar_participante_service($scope.participantes[k].add_participante, $scope.cbMembershipProfile[k]))
        {
            myAlert('O Perfil de Participação selecionado já foi atribuído à este Participante neste ticket.', 'warning');
            
            return false;
        }
        
        if ($scope.participantes[k].add_participante && $scope.cbMembershipProfile[k]) {
            var template = {'id_usuario': $scope.participantes[k].add_participante, 'recebe_email': ''};

            $scope.participantes[k].equipe_membros.push(template);
            
            $scope.participantes[k].add_participante = '';
            $scope.participantes[k].adiciona_equipe = 0;

            $('.workflow-member-edit-sub .selectize-input').removeClass('has-error');
            $('.workflow-member-edit-sub .select-control select').removeClass('has-error');

            $scope.has_changed = true;
        } else {
            $('.workflow-member-edit-sub .selectize-input').addClass('has-error');
            $('.workflow-member-edit-sub .select-control select').addClass('has-error');

            myAlert('Selecione o Participante e seu respectivo Perfil de Participação neste ticket.', 'warning');
        }
    };

    $scope.remover_membro = function (k) {
        
        if ($scope.participantes[k].id) {
            $scope.cbMembershipDelete.push($scope.participantes[k].id);
        }

        $scope.cbMembershipProfile.splice(parseInt(k), 1);
        
        $scope.participantes.splice(parseInt(k), 1);
        
        $scope.has_changed = true;
    };

    $scope.remover_membro_equipe = function (k, e) {
        if ($scope.participantes[k].equipe_membros[e].id) {
            $scope.cbMembershipEquipeDelete.push($scope.participantes[k].equipe_membros[e].id);
            $scope.has_changed = true;
        }

        $scope.participantes[k].equipe_membros.splice(parseInt(e), 1);
    };

    $scope.carregar_membros = function () {
        params = {
            _token: csrf_token
        };
        
        //$rootScope.loading = true;
        $rootScope.statechange = false;

        $.post(site_url + 'site/artwork/services/package/members', params, function (data) {
            $scope.membros = data;

            $scope.$apply();
            
            $rootScope.statechange = true;
        }, 'json');
    };
    
    
    
    
    $scope.carregar_participantes_old = function () {
        //$rootScope.loading = true;
        
        $scope.cbMembershipProfile = [];
                
        $.get(site_url + 'site/artwork/services/package/ticket_members/' + id_ticket, function (data) {
            if (data.length) {
                $scope.participantes = data;
                
                $scope.$apply();
                
                $scope.hide_load();
                
                //$rootScope.loading = false;
                
                $scope.has_changed = false;
            }
        }, 'json');

    };
    
    $scope.carregar_participantes = function () {
        
        //$rootScope.loading = true;
        $rootScope.statechange = false;
        
        $scope.participantes = [];
        
        $http.get(site_url + 'site/artwork/services/package/ticket_members/' + id_ticket).success(function(data){
            
            $scope.participantes = data;
            
            $scope.hide_load();
            
            //$rootScope.loading = false;
            $rootScope.statechange = true;
                
            $scope.has_changed = false;
            
        });

    };

    $scope.salvar_participantes_service = function () {
        params = {
            _token: csrf_token,
            id_ticket: id_ticket,
            add_participante: $scope.participantes,
            cbMembershipProfile: $scope.cbMembershipProfile,
            membros_excluidos: $scope.cbMembershipDelete,
            equipe_membros_excluidos: $scope.cbMembershipEquipeDelete
        };
        
        $scope.show_load();

        $.post(site_url + 'site/artwork/services/package/ticketSalvarParticipantes', params, function (data) {
                $.confirm({
                    icon: 'icon fa fa-check',
                    title: 'Sucesso',
                    content: 'Alterações salvas com sucesso',
                    closeIcon: true,
                    boxWidth: '910px',
                    useBootstrap: false,
                    closeIconClass: 'jconfirm-btn-close fa fa-times',
                    buttons: {
                        Entendi: {
                            keys: ['enter', 'space'],
                            text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                            btnClass: 'btn-confirm',
                            action: function (Entendi) {}
                        }
                    },
                    onOpenBefore: function () {
                        //$('body').addClass('jconfirm-overlay');
                        $('body').addClass('no-scroll');
                        //$('.master').addClass('blurred');
                        $('.jconfirm').css('overflow', 'auto');
                        $('.jconfirm').overlayScrollbars({
                            scrollbars: {
                                autoHide: 'move'
                            }
                        });

                        $scope.hide_load();
                    },
                    onDestroy: function () {
                        //$('body').removeClass('jconfirm-overlay');
                        $('body').removeClass('no-scroll');
                        //$('.master').removeClass('blurred');
                        $('.jconfirm').css('overflow', 'hidden');
                        
                        $scope.cbMembershipDelete = [];
                        $scope.cbMembershipEquipeDelete = [];
                        $scope.carregar_participantes();
                        $scope.carregar_ticket(false, 'force');
                    }
                });
            })
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
            });
    };

    $scope.salvar_participantes = function () {
        $.confirm({
            icon: 'icon fa fa-question',
            title: 'Confirmar',
            content: 'Deseja salvar alterações?',
            closeIcon: true,
            boxWidth: '910px',
            useBootstrap: false,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn-confirm',
                    action: function (Todos) {
                        $scope.salvar_participantes_service();
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel'
                }
            },
            onOpenBefore: function () {
                //$('body').addClass('jconfirm-overlay');
                $('body').addClass('no-scroll');
                //$('.master').addClass('blurred');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                //$('body').removeClass('jconfirm-overlay');
                $('body').removeClass('no-scroll');
                //$('.master').removeClass('blurred');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };

    $scope.adicionar_equipe_toggle = function (participante, participantes, index) {
        for (i = 0; i < participantes.length; i++) {
            if (participantes[i].adiciona_equipe && i !== index) {
                participantes[i].adiciona_equipe = false;
            }
        }
        participante.adiciona_equipe=!participante.adiciona_equipe;
    };
    
    $rootScope.change_tab = function($event) {
        if(angular.element('#participantes .btn-group').is(':visible')) {
            $.confirm({
                icon: 'fa fa-question',
                title: 'Confirmar',
                content: 'Você incluiu novos participantes no ticket, deseja salvar?',
                closeIcon: true,
                closeIconClass: 'jconfirm-btn-close fa fa-times',
                useBootstrap: false,
                boxWidth: '910px',
                buttons: {
                    Salvar: {
                        keys: ['enter', 'space'],
                        text: '<span><i class="icon fa fa-check"></i>Salvar</span>',
                        btnClass: 'btn btn-confirm',
                        action: function (Confirmar) {
                            $scope.salvar_participantes_service();
                        }
                    },
                    Cancelar: {
                        keys: ['esc'],
                        text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                        btnClass: 'btn btn-cancel',
                        action: function (Cancelar) {
                            $scope.carregar_participantes();
                        }
                    }
                },
                
                onOpenBefore: function () {
                    //$('body').addClass('jconfirm-overlay');
                    $('body').addClass('no-scroll');
                    //$('.master').addClass('blurred');
                    $('.jconfirm').css('overflow', 'auto');
                    $('.jconfirm').overlayScrollbars({
                        scrollbars: {
                            autoHide: 'move'
                        }
                    });
                },
                onClose: function () {
                    //$('body').removeClass('jconfirm-overlay');
                    $('body').removeClass('no-scroll');
                    //$('.master').removeClass('blurred');
                    $('.jconfirm').css('overflow', 'hidden');
                }
            });

            $event.preventDefault();
        }
    };

    $scope.carregar_participantes();
});