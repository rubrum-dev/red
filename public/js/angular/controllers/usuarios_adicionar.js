var site_url = '/';
var app = angular.module('app', ['ui.mask']);

app.directive('maskChange', function () {
    return {
        restrict: 'A',
        scope: {
            maskChange: "="
        },
        require: '?ngModel',
        link: function (scope, elem, attrs, ngModel) {

            var novoTel, flag = false,
                val;

            elem.off('keyup');
            elem.on('keyup', function (ev) {

                if (/^\d+$/.test(ev.key) || ev.key == 'Backspace' || ev.key == 'Delete') {

                    novoTel = String(ngModel.$viewValue).replace(/[\(\)\_\-/\s]/g, '')

                    if (novoTel.length == 10 && !flag) {
                        flag = true;
                        scope.maskChange = "(99)9999-9999";
                        scope.$apply();
                    } else if (novoTel.length == 10 && flag) {
                        flag = false;
                        scope.maskChange = "(99)9?9999-9999";

                        scope.$apply();
                        ngModel.$viewValue += ev.key
                        ngModel.$render();

                    } else if (novoTel.length < 10) {
                        flag = false;
                    }
                }
            });
        }

    };
});

app.controller('UsuariosAdicionarCtrl', function ($scope) {
    $scope.perfisAdicionados = [];
    $scope.perfis = [];
    $scope.nome = '';
    $scope.sobrenome = '';
    $scope.email = '';
    $scope.telefone = '';
    $scope.phoneMask = '(99)9999-9999';
    $scope.id_adm_empresa = '';
    $scope.empresa_nome = '';
    $scope.id_departamento = '';
    $scope.departamento_nome = '';
    $scope.id_cargo = '';
    $scope.cargo_nome = '';
    $scope.addPerfil = '';
    $scope.perfil_adm_sistema_1 = false;
    $scope.perfil_adm_sistema_3 = false;
    $scope.artwIsAllSelected = false;
    $scope.tcdrwIsAllSelected = false;

    $scope.uncheck_ativar_usuario = function () {
        if (usuariosValidos >= limiteUsuarios && cadastrar) {
            $.alert({
                icon: 'icon fa fa-exclamation',
                title: 'Atenção',
                content: 'Para ativar o usuário é necessário desativar um outro usuário que está ativo.',
                closeIcon: true,
                boxWidth: '910px',
                scrollToPreviousElement: false,
                useBootstrap: false,
                closeIconClass: 'jconfirm-btn-close fa fa-times',
                buttons: {
                    Entendi: {
                        keys: ['esc', 'enter', 'space'],
                        text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                        btnClass: 'btn-confirm',
                        action: function (Entendi) {
                        }
                    },
                },
                onOpenBefore: function () {
                    $('body').addClass('no-scroll');
                    $('.jconfirm').css('overflow', 'auto');
                    $('.jconfirm').overlayScrollbars({
                        scrollbars: {
                            autoHide: 'move'
                        }
                    });
                },
                onClose: function () {
                    $('body').removeClass('no-scroll');
                    $('.jconfirm').css('overflow', 'hidden');

                    $scope.$apply(function() {
                        $scope.perfil_adm_sistema_3 = false;
                    });
                }
            });
        }
    };

    $scope.superAdmToggle = function() {
        if (!$scope.perfil_adm_sistema_2 && !$scope.perfil_adm_sistema_5 && !$scope.perfil_adm_sistema_6 && !$scope.perfil_adm_sistema_7 && !$scope.perfil_adm_sistema_4) {
            $scope.perfil_adm_sistema_1 = false; 
        }

        if ($scope.perfil_adm_sistema_2 && $scope.perfil_adm_sistema_5 && $scope.perfil_adm_sistema_6 && $scope.perfil_adm_sistema_7 && $scope.perfil_adm_sistema_4) {
            $scope.perfil_adm_sistema_1 = true; 
        }
    };

    $scope.admToggleAll = function() {
        if($scope.perfil_adm_sistema_1 === true) {
            $scope.perfil_adm_sistema_2 = true;
            $scope.perfil_adm_sistema_5 = true;
            $scope.perfil_adm_sistema_6 = true;
            $scope.perfil_adm_sistema_7 = true;
            $scope.perfil_adm_sistema_4 = true;
        } else {
            $scope.perfil_adm_sistema_2 = false;
            $scope.perfil_adm_sistema_5 = false;
            $scope.perfil_adm_sistema_6 = false;
            $scope.perfil_adm_sistema_7 = false;
            $scope.perfil_adm_sistema_4 = false;
        }
    };
    
    $scope.artwToggleAll = function() {
        if($scope.artwIsAllSelected) {
            $scope.art_permissao_1 = true;
            $scope.art_permissao_6 = true;
            $scope.art_permissao_4 = true;
            $scope.art_permissao_9 = true;
            $scope.art_permissao_3 = true;
            $scope.art_permissao_2 = true;
            $scope.art_permissao_8 = true;
            $scope.art_permissao_7 = true;
            $scope.art_permissao_10 = true;
            $scope.art_permissao_11 = true;
            $scope.work_permissao_1 = true;
            $scope.art_permissao_12 = true;
        } else {
            $scope.art_permissao_1 = false;
            $scope.art_permissao_6 = false;
            $scope.art_permissao_4 = false;
            $scope.art_permissao_9 = false;
            $scope.art_permissao_3 = false;
            $scope.art_permissao_2 = false;
            $scope.art_permissao_8 = false;
            $scope.art_permissao_7 = false;
            $scope.work_permissao_1 = false;
            $scope.art_permissao_10 = false;
            $scope.art_permissao_11 = false;
            $scope.art_permissao_12 = false;
        }
    }

    $scope.techDrawToggleAll = function() {
        if($scope.tcdrwIsAllSelected) {
            $scope.tech_permissao_1 = true;
            $scope.tech_permissao_2 = true;
            $scope.tech_permissao_3 = true;
        } else {
            $scope.tech_permissao_1 = false;
            $scope.tech_permissao_2 = false;
            $scope.tech_permissao_3 = false;
        }
    }

    $scope.adSmartToggleAll = function() {
        if($scope.adsIsAllSelected) {
            $scope.ads_permissao_1 = true;
            $scope.ads_permissao_2 = true;
            $scope.ads_permissao_3 = true;
            $scope.ads_permissao_4 = true;
            $scope.ads_permissao_5 = true;
            $scope.ads_permissao_6 = true;
        } else {
            $scope.ads_permissao_1 = false;
            $scope.ads_permissao_2 = false;
            $scope.ads_permissao_3 = false;
            $scope.ads_permissao_4 = false;
            $scope.ads_permissao_5 = false;
            $scope.ads_permissao_6 = false;
        }
    }

    $scope.dashboardToggleAll = function() {
        if($scope.dashboardIsAllSelected) {
            $scope.dashboard_permissao_1 = true;
            $scope.dashboard_permissao_2 = true;
        } else {
            $scope.dashboard_permissao_1 = false;
            $scope.dashboard_permissao_2 = false;
        }
    }

    $scope.show_load = function () {
        $('.lds-css').show();
    }

    $scope.hide_load = function () {
        $('.lds-css').hide();
    }

    $scope.add_usuario_submit = function ($event) {
        var modulesPermission = document.querySelectorAll("[name='permissao[]']:checked");
        var emailRegex = /^[a-z0-9][a-z0-9-_\.]+@([a-z]|[a-z0-9]?[a-z0-9-]+[a-z0-9])\.[a-z0-9]{2,10}(?:\.[a-z]{2,10})?$/;

        if ($scope.nome === '') {
            angular.element('#nome').addClass('has-error');
            myAlert('Nome é obrigatório.', 'warning');
            $event.preventDefault();
            return false;
        } else {
            angular.element('#nome').removeClass('has-error');
        }

        if ($scope.sobrenome === '') {
            angular.element('#sobrenome').addClass('has-error');
            myAlert('Sobrenome é obrigatório.', 'warning');
            $event.preventDefault();
            return false;
        } else {
            angular.element('#sobrenome').removeClass('has-error');
        }

        if ($scope.email === '' || !emailRegex.test($scope.email)) {
            angular.element('#email').addClass('has-error');
            myAlert('E-mail é obrigatório.', 'warning');
            $event.preventDefault();
            return false;
        } else {
            angular.element('#email').removeClass('has-error');
        }

        if ($scope.id_adm_empresa === '') {
            angular.element('#id_adm_empresa').addClass('has-error');
            myAlert('Selecione a Empresa.', 'warning');
            $event.preventDefault();
            return false;
        } else {
            angular.element('#id_adm_empresa').removeClass('has-error');
        }

        if ($scope.id_departamento === '') {
            angular.element('#id_departamento').addClass('has-error');
            myAlert('Selecione o Departamento.', 'warning');
            $event.preventDefault();
            return false;
        } else {
            angular.element('#id_departamento').removeClass('has-error');
        }

        if ($scope.id_departamento === '0' && $scope.departamento_nome === '') {
            angular.element('#departamento_nome').addClass('has-error');
            myAlert('Digite o nome do Novo Departamento.', 'warning');
            $event.preventDefault();
            return false;
        } else {
            angular.element('#departamento_nome').removeClass('has-error');
        }
        
        if ($scope.id_cargo === '0' && $scope.cargo_nome === '') {
            angular.element('#cargo_nome').addClass('has-error');
            myAlert('Digite o nome do Novo Cargo.', 'warning');
            $event.preventDefault();
            return false;
        } else {
            angular.element('#cargo_nome').removeClass('has-error');
        }

        if (!$scope.addPerfil && $scope.perfisAdicionados.length === 0) {
            angular.element('#addPerfil').addClass('has-error');
            myAlert('Selecione um Perfil de Acesso.', 'warning');
            $event.preventDefault();
            return false;
        }

        if ($scope.addPerfil) {
            angular.element('#addPerfil').addClass('has-error');
            myAlert('Adicione o Perfil de Acesso clicando em Adicionar.', 'warning');
            $event.preventDefault();
            return false;
        } else {
            angular.element('#addPerfil').removeClass('has-error');
        }

        if (modulesPermission.length === 0) {
            myAlert('Marque ao menos uma Permissão aos Módulos.', 'warning');
            $event.preventDefault();
            return false;
        } else {
            if (!$scope.perfil_adm_sistema_3 && usuariosValidos >= limiteUsuarios && cadastrar) {
                $scope.usuario_ativo_alert()
                $event.preventDefault();
            }
        }
    };

    $scope.usuario_ativo_alert = function() {
        $.alert({
            icon: 'icon fa fa-exclamation',
            title: 'Atenção',
            content: 'O limite de usuários Ativos da sua conta foi atingido, este usuário será cadastrado como Inativo. Libere o limite de usuários Ativos marcando usuários pouco usados como Inativos ou contrate mais usuários no seu plano.',
            closeIcon: true,
            boxWidth: '910px',
            scrollToPreviousElement: false,
            useBootstrap: false,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            escapeKey: 'Entendi',
            buttons: {
                Entendi: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                    btnClass: 'btn-confirm',
                    action: function (Entendi) {
                        $('form').unbind('submit').submit();
                    }
                }
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };

    $scope.adicionar_perfil = function () {
        if ($scope.addPerfil) {
            var template = {
                'id_perfil': $scope.perfis[$scope.addPerfil].id,
                'nome': $scope.perfis[$scope.addPerfil].nome
            };

            $scope.perfisAdicionados.push(template);
        
            $scope.addPerfil = '';
            
            $('#addPerfil').removeClass('has-error');
        } else {
            $('#addPerfil').addClass('has-error');
            myAlert('Selecione o perfil de acesso.', 'warning')
        }
    };

    $scope.remover_perfil = function (perfil) {
        $scope.perfisAdicionados.splice(perfil, 1);
    };

    $scope.carregar_perfis = function () {
        $scope.show_load();

        $.get(site_url + 'site/artwork/services/package/perfis', function (data) {
            $scope.perfis = data;

            $scope.$apply();
            $scope.hide_load();

        }, 'json');
    };

    $scope.carregar_perfis();

});