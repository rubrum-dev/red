var compartilharItem = angular.module('compartilharItemMod',
    [
        'cp.ngConfirm',
        '720kb.tooltips',
        'ngAnimate',
        'selectize'
    ]);

compartilharItem.controller('compartilharItemCtrl', function (
    $scope,
    $timeout,
    $rootScope,
    $ngConfirm,
    $http,
    $window
) {
    $scope.nome_caracteristica = '';

    $rootScope.artwSharedItemsConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome_original',
        delimiter: '|',
        placeholder: 'Selecione as embalagens',
        dropdownParent: null,
        searchField: ['nome_original'],
        scrollDuration: 0,
        maxItems: 1,
        options: '',
        onInitialize: function (selectize) {
            $('.selectize-dropdown').addClass('direction-up');
        }
    };
    
    $scope.artwItemSharing = function (id_item) {
        
        $scope.show_load();
        
        $scope.itemSelecionado = [
            {'embalagens':[]}
        ];
        
        $http.get(site_url + 'site/artwork/services/package/getItem/' + id_item + '/1').success(function(data){
            
            $scope.itemSelecionado = data;
            
            $scope.hide_load();
            
            $scope.artwItemSharingModal(id_item);
            
        });
        
    };

    $scope.artwItemSharingModal = function (id_item) {
        var customLoadingHTML = '<div class="line-wobble"></div>';
        
        if ($scope.itemSelecionado.permite_edicao) {
            
            var artwItemSharingModal = $ngConfirm({
                title: '',
                contentUrl: '/view/modal_compartilhar_item.html?v=3.3',
                scope: $scope,
                closeIcon: true,
                closeIconClass: 'ng-confirm-btn-close fa fa-times',
                escapeKey: 'Cancelar',
                buttons: {

                    Cancelar: {
                        text: 'Cancelar',
                        btnClass: 'btn-control btn-alt'
                    },
                    Concluir: {
                        text: 'Confirmar',
                        btnClass: 'btn-control btn-positive',
                        keys: ['enter', 'space'],
                        action: function (Concluir) {
                            if (!$scope.nome_caracteristica) {
                                $('#caracteristica').addClass('has-error');
                                myAlert('Dê um nome de característica.', 'warning');
                                return false;
                            } else if ($scope.itemSelecionado.compartilhadas.length == 0) {
                                $('#caracteristica').removeClass('has-error');
                                $('#embalagens_compartilhadas_add').find('.selectize-input').addClass('has-error');
                                myAlert('Adicione uma embalagem.', 'warning');
                                return false;
                            } else {
                                $('#embalagens_compartilhadas_add').find('.selectize-input').removeClass('has-error');
                                $scope.artwFileSharingConfirm();
                            }
                        }
                    }

                },
                backgroundDismiss: false,
                boxWidth: '910px',
                useBootstrap: false,
                onReady: function() {
                    $timeout(function () {
                        $('.ng-confirm-box .line-wobble').remove();
                        $('.ng-confirm-content').css('visibility', 'visible');
                        $('.ng-confirm-buttons').show();
                    }, new Date().getMilliseconds())
                },
                onOpenBefore: function () {
                    setTimeout(function () {
                        $('.ng-confirm .ng-confirm-scrollpane').overlayScrollbars({
                            scrollbars: {
                                autoHide: 'move'
                            }
                        });
                    }, 0);
                },
                onOpen: function () {
                    $('.ng-confirm-content').css('visibility', 'hidden');
                    $('.ng-confirm-buttons').hide();
                    $('.ng-confirm-box').append(customLoadingHTML);
                },
                onScopeReady: function () {},
                onDestroy: function () {
                    $rootScope.embalagens_compartilhadas_add = '';
                }
            });
            
        } else {
            
            var artwItemSharingModal = $ngConfirm({
                title: '',
                contentUrl: '/view/modal_compartilhar_item.html?v=2.0',
                scope: $scope,
                closeIcon: true,
                closeIconClass: 'ng-confirm-btn-close fa fa-times',
                backgroundDismiss: false,
                boxWidth: '910px',
                useBootstrap: false,
                onReady: function() {
                    $timeout(function () {
                        $('.ng-confirm-box .line-wobble').remove();
                        $('.ng-confirm-content').css('visibility', 'visible');
                    }, new Date().getMilliseconds())
                },
                onScopeReady: function () {},
                onOpenBefore: function () {
                    setTimeout(function () {
                        $('.ng-confirm .ng-confirm-scrollpane').overlayScrollbars({
                            scrollbars: {
                                autoHide: 'move'
                            }
                        });
                    }, 0);
                },
                onOpen: function () {
                    $('.ng-confirm-box').append(customLoadingHTML);
                    $('.ng-confirm-content').css('visibility', 'hidden');
                    $('.ng-confirm-content-pane').addClass('padding-bottom-30');
                    $('.ng-confirm-buttons').remove();
                },
                onDestroy: function () {
                    $rootScope.embalagens_compartilhadas_add = '';
                }
            });
            
        }
        
        
    };

    $scope.artwFileSharingConfirm = function (id_item) {
        var msgSharingConfirm = '';
        
        if(!$scope.itemSelecionado.compartilhado) {
            msgSharingConfirm = 'O ato de transformar um Item em Item Compartilhado é irreversível, tem certeza que deseja confirmar?'
        } else {
            msgSharingConfirm = 'Deseja confirmar?'
        }

        $.confirm({
            animation: 'scale',
            closeAnimation: 'scale',
            animationSpeed: 300,
            title: '',
            animateFromElement: false,
            content: '<div class="custom-alert-contents confirmation">' +
                '   <span class="icon fa fa-question"></span>' +
                '   <div class="custom-alert-heading">' +
                '       <h4>Confirmar</h4>' +
                '   </div>' +
                '   <p>'+ msgSharingConfirm +'</p>' +
                '</div>',
            closeIcon: true,
            scrollToPreviousElement: false,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            boxWidth: '910px',
            useBootstrap: false,
            escapeKey: 'Cancelar',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn-confirm',
                    action: function (Confirmar) {

                        params = {
                            _token: csrf_token,
                            id_item: $scope.itemSelecionado.id,
                            nome_caracteristica: $scope.nome_caracteristica,
                            compartilhados: $scope.itemSelecionado.compartilhadas
                        };
                        
                        $.post(site_url + 'site/artwork/services/package/postCompartilharItem', params, function (data) {
                            $.alert({
                                animation: 'scale',
                                closeAnimation: 'scale',
                                animationSpeed: 300,
                                title: '',
                                animateFromElement: false,
                                content: '<div class="custom-alert-contents success">' +
                                    '   <span class="icon fa fa-check"></span>' +
                                    '   <div class="custom-alert-heading">' +
                                    '       <h4>Sucesso</h4>' +
                                    '   </div>' +
                                    '   <p>Item compartilhado com sucesso.</p>' +
                                    '</div>',
                                escapeKey: 'Entendi',
                                buttons: {
                                    Entendi: {
                                        keys: ['esc', 'enter', 'space'],
                                        text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                                        btnClass: 'btn-confirm my-alert-confirm',
                                        action: function (Confirmar) {
                                            $scope.nome_caracteristica = '';
                                            $window.location.reload();
                                        }
                                    }
                                },
                                onOpenBefore: function () {
                                    //$('body').addClass('jconfirm-overlay');
                                    $('body').addClass('no-scroll');
                                    $('.jconfirm-content').css('marginLeft', '0px');
                                    $('.jconfirm').css('overflow', 'auto');
                                }
                            });
                        }, 'json')
                        .fail(function (data) {
                            myAlert('Ocorreu um erro.', 'error');
                            return;
                        });
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn-cancel'
                }
            },
            onOpenBefore: function () {
                //$('body').addClass('jconfirm-overlay');
                $('body').addClass('no-scroll');
                $('.jconfirm-content').css('marginLeft', '0px');
                $('.jconfirm').css('overflow', 'auto');
            },
            onOpen: function () {
                //$('.master').addClass('blurred');
            },
            onClose: function () {
                //$('body').removeClass('jconfirm-overlay');
                $('body').removeClass('no-scroll');
                //$('.master').removeClass('blurred');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };
    
    $scope.getById = function (id, array) {

        for (i = 0; i < array.length; i++) {
            if (array[i].id == id) {
                return array[i];
            }
        }
    };

    $scope.compartilhado_add = function () {
        if ($scope.getById($scope.embalagens_compartilhadas_add, $scope.itemSelecionado.compartilhadas)) {
            $('#embalagens_compartilhadas_add').find('.selectize-input').addClass('has-error');
            myAlert('Está embalagem já foi adicionada.', 'warning');
            return false;
        }
        
        if (!$scope.embalagens_compartilhadas_add || $scope.embalagens_compartilhadas_add == 'undefined') {
            $('#embalagens_compartilhadas_add').find('.selectize-input').addClass('has-error');
            myAlert('Não foi selecionada uma embalagem.', 'warning');
            return false;
        } else {
            
            var item = $scope.getById($scope.embalagens_compartilhadas_add, $scope.itemSelecionado.embalagens);
            
            $rootScope.embalagens_compartilhadas_add = '';
            $scope.itemSelecionado.compartilhadas.push(item);
        }
    };

    $scope.compartilhado_excluir = function (index) {
        $scope.itemSelecionado.compartilhadas.splice(index, 1);
    };
});