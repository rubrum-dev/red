var site_url = '/';
var app = angular.module('app', [
    'cp.ngConfirm', 
    'selectize', 
    '720kb.tooltips', 
    'ngAnimate',
    'grupoTrabalhoMod',
    'compartilharItemMod'
]);

app.controller('TicketMembersCtrl', function ($scope, $http, $rootScope, $timeout, $ngConfirm)
{

    $scope.myConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome', 'empresa', 'departamento'],
        placeholder: 'Selecionar participante',
        maxItems: 1,
        options: '',
        render: { 
            option: function(data, escape){
                if(data.ignore === true) {
                    return '<div style="display:none"></div>';
                } else if(data.departamento !== '') {
                    return '<div>' +
                    '   <span class="heading">' + escape(data.nome) + '</span>' +
                    '   <span class="caption">' + escape(data.empresa) + '</span>' + 
                    '   <span class="slash">' + escape('/') + '</span>' +
                    '   <span class="caption">' + escape(data.departamento) + '</span>' +
                    '</div>';
                } else {
                    return '<div>' +
                    '   <span class="heading">' + escape(data.nome) + '</span>' +
                    '   <span class="caption">' + escape(data.empresa) + '</span>' + 
                    '   <span class="slash">' + escape('/') + '</span>' +
                    '   <span class="caption">' + 'Departamento' + '</span>' +
                    '</div>';
                }
            }
        },
        onInitialize: function (selectize) {
            $('.selectize-dropdown').addClass('direction-up');  
        },
        onDropdownOpen: function () {
            this.clear();
            this.focus();
        }
    };

    $rootScope.participantes = [
        {'id_usuario': id_usuario, 'id_perfil': '', 'recebe_email': '', 'adiciona_equipe': 0, 'add_participante': '', 'equipe_membros': []}
    ];

    $scope.id_ticket = id_ticket;
    $scope.membros = {};
    $scope.cbMembershipProfile = [];
    $scope.prosseguir = '';
    $scope.itens = {};
    $scope.ticket = {};

    $scope.show_load = function()
    {
        $('.lds-css').show();
    };

    $scope.hide_load = function()
    {
        $('.lds-css').hide();
    };
    
    $scope.verificar_participante_old = function(id_usuario)
    {
        var n = 0;
                
        for(i = 0; i < $scope.participantes.length; i++) {
            
            if($scope.participantes[i].id_usuario == $scope.add_participante && ($scope.participantes[i].id_perfil == $scope.add_perfil || $scope.cbMembershipProfile[i] ==  $scope.add_perfil)) {
                
                n++;
                
            }
            
        }
                
        if (n > 0) {
            
            return true;
            
        } else {
            
            return false;
            
        }
        
    };

    $scope.verificar_membro_old = function()
    {
        for (i = 0; i < $scope.participantes.length; i++) {
            
            if ($scope.participantes[i].add_participante == $scope.participantes[i].id_usuario && !$scope.membro_selecionado) {
                return true;
            }
            
            for (j = 0; j < $scope.participantes[i].equipe_membros.length; j++) {
                
                if ($scope.participantes[i].add_participante == $scope.participantes[i].equipe_membros[j].id_usuario && !$scope.membro_selecionado) {
                    return true;
                }
            
                if ($scope.add_participante == $scope.participantes[i].equipe_membros[j].id_usuario && !$scope.membro_selecionado) {
                    return true;
                }
            }
        }
    };
    
    $scope.verificar_participante_service = function(id_usuario, id_perfil)
    {
        var n = 0;
                
        for(i = 0; i < $scope.participantes.length; i++)
        {
            if($scope.participantes[i].id_usuario == id_usuario && ($scope.participantes[i].id_perfil == id_perfil || $scope.cbMembershipProfile[i] ==  id_perfil))
            {
                n++;
            }
            
            if ($scope.participantes[i].equipe_membros)
            {
                for(m = 0; m < $scope.participantes[i].equipe_membros.length; m++)
                {
                    if($scope.participantes[i].equipe_membros[m].id_usuario == id_usuario && ($scope.participantes[i].id_perfil == id_perfil || $scope.cbMembershipProfile[i] ==  id_perfil))
                    {
                        n++;
                    }
                }
            }
        }
                
        if (n > 0)
        {
            return n;
        }
        else
        {
            return false;
        }
    };
    
    $scope.selecionar_primeiro = function(id_usuario, id_perfil, id_perfil_antigo)
    {
        verifica = $scope.verificar_participante_service(id_usuario, id_perfil);
        
        if (verifica > 1)
        {
            myAlert('O Perfil de Participação selecionado já foi atribuído à este Participante neste ticket.', 'warning');
            
            $scope.participantes[0].id_perfil = id_perfil_antigo;
            $scope.cbMembershipProfile[0] = id_perfil_antigo;
            
            return false;
        }
    };
    
    $scope.adicionar = function ()
    {
        if ($scope.verificar_participante_service($scope.add_participante, $scope.add_perfil))
        {
            myAlert('O Perfil de Participação selecionado já foi atribuído à este Participante neste ticket.', 'warning');
            
            return false;
        }
        
        if ($scope.add_participante && $scope.add_perfil)
        {
            var template = {'id_usuario': $scope.add_participante, 'id_perfil': $scope.add_perfil, 'recebe_email': $scope.add_email, 'equipe_membros': []};
                        
            $scope.participantes.push(template);

            $scope.add_participante = '';
            $scope.add_perfil = '';
            $scope.add_email = '';
            
            $('.input-fields-group:last .selectize-control .selectize-input').removeClass('has-error');
            $('.input-fields-group:last .select-control select').removeClass('has-error');
        } else {
            $('.input-fields-group:last .selectize-control .selectize-input').addClass('has-error');
            $('.input-fields-group:last .select-control select').addClass('has-error');
            
            myAlert('Selecione o Participante e seu respectivo Perfil de Participação neste ticket.', 'warning');
        }
    };
    
    $scope.adicionar_membro = function (participante, k)
    {       
        if ($scope.verificar_participante_service($scope.participantes[k].add_participante, $scope.cbMembershipProfile[k]))
        {
            myAlert('O Perfil de Participação selecionado já foi atribuído à este Participante neste ticket.', 'warning');
            
            return false;
        }
        
        if ($scope.participantes[k].add_participante && $scope.cbMembershipProfile[k])
        {
            var template = {'id_usuario': $scope.participantes[k].add_participante, 'recebe_email': ''};
                        
            $scope.participantes[k].equipe_membros.push(template);
                        
            $scope.participantes[k].add_participante = '';

            $scope.participantes[k].adiciona_equipe = false;
                        
            $('.membership-team-col .selectize-control .selectize-input').removeClass('has-error');
        } else {
            $('.membership-team-col .selectize-control .selectize-input').addClass('has-error');
            
            myAlert('Selecione o Participante e seu respectivo Perfil de Participação neste ticket.', 'warning');
        }
    };

    $scope.remover = function (k)
    {
        $scope.cbMembershipProfile.splice(parseInt(k), 1);

        $scope.participantes.splice(parseInt(k), 1);
    };

    $scope.remover_membro = function (k, e)
    {
        $scope.participantes[k].equipe_membros.splice(parseInt(e), 1);
    };
    
    $scope.adicionar_equipe_toggle = function(participante, participantes, index) 
    {
        for (i = 0; i < participantes.length; i++) {
            if (participantes[i].adiciona_equipe && i !== index) {
                participantes[i].adiciona_equipe = false;
            }
        }
                    
        participante.adiciona_equipe=!participante.adiciona_equipe;
    }

    $scope.carregar_ticket = function ()
    {
        $scope.show_load();

        $.get(site_url + 'site/artwork/services/package/ticket/' + id_ticket, function (data)
        {
            $scope.ticket = data;

            $scope.solicitacoes = (data.ciclo_atual.solicitacoes) ? data.ciclo_atual.solicitacoes : [];

            $scope.$apply();
            $scope.hide_load();

        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro!', 'error');
                    $scope.hide_load();
                });
    };

    $scope.carregar_membros = function ()
    {
        $scope.show_load();

        params = {
            _token: csrf_token
        };
        
        $.post(site_url + 'site/artwork/services/package/members', params, function (data)
        {
            $scope.membros = data;
            $scope.$apply();
            $scope.hide_load();
        }, 'json');
    };

    $scope.carregar_participantes = function ()
    {
        $scope.show_load();

        $.get(site_url + 'site/artwork/services/package/ticket_members/' + id_ticket, function (data)
        {
            if (data.length)
            {
                $scope.participantes = data;

                angular.forEach($scope.participantes, function (participante, key) {
                    $scope.cbMembershipProfile.push('' + participante.id_perfil + '');
                });

                $scope.$apply();

                $scope.carregar_membros();

                $scope.hide_load();
            }

        }, 'json');

    };

    $scope.carregar_membros();
    $scope.carregar_participantes();
    $scope.carregar_ticket();
});
