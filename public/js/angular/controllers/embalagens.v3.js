var site_url = '/';
var app = angular.module('app', [
    'embalagensResolveLoader',
    'cp.ngConfirm', 
    '720kb.tooltips', 
    'ngAnimate', 
    'selectize', 
    'ngFileUpload', 
    'compartilharItemMod'
]);

var customLoadingHTML = '<div class="line-wobble"></div>';

app.animation('.slide-toggle-js', function () {
    return {
        enter: function (element, done) {
            $(element).slideDown(200, function ($scope) {
                done();
            });
        },
        leave: function (element, done) {
            $(element).slideUp(200, function () {
                done();
            });
        }
    };
});

app.filter('filterById', function () {
    return function (input, id) {
        var out = [];
        for (var i = 0; i < input.length; i++) {
            if (input[i].id == id)
                out.push(input[i]);
        }
        return out;
    };
});

app.directive('validFile', function validFile($parse) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            ngModelCtrl.$validators.validFile = function () {
                element.on('change', function () {
                    var value = element.val(),
                        model = $parse(attrs.ngModel),
                        modelSetter = model.assign;

                    scope.uploadedFileType = null;

                    if (!value) {
                        modelSetter(scope, '');
                    } else {
                        var ext = value.substring(value.lastIndexOf('.') + 1).toLowerCase();

                        if (attrs.validFile.indexOf(ext) !== -1) {
                            scope.uploadedFileType = ext;
                            modelSetter(scope, element[0].files[0]);
                        } else {
                            myAlert('Formato de arquivo inválido.', 'warning');
                            scope.uploadedFileType = 'other';
                            modelSetter(scope, '');
                            scope.limparCamposUpload();
                        }
                    }
                });
            };
        }
    };
});

app.controller('EmbalagensCtrl', function ($scope, $http, $rootScope, $compile, $timeout, $ngConfirm, Upload) {
    $scope.tab = 1;
    $scope.dados = {};
    $scope.listagem = {};
    $scope.embalagens = {};
    $scope.exibir_listagem = false;
    $scope.itens = {};
    $scope.pdf = {};
    $scope.arte = {};
    $scope.files_pdf = [];
    $scope.files_arte = [];
    $scope.activeTabs = [];
    $scope.listagem.stateJS = [];
    $scope.exibe_loading = false;
    $scope.chkEnviaNotificacao = true;
    
    $scope.show_load = function () {
        $('.lds-css').show();
        $scope.exibe_loading = true;
    };

    $scope.hide_load = function () {
        $('.lds-css').hide();
        $scope.exibe_loading = false;
    };

    $scope.cancelar_versao_confirm = function (id_ticket) {
        var message = 'Cancelar a versão irá mover o ticket da versão vigente para a aba Cancelados em Gerenciar Tickets e transformar a versão anterior na versão vigente de mercado. Isso é uma ação irreversível, deseja confirmar?';
        
        $.confirm({
            icon: 'fa fa-question',
            title: 'Confirmar',
            content: $compile('<p class="margin-bottom-20">'+ message +'</p>' +
            '<label class="switch-control d-inline-flex flex-align-center">' +
            '    <input type="checkbox" name="chkEnviaNotificacao" id="chkEnviaNotificacao" ng-checked="chkEnviaNotificacao" ng-model="chkEnviaNotificacao" />' +
            '    <span class="switch-toggle pull-left"></span>' +
            '    <strong class="d-block txt-regular txt-default margin-left-10">Enviar um e-mail para os Participantes e Fornecedores avisando que a esta versão foi cancelada.</span>' +
            '</label>')($scope),
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            useBootstrap: false,
            boxWidth: '910px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        $scope.cancelar_versao(id_ticket);
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {}
                }
            },
            onOpen: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };

    $scope.cancelar_versao = function (id_ticket) {
        $ngConfirm({
            useBootstrap: false,
            boxWidth: '910px',
            title: '',
            contentUrl: '/view/modal_cancelar_versao.html',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'ng-confirm-btn-close fa fa-times',
            backgroundDismiss: true,
            escapeKey: 'Sair',
            buttons: {
                Sair: {
                    keys: ['esc'],
                    text: 'Sair',
                    btnClass: 'btn-control btn-alt',
                    action: function (Sair) {}
                },
                Concluir: {
                    keys: ['enter', 'space'],
                    text: 'Concluir',
                    btnClass: 'btn-control btn-positive',
                    action: function (Concluir) {
                        var descricao = $scope.cancelamento_descricao;

                        if (descricao === '') {
                            $('#cancelamento_descricao').addClass('has-error');

                            myAlert('Informe o motivo do cancelamento do ticket.', 'warning');

                            return false;
                        }

                        $('#cancelamento_descricao').removeClass('has-error');

                        $scope.cancelar_versao_service(id_ticket);
                    }
                }
            },
            onReady: function() {
                $timeout(function () {
                    $('.ng-confirm-box .line-wobble').remove();
                    $('.ng-confirm-content').css('visibility', 'visible');
                    $('.ng-confirm-buttons').show();
                }, new Date().getMilliseconds())
            },
            onOpenBefore: function () {
                setTimeout(function () {
                    $('.ng-confirm .ng-confirm-scrollpane').overlayScrollbars({
                        scrollbars: {
                            autoHide: 'move'
                        }
                    });
                }, 0);
            },
            onOpen: function() {
                $('.ng-confirm-content').css('visibility', 'hidden');
                $('.ng-confirm-buttons').hide();
                $('.ng-confirm-box').append(customLoadingHTML);
            }
        });

    };

    $scope.cancelar_versao_service = function (id_ticket) {
        //$scope.show_load();
        //$scope.interval_tickets_stop();

        $scope.show_load();

        params = {
            _token: csrf_token,
            id: id_ticket,
            motivo: $scope.cancelamento_descricao,
            envia_notificacao: $scope.chkEnviaNotificacao,
            tipo: 'versao',
            acao: 'cancelar'
        };

        $.post(site_url + 'site/artwork/services/package/ticket/pausarCancelar', params, function (data) {
            myAlert('Versão cancelada com sucesso. O ticket dessa versão foi movido para a aba Cancelados em Gerenciar Tickets e a versão anterior agora é a versão vigente de mercado.', 'success');
            $scope.cancelamento_descricao = '';
            $scope.carregar_embalagens();
            //$scope.carregar_ticket(1);
            //$scope.interval_tickets_start();
            location.href = '#/tarefas';
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });

        $scope.cancelamento_descricao = '';
    };

    $scope.getItemTipoById = function (id) {

        for (i = 0; i < $scope.embalagens.item_tipos.length; i++) {
            if ($scope.embalagens.item_tipos[i].id == id) {
                return $scope.embalagens.item_tipos[i];
            }
        }
    };

    $scope.exibeEditarItem = function (embalagem, item) {

        for (i = 0; i < embalagem.itens.length; i++) {
            if (embalagem.itens[i].exibir_editar_item == true) {
                embalagem.itens[i].exibir_editar_item = false;
            }
        }

        embalagem.exibir_formulario = false;
        item.exibir_editar_item = !item.exibir_editar_item;

    };

    $scope.exibeNovoItem = function (embalagem, acao) {

        if (acao) {

            for (i = 0; i < embalagem.itens.length; i++) {

                if (embalagem.itens[i].exibir_editar_item == true) {
                    embalagem.itens[i].exibir_editar_item = false;
                }

            }

        }

        embalagem.exibir_formulario = acao;

    };

    $scope.alterar_item = function (item) {

        if (item.id_tipo == item.id_tipo_original) {
            item.exibir_editar_item = false;
            return;
        }

        $scope.show_load();

        params = {
            _token: csrf_token,
            id: item.id,
            id_tipo: item.id_tipo
        };

        $.post(site_url + 'site/artwork/services/package/alterarItem', params, function (data) {

                if (data.error) {
                    myAlert(data.message, 'error');
                    $scope.hide_load();
                } else {
                    myAlert(data.message, 'success');

                    item.nome = $scope.getItemTipoById(item.id_tipo).nome;
                    item.exibir_editar_item = false;
                    item.id_tipo_original = item.id_tipo;

                    $scope.$apply();

                    $scope.hide_load();
                }

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });

    };

    $scope.descontinuar_item_service = function (item) {

        $scope.show_load();

        params = {
            _token: csrf_token,
            id_item: item.id
        };

        $.post(site_url + 'site/artwork/services/package/descontinuarItem', params, function (data) {

                if (data.error) {
                    myAlert(data.message, 'error');
                    $scope.hide_load();
                } else {
                    myAlert(data.message, 'success');
                    $scope.carregar_embalagens();
                    $scope.hide_load();
                }

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });

    };

    $scope.descontinuar_item = function (item) {
        $.confirm({
            title: '',
            content: '<div class="custom-alert-contents confirmation">' +
                '   <span class="icon fa fa-question"></span>' +
                '   <div class="custom-alert-heading">' +
                '       <h4>Confirmar</h4>' +
                '   </div>' +
                '   <p>Deseja descontinuar esse item?</p>' +
                '</div>',
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            boxWidth: '910px',
            useBootstrap: false,
            escapeKey: 'Cancelar',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn-confirm',
                    action: function (Confirmar) {
                        $scope.descontinuar_item_service(item);
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn-cancel'
                }
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm-content').css('marginLeft', '0px');
                $('.jconfirm').css('overflow', 'auto');
                $('.my-alert-confirm').focus();
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
                $('.my-alert-confirm').blur();
            }
        });
    };

    $scope.retornar_item_service = function (item, id_item) {

        $scope.show_load();

        if (item) {

            id_item = item.id;

        }

        params = {
            _token: csrf_token,
            id_item: id_item
        };

        $.post(site_url + 'site/artwork/services/package/retornarItem', params, function (data) {

                if (data.error) {
                    myAlert(data.message, 'error');
                    $scope.hide_load();
                } else {
                    myAlert(data.message, 'success');
                    $scope.carregar_embalagens();
                    $scope.hide_load();
                }

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });

    };

    $scope.retornar_item = function (item) {
        $.confirm({
            title: '',
            content: '<div class="custom-alert-contents confirmation">' +
                '   <span class="icon fa fa-question"></span>' +
                '   <div class="custom-alert-heading">' +
                '       <h4>Confirmar</h4>' +
                '   </div>' +
                '   <p>Deseja retornar esse item para mercado?</p>' +
                '</div>',
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            escapeKey: 'Cancelar',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn-confirm my-alert-confirm',
                    action: function (Confirmar) {
                        $scope.retornar_item_service(item);
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn-cancel'
                }
            },
            boxWidth: '910px',
            useBootstrap: false,
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm-content').css('marginLeft', '0px');
                $('.jconfirm').css('overflow', 'auto');
                $('.my-alert-confirm').focus();
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
                $('.my-alert-confirm').blur();
            }
        });
    };

    $scope.excluir_item_service = function (item) {

        $scope.show_load();

        params = {
            _token: csrf_token,
            id_item: item.id
        };

        $.post(site_url + 'site/artwork/services/package/excluirItem', params, function (data) {

                if (data.error) {
                    myAlert(data.message, 'error');
                    $scope.hide_load();
                } else {
                    myAlert(data.message, 'success');
                    $scope.carregar_embalagens();
                    $scope.hide_load();
                }

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });

    };

    $scope.excluir_item = function (item) {
        $.confirm({
            title: '',
            content: '<div class="custom-alert-contents confirmation">' +
                '   <span class="icon fa fa-question"></span>' +
                '   <div class="custom-alert-heading">' +
                '       <h4>Confirmar</h4>' +
                '   </div>' +
                '   <p>Deseja excluir esse item?</p>' +
                '</div>',
            closeIcon: true,
            boxWidth: '910px',
            useBootstrap: false,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            escapeKey: 'Cancelar',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn-confirm my-alert-confirm',
                    action: function (Confirmar) {
                        $scope.excluir_item_service(item);
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn-cancel'
                }
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm-content').css('marginLeft', '0px');
                $('.jconfirm').css('overflow', 'auto');
                $('.my-alert-confirm').focus();
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
                $('.my-alert-confirm').blur();
            }
        });
    };

    $scope.excluir_embalagem_service = function (embalagem) {

        $scope.show_load();

        params = {
            _token: csrf_token,
            id_embalagem: embalagem.id,
        };

        $.post(site_url + 'site/artwork/services/package/excluirEmbalagem', params, function (data) {

                if (data.error) {
                    myAlert(data.message, 'error');
                    $scope.hide_load();
                } else {
                    $scope.carregar_embalagens();
                    myAlert(data.message, 'success');
                }

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });

    };

    $scope.excluir_embalagem = function (embalagem) {
        $.confirm({
            title: '',
            content: '<div class="custom-alert-contents confirmation">' +
                '   <span class="icon fa fa-question"></span>' +
                '   <div class="custom-alert-heading">' +
                '       <h4>Confirmar</h4>' +
                '   </div>' +
                '   <p>Deseja excluir essa embalagem?</p>' +
                '</div>',
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            boxWidth: '910px',
            useBootstrap: false,
            escapeKey: 'Cancelar',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn-confirm my-alert-confirm',
                    action: function (Confirmar) {
                        $scope.excluir_embalagem_service(embalagem);
                    }
                },
                Cancelar: {
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn-cancel'
                }
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm-content').css('marginLeft', '0px');
                $('.jconfirm').css('overflow', 'auto');
                $('.my-alert-confirm').focus();
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
                $('.my-alert-confirm').blur();
            }
        });
    };

    $scope.criar_item = function (embalagem) {

        var id_item = embalagem.id_item_tipo_novo;

        if (!id_item) {
            $('body').find('.artwork-container .input-required').addClass('has-error');
            $scope.hide_load();
            myAlert('Selecione o Item.', 'warning');

            return;
        }

        $scope.show_load();

        // Verifica se já existe
        params = {
            _token: csrf_token,
            id: embalagem.id,
            id_tipo: embalagem.id_item_tipo_novo
        };

        $.post(site_url + 'site/artwork/services/package/checkItem', params, function (data) {

                if (data.error) {

                    if (data.status == 1) {

                        $('body').find('.package-container .input-required').addClass('input-error');
                        myAlert('Item já existente.', 'error');
                        $scope.hide_load();
                        return;

                    } else {

                        // Tela de Confirmação
                        $.confirm({
                            icon: 'fa fa-question',
                            title: 'Confirmar',
                            content: 'Este item já encontra-se cadastrado nessa embalagem, porém está descontinuado. Deseja retorná-lo para mercado?',
                            closeIcon: true,
                            closeIconClass: 'jconfirm-btn-close fa fa-times',
                            escapeKey: 'Cancelar',
                            buttons: {
                                Confirmar: {
                                    keys: ['enter', 'space'],
                                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                                    btnClass: 'btn-confirm',
                                    action: function (Confirmar) {
                                        $scope.retornar_item_service('', data.id_item);
                                    }
                                },
                                Cancelar: {
                                    keys: ['esc'],
                                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                                    btnClass: 'btn-cancel'
                                }
                            },
                            boxWidth: '910px',
                            useBootstrap: false,

                            onOpenBefore: function () {
                                $('body').addClass('no-scroll');
                                $('.jconfirm').css('overflow', 'auto');
                                $('.my-alert-confirm').focus();
                                $('.jconfirm').overlayScrollbars({
                                    scrollbars: {
                                        autoHide: 'move'
                                    }
                                });
                            },
                            onClose: function () {
                                $('body').removeClass('no-scroll');
                                $('.jconfirm').css('overflow', 'hidden');
                                $('.my-alert-confirm').blur();

                                $scope.hide_load();
                            }
                        });
                    }

                } else {

                    $scope.artwItemCreate = $ngConfirm({
                        animation: 'scale',
                        closeAnimation: 'scale',
                        boxWidth: '910px',
                        useBootstrap: false,
                        title: '',
                        contentUrl: '/view/modal_cadastrar_item.html',
                        scope: $scope,
                        closeIcon: true,
                        closeIconClass: 'ng-confirm-btn-close fa fa-times',
                        onReady: function() {
                            $timeout(function () {
                                $('.ng-confirm-box .line-wobble').remove();
                                $('.ng-confirm-content').css('visibility', 'visible');
                                $('.ng-confirm-buttons').show();
                            }, new Date().getMilliseconds());
                        },
                        onOpenBefore: function () {
                            setTimeout(function () {
                                $('.ng-confirm .ng-confirm-scrollpane').overlayScrollbars({
                                    scrollbars: {
                                        autoHide: 'move'
                                    }
                                });
                            }, 0);
                        },
                        onOpen: function () {
                            $('.ng-confirm-content').css('visibility', 'hidden');
                            $('body').addClass('no-scroll');
                            $('.ng-confirm-buttons').addClass('no-padding');
                            $('.ng-confirm-buttons').hide();
                            $('.ng-confirm-box').append(customLoadingHTML);
                        },
                        onClose: function () {
                            $('body').removeClass('no-scroll');
                        },
                        onScopeReady: function () {
                            $('body').find('.artwork-container .input-required').removeClass('has-error');
                            $scope.nome_original = embalagem.nome_original;
                            $scope.id_item_tipo_novo = embalagem.id_item_tipo_novo;
                            $scope.id_variacao_novo = embalagem.id;
                        }
                    });

                    $scope.hide_load();

                }

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
                return;
            });

    };

    $scope.carregar_embalagens = function () {
        var url = site_url + 'site/artwork/services/package/getEmbalagens/' + id_produto;
        
        $scope.show_load();
        
        $.ajax({
            url: url,
            type: "get",
            contentType: false,
            processData: false,
            start_time: new Date().getTime(),
            success: function (data, status) {
                if (data.error) {
                    myAlert(data.message, 'error');
                } else {
                    $scope.embalagens = data;
                    $scope.exibir_listagem = true;
                    $scope.$apply();
                    $scope.hide_load();
                }
                
                $timeout(function () {
                    $rootScope.loaded =  true;                
                }, new Date().getTime() - this.start_time);
            },
            error: function () {
                $scope.hide_load();
                myAlert('Ocorreu um erro.', 'error');
            }
        });
    };

    $scope.isOpenTab = function (tab) {
        if ($scope.activeTabs.indexOf(tab) > -1) {
            return true;
        } else {
            return false;
        }
    };

    $scope.openTab = function (tab) {
        if ($scope.isOpenTab(tab)) {
            $scope.activeTabs.splice($scope.activeTabs.indexOf(tab), 1);
        } else {
            $scope.activeTabs.push(tab);
        }
    };

    $scope.setUploadPdf = function (element) {
        $scope.$apply(function (scope) {
            // Turn the FileList object into an Array
            for (var i = 0; i < element.files.length; i++) {
                $scope.files_pdf.push(element.files[i]);
            }
        });
    };

    $scope.setUploadArte = function (element) {
        $scope.$apply(function (scope) {
            // Turn the FileList object into an Array
            for (var i = 0; i < element.files.length; i++) {
                $scope.files_arte.push(element.files[i]);
            }
        });
    };

    $scope.salvar_arquivosOLD = function () {
        $scope.show_load();

        if (!$scope.files_pdf.length) {
            $scope.hide_load();
            myAlert('Anexe o PDF.', 'warning');
            return;
        }

        if ($scope.id_item_tipo_novo) {
            $scope.dados.id_variacao = $scope.id_variacao_novo;
        }

        var formData = new FormData();

        if ($scope.dados.id_item) {
            formData.append("id_item", $scope.dados.id_item);
        }

        if ($scope.id_item_tipo_novo) {
            formData.append("id_item_tipo", $scope.id_item_tipo_novo);
        }

        formData.append("id_variacao", $scope.dados.id_variacao);


        for (var i = 0; i < $scope.files_pdf.length; i++) {
            formData.append("pdf", $scope.files_pdf[i]);
        }

        for (var i = 0; i < $scope.files_arte.length; i++) {
            formData.append("arte", $scope.files_arte[i]);
        }

        $.ajax({
            url: site_url + 'site/artwork/services/package/postArtes',
            headers: {
                'x-csrf-token': csrf_token
            },
            type: 'POST',
            data: formData,
            success: function (data) {
                var ok = $.alert({
                    title: '',
                    content: '<div class="alert-box-content success clearfix">' +
                        '   <span class="icon fa fa-check"></span>' +
                        '   <div class="alert-box-body">' +
                        '       <div class="heading">' +
                        '           <h4>Sucesso</h4>' +
                        '       </div>' +
                        '       <p class="text">Arquivos enviados com sucesso</p>' +
                        '   </div>' +
                        '</div>',
                    boxWidth: '910px',
                    useBootstrap: false,
                    closeIcon: true,
                    closeIconClass: 'jconfirm-btn-close fa fa-times',
                    scrollToPreviousElement: false,
                    escapeKey: true,
                    buttons: {
                        Entendi: {
                            keys: ['enter', 'space', 'esc'],
                            text: 'Entendi',
                            btnClass: 'btn-confirm my-alert-confirm',
                            action: function (Confirmar) {
                                $scope.carregar_embalagens();
                                $scope.files_pdf = [];
                                $scope.files_arte = [];
                            }
                        }
                    },
                    closeIcon: true,
                    scrollToPreviousElement: false,
                    closeIconClass: 'jconfirm-btn-close icon-cross',
                    boxWidth: '910px',
                    useBootstrap: false,
                    onOpenBefore: function () {
                        $('body').addClass('no-scroll');
                        $('.jconfirm').css('overflow', 'auto');
                        $('.jconfirm').overlayScrollbars({
                            scrollbars: {
                                autoHide: 'move'
                            }
                        });
                    },
                    onClose: function () {
                        $('body').removeClass('no-scroll');
                        $('.jconfirm').css('overflow', 'hidden');

                        $scope.carregar_embalagens();
                    }
                });
            },
            error: function () {
                $scope.hide_load();
                myAlert('Ocorreu um erro.', 'error');
            },
            contentType: false,
            processData: false
        });
    };

    $scope.salvar_arquivos = function () {
        if ($scope.dados.pdf) {

            if (!$scope.pdf && !$scope.arte) {
                $('body').find('.artw-files-upload .files-pdf-attach .files-control-fake').addClass('has-error');
                myAlert('Anexe um Arquivo.', 'warning');
                return;
            }

            if ($scope.pdf && $scope.pdf.size > 52428800) {
                $('body').find('.artw-files-upload .files-pdf-attach .files-control-fake').addClass('has-error');
                myAlert('O tamanho máximo permitido para um arquivo é 50 MB.', 'warning');
                return;
            }

            if ($scope.pdf) {

                $scope.upload = $scope.pdf;

            } else {

                $scope.upload = $scope.arte;

            }

        } else if(!$scope.dados.pdf) {
            if (!$scope.pdf || angular.equals({}, $scope.pdf)) {
                $('body').find('.artw-files-upload .files-pdf-attach .files-control-fake').addClass('has-error');
                myAlert('Anexe o PDF.', 'warning');
                return;
            }

            if ($scope.pdf && $scope.pdf.size > 52428800) {
                $('body').find('.artw-files-upload .files-pdf-attach .files-control-fake').addClass('has-error');
                myAlert('O tamanho máximo permitido para um arquivo é 50 MB.', 'warning');
                return;
            }

            $scope.upload = $scope.pdf;
        }

        $scope.upload.progress = 0;

        if ($scope.id_item_tipo_novo) {
            $scope.dados.id_variacao = $scope.id_variacao_novo;
        }
        
        $scope.upload.upload = Upload.upload({
            url: site_url + 'site/artwork/services/package/postArtes',
            data: {
                id_variacao: $scope.dados.id_variacao,
                id_item: $scope.dados.id_item,
                id_item_tipo: $scope.id_item_tipo_novo,
                pdf: $scope.pdf,
                arte: $scope.arte
            }
        }).then(function (response) {
            $timeout(function () {
                $scope.artwUpload.close();

                var ok = $.alert({
                    animation: 'scale',
                    closeAnimation: 'scale',
                    animationSpeed: 300,
                    title: '',
                    content: '<div class="custom-alert-contents success">' +
                        '   <span class="icon fa fa-check"></span>' +
                        '   <div class="custom-alert-heading">' +
                        '       <h4>Sucesso</h4>' +
                        '   </div>' +
                        '   <p>Arquivos enviados com sucesso</p>' +
                        '</div>',
                    closeIcon: true,
                    scrollToPreviousElement: false,
                    closeIconClass: 'jconfirm-btn-close fa fa-times',
                    buttons: {
                        ok: {
                            keys: ['enter', 'space', 'esc'],
                            text: '<span><i class="icon fa fa-check"></i>Ok</span>',
                            btnClass: 'btn-confirm my-alert-confirm',
                            action: function (Confirmar) {
                                $scope.pdf = '';
                                $scope.upload = '';
                                $scope.carregar_embalagens();
                            }
                        }
                    },
                    boxWidth: '910px',
                    useBootstrap: false,
                    onOpenBefore: function () {
                        $('body').addClass('no-scroll');
                        $('.jconfirm-content').css('marginLeft', '0px');
                        $('.jconfirm').css('overflow', 'auto');
                        $('.my-alert-confirm').focus();
                        $('.jconfirm').overlayScrollbars({
                            scrollbars: {
                                autoHide: 'move'
                            }
                        });
                    },
                    onClose: function () {
                        $('body').removeClass('no-scroll');
                        $('.jconfirm').css('overflow', 'hidden');
                        $('.my-alert-confirm').blur();

                        $scope.carregar_embalagens();
                    }
                });

            });
        }, function (response) {
            if (response.status > 0) {
                myAlert('Ocorreu um erro.', 'error');
            }
        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            $scope.upload.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
    };

    $scope.fileAttachmentCtrl = function () {
        $('.files-control .files-control-fake').find('strong').text('Anexar arquivo PDF com no máx. 50 MB');
        $('.files-control.files-artw-attach .files-control-fake').find('strong').text('Anexar arquivo ZIP ou RAR');
        
        
        $('.files-control input[type="file"]').bind('change click', function () {
            $(this).each(function () {
                var filePlaceholder = $(this).val().split('\\').pop();
                
                $(this).next('.files-control-fake').find('strong').text(filePlaceholder);

                if (!filePlaceholder) {
                    $(this).val('');
                    $(this).next('.files-control-fake').find('strong').text('Anexar arquivo PDF com no máx. 50 MB');
                }
            });
        });

        $('.files-control.files-artw-attach input[type="file"]').bind('change click', function () {
            $(this).each(function () {
                var filePlaceholder = $(this).val().split('\\').pop();
                
                $(this).next('.files-control-fake').find('strong').text(filePlaceholder);

                if (!filePlaceholder) {
                    $(this).val('');
                    $(this).next('.files-control-fake').find('strong').text('Anexar arquivo ZIP ou RAR');
                }
            });
        });
    };

    $scope.limparCamposUpload = function () {
        $('.files-control').find('input[type="file"]').val('');
        $('.files-control.files-pdf-attach').find('.files-control-fake strong').text('Anexar arquivo PDF com no máx. 50 MB');
        $('.files-control.files-artw-attach').find('.files-control-fake strong').text('Anexar arquivo ZIP ou RAR');
    };

    $scope.artwFileUpload = function (id_variacao, id_item) {
        if (id_item === undefined) {
            id_item = '';
            $scope.artwItemCreate.close();
        }

        $scope.artwUpload = $ngConfirm({
            boxWidth: '910px',
            useBootstrap: false,
            title: '',
            contentUrl: '/view/modal_upload_arquivo.html?v=3.0',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'ng-confirm-btn-close fa fa-times',
            escapeKey: 'Cancelar',
            buttons: {
                Cancelar: {
                    text: 'Cancelar',
                    btnClass: 'btn-control btn-alt'
                },
                Carregar: {
                    text: 'Concluir',
                    btnClass: 'btn-control btn-positive',
                    keys: ['enter', 'space'],
                    action: function (Confirmar) {
                        $scope.salvar_arquivos();
                        //prevent close
                        return false;
                    }
                }
            },
            onScopeReady: function () {
                var url = site_url + 'site/artwork/services/package/getArtes/' + id_variacao;
        
                url += '/' + id_item;

                if (id_item) {
                    $.get(url, function (data) {
                            if (data.error) {
                                myAlert(data.message, 'error');
                            } else {
                                $scope.dados = data;
                                $scope.$apply();
                            }
                        }, 'json')
                        .fail(function (data) {
                            myAlert('Ocorreu um erro.', 'error');
                        });
                }
            },
            onReady: function () {
                $scope.fileAttachmentCtrl();
                
                setTimeout(function () {
                    $('.ng-confirm-content').css('visibility', 'visible');
                    $('.ng-confirm-buttons').show();
                    $('.ng-confirm-box .line-wobble').remove();
                }, 1200);
            },
            onOpenBefore: function () {
                setTimeout(function () {
                    $('.ng-confirm .ng-confirm-scrollpane').overlayScrollbars({
                        scrollbars: {
                            autoHide: 'move'
                        }
                    });
                }, 0);
            },
            onOpen: function () {
                $('.ng-confirm-content').css('visibility', 'hidden');
                $('body').addClass('no-scroll');
                $('.ng-confirm-buttons').hide();
                $('.ng-confirm-box').append(customLoadingHTML);
            },
            onClose: function () {
                $scope.pdf = '';
                $scope.dados = {};
                $scope.id_item_tipo_novo = '';
            }
        });
    };

    $scope.mostrarImagemPackshelf = function(id_sku) {
        $scope.viewPackshelfImg = $ngConfirm({
            boxWidth: '910px',
            useBootstrap: false,
            title: '',
            contentUrl: '/view/modal_imagem_packshelf.html?v=1.0',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'ng-confirm-btn-close fa fa-times',
            escapeKey: true,
            backgroundDismiss: true,
            onScopeReady: function () {
                var url = site_url + 'site/artwork/produtos/marca/sku/modal/' + id_sku;

                $scope.dados = [];
                                
                $.get(url, function (data) {
                    if (data.error) {
                        myAlert(data.message, 'error');
                    } else {
                        $('.ng-confirm-buttons').remove();
                    
                        $scope.dados = data;
                        $scope.$apply();
                    }
                }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                });
            },
            onReady: function () {
                $timeout(function () {
                    $('.ng-confirm-box .line-wobble').remove();
                    $('.ng-confirm-content').css('visibility', 'visible');
                }, new Date().getMilliseconds());
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.ng-confirm-buttons').remove();
                
                setTimeout(function () {
                    $('.ng-confirm .ng-confirm-scrollpane').overlayScrollbars({
                        scrollbars: {
                            autoHide: 'move'
                        }
                    });
                }, 0);            
            },
            onOpen: function () {
                $('body').addClass('no-scroll');
                $('.ng-confirm-box').append(customLoadingHTML);
                $('.ng-confirm-content').css('visibility', 'hidden');
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.ng-confirm-buttons').remove();
            },
            onDestroy: function() {
                $('body').removeClass('no-scroll');
                $('.ng-confirm-buttons').remove();
            }
        });
    };

    $scope.carregar_embalagens();
});
