var site_url = '/';
var app = angular.module('app', ['selectize']);

app.controller('technicalPlanCreateCtrl', function ($scope, $filter, $http, $timeout) {
    $scope.cbPckType = cbPckType;
    $scope.cbPckContentSize = cbPckContentSize;
    $scope.packageHasVariation = possui_variacao;
    $scope.tipos = [];
    $scope.tamanhos = [];
    $scope.variacoes = [];
    $scope.variacaoName = variacao_edicao;
    $scope.variacao_edicao = variacao_edicao;
    
    $scope.pkgTypesConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        delimiter: '|',
        maxItems: 1,
        placeholder: 'Selecione o tipo de embalagem',
        onInitialize: function (selectize) {},
        onDropdownOpen: function () {},
        onChange: function (value) {
            if (value !== '') {
                $scope.cbPckContentSize = '';
            }
        }
    };

    $scope.pkgContentSizeConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        delimiter: '|',
        maxItems: 1,
        placeholder: 'Selecione o volume',
        onInitialize: function (selectize) {},
        onDropdownOpen: function () {}
    };

    $scope.show_load = function () {
        $('.master').addClass('blurred');
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        $('.master').removeClass('blurred');
        $('.lds-css').hide();
    };

    $scope.carregar_tipos = function () {
        $scope.show_load();
            
        $.get(site_url + 'site/artwork/services/package/types', function (data) {
            $scope.tipos = data;
            $scope.$apply();
            $scope.hide_load();
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
    };

    $scope.carregar_tamanhos = function () {
        if ($scope.cbPckType != '' && $scope.cbPckType != 0) {
            $scope.show_load();
            
            $.get(site_url + 'site/artwork/services/package/types/' + $scope.cbPckType + '/sizes', function (data) {
                $scope.tamanhos = data;
                $scope.$apply();
                $scope.hide_load();

            }, 'json')
                    .fail(function (data) {
                        myAlert('Ocorreu um erro.', 'error');
                        $scope.hide_load();
                    });
        } else {
            $scope.tamanhos = [];
            $scope.cbPckContentSize = '';
        }
    };

    $scope.carregar_variacoes = function ()
    {
        if ($scope.cbPckContentSize != '' && $scope.cbPckContentSize != 0)
        {
            $scope.show_load();

            var url = 'site/techdraw/services/checkVariacoes/' + $scope.cbPckContentSize;

            $.get(site_url + url, function (data)
            {
                $scope.variacoes = data;

                $scope.$apply();
                $scope.hide_load();

            }, 'json')
                    .fail(function (data) {
                        myAlert('Ocorreu um erro.', 'error');
                        $scope.hide_load();
                    });
        }
    };
    
    if ($scope.packageHasVariation == 'Sim') {
        $scope.carregar_variacoes();
    }

    $scope.cadastrar_variacao = function () {
        if ($scope.packageHasVariation === '' || $scope.packageHasVariation === 'Nao') {
            $scope.variacaoName = '';
        }
    };
    
    $scope.carregar_tipos();
    $scope.carregar_tamanhos();
    
});