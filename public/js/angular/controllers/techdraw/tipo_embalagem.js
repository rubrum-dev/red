var customLoadingHTML = '<div class="line-wobble"></div>';
var site_url = '/';
var app = angular.module('app', ['tipoEmbalagemResolveLoader', 'cp.ngConfirm', '720kb.tooltips', 'ngAnimate', 'ngFileUpload']);

app.animation('.slide-toggle-js', function () {
    return {
        enter: function (element, done) {
            $(element).hide().slideDown(200, function ($scope) {
                done();
            });
        },
        leave: function (element, done) {
            $(element).slideUp(200, function () {
                done();
            });
        }
    };
});

app.directive('fileInput', function () {
    return {
        link: function ($scope) {
            $scope.onchange = function (sender) {
                $scope.$apply(function ($scope) {
                    $scope.files_pdf = sender.files[0];
                });
            };
        }
    };
});

app.directive('validFile', function validFile($parse) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            ngModelCtrl.$validators.validFile = function () {
                element.on('change', function () {
                    var value = element.val(),
                        model = $parse(attrs.ngModel),
                        modelSetter = model.assign;

                    scope.uploadedFileType = null;

                    if (!value) {
                        modelSetter(scope, '');
                    } else {
                        var ext = value.substring(value.lastIndexOf('.') + 1).toLowerCase();

                        if (attrs.validFile.indexOf(ext) !== -1) {
                            scope.uploadedFileType = ext;
                            modelSetter(scope, element[0].files[0]);
                        } else {
                            myAlert('Formato de arquivo inválido.', 'warning');
                            scope.uploadedFileType = 'other';
                            modelSetter(scope, '');
                            scope.limparCamposUpload();
                        }
                    }
                });
            };
        }
    };
});

app.controller('tipoEmbalagemCtrl', function ($scope, $rootScope, $ngConfirm, Upload, $timeout) {
    $scope.dados = {};
    $scope.files_pdf = [];
    $scope.editItem = [];
    $scope.itens = [];

    $scope.show_load = function () {
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        $('.lds-css').hide();
    };

    $scope.fileAttachmentCtrl = function () {
        $('.files-control .files-control-fake').find('strong').text('Anexar arquivo PDF.');
        
        $('.files-control input[type="file"]').on('change click', function () {
            $(this).each(function () {
                var filePlaceholder = $(this).val().split('\\').pop();
                
                $(this).next('.files-control-fake').find('strong').text(filePlaceholder);

                if (!filePlaceholder) {
                    $(this).val('');
                    $(this).next('.files-control-fake').find('strong').text('Anexar arquivo PDF.');
                }
            });
        });
    };
    
    $scope.exibeEditarItem = function (planta, item) {
        for (let i = 0; i < planta.itens.length; i++) {
            if (planta.itens[i].exibir_editar_item == true) {
                planta.itens[i].exibir_editar_item = false;
            }
        }
        planta.exibir_formulario = false;
        item.exibir_editar_item = !item.exibir_editar_item;
    };

    $scope.exibeNovoItem = function (planta, acao) {
        if (acao) {
            for (let i = 0; i < planta.itens.length; i++) {
                if (planta.itens[i].exibir_editar_item == true) {
                    planta.itens[i].exibir_editar_item = false;
                }
            }
        }
        planta.exibir_formulario = acao;
    };

    $scope.carregar_itens = function () {
        $scope.show_load();

        var url = site_url + 'site/techdraw/services/getPlantas/' + id_tipo;

        $.ajax({
            url: url,
            type: "get",
            contentType: false,
            processData: false,
            start_time: new Date().getTime(),
            success: function (data) {
                if (data.error) {
                    myAlert(data.message, 'error');
                } else {
                    $scope.itens = data;
                    $scope.$apply();
                }

                $timeout(function () {
                    $rootScope.loaded =  true;                
                }, new Date().getTime() - this.start_time);

                $scope.hide_load();
            },
            error: function () {
                myAlert('Ocorreu um erro.', 'error');
            }
        });
    };

    $scope.carregar_itens();

    $scope.criar_item = function (planta) {
        var id_item = planta.id_item_tipo_novo;

        if (!id_item) {
            $('body').find('.package-container .input-required').addClass('input-error');
            $scope.hide_load();
            myAlert('Selecione o Item.', 'warning');
            return;
        }

        $scope.show_load();

        // Verifica se já existe
        params = {
            _token: csrf_token,
            id: planta.id,
            id_tipo: planta.id_item_tipo_novo
        };

        $.post(site_url + 'site/techdraw/services/criarItem', params, function (data) {
                if (data.error) {
                    myAlert(data.message, 'error');
                    $scope.hide_load();
                } else {
                    myAlert(data.message, 'success');

                    $scope.$apply();
                    $scope.carregar_itens();
                    $scope.hide_load();
                }

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
    };

    $scope.getItemTipoById = function (id) {

        for (let i = 0; i < $scope.itens.item_tipos.length; i++) {
            if ($scope.itens.item_tipos[i].id == id) {
                return $scope.itens.item_tipos[i];
            }
        }
    };

    $scope.alterar_item = function (item) {
        if (item.id_tipo == item.id_tipo_original) {
            item.exibir_editar_item = false;
            return;
        }

        $scope.show_load();

        params = {
            _token: csrf_token,
            id: item.id,
            id_tipo: item.id_tipo
        };

        $.post(site_url + 'site/techdraw/services/alterarItem', params, function (data) {

                if (data.error) {
                    myAlert(data.message, 'error');
                    $scope.hide_load();
                } else {
                    myAlert(data.message, 'success');

                    item.nome = $scope.getItemTipoById(item.id_tipo).nome;
                    item.exibir_editar_item = false;
                    item.id_tipo_original = item.id_tipo;

                    $scope.$apply();

                    $scope.hide_load();
                }

            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });

    };

    $scope.setUploadPdf = function (element) {
        //console.log(element.files);
        $scope.$apply(function (scope) {
            // Turn the FileList object into an Array
            for (var i = 0; i < element.files.length; i++) {
                $scope.files_pdf.push(element.files[i]);
            }
        });
    };

    $scope.carregar_dados = function (id_item) {
        var url = site_url + 'site/techdraw/services/getArtes/' + id_item;

        $scope.finished = false;

        $.get(url, function (data) {
                if (data.error) {
                    myAlert(data.message, 'error');
                    techDrawUpload.close();
                    return false;
                } else {
                    $scope.dados = data;
                    $scope.$apply();

                    $timeout(function () {
                        $scope.finished = true;
                    }, new Date().getMilliseconds());
                }
            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.techDrawUploadModal.close();
                return false;
            });
    }

    $scope.salvar_arquivosOLD = function () {
        $scope.show_load();

        if (!$scope.files_pdf.length) {
            $scope.hide_load();
            myAlert('Anexe o PDF.', 'warning');
            return;
        }

        var formData = new FormData();

        if ($scope.dados.id_item) {
            formData.append("id_item", $scope.dados.id_item);
        }

        for (var i = 0; i < $scope.files_pdf.length; i++) {
            formData.append("pdf", $scope.files_pdf[i]);
        }

        $.ajax({
            url: site_url + 'site/techdraw/services/postArtes',
            headers: {
                'x-csrf-token': csrf_token
            },
            type: 'POST',
            data: formData,
            success: function (data) {
                $scope.hide_load();

                var ok = $.alert({
                    icon: 'fa fa-check',
                    title: 'Sucesso',
                    content: 'Arquivos enviados com sucesso',
                    escapeKey: 'ok',
                    buttons: {
                        ok: {
                            keys: ['enter', 'space', 'esc'],
                            text: 'Ok',
                            text: '<span><i class="icon fa fa-check"></i>Ok</span>',
                            btnClass: 'btn-confirm',
                            action: function (Confirmar) {}
                        }
                    },
                    closeIcon: true,
                    closeIconClass: 'jconfirm-btn-close fa fa-times',
                    boxWidth: '910px',
                    useBootstrap: false,
                    onOpenBefore: function () {
                        $('body').addClass('no-scroll');
                        $('.jconfirm').css('overflow', 'auto');
                        $('.jconfirm').overlayScrollbars({
                            scrollbars: {
                                autoHide: 'move'
                            }
                        });
                    },
                    onClose: function () {
                        $('.file-attachment-control .file-placeholder').text('Procurar...');
                        $('#tcdFileUpload').val('');
                        $('body').removeClass('no-scroll');
                        $('.jconfirm').css('overflow', 'hidden');
                        $scope.carregar_itens();
                        $scope.files_pdf = [];
                        $scope.tcdFileUpload = '';
                    }
                });
            },
            error: function () {
                $scope.hide_load();
                myAlert('Ocorreu um erro.', 'error');
            },
            contentType: false,
            processData: false
        });
    };

    $scope.salvar_arquivos = function () {

        if (!$scope.pdf) {
            myAlert('Anexe o PDF.', 'warning');
            return;
        }

        $scope.pdf.progress = 0;

        $scope.pdf.upload = Upload.upload({
            url: site_url + 'site/techdraw/services/postArtes',
            data: {
                id_item: $scope.dados.id_item,
                pdf: $scope.pdf
            }
        });

        $scope.pdf.upload.then(function (response) {
            $timeout(function () {

                $scope.techDrawUploadModal.close();


                var ok = $.alert({
                    icon: 'fa fa-check',
                    title: 'Sucesso',
                    content: 'Arquivos enviados com sucesso',
                    escapeKey: 'ok',
                    buttons: {
                        ok: {
                            keys: ['enter', 'space', 'esc'],
                            text: 'Ok',
                            text: '<span><i class="icon fa fa-check"></i>Ok</span>',
                            btnClass: 'btn-confirm',
                            action: function (Confirmar) {}
                        }
                    },
                    closeIcon: true,
                    closeIconClass: 'jconfirm-btn-close fa fa-times',
                    boxWidth: '910px',
                    useBootstrap: false,
                    onOpenBefore: function () {
                        $('body').addClass('no-scroll');
                        $('.jconfirm').css('overflow', 'auto');
                        $('.jconfirm').overlayScrollbars({
                            scrollbars: {
                                autoHide: 'move'
                            }
                        });
                    },
                    onClose: function () {
                        $('.file-attachment-control .file-placeholder').text('Procurar...');
                        $('#tcdFileUpload').val('');
                        $('body').removeClass('no-scroll');
                        $('.jconfirm').css('overflow', 'hidden');
                        $scope.carregar_itens();
                        $scope.pdf = '';
                    }
                });
            });
        }, function (response) {
            if (response.status > 0) {

                myAlert('Ocorreu um erro.', 'error');

            }

        }, function (evt) {
            $scope.pdf.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
    };

    $scope.limparCamposUpload = function () {
        $('.files-control').find('input[type="file"]').val('');
        $('.techdraw-file-attach').find('.files-control-fake strong').text('Anexar arquivo PDF.');
    };

    $scope.techDrawUpload = function (item) {
        $scope.techDrawUploadModal = $ngConfirm({
            boxWidth: '910px',
            useBootstrap: false,
            title: '',
            contentUrl: '/view/techdraw/modal_upload_planta_tecnica.html?v=29092022',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'ng-confirm-btn-close fa fa-times',
            backgroundDismiss: true,
            escapeKey: 'Cancelar',
            buttons: {
                Cancelar: {
                    keys: ['esc'],
                    text: 'Cancelar',
                    btnClass: 'btn btn-control btn-alt'
                },
                Carregar: {
                    keys: ['enter', 'space'],
                    text: 'Concluir',
                    btnClass: 'btn btn-control btn-positive',
                    action: function (Confirmar) {
                        $scope.salvar_arquivos(item);
                        return false;
                    }
                }
            },
            onScopeReady: function () {
                $scope.carregar_dados(item.id);
            },
            onReady: function () {
                $scope.$watch('finished', function () {
                    $scope.fileAttachmentCtrl();
            
                    if($scope.finished) {
                        $('.ng-confirm-content').css('visibility', 'visible');
                        $('.ng-confirm-buttons').show();
                        $('.ng-confirm-box .line-wobble').remove();
                    }
                });
            },
            onOpenBefore: function () {
                setTimeout(function () {
                    $('.ng-confirm .ng-confirm-scrollpane').overlayScrollbars({
                        scrollbars: {
                            autoHide: 'move'
                        }
                    });
                }, 0);
            },
            onOpen: function () {
                $('body').addClass('no-scroll');
                $('.ng-confirm-content').css('visibility', 'hidden');
                $('.ng-confirm-buttons').hide();
                $('.ng-confirm-box').append(customLoadingHTML);
            },
            onDestroy: function (scope) {
                $scope.dados = {};
                $scope.pdf = '';
            }
        });
    };

    $scope.techDrawItemOptions = function () {
        var techDrawItemOptions = $ngConfirm({
            boxWidth: '910px',
            useBootstrap: false,
            title: '',
            contentUrl: '/view/techdraw/modal_item_opcoes.html',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'ng-confirm-btn-close fa fa-times',
            backgroundDismiss: true
        });
    };

    $scope.excluirPlanta = function (planta) {
        $.confirm({
            icon: 'fa fa-question',
            title: 'Confirmar',
            content: 'Deseja excluir essa planta técnica?',
            scrollToPreviousElement: false,
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            useBootstrap: false,
            boxWidth: '910px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        $scope.show_load();

                        params = {
                            _token: csrf_token,
                            id: planta.id
                        };

                        $.post(site_url + 'site/techdraw/services/removerPlanta', params, function (data) {
                                if (data.error) {
                                    myAlert(data.message, 'error');
                                    $scope.hide_load();
                                } else {
                                    myAlert(data.message, 'success');
                                    $scope.carregar_itens();
                                    $scope.$apply();
                                    $scope.hide_load();
                                }
                            }, 'json')
                            .fail(function (data) {
                                myAlert('Ocorreu um erro.', 'error');
                                $scope.hide_load();
                            });
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel'
                }
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };

    $scope.excluirItem = function (item) {
        $.confirm({
            icon: 'fa fa-question',
            title: 'Confirmar',
            content: 'Deseja excluir esse item?',
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            useBootstrap: false,
            boxWidth: '910px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        $scope.show_load();

                        params = {
                            _token: csrf_token,
                            id: item.id
                        };

                        $.post(site_url + 'site/techdraw/services/removerItem', params, function (data) {
                                if (data.error) {
                                    myAlert(data.message, 'error');
                                    $scope.hide_load();
                                } else {
                                    myAlert(data.message, 'success');
                                    $scope.carregar_itens();
                                    $scope.$apply();
                                    $scope.hide_load();
                                }
                            }, 'json')
                            .fail(function (data) {
                                myAlert('Ocorreu um erro.', 'error');
                                $scope.hide_load();
                            });
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel'
                }
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };
});