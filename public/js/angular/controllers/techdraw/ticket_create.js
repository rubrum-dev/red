var site_url = '/';
var app = angular.module('app', []);

app.controller('TicketCreateCtrl', function ($scope)
{
    $scope.solicitacoes = solicitacoes;

    $scope.tipos = [];

    $scope.show_load = function()
    {
        //$('.master').addClass('blurred');
        $('.lds-css').show();
    }

    $scope.hide_load = function()
    {
        //$('.master').removeClass('blurred');
        $('.lds-css').hide();
    }

    $scope.carregar_tipos = function ()
    {
        $scope.show_load();

        $.get(site_url + 'site/techdraw/services/package/request/types', function (data)
        {
            $scope.tipos = data;

            $scope.$apply();
            $scope.hide_load();

        }, 'json');
    }

    $scope.adicionar = function ()
    {
        if ($scope.add_tipo && $scope.add_descricao)
        {
            var template = {'id': $scope.add_tipo, 'descricao': $scope.add_descricao};

            //console.log(template);

            $scope.solicitacoes.push(template);

            $scope.add_tipo = '';
            $scope.add_descricao = '';
        }
    }

    $scope.remover = function (k)
    {
        $scope.solicitacoes.splice(parseInt(k), 1);

        //console.log(parseInt(k));
        //console.log($scope.participantes);
    }

    $scope.carregar_tipos();

});
