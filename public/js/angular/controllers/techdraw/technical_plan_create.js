var site_url = '/';
var app = angular.module('app', ['selectize']);

app.controller('technicalPlanCreateCtrl', function ($scope, $filter, $http, $timeout) {
    $scope.cbPckType = '';
    $scope.cbPckContentSize = '';
    $scope.packageHasVariation = '';
    $scope.tipos = [];
    $scope.tamanhos = [];
    $scope.variacoes = [];

    $scope.tamanhos = [];

    $scope.pkgTypesConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        maxItems: 1,
        delimiter: '|',
        placeholder: 'Selecione o tipo de embalagem',
        onInitialize: function (selectize) {
            $scope.carregar_tipos();
        },
        onDropdownOpen: function () {}
    };

    $scope.pkgContentSizeConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome'],
        maxItems: 1,
        delimiter: '|',
        placeholder: 'Selecione o volume',
        onInitialize: function (selectize) {
            $('.selectize-dropdown').overlayScrollbars({});
        },
        onDropdownOpen: function () {}
    };

    $scope.show_load = function () {
        $('.master').addClass('blurred');
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        $('.master').removeClass('blurred');
        $('.lds-css').hide();
    };

    $scope.carregar_tipos = function () {
        $.get(site_url + 'site/artwork/services/package/types', function (data) {
            $scope.show_load();
            $scope.tipos = data;
            $scope.tamanhos = [];
            $scope.cbPckType = '';
            $scope.cbPckContentSize = '';
            $scope.$apply();
            $scope.hide_load();
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
    };

    $scope.carregar_tamanhos = function () {
        if ($scope.cbPckType != '' && $scope.cbPckType != 0) {
            $scope.show_load();
            $.get(site_url + 'site/artwork/services/package/types/' + $scope.cbPckType + '/sizes', function (data) {
                $scope.tamanhos = data;
                $scope.cbPckContentSize = '';

                $scope.$apply();
                $scope.hide_load();

            }, 'json')
                    .fail(function (data) {
                        myAlert('Ocorreu um erro.', 'error');
                        $scope.hide_load();
                    });
        } else {
            $scope.tamanhos = [];
            $scope.cbPckContentSize = '';
        }
    };

    $scope.carregar_variacoes = function ()
    {
        if ($scope.cbPckContentSize != '' && $scope.cbPckContentSize != 0)
        {
            $scope.show_load();

            var url = 'site/techdraw/services/checkVariacoes/' + $scope.cbPckContentSize;

            $.get(site_url + url, function (data)
            {
                $scope.variacoes = data;

                $scope.$apply();
                $scope.hide_load();

            }, 'json')
                    .fail(function (data) {
                        myAlert('Ocorreu um erro.', 'error');
                        $scope.hide_load();
                    });
        }
    };

    $scope.cadastrar_variacao = function () {
        if ($scope.packageHasVariation === '' || $scope.packageHasVariation === 'Nao') {
            $scope.variacaoName = '';
        }
    };
});