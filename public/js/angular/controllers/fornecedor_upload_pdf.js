var site_url = '/';

var app = angular.module('app', ['ngFileUpload']);

app.filter('bytes', function() {
	return function(bytes, precision) {
		if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
		
        if (typeof precision === 'undefined') precision = 1;
		
        var units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB'],
			number = Math.floor(Math.log(bytes) / Math.log(1024));
		
            return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  ' ' + units[number];
	}
});

app.controller('fornecedorUploadPDFCtrl', function ($scope, Upload, $http) {
    $scope.fornecedorPdfs = [];

    $scope.upload_arquivo = function($event) {
        var file = document.getElementById('pdfFileUpload').files[0];
        $scope.pdf = file;
        
        if (!$scope.pdf) {
            $('.col-files-attach').find('.files-control-fake').addClass('has-error');
            $('.col-select-supplier').find('select').addClass('has-error');
            
            myAlert('Anexe um arquivo PDF.', 'warning');
            
            return false;
        } 
        
        if ($scope.pdf.size > 52428800) {
            $('.col-files-attach').find('.files-control-fake').addClass('has-error');
            
            myAlert('O tamanho máximo permitido para um arquivo é 50 MB.', 'warning');
            
            return false;
        }

        $('.col-files-attach').find('.files-control-fake').removeClass('has-error');
        $('.col-select-supplier').find('select').removeClass('has-error');        
        //$scope.fornecedorPdfs = new Array()
        //$scope.fornecedorPdfs.push($scope.pdf);
        
        params = {
            _token: csrf_token,
            pdf: file
        };

        $scope.pdf.progress = 0;

        $scope.pdf.upload = Upload.upload({
            url: site_url + 'site/artwork/services/package/ticket/uploadFornecedorPdf',
            data: params
        });

        $scope.pdf.upload.then(function (response) {
            console.log(params);
            if (response.data.error)
            {
                myAlert(response.data.message);
                $scope.pdf = '';
                $scope.limparCamposUpload();
            } else {
                $scope.pdf = '';
                $scope.limparCamposUpload();
            }
        }, function (response) {
            if (response.status > 0) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.pdf.progress = 0;
            }
        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            $scope.pdf.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
    };

    $scope.excluir = function()  {
        $.confirm({
            icon: 'icon fa fa-question',
            title: 'Confirmar',
            content: 'Deseja excluir esse PDF?',
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            useBootstrap: false,
            boxWidth: '910px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        $scope.excluir_pdf_service();
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {}
                }
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };

    $scope.excluir_pdf_service = function(index) {
        $scope.fornecedorPdfs.splice(index, 1)
        $scope.$apply()
    };

    $scope.enviar_para_cliente = function() {
        $.confirm({
            icon: 'icon fa fa-question',
            title: 'Confirmar',
            content: 'Deseja enviar o PDF anexado para o cliente?',
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            useBootstrap: false,
            boxWidth: '910px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        $scope.enviar_para_cliente_service();
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {}
                }
            },
            onOpenBefore: function () {
                $('body').addClass('no-scroll');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };

    $scope.enviar_para_cliente_service = function() {
        // TO-DO
        $scope.show_load();
        
        params = {
            _token: csrf_token,
            id: id_pdf,
            acao: acao
        };
        
        $.post(site_url + 'site/artwork/services/package/ticket/acaoEnviarPdf', params, function (data) {
                myAlert('PDF enviado com sucesso.', 'success');
            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
    };

    $scope.limparCamposUpload = function () {
        $('.files-control.attachPDF').find('.files-control-fake strong').text('Anexar arquivo PDF com no máx. 50 MB');
    };

    angular.element('.files-control, .files-control.attach-artwork').on('click', function () {
        angular.forEach(angular.element('.files-control.attachPDF'), function () {
            $scope.limparCamposUpload();
        });
    });
});