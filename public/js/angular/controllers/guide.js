
var site_url = '/';
var app = angular.module('app', ['cp.ngConfirm']);

app.controller('guideCtrl', function ($scope, $ngConfirm) {
    $scope.guideColorInfo_1 = function() {
        var artwUpload = $ngConfirm({
            boxWidth: '740px',
            useBootstrap: false,
            title: '',
            contentUrl: '/view/guide/brahma/guide_color_info_1.html',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'custom-alert-close'
        });
    }

    $scope.guideColorInfo_2 = function() {
        var artwUpload = $ngConfirm({
            boxWidth: '740px',
            useBootstrap: false,
            title: '',
            contentUrl: '/view/guide/brahma/guide_color_info_2.html',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'custom-alert-close'
        });
    }

    $scope.guideColorInfo_3 = function() {
        var artwUpload = $ngConfirm({
            boxWidth: '740px',
            useBootstrap: false,
            title: '',
            contentUrl: '/view/guide/brahma/guide_color_info_3.html',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'custom-alert-close'
        });
    }

    $scope.guideColorInfo_4 = function() {
        var artwUpload = $ngConfirm({
            boxWidth: '740px',
            useBootstrap: false,
            title: '',
            contentUrl: '/view/guide/brahma/guide_color_info_4.html',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'custom-alert-close'
        });
    }

    $scope.guideColorInfo_5 = function() {
        var artwUpload = $ngConfirm({
            boxWidth: '740px',
            useBootstrap: false,
            title: '',
            contentUrl: '/view/guide/brahma/guide_color_info_5.html',
            scope: $scope,
            closeIcon: true,
            closeIconClass: 'custom-alert-close'
        });
    }
});