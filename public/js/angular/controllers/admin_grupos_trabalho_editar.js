var site_url = '/';
var admGruposTrabalhoEditar = angular.module('admGruposTrabalhoEditar', [
    'cp.ngConfirm',
    'ngAnimate',
    'selectize'
]);

admGruposTrabalhoEditar.controller('AdmGruposTrabalhoEditarCtrl', function (
    $scope,
    $timeout,
    $window,
    $rootScope,
    $ngConfirm,
    $http,
    $routeParams
) {
    $scope.grupos = {
        locais: [],
        globais: []
    };

    $scope.loaded = false;
    $rootScope.participantes = [];
    $scope.membros = {};
    $rootScope.cbMembershipProfile = [];
    $rootScope.grupo = {};

    $scope.myConfig = {
        create: false,
        valueField: 'id',
        labelField: 'nome',
        searchField: ['nome', 'empresa', 'departamento'],
        placeholder: 'Selecionar participante',
        maxItems: 1,
        options: '',
        render: {
            option: function (data, escape) {
                if (data.ignore === true) {
                    return '<div style="display:none"></div>';
                } else if (data.departamento !== '') {
                    return '<div>' +
                        '   <span class="heading">' + escape(data.nome) + '</span>' +
                        '   <span class="caption">' + escape(data.empresa) + '</span>' +
                        '   <span class="slash">' + escape('/') + '</span>' +
                        '   <span class="caption">' + escape(data.departamento) + '</span>' +
                        '</div>';
                } else {
                    return '<div>' +
                        '   <span class="heading">' + escape(data.nome) + '</span>' +
                        '   <span class="caption">' + escape(data.empresa) + '</span>' +
                        '   <span class="slash">' + escape('/') + '</span>' +
                        '   <span class="caption">' + 'Departamento' + '</span>' +
                        '</div>';
                }
            }
        },
        onInitialize: function (selectize) {},
        onDropdownOpen: function () {
            $('.selectize-dropdown').addClass('direction-up');

            this.clear();
            this.focus();
        }
    };

    $scope.show_load = function () {
        $('.lds-css').show();
    };

    $scope.hide_load = function () {
        $('.lds-css').hide();
    };

    $scope.carregar_grupos = function () {
        $scope.show_load();
        $scope.loaded = false;
        $http.get(site_url + 'site/artwork/services/package/getGruposDeAprovacao').success(function (data) {
            $rootScope.grupos = data;
            $scope.hide_load();
            $scope.loaded = true;
        });
    };

    $scope.carregar_membros = function () {
        $scope.loaded = false;
        $scope.show_load();

        params = {
            _token: csrf_token
        };

        $.post(site_url + 'site/artwork/services/package/members', params, function (data) {
            $scope.membros = data;
            $scope.$apply();
            $scope.loaded = true;
            $scope.hide_load();
        }, 'json');
    };

    $scope.verificar_participante_service = function (id_usuario, id_perfil) {
        var n = 0;

        for (i = 0; i < $rootScope.participantes.length; i++) {
            if ($rootScope.participantes[i].id_usuario == id_usuario && ($rootScope.participantes[i].id_perfil == id_perfil || $scope.cbMembershipProfile[i] == id_perfil)) {
                n++;
            }

            if ($scope.participantes[i].equipe_membros) {
                for (m = 0; m < $rootScope.participantes[i].equipe_membros.length; m++) {
                    if ($rootScope.participantes[i].equipe_membros[m].id_usuario == id_usuario && ($rootScope.participantes[i].id_perfil == id_perfil || $scope.cbMembershipProfile[i] == id_perfil)) {
                        n++;
                    }
                }
            }
        }

        if (n > 0) {
            return n;
        } else {
            return false;
        }
    };

    $scope.selecionar_primeiro = function (id_usuario, id_perfil, id_perfil_antigo) {
        verifica = $scope.verificar_participante_service(id_usuario, id_perfil);

        if (verifica > 1) {
            myAlert('O Perfil de Participação selecionado já foi atribuído à este Participante neste ticket.', 'warning');

            $rootScope.participantes[0].id_perfil = id_perfil_antigo;
            $scope.cbMembershipProfile[0] = id_perfil_antigo;

            return false;
        }
    };

    $scope.adicionar = function () {
        if ($scope.verificar_participante_service($scope.add_participante, $scope.add_perfil)) {
            myAlert('O Perfil de Participação selecionado já foi atribuído à este Participante neste ticket.', 'warning');

            return false;
        }

        if ($scope.add_participante && $scope.add_perfil) {
            var template = {
                'id_usuario': $scope.add_participante,
                'id_perfil': $scope.add_perfil,
                'recebe_email': $scope.add_email,
                'equipe_membros': []
            };

            $rootScope.participantes.push(template);

            $scope.add_participante = '';
            $scope.add_perfil = '';
            $scope.add_email = '';

            $('.input-fields-group:last .selectize-control .selectize-input').removeClass('has-error');
            $('.input-fields-group:last .select-control select').removeClass('has-error');
        } else {
            $('.input-fields-group:last .selectize-control .selectize-input').addClass('has-error');
            $('.input-fields-group:last .select-control select').addClass('has-error');

            myAlert('Selecione o Participante e seu respectivo Perfil de Participação neste ticket.', 'warning');
        }
    };

    $scope.adicionar_membro = function (participante, k) {
        if ($scope.verificar_participante_service($rootScope.participantes[k].add_participante, $scope.cbMembershipProfile[k])) {
            myAlert('O Perfil de Participação selecionado já foi atribuído à este Participante neste ticket.', 'warning');

            return false;
        }

        if ($rootScope.participantes[k].add_participante && $scope.cbMembershipProfile[k]) {
            var template = {
                'id_usuario': $rootScope.participantes[k].add_participante,
                'recebe_email': ''
            };

            $rootScope.participantes[k].equipe_membros.push(template);
            $rootScope.participantes[k].add_participante = '';
            $rootScope.participantes[k].adiciona_equipe = false;

            $('.membership-team-col .selectize-control .selectize-input').removeClass('has-error');
        } else {
            $('.membership-team-col .selectize-control .selectize-input').addClass('has-error');

            myAlert('Selecione o Participante e seu respectivo Perfil de Participação neste ticket.', 'warning');
        }
    };

    $scope.remover = function (k) {
        $scope.cbMembershipProfile.splice(parseInt(k), 1);
        $rootScope.participantes.splice(parseInt(k), 1);
    };

    $scope.remover_membro = function (k, e) {
        $rootScope.participantes[k].equipe_membros.splice(parseInt(e), 1);
    };

    $scope.adicionar_equipe_toggle = function (participante, participantes, index) {
        for (i = 0; i < participantes.length; i++) {
            if (participantes[i].adiciona_equipe && i !== index) {
                participantes[i].adiciona_equipe = false;
            }
        }

        participante.adiciona_equipe = !participante.adiciona_equipe;
    };

    $scope.salvar_grupo_trabalho_confirm = function () {
        $.confirm({
            icon: 'fa fa-question',
            title: 'Confirmar',
            content: 'Já existe um Grupo de Trabalho com este nome. Deseja substituí-lo?',
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            useBootstrap: false,
            boxWidth: '910px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        $scope.salvar_grupo_trabalho(1, $rootScope.grupo);
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {}
                }
            },
            onOpenBefore: function () {
                //$('body').addClass('jconfirm-overlay');
                $('body').addClass('no-scroll');
                //$('.master').addClass('blurred');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                //$('body').removeClass('jconfirm-overlay');
                $('body').removeClass('no-scroll');
                //$('.master').removeClass('blurred');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };

    $scope.getGrupoByName = function (name) {
        for (i = 0; i < $rootScope.grupos.locais.length; i++) {
            if ($rootScope.grupos.locais[i].nome == name) {
                return $rootScope.grupos.locais[i];
            }
        }

        for (i = 0; i < $rootScope.grupos.globais.length; i++) {
            if ($rootScope.grupos.globais[i].nome == name) {
                return $rootScope.grupos.globais[i];
            }
        }
    };

    $scope.salvar_grupo_trabalho = function (substituir, grupo) {
        if (!$scope.grupo_nome) {
            $scope.hide_load();
            angular.element('#grupoTrabalhoNome').addClass('has-error');
            myAlert('Digite o Nome do Grupo de Trabalho.', 'warning');
            return;
        }

        if ( ( (grupo && $scope.grupo_nome != grupo.nome) || !grupo ) && $scope.getGrupoByName($scope.grupo_nome) && !substituir) {
            //$scope.hide_load();
            //angular.element('#grupoTrabalhoNome').addClass('has-error');
            //myAlert('Já existe um Grupo de Trabalho com este nome. Escolha um nome diferente.', 'warning');

            //return;
        }

        angular.element('#grupoTrabalhoNome').removeClass('has-error');
        
        $scope.show_load();

        if (!substituir) {
            substituir == 0;
        }

        if ($scope.grupo_id) {
            
            params = {
                _token: csrf_token,
                grupo_nome: $scope.grupo_nome,
                grupo_global: $scope.grupo_global,
                grupo_id: $scope.grupo_id,
                participantes: $rootScope.participantes,
                cbMembershipProfile: $scope.cbMembershipProfile
            };

            url = site_url + 'site/artwork/services/package/postSalvarGruposDeAprovacao';

        } else {

            params = {
                _token: csrf_token,
                grupo_nome: $scope.grupo_nome,
                grupo_global: $scope.grupo_global,
                participantes: $rootScope.participantes,
                cbMembershipProfile: $scope.cbMembershipProfile
            };

            url = site_url + 'site/artwork/services/package/postGruposDeAprovacao';
        }

        $.post(url, params, function (data) {
                $scope.$apply();
                $scope.hide_load();

                if (data.success == 201)
                {
                    myAlert(data.message, 'error');
                    $scope.hide_load();
                    return;
                }

                $.alert({
                    title: '',
                    content: '<div class="custom-alert-contents success">' +
                        '   <span class="icon fa fa-check"></span>' +
                        '   <div class="custom-alert-heading">' +
                        '       <h4>Sucesso</h4>' +
                        '   </div>' +
                        '   <p>Grupo de Trabalho salvo com sucesso.</p>' +
                        '</div>',
                    escapeKey: 'Entendi',
                    buttons: {
                        Entendi: {
                            keys: ['esc', 'enter', 'space'],
                            text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                            btnClass: 'btn-confirm my-alert-confirm',
                            action: function (Entendi) {
                                $('.lds-css').show(200, function () {
                                    $window.location.href = site_url + 'admin/grupos-trabalho';
                                });
                            }
                        }
                    },
                    closeIcon: true,
                    closeIconClass: 'jconfirm-btn-close fa fa-times',
                    boxWidth: '910px',
                    useBootstrap: false,
                    onOpenBefore: function () {
                        //$('body').addClass('jconfirm-overlay');
                        $('body').addClass('no-scroll');
                        //$('.master').addClass('blurred');
                        $('.jconfirm-content').css('marginLeft', '0px');
                        //$('.ng-confirm').addClass('blurred');
                        $('.jconfirm').css('overflow', 'auto');
                        $('.my-alert-confirm').focus();
                        $('.jconfirm').overlayScrollbars({
                            scrollbars: {
                                autoHide: 'move'
                            }
                        });
                    },
                    onClose: function () {
                        //$('body').removeClass('jconfirm-overlay');
                        $('body').removeClass('no-scroll');
                        //$('.master').removeClass('blurred');
                        //$('.ng-confirm').removeClass('blurred');
                        $('.jconfirm').css('overflow', 'hidden');
                        $('.my-alert-confirm').blur();
                        $('.lds-css').show(200, function () {
                            $window.location.href = site_url + 'admin/grupos-trabalho';
                        });
                        if ($('.ng-confirm').length) {
                            //$('.master').addClass('blurred');
                        }
                    }
                });
            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro!', 'error');
                $scope.hide_load();
            });
    };

    $scope.keep_data_on_refresh = function () {
        $rootScope.grupo = JSON.parse(localStorage.getItem("grupo") || "[]");
        $scope.grupo_nome = localStorage.getItem('grupo_nome');
        $scope.grupo_global = localStorage.getItem('grupo_global');
        $scope.grupo_global = (localStorage.getItem('grupo_global') == 1) ? true : false;
        $scope.grupo_id = localStorage.getItem('grupo_id');
        $rootScope.participantes = JSON.parse(localStorage.getItem('participantes'));
        $scope.loaded = true;
        $scope.hide_load();
    };

    $scope.keep_data_on_refresh();
    $scope.carregar_grupos();
    $scope.carregar_membros();
});