var dtTicketsMod = angular.module('dtTicketsMod', []);

dtTicketsMod.controller('dtTicketsModCtrl', function ($compile, $scope, $rootScope, $http) {
    $scope.numbers = ['1', '2', '3', '4', '10', '30', '50'];
    $scope.isCurrent = false;
    $scope.loading = false;
    $scope.timeline_tarefas = [{
            id: 'aberto',
            nome: 'Aberto',
            iconClass: 'fa-flag',
            status: 0
        },
        {
            id: 'edicao',
            nome: 'Edição',
            iconClass: 'fa-paint-brush',
            status: 1
        },
        {
            id: 'revisao',
            nome: 'Revisão',
            iconClass: 'fa-eye',
            status: 2
        },
        {
            id: 'primeira-aprovacao',
            nome: 'Primeira Aprovação',
            iconClass: 'fa-thumbs-up',
            status: 1
        },
        {
            id: 'demais-aprovacoes',
            nome: 'Demais Aprovações',
            iconClass: 'fa-gavel',
            status: 1
        },
        {
            id: 'finalizacao',
            nome: 'Finalização',
            iconClass: 'fa-paint-brush',
            status: 1
        },
        {
            id: 'envio-arte',
            nome: 'Envio da Arte',
            iconClass: 'fa-paper-plane',
            status: 0
        },
        {
            id: 'pdf-fornecedor',
            nome: 'PDF do Fornecedor',
            iconClass: 'fa-industry',
            status: 2
        },
        {
            id: 'encerramento',
            nome: 'Encerramento',
            iconClass: 'fa-flag-checkered',
            status: 0
        }
    ];

    $scope.timeline_subtarefas = [{
            status: 1
        },
        {
            status: 1
        },
        {
            status: 1
        },
        {
            status: 2
        },
        {
            status: 0
        },
        {
            status: 0
        }
    ]

    $scope.tarefas = {};

    $scope.participante_tarefas_OLD = [{
            nome: 'Railton Costa',
            empresa: 'Ambev',
            departamento: 'Jurídico',
            dt_tarefa_concluida: '12:32 de 27/04/18',
            aprova_etapa: true,
            reprova_etapa: false,
            equipe_membros: []
        }, 
        {
            nome: 'Luis Craveiro',
            empresa: 'Ambev',
            departamento: 'Jurídico',
            dt_tarefa_concluida: '12:32 de 27/04/18',
            aprova_etapa: false,
            reprova_etapa: true,
            equipe_membros: []
        }, 
        {
            nome: 'Carlos Oliveira',
            empresa: 'Ambev',
            departamento: 'Jurídico',
            aprova_etapa: null,
            reprova_etapa: null,
            dt_tarefa_concluida: '12:32 de 27/04/18',
            equipe_membros: [{
                    nome: 'João da Silva 1',
                    empresa: 'Ambev',
                    departamento: 'Jurídico'
                },
                {
                    nome: 'João da Silva 2',
                    empresa: 'Ambev',
                    departamento: 'Jurídico'
                },
                {
                    nome: 'João da Silva 3',
                    empresa: 'Ambev',
                    departamento: 'Jurídico'
                },
                {
                    nome: 'João da Silva 4',
                    empresa: 'Ambev',
                    departamento: 'Jurídico'
                },
                {
                    nome: 'João da Silva 5',
                    empresa: 'Ambev',
                    departamento: 'Jurídico'
                }
            ]
        }, 
        {
            nome: 'Maurici Aragão 1',
            empresa: 'Ambev',
            departamento: 'Jurídico',
            aprova_etapa: null,
            reprova_etapa: null,
            dt_tarefa_concluida: '12:32 de 27/04/18',
            equipe_membros: []
        }, 
        {
            nome: 'Maurici Aragão 2',
            empresa: 'Ambev',
            departamento: 'Jurídico',
            aprova_etapa: null,
            reprova_etapa: null,
            dt_tarefa_concluida: '12:32 de 27/04/18',
            equipe_membros: []
        }, 
        {
            nome: 'Maurici Aragão 3',
            empresa: 'Ambev',
            departamento: 'Jurídico',
            aprova_etapa: null,
            reprova_etapa: null,
            dt_tarefa_concluida: '12:32 de 27/04/18',
            equipe_membros: []
        }, 
        {
            nome: 'Maurici Aragão 4',
            empresa: 'Ambev',
            departamento: 'Jurídico',
            aprova_etapa: null,
            reprova_etapa: null,
            dt_tarefa_concluida: '12:32 de 27/04/18',
            equipe_membros: []
        }, 
        {
            nome: 'Maurici Aragão 5',
            empresa: 'Ambev',
            departamento: 'Jurídico',
            aprova_etapa: null,
            reprova_etapa: null,
            dt_tarefa_concluida: '12:32 de 27/04/18',
            equipe_membros: []
        }, 
        {
            nome: 'Maurici Aragão 6',
            empresa: 'Ambev',
            departamento: 'Jurídico',
            aprova_etapa: null,
            reprova_etapa: null,
            dt_tarefa_concluida: '12:32 de 27/04/18',
            equipe_membros: []
        }
    ]

    $scope.tarefa = {};

    var dropdown_list_tpl = $compile(angular.element('<div class="dropdown-task-contents">' +
        '   <h4 class="dropdown-task-title">' +
        '       <i class="icon fa {{ tarefas.icone }}"></i>' +
        '       <span class="txt-task"><strong class="txt-bold">{{ tarefas.titulo }}</strong> &nbsp;&sol;&nbsp; <em class="txt-regular no-italic">{{ tarefas.subtitulo }}</em></span>' +
        '   </h4>' +
        '   <ul class="dropdown-menu-ul">' +
        '       <li ng-repeat="participante in tarefas.participantes">' +
        '           <i ng-class="{\'txt-color-alt\': tarefas.etapa_ativa === 0}" class="dropdown-task-icon fa {{participante.icone}}"></i>' +
        '           <span class="txt-small"><strong class="txt-semibold txt-small">{{participante.nome}}</strong> &bull; {{participante.empresa}}<strong class="txt-regular txt-small" ng-if="participante.departamento"> / {{participante.departamento}}</strong><strong ng-if="participante.dt_tarefa_concluida" class="txt-regular txt-small"> &bull; {{participante.dt_tarefa_concluida}}</strong></span>' +
        '           <ul>' +
        '               <li ng-repeat="membro in participante.membros">' +
        '                   <span class="txt-small"><strong class="txt-semibold txt-small">{{membro.nome}}</strong> &bull; {{membro.empresa}} / {{membro.departamento}}</span>' +
        '               </li>' +
        '           </ul>' +
        '       </li>' +
        '   </ul>' +
        '</div>'))($scope);
        
    $scope.dropdown_tarefas = function (id_ticket, etapa) {
        $scope.loading = true;
        $scope.tarefas = {
            titulo: '',
            subtitulo: '',
            icone: '',
            participantes: []
        };

        angular.element('.dropdown-task-contents').remove();
        angular.element('a.arrow-down').hide();

        $rootScope.getDropdownMenuHeight();
                        
        $.get('/site/artwork/services/package/ticket/getTimeline/' + etapa + '/' + id_ticket, function (data) {
            $scope.loading = false;
            $scope.tarefas = data;
            $scope.$apply();

            angular.element('.dropdown-task-contents').remove();
            angular.element('.dropdown-task-list').find('a.arrow-up').after(dropdown_list_tpl);

            $rootScope.getDropdownMenuHeight();
        }, 'json')
        .fail(function (data) {
            myAlert('Ocorreu um erro.', 'error');
        });
    };
});