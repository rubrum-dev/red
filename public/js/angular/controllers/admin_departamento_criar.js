var site_url = '/';
var app = angular.module('app', []);
app.controller('DepartamentoCriarCtrl', function ($scope) {
    $scope.nomeDepartamento = '';
    $scope.chkDepartamentoAtivo = true;
    $scope.show_load = function () {
        //$('.master').addClass('blurred');
        $('.lds-css').show();
    };
    $scope.hide_load = function () {
        //$('.master').removeClass('blurred');
        $('.lds-css').hide();
    };
    $scope.enviarFormulario = function (form) {
        $scope.show_load();
        params = {
            _token: csrf_token,
            nomeDepartamento: $scope.nomeDepartamento,
            chkDepartamentoAtivo: $scope.chkDepartamentoAtivo
        }
        $.post(site_url + 'admin/departamentos/salvar', params, function (data) {
            if (data.error) {
                myAlert(data.message, 'error');
                $scope.hide_load();
            } else {

                if (data.success == 201){
                    myAlert(data.message, 'sucess');
                    $scope.hide_load();
                    return;
                }

                $.confirm({
                    animationSpeed: 200,
                    animateFromElement: false,
                    draggable: false,
                    icon: 'fa fa-check',
                    title: 'Sucesso',
                    content: 'Departamento criado com sucesso.',
                    closeIcon: true,
                    boxWidth: '910px',
                    scrollToPreviousElement: false,
                    useBootstrap: false,
                    closeIconClass: 'jconfirm-btn-close fa fa-times',
                    buttons: {
                        Entendi: {
                            keys: ['esc', 'enter', 'space'],
                            text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                            btnClass: 'btn-confirm my-alert-confirm',
                            action: function (Todos) {
                                $('.lds-css').show(200, function () {
                                    location = data.url;
                                });
                            }
                        }
                    },
                    onOpen: function () {
                        //$('body').addClass('jconfirm-overlay');
                        $('body').addClass('no-scroll');
                        //$('.master').addClass('blurred');
                        $('.jconfirm').css('overflow', 'auto');
                        $scope.hide_load();
                    },
                    onClose: function () {
                        //$('body').removeClass('jconfirm-overlay');
                        $('body').removeClass('no-scroll');
                        //$('.master').removeClass('blurred');
                        $('.jconfirm').css('overflow', 'hidden');
                        $scope.hide_load();
                    }
                });
            }
        }, 'json')
        .fail(function (data) {
            myAlert('Ocorreu um erro.', 'error');
            $scope.hide_load();
        });
    }
});