var pdf_fornecedor = angular.module("abaPdfFornecedorMod", []);

pdf_fornecedor.controller('abaPdfFornecedorCtrl', function (
    $scope,
    $location,
    $http,
    $interval,
    $rootScope,
    $ngConfirm,
    $window,
    Upload,
    $timeout
) {
    $rootScope.statechange = true;
    
    $scope.add_pdf_fornecedor = function (id) {
        var file = document.getElementById('pdfFileUpload').files[0];

        params = {
            _token: csrf_token,
            id: $scope.ticket.id,
            pdf: file,
            fornecedor: $scope.fornecedorSelecionado
        };

        $scope.pdf.progress = 0;

        $scope.pdf.upload = Upload.upload({
            url: site_url + 'site/artwork/services/package/ticket/uploadFornecedorPdf',
            data: params
        });

        $scope.pdf.upload.then(function (response) {
            
            if (response.data.error)
            {
                
                myAlert(response.data.message);
                $scope.pdf = '';
                $scope.fornecedorSelecionado = '';
                $scope.limparCamposUpload();
                
            } else {
                
                $scope.pdf = '';
                $scope.fornecedorSelecionado = '';
                $scope.limparCamposUpload();
                myAlert('PDF do Fornecedor salvo com sucesso.', 'success');
                $scope.carregar_ticket(1,1);
                
            }
            
            
            
        }, function (response) {
            if (response.status > 0) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.pdf.progress = 0;
                $scope.interval_tickets_start();
            }
        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            $scope.pdf.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
    };

    $scope.upload_arquivo = function () {
        if (!$scope.pdf || !$scope.fornecedorSelecionado) {
            $('.col-files-attach').find('.files-control-fake').addClass('has-error');
            $('.col-select-supplier').find('select').addClass('has-error');
            myAlert('Anexe o PDF e selecione o fornecedor.');
            return false;
        }

        if ($scope.pdf.size > 52428800) {
            $('.col-files-attach').find('.files-control-fake').addClass('has-error');
            myAlert('O tamanho máximo permitido para um arquivo é 50 MB.', 'warning');
            return false;
        } else {
            $('.col-files-attach').find('.files-control-fake').removeClass('has-error');
            $('.col-select-supplier').find('select').removeClass('has-error');
            
            params = {
                _token: csrf_token,
                id_ticket: $scope.ticket.id,
                id_fornecedor: $scope.fornecedorSelecionado
            };
            
            $.post(site_url + 'site/artwork/services/package/ticket/checkFornecedorPdf', params, function (data) {
            
                if (data.error) {
                    
                    myAlert(data.message);
                    
                } else {
                    
                    $scope.add_pdf_fornecedor();
                    
                }
                
            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
            
        }
    };
    
    $scope.notificar_pdf_anexado_service = function(id_pdf) {
        // TO-DO
        $scope.show_load();
        
        params = {
            _token: csrf_token,
            id: id_pdf,
        };
        
        $.post(site_url + 'site/artwork/services/package/ticket/acaoNotificarPdfAnexado', params, function (data) {
                myAlert(data.message, 'success');
                $scope.carregar_ticket(1, 1);
            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
    };

    $scope.acao_pdf_service = function (id_pdf, acao) {
        
        $scope.show_load();
        
        params = {
            _token: csrf_token,
            id: id_pdf,
            acao: acao
        };
        
        $.post(site_url + 'site/artwork/services/package/ticket/acaoFornecedorPdf', params, function (data) {
            
                myAlert(data.message, 'success');
                $scope.carregar_ticket(1, 1);
                
            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
        
    };
    
    $scope.excluir_pdf_service = function (id_pdf) {
        
        $scope.show_load();
        
        params = {
            _token: csrf_token,
            id: id_pdf
        };
        
        $.post(site_url + 'site/artwork/services/package/ticket/excluirFornecedorPdf', params, function (data) {
            
                myAlert(data.message, 'success');
                $scope.carregar_ticket(1, 1);
                
            }, 'json')
            .fail(function (data) {
                myAlert('Ocorreu um erro.', 'error');
                $scope.hide_load();
            });
        
    };
    
    $scope.excluir_pdf = function (id_pdf) {
        $.confirm({
            icon: 'icon fa fa-question',
            title: 'Confirmar',
            content: 'Deseja excluir?',
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            useBootstrap: false,
            boxWidth: '910px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        
                        $scope.excluir_pdf_service(id_pdf);
                        
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {}
                }
            },
            onOpenBefore: function () {
                //$('body').addClass('jconfirm-overlay');
                $('body').addClass('no-scroll');
                //$('.master').addClass('blurred');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                //$('body').removeClass('jconfirm-overlay');
                $('body').removeClass('no-scroll');
                //$('.master').removeClass('blurred');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };

    $scope.aprovar_pdf = function (id_pdf) {
        $.confirm({
            icon: 'icon fa fa-question',
            title: 'Confirmar',
            content: 'Deseja aprovar?',
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            useBootstrap: false,
            boxWidth: '910px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        
                        $scope.acao_pdf_service(id_pdf, 'aprovar');
                        
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {}
                }
            },
            onOpenBefore: function () {
                //$('body').addClass('jconfirm-overlay');
                $('body').addClass('no-scroll');
                //$('.master').addClass('blurred');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                //$('body').removeClass('jconfirm-overlay');
                $('body').removeClass('no-scroll');
                //$('.master').removeClass('blurred');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };


    $scope.reprovar_pdf = function (id_pdf) {
        $.confirm({
            icon: 'icon fa fa-question',
            title: 'Confirmar',
            content: 'Ao reprovar esse PDF, o mesmo será excluído. Deseja confirmar?',
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            useBootstrap: false,
            boxWidth: '910px',
            buttons: {
                Confirmar: {
                    keys: ['enter', 'space'],
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn btn-confirm',
                    action: function (Confirmar) {
                        
                        $scope.acao_pdf_service(id_pdf, 'reprovar');
                        
                    }
                },
                Cancelar: {
                    keys: ['esc'],
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn btn-cancel',
                    action: function (Cancelar) {}
                }
            },
            onOpenBefore: function () {
                //$('body').addClass('jconfirm-overlay');
                $('body').addClass('no-scroll');
                //$('.master').addClass('blurred');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                //$('body').removeClass('jconfirm-overlay');
                $('body').removeClass('no-scroll');
                //$('.master').removeClass('blurred');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    };
    
    $scope.deleteArte = function (id) {
        $scope.show_load();

        params = {
            _token: csrf_token,
            id: id
        };

        $.post(site_url + 'site/artwork/services/package/ticket/deleteArte', params, function (data) {
            $scope.carregar_ticket(1);
            $scope.interval_tickets_start();
        }, 'json')
                .fail(function (data) {
                    myAlert('Ocorreu um erro.', 'error');
                    $scope.hide_load();
                });
    };

    $scope.customFileInputCtrl();
});