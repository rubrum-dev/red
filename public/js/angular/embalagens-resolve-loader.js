(function () {
    var embalagensResolveLoader = angular.module('embalagensResolveLoader', []);
    var html = '';

    html += '<div ng-if="!$root.loaded" class="content-placeholder-container">';
    html += '   <div class="flexbox-container margin-top-20 margin-bottom-10">';
    html += '       <div class="flex margin-left-auto">';
    html += '           <span class="btn-function no-hover"><i class="linear-background icon"></i><strong class="linear-background text"></strong></span>';
    html += '       </div>';
    html += '   </div>';
    html += '   <div class="flexbox-container flexbox-group">';
    html += '       <div class="flex flex-12-large">';
    html += '           <div class="sub-bs block bspacer-h">';
    html += '               <div class="table-placeholder">';
    html += '                   <div class="flexbox-container flex-column">';
    html += '                       <div class="table-thead-tr flexbox-container flexbox-group padding-top-5 padding-bottom-5 no-margin">';
    html += '                           <div class="table-thead-th d-flex linear-background"></div>';
    html += '                       </div>';
    html += '                   </div>';
    html += '                   <div class="flexbox-container flex-column">';

    for (var i = 0; i <= 8; i++) {
        html += '                       <div class="table-tbody-tr">';
        html += '                           <div class="table-tbody-td d-flex flex flex-fluid fill-avaliable flex-align-center"><span class="table-wrap d-block linear-background margin-right-10"></span><span class="btn-function no-hover"><i class="icon linear-background"></i></span></div>';
        html += '                       </div>';
    }

    html += '                   </div>';
    html += '               </div>';
    html += '           </div>';
    html += '       </div>';
    html += '   </div>';
    html += '</div>';

    embalagensResolveLoader.directive('resolveLoader', function ($rootScope) {
        return {
            restrict: 'A',
            replace: true,
            template: html,
            link: function (scope, element) {
                $rootScope.loaded = false;
            }
        };
    });
})();