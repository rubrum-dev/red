(function () {
    var enviarArteResolveLoader = angular.module('enviarArteResolveLoader', []);
    var html = '';

    html += '<div ng-if="!$root.loaded" class="content-placeholder-container ticket-placeholder-container margin-top-20">';
    html += '   <div class="workflow-files-approval flexbox-container">';
    html += '       <div class="flex flex-fluid">';
    html += '           <div class="block">';
    html += '               <div class="master-title-sub linear-background"></div>';
    html += '               <div class="file-panel linear-background"></div>';
    html += '           </div>';
    html += '           <div class="block">';
    html += '               <div class="master-title-sub linear-background"></div>';
    html += '               <div class="file-panel final-art-panel linear-background"></div>';
    html += '           </div>';
    html += '           <div class="block">';
    html += '               <div class="master-title-sub linear-background"></div>';
    html += '               <div class="input-control linear-background no-hover"></div>';
    html += '               <div class="input-control textbox-control linear-background no-hover margin-top-20"></div>';
    html += '               <div class="flexbox-container">';
    html += '                   <div class="btn-control linear-background no-hover margin-left-auto margin-top-20"></div>';
    html += '               </div>';
    html += '           </div>';
    html += '           <div class="block">';
    html += '               <div class="master-title-sub linear-background"></div>';
    html += '               <div class="file-panel linear-background"><span class="file-panel-options"></span></div>';
    html += '           </div>';
    html += '       </div>';
    html += '   </div>';
    html += '</div>';

    enviarArteResolveLoader.directive('resolveLoader', function ($rootScope) {
        return {
            restrict: 'A',
            replace: true,
            template: html,
            link: function (scope, element) {
                $rootScope.loaded = false;
            }
        };
    });
})();