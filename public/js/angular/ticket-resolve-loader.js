(function () {
    var ticketResolveLoader = angular.module('ticketResolveLoader', []);
    
    ticketResolveLoader.directive('resolveLoader', function ($rootScope) {
        return {
            restrict: 'A',
            replace: true,
            template: '<div ng-if="!statechange" class="content-placeholder-container ticket-placeholder-container margin-top-20">' +
                '<div ng-if="activetab === \'/visao-geral\'">' +
                    '<div class="workflow-general-info bs">' +
                        '<div class="flexbox-container">' +
                            '<div class="flex flex-fluid">' +
                                '<span class="workflow-general-info-title txt-small d-block linear-background margin-bottom-10"></span>' +
                                '<span class="workflow-general-info-txt d-block linear-background"></span>' +
                            '</div>' +
                            '<div class="flex flex-5-large">' +
                                '<div class="flexbox-container flexbox-group">' +
                                    '<div class="flex flex-6-large">' +
                                        '<span class="workflow-general-info-title txt-small d-block linear-background margin-bottom-10"></span>' +
                                        '<span class="workflow-general-info-txt d-block linear-background"></span>' +
                                    '</div>' +
                                    '<div class="flex flex-6-large">' +
                                        '<span class="workflow-general-info-title txt-small d-block linear-background margin-bottom-10"></span>' +
                                        '<span class="workflow-general-info-txt d-block linear-background"></span>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="flexbox-container flexbox-group margin-top-30">' +
                                    '<div class="flex flex-4-large">' +
                                        '<span class="workflow-general-info-title txt-small d-block linear-background margin-bottom-10"></span>' +
                                        '<span class="workflow-general-info-txt d-block linear-background"></span>' +
                                    '</div>' +
                                    '<div class="flex flex-4-large">' +
                                        '<span class="workflow-general-info-title txt-small d-block linear-background margin-bottom-10"></span>' +
                                        '<span class="workflow-general-info-txt d-block linear-background"></span>' +
                                    '</div>' +
                                    '<div class="flex flex-4-large">' +
                                        '<span class="workflow-general-info-title txt-small d-block linear-background margin-bottom-10"></span>' +
                                        '<span class="workflow-general-info-txt d-block linear-background">&nbsp;</span>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="workflow-instructions-cycle margin-top-30">' +
                        '<div class="master-title-sub linear-background"></div>' +
                        '<div class="instruction sub-bs">' +
                            '<div class="col-instruction-type linear-background"></div>' +
                            '<div class="col-instruction-description">' +
                                '<span class="user-request-title d-block linear-background margin-bottom-10">&nbsp;</span>' + 
                                '<span class="comment-text-block d-block linear-background margin-bottom-15">&nbsp;</span>' +
                                '<span class="btn-control linear-background">&nbsp;</span>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="workflow-files-approval flexbox-container">' +
                        '<div class="flex flex-fluid">' +
                            '<div class="block">' +
                                '<div class="master-title-sub linear-background"></div>' + 
                                '<div class="file-panel linear-background"><span class="file-panel-options"></span></div>' +
                            '</div>' +
                            '<div class="block">' + 
                                '<div class="master-title-sub linear-background"></div>' +
                                '<div class="file-panel linear-background"><span class="file-panel-options"></span></div>' +
                            '</div>' +
                            '<div class="block">' +
                                '<div class="master-title-sub linear-background"></div>' +
                                '<div class="file-panel linear-background"><span class="file-panel-options"></span></div>' +
                            '</div>' +
                            '<div class="block">' +
                                '<div class="master-title-sub linear-background"></div>' +
                                '<div class="file-panel linear-background"><span class="file-panel-options"></span></div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div ng-if="activetab === \'/anexos\'">' +
                    '<div class="files-list-container">' + 
                        '<div class="files-list-item">' +
                            '<i class="icon linear-background"></i>' + 
                            '<div class="files-list-body">' +
                                '<strong class="files-list-item-heading linear-background"></strong>' +
                                '<small class="files-list-item-caption attached-by-txt linear-background"></small>' +
                                '<small class="files-list-item-caption file-info-txt linear-background"></small>' +
                            '</div>' +
                        '</div>' +
                        '<div class="files-list-item">' +
                            '<i class="icon linear-background"></i>' + 
                            '<div class="files-list-body">' +
                                '<strong class="files-list-item-heading linear-background"></strong>' +
                                '<small class="files-list-item-caption attached-by-txt linear-background"></small>' +
                                '<small class="files-list-item-caption file-info-txt linear-background"></small>' +
                            '</div>' +
                        '</div>' +
                        '<div class="files-list-item">' +
                            '<i class="icon linear-background"></i>' + 
                            '<div class="files-list-body">' +
                                '<strong class="files-list-item-heading linear-background"></strong>' +
                                '<small class="files-list-item-caption attached-by-txt linear-background"></small>' +
                                '<small class="files-list-item-caption file-info-txt linear-background"></small>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div ng-if="activetab === \'/participantes\'">' +
                    '<div class="workflow-membership-table table-placeholder">' + 
                        '<div class="flexbox-container flex-column">' +
                            '<div class="table-thead-tr flexbox-container padding-top-5 padding-bottom-5 no-magin">' +
                                '<div class="table-thead-th d-flex linear-background"></div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="flexbox-container flex-column">' +
                            '<div class="table-tbody-tr">' +
                                '<div class="table-tbody-td d-flex flex flex-fluid fill-avaliable flex-align-center"><span class="table-wrap d-block linear-background margin-right-10"></span><span class="btn-function no-hover"><i class="icon linear-background"></i></span></div>' +
                            '</div>' +
                            '<div class="table-tbody-tr">' +
                                '<div class="table-tbody-td d-flex flex flex-fluid fill-avaliable flex-align-center"><span class="table-wrap d-block linear-background margin-right-10"></span><span class="btn-function no-hover"><i class="icon linear-background"></i></span></div>' +
                            '</div>' +
                            '<div class="table-tbody-tr">' + 
                                '<div class="table-tbody-td d-flex flex flex-fluid fill-avaliable flex-align-center"><span class="table-wrap d-block linear-background margin-right-10"></span><span class="btn-function no-hover"><i class="icon linear-background"></i></span></div>' +
                            '</div>' +
                            '<div class="table-tbody-tr">' + 
                                '<div class="table-tbody-td d-flex flex flex-fluid fill-avaliable flex-align-center"><span class="table-wrap d-block linear-background margin-right-10"></span><span class="btn-function no-hover"><i class="icon linear-background"></i></span></div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div ng-if="activetab === \'/tarefas\'">' +
                    '<div class="tasks-container">' +
                        '<div class="task">' +
                            '<i class="icon linear-background"></i>' + 
                            '<div class="body">' +
                                '<div class="event-title"><span class="linear-background"></span></div>' +
                                '<div class="event-caption"><span class="linear-background"></span></div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="task">' +
                            '<i class="icon linear-background"></i>' + 
                            '<div class="body">' +
                                '<div class="event-title"><span class="linear-background"></span></div>' +
                                '<div class="event-caption"><span class="linear-background"></span></div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="task">' +
                            '<i class="icon linear-background"></i>' + 
                            '<div class="body">' +
                                '<div class="event-title"><span class="linear-background"></span></div>' +
                                '<div class="event-caption"><span class="linear-background"></span></div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="task">' +
                            '<i class="icon linear-background"></i>' + 
                            '<div class="body">' +
                                '<div class="event-title"><span class="linear-background"></span></div>' +
                                '<div class="event-caption"><span class="linear-background"></span></div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="task">' +
                            '<i class="icon linear-background"></i>' + 
                            '<div class="body">' +
                                '<div class="event-title"><span class="linear-background"></span></div>' +
                                '<div class="event-caption"><span class="linear-background"></span></div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="task">' +
                            '<i class="icon linear-background"></i>' + 
                            '<div class="body">' +
                                '<div class="event-title"><span class="linear-background"></span></div>' +
                                '<div class="event-caption"><span class="linear-background"></span></div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="task">' +
                            '<i class="icon linear-background"></i>' + 
                            '<div class="body">' +
                                '<div class="event-title"><span class="linear-background"></span></div>' +
                                '<div class="event-caption"><span class="linear-background"></span></div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="task">' +
                            '<i class="icon linear-background"></i>' + 
                            '<div class="body">' +
                                '<div class="event-title"><span class="linear-background"></span></div>' +
                                '<div class="event-caption"><span class="linear-background"></span></div>' +
                            '</div>' +
                        '</div>' + 
                        '<div class="task">' +
                            '<i class="icon linear-background"></i>' + 
                            '<div class="body">' +
                                '<div class="event-title"><span class="linear-background"></span></div>' +
                                '<div class="event-caption"><span class="linear-background"></span></div>' +
                            '</div>' +
                        '</div>' + 
                    '</div>' +
                '</div>' +
                '<div ng-if="activetab === \'/mensagens\'">' + 
                    '<div class="flexbox-container">' + 
                        '<span class="btn-function no-hover flex margin-left-auto"><i class="icon linear-background"></i><stong class="text linear-background"></strong></span>' +
                    '</div>' +
                    '<div class="flexbox-container">' +
                        '<div class="chat-comment-list flex flex-fluid">' + 
                            '<div class="comment clearfix">' +
                                '<i class="icon linear-background"></i>' + 
                                '<span class="user-request-title d-block linear-background margin-top-5 margin-bottom-10"></span>' +
                                '<span class="comment-text-block d-block linear-background margin-bottom-15"></span>' +
                                '<span class="btn-control no-hover linear-background"></span>' +
                            '</div>' +
                            '<div class="comment clearfix">' + 
                                '<i class="icon linear-background"></i>' + 
                                '<span class="user-request-title d-block linear-background margin-top-5 margin-bottom-10"></span>' +
                                '<span class="comment-text-block d-block linear-background margin-bottom-15"></span>' +
                                '<span class="btn-control no-hover linear-background"></span>' +
                            '</div>' +
                            '<div class="comment clearfix">' +
                                '<i class="icon linear-background"></i>' +
                                '<span class="user-request-title d-block linear-background margin-top-5 margin-bottom-10"></span>' +
                                '<span class="comment-text-block d-block linear-background margin-bottom-15"></span>' +
                                '<span class="btn-control no-hover linear-background"></span>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div ng-if="activetab === \'/pdf-fornecedor\'">' +
                    '<div class="flexbox-containter">' +
                        '<div class="flex flex-fluid">' +
                            '<div class="file-panel block linear-background"><span class="file-panel-options"></span></div>' +
                            '<div class="file-panel block linear-background"><span class="file-panel-options"></span></div>' +
                            '<div class="file-panel block linear-background"><span class="file-panel-options"></span></div>' +
                            '<div class="file-panel block linear-background"><span class="file-panel-options"></span></div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div ng-if="activetab === \'/opcoes\'">' + 
                    '<ul class="list-group-indent">' +
                        '<li class="list-item-horizontal">' + 
                            '<div class="indent">' +
                                '<div class="pull"><i class="icon linear-background"></i><div class="text linear-background"></div></div>' +
                            '</div>' +
                        '</li>' +
                        '<li class="list-item-horizontal">' + 
                            '<div class="indent">' +
                                '<div class="pull"><i class="icon linear-background"></i><div class="text linear-background"></div></div>' +
                            '</div>' +
                        '</li>' +
                        '<li class="list-item-horizontal">' + 
                            '<div class="indent">' +
                                '<div class="pull"><i class="icon linear-background"></i><div class="text linear-background"></div></div>' +
                            '</div>' +
                        '</li>' +
                        '<li class="list-item-horizontal">' + 
                            '<div class="indent">' +
                                '<div class="pull"><i class="icon linear-background"></i><div class="text linear-background"></div></div>' +
                            '</div>' +
                        '</li>' +
                        '<li class="list-item-horizontal">' + 
                            '<div class="indent">' +
                                '<div class="pull"><i class="icon linear-background"></i><div class="text linear-background"></div></div>' +
                            '</div>' +
                        '</li>' +
                        '<li class="list-item-horizontal">' + 
                            '<div class="indent">' +
                                '<div class="pull"><i class="icon linear-background"></i><div class="text linear-background"></div></div>' +
                            '</div>' +
                        '</li>' +
                    '</ul>' +
                '</div>' +
            '</div>',
            link: function (scope, element) {
                $rootScope.statechange = true;
                element.addClass('ng-hide');

                var unregister = $rootScope.$on('$routeChangeStart', function () {
                    $rootScope.statechange = false;
                    element.removeClass('ng-hide');
                });

                scope.$on('$destroy', unregister);
            }
        };
    });
})();