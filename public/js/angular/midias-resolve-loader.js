(function () {
    var midiasResolveLoader = angular.module('midiasResolveLoader', []);
    var html = '';

    html += '<div ng-if="!$root.loaded" class="content-placeholder-container">';
    html += '   <div class="flexbox-container margin-top-20 margin-bottom-20">';
    html += '       <div class="d-inline-flex flex margin-left-auto">';
    html += '           <span class="btn-function no-hover"><i class="linear-background icon"></i><strong class="linear-background text"></strong></span>';
    html += '       </div>';
    html += '   </div>';
    html += '   <div class="flexbox-container flexbox-group">';
    html += '       <div class="flex flex-12-large">';
    html += '           <div class="block">';
    html += '               <div class="table-placeholder">';
    html += '                   <div class="flexbox-container flex-column">';

    for (var i = 0; i < 9; i++) {
        html += '                       <div class="table-tbody-tr">';
        html += '                           <div class="table-tbody-td d-flex flex flex-fluid fill-avaliable flex-align-center"><span class="table-wrap d-block linear-background margin-right-10"></span><span class="btn-function no-hover"><i class="icon linear-background"></i></span></div>';
        html += '                       </div>';
    }

    html += '                   </div>';
    html += '               </div>';
    html += '           </div>';
    html += '       </div>';
    html += '   </div>';
    html += '</div>';

    midiasResolveLoader.directive('resolveLoader', function ($rootScope) {
        return {
            restrict: 'A',
            replace: true,
            template: html,
            link: function (scope, element) {
                $rootScope.loaded = false;
            }
        };
    });
})();