(function () {
    var dashboardResolveLoader = angular.module('dashboardResolveLoader', []);

    dashboardResolveLoader.directive('resolveLoader', function($rootScope, $timeout) {
        return {
            restrict: 'A',
            replace: true,
            template: '<div ng-if="!dashboard.dados" class="content-placeholder-container dashboard-placeholder">' +
            '   <div ng-if="activetab === \'/\' || activetab === \'/indicadores-gerais\'" class="flexbox-container margin-bottom-20">' +
            '       <div class="d-flex flex flex-align-center margin-left-auto">' +
            '           <span class="linear-background updated-at-placeholder"></span>' +
            '           <span class="btn-function-placeholder"><i class="linear-background icon"></i></span>' +
            '       </div>' +
            '   </div>' +
            '   <div ng-if="activetab === \'/\'" class="cards-placeholder-container">' +
            '       <div class="flexbox-container flexbox-group margin-bottom-20">' +
            '           <div class="flex flex-12-large">' +
            '               <div class="linear-background card"></div>' +
            '           </div>' +
            '       </div>' +
            '       <div class="flexbox-container flexbox-group margin-bottom-20">' +
            '           <div class="flex flex-12-large">' +
            '               <div class="linear-background card"></div>' +
            '           </div>' +
            '       </div>' +
            '       <div class="flexbox-container flexbox-group margin-bottom-20">' +
            '           <div class="flex flex-12-large">' +
            '               <div class="linear-background card"></div>' +
            '           </div>' +
            '       </div>' +
            '       <div class="flexbox-container flexbox-group margin-bottom-20">' +
            '           <div class="flex flex-12-large">' +
            '               <div class="linear-background card"></div>' +
            '           </div>' +
            '       </div>' +
            '   </div>' +
            '   <div ng-if="activetab === \'/indicadores-gerais\'" class="cards-placeholder-container">' +
            '       <div class="flexbox-container flexbox-group margin-bottom-20">' +
            '           <div class="flex flex-12-large">' +
            '               <div class="linear-background card"></div>' +
            '           </div>' +
            '       </div>' +
            '       <div class="flexbox-container flexbox-group margin-bottom-20">' +
            '           <div class="flex flex-6-large">' +
            '               <div class="linear-background card"></div>' +
            '           </div>' +    
            '           <div class="flex flex-6-large">' +
            '               <div class="linear-background card"></div>' +
            '           </div>' +
            '       </div>' +
            '       <div class="flexbox-container flexbox-group margin-bottom-20">' +
            '           <div class="flex flex-12-large">' +
            '               <div class="linear-background card"></div>' +
            '           </div>' +
            '       </div>' +
            '       <div class="flexbox-container flexbox-group margin-bottom-20">' +
            '           <div class="flex flex-6-large">' +
            '               <div class="linear-background card"></div>' +
            '           </div>' +    
            '           <div class="flex flex-6-large">' +
            '               <div class="linear-background card"></div>' +
            '           </div>' +
            '       </div>' +
            '   </div>' +
            '   <div ng-if="activetab === \'/relatorios\'">' +
            '       <div class="flexbox-container">' +
            '           <ul class="list-group-indent">' +
            '               <li class="list-item-horizontal">' +
            '                   <div class="indent">' +
            '                       <div class="pull">' +
            '                           <i class="icon linear-background"></i>' +
            '                           <div class="text linear-background"></div>' +
            '                       </div>' +
            '                   </div>' +
            '               </li>' +
            '               <li class="list-item-horizontal">' +
            '                   <div class="indent">' +
            '                       <div class="pull">' +
            '                           <i class="icon linear-background"></i>' +
            '                           <div class="text linear-background"></div>' +
            '                       </div>' +
            '                   </div>' +
            '               </li>' +
            '           </ul>' +
            '       </div>' +
            '   </div>' +
            '   <div ng-if="$root.activenav === \'/relatorios/tickets-encerrados-por-participante-e-funcao\'">' +
            '       <div class="relatorio-header flex-align-center flexbox-container margin-top-20 margin-bottom-20 margin-left-10 margin-right-10">' +
            '            <figure class="linear-background figure flex"></figure>' +
            '            <span class="linear-background flex relatorio-txt txt-semibold"></span>' +
            '       </div>' +
            '       <div class="flexbox-container margin-top-20">' +
            '           <div class="flex flex-fluid">' +    
            '               <div class="linear-background relatorio-content"></div>' +
            '           </div>' +
            '       </div>' +
            '   </div>' +
            '   <div ng-if="$root.activenav === \'/relatorios/tickets-em-andamento-por-participante-e-funcao\'">' +
            '       <div class="relatorio-header flex-align-center flexbox-container margin-top-20 margin-bottom-20 margin-left-10 margin-right-10">' +
            '            <figure class="linear-background figure flex"></figure>' +
            '            <span class="linear-background flex relatorio-txt txt-semibold"></span>' +
            '       </div>' +
            '       <div class="flexbox-container margin-top-20">' +
            '           <div class="flex flex-fluid">' +    
            '               <div class="linear-background relatorio-content"></div>' +
            '           </div>' +
            '       </div>' +
            '   </div>' +
            '</div>',
            link: function(scope, element) {
                $rootScope.loading = false;
    
                element.addClass('ng-hide');
                
                $rootScope.$on('$routeChangeStart', function(event, currentRoute, previousRoute) {
                    if (previousRoute) return;
    
                    $timeout(function() {
                        $rootScope.loading = true;
                        element.removeClass('ng-hide');
                    });
                });
    
                $rootScope.$on('$routeChangeSuccess', function() {
                    $timeout(function() {
                        $rootScope.loading = false;
                        element.addClass('ng-hide');
                    });
                });
            }
        };
    });
})();