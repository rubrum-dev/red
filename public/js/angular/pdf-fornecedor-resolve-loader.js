(function () {
    var pdfFornecedorResolveLoader = angular.module('pdfFornecedorResolveLoader', []);
    var html = '';

    html += '<div ng-if="!$root.loaded" class="content-placeholder-container ticket-placeholder-container margin-top-30">';
    html += '   <div class="workflow-files-approval flexbox-container">';
    html += '       <div class="flex flex-fluid">';
    html += '           <div class="master-title-sub linear-background margin-bottom-20"></div>';
    html += '           <div class="block">';
    html += '               <div class="input-control linear-background no-hover"></div>';
    html += '           </div>';

    for (var i = 0; i <= 1; i++) {
        html += '       <div class="block">';
        html += '           <div class="supplier-pdf-panel file-panel linear-background"><span class="file-panel-options"></span></div>';
        html += '       </div>';
    }

    html += '       </div>';
    html += '   </div>';
    html += '</div>';

    pdfFornecedorResolveLoader.directive('resolveLoader2', function ($rootScope) {
        return {
            restrict: 'A',
            replace: true,
            template: html,
            link: function (scope, element) {
                $rootScope.loaded = false;
            }
        };
    });
})();