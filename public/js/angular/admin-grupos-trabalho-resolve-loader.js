(function () {
    var adminGruposTrabalhoResolveLoader = angular.module('adminGruposTrabalhoResolveLoader', []);
    
    var html = '';

    html += '<div ng-if="!$root.loaded" class="content-placeholder-container dataTables_wrapper">';
    html += '    <div class="dt-loading-indicator fadein">';
    html += '        <div class="flexbox-container margin-bottom-15">';
    html += '            <div class="flex flex-3-large">';
    html += '                <div class="linear-background dt-filter-search"></div>';
    html += '            </div>';
    html += '            <div class="flex margin-left-auto">';
    html += '                <div class="btn-function"><i class="icon linear-background"></i><span class="text linear-background"></span></div>';
    html += '            </div>';
    html += '        </div>';
    html += '        <div style="height:24px;" class="dt-thead-container flexbox-container flexbox-group flex-align-center">';
    html += '            <div class="flex flex-12-large">';
    html += '                <div class="table-thead-tr no-margin-top no-margin-bottom"><div class="table-thead-th linear-background"></div></div>';
    html += '            </div>';
    html += '        </div>';
    html += '        <div class="dt-tbody-container flexbox-container flexbox-group">';
    html += '            <div class="flex flex-12-large">';

    for (var i = 0; i <= 8; i++) {
        html += '<div class="table-tbody-tr"><i class="dt-column-ico margin-left-10 linear-background"></i><div class="dt-column-txt linear-background"></div></div>';
    }

    html += '            </div>';
    html += '        </div>';
    html += '        <div class="flexbox-container flex-align-center margin-top-15">';
    html += '            <div class="flex flex-2-large margin-right-auto">';
    html += '                <div class="dt-pagination-length linear-background"></div>';
    html += '            </div>';
    html += '            <div class="flex flex-3-large margin-left-auto">';
    html += '                <div class="dt-pagination-info-txt linear-background"></div>';
    html += '            </div>';
    html += '        </div>';
    html += '    </div>';
    html += '</div>';

    adminGruposTrabalhoResolveLoader.directive('resolveLoader', function ($rootScope) {
        return {
            restrict: 'A',
            replace: true,
            template: html,
            link: function (scope, element) {
                $rootScope.loaded = false;
                element.addClass('ng-hide');

                var unregister = $rootScope.$on('$routeChangeStart', function () {
                    $rootScope.loaded = true;
                    element.removeClass('ng-hide');
                });

                scope.$on('$destroy', unregister);
            }
        };
    });
})();