var url = window.location.href;
var arr = url.split("/");
var path = arr[0] + "//" + arr[2];

window.path = path;

$(function () {
	$('input.minicolors').minicolors({
		theme: 'bootstrap'
	});

	$('form').validate({
		rules: {
			nome: {
				required: true,
				normalizer: function(value) {
					// Note: the value of `this` inside the `normalizer` is the corresponding
					// DOMElement. In this example, `this` reference the `username` element.
					// Trim the value of the input
					return $.trim(value);
				}
			},
			ordem: {
				required: true
			}
		},
		messages: {
			nome: {
				required: 'Nome é obrigatório.'
			},
			ordem: {
				required: 'Ordem de Exibição é obrigatório.'
			}
		}
	});
		
	$('#btn-create').click(function () {
		window.id = null;
                
        $('input.minicolors').minicolors('value', '');
		$('form').validate().resetForm();
		$('#box-ordem').hide();
		$(".modal-title").text("Nova Marca");
		$('input[name=id]').val('');
		$('input[name=nome]').val('');
        $('input[name=cor_hexa]').val('');
		$('#editStatus').val('1');
		$('#editStatus').prop('checked', true);
		$('#list-tipos-logos .image-only').removeClass('has-uploaded');
		$('#list-tipos-logos .image-only').removeAttr('style');
		$('#list-tipos-logos .ui-tooltip-2').attr("data-title", 'Nenhum arquivo.');
			
		$('#list-tipos-logos').find('img').each(function () {
			var img = $(this);
			$(this).attr("src", window.path + '');
			
			$(this).error(function () {
				$(this).css('display', 'none')
			});
		});

		$('#list-tipos-logos').find('input[type="file"]').each(function () {
			$(this).val('');
		});
	});

	$('#list-familias .btn-edit').on('click', function (e) {
		e.preventDefault();

		$('form').validate().resetForm();
		$('#box-ordem').show();
		$('.modal-loading').show();
		$('.modalBtnGroup input[type="submit"]').prop('disabled', true);
		$('input.minicolors').minicolors('value', '');

		var btn = $(this).attr('id');
		var last = $('#editOrdem option:last-child').val();

		window.id = $('#' + btn).data('familia-id');

		$('#list-tipos-logos .image-only').css('background', '');
		$('#list-tipos-logos .ui-tooltip-2').attr("data-title", 'Nenhum arquivo.');
		$('#list-tipos-logos .image-only').removeClass('has-uploaded');
		$('#list-tipos-logos .image-only').removeClass('image-broken');
		$('#list-tipos-logos').find('img').css('display', 'none');
		
		$('#list-tipos-logos').find('input[type="file"]').each(function () {
			$(this).val('');
		});

		$.ajax({
			type: 'get',
			url: window.path + '/admin/family/edit/' + window.id,
			context: document.body
		}).done(function (data) {
			$('#editId').val(data.editFamily.id);
			$('#editNome').val(data.editFamily.nome);
			$('#cor_hexa').val(data.editFamily.cor_hexa);
			$('input.minicolors').minicolors('value', data.editFamily.cor_hexa);
			$('.modalBtnGroup input[type="submit"]').prop('disabled', false);

			$('#list-tipos-logos').find('img').load(function () {
				$('.modal-loading').hide();
			});

			$('#list-tipos-logos').find('img').each(function () {
				$(this).attr("src", window.path + '');
				$(this).error(function () {
					$(this).css('display', 'none');
					$(this).parents('.image-only').addClass('image-broken');
					$('.modal-loading').hide();
				});
			});

			if (data.editFamily.ordem !== null) {
				$('#editOrdem').val(data.editFamily.ordem - 1);
			} else {
				$('#editOrdem').val(last);
			}
			
			if (data.editFamily.status == 1) {
				$('#editStatus').prop('checked', true);
			} else {
				$('#editStatus').prop('checked', false);
			}

			$(data.editFamily.logotipos).each(function (index, value) {
				$('#list-tipos-logos').find('img').each(function () {
					if (value.id_tipo_logotipo == $(this).attr('id')) {
						$(this).attr("src", window.path + value.thumb);
						$(this).css('display', 'block');
						$('#list-tipos-logos .image-only').removeClass('image-broken');
						$('#list-tipos-logos .image-only').addClass('has-uploaded');
						$('#list-tipos-logos .image-only').css('background', data.editFamily.cor_hexa)
						$('#list-tipos-logos .ui-tooltip-2').attr("data-title", value.thumb.substring(value.thumb.lastIndexOf('/')+1));
					}
				});
			});
		});

		$(".modal-title").text("Editar Marca");
	});

	$('#list-familias .btn-remove').on('click', function (e) {
		e.preventDefault();

		if (confirm("Deseja realmente remover essa familia? \n\n* ATENÇÃO: Todos os Produtos vinculados a essa família serão removidos!") == true) {
			var btn = $(this).attr('id');
			window.id = $('#' + btn).data('familia-id');

			$('.lds-css').show();
			
			$.ajax({
				type: 'get',
				url: window.path + '/admin/family/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					$('.lds-css').fadeOut('200', function() {
						alert('Marca Removida com Sucesso');
						window.location.href = window.path + '/admin/family/';
					});
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();

		var resp = window.id ? 'Marca Atualizada com Sucesso' : 'Marca Criada com Sucesso';

		if (!$(this).valid()) {
			return false;
		}

		$(this).validate().resetForm();
		$('.modal').modal('hide');

		$.ajax({
			url: window.id ? window.path + '/admin/family/update/' + window.id : window.path + '/admin/family/create',
			data: new FormData($("#form-family")[0]),
			type: 'post',
			processData: false,
			contentType: false,
			xhr: function () {
				var loading = 0;
				// get the native XmlHttpRequest object
				var xhr = $.ajaxSettings.xhr();
				// set the onprogress event handler
				xhr.upload.onprogress = function (evt) {
					loading = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
					$('#packShelfImgProgress').show();
					$('#svg').find('path').show();
					$('#svg').find('path').attr('stroke-dasharray', loading * 2.5 + ' ' + 250.2);
					$('#count').text(loading + '%');
				};
				// set the onload event handler
				xhr.upload.onload = function () {
					if (xhr.readyState === 4) {
						$('#packShelfImgProgress').hide();
						$('#svg').find('path').hide();
					}
				};
				// return the customized object
				return xhr;
			},
			success: function (data) {
				if (data.error) {
					$('#packShelfImgProgress').hide();
					$('#svg').find('path').hide();
					alert(data.message);
					return false;
				} else {
					$('#packShelfImgProgress').hide();
					$('#svg').find('path').hide();
					alert(resp);
					window.location.reload();
				}
			}
		});
	});

	$('.modal').on('show.bs.modal', function () {
		$('.modal').overlayScrollbars({
			autoUpdate: true
		});
	});
	
	$('.modal').on('hidden.bs.modal', function () {
		$('input[type=file]').val('');
		$('form').validate().resetForm();
		$('#list-tipos-logos .image-only').removeClass('has-uploaded');
		$('#list-tipos-logos .image-only').removeClass('image-broken');
	});
});