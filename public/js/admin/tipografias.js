window.path = window.location.origin;

$(function () {
	$("form").validate({
		rules:{
			nome:{
				required: true
			},
			id_tipo_tipografia:{
				required: true
			}
		},
		messages:{
			nome:{
				required: "Digite o nome da tipografia"
			},
			id_tipo_tipografia:{
				required: "Selecione um tipo de tipografia"
			}
		}
	});

	$('#txt-search').keyup(function(e){
        if(e.keyCode == 13)
        {
            $( "#btn-search" ).trigger( "click" );
        }
    });

	$('#list-typography').tablesorter({
        headers: {
            3: { sorter: false }
        }
    });

	$('#btn-create').click(function () {
		window.id = null;
		$('form').validate().resetForm();
		$(".modal-title").text("Nova Tipografia");
		$('input[name=id]').val('');
		$('input[name=nome]').val('');
		$('input[name=link]').val('');
		$('textarea').val('');
		$('#editStatus').val('1');
		$('select option:contains("Selecione o Tipo...")').prop('selected',true);
		$('#listImage img').attr("src", window.path + '/images/backend/noImage.png');
		$('#listFile img').attr("src", window.path + '/images/backend/noFile.png');
		$('#listImage img').width("70px");
		$('#listImage').find('input[type="file"]').each(function() {
		    $(this).val('');
		});
		$('#listFile').find('input[type="file"]').each(function() {
		    $(this).val('');
		});
	});

	$('#list-typography .btn-edit').on('click', function (e) {
		e.preventDefault();
		$('form').validate().resetForm();
		var btn = $(this).attr('id');
		window.id = $('#'+btn).data('typography-id');
		$('#listImage img').attr("src", window.path + '/images/backend/noImage.png');
		$('#listFile img').attr("src", window.path + '/images/backend/noFile.png');
		$('#listImage img').width("70px");
		$('#editLink').val('');
		$('#list-tipos-logos').find('input[type="file"]').each(function() {
		    $(this).val('');
		});
		$.ajax({
			type: 'get',
			url: window.path + '/admin/typography/edit/' + window.id,
			context: document.body
		}).done(function(data){
			$('#editId').val(data.editTypography.id);
			$('#editNome').val(data.editTypography.nome);
			$('#editTipo').val(data.editTypography.id_tipo_tipografia);
			$('#editLink').val(data.editTypography.link);
			$('#editDescricao').val(data.editTypography.descricao);
			$('#editStatus').val(data.editTypography.status);
			if(data.editTypography.thumb){
				$('#listImage img').attr("src", window.path + data.editTypography.thumb);
				$('#listImage img').width("300px");
			}
			if(data.editTypography.path_down){
				$('#listFile img').attr("src", window.path + '/images/backend/zipFile.png');
			}
		});
		$(".modal-title").text("Editar Tipografia");
	});

	$('#list-typography .btn-remove').on('click', function (e) {
		e.preventDefault();
		if (confirm("Deseja realmente remover essa tipografia?") == true) {
			var btn = $(this).attr('id');
			window.id = $('#'+btn).data('typography-id');
			$.ajax({
				type: 'get',
				url: window.path + '/admin/typography/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					alert('Tipografia Removida com Sucesso');
					window.location.href = window.path + '/admin/typography/';
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();
		var resp = window.id ? 'Tipografia Atualizada com Sucesso' : 'Tipografia Criada com Sucesso';
		$.ajax({
			url: window.id ? window.path + '/admin/typography/update/' + window.id : window.path + '/admin/typography/create',
			data:new FormData($("#form-typography")[0]),
			type:'post',
			processData: false,
			contentType: false,
			success: function (data) {
				if(data.error){
					alert(data.message);
					return false;
				}
				else {
					alert(resp);
					window.location.reload();
				}
			}
		});
	});

});
