window.path = window.location.origin;

$(function () {
	$("form").validate({
		rules:{
			nome:{
				required: true
			},
			sobrenome:{
				required: true
			},
			email:{
				required: true, email: true
			},
			id_adm_perfil: {
                required: true
            },
            id_adm_empresa: {
            	required: true
            }
		},
		messages:{
			nome:{
				required: "Digite o nome"
			},
			sobrenome:{
				required: "Digite o sobrenome"
			},
			email:{
				required: "Digite o e-mail",
				email: "Digite um e-mail válido"
			},
			id_adm_perfil:{
				required: "Selecione um perfil"
			},
			id_adm_empresa:{
				required: "Selecione uma empresa"
			}
		}
	});

	$('#txt-search').keyup(function(e){
        if(e.keyCode == 13)
        {
            $( "#btn-search" ).trigger( "click" );
        }
    });

	$('#list-users').tablesorter({
        headers: {
            5: { sorter: false }
        }
    });

	// $("#editFone").mask("(99) 9999-9999");

	$("#editFone").inputmask({
        mask: ["(99) 9999-9999", "(99) 99999-9999", ],
        keepStatic: true
    });

	$('#btn-create').click(function () {
		window.id = null;
		$('form').validate().resetForm();
		$(".modal-title").text("Novo Usuário");
		$('input[name=id]').val('');
		$('input[name=nome]').val('');
		$('input[name=sobrenome]').val('');
		$('input[name=email]').val('');
		$('input[name=email]').prop('disabled', false);
		$('input[name=fone]').val('');
		$('input[name=cargo]').val('');
		$('input[name=depto]').val('');
		$('input[name=token_guide]').val('');
		$('#listModulos').hide();
		$('select option:contains("Selecione o Perfil...")').prop('selected',true);
		$('select option:contains("Selecione a Empresa...")').prop('selected',true);
		$('#editStatus').val('1');
		$('input[type="checkbox"]').each(function( index, value ) {
			$('#'+value.id).prop("checked", false);
		});
	});

	$('#btn-cancel-modal').click(function () {
		$('input[type="checkbox"]').each(function( index, value ) {
			$('#'+value.id).prop("checked", false);
		});
	});

	$('#list-users .btn-edit').on('click', function (e) {
		e.preventDefault();
		$('form').validate().resetForm();
		var btn = $(this).attr('id');
		window.id = $('#'+btn).data('user-id');
		$.ajax({
			type: 'get',
			url: window.path + '/admin/user/edit/' + window.id,
			context: document.body
		}).done(function(data){
			$('#editId').val(data.editUser.id);
			$('#editNome').val(data.editUser.nome);
			$('#editSobrenome').val(data.editUser.sobrenome);
			$('#editEmail').val(data.editUser.email);
			$('#editEmail').prop('disabled', true);
			$('#editFone').val(data.editUser.fone);
			$('#editCargo').val(data.editUser.cargo);
			$('#editDepto').val(data.editUser.depto);
			if(data.editUser.token_guide == '1')
				$('#editToken').prop("checked", true);
			else
				$('#editToken').prop("checked", false);
			$('#editPerfil').val(data.editUser.id_adm_perfil);
			$('#editEmpresa').val(data.editUser.id_adm_empresa);
			$('#editStatus').val(data.editUser.status);
			$(data.editUser.modules).each(function( index, value ) {
			  	$('#editModulo'+value.id).prop("checked", true);
			});
			if($('#editPerfil').val() == 1 || $('#editPerfil').val() == 2)
	        	$('#listModulos').hide();
	        else
	        	$('#listModulos').show();
		});
		$(".modal-title").text("Editar Usuário");
	});

	$('#list-users .btn-remove').on('click', function (e) {
		e.preventDefault();
		if (confirm("Deseja realmente remover esse usuário?") == true) {
			var btn = $(this).attr('id');
			window.id = $('#'+btn).data('user-id');
			$.ajax({
				type: 'get',
				url: window.path + '/admin/user/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					alert('Usuário Removido com Sucesso');
					window.location.reload();
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();
		var resp = window.id ? 'Usuário Atualizado com Sucesso' : 'Usuário Criado com Sucesso';
		$.ajax({
			type: 'post',
			url: window.id ? window.path + '/admin/user/update/' + window.id : window.path + '/admin/user/create',
			data: $('form').serialize(),
			success: function (data) {
				if(data.error){
					alert(data.message);
					return false;
				}
				else {
					alert(resp);
					window.location.reload();
				}
			}
		});
	});

    $('#editPerfil').change(function(){
    	if($('#editPerfil').val() == 1 || $('#editPerfil').val() == 2)
        	$('#listModulos').hide();
        else
        	$('#listModulos').show();
    });

    // Habilitar Guide ao Clicar em Enxoval
    $('#editModulo4').click(function(){
    	if($('#editModulo4').prop("checked") && !$('#editModulo1').prop("checked")){
    		$('#editModulo1').prop("checked", true);
			$('#listTokens').show();
    		alert('O Módulo Guide Também Foi Habilitado.');
    	}
    });

    // Desabilitar Enxoval ao desabilitar Guide
    $('#editModulo1').click(function(){
		if(!$('#editModulo1').prop("checked"))
			$('#listTokens').hide();
		else
			$('#listTokens').show();
    	if(!$('#editModulo1').prop("checked") && $('#editModulo4').prop("checked")){
    		$('#editModulo4').prop("checked", false);
    		alert('O Módulo Enxoval Também Foi Desabilitado.');
    	}
    });

});
