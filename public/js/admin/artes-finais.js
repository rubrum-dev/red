window.path = window.location.origin;

$(function () {
	$('form').validate({
		rules: {
			nome: {
				required: true,
				normalizer: function(value) {
					// Note: the value of `this` inside the `normalizer` is the corresponding
					// DOMElement. In this example, `this` reference the `username` element.
					// Trim the value of the input
					return $.trim(value);
				}
			},
			image: {
				validImgUpload: true,
			},
			cmyk: {
				validCmykUpload: true
			}
		},
		errorPlacement: function (error, element) {
            if (element.attr('name') == 'nome') {
                error.insertAfter('#editNome');
            }

			if (element.attr('name') == 'image') {
                error.insertAfter('#upload-image');
            }

			if (element.attr('name') == 'cmyk') {
                error.insertAfter('#upload-cmyk');
            }
        },
		messages: {
			nome: {
				required: 'Legenda da Imagem é obrigatório.'
			}
		}
	});

	$.validator.addMethod('validImgUpload', function(value, element) {
		if ($('#listImage img')[0].currentSrc.substring($('#listCmyk img')[0].currentSrc.lastIndexOf('/')+ 1) === '') {
			if (element.value === '') {
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}, 'Selecione uma imagem em miniatura para upload.');

	$.validator.addMethod('validCmykUpload', function(value, element) {
		if ($('#listCmyk img')[0].currentSrc.substring($('#listCmyk img')[0].currentSrc.lastIndexOf('/')+ 1) === '') {
			if (element.value === '') {
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}, 'Selecione uma imagem em baixa resolução para upload.');

	//$.validator.addMethod("dateBR", function (value, element) {
	//	if (value.length != 10) return false;

	//	var data = value;
	//	var dia = data.substr(0, 2);
	//	var barra1 = data.substr(2, 1);
	//	var mes = data.substr(3, 2);
	//	var barra2 = data.substr(5, 1);
	//	var ano = data.substr(6, 4);

	//	if (data.length != 10 || barra1 != "/" || barra2 != "/" || isNaN(dia) || isNaN(mes) || isNaN(ano) || dia > 31 || mes > 12) return false;

	//	if ((mes == 4 || mes == 6 || mes == 9 || mes == 11) && dia == 31) return false;

	//	if (mes == 2 && (dia > 29 || (dia == 29 && ano % 4 != 0))) return false;

	//	if (ano < 1900) return false;

	//	return true;
	//}, "Informe uma data válida");
	
	$('#listImage img').error(function() { $(this).css('display', 'none') });
	$('#listCmyk img').error(function() { $(this).css('display', 'none') });
	$('#listRgb img').error(function() { $(this).css('display', 'none') });
	
	$('#btn-create').click(function () {
		window.id = null;

		$('form').validate().resetForm();
		
		$(".modal-title").text("Nova Imagem");
		$('#editNomeArte').val('');
		$('#editNome').val('');
		//$('#editExpiracao').val('');
		//$('#editExpiracao').prop('placeholder', 'dd/mm/aaaa');
		//$('#box-expiracao').hide();
		$('#btn-remove-rgb').hide();
		$('#listImage img').attr("src", window.path + '');
		$('#listCmyk img').attr("src", window.path + '');
		$('#listRgb img').attr("src", window.path + '');
		$('#listPdf img').attr("src", window.path + '');
		$('#listZip img').attr("src", window.path + '');

		$('#listImage .image-only').removeClass('has-uploaded');
		$('#listCmyk .image-only').removeClass('has-uploaded');
		$('#listRgb .file-image-type-only').removeClass('has-uploaded');
		
		$('#listImage .ui-tooltip-2').attr("data-title", 'Nenhum arquivo.');		
		$('#listCmyk .ui-tooltip-2').attr("data-title", 'Nenhum arquivo.');		
		$('#listRgb .ui-tooltip-2').attr("data-title", 'Nenhum arquivo.');		

		$('#listImage img').error(function() { $(this).css('display', 'none') });
		$('#listCmyk img').error(function() { $(this).css('display', 'none') });
		$('#listRgb img').error(function() { $(this).css('display', 'none') });
		//$('#listPdf img').error(function() { $(this).css('display', 'none') });
		//$('#listZip img').error(function() { $(this).css('display', 'none') });
		$('#listImage img').width("70px");
		$('#listImage').find('input[type="file"]').val('');
		$('#listCmyk').find('input[type="file"]').val('');
		$('#listRgb').find('input[type="file"]').val('');
		$('#listPdf').find('input[type="file"]').val('');
		$('#listZip').find('input[type="file"]').val('');
	});

	$('#list-artes .btn-edit').on('click', function (e) {
		e.preventDefault();

		$('form').validate().resetForm();
		
		//$('form').validate().destroy();

		var btn = $(this).attr('id');
		window.id = $('#' + btn).data('arte-id');

		$('.modal-loading').show();
		$('.modalBtnGroup input[type="submit"]').prop('disabled', true);

		$('#listImage .image-only').removeClass('has-uploaded');
		$('#listCmyk .image-only').removeClass('has-uploaded');
		$('#listRgb .file-image-type-only').removeClass('has-uploaded');

		$('#listImage img').attr("src", window.path + '');
		$('#listCmyk img').attr("src", window.path + '');
		$('#listRgb img').attr("src", window.path + '');
		//$('#listImage img').width("70px");
		$('#listImage').find('input[type="file"]').val('');
		$('#listCmyk').find('input[type="file"]').val('');
		$('#listRgb').find('input[type="file"]').val('');
		//$('#listPdf').find('input[type="file"]').val('');
		//$('#listZip').find('input[type="file"]').val('');
		//$('#editExpiracao').val('');
		//$('#editExpiracao').prop('placeholder', 'dd/mm/aaaa');

		$('#listImage img').error(function() { $(this).css('display', 'none') });
		$('#listCmyk img').error(function() { $(this).css('display', 'none') });
		$('#listRgb img').error(function() { $(this).css('display', 'none') });
		
		$('#listImage .ui-tooltip-2').attr("data-title", 'Nenhum arquivo.');		
		$('#listCmyk .ui-tooltip-2').attr("data-title", 'Nenhum arquivo.');		
		$('#listRgb .ui-tooltip-2').attr("data-title", 'Nenhum arquivo.');		
				
		$('#btn-remove-rgb').hide();

		$.ajax({
			type: 'get',
			url: window.path + '/admin/sku/finalart/edit/' + window.id,
			context: document.body
		}).done(function (data) {
			$('#editId').val(data.editArt.id);
			$('#editNomeArte').val(data.editArt.nome_arte);
			$('#editNome').val(data.editArt.nome);

			$('.modal-loading').hide();
			$('.modalBtnGroup input[type="submit"]').prop('disabled', false);

			if (data.editArt.thumb) {
				$('#listImage .image-only').addClass('has-uploaded');
				$('#listImage img').attr("src", window.path + data.editArt.thumb);
				$('#listImage .ui-tooltip-2').attr("data-title", data.editArt.thumb.substring(data.editArt.thumb.lastIndexOf('/')+1));		
				
				$('#listImage img').css({
					'display': 'block', 
				});
			}

			if (data.editArt.path_cmyk) {
				$('#listCmyk .image-only').addClass('has-uploaded');
				$('#listCmyk img').attr("src", window.path + data.editArt.path_cmyk);
				$('#listCmyk .ui-tooltip-2').attr("data-title", data.editArt.path_cmyk.substring(data.editArt.path_cmyk.lastIndexOf('/')+1));		
				
				$('#listCmyk img').css({
					'display': 'block', 
				});
			}

			if (data.editArt.path_rgb) {
				$('#listRgb .file-image-type-only').addClass('has-uploaded');
				$('#listRgb .ui-tooltip-2').attr("data-title", data.editArt.path_rgb.substring(data.editArt.path_rgb.lastIndexOf('/')+1));		
				//$('#listRgb img').attr("src", window.path + '/images/backend/image-icon.png');
				//$('#listRgb img').css({
				//	'display': 'block', 
				//});
				$('#btn-remove-rgb').show();
			}
		});

		$(".modal-title").text("Editar Imagem");
	});

	$('#list-artes .btn-remove').on('click', function (e) {
		e.preventDefault();
		if (confirm("Deseja realmente remover essa imagem?") == true) {
			var btn = $(this).attr('id');
			var prodId = $('#list-artes').data('sku-id');
			window.id = $('#' + btn).data('arte-id');

			$('.lds-css').show();
			
			$.ajax({
				type: 'get',
				url: window.path + '/admin/sku/finalart/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					$('.lds-css').fadeOut(200, function () {
						alert('Imagem removida com sucesso.');
						window.location.href = window.path + '/admin/sku/finalart/' + prodId;
					});
				}
			});
		}
	});

	// Usar um data type (RGB, CMYK, ETC). Passar parâmetro para a função genérica do PHP tratar conforme o path.
	$('#btn-remove-rgb').on('click', function (e) {
		e.preventDefault();

		if (confirm("Deseja realmente remover essa imagem?") == true) {
			$('.lds-css').show();

			$.ajax({
				type: 'get',
				url: window.path + '/admin/sku/finalart/remove-sku-file/rgb/' + $('#editId').val(),
				data: $('form').serialize(),
				success: function () {
					$('#listRgb img').attr("src", window.path + '');
					$('#listRgb .file-image-type-only').removeClass('has-uploaded');
					$('#listRgb .ui-tooltip-2').attr("data-title", 'Nenhum arquivo.');		
					$('#btn-remove-rgb').hide();
					
					$('.lds-css').fadeOut('200', function() {
						alert('Imagem Removida com Sucesso');
						return false;
					});
				}
			});
		}
	});
	
	$('form').on('submit', function (e) {
		e.preventDefault();

		var prodId = $('#list-artes').data('sku-id');
		var resp = window.id ? 'Imagem atualizada com sucesso.' : 'Imagem cadastrada com sucesso.';

		if (!$(this).valid()) {
			return false;
		}

		$(this).validate().resetForm();
		$('.modal').modal('hide');

		$.ajax({
			url: window.id ? window.path + '/admin/sku/finalart/update/' + window.id : window.path + '/admin/sku/finalart/' + prodId + '/create',
			data: new FormData($("#form-art")[0]),
			type: 'post',
			processData: false,
			contentType: false,
			xhr: function () {
				var loading = 0;
				// get the native XmlHttpRequest object
				var xhr = $.ajaxSettings.xhr();
				// set the onprogress event handler
				xhr.upload.onprogress = function (evt) {
					loading = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
					$('#packShelfImgProgress').show();
					$('#svg').find('path').show();
					$('#svg').find('path').attr('stroke-dasharray', loading * 2.5 + ' ' + 250.2);
					$('#count').text(loading + '%');
				};
				// set the onload event handler
				xhr.upload.onload = function () {
					if (xhr.readyState === 4) {
						$('#packShelfImgProgress').hide();
						$('#svg').find('path').hide();
					}
				};
				// return the customized object
				return xhr;
			},
			success: function (data) {
				if (data.error) {
					$('#packShelfImgProgress').hide();
					$('#svg').find('path').hide();
					alert(data.message);
					return false;
				} else {
					$('#packShelfImgProgress').hide();
					$('#svg').find('path').hide();
					alert(resp);
					window.location.reload();
				}
			}
		});
	});

	$('.modal').on('show.bs.modal', function () {
		$('.modal').overlayScrollbars({
			autoUpdate: true
		});
	});
	
	$('.modal').on('hidden.bs.modal', function () {
		$('input[type=file]').val('');
		$('form').validate().resetForm();
	});
});