window.path = window.location.origin;

var getByName = function(name) {
	for(var i = 0; i < empresas.length; i++) {
		if (empresas[i].nome == name) {
			return empresas[i];
		}
	}
};

$(function () {
	$("form").validate({
		rules: {
			nome: {
				required: true,
				normalizer: function(value) {
					// Note: the value of `this` inside the `normalizer` is the corresponding
					// DOMElement. In this example, `this` reference the `username` element.
					// Trim the value of the input
					return $.trim(value);
				}
			}
		},
		messages: {
			nome: {
				required: "Nome é obrigatório."
			}
		}
	});

	$('#btn-create').click(function () {
		window.id = null;
		$('form').validate().resetForm();
		$(".modal-title").text("Nova Empresa");
		$('input[name=id]').val('');
		$('input[name=nome]').val('');
	});

	$('#list-company .btn-edit').on('click', function (e) {
		e.preventDefault();
		$('form').validate().resetForm();

		$('.modal-loading').show();
		$('.modalBtnGroup input[type="submit"]').prop('disabled', true);

		var btn = $(this).attr('id');
		window.id = $('#' + btn).data('company-id');
		$.ajax({
			type: 'get',
			url: window.path + '/admin/company/edit/' + window.id,
			context: document.body
		}).done(function (data) {
			$('#editId').val(data.editCompany.id);
			$('#editNome').val(data.editCompany.nome);

			$('.modal-loading').hide();
			$('.modalBtnGroup input[type="submit"]').prop('disabled', false);
		});
		$(".modal-title").text("Editar Empresa");
	});

	$('#list-company .btn-remove').on('click', function (e) {
		e.preventDefault();
		if (confirm("Deseja realmente remover essa empresa? \n\n* ATENÇÃO: Todos os usuários vinculados a essa empresa serão removidos!") == true) {
			var btn = $(this).attr('id');
			window.id = $('#' + btn).data('company-id');
			$.ajax({
				type: 'get',
				url: window.path + '/admin/company/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					alert('Empresa Removida com Sucesso');
					window.location.reload();
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();
		
		var txtName = $("#editNome");
        var editNome = $.trim(txtName.val());
        
		txtName.val(editNome);
		
		var resp = window.id ? 'Empresa Atualizada com Sucesso' : 'Empresa Criada com Sucesso';

		if (!$(this).valid()) {
			return false;
		}

		for(var k = 0; k < empresas.length; k++) {
			if ( ( (empresas[k] && editNome !== empresas[k].nome) || !empresas[k] ) && getByName(editNome) && getByName(editNome).id !== window.id ) {
				alert('Já existe uma Empresa com este nome.')
				$('#editNome').focus();
				return false;
			} 
		}

		$(this).validate().resetForm();
		$('.modal').modal('hide');
		$('.lds-css').show();
		
		$.ajax({
			type: 'post',
			url: window.id ? window.path + '/admin/company/update/' + window.id : window.path + '/admin/company/create',
			data: $('form').serialize(),
			success: function () {
				$('.lds-css').fadeOut(200, function() {
					alert(resp);
					window.location.reload();
				});
				//alert(resp);
				//window.location.reload();
			}
		});
	});

	$('.modal').on('show.bs.modal', function () {
		$('.modal').overlayScrollbars({
			autoUpdate: true
		});
	});
	
	$('.modal').on('hidden.bs.modal', function () {
		$('form').validate().resetForm();
	});
});