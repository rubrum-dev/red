window.path = window.location.origin;

var getByName = function(name) {
	for(var i = 0; i < perfis.length; i++) {
		if (perfis[i].nome == name) {
			return perfis[i];
		}
	}
};

$(function () {
	$('form').validate({
		rules: {
			nome: {
				required: true,
				normalizer: function(value) {
					// Note: the value of `this` inside the `normalizer` is the corresponding
					// DOMElement. In this example, `this` reference the `username` element.
					// Trim the value of the input
					return $.trim(value);
				}
			},
			'families[]': {
				required: true
			}
		},
		messages: {
			nome: {
				required: 'Nome é obrigatório.'
			},
			'families[]': {
				required: 'Selecione pelo menos uma marca para compor o perfil de acesso.'
			}
		},
		errorPlacement: function (error, element) {
			if (element.attr("name") == "nome") {
				error.appendTo(".box-nome");
			}

			if (element.attr("name") == "families[]") {
				error.insertAfter(".check-uncheck-all");
			}
		}
	});

	$('#desmarcacao').hide();

	$('#marcacao').click(function () {
		$('.selectFamila').each(function () {
			this.checked = 1;
		});
		$('#desmarcacao').show();
		$('#marcacao').hide();
	});

	$('#desmarcacao').click(function () {
		$('.selectFamila').each(function () {
			this.checked = 0;
		});
		$('#desmarcacao').hide();
		$('#marcacao').show();
	});

	$('#btn-create').click(function () {
		window.id = null;

		$('form').validate().resetForm();
		$(".modal-title").text("Novo Perfil de Acesso");
		$('input[name=id]').val('');
		$('input[name=nome]').val('');
		$('#editGerencia').prop("checked", false);
		$('#editStatus').val('1');
		$('#editStatus').prop('checked', true);

		$('#checkFamiliesGroup input[type="checkbox"]').each(function (index, value) {
			$('#' + value.id).prop("checked", false);
		});
	});

	$('#btn-cancel-modal').click(function () {
		$('input[type="checkbox"]').each(function (index, value) {
			$('#' + value.id).prop("checked", false);
		});
	});

	$('#list-profiles .btn-edit').on('click', function (e) {
		e.preventDefault();

		$('form').validate().resetForm();

		$('.modal-loading').show();
		$('.modalBtnGroup input[type="submit"]').prop('disabled', true);

		var btn = $(this).attr('id');

		window.id = $('#' + btn).data('profile-id');

		$.ajax({
			type: 'get',
			url: window.path + '/admin/profile/edit/' + window.id,
			context: document.body
		}).done(function (data) {
			$('#editId').val(data.editProfile.id);
			$('#editNome').val(data.editProfile.nome);

			$('.modal-loading').hide();
			$('.modalBtnGroup input[type="submit"]').prop('disabled', false);

			if (data.editProfile.status === '1') {
				$('#editStatus').prop('checked', true);
			} else {
				$('#editStatus').prop('checked', false);
			}

			if (data.editProfile.gerencia_usuario == 1)
				$('#editGerencia').prop("checked", true);
			else
				$('#editGerencia').prop("checked", false);

			$(data.editProfile.familias).each(function (index, value) {
				$('#editFamilia' + value.id).prop("checked", true);
			});
		});
		
		$(".modal-title").text("Editar Perfil de Acesso");
	});

	$('#list-profiles .btn-remove').on('click', function (e) {
		e.preventDefault();

		if (confirm("Deseja realmente remover esse perfil? \n\n* ATENÇÃO: Todos os Usuários vinculados a esse perfil serão removidos!") == true) {
			var btn = $(this).attr('id');
			window.id = $('#' + btn).data('profile-id');
			$('.lds-css').show();
			$.ajax({
				type: 'get',
				url: window.path + '/admin/profile/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					//alert('Perfil Removido com Sucesso');
					//window.location.reload();
					$('.lds-css').fadeOut('200', function() {
						alert('Perfil Removido com Sucesso');
						window.location.reload();
					});
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();

		var txtName = $("#editNome");
        var editNome = $.trim(txtName.val());
        
		txtName.val(editNome);
		
		var resp = window.id ? 'Perfil Atualizado com Sucesso' : 'Perfil Criado com Sucesso';

		if (!$(this).valid()) {
			return false;
		}

		for(var k = 0; k < perfis.length; k++) {
			if ( ( (perfis[k] && editNome !== perfis[k].nome) || !perfis[k] ) && getByName(editNome) && getByName(editNome).id !== window.id ) {
				alert('Já existe um Perfil com este nome.')
				$('#editNome').focus();
				return false;
			} 
		}

		$(this).validate().resetForm();
		$('.modal').modal('hide');

		$('.lds-css').show();
		//$('.master').addClass('blurred');

		$.ajax({
			type: 'post',
			url: window.id ? window.path + '/admin/profile/update/' + window.id : window.path + '/admin/profile/create',
			data: $('form').serialize(),
			success: function () {				
				$('.lds-css').fadeOut('200', function() {
					alert(resp);
					window.location.reload();
				});
        		//$('.master').removeClass('blurred');
				//setTimeout(function () {
				//	alert(resp);
				//	window.location.reload();
				//}, 1000);	
			}
		});				
	});

	$('.modal').on('show.bs.modal', function () {
		$('.modal').overlayScrollbars({
			autoUpdate: true
		});
	});
	
	$('.modal').on('hidden.bs.modal', function () {
		$('form').validate().resetForm();
	});
});