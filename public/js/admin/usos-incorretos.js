window.path = window.location.origin;

$(function () {
	$("form").validate({
		rules:{
			id_tipo_uso_incorreto:{
				required: true
			}
		},
		messages:{
			id_tipo_uso_incorreto:{
				required: "Selecione o tipo de uso incorreto"
			}
		}
	});

	$('#btn-create').click(function () {
		window.id = null;
		$('form').validate().resetForm();
		$(".modal-title").text("Novo Uso Incorreto");
		$('input[name=id]').val('');
		$('textarea').val('');
		$('#editStatus').val('1');
		$('select option:contains("Selecione o Tipo...")').prop('selected',true);
		$('#list-tipos-logos img').attr("src", window.path + '/images/backend/noImage.png');
		$('#list-tipos-logos').find('input[type="file"]').val('');
	});

	$('#list-usos-incorretos .btn-edit').on('click', function (e) {
		e.preventDefault();
		$('form').validate().resetForm();
		var btn = $(this).attr('id');
		window.id = $('#'+btn).data('uso-id');
		$('#list-tipos-logos img').attr("src", window.path + '/images/backend/noImage.png');
		$('#list-tipos-logos').find('input[type="file"]').val('');
		$.ajax({
			type: 'get',
			url: window.path + '/admin/product/misuse/edit/' + window.id,
			context: document.body
		}).done(function(data){
			$('#editId').val(data.editMisuse.id);
			$('#editUsoIncorreto').val(data.editMisuse.id_tipo_uso_incorreto);
			$('#editDescricao').val(data.editMisuse.descricao);
			$('#editStatus').val(data.editMisuse.status);
			$('#list-tipos-logos img').attr("src", window.path + data.editMisuse.thumb);
		});
		$(".modal-title").text("Editar Uso Incorreto");
	});

	$('#list-usos-incorretos .btn-remove').on('click', function (e) {
		e.preventDefault();
		if (confirm("Deseja realmente remover essa uso incorreto?") == true) {
			var btn = $(this).attr('id');
			var prodId = $('#list-usos-incorretos').data('prod-id');
			window.id = $('#'+btn).data('uso-id');
			$.ajax({
				type: 'get',
				url: window.path + '/admin/product/misuse/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					alert('Uso Incorreto Removido com Sucesso');
					window.location.href = window.path + '/admin/product/misuse/' + prodId;
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();
		var prodId = $('#list-usos-incorretos').data('prod-id');
		var resp = window.id ? 'Uso Incorreto Atualizado com Sucesso' : 'Uso Incorreto Criado com Sucesso';
		$.ajax({
			url: window.id ? window.path + '/admin/product/misuse/update/' + window.id : window.path + '/admin/product/misuse/' + prodId + '/create',
			data:new FormData($("#form-misuse")[0]),
			type:'post',
			processData: false,
			contentType: false,
			success:function (data) {
				if(data.error){
					alert(data.message);
					return false;
				}
				else {
					alert(resp);
					window.location.reload();
				}
			},
		});
	});

});
