window.path = window.location.origin;

$(function () {
	$("form").validate({
		rules:{
			nome:{
				required: true
			}
		},
		messages:{
			nome:{
				required: "Digite o nome"
			}
		}
	});

	$('#btn-create').click(function () {
		window.id = null;
		$('form').validate().resetForm();
		$(".modal-title").text("Novo Enxoval");
		$('#editNome').val('');
		$('#editStatus').val('1');
		$('#listImage').find('input[type="file"]').val('');
		$('#listImage img').width("70px");
		$('#listImage img').attr("src", window.path + '/images/backend/noImage.png');
		$('#listFile').find('input[type="file"]').val('');
		$('#listFile img').attr("src", window.path + '/images/backend/noFile.png');
		$('#listFile img').width("70px");
	});

	$('#list-enxovais .btn-edit').on('click', function (e) {
		e.preventDefault();
		$('form').validate().resetForm();
		var btn = $(this).attr('id');
		window.id = $('#'+btn).data('enxoval-id');
		$('#listImage').find('input[type="file"]').val('');
		$('#listImage img').width("70px");
		$('#listImage img').attr("src", window.path + '/images/backend/noImage.png');
		$('#listFile').find('input[type="file"]').val('');
		$('#listFile img').width("70px");
		$('#listFile img').attr("src", window.path + '/images/backend/noFile.png');
		$.ajax({
			type: 'get',
			url: window.path + '/admin/product/outfit/edit/' + window.id,
			context: document.body
		}).done(function(data){
			$('#editId').val(data.editEnxoval.id);
			$('#editNome').val(data.editEnxoval.nome);
			$('#editStatus').val(data.editEnxoval.status);
			if(data.editEnxoval.thumb){
				$('#listImage img').attr("src", window.path + data.editEnxoval.thumb);
			}
			if(data.editEnxoval.path_down){
				$('#listFile img').attr("src", window.path + '/images/backend/zipFile.png');
			}
		});
		$(".modal-title").text("Editar Enxoval");
	});

	$('#list-enxovais .btn-remove').on('click', function (e) {
		e.preventDefault();
		if (confirm("Deseja realmente remover esse enxoval?") == true) {
			var btn = $(this).attr('id');
			var prodId = $('#list-enxovais').data('prod-id');
			window.id = $('#'+btn).data('enxoval-id');
			$.ajax({
				type: 'get',
				url: window.path + '/admin/product/outfit/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					alert('Enxoval Removido com Sucesso');
					window.location.href = window.path + '/admin/product/outfit/' + prodId;
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();
		var prodId = $('#list-enxovais').data('prod-id');
		var resp = window.id ? 'Enxoval Atualizado com Sucesso' : 'Enxoval Criado com Sucesso';
		$.ajax({
			url: window.id ? window.path + '/admin/product/outfit/update/' + window.id : window.path + '/admin/product/outfit/' + prodId + '/create',
			data:new FormData($("#form-outfit")[0]),
			type:'post',
			processData: false,
			contentType: false,
			success:function (data) {
				if(data.error){
					alert(data.message);
					return false;
				}
				else {
					alert(resp);
					window.location.reload();
				}
			},
		});
	});

});
