window.path = window.location.origin;

function getOutfits(data)
{
	var val = '';
	var cont = 1;
	$(data.editProduct.enxovais).each(function( index, value ) {
		status = value.status == 1 ? 'Ativo' : 'Inativo';
		if(cont == 1)
			val += '<tr>';
		val += '<td style="border-top: none;">';
		val += '	<div class="box-familia-enxoval">';
		val += '   		<img src="' + window.path + '/images/backend/zipFile.png" class="img-rounded img-logo" alt="alt">';
		val += '		<div class="box-familia-enxoval-text">';
		val += 				value.nome + '<br />';
		val += 				status + '<br />';
		val += 				'<a href="' + value.path_down + '" target="_blank"><span class="glyphicon glyphicon-download"></span>Download</a>';
		val += '		</div>';
		val += '	</div>';
		val += '</td>';
		if(cont % 3 == 0)
			val += '</tr>';
		cont++;
	});
	val += '</tr>';

	$('#list-tipos-enxovais').append(val);
}

function getColors(data)
{
	var val = '';
    $(data.tiposCores).each(function( index, value ) {
    	window["contType_" + value.id] = 1;
    	val += '<table id="list-tipos-cores" class="table">';
		val += '	<tr>';
		val += '		<td class="name-logo" style="border-top: none;" colspan="3">' + value.nome + '</td>';
		val += '	</tr>';
		val += '	<tr><td class="col-sm-1" style="border-top: none;">';
		val += '		<table id="td-color-' + value.id + '">';
		val += '		</table>';
		val += '	</td></tr>';
		val += '</table>';
	});
	$('#list-tipos-cores').append(val);

	val = '';
	var table = [];
	$(data.editProduct.cores).each(function( index, value ) {
		if(window["contType_" + value.id_tipo_cor] == 1)
			val += '<tr>';
	  	val += '<td class="col-sm-1" style="border-top: none;">';
	  	val += '	<div class="box-product-colors">';
		val += '   	<div class="box-product-colors-thumb" style="background-color: #' + value.hex + ';">';
		val += '    	</div>';
		val += '		<div class="box-product-colors-text">';
		val += 			value.nome + '<br />';
		$(data.niveisCores).each(function( indice, nivel ) {
			if(nivel.id == value.id_nivel_cor)
				val += nivel.nome;
		});
		val += '		</div>';
		val += '	</div>';
		val += '</td>';
		if(window["contType_" + value.id_tipo_cor] % 5 == 0)
			val += '</tr>';
		window["contType_" + value.id_tipo_cor]++;

  		table[value.id_tipo_cor] += val;
  		val = '';
	});

	$.each(table, function( index, value ) {
  		$('#td-color-' + index).append(table[index]);
	});
}

function getTypography(data)
{
    var val = '';
    $(data.tiposTipografias).each(function( index, value ) {
    	window["contType_" + value.id] = 1;
		val += '<table id="list-tipos-tipografias" class="table">';
		val += '	<tr>';
		val += '		<td class="name-logo" style="border-top: none;" colspan="3">' + value.nome + '</td>';
		val += '	</tr>';
		val += '	<tr><td class="col-sm-1" style="border-top: none;">';
		val += '		<table id="td-typography-' + value.id + '">';
		val += '		</table>';
		val += '	</td></tr>';
		val += '</table>';
	});
	$('#list-tipos-tipografias').append(val);

	val = '';
	var table = [];
	$(data.editProduct.tipografias).each(function( index, value ) {
		if(window["contType_" + value.id_tipo_tipografia] == 1)
			val += '<tr>';
		val += '<td>';
		val += '		<div class="box-product-tipografias">';
		val += '   			<img src="' + window.path + value.thumb + '" class="img-rounded img-logo" alt="alt">';
		val += '			<div class="box-product-tipografias-text">';
		val += 					value.nome + '<br />';
		val += '				<a href="' + value.path_down + '" target="_blank"><span class="glyphicon glyphicon-download"></span>Download</a>';
		val += '			</div>';
		val += '		</div>';
		val += '</td>';
		if(window["contType_" + value.id_tipo_tipografia] % 2 == 0)
			val += '</tr>';
		window["contType_" + value.id_tipo_tipografia]++;

		table[value.id_tipo_tipografia] += val;
		val = '';
	});

	$.each(table, function( index, value ) {
		$('#td-typography-' + index).append(table[index]);
	});
}

function getMisuse(data)
{
    var val = '';
    $(data.tiposUsosIncorretos).each(function( index, value ) {
    	window["contType_" + value.id] = 1;
		val += '<table id="list-tipos-usos-incorretos" class="table">';
		val += '	<tr>';
		val += '		<td class="name-logo" style="border-top: none;" colspan="3">' + value.nome + '</td>';
		val += '	</tr>';
		val += '	<tr><td class="col-sm-1" style="border-top: none;">';
		val += '		<table id="td-misuse-' + value.id + '">';
		val += '		</table>';
		val += '	</td></tr>';
		val += '</table>';
	});
	$('#list-tipos-usos-incorretos').append(val);

	val = '';
	var table = [];
	$(data.editProduct.usosIncorretos).each(function( index, value ) {
		if(window["contType_" + value.id_tipo_uso_incorreto] == 1)
			val += '<tr>';
		val += '<td>';
		val += '		<div class="box-product-usos-incorretos">';
		val += '   			<img src="' + window.path + value.thumb + '" class="img-rounded img-logo" alt="alt">';
		val += '			<div class="box-product-usos-incorretos-text">';
		val += 				value.descricao;
		val += '			</div>';
		val += '		</div>';
		val += '</td>';
		if(window["contType_" + value.id_tipo_uso_incorreto] % 5 == 0)
			val += '</tr>';
		window["contType_" + value.id_tipo_uso_incorreto]++;

		table[value.id_tipo_uso_incorreto] += val;
		val = '';
	});

	$.each(table, function( index, value ) {
		$('#td-misuse-' + index).append(table[index]);
	});
}

function getApplication(data)
{
    var val = '';
    $(data.tiposUsosAplicacoes).each(function( index, value ) {
    	window["contType_" + value.id] = 1;
		val += '<table id="list-tipos-usos-aplicacoes" class="table">';
		val += '	<tr>';
		val += '		<td class="name-logo" style="border-top: none;" colspan="3">' + value.nome + '</td>';
		val += '	</tr>';
		val += '	<tr><td class="col-sm-1" style="border-top: none;">';
		val += '		<table id="td-application-' + value.id + '">';
		val += '		</table>';
		val += '	</td></tr>';
		val += '</table>';
	});
	$('#list-tipos-usos-aplicacoes').append(val);

	val = '';
	var table = [];
	$(data.editProduct.usosAplicacoes).each(function( index, value ) {
		if(window["contType_" + value.id_tipo_uso_aplicacao] == 1)
			val += '<tr>';
		val += '<td>';
		val += '		<div class="box-product-usos-aplicacoes">';
		val += '   			<img src="' + window.path + value.thumb + '" class="img-rounded img-logo" alt="alt">';
		val += '			<div class="box-product-usos-aplicacoes-text">';
		val += 				value.descricao;
		val += '			</div>';
		val += '		</div>';
		val += '</td>';
		if(window["contType_" + value.id_tipo_uso_aplicacao] % 5 == 0)
			val += '</tr>';
		window["contType_" + value.id_tipo_uso_aplicacao]++;

		table[value.id_tipo_uso_aplicacao] += val;
		val = '';
	});

	$.each(table, function( index, value ) {
		$('#td-application-' + index).append(table[index]);
	});
}

function getVersion(data)
{
	var val = '';
    $(data.categoriasVersoes).each(function( index, value ) {
    	window["contType_" + value.id] = 1;
    	val += '<table id="list-tipos-versoes" class="table">';
		val += '	<tr>';
		val += '		<td class="name-logo" style="border-top: none;" colspan="3">' + value.nome + '</td>';
		val += '	</tr>';
		val += '	<tr><td class="col-sm-1" style="border-top: none;">';
		val += '		<table id="td-version-' + value.id + '">';
		val += '		</table>';
		val += '	</td></tr>';
		val += '</table>';
	});
	$('#list-tipos-versoes').append(val);

	val = '';
	var table = [];
	$(data.editProduct.versoes).each(function( index, value ) {
			if(window["contType_" + value.id_categoria_versao] == 1)
				val += '<tr>';
			val += '<td>';
			val += '		<div class="box-product-versoes">';
			val += '   			<img src="' + window.path + value.thumb + '" class="img-rounded img-logo" alt="alt">';
			val += '			<div class="box-product-versoes-text">';
			val += 					value.descricao + '<br />';
			val += '				<a href="' + value.path_down + '" target="_blank"><span class="glyphicon glyphicon-download"></span>Download</a>';
			val += '			</div>';
			val += '		</div>';
			val += '</td>';
			if(window["contType_" + value.id_categoria_versao] % 2 == 0)
				val += '</tr>';
			window["contType_" + value.id_categoria_versao]++;

	  		table[value.id_categoria_versao] += val;
  			val = '';
	});

	$.each(table, function( index, value ) {
  		$('#td-version-' + index).append(table[index]);
	});
}

function getProportion(data)
{
	var val = '';
    $(data.categoriasProporcoes).each(function( index, value ) {
    	window["contType_" + value.id] = 1;
    	val += '<table id="list-tipos-proporcoes" class="table">';
		val += '	<tr>';
		val += '		<td class="name-logo" style="border-top: none;" colspan="3">' + value.nome + '</td>';
		val += '	</tr>';
		val += '	<tr><td class="col-sm-1" style="border-top: none;">';
		val += '		<table id="td-proportion-' + value.id + '">';
		val += '		</table>';
		val += '	</td></tr>';
		val += '</table>';
	});
	$('#list-tipos-proporcoes').append(val);

	val = '';
	var table = [];
	$(data.editProduct.proporcoes).each(function( index, value ) {
		if(window["contType_" + value.id_categoria_proporcao] == 1)
			val += '<tr>';
		val += '<td>';
		val += '		<div class="box-product-proporcoes">';
		val += '   			<img src="' + window.path + value.thumb + '" class="img-rounded img-logo" alt="alt">';
		val += '			<div class="box-product-proporcoes-text">';
								$(data.tiposProporcoesVersoes).each(function( indice, tipo ) {
									if(tipo.id == value.id_tipo_proporcao_versao)
										val += tipo.nome;
								});
		val += '				<br />';
		val += 					value.descricao;
		val += '			</div>';
		val += '		</div>';
		val += '</td>';
		if(window["contType_" + value.id_categoria_proporcao] % 5 == 0)
			val += '</tr>';
		window["contType_" + value.id_categoria_proporcao]++;

  		table[value.id_categoria_proporcao] += val;
  		val = '';
	});

	$.each(table, function( index, value ) {
  		$('#td-proportion-' + index).append(table[index]);
	});
}

function getIcon(data)
{
	var val = '';
    $(data.categoriasIcones).each(function( index, value ) {
    	window["contType_" + value.id] = 1;
    	val += '<table id="list-tipos-icones" class="table">';
		val += '	<tr>';
		val += '		<td class="name-logo" style="border-top: none;" colspan="3">' + value.nome + '</td>';
		val += '	</tr>';
		val += '	<tr><td class="col-sm-1" style="border-top: none;">';
		val += '		<table id="td-icon-' + value.id + '">';
		val += '		</table>';
		val += '	</td></tr>';
		val += '</table>';
	});
	$('#list-tipos-icones').append(val);

	val = '';
	var table = [];
	$(data.editProduct.icones).each(function( index, value ) {
		if(window["contType_" + value.id_categoria_icone] == 1)
			val += '<tr>';
		val += '<td>';
		val += '		<div class="box-product-icones">';
		val += '   			<img src="' + window.path + value.thumb + '" class="img-rounded img-logo" alt="alt">';
		val += '			<div class="box-product-icones-text">';
		val += 					value.descricao + '<br />';
		val += '			<a href="' + value.path_down + '" target="_blank"><span class="glyphicon glyphicon-download"></span>Download</a>';
		val += '			</div>';
		val += '		</div>';
		val += '</td>';
		if(window["contType_" + value.id_categoria_icone] % 5 == 0)
			val += '</tr>';
		window["contType_" + value.id_categoria_icone]++;

  		table[value.id_categoria_icone] += val;
  		val = '';
	});

	$.each(table, function( index, value ) {
  		$('#td-icon-' + index).append(table[index]);
	});
}
