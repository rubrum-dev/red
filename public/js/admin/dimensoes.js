window.path = window.location.origin;

$(function () {
	$('form').validate({
		rules: {
			nome: {
				required: true,
				normalizer: function(value) {
					// Note: the value of `this` inside the `normalizer` is the corresponding
					// DOMElement. In this example, `this` reference the `username` element.
					// Trim the value of the input
					return $.trim(value);
				}
			},
			id_tipo_embalagem: {
				required: true
			}
		},
		messages: {
			nome: {
				required: 'Volume é obrigatório.'
			},
			id_tipo_embalagem: {
				required: 'Tipo de Embalagem é obrigatório.'
			}
		}
	});

	$('#btn-create').click(function () {
		window.id = null;
		$('#box-ordem').hide();
		$(".modal-title").text("Novo Volume");
		$('input[name=id]').val('');
		$('input[name=nome]').val('');
		$('#editStatus').val('1');
		$('#editStatus').prop('checked', true);
		$('select option:contains("Selecione o Tipo de Embalagem")').prop('selected', true);
	});

	$('#list-dimensions .btn-edit').on('click', function (e) {
		e.preventDefault();
		$('form').validate().resetForm();
		$('#box-ordem').show();
		$('.modal-loading').show();
		$('.modalBtnGroup input[type="submit"]').prop('disabled', true);

		var btn = $(this).attr('id');
		var last = $('#editOrdem option:last-child').val();
		window.id = $('#' + btn).data('dimension-id');

		$.ajax({
			type: 'get',
			url: window.path + '/admin/dimensions/edit/' + window.id,
			context: document.body
		}).done(function (data) {
			$('#editId').val(data.editDimension.id);
			$('#editNome').val(data.editDimension.nome);
			$('#editEmbalagem').val(data.editDimension.id_tipo_embalagem);

			$('#editOrdem').val(data.editDimension.ordem);
			
			$('.modal-loading').hide();
			$('.modalBtnGroup input[type="submit"]').prop('disabled', false);

			if (data.editDimension.status == 1)
				$('#editStatus').prop('checked', true);
			else
				$('#editStatus').prop('checked', false);
		});
		
		$(".modal-title").text("Editar Volume");
	});

	$('#list-dimensions .btn-remove').on('click', function (e) {
		e.preventDefault();
		if (confirm("Deseja realmente remover esse volume? \n\n* ATENÇÃO: Todos os skus vinculados a essa volume serão removidos!") == true) {
			var btn = $(this).attr('id');
			window.id = $('#' + btn).data('dimension-id');
			$.ajax({
				type: 'get',
				url: window.path + '/admin/dimensions/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					alert('Volume Removido com Sucesso');
					window.location.reload();
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();

		var resp = window.id ? 'Volume Atualizado com Sucesso' : 'Volume Criado com Sucesso';
		
		if (!$(this).valid()) {
			return false;
		}

		$(this).validate().resetForm();
		$('.modal').modal('hide');
		$('.lds-css').show();

		$.ajax({
			url: window.id ? window.path + '/admin/dimensions/update/' + window.id : window.path + '/admin/dimensions/create',
			data: new FormData($("#form-package")[0]),
			type: 'post',
			processData: false,
			contentType: false,
			success: function (data) {
				if (data.error) {
					$('.lds-css').fadeOut('200', function() {
						alert(data.message);
						return false;
					});
				} else {
					//alert(resp);
					//window.location.reload();
					$('.lds-css').fadeOut('200', function() {
                        alert(resp);
                        location.reload();                       
                    });
				}
			}
		});
	});

	$('.modal').on('show.bs.modal', function () {
		$('.modal').overlayScrollbars({
			autoUpdate: true
		});
	});
	
	$('.modal').on('hidden.bs.modal', function () {
		$('form').validate().resetForm();
	});
});