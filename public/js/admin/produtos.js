var url = window.location.href;
var arr = url.split("/");
var path = arr[0] + "//" + arr[2];

window.path = path;

$(function () {
	$('input.minicolors').minicolors({
		theme: 'bootstrap'
	});

	$('form').validate({
		rules: {
			nome: {
				required: true,
				normalizer: function (value) {
					// Note: the value of `this` inside the `normalizer` is the corresponding
					// DOMElement. In this example, `this` reference the `username` element.
					// Trim the value of the input
					return $.trim(value);
				}
			},
			id_familia: {
				required: true
			},
			id_categoria_produto: {
				required: true
			},
			ordem: {
				required: true,
			}
		},
		messages: {
			nome: {
				required: 'Produto é obrigatório.'
			},
			id_familia: {
				required: 'Marca é obrigatório.'
			},
			ordem: {
				required: 'Ordem de Exibição é obrigatório.'
			},
			id_categoria_produto: {
				required: 'Categoria de Produto é obrigatório.'
			}
		}
	});

	$('#btn-create').click(function () {
		window.id = null;

		$('input.minicolors').minicolors('value', '');
		$('form').validate().resetForm();
		$('#box-ordem').hide();
		$('#list-tipos-cores').empty();
		$('#list-tipos-tipografias').empty();
		$('#list-tipos-usos-incorretos').empty();
		$('#list-tipos-usos-aplicacoes').empty();
		$('#list-tipos-versoes').empty();
		$('#list-tipos-proporcoes').empty();
		$('#list-tipos-icones').empty();
		$('#list-tipos-enxovais').empty();
		$(".modal-title").text("Novo Produto");
		$('input[name=id]').val('');
		$('input[name=nome]').val('');
		$('input[name=cor_hexa]').val('');
		$('select option:contains("Selecione a Marca")').prop('selected', true);
		$('select option:contains("Selecione a Categoria")').prop('selected', true);
		$('#editStatus').val('1');
		$('#editStatus').prop('checked', true);
		$('#chkUseFamilyLogo').prop('checked', true);
		$('#list-tipos-logos .image-only').removeClass('has-uploaded');
		$('#list-tipos-logos .file-archive-only').removeClass('has-uploaded');
		$('#list-tipos-logos .ui-tooltip-2').attr("data-title", 'Nenhum arquivo.');
		$('#list-tipos-logos').find('tr:eq(1) .image-only').css('background', '');
		$('#editFile .ui-tooltip-2').attr("data-title", 'Nenhum arquivo.');

		$('#chkUseFamilyLogo').bind('change', function () {
			if ($(this).is(':checked')) {
				$('#logos-id input[type="file"]').val('');
				$('#editFile input[type="file"]').val('');
				$('#logos-id').hide();
				$('#editFile').hide();
			} else {
				$('#logos-id').show();
				$('#editFile').show();
			}
		});

		$("#editFamiliassss").change(function () {
			alert($(this).find(':selected').data('city'));
		});

		$('#list-tipos-logos').find('img').each(function () {
			$(this).attr("src", window.path + '/');
			$(this).error(function () {
				$(this).css('display', 'none')
			});
		});

		$('#list-tipos-logos').find('input[type="file"]').each(function () {
			$(this).val('');
		});

		$('#editFile img').attr("src", window.path + '/');

		$('#editFile img').error(function () {
			$(this).css('display', 'none')
		});

		if ($('#chkUseFamilyLogo').is(':checked')) {
			$('#logos-id input[type="file"]').val('');
			$('#editFile input[type="file"]').val('');
			$('#logos-id').hide();
			$('#editFile').hide();
		} else {
			$('#logos-id').show();
			$('#editFile').show();
		}

		checkId();
	});

	$('#list-produtos .btn-edit').on('click', function (e) {
		e.preventDefault();

		$('form').validate().resetForm();
		$('#box-ordem').show();
		$('#editOrdem').empty();
		$('#list-tipos-cores').empty();
		$('#list-tipos-tipografias').empty();
		$('#list-tipos-usos-incorretos').empty();
		$('#list-tipos-usos-aplicacoes').empty();
		$('#list-tipos-versoes').empty();
		$('#list-tipos-proporcoes').empty();
		$('#list-tipos-icones').empty();
		$('#list-tipos-enxovais').empty();
		$('input.minicolors').minicolors('value', '');
		$('#list-tipos-logos .image-only').removeClass('has-uploaded');
		$('#list-tipos-logos .file-archive-only').removeClass('has-uploaded');
		$('#list-tipos-logos .ui-tooltip-2').attr("data-title", 'Nenhum arquivo.');
		$('#list-tipos-logos').find('tr:eq(1) .image-only').css('background', '');
		$('#editFile .ui-tooltip-2').attr("data-title", 'Nenhum arquivo.');
		$('.modal-loading').show();
		$('.modalBtnGroup input[type="submit"]').prop('disabled', true);

		var val = '';
		var btn = $(this).attr('id');

		window.id = $('#' + btn).data('produto-id');

		$('#list-tipos-logos').find('img').css('display', 'none');
		$('#list-tipos-logos .file-archive-only').find('img').css('display', 'none');

		$('#list-tipos-logos').find('input[type="file"]').each(function () {
			$(this).val('');
		});

		$.ajax({
			type: 'get',
			url: window.path + '/admin/product/edit/' + window.id,
			context: document.body
		}).done(function (data) {
			$.each(data.count, function (i, item) {
				$('#editOrdem').append($('<option>', {
					value: item - 1,
					text: item
				}));
			});

			$('.modalBtnGroup input[type="submit"]').prop('disabled', false);

			var last = $('#editOrdem option:last-child').val();

			$('#editId').val(data.editProduct.id);
			$('#editNome').val(data.editProduct.nome);
			$('#cor_hexa').val(data.editProduct.cor_hexa);
			$('#editFamilia').val(data.editProduct.id_familia);
			$('#editCategoria').val(data.editProduct.id_categoria_produto);
			$('input.minicolors').minicolors('value', data.editProduct.cor_hexa);
			
			$('#list-tipos-logos').find('img').load(function () {
				$('.modal-loading').hide();
			});

			$('#list-tipos-logos').find('img').each(function () {
				$(this).attr("src", window.path + '/');
				$(this).error(function () {
					$(this).css('display', 'none');
					$(this).parents('.image-only').addClass('image-broken');
					$('.modal-loading').hide();
				});
			});

			$('#list-tipos-logos .file-archive-only').find('img').error(function () {
				$(this).css('display', 'none');
			});

			$('#chkUseFamilyLogo').bind('change', function () {
				if ($(this).is(':checked')) {
					$('input.minicolors').minicolors('value', '');
					$('#logos-id input[type="file"]').val('');
					$('#editFile input[type="file"]').val('');
					$('#list-tipos-logos .image-only').removeClass('has-uploaded');
					$('#editFile .file-archive-only').removeClass('has-uploaded');
					$('#list-tipos-logos .ui-tooltip-2').attr("data-title", 'Nenhum arquivo.');
					$('#list-tipos-logos').find('tr:eq(1) .image-only').css('background', '');
					$('#list-tipos-logos').find('tr:eq(0) img').attr('src', window.path + '/');
					$('#list-tipos-logos').find('tr:eq(1) img').attr('src', window.path + '/');
					$('#logos-id').hide();
					$('#editFile').hide();
				} else {
					$('#logos-id').show();
					$('#editFile').show();
				}
			});

			if (data.editProduct.ordem !== null) {
				$('#editOrdem').val(data.editProduct.ordem - 1);
			} else {
				$('#editOrdem').val(last);
			}

			if (data.editProduct.status == 1)
				$('#editStatus').prop("checked", true);
			else
				$('#editStatus').prop("checked", false);

			if (data.editProduct.artwork == 1)
				$('#editArtwork').prop("checked", true);
			else
				$('#editArtwork').prop("checked", false);

			if (data.editProduct.packshelf == 1)
				$('#editPackShelf').prop("checked", true);
			else
				$('#editPackShelf').prop("checked", false);

			if (data.editProduct.replicar_logo_marca == 1) {
				$('#chkUseFamilyLogo').prop("checked", true);
				$('#logos-id input[type="file"]').val('');
				$('#editFile input[type="file"]').val('');
				$('#list-tipos-logos .image-only').removeClass('has-uploaded');
				$('#editFile .file-archive-only').removeClass('has-uploaded');
				$('#list-tipos-logos .ui-tooltip-2').attr("data-title", 'Nenhum arquivo.');
				$('#logos-id').find('tr .image-only').css('background', '');
				$('#logos-id').find('tr img').attr('src', window.path + '/');
				$('#logos-id').hide();
				$('#editFile').hide();
			} else {
				$('#chkUseFamilyLogo').prop("checked", false);
				$('#logos-id').show();
				$('#editFile').show();
			}

			$(data.editProduct.logotipos).each(function (index, value) {
				if (data.editProduct.replicar_logo_marca == 0) {
					$('#list-tipos-logos').find('img').each(function (i) {
						if (value.id_tipo_logotipo == $(this).attr('id')) {
							$('#logos-id').show();
							$(this).parents('#list-tipos-logos').find('tr:eq(' + i + ') .image-only').removeClass('image-broken');
							$(this).parents('#list-tipos-logos').find('tr:eq(' + i + ') .image-only').addClass('has-uploaded');
							$(this).parents('#list-tipos-logos').find('tr:eq(' + i + ') img').css('display', 'block');
							$(this).parents('#list-tipos-logos').find('tr:eq(' + i + ') img').attr("src", window.path + value.thumb);
							$(this).parents('#list-tipos-logos').find('tr:eq(' + i + ') .ui-tooltip-2').attr("data-title", value.thumb.substring(value.thumb.lastIndexOf('/') + 1));

							if (value.id_tipo_logotipo == 1) {
								$(this).parents('#list-tipos-logos').find('tr:eq(0) .image-only').css('background', '');
								$(this).parents('#list-tipos-logos').find('tr:eq(1) .image-only').css('background', data.editProduct.cor_hexa);
							}
						}
					});
				}
			});

			$('#editFile img').error(function () {
				$('#editFile img').css('display', 'none')
			});

			if (data.editProduct.path_down) {
				$('#list-tipos-logos .file-archive-only').addClass('has-uploaded');
				$('#editFile img').attr("src", window.path + '');
				$('#editFile .ui-tooltip-2').attr("data-title", data.editProduct.path_down.substring(data.editProduct.path_down.lastIndexOf('/') + 1));
			}

			checkId();
		});

		$(".modal-title").text("Editar Produto");
	});

	$('#list-produtos .btn-remove').on('click', function (e) {
		e.preventDefault();

		if (confirm('Deseja realmente remover esse produto?') == true) {
			var btn = $(this).attr('id');
			window.id = $('#' + btn).data('produto-id');

			$('.lds-css').show();

			$.ajax({
				type: 'get',
				url: window.path + '/admin/product/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					$('.lds-css').hide(function () {
						alert('Produto Removido com Sucesso');
						window.location.href = window.path + '/admin/product/';
					});
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();
		var resp = window.id ? 'Produto Atualizado com Sucesso' : 'Produto Criado com Sucesso';

		if (!$(this).valid()) {
			return false;
		}

		$(this).validate().resetForm();
		$('.modal').modal('hide');

		$.ajax({
			url: window.id ? window.path + '/admin/product/update/' + window.id : window.path + '/admin/product/create',
			data: new FormData($("#form-product")[0]),
			type: 'post',
			processData: false,
			contentType: false,
			xhr: function () {
				var loading = 0;
				// get the native XmlHttpRequest object
				var xhr = $.ajaxSettings.xhr();
				// set the onprogress event handler
				xhr.upload.onprogress = function (evt) {
					loading = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
					$('#packShelfImgProgress').show();
					$('#svg').find('path').show();
					$('#svg').find('path').attr('stroke-dasharray', loading * 2.5 + ' ' + 250.2);
					$('#count').text(loading + '%');
				};
				// set the onload event handler
				xhr.upload.onload = function () {
					if (xhr.readyState === 4) {
						$('#packShelfImgProgress').hide();
						$('#svg').find('path').hide();
					}
				};
				// return the customized object
				return xhr;
			},
			success: function (data) {
				console.log(data)
				if (data.error) {
					$('#packShelfImgProgress').hide();
					$('#svg').find('path').hide();
					alert(data.message);
					return false;
				} else {
					$('#packShelfImgProgress').hide();
					$('#svg').find('path').hide();
					alert(resp);
					window.location.reload();
				}
			}
		});
	});

	$('.modal').on('show.bs.modal', function () {
		$('.modal').overlayScrollbars({
			autoUpdate: true
		});
	});

	$('.modal').on('hidden.bs.modal', function () {
		$('input[type=file]').val('');
		$('form').validate().resetForm();
		$('#list-tipos-logos .image-only').removeClass('has-uploaded');
		$('#list-tipos-logos .image-only').removeClass('image-broken');
		$('#list-tipos-logos .file-archive-only').removeClass('has-uploaded');
		$('#list-tipos-logos .ui-tooltip-2').attr("data-title", 'Nenhum arquivo.');
		$('#editFile .ui-tooltip-2').attr("data-title", 'Nenhum arquivo.');
	});
});

function checkId() {
	if ($('#editId').val() == '') {
		$('#manage-attrs').hide();
	} else {
		$('#manage-attrs').show();
	}
}