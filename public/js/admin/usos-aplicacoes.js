window.path = window.location.origin;

$(function () {
	$("form").validate({
		rules:{
			id_tipo_uso_aplicacao:{
				required: true
			}
		},
		messages:{
			id_tipo_uso_aplicacao:{
				required: "Selecione o tipo de uso e aplicação"
			}
		}
	});

	$('#btn-create').click(function () {
		window.id = null;
		$('form').validate().resetForm();
		$(".modal-title").text("Novo Uso e Aplicação");
		$('input[name=id]').val('');
		$('textarea').val('');
		$('#editStatus').val('1');
		$('select option:contains("Selecione o Tipo...")').prop('selected',true);
		$('#list-tipos-logos img').attr("src", window.path + '/images/backend/noImage.png');
		$('#list-tipos-logos').find('input[type="file"]').val('');
	});

	$('#list-usos-aplicacoes .btn-edit').on('click', function (e) {
		e.preventDefault();
		$('form').validate().resetForm();
		var btn = $(this).attr('id');
		window.id = $('#'+btn).data('uso-id');
		$('#list-tipos-logos img').attr("src", window.path + '/images/backend/noImage.png');
		$('#list-tipos-logos').find('input[type="file"]').val('');
		$.ajax({
			type: 'get',
			url: window.path + '/admin/product/application/edit/' + window.id,
			context: document.body
		}).done(function(data){
			$('#editId').val(data.editApplication.id);
			$('#editUsoAplicacao').val(data.editApplication.id_tipo_uso_aplicacao);
			$('#editDescricao').val(data.editApplication.descricao);
			$('#editStatus').val(data.editApplication.status);
			$('#list-tipos-logos img').attr("src", window.path + data.editApplication.thumb);
		});
		$(".modal-title").text("Editar Uso e Aplicação");
	});

	$('#list-usos-aplicacoes .btn-remove').on('click', function (e) {
		e.preventDefault();
		if (confirm("Deseja realmente remover esse uso e aplicação?") == true) {
			var btn = $(this).attr('id');
			var prodId = $('#list-usos-aplicacoes').data('prod-id');
			window.id = $('#'+btn).data('uso-id');
			$.ajax({
				type: 'get',
				url: window.path + '/admin/product/application/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					alert('Uso e Aplicação Removido com Sucesso');
					window.location.href = window.path + '/admin/product/application/' + prodId;
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();
		var prodId = $('#list-usos-aplicacoes').data('prod-id');
		var resp = window.id ? 'Uso e Aplicação Atualizado com Sucesso' : 'Uso e Aplicação Criado com Sucesso';
		$.ajax({
			url: window.id ? window.path + '/admin/product/application/update/' + window.id : window.path + '/admin/product/application/' + prodId + '/create',
			data:new FormData($("#form-application")[0]),
			type:'post',
			processData: false,
			contentType: false,
			success:function (data) {
				if(data.error){
					alert(data.message);
					return false;
				}
				else {
					alert(resp);
					window.location.reload();
				}
			},
		});
	});

});
