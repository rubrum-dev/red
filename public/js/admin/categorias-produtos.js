window.path = window.location.origin;

var getByName = function(name) {
	for(var i = 0; i < categorias.length; i++) {
		if (categorias[i].nome == name) {
			return categorias[i];
		}
	}
};

$(function () {
	$("form").validate({
		rules:{
			nome:{
				required: true,
				normalizer: function(value) {
					// Note: the value of `this` inside the `normalizer` is the corresponding
					// DOMElement. In this example, `this` reference the `username` element.
					// Trim the value of the input
					return $.trim(value);
				}
			}
		},
		messages:{
			nome:{
				required: "Nome é obrigatório."
			}
		}
	});

	$('#btn-create').click(function () {
		window.id = null;
		$('form').validate().resetForm();
		$(".modal-title").text("Nova Categoria de Produto");
		$('input[name=id]').val('');
		$('input[name=nome]').val('');
	});

	$('#list-category .btn-edit').on('click', function (e) {
		e.preventDefault();
		$('form').validate().resetForm();

		$('.modal-loading').show();
		$('.modalBtnGroup input[type="submit"]').prop('disabled', true);

		var btn = $(this).attr('id');
		window.id = $('#'+btn).data('category-id');
		$.ajax({
			type: 'get',
			url: window.path + '/admin/product-category/edit/' + window.id,
			context: document.body
		}).done(function(data){
			$('#editId').val(data.editCategory.id);
			$('#editNome').val(data.editCategory.nome);
			$('.modal-loading').hide();
			$('.modalBtnGroup input[type="submit"]').prop('disabled', false);
		});
		$(".modal-title").text("Editar Categoria de Produto");
	});

	$('#list-category .btn-remove').on('click', function (e) {
		e.preventDefault();
		if (confirm("Deseja realmente remover essa categoria de produto? \n\n* ATENÇÃO: Todos os Produtos vinculados a essa categoria serão removidos!") == true) {
			var btn = $(this).attr('id');
			window.id = $('#'+btn).data('category-id');
			$('.lds-css').show();
			$.ajax({
				type: 'get',
				url: window.path + '/admin/product-category/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					//alert('Categoria de Produto Removida com Sucesso');
					//window.location.reload();
					$('.lds-css').fadeOut('200', function() {
						alert('Categoria de Produto Removida com Sucesso');
						window.location.reload();
					});
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();

		var txtName = $("#editNome");
        var editNome = $.trim(txtName.val());
        
		txtName.val(editNome);
		
		var resp = window.id ? 'Categoria de Produto Atualizada com Sucesso' : 'Categoria de Produto Criada com Sucesso';
		
		if (!$(this).valid()) {
			return false;
		}

		for(var k = 0; k < categorias.length; k++) {
			if ( ( (categorias[k] && editNome !== categorias[k].nome) || !categorias[k] ) && getByName(editNome) && getByName(editNome).id !== window.id ) {
				alert('Já existe uma Categoria de Produto com este nome.')
				$('#editNome').focus();
				return false;
			} 
		}

		$(this).validate().resetForm();
		$('.modal').modal('hide');
		$('.lds-css').show();

		$.ajax({
			type: 'post',
			url: window.id ? window.path + '/admin/product-category/update/' + window.id : window.path + '/admin/product-category/create',
			data: $('form').serialize(),
			success: function () {
				//alert(resp);
				//window.location.reload();
				$('.lds-css').fadeOut('200', function() {
					alert(resp);
					window.location.reload();
				});	
			}
		});
	});

	$('.modal').on('show.bs.modal', function () {
		$('.modal').overlayScrollbars({
			autoUpdate: true
		});
	});
	
	$('.close').on('click', function () {
		$('form').validate().resetForm();
		$('.modal').modal('hide');
	});
});
