window.path = window.location.origin;

var getByName = function(name) {
	for(var i = 0; i < embalagens.length; i++) {
		if (embalagens[i].nome == name) {
			return embalagens[i];
		}
	}
};

$(function () {
	$("form").validate({
		rules: {
			nome: {
				required: true,
				normalizer: function(value) {
					// Note: the value of `this` inside the `normalizer` is the corresponding
					// DOMElement. In this example, `this` reference the `username` element.
					// Trim the value of the input
					return $.trim(value);
				}
			},
			id_natureza_embalagem: {
				required: true
			},
			ordem: {
				required: true
			}
		},
		messages: {
			nome: {
				required: 'Nome é obrigatório.'
			},
			id_natureza_embalagem: {
				required: 'Natureza é obrigatório.'
			},
			ordem: {
				required: 'Ordem de Exibição é obrigatório.'
			}
		}
	});

	$('#btn-create').click(function () {
		window.id = null;
		$('form').validate().resetForm();
		$('#box-ordem').hide();
		$(".modal-title").text("Novo Tipo de Embalagem");
		$('input[name=id]').val('');
		$('input[name=nome]').val('');
		$('#editStatus').val('1');
		$('#editStatus').prop('checked', true);
		$('select option:contains("Selecione a Natureza")').prop('selected', true);
	});

	$('#list-package .btn-edit').on('click', function (e) {
		e.preventDefault();
		$('form').validate().resetForm();
		$('#box-ordem').show();
		var btn = $(this).attr('id');
		var last = $('#editOrdem option:last-child').val();
		window.id = $('#' + btn).data('package-id');
		$.ajax({
			type: 'get',
			url: window.path + '/admin/package/edit/' + window.id,
			context: document.body
		}).done(function (data) {
			$('#editId').val(data.editPackage.id);
			$('#editNome').val(data.editPackage.nome);

			if (data.editPackage.ordem !== null) {
				$('#editOrdem').val(data.editPackage.ordem - 1);
			} else {
				$('#editOrdem').val(last);
			}

			if (data.editPackage.status == 1) {
				$('#editStatus').prop('checked', true);
			} else {
				$('#editStatus').prop('checked', false);
			}

			$('#editNatureza').val(data.editPackage.natureza.id);
		});

		$(".modal-title").text("Editar Tipo de Embalagem");
	});

	$('#list-package .btn-remove').on('click', function (e) {
		e.preventDefault();
		if (confirm("Deseja realmente remover esse tipo de embalagem? \n\n* ATENÇÃO: Todos os volumes e skus vinculados a esse tipo de embalagem serão removidos!") == true) {
			var btn = $(this).attr('id');
			window.id = $('#' + btn).data('package-id');
			$('.lds-css').show();
			$.ajax({
				type: 'get',
				url: window.path + '/admin/package/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					//alert('Tipo de Embalagem Removida com Sucesso');
					//window.location.reload();
					$('.lds-css').fadeOut('200', function() {
						alert('Tipo de Embalagem Removida com Sucesso');
						location.reload();                       
					});
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();

		var txtName = $("#editNome");
        var editNome = $.trim(txtName.val());
        
		txtName.val(editNome);
		
		var resp = window.id ? 'Tipo de Embalagem Atualizada com Sucesso' : 'Tipo de Embalagem Criada com Sucesso';

		if (!$(this).valid()) {
			return false;
		}

		if (embalagens) {
			for(var k = 0; k <= embalagens.length; k++) {
				if ( ( (embalagens[k] && editNome !== embalagens[k].nome) || !embalagens[k] ) && getByName(editNome) && getByName(editNome).id !== window.id ) {
					alert('Já existe um Item de Embalagem com este nome.')
					$('#editNome').focus();
					return false;
				} 
			}
		}

		$(this).validate().resetForm();
		$('.modal').modal('hide');
		$('.lds-css').show();

		$.ajax({
			type: 'post',
			url: window.id ? window.path + '/admin/package/update/' + window.id : window.path + '/admin/package/create',
			data: $('form').serialize(),
			success: function () {
				//alert(resp);
				//window.location.reload();
				$('.lds-css').fadeOut('200', function() {
					alert(resp);
					location.reload();                       
				});
			}
		});
	});

	$('.modal').on('show.bs.modal', function () {
		$('.modal').overlayScrollbars({
			autoUpdate: true
		});
	});
	
	$('.modal').on('hidden.bs.modal', function () {
		$('form').validate().resetForm();
	});
});