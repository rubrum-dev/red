window.path = window.location.origin;

$(function () {
	$("form").validate({
		rules:{
			nome:{
				required: true
			},
			id_produto:{
				required: true
			},
			image:{
				required: true
			}
		},
		messages:{
			nome:{
				required: "Digite o nome da promoção"
			},
			id_produto:{
				required: "Selecione um produto relacionado"
			},
			image:{
				required: "Selecione uma imagem para upload"
			}
		}
	});

	$("#editExpiracao").mask("99/99/9999",{placeholder:"dd/mm/aaaa"});

	$('#btn-create').click(function () {
		window.id = null;
		$('form').validate().resetForm();
		$(".modal-title").text("Nova Promoção");
		$('input[name=id]').val('');
		$('input[name=nome]').val('');
		$('input[name=dt_expiracao]').val('');
		$('#editStatus').val('1');
		$('#editStatus').prop('checked', true);
		$('#listFile img').attr("src", window.path + '/images/backend/noImage.png');
		$('#listFile').find('input[type="file"]').each(function() {
		    $(this).val('');
		});
	});

	$('#list-promocoes .btn-edit').on('click', function (e) {
		e.preventDefault();
		$('form').validate().resetForm();
		var btn = $(this).attr('id');
		window.id = $('#'+btn).data('promocao-id');
		$('#listFile img').attr("src", window.path + '/images/backend/noImage.png');
		$('#list-tipos-logos').find('input[type="file"]').each(function() {
		    $(this).val('');
		});
		$.ajax({
			type: 'get',
			url: window.path + '/admin/promotions/edit/' + window.id,
			context: document.body
		}).done(function(data){
			$('#editId').val(data.editPromocao.id);
			$('#editNome').val(data.editPromocao.nome);
			$('#editProduto').val(data.editPromocao.id_produto);
			$('#editExpiracao').val(data.editPromocao.dt_expiracao);
			$('#editStatus').val(data.editPromocao.status);

			if($('#editStatus').val() === '1') {
				$('#editStatus').prop('checked', true);
			} else {
				$('#editStatus').prop('checked', false);
			}

			if(data.editPromocao.thumb){
				$('#listFile img').attr("src", window.path + data.editPromocao.thumb);
			}
		});
		$(".modal-title").text("Editar Promoção");
	});

	$('#list-promocoes .btn-remove').on('click', function (e) {
		e.preventDefault();
		if (confirm("Deseja realmente remover essa promoção?") == true) {
			var btn = $(this).attr('id');
			window.id = $('#'+btn).data('promocao-id');
			$.ajax({
				type: 'get',
				url: window.path + '/admin/promotions/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					alert('Promoção Removida com Sucesso');
					window.location.href = window.path + '/admin/promotions/';
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();
		$('.btn-success').prop('disabled', true);
		var resp = window.id ? 'Promoção Atualizada com Sucesso' : 'Promoção Criada com Sucesso';
		$.ajax({
			url: window.id ? window.path + '/admin/promotions/update/' + window.id : window.path + '/admin/promotions/create',
			data:new FormData($("#form-ftps")[0]),
			type:'post',
			processData: false,
			contentType: false,
			success: function (data) {
				if(data.error){
					$('.btn-success').prop('disabled', false);
					alert(data.message);
					return false;
				}
				else {
					alert(resp);
					window.location.reload();
				}
			}
		});
	});
});
