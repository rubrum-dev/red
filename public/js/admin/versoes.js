window.path = window.location.origin;

$(function () {
	$("form").validate({
		rules:{
			id_categoria_versao:{
				required: true
			},
			id_tipo_proporcao_versao:{
				required: true
			}
		},
		messages:{
			id_categoria_versao:{
				required: "Selecione uma categoria"
			},
			id_tipo_proporcao_versao:{
				required: "Selecione um tipo de versão"
			}
		}
	});

	$('#btn-create').click(function () {
		window.id = null;
		$('form').validate().resetForm();
		$(".modal-title").text("Nova Versão");
		$('textarea').val('');
		$('#editStatus').val('1');
		$('select option:contains("Selecione o Tipo...")').prop('selected',true);
		$('select option:contains("Selecione a Categoria...")').prop('selected',true);
		$('#listImage img').attr("src", window.path + '/images/backend/noImage.png');
		$('#listFile img').attr("src", window.path + '/images/backend/noFile.png');
		$('#listImage img').width("70px");
		$('#listImage').find('input[type="file"]').val('');
		$('#listFile').find('input[type="file"]').val('');
	});

	$('#list-versoes .btn-edit').on('click', function (e) {
		e.preventDefault();
		$('form').validate().resetForm();
		var btn = $(this).attr('id');
		window.id = $('#'+btn).data('versao-id');
		$('#listImage img').attr("src", window.path + '/images/backend/noImage.png');
		$('#listFile img').attr("src", window.path + '/images/backend/noFile.png');
		$('#listImage img').width("70px");
		$('#listImage').find('input[type="file"]').val('');
		$('#listFile').find('input[type="file"]').val('');
		$.ajax({
			type: 'get',
			url: window.path + '/admin/product/version/edit/' + window.id,
			context: document.body
		}).done(function(data){
			$('#editId').val(data.editVersion.id);
			$('#editCategoria').val(data.editVersion.id_categoria_versao);
			$('#editTipo').val(data.editVersion.id_tipo_proporcao_versao);
			$('#editDescricao').val(data.editVersion.descricao);
			$('#editStatus').val(data.editVersion.status);
			if(data.editVersion.thumb){
				$('#listImage img').attr("src", window.path + data.editVersion.thumb);
			}
			if(data.editVersion.path_down){
				$('#listFile img').attr("src", window.path + '/images/backend/zipFile.png');
			}
		});
		$(".modal-title").text("Editar Versão");
	});

	$('#list-versoes .btn-remove').on('click', function (e) {
		e.preventDefault();
		if (confirm("Deseja realmente remover essa versão?") == true) {
			var btn = $(this).attr('id');
			var prodId = $('#list-versoes').data('prod-id');
			window.id = $('#'+btn).data('versao-id');
			$.ajax({
				type: 'get',
				url: window.path + '/admin/product/version/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					alert('Versão Removida com Sucesso');
					window.location.href = window.path + '/admin/product/version/' + prodId;
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();
		var prodId = $('#list-versoes').data('prod-id');
		var resp = window.id ? 'Versão Atualizada com Sucesso' : 'Versão Criada com Sucesso';
		$.ajax({
			url: window.id ? window.path + '/admin/product/version/update/' + window.id : window.path + '/admin/product/version/' + prodId + '/create',
			data:new FormData($("#form-version")[0]),
			type:'post',
			processData: false,
			contentType: false,
			success:function (data) {
				if(data.error){
					alert(data.message);
					return false;
				}
				else {
					alert(resp);
					window.location.reload();
				}
			}
		});
	});

});
