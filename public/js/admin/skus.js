var url = window.location.href;
var arr = url.split("/");
var path = arr[0] + "//" + arr[2];
window.path = path;

$(function () {
	$('form').validate({
		rules:{
			id_produto:{
				required: true
			},
			ordem:{
				required: true
			},
			id_dimensao:{
				required:{
					depends: function(){
						return $('input[name=promocional]:checked').val() == 0 ? true : false;
                	}
				}
			},
			nome_promo:{
				required:{
					depends: function(){
                    	return $('input[name=promocional]:checked').val() == 1 ? true : false;
                	}
				}
			},
			dt_expiracao:{
				required:{
					depends: function(){
						return $('input[name=promocional]:checked').val() == 1 ? true : false;
                	}
				},
            	dateBR: true
			}
		},
		messages:{
			id_produto:{
				required: 'Selecione o produto.'
			},
			ordem:{
				required: 'Selecione a ordem de exibição.'
			},
			id_dimensao:{
				required: 'Selecione o volume.'
			},
			nome_promo:{
				required: 'Digite um nome promocional.'
			},
			dt_expiracao:{
				required: 'Data de expiração obrigatória.'
			}
		}
	});

	$.validator.addMethod('dateBR', function(value, element) {
	    if(value.length!=10) return false;
	    var data   = value;
	    var dia    = data.substr(0,2);
	    var barra1 = data.substr(2,1);
	    var mes    = data.substr(3,2);
	    var barra2 = data.substr(5,1);
	    var ano    = data.substr(6,4);
	    if(data.length!=10||barra1!="/"||barra2!="/"||isNaN(dia)||isNaN(mes)||isNaN(ano)||dia>31||mes>12)return false;
	    if((mes==4||mes==6||mes==9||mes==11) && dia==31)return false;
	    if(mes==2  &&  (dia>29||(dia==29 && ano%4!=0)))return false;
	    if(ano < 1900)return false;
	    return true;
	}, 'Informe uma data válida');

	$('#editExpiracao').mask('99/99/9999', { placeholder: 'dd/mm/aaaa' });

	$('input[name=promocional]').click(function(){
		checkPromo();
	});

	$('#btn-create').click(function () {
		window.id = null;
		$('form').validate().resetForm();
		$('input[name=promocional][value="0"]').prop('checked', true);
		$('#td-artes-finais').empty();
		$(".modal-title").text("Nova Embalagem");
		$('input[name=id]').val('');
		$('input[name=nome]').val('');
		$('input[name=nome_promo]').val('');
		$('select option:contains("Selecione o Produto...")').prop('selected',true);
		$('select option:contains("Selecione o Volume...")').prop('selected',true);
		$('#editExpiracao').val('');
		$('#editExpiracao').prop('placeholder', 'dd/mm/aaaa');
		$('#editStatus').val('1');
		$('#listImage').find('input[type="file"]').val('');
		$('#listImage img').width("70px");
		$('#listImage img').attr("src", window.path + '/images/backend/noImage.png');
		checkPromo();
	});

	$('#list-sku .btn-edit').on('click', function (e) {
		e.preventDefault();
		$('form').validate().resetForm();
		$('#editOrdem').empty();
		$('#td-artes-finais').empty();
		$('input[name=nome_promo]').val('');
		var btn = $(this).attr('id');
		var val = '';
		window.id = $('#'+btn).data('sku-id');
		$('#listImage').find('input[type="file"]').val('');
		$('#listImage img').width("70px");
		$('#listImage img').attr("src", window.path + '/images/backend/noImage.png');
		$.ajax({
			type: 'get',
			url: window.path + '/admin/sku/edit/' + window.id,
			context: document.body
		}).done(function(data){
			$.each(data.count, function (i, item) {
			    $('#editOrdem').append($('<option>', {
					value: item - 1,
			        text : item
			    }));
			});
			var last = $('#editOrdem option:last-child').val();
			$('#editId').val(data.editSku.id);
			$('#editNome').val(data.editSku.nome);
			$('#editNomePromo').val(data.editSku.nome);
			$('#editProduto').val(data.editSku.id_produto);
			$('#editDimensao').val(data.editSku.id_dimensao);
			
			if(data.editSku.promocional == 1){
				$('input[name=promocional][value="1"]').prop('checked', true);
				$('#editExpiracao').val(data.editSku.dt_expiracao);
				if (data.editSku.thumb){
					$('#thumb-sku img').attr("src", window.path + data.editSku.thumb);
				}
			} else {
				$('input[name=promocional][value="0"]').prop('checked', true);
			}
			
			checkPromo();
			
			if(data.editSku.ordem !== null)
				$('#editOrdem').val(data.editSku.ordem - 1);
			else
				$('#editOrdem').val(last);
			
			$('#editStatus').val(data.editSku.status);
			$('#btnEditArtesFinais').attr("href", window.path + '/admin/sku/finalart/' + data.editSku.id);
			
			getFinalArts(data);
		});
		$(".modal-title").text("Editar Embalagem");
	});

	$('#list-sku .btn-remove').on('click', function (e) {
		e.preventDefault();
		if (confirm("Deseja realmente remover essa embalagem?") == true) {
			var btn = $(this).attr('id');
			window.id = $('#'+btn).data('sku-id');
			$.ajax({
				type: 'get',
				url: window.path + '/admin/sku/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					alert('Embalagem Removida com Sucesso');
					window.location.href = window.path + '/admin/sku/';
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();
		var resp = window.id ? 'Embalagem Atualizada com Sucesso' : 'Embalagem Criada com Sucesso';
		
		if (!$(this).valid()) {
			return false;
		}

		$(this).validate().resetForm();
		$('.modal').modal('hide');
		
		$.ajax({
			url: window.id ? window.path + '/admin/sku/update/' + window.id : window.path + '/admin/sku/create',
			data:new FormData($("#form-sku")[0]),
			type:'post',
			processData: false,
			contentType: false,
			success:function(data){
				if(data.error){
					alert(data.message);
					return false;
				}
				else {
					alert(resp);
					location.reload();
				}
			},
		});
	});
	
	$('.modal').on('hidden.bs.modal', function () {
		$('form').validate().resetForm();
	});
});

function checkPromo(){
	if($('input[name=promocional]:checked').val() == 0){
		$('#nome-promo').hide();
		$('#box-expiracao').hide();
		
		if($('#editId').val()){
			$('#box-nome').show();
			$('#box-ordem').show();
			$('#manage-attrs').show();
		}
		else{
			$('#box-nome').hide();
			$('#box-ordem').hide();
			$('#manage-attrs').hide();
		}
		
		$('#listImage').find('input[type="file"]').val('');
	}
	else{
		$('#nome-promo').show();
		$('#box-expiracao').show();
		$('#box-nome').hide();
	}
}

function getFinalArts(data)
{
	var val = '';
	var date = '';
	var cont = 1;
	$(data.editSku.artes_finais).each(function( index, value ) {
		if(value.promocional == 1){
			date = new Date(value.dt_expiracao);
			date = (date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear());
		}
		promo = value.promocional == 1 ? 'Promocional' : 'Regular';
		status = value.status == 1 ? 'Ativo' : 'Inativo';
		if(cont == 1)
			val += '<tr>';
		val += '<td style="border-top: none;">';
		val += '	<div class="box-sku-artes-finais">';
		val += '   		<img src="' + window.path + value.thumb + '" class="img-rounded img-logo" alt="alt">';
		val += '		<div class="box-sku-artes-finais-text">';
		val += 			value.nome + '<br />';
		if(value.arte_exibicao == 1)
			val += 			'Imagem de Exibição <br />';
		val += 				promo + '<br />';
		if(value.promocional == 1)
			val += 			date + '<br />';
		val += 				status;
		val += '		</div>';
		val += '	</div>';
		val += '</td>';
		if(cont % 3 == 0)
			val += '</tr>';
		cont++;
	});
	val += '</tr>';

	$('#td-artes-finais').append(val);
}
