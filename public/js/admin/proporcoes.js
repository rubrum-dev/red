window.path = window.location.origin;

$(function () {
	$("form").validate({
		rules:{
			id_categoria_proporcao:{
				required: true
			},
			id_tipo_proporcao_versao:{
				required: true
			}
		},
		messages:{
			id_categoria_proporcao:{
				required: "Selecione uma categoria"
			},
			id_tipo_proporcao_versao:{
				required: "Selecione um tipo de proporção"
			}
		}
	});

	$('#btn-create').click(function () {
		window.id = null;
		$('form').validate().resetForm();
		$(".modal-title").text("Nova Proporção");
		$('textarea').val('');
		$('#editStatus').val('1');
		$('select option:contains("Selecione o Tipo...")').prop('selected',true);
		$('select option:contains("Selecione a Categoria...")').prop('selected',true);
		$('#listImage img').attr("src", window.path + '/images/backend/noImage.png');
		$('#listImage img').width("70px");
		$('#listImage').find('input[type="file"]').val('');
	});

	$('#list-proporcoes .btn-edit').on('click', function (e) {
		e.preventDefault();
		$('form').validate().resetForm();
		var btn = $(this).attr('id');
		window.id = $('#'+btn).data('proporcao-id');
		$('#listImage img').attr("src", window.path + '/images/backend/noImage.png');
		$('#listImage img').width("70px");
		$('#listImage').find('input[type="file"]').val('');
		$.ajax({
			type: 'get',
			url: window.path + '/admin/product/proportion/edit/' + window.id,
			context: document.body
		}).done(function(data){
			$('#editId').val(data.editProportion.id);
			$('#editCategoria').val(data.editProportion.id_categoria_proporcao);
			$('#editTipo').val(data.editProportion.id_tipo_proporcao_versao);
			$('#editDescricao').val(data.editProportion.descricao);
			$('#editStatus').val(data.editProportion.status);
			if(data.editProportion.thumb){
				$('#listImage img').attr("src", window.path + data.editProportion.thumb);
			}
		});
		$(".modal-title").text("Editar Proporção");
	});

	$('#list-proporcoes .btn-remove').on('click', function (e) {
		e.preventDefault();
		if (confirm("Deseja realmente remover essa proporção?") == true) {
			var btn = $(this).attr('id');
			var prodId = $('#list-proporcoes').data('prod-id');
			window.id = $('#'+btn).data('proporcao-id');
			$.ajax({
				type: 'get',
				url: window.path + '/admin/product/proportion/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					alert('Proporção Removida com Sucesso');
					window.location.href = window.path + '/admin/product/proportion/' + prodId;
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();
		var prodId = $('#list-proporcoes').data('prod-id');
		var resp = window.id ? 'Proporção Atualizada com Sucesso' : 'Proporção Criada com Sucesso';
		$.ajax({
			url: window.id ? window.path + '/admin/product/proportion/update/' + window.id : window.path + '/admin/product/proportion/' + prodId + '/create',
			data:new FormData($("#form-proportion")[0]),
			type:'post',
			processData: false,
			contentType: false,
			success:function (data) {
				if(data.error){
					alert(data.message);
					return false;
				}
				else {
					alert(resp);
					window.location.reload();
				}
			}
		});
	});

});
