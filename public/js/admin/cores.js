window.path = window.location.origin;

$(function () {
	$("form").validate({
		rules:{
			nome:{
				required: true
			},
			id_tipo_cor:{
				required: true
			},
			id_nivel_cor:{
				required: true
			},
			spot:{
				required: true
			},
			cmyk:{
				required: true
			},
			rgb:{
				required: true
			},
			hex:{
				required: true
			}
		},
		messages:{
			nome:{
				required: "Digite o nome da embalagem"
			},
			id_tipo_cor:{
				required: "Selecione um tipo de cor"
			},
			id_nivel_cor:{
				required: "Selecione um nivel de cor"
			},
			spot:{
				required: "Digite a cor spot"
			},
			cmyk:{
				required: "Digite a cor cmyk"
			},
			rgb:{
				required: "Digite a cor rgb"
			},
			hex:{
				required: "Digite a cor web"
			}
		}
	});

	$('#editHex').focusout(function() {
		$('#editBoxColor').css('background-color', '#' + $('#editHex').val());
	});

	$('#btn-create').click(function () {
		window.id = null;
		$('form').validate().resetForm();
		$(".modal-title").text("Nova Cor");
		$('input[name=id]').val('');
		$('input[name=nome]').val('');
		$('input[name=spot]').val('');
		$('input[name=cmyk]').val('');
		$('input[name=rgb]').val('');
		$('input[name=hex]').val('');
		$('textarea').val('');
		$('#editStatus').val('1');
		$('#editBoxColor').css('background-color', '');
		$('select option:contains("Selecione o Tipo...")').prop('selected',true);
		$('select option:contains("Selecione o Nivel...")').prop('selected',true);
	});

	$('#list-cores .btn-edit').on('click', function (e) {
		e.preventDefault();
		$('form').validate().resetForm();
		var btn = $(this).attr('id');
		window.id = $('#'+btn).data('cor-id');
		$.ajax({
			type: 'get',
			url: window.path + '/admin/product/color/edit/' + window.id,
			context: document.body
		}).done(function(data){
			$('#editId').val(data.editColor.id);
			$('#editNome').val(data.editColor.nome);
			$('#editTipo').val(data.editColor.tipos_cores.id);
			$('#editNivel').val(data.editColor.niveis_cores.id);
			$('#editSpot').val(data.editColor.spot);
			$('#editCmyk').val(data.editColor.cmyk);
			$('#editRgb').val(data.editColor.rgb);
			$('#editHex').val(data.editColor.hex);
			$('#editDescricao').val(data.editColor.descricao);
			$('#editStatus').val(data.editColor.status);
			$('#editBoxColor').css('background-color', '#' + data.editColor.hex);
		});
		$(".modal-title").text("Editar Cor");
	});

	$('#list-cores .btn-remove').on('click', function (e) {
		e.preventDefault();
		if (confirm("Deseja realmente remover essa cor?") == true) {
			var btn = $(this).attr('id');
			window.id = $('#'+btn).data('cor-id');
			$.ajax({
				type: 'get',
				url: window.path + '/admin/product/color/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					alert('Cor Removida com Sucesso');
					window.location.reload();
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();
        var prodId = $('#list-cores').data('prod-id');
		var resp = window.id ? 'Cor Atualizada com Sucesso' : 'Cor Criada com Sucesso';
		$.ajax({
			type: 'post',
			url: window.id ? window.path + '/admin/product/color/update/' + window.id : window.path + '/admin/product/color/' + prodId + '/create',
			data: $('form').serialize(),
			success: function () {
				alert(resp);
				window.location.reload();
			}
		});
	});

});
