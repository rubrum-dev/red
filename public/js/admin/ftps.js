window.path = window.location.origin;

$(function () {
	$("form").validate({
		rules:{
			nome:{
				required: true
			},
			file:{
				required: true
			}
		},
		messages:{
			nome:{
				required: "Digite o nome do arquivo"
			},
			file:{
				required: "Selecione um arquivo para upload"
			}
		}
	});

	$('#txt-search').keyup(function(e){
        if(e.keyCode == 13)
        {
            $( "#btn-search" ).trigger( "click" );
        }
    });

	$('#list-ftps').tablesorter({
        headers: {
            3: { sorter: false }
        }
    });

	$('#btn-create').click(function () {
		window.id = null;
		$('form').validate().resetForm();
		$(".modal-title").text("Novo Arquivo");
		$('input[name=id]').val('');
		$('input[name=nome]').val('');
		$('#listFile img').attr("src", window.path + '/images/backend/noFile.png');
		$('#listFile').find('input[type="file"]').each(function() {
		    $(this).val('');
		});
	});

	$('#list-ftps .btn-edit').on('click', function (e) {
		e.preventDefault();
		$('form').validate().resetForm();
		var btn = $(this).attr('id');
		window.id = $('#'+btn).data('ftp-id');
		$('#listFile img').attr("src", window.path + '/images/backend/noFile.png');
		$('#list-tipos-logos').find('input[type="file"]').each(function() {
		    $(this).val('');
		});
		$.ajax({
			type: 'get',
			url: window.path + '/admin/ftp/edit/' + window.id,
			context: document.body
		}).done(function(data){
			$('#editId').val(data.editFtp.id);
			$('#editNome').val(data.editFtp.nome);
			if(data.editFtp.path){
				$('#listFile img').attr("src", window.path + '/images/backend/zipFile.png');
			}
		});
		$(".modal-title").text("Editar Arquivo");
	});

	$('#list-ftps .btn-remove').on('click', function (e) {
		e.preventDefault();
		if (confirm("Deseja realmente remover esse arquivo?") == true) {
			var btn = $(this).attr('id');
			window.id = $('#'+btn).data('ftp-id');
			$.ajax({
				type: 'get',
				url: window.path + '/admin/ftp/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					alert('Arquivo Removido com Sucesso');
					window.location.href = window.path + '/admin/ftp/';
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();
		$('#center-gif').show();
		$('.btn-success').prop('disabled', true);
		var resp = window.id ? 'Arquivo Atualizado com Sucesso' : 'Arquivo Criado com Sucesso';
		$.ajax({
			url: window.id ? window.path + '/admin/ftp/update/' + window.id : window.path + '/admin/ftp/create',
			data:new FormData($("#form-ftps")[0]),
			type:'post',
			processData: false,
			contentType: false,
			success: function (data) {
				if(data.error){
					$('#center-gif').hide();
					$('.btn-success').prop('disabled', false);
					alert(data.message);
					return false;
				}
				else {
					$('#center-gif').hide();
					alert(resp);
					window.location.reload();
				}
			}
		});
	});

	$('#list-ftps .copy-link').on('click', function (e) {
		var ftp =  $(this).parent().parent().find('.link-ftp').text();
		window.prompt("Copiar para a área de transferência: Ctrl+C, Enter", window.path + ftp);
	});

});
