window.path = window.location.origin;

$(function () {
	$("form").validate({
		rules:{
			id_categoria_icone:{
				required: true
			}
		},
		messages:{
			id_categoria_icone:{
				required: "Selecione uma categoria"
			}
		}
	});

	$('#btn-create').click(function () {
		window.id = null;
		$('form').validate().resetForm();
		$(".modal-title").text("Novo Ícone");
		$('textarea').val('');
		$('#editStatus').val('1');
		$('select option:contains("Selecione a Categoria...")').prop('selected',true);
		$('#listImage img').attr("src", window.path + '/images/backend/noImage.png');
		$('#listFile img').attr("src", window.path + '/images/backend/noFile.png');
		$('#listImage img').width("70px");
		$('#listImage').find('input[type="file"]').val('');
		$('#listFile').find('input[type="file"]').val('');
	});

	$('#list-icones .btn-edit').on('click', function (e) {
		e.preventDefault();
		$('form').validate().resetForm();
		var btn = $(this).attr('id');
		window.id = $('#'+btn).data('icone-id');
		$('#listImage img').attr("src", window.path + '/images/backend/noImage.png');
		$('#listFile img').attr("src", window.path + '/images/backend/noFile.png');
		$('#listImage img').width("70px");
		$('#listImage').find('input[type="file"]').val('');
		$('#listFile').find('input[type="file"]').val('');
		$.ajax({
			type: 'get',
			url: window.path + '/admin/product/icon/edit/' + window.id,
			context: document.body
		}).done(function(data){
			$('#editId').val(data.editIcon.id);
			$('#editCategoria').val(data.editIcon.id_categoria_icone);
			$('#editDescricao').val(data.editIcon.descricao);
			$('#editStatus').val(data.editIcon.status);
			if(data.editIcon.thumb){
				$('#listImage img').attr("src", window.path + data.editIcon.thumb);
			}
			if(data.editIcon.path_down){
				$('#listFile img').attr("src", window.path + '/images/backend/zipFile.png');
			}
		});
		$(".modal-title").text("Editar Ícone");
	});

	$('#list-icones .btn-remove').on('click', function (e) {
		e.preventDefault();
		if (confirm("Deseja realmente remover essa versão?") == true) {
			var btn = $(this).attr('id');
			var prodId = $('#list-icones').data('prod-id');
			window.id = $('#'+btn).data('icone-id');
			$.ajax({
				type: 'get',
				url: window.path + '/admin/product/icon/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					alert('Ícone Removido com Sucesso');
					window.location.href = window.path + '/admin/product/icon/' + prodId;
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();
		var prodId = $('#list-icones').data('prod-id');
		var resp = window.id ? 'Ícone Atualizado com Sucesso' : 'Ícone Criado com Sucesso';
		$.ajax({
			url: window.id ? window.path + '/admin/product/icon/update/' + window.id : window.path + '/admin/product/icon/' + prodId + '/create',
			data:new FormData($("#form-icon")[0]),
			type:'post',
			processData: false,
			contentType: false,
			success:function (data) {
				if(data.error){
					alert(data.message);
					return false;
				}
				else {
					alert(resp);
					window.location.reload();
				}
			}
		});
	});

});
