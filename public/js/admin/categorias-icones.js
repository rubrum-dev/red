window.path = window.location.origin;

$(function () {
	$("form").validate({
		rules:{
			nome:{
				required: true
			}
		},
		messages:{
			nome:{
				required: "Digite o nome da categoria"
			}
		}
	});

	$('#txt-search').keyup(function(e){
        if(e.keyCode == 13)
        {
            $( "#btn-search" ).trigger( "click" );
        }
    });

	$('#list-category').tablesorter({
        headers: {
            1: { sorter: false }
        }
    });

	$('#btn-create').click(function () {
		window.id = null;
		$('form').validate().resetForm();
		$(".modal-title").text("Nova Categoria de Ícone");
		$('input[name=id]').val('');
		$('input[name=nome]').val('');
	});

	$('#list-category .btn-edit').on('click', function (e) {
		e.preventDefault();
		$('form').validate().resetForm();
		var btn = $(this).attr('id');
		window.id = $('#'+btn).data('category-id');
		$.ajax({
			type: 'get',
			url: window.path + '/admin/icon-category/edit/' + window.id,
			context: document.body
		}).done(function(data){
			$('#editId').val(data.editCategory.id);
			$('#editNome').val(data.editCategory.nome);
		});
		$(".modal-title").text("Editar Categoria de Ícone");
	});

	$('#list-category .btn-remove').on('click', function (e) {
		e.preventDefault();
		if (confirm("Deseja realmente remover essa categoria de ícone? \n\n* ATENÇÃO: Todos os Ícones vinculados a essa categoria serão removidos!") == true) {
			var btn = $(this).attr('id');
			window.id = $('#'+btn).data('category-id');
			$.ajax({
				type: 'get',
				url: window.path + '/admin/icon-category/remove/' + window.id,
				data: $('form').serialize(),
				success: function () {
					alert('Categoria de Ícone Removida com Sucesso');
					window.location.reload();
				}
			});
		}
	});

	$('form').on('submit', function (e) {
		e.preventDefault();
		var resp = window.id ? 'Categoria de Ícone Atualizada com Sucesso' : 'Categoria de Ícone Criada com Sucesso';
		$.ajax({
			type: 'post',
			url: window.id ? window.path + '/admin/icon-category/update/' + window.id : window.path + '/admin/icon-category/create',
			data: $('form').serialize(),
			success: function () {
				alert(resp);
				window.location.reload();
			}
		});
	});

});
