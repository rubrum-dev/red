window.path = window.location.origin;

$(function () {

    $('.form-settings').validate({
        rules: {
            nome: {
                required: true
            },
            sobrenome: {
                required: true
            }
        },
        messages: {
            nome: {
                required: 'Digite o nome'
            },
            sobrenome: {
                required: 'Digite o sobrenome'
            }
        }
    });

    $('.user-settings-form').submit(function () {
        var foneRegex = /^(?:(?:\+|00)?(55)\s?)?(?:\(?([1-9][0-9])\)?\s?)?(?:((?:9\d|[2-9])\d{3})\-?(\d{4}))$/,
            isValid = false;

        if ($('#editNome').val() === '') {
            $('.input-error').removeClass('input-error');
            
            if (!isValid) {
                $('#editNome').addClass('input-error');
                $('#editNome').focus();
            }

            myAlert('É um campo obrigatório.', 'warning');
            isValid = true;
        } else if ($('#editSobrenome').val() === '') {
            $('.input-error').removeClass('input-error');
            
            if (!isValid) {
                $('#editSobrenome').addClass('input-error');
                $('#editSobrenome').focus();
            }

            myAlert('É um campo obrigatório.', 'warning');
            isValid = true;
        } else if ($('#departamento').val() === '') {
            $('.input-error').removeClass('input-error');
            
            if (!isValid) {
                $('#departamento').addClass('input-error');
                $('#departamento').focus();
            }

            myAlert('É um campo obrigatório.', 'warning');
            isValid = true;
        } else if ($('#editFone').val() === '' || !foneRegex.test($('#editFone').val())) {
            $('.input-error').removeClass('input-error');
            
            if (!isValid) {
                $('#editFone').addClass('input-error');
                $('#editFone').focus();
            }

            myAlert('Digite o telefone corretamente.', 'warning');
            isValid = true;
        } else if ($('#cargo').val() === '') {
            $('.input-error').removeClass('input-error');
            
            if (!isValid) {
                $('#cargo').addClass('input-error');
                $('#cargo').focus();
            }

            myAlert('É um campo obrigatório.', 'warning');
            isValid = true;
        } else if ($('#inputPass').val() === '') {
            $('.input-error').removeClass('input-error');
            
            if (!isValid) {
                $('#inputPass').addClass('input-error');
                $('#inputPass').focus();
            }

            myAlert('É um campo obrigatório.', 'warning');
            isValid = true;
        } else if ($('#inputPass').val().length < 6) {
            $('.input-error').removeClass('input-error');
            
            if (!isValid) {
                $('#inputPass').addClass('input-error');
                $('#inputPass').focus();
            }

            myAlert('Digite senha atual contendo no mínimo 6 caracteres.', 'warning');
            isValid = true;
        } else if ($('#inputNewPass').val() === '') {
            $('.input-error').removeClass('input-error');
            
            if (!isValid) {
                $('#inputNewPass').addClass('input-error');
                $('#inputNewPass').focus();
            }

            myAlert('É um campo obrigatório.', 'warning');
            isValid = true;
        } else if ($('#inputNewPass').val().length < 6) {
            $('.input-error').removeClass('input-error');
            
            if (!isValid) {
                $('#inputNewPass').addClass('input-error');
                $('#inputNewPass').focus();
            }

            myAlert('Digite a nova senha contendo no mínimo 6 caracteres.', 'warning');
            isValid = true;
        } else if ($('#inputConfNewPass').val() === '') {
            $('.input-error').removeClass('input-error');
            
            if (!isValid) {
                $('#inputConfNewPass').addClass('input-error');
                $('#inputConfNewPass').focus();
            }

            myAlert('É um campo obrigatório.', 'warning');
            isValid = true;

        } else if ($('#inputConfNewPass').val().length < 6) {
            $('.input-error').removeClass('input-error');
            
            if (!isValid) {
                $('#inputConfNewPass').addClass('input-error');
                $('#inputConfNewPass').focus();
            }

            myAlert('Repita a nova senha contendo no mínimo 6 caracteres.', 'warning');
            isValid = true;
        } else if ($('#inputNewPass').val() !== $('#inputConfNewPass').val()) {
            $('.input-error').removeClass('input-error');
            
            if (!isValid) {
                $('#inputNewPass').addClass('input-error');
                $('#inputNewPass').focus();
            }

            myAlert('Os dados inseridos não são iguais.', 'warning');
            isValid = true;
        } else if (!$('input[name="notificacao_favoritos"]').is(':checked')) {
            $('.input-error').removeClass('input-error');
            
            if (!isValid) {
                $('input[name="notificacao_favoritos"]').focus();
            }

            myAlert('Não foi marcada uma opção.', 'warning');
            isValid = true;
        }
        
        if (isValid) {
            return false;
        }
    });

    $('#editFone').inputmask({
        mask: ['(99) 9999-9999', '(99) 99999-9999', ],
        keepStatic: true
    });

    $('.btn-cancel').click(function () {
        $(location).attr('href', window.path + '/site');
    });

});