$(function () {
    var password = document.getElementById("inputNewPwd");
    var passwordStrength = document.getElementById("password-strength");
    //var lowUpperCase = document.querySelector(".low-upper-case i");
    var lowrCase = document.querySelector(".lower-case i");
    var upprCase = document.querySelector(".upper-case i");
    var number = document.querySelector(".one-number i");
    //var specialChar = document.querySelector(".one-special-char i");
    var sixChar = document.querySelector(".six-character i");

    password.addEventListener("keyup", function () {
        var pass = document.getElementById("inputNewPwd").value;
        checkStrength(pass);
    });

    function checkStrength(password) {
        var strength = 0;

        //If it has lowercase words
        if ((password.match(/[a-z]/) != null)) {
            strength += 1;
            lowrCase.classList.remove('fa-circle');
            lowrCase.classList.add('fa-check');
        
        } else {
            lowrCase.classList.add('fa-circle');
            lowrCase.classList.remove('fa-check');
        }

        //If it has uppercase words
        if ((password.match(/[A-Z]/) != null)) {
            strength += 1;
            upprCase.classList.remove('fa-circle');
            upprCase.classList.add('fa-check');
        
        } else {
            upprCase.classList.add('fa-circle');
            upprCase.classList.remove('fa-check');
        }

        //If it has numbers
        if (password.match(/([0-9])/)) {
            strength += 1;
            number.classList.remove('fa-circle');
            number.classList.add('fa-check');
        } else {
            number.classList.add('fa-circle');
            number.classList.remove('fa-check');
        }
        
        //If password is greater than 5
        if (password.length > 5) {
            strength += 1;
            sixChar.classList.remove('fa-circle');
            sixChar.classList.add('fa-check');
        } else {
            sixChar.classList.add('fa-circle');
            sixChar.classList.remove('fa-check');
        }

        if (password.length >= 8 && strength == 4) {
            strength += 1;
        }

        //If it has special characters
        //if (password.match(/([%,&,@,#,$])/)) {
        //    strength += 1;
        //}

        // If value is less than 2
        if (strength < 1) {
            passwordStrength.style = 'width: 10%';
            //document.getElementById('result').innerHTML = '<strong>A senha é fraca</strong>'
        } else if (strength == 2) {
            passwordStrength.style = 'width: 10%';
            //document.getElementById('result').innerHTML = '<strong>A senha é fraca</strong>'
        } else if (strength == 3) {
            passwordStrength.style = 'width: 20%';        
            //document.getElementById('result').innerHTML = '<strong>A senha é fraca</strong>'
        } else if (strength == 4) {
            passwordStrength.style = 'width: 50%';
            //document.getElementById('result').innerHTML = '<strong>A senha é média</strong>'
        } else if (strength > 4) {
            passwordStrength.style = 'width: 100%';
            //document.getElementById('result').innerHTML = '<strong>A senha é forte</strong>'
        
        }
    }
});