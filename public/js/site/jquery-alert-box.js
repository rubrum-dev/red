$(function() {
    function fancyPopup() {
        //Seleciona os elementos a com atributo rel="popup"
        $('a[rel=popup]').click(function(event) {
            event.preventDefault();

            var id = $(this).attr('href'),
                //Armazena a largura e a altura da tela
                maskWidth = $(window).width(),
                maskHeight = $(document).height(),
                //Armazena a largura e a altura da janela
                winW = $(window).width(),
                winH = $(window).height();

            //Define largura e altura do div#mask iguais ás dimensões da tela
            $('.popup-mask').css({
                width: maskWidth,
                height: maskHeight
            });

            $('.popup-mask').fadeIn(100);
            $('.popup-mask').fadeTo(100, 0.8);
            //Centraliza na tela a janela popup
            $(id).css('top', winH / 2 - $(id).height() / 2);
            $(id).css('left', winW / 2 - $(id).width() / 2);
            $(id).fadeIn(100);
        });

        //Se o botão fechar for clicado
        $('.popup .popup-close').click(function(event) {
            event.preventDefault();
            $('.popup-mask, .popup').hide();
        });

        $(window).bind('load resize', function(event) {
            if ($(this).width() <= 780) {
                $('a[rel=popup]').click(function() {
                    $('.popup').css({
                        top: '50%',
                        left: '50%',
                        transform: 'translate(-50%, -50%)'
                    });
                });
            }

            event.preventDefault();
        });
    }

    fancyPopup();
});