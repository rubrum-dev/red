$(function () {
    if($('input[name=regular]').val() == 0)
        $('#embalagens-regulares').hide();

    if($('input[name=promo]').val() == 0)
        $('#embalagens-promocionais').hide();

    $('.ticket-alt-request-form button[type="submit"]').click(function () {
        var counter = 0;

        $(this).parents('.ticket-alt-request-form').find('.input-required').each(function() {
            if ($(this).val() === '') {
                $(this).addClass('input-error');
                counter++;
            
            } else {
                $(this).removeClass('input-error');
            }
        });
        
        if(counter > 0){
            myAlert('Verifique o preenchimento do campo', 'warning');
        } else {
            $(this).parents('.ticket-alt-request-form').submit();
        }
    });
});
