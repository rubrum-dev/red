$(function () {
    $(document).delegate('.textbox-fake', 'keydown', function (event) {
        // backspace
        if (window.getSelection && event.which == 8) {
            // fix backspace bug in FF
            // https://bugzilla.mozilla.org/show_bug.cgi?id=685445
            var selection = window.getSelection();
            if (!selection.isCollapsed || !selection.rangeCount) {
                return;
            }
    
            var curRange = selection.getRangeAt(selection.rangeCount - 1);
            
            if (curRange.commonAncestorContainer.nodeType == 3 && curRange.startOffset > 0) {
                // we are in child selection. The characters of the text node is being deleted
                return;
            }
    
            var range = document.createRange();
            
            if (selection.anchorNode != this) {
                // selection is in character mode. expand it to the whole editable field
                range.selectNodeContents(this);
                range.setEndBefore(selection.anchorNode);
            } else if (selection.anchorOffset > 0) {
                range.setEnd(this, selection.anchorOffset);
            } else {
                // reached the beginning of editable field
                return;
            }
            
            range.setStart(this, range.endOffset - 1);
        
            var previousNode = range.cloneContents().lastChild;
            if (previousNode && previousNode.contentEditable == 'false') {
                // this is some rich content, e.g. smile. We should help the user to delete it
                range.deleteContents();
                event.preventDefault();
            }
        }
    });
    
    $('.check-edit-status').each(function () {
		$(this).on('change', function () {
			if($(this).is(':checked')) {
				$(this).parents('.chk-edition-control').find('.chk-edition-txt').text('Revisado');
			} else {
				$(this).parents('.chk-edition-control').find('.chk-edition-txt').text('Em Edição');
			}
		});
	});

    $('.check-edit-status').each(function() {
        $(this).on('click', function() {
            return false;
        });
    });

    $('.chk-edition-submenu li a').each(function() {
        var el = '<span></span>';

        $(this).click(function(e) {
            $(this).parents('.chk-edition-control').find('.chk-edition-txt').text('Revisado');
            $(this).parents('.chk-edition-control').siblings('.checked-info').show();
            $(this).parents('.chk-edition-control').find('.check-edit-status').prop('checked', !$(this).parents('.chk-edition-control').find('.check-edit-status').prop("checked"));

            if ($(this).parents('.chk-edition-control').find('.check-edit-status').prop('checked')) {
                $(this).parents('.chk-edition-control').siblings('.checked-info').show();
                $(this).parents('.chk-edition-control').find('.chk-edition-submenu li a').text('Em Edição');
                $(this).parents('.chk-edition-control').find('.chk-edition-submenu li a').append($(el).addClass('icon-three-points'));
                $(this).parents('.chk-edition-control').find('.chk-edition-txt').text('Revisado');
            } else {
                $(this).parents('.chk-edition-control').siblings('.checked-info').hide();
                $(this).parents('.chk-edition-control').find('.chk-edition-submenu li a').text('Revisado');
                $(this).parents('.chk-edition-control').find('.chk-edition-submenu li a').append($(el).addClass('icon-check'));
                $(this).parents('.chk-edition-control').find('.chk-edition-txt').text('Em Edição');
            }

            e.preventDefault();
        });
    });

    $('div').delegate('.teste-clique1', 'click', function(e) {

        if (!$(this).find('.icon').hasClass('fa-minus')) {
            $(this).parents('.accordion').find('.accordion-state-txt').text('Ocultar');
            $(this).parents('.accordion').find('h4 .icon').removeClass('fa-plus').addClass('fa-minus active');
        } else {
            $(this).parents('.accordion').find('.accordion-state-txt').text('Exibir');
            $(this).parents('.accordion').find('h4 .icon').removeClass('fa-minus active').addClass('fa-plus');
        }

        $(this).parents('.accordion').find('.accordion-content').delay(200).slideToggle(200);

        e.stopPropagation();
        e.preventDefault();
    });

	$('.files-control input').each(function() {
        $('.files-control').find('.files-control-fake strong').text('Anexar arquivo');
        
        $(document).on('change', '.files-control input', function() {
			$(this).each(function() {
	            var filePlaceholder = $(this).val().split('\\').pop();

                $(this).next('.files-control-fake').find('strong').text(filePlaceholder);
            });
	    });
    });

    $('.top-packing-logo .logo-down').click(function (e) {
		setTimeout(removeLoader, 500);
    });
    
    $('.btn-download-all-files > a').click(function (e) {
		setTimeout(removeLoader, 500);
	});
});
