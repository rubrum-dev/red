$(function () {
    $('#tfStartDate').mask('99/99/9999', {
        placeholder: 'dd/mm/aaaa'
    });

    $('#tfArtSendDate').mask('99/99/9999', {
        placeholder: 'dd/mm/aaaa'
    });

    $('#tfDeadlineDate').mask('99/99/9999', {
        placeholder: 'dd/mm/aaaa'
    });

    function customMultiFileInput() {
        $('.files-control-multi:last').find('.files-control-fake strong').text('Anexar arquivo com no máx. 100 MB')
        
        $('.files-control-multi:last input[type="file"]').on('click', function () {
            $(this).each(function () {
                $(this).val('');
                $(this).parents('.files-control-multi:last').find('.files-control-fake strong').text('Anexar arquivo com no máx. 100 MB');
            });
        });

        $('.files-control-multi:last input[type="file"]').on('change', function () {
            $(this).each(function () {
                var filePlaceholder = $(this).val().split('\\').pop();
                $(this).parents('.files-control-multi').find('.files-control-fake strong').text(filePlaceholder);
            });
        });
    }
        
    var addFileAttachBtn = $('#btnAddFileAttach');
    var fileAttachGroupTpl = $('.artw-attach-files-group:last').html();
    var fileAttachGroupTpl2 = '<div class="input-fields-group clearfix">' +
        '	<div class="form-inline column-upload">' +
        '		<div class="upload-control clearfix">' +
        '			<span class="icon-paperclip-oblique">Anexar</span>' +
        '			<input type="file" name="artworkFileUpload[]" />' +
        '			<span class="file-placeholder">Procurar...</span>' +
        '		</div>' +
        '	</div>' +
        '	<div class="form-inline btn-add">' +
        '		<button type="button" id="btnAddFileAttach" class="btn-remove-attach btn-hollow">' +
        '			<span class="icon-cross"></span> Remover' +
        '		</button>' +
        '	</div>' +
        '</div>';
    var buttonRemoveFileAttach = '<div class="btn-add flex add-files-submit-col">' + 
    '   <button type="button" id="btnRemoveFileAttach" class="btn-control btn-alt btn-remove-attach">Excluir</button>';
    '</div>';
    var x = 1;

    $(document).on("click", "#fileAttachWrap .btn-add-fields", function (e) {
        var fileInput = $('#fileAttachWrap input[type="file"]:last')[0];

        e.preventDefault();

        if ($(this).parents('.fileAttachWrap').find('.input-fields-group input[type="file"]:last').val() === '') {
            myAlert('Escolha o arquivo.', 'warning');

            return false;
        } else if(fileInput.files[0].size > 105255200) {
            myAlert('O tamanho máximo permitido para um arquivo é 100 MB.', 'warning');
            return false;
        } else {
            $(this).parents('.fileAttachWrap').find('.input-fields-group input').attr('readonly', true);

            if ($(this).parents('.fileAttachWrap').find('.artw-attach-files-group input[type="file"]').attr('readonly', true)) {
                $(this).parents('.fileAttachWrap').find('.artw-attach-files-group .files-control-multi').addClass('files-control-readonly');
            }

            $('.artw-attach-files-group:last .btn-add').replaceWith(buttonRemoveFileAttach);
            $('.artw-attach-files-group').append($(fileAttachGroupTpl).hide().fadeIn(300));
            
            x++;

            customMultiFileInput();
        }
    });

    $(document).on('click', '#fileAttachWrap .btn-remove-attach', function (e) {
        e.preventDefault();

        $(this).parents('.files-control-multi').fadeOut(200, function () {
            $(this).remove()

            x--;
        });
    });

    $('.only-alpha-numeric').bind('keyup', function () {
		var input = $(this).val();
		re = /[ !@#$%^´`&*_()+§ª°\=\[\]{};':"¨¬£³²¹|,.<>?]/gi;
		var isSplChar = re.test(input);

		if (isSplChar) {
			var noSplChar = input.replace(/[ !@#$%^´`&*_()+§ª°\=\[\]{};':"¨¬£³²¹|,.<>?]/gi, '');

			myAlert('Utilize apenas letras, números e alguns caracteres especiais como "&#45;", "&#47;" e "&#92;".', 'warning');
            $(this).val(noSplChar);
			$(this).focus();
		}
	});
    
    customMultiFileInput();
});