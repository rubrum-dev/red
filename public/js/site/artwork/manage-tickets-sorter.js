$(function() {
    $('.sort-tickets-table > thead > tr > th > a').each(function() {
        $(this).on('click', function() {
            if ($(this).parents('.sort-tickets-table > thead > tr > th').find('.icon').hasClass('icon-arrow-down-thin')) {
                $(this).parents('.sort-tickets-table > thead > tr > th').find('.icon').removeClass('icon-arrow-down-thin');
                $(this).parents('.sort-tickets-table > thead > tr > th').find('.icon').addClass('icon-arrow-up-thin');
            } else {
                $(this).parents('.sort-tickets-table > thead > tr > th').find('.icon').removeClass('icon-arrow-up-thin');
                $(this).parents('.sort-tickets-table > thead > tr > th').find('.icon').addClass('icon-arrow-down-thin');
            }
        });
    });

    $('select[name="cboTicketStatus"]').on('change', function() {
        $('.sort-tickets-table > tbody').show();

        var item = $(this),
            selection = $(this).val(),
            counter = $('.tickets-counter'),
            dataset = $('.sort-tickets-table > tbody').find('tr');

        dataset.fadeIn(200);

        var i = dataset.length;

        dataset.filter(function(index, item) {

            if ($(item).find('td:nth-child(6n+4)').text().split(',').indexOf(selection) === -1) {
                i--;
            }

            $(counter).text(i);

            return $(item).find('td:nth-child(6n+4)').text().split(',').indexOf(selection) === -1;
        }).hide();

        if ($(this).val() === 'Filtrar status dos tickets') {
            dataset.fadeIn(200);

            $(counter).text(dataset.length);
        }
    });
});

function sortTable(n) {
    var table,
        rows,
        switching,
        i,
        x,
        y,
        shouldSwitch,
        dir,
        switchCount = 0;

    table = document.getElementById('myTable2');
    switching = true;

    dir = 'asc';

    while (switching) {
        switching = false;
        rows = table.getElementsByTagName('tr');

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;

            x = rows[i].getElementsByTagName('td')[n];
            y = rows[i + 1].getElementsByTagName('td')[n];

            if (dir === 'asc') {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                    break;
                }
            } else if (dir === 'desc') {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                    break;
                }
            }

        }

        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            switchCount++;
        } else {
            if (switchCount === 0 && dir === 'asc') {
                dir = 'desc';
                switching = true;
            }
        }
    }
}