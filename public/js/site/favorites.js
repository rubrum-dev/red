window.path = window.location.origin;

$(function () {
	$('.btn-delete').click(function () {
		var btn = $(this).attr('id'),
			family = $('#' + btn).data('family-id'),
			product = $('#' + btn).data('prod-id');

		function deleteFavoriteConfirm(message) {
			if ($.confirm) {
    			$.confirm({
					icon: 'icon fa fa-question',
					title: 'Confirmar',
					content: message,
					closeIconClass: 'jconfirm-btn-close fa fa-times',
					escapeKey: 'cancel',
    				buttons: {
						ok: {
							text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
							btnClass: 'btn-confirm',
							keys: ['enter', 'space'],
            				action: function () {
								$.ajax({
									type: 'get',
									url: window.path + '/site/' + family + '/produtos/' + product + '/favorite',
									context: document.body
								}).done(function (data) {
									deleteFavoriteSuccess(data.prod + ' ' + 'excluido com sucesso.');
								});
							}
						},
						cancel: {
							text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
							btnClass: 'btn-cancel'
						}
					}
				});
			} else {
				window.confirm(message);
			}
		}

		function deleteFavoriteSuccess(message) {
			if ($.alert) {
				$.alert({
					icon: 'icon fa fa-check',
					title: 'Sucesso',
					content: message,
					escapeKey: 'ok',
    				buttons: {
						ok: {
							text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
							btnClass: 'btn-confirm',
							keys: ['enter', 'space'],
            				action: function () {
								location.reload();
							}
						}
					},
					onClose: function() {
						location.reload();
					}
				});
			} else {
				alert(message);
			}
		}

		deleteFavoriteConfirm('Deseja excluir esse favorito?');
	});
});