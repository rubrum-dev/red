var customLoadingHTML = '<div class="line-wobble"></div>';

function modalPesquisaAgradecimento() {
    $.dialog({
        title: '',
        content: 'url:/site/pesquisa_agradecimento?v=19082024',
        animateFromElement: false,
        closeIcon: true,
        useBootstrap: false,
        boxWidth: '990px',
        scrollToPreviousElement: false,
        closeIconClass: 'jconfirm-btn-close fa fa-times',
        escapeKey: true,
        onContentReady: function () {
            $('.jconfirm-box .line-wobble').remove();        
        },
        onOpenBefore: function () {
            $('.jconfirm-box').append(customLoadingHTML);                    
            $('.jconfirm-box').addClass('jconfirm-box-alt');
            $('body').addClass('jconfirm-overlay');
            $('body').addClass('no-scroll');
            $('.jconfirm-content').css('marginLeft', '0px');
            $('.jconfirm-buttons').remove();
            $('.master').addClass('blurred');
            $('.jconfirm').addClass('jc-pesquisa-agradecimento');
            $('.jconfirm').css('overflow', 'auto');
            $('.jconfirm').overlayScrollbars({
                scrollbars: {
                    autoHide: 'scroll'
                }
            });
        },
        onClose: function () {
            $('body').removeClass('jconfirm-overlay');
            $('body').removeClass('no-scroll');
            $('.master').removeClass('blurred');
            $('.jconfirm').removeClass('jc-pesquisa-agradecimento');
            $('.jconfirm').css('overflow', 'hidden');
        }
    });
}
    
function modalPesquisa() {
    $.dialog({
        title: '',
        content: 'url:/site/pesquisa?v=19082024',
        animateFromElement: false,
        closeIcon: true,
        useBootstrap: false,
        boxWidth: '990px',
        scrollToPreviousElement: false,
        closeIconClass: 'jconfirm-btn-close fa fa-times',
        escapeKey: true,
        onContentReady: function () {
            var loadCounter = 0;

            var loadIframe = function () {
                loadCounter += 1;

                if (loadCounter > 1) {
                    var osInstance = $('.jconfirm').overlayScrollbars();

                    osInstance.scroll({ y : "0px"  }, 500)
                }
            }

            $('iframe').on('load', function () {
                $('.jconfirm-box .line-wobble').remove();    
                
                loadIframe();
            });
        },
        onOpenBefore: function () {
            $('.jconfirm-box').append(customLoadingHTML);                    
            $('.jconfirm-box').addClass('jconfirm-box-alt');
            $('body').addClass('jconfirm-overlay');
            $('body').addClass('no-scroll');
            $('.jconfirm-content').css('marginLeft', '0px');
            $('.jconfirm-buttons').remove();
            $('.master').addClass('blurred');
            $('.jconfirm').css('overflow', 'auto');
            $('.jconfirm').overlayScrollbars({
                scrollbars: {
                    autoHide: 'scroll'
                }
            });
        },
        onClose: function () {
            $('body').removeClass('jconfirm-overlay');
            $('body').removeClass('no-scroll');
            $('.master').removeClass('blurred');
            $('.jconfirm').css('overflow', 'hidden');
        }
    })
}

function siteNews(origem) {
    switch (origem) {
        case 'click':
            $.confirm({
                title: '',
                content: 'url:/site/news/novidades?v=12022025',
                animateFromElement: false,
                closeIcon: true,
                useBootstrap: false,
                boxWidth: '990px',
                scrollToPreviousElement: false,
                closeIconClass: 'jconfirm-btn-close fa fa-times',
                escapeKey: true,
                onContentReady: function () {
                    $('.custom-alert-heading h4 .dont-show-again').remove();
                    $('.jconfirm-box .line-wobble').remove();    
                },
                onOpenBefore: function () {
                    $('.jconfirm-box').append(customLoadingHTML);                    
                    $('.jconfirm-box').addClass('jconfirm-box-alt');
                    $('body').addClass('no-scroll');
                    $('.jconfirm-content').css('marginLeft', '0px');
                    $('.cr-show, .cr-hide').remove();
                    $('.jconfirm-buttons').remove();
                    $('.jconfirm').css('overflow', 'auto');

                    $('.jconfirm').overlayScrollbars({
                        scrollbars: {
                            autoHide: 'scroll'
                        }
                    });
                },
                onClose: function () {
                    $('body').removeClass('no-scroll');
                    $('.jconfirm').css('overflow', 'hidden');
                }
            });
            break;
        default:
            $.confirm({
                title: '',
                content: 'url:/site/news/novidades?v=12022025',
                animateFromElement: false,
                closeIcon: true,
                useBootstrap: false,
                boxWidth: '990px',
                scrollToPreviousElement: false,
                closeIconClass: 'jconfirm-btn-close fa fa-times',
                escapeKey: true,
                onContentReady: function () {
                    $('.jconfirm-box .line-wobble').remove();    
                },
                onOpenBefore: function () {
                    $('.jconfirm-box').append(customLoadingHTML);                    
                    $('.jconfirm-box').addClass('jconfirm-box-alt');
                    $('body').addClass('no-scroll');
                    $('.jconfirm-content').css('marginLeft', '0px');
                    $('.cr-show, .cr-hide').remove();
                    $('.jconfirm-buttons').remove();
                    $('.jconfirm').css('overflow', 'auto');

                    $('.jconfirm').overlayScrollbars({
                        scrollbars: {
                            autoHide: 'scroll'
                        }
                    });
                },
                onClose: function () {
                    $('body').removeClass('no-scroll');
                    $('.jconfirm').css('overflow', 'hidden');
                }
            });
            
            //modalPesquisa();
            //modalPesquisaAgradecimento();
            
            break;
    }
}
            
function exibir_news() {
    var checkbox = $('#dontShowAgain');

    $.ajax({
        url: '/site/news/getExibir',
        type: 'POST',
        data: {
            _token: csrf_token,
            dontShowNews: checkbox.is(':checked')
        },
        success: function (data) {
            if (checkbox.is(':checked')) {
            } else {
            }
        }
    });
}