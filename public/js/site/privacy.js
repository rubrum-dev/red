$(function () {
    $('body').niceScroll({
        cursorcolor: '#303030',
        cursoropacitymin: 0,
        cursoropacitymax: 0.5,
        cursorwidth: '8px',
        cursorborder: '1px solid transparent',
        zindex: 99999999,
        autohidemode: 'cursor',
        hidecursordelay: 0,
    });

    $('.sidebar-privacidade-ul').visualNav({
        link: 'a.privacidade-link',
        contentClass: 'section',
        useHash: false,
        offsetTop: 80,
        bottomMargin: 80,
        animationTime: 500,
        changed: function (visNav, $selected) {}
    });

    setTimeout(() => {
        history.replaceState('', document.title, window.location.origin + window.location.pathname + window.location.search);
    }, 5);
});