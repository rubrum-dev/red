var site_url = '/';

var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
var start_time = new Date().getTime();
    
$(function () {
    $('.fa-spin').hide();
    
    $('body').overlayScrollbars({
        className: 'os-theme-light',
        scrollbars: {
            autoHide: 'scroll'
        }
    });
    
    function tooltipShowHideFewSeconds() {
        setTimeout(function () {
            $('.tooltip2 .tooltiptext').addClass('visible');
        }, 0);

        setTimeout(function () {
            $('.tooltip2 .tooltiptext').removeClass('visible');
        }, 6000);
    }
    
    function formLogin() {
        var emailRegex = /^[a-z0-9][a-z0-9-_\.]+@([a-z]|[a-z0-9]?[a-z0-9-]+[a-z0-9])\.[a-z0-9]{2,10}(?:\.[a-z]{2,10})?$/;

        $('.no-space').on('input', function () {
            $(this).val($(this).val().replace(/ /g, ''));
        });

        $('.input-email-only').on('input', function () {
            $(this).val($(this).val().replace(/ /g, ""));
            $(this).val($(this).val().toLowerCase());    
        });

        $('.sign-in').on('click', function () {
            $(this).css('pointerEvents', 'none');
            $(this).addClass('hidden');
            $(this).parents('.home-container').find('.back').css('pointerEvents', 'all');
            $(this).parents('.home-container').find('.access-method-txt').addClass('hidden');
            $(this).parents('.home-container').find('.free-access').addClass('hidden');
            $(this).parents('.home-container').find('.login-sso').addClass('hidden');
            $(this).parents('.home-container').find('.form-sign-in').animate({
                opacity: 'toggle'
            }, 200);
        });

        $('.back').on('click', function() {
            $(this).css('pointerEvents', 'all');
            $(this).parents('.home-container').find('.form-sign-in').hide();
            $('.access-method-txt').removeClass('hidden').hide().fadeIn(200);
            $('.free-access').removeClass('hidden').hide().fadeIn(200);
            $('.sign-in').css('pointerEvents', 'all');
            $('.sign-in').removeClass('hidden').hide().fadeIn(200);
            $('.login-sso').removeClass('hidden').hide().fadeIn(200);
            $('.input-control').removeClass('has-error');
            $('.input-control').val('');

            tooltipShowHideFewSeconds();
        });

        $('.form-sign-in').submit(function() {
            if($('#inputEmail').val() === '' || !emailRegex.test($('#inputEmail').val())) {
                $('#inputEmail').focus();
                $('#inputEmail').addClass('has-error');
                myAlert('Verifique o preenchimento do campo.', 'warning');
                return false;
            } else {
                $('#inputEmail').removeClass('has-error');
            } 
            
            if($('#inputPassword').val() === '') {
                $('#inputPassword').focus();
                $('#inputPassword').addClass('has-error');
                myAlert('Verifique o preenchimento do campo.', 'warning');
                return false;
            } else {
                $('#inputPassword').removeClass('has-error');
            }

            if (!isSafari) {
                $(window).bind('beforeunload', function () {
                    $('.btn-registered').addClass('proccessing');
                    $('.btn-registered > span > strong').text('')
                    $('.btn-registered > span > .fa-lock').hide();
                    $('.fa-spin').show();
                });
        
                $(window).bind('unload', function () {
                    setTimeout(function() {
                        $('.btn-registered').removeClass('proccessing');
                        $('.btn-registered > span > strong').text('Acessar');
                        $('.btn-registered .fa-lock').show();
                        $('.fa-spin').hide();
                    }, new Date().getTime() - start_time);
                });
            } else {
                return;
            }
        });

        $('.form-first-access').submit(function () {
            let pass_1 = $('#inputNewPwd').val();
            let pass_2 = $('#inputPwdConfirm').val();

            if(pass_1 === '') {
                $('#inputNewPwd').focus();
                $('#inputNewPwd').addClass('has-error');
                myAlert('Verifique o preenchimento do campo.', 'warning');
                return false;
            } else {
                $('#inputNewPwd').removeClass('has-error');
            }

            if(!pass_1.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
                $('#inputNewPwd').focus();
                $('#inputNewPwd').addClass('has-error');
                myAlert('A senha deve conter letras maiúsculas e minúsculas.', 'warning');
                return false;
            } else {
                $('#inputNewPwd').removeClass('has-error');
            }

            if(!pass_1.match(/([0-9])/)) {
                $('#inputNewPwd').focus();
                $('#inputNewPwd').addClass('has-error');
                myAlert('A senha deve conter números.', 'warning');
                return false;
            } else {
                $('#inputNewPwd').removeClass('has-error');
            }

            if(pass_1.length < 6) {
                $('#inputNewPwd').focus();
                $('#inputNewPwd').addClass('has-error');
                myAlert('A senha deve conter no mínimo 6 caracteres.', 'warning');
                return false;
            } else {
                $('#inputNewPwd').removeClass('has-error');
            }
            
            if(pass_2 === '') {
                $('#inputPwdConfirm').focus();
                $('#inputPwdConfirm').addClass('has-error');
                myAlert('Verifique o preenchimento do campo.', 'warning');
                return false;
            } else {
                $('#inputPwdConfirm').removeClass('has-error');
            }

            if(pass_2.length < 6) {
                $('#inputPwdConfirm').focus();
                $('#inputPwdConfirm').addClass('has-error');
                myAlert('A senha deve conter no mínimo 6 caracteres.', 'warning');
                return false;
            } else {
                $('#inputPwdConfirm').removeClass('has-error');
            }

            if(pass_1 !== pass_2) {
                $('#inputPwdConfirm').focus();
                $('#inputPwdConfirm').addClass('has-error');
                myAlert('Os dados preenchidos não são iguais.', 'warning');
                return false;
            } else {
                $('#inputPwdConfirm').removeClass('has-error');
            }

            if(!$('#chkAceitarTermos').is(':checked')) {
                myAlert('É necessário marcar que aceita os Termos de Uso.', 'warning');
                return false;
            }

            if(!$('#chkAceitarPoliticaPrivacidade').is(':checked')) {
                myAlert('É necessário marcar que concorda com a Política de Privacidade.', 'warning');
                return false;
            }

            if (!isSafari) {
                $(window).bind('beforeunload', function () {
                    $('.btn-salvar-acessar').addClass('proccessing');
                    $('.btn-salvar-acessar > span > strong').text('')
                    $('.btn-salvar-acessar > span > .fa-lock').hide();
                    $('.fa-spin').show();
                });
        
                $(window).bind('unload', function () {
                    setTimeout(function() {
                        $('.btn-salvar-acessar').removeClass('proccessing');
                        $('.btn-salvar-acessar > span > strong').text('Salvar e Acessar');
                        $('.btn-salvar-acessar > span > .fa-lock').show();
                        $('.fa-spin').hide();
                    }, new Date().getTime() - start_time);
                });
            } else {
                return;
            }
        });

        $('.form-reminder').submit(function () {
            if($('#inputEmail').val() === '' || !emailRegex.test($('#inputEmail').val())) {
                $('#inputEmail').focus();
                $('#inputEmail').addClass('has-error');
                myAlert('Verifique o preenchimento do campo.', 'warning');
                return false;
            } else {
                $('#inputEmail').removeClass('has-error');
            }

            if (!isSafari) {
                $(window).bind('beforeunload', function () {
                    $('.btn-recuperar-acesso').addClass('proccessing');
                    $('.btn-recuperar-acesso > span > strong').text('')
                    $('.fa-spin').show();
                    $('.btn-recuperar-acesso > span > .fa-key').hide();
                });
        
                $(window).bind('unload', function () {
                    setTimeout(function() {
                        $('.btn-recuperar-acesso').removeClass('proccessing');
                        $('.btn-recuperar-acesso > span > strong').text('Recuperar Acesso e Senha');
                        $('.fa-spin').hide();
                        $('.btn-recuperar-acesso > span > .fa-key').show();
                    }, new Date().getTime() - start_time);
                });
            } else {
                return;
            }
        });
    }

    tooltipShowHideFewSeconds();
    formLogin();
});

function reenviarToken(id_usuario) {
    $.get(site_url + 'login/gerarToken/recuperar/'+id_usuario, function (data) {
        myAlert('Um link para recuperar seu acesso foi enviado por e-mail, verifique sua caixa de entrada e siga as instruções.', 'success');
    }, 'json')
    .fail(function (data) {
        myAlert('Ocorreu um erro.', 'error');
    });
}