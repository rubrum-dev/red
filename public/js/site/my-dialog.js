$(function () {
    $(document).delegate('.link-ajuda', 'click', function (event) {
        $.dialog({
            title: '',
            content: '<div class="custom-alert-contents default">' +
                '   <span class="icon fa fa-comments"></span>' +
                '   <div class="custom-alert-heading">' +
                '       <h4>Suporte</h4>' +
                '   </div>' +
                '   <p class="txt-regular">Entre em contato conosco para relatar problemas ou dúvidas.</p>' +
                '   <div class="media-container media-object-indent no-margin-bottom">' +
                '       <div class="media-object">' +
                '           <span class="fa fa-envelope"></span>' +
                '           <div class="media-object-content">' +
                '               <h4>suporte@rubrum.com.br</h4>' +
                '               <p><small>Horário comercial de segunda à sexta-feira</small></p>' +
                '           </div>' +
                '       </div>' +
                '   </div>' +
                '</div>',
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            boxWidth: '910px',
            useBootstrap: false,
            onOpenBefore: function () {
                $('.jconfirm .jconfirm-box div.jconfirm-content-pane').css('marginBottom', '95px');
                //$('body').addClass('jconfirm-overlay');
                $('body').addClass('no-scroll');
                $('.jconfirm-content').css('marginLeft', '0px');
                //$('.master').addClass('blurred');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('.jconfirm .jconfirm-box div.jconfirm-content-pane').css('marginBottom', '50px');
                //$('body').removeClass('jconfirm-overlay');
                $('body').removeClass('no-scroll');
                //$('.master').removeClass('blurred');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    });
});