$(function() {
    $('.table-gerenciar-grupos > thead > tr > th > a').on('click', function() {
        if ($(this).parents('.table-gerenciar-grupos > thead > tr').find('.icon').hasClass('icon-arrow-down')) {
            $(this).parents('.table-gerenciar-grupos > thead > tr').find('.icon').removeClass('icon-arrow-down');
            $(this).parents('.table-gerenciar-grupos > thead > tr').find('.icon').addClass('icon-arrow-up');
        } else {
            $(this).parents('.table-gerenciar-grupos > thead > tr').find('.icon').removeClass('icon-arrow-up');
            $(this).parents('.table-gerenciar-grupos > thead > tr').find('.icon').addClass('icon-arrow-down');
        }
    });
});

function sortTable(n) {
    var table,
        rows,
        switching,
        i,
        x,
        y,
        shouldSwitch,
        dir,
        switchCount = 0;

    table = document.getElementById('myTable2');
    switching = true;

    dir = 'asc';

    while (switching) {
        switching = false;
        rows = table.getElementsByTagName('tr');

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;

            x = rows[i].getElementsByTagName('td')[n];
            y = rows[i + 1].getElementsByTagName('td')[n];

            if (dir === 'asc') {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                    break;
                }
            } else if (dir === 'desc') {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                    break;
                }
            }

        }

        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            switchCount++;
        } else {
            if (switchCount === 0 && dir === 'asc') {
                dir = 'desc';
                switching = true;
            }
        }
    }
}