$(function () {
    $('#dataTables_table_0').on('processing.dt', function (e, settings, processing) {
        var loadingPlaceholder = '';
        
        loadingPlaceholder += '<div class="content-placeholder-container">';
        loadingPlaceholder += '    <div class="dt-loading-indicator">';
        loadingPlaceholder += '        <div class="flexbox-container margin-bottom-15">';
        loadingPlaceholder += '            <div class="flex flex-3-large">';
        loadingPlaceholder += '                <div class="linear-background dt-filter-search"></div>';
        loadingPlaceholder += '            </div>';
        loadingPlaceholder += '            <div class="flex flex-3-large">';
        loadingPlaceholder += '                <div class="linear-background dt-filter-select"></div>';
        loadingPlaceholder += '            </div>';
        loadingPlaceholder += '            <div class="flex margin-left-auto">';
        loadingPlaceholder += '                <div class="btn-help linear-background no-hover"></div>';
        loadingPlaceholder += '            </div>';
        loadingPlaceholder += '            <div class="flex">';
        loadingPlaceholder += '                <div class="btn-function no-hover"><i class="icon linear-background"></i></div>';
        loadingPlaceholder += '            </div>';
        loadingPlaceholder += '            <div class="flex">';
        loadingPlaceholder += '                <div class="btn-function no-hover"><i class="icon linear-background"></i></div>';
        loadingPlaceholder += '            </div>';
        loadingPlaceholder += '        </div>';
        loadingPlaceholder += '        <div class="dt-thead-container flexbox-container flexbox-group">';
        loadingPlaceholder += '            <div class="flex flex-12-large">';
        loadingPlaceholder += '                <div class="table-thead-tr"><div class="table-thead-th linear-background"></div></div>';
        loadingPlaceholder += '            </div>';
        loadingPlaceholder += '        </div>';
        loadingPlaceholder += '        <div class="dt-tbody-container flexbox-container flexbox-group">';
        loadingPlaceholder += '            <div class="flex flex-12-large">';    
        
        for(var i=0; i<=3; i++) {
            loadingPlaceholder += '<div class="table-tbody-tr"><i class="dt-column-ico linear-background"></i><div class="dt-column-txt linear-background"></div></div>';
        }
        
        loadingPlaceholder += '            </div>';
        loadingPlaceholder += '        </div>';
        loadingPlaceholder += '        <div class="flexbox-container flex-align-center margin-top-15">';
        loadingPlaceholder += '            <div class="flex flex-2-large margin-right-auto">';    
        loadingPlaceholder += '                <div class="dt-pagination-length linear-background"></div>';
        loadingPlaceholder += '            </div>';
        loadingPlaceholder += '            <div class="flex flex-3-large margin-left-auto">';
        loadingPlaceholder += '                <div class="dt-pagination-info-txt linear-background"></div>';
        loadingPlaceholder += '            </div>';
        loadingPlaceholder += '        </div>';
        loadingPlaceholder += '    </div>';
        loadingPlaceholder += '</div>'

        if(processing) {
            $('.dataTables_empty').removeClass('fa-ticket');
            $('.dataTables_empty').text('');
            $(loadingPlaceholder).appendTo('.dt-content-container');
        } else {
            $('div.dt-loading-indicator').remove();
            $('div.dt-content-container').hide().fadeIn(200);
        }
    }).dataTable();
})