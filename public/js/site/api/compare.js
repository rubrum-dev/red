$(function () {
    $('#inspector-back').on('click', function () {
        $.confirm({
            icon: 'icon fa fa-question',
            title: 'Confirmar',
            content: 'Ao voltar todas as anotações serão excluídas.<br /> Deseja confirmar?',
            animation: 'scale',
            container: 'body',
            boxWidth: '768px',
            useBootstrap: false,
            offsetTop: 40,
            offsetBottom: 40,
            bootstrapClasses: {
                container: 'container',
                containerFluid: 'container-fluid',
                row: 'row',
            },
            closeIcon: true,
            closeIconClass: 'jconfirm-btn-close fa fa-times',
            escapeKey: 'cancel',
            buttons: {
                ok: {
                    text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                    btnClass: 'btn-confirm',
                    keys: ['enter', 'space'],
                    action: function () {
                        location = $('#inspector-back').attr("data-href");
                    }
                },
                cancel: {
                    text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                    btnClass: 'btn-cancel',
                    action: function () {}
                }
            },
            onOpenBefore: function () {
                //$('body').addClass('jconfirm-overlay');
                $('body').addClass('no-scroll');
                //$('.master').addClass('blurred');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'scroll'
                    }
                });
            },
            onClose: function () {
                //$('body').removeClass('jconfirm-overlay');
                $('body').removeClass('no-scroll');
                //$('.master').removeClass('blurred');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    });

    const iframe = document.getElementById('myFrame');
    
    $(iframe).hide();
    
    const handleLoad = function() {
        $('.lds-css').hide();
        $(iframe).show()
    };

    iframe.addEventListener('load', handleLoad, true);
});