window.path = window.location.origin;

$(function () {
	$(".btn-search").click(function(){
		var search = $('.text-serach').val();
		if($.trim(search) == ''){			
			alert('Digite o que deseja pesquisar!');
			$('.text-serach').val('');
		}
		else {			
			$(location).attr('href', window.path + '/site/search/' + search);
		}
	});

	$(".text-serach").keypress(function(e){
		if(e.which == 13)
			$(".btn-search").trigger('click');
	});
});
