window.path = window.location.origin;

$(function () {

	$('#list-tokens .btn-extend').on('click', function (e) {
		e.preventDefault();
        var btn = $(this).attr('id');
		window.id = $('#'+btn).data('token-id');
		$.ajax({
			type: 'get',
			url: window.path + '/admin/token/extend/' + window.id,
			context: document.body
		}).done(function(data){
            if(data.error){
                alert(data.message);
                return false;
            }
            else {
                alert(data.message);
                window.location.reload();
            }
		});
	});

	$('#list-tokens .btn-remove').on('click', function (e) {
		e.preventDefault();
		if (confirm("Deseja realmente remover esse token?") == true) {
			var btn = $(this).attr('id');
			window.id = $('#'+btn).data('token-id');
			$.ajax({
				type: 'get',
				url: window.path + '/admin/token/remove/' + window.id,
				success: function(data){
                    if(data.error){
                        alert(data.message);
                        return false;
                    }
                    else {
                        alert(data.message);
                        window.location.reload();
                    }
				}
			});
		}
	});

	$('#list-tokens .copy-link').on('click', function (e) {
		var url = $(this).data('token-url');
		$('#show-token').val(window.path + url);
	});

});
