$(function () {
    $('.site-news').on('click', function () {
        $.confirm({
            title: '',
            content: 'url:/site/news/novidades',
            closeIcon: true,
            boxWidth: '790px',
            scrollToPreviousElement: false,
            closeIconClass: 'custom-alert-close',
            escapeKey: true,
            alignMiddle: false,
            offsetTop: 0,
            offsetBottom: 0,
            buttons: {
                Entendi: {
                    text: 'Entendi',
                    keys: ['enter', 'space', 'esc'],
                    btnClass: 'custom-btn-primary'
                },
            },
            onContentReady: function () {
                $('.cr-show, .cr-hide').remove();
                $('.jconfirm-buttons').remove();
                $('#default').slide({
                    next: '#next1',
                    prev: '#prev1',
                    pagination: '#page-default',
                    effect: 'slide',
                    enableHideWidget: false
                });

                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'scroll'
                    }
                });
            }
        });
    });

    function exibir_news() {
        var checkbox = $('#dontShowAgain');

        $.ajax({
            url: '/site/news/getExibir',
            type: 'POST',
            data: {
                _token: csrf_token,
                dontShowNews: checkbox.is(':checked')
            },
            success: function (data) {
                if (checkbox.is(':checked')) {
                    console.log(data);
                } else {
                    console.log(data);
                }
            }
        });
    }
});
