window.path = window.location.origin;

$(function () {
    $("form").validate({
		rules:{
			nome:{
				required: true
			},
			sobrenome:{
				required: true
			},
			email:{
				required: true, email: true
			},
			id_adm_perfil: {
				required: true
			}
		},
		messages:{
			nome:{
				required: "Nome obrigatório"
			},
			sobrenome:{
				required: "Sobrenome obrigatório"
			},
			email:{
				required: "Email obrigatório"
			},
			id_adm_perfil:{
				required: "Selecione um perfil de acesso"
			}
		},
        submitHandler: function() {
            $('.mask-load').show();
        }
	});

    // $("#editFone").mask("(99) 9999-9999");

    $("#editFone").inputmask({
        mask: ["(99) 9999-9999", "(99) 99999-9999", ],
        keepStatic: true
    });

    $('.link-user').on('click', function(){
        $('form').validate().resetForm();
        $.each($('input[type=text]'), function () {
            $(this).val('');
        });
        $("#editEmail").prop('disabled', false);
        $('#editEmail').removeClass('disabled');
        $('select option:contains("Selecione o Perfil...")').prop('selected',true);
        $('input[name=status][value="1"]').prop('checked', true);
        $.each($('input[type=checkbox]'), function () {
            $(this).prop('checked', false);
        });
        $('#editModulo3').prop('checked', true);
        $('#cadastro-save').val('Cadastrar');
        $("#title").text("CADASTRO DE USUÁRIO");
    })

    $('.edit-user').on('click', function (e) {
		e.preventDefault();
        $('form').validate().resetForm();
        $.each($('input[type=text]'), function () {
            $(this).val('');
        });
        $("#editEmail").prop('disabled', true);
        $('#editEmail').addClass('disabled');
        $('select option:contains("Selecione o Perfil...")').prop('selected',true);
        $('input[name=status][value="1"]').prop('checked', true);
        $.each($('input[type=checkbox]'), function () {
            $(this).prop('checked', false);
        });
        $('#cadastro-save').val('Salvar');
        var btn = $(this).attr('id');
        window.id = $('#'+btn).data('id-user');
		$.ajax({
			type: 'get',
			url: window.path + '/site/manager/edit/' + window.id,
			context: document.body
		}).done(function(data){
            $('#editId').val(data.editUser.id);
            $('#editNome').val(data.editUser.nome);
            $('#editSobrenome').val(data.editUser.sobrenome);
            $('#editEmail').val(data.editUser.email);
            $('#editFone').val(data.editUser.fone);
            $('#editCargo').val(data.editUser.cargo);
			$('#editDepto').val(data.editUser.depto);
            $('#editPerfil').val(data.editUser.id_adm_perfil);
            if(data.editUser.status == 1)
                $('input[name=status][value="1"]').prop('checked', true);
	        else
                $('input[name=status][value="0"]').prop('checked', true);

            $(data.editUser.modules).each(function( index, value ) {
			  	$('#editModulo'+value.id).prop("checked", true);
			});
        });
		$("#title").text("EDIÇÃO DE USUÁRIO");
	});

    $('form').on('submit', function (e) {
		e.preventDefault();
		var resp = window.id ? 'Usuário atualizado com sucesso' : 'Usuário criado com sucesso. Um email foi enviado ao usuário com detalhes de acesso.';
		$.ajax({
			type: 'post',
			url: window.path + '/site/manager/save',
			data: $('form').serialize(),
            dataType: 'json',
			success: function (data) {
				if(data.error){
					alert(data.message);
                    $('.mask-load').hide();
                    return false;
				}
				else {
					alert(resp);
					window.location.reload();
				}
			}
		});
	});

    $('.remove-user').on('click', function (e) {
        e.preventDefault();
        var btn = $(this).attr('id');
        window.id = $('#'+btn).data('id-user');
        if (confirm("Deseja realmente remover esse usuário?") == true) {
            $.ajax({
                type: 'get',
                url: window.path + '/site/manager/remove/' + window.id,
                data: $('form').serialize(),
                success: function () {
                    alert('Usuário Removido com Sucesso');
                    window.location.reload();
                }
            });
        }
    });

    $('#cancel-user').on('click', function(){
        $(".close-modal").trigger("click");
    });

    // Habilitar Guide ao Clicar em Enxoval
    $('#editModulo4').click(function(){
    	if($('#editModulo4').prop("checked") && !$('#editModulo1').prop("checked")){
    		$('#editModulo1').prop("checked", true);
    		alert('O Módulo Guide Também Foi Habilitado.');
    	}
    });

    // Desabilitar Enxoval ao desabilitar Guide
    $('#editModulo1').click(function(){
    	if(!$('#editModulo1').prop("checked") && $('#editModulo4').prop("checked")){
    		$('#editModulo4').prop("checked", false);
    		alert('O Módulo Ads Também Foi Desabilitado.');
    	}
    });

});
