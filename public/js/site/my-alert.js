function myAlert(message, tipo, titulo) {
    switch (tipo) {
        case 'alert':
            var class2 = 'warning';
            var icon = 'fa-exclamation';
            var titulo = 'Atenção';
            break;
        case 'error':
            var class2 = 'danger';
            var icon = 'fa-exclamation';
            var titulo = 'Erro';
            break;
        case 'confirm':
            var class2 = 'confirmation';
            var icon = 'fa-question';
            var titulo = 'Confirmar';
            break;
        case 'success':
            var class2 = 'success';
            var icon = 'fa-check';
            var titulo = 'Sucesso';
            break;
        default:
            var class2 = 'warning';
            var icon = 'fa-exclamation';
            var titulo = 'Atenção';
            break;
    }

    $.alert({
        title: '',
        content: '<div class="custom-alert-contents ' + class2 + '">' +
            '   <span class="icon fa ' + icon + '"></span>' +
            '   <div class="custom-alert-heading">' +
            '       <h4>' + titulo + '</h4>' +
            '   </div>' +
            '   <p>' + message + '</p>' +
            '</div>',
        escapeKey: 'Entendi',
        buttons: {
            Entendi: {
                keys: ['esc', 'enter', 'space'],
                text: '<span><i class="icon fa fa-check"></i>Entendi</span>',
                btnClass: 'btn-confirm my-alert-confirm'
            }
        },
        closeIcon: true,
        closeIconClass: 'jconfirm-btn-close fa fa-times',
        boxWidth: '910px',
        useBootstrap: false,
        onOpenBefore: function () {
            //$('body').addClass('jconfirm-overlay');
            $('body').addClass('no-scroll');
            //$('.master').addClass('blurred');
            $('.jconfirm-content').css('marginLeft', '0px');
            //$('.ng-confirm').addClass('blurred');
            $('.jconfirm').css('overflow', 'auto');
            $('.my-alert-confirm').focus();
            $('.jconfirm').overlayScrollbars({
                scrollbars: {
                    autoHide: 'move'
                }
            });
        },
        onClose: function () {
            //$('body').removeClass('jconfirm-overlay');
            $('body').removeClass('no-scroll');
            //$('.master').removeClass('blurred');
            //$('.ng-confirm').removeClass('blurred');
            $('.jconfirm').css('overflow', 'hidden');
            $('.my-alert-confirm').blur();

            if ($('.ng-confirm').length) {
                //$('.master').addClass('blurred');
            }
        }
    });
}

function excluir(message, url) {
    $.confirm({
        title: '',
        content: '<div class="custom-alert-contents confirmation">' +
            '   <span class="icon fa fa-question"></span>' +
            '   <div class="custom-alert-heading">' +
            '       <h4>Confirmar</h4>' +
            '   </div>' +
            '   <p>' + message + '</p>' +
            '</div>',
        escapeKey: 'Cancelar',
        buttons: {
            Confirmar: {
                text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                btnClass: 'btn-confirm my-alert-confirm',
                action: function (Confirmar) {
                    window.location = url;
                }
            },
            Cancelar: {
                text: '<span><i class="icon fa fa-times"></i>Cancelar</span>',
                btnClass: 'btn-cancel'
            }
        },
        closeIcon: true,
        closeIconClass: 'jconfirm-btn-close fa fa-times',
        boxWidth: '910px',
        useBootstrap: false,
        onOpenBefore: function () {
            //$('body').addClass('jconfirm-overlay');
            $('body').addClass('no-scroll');
            $('.jconfirm-content').css('marginLeft', '0px');
            //$('.master').addClass('blurred');
            $('.jconfirm').css('overflow', 'auto');
            $('.my-alert-confirm').focus();
            $('.jconfirm').overlayScrollbars({
                scrollbars: {
                    autoHide: 'move'
                }
            });
        },
        onClose: function () {
            //$('body').removeClass('jconfirm-overlay');
            $('body').removeClass('no-scroll');
            //$('.master').removeClass('blurred');
            $('.jconfirm').css('overflow', 'hidden');
            $('.my-alert-confirm').blur();
        }
    });
}

function addNewItem(message) {
    $.confirm({
        title: '',
        content: '<div class="custom-alert-contents warning">' +
            '   <span class="icon fa fa-exclamation"></span>' +
            '   <div class="custom-alert-heading">' +
            '       <h4>Atenção</h4>' +
            '   </div>' +
            '   <p>' + message + '</p>' +
            '</div>',
        escapeKey: 'Confirmar',
        buttons: {
            Confirmar: {
                keys: ['enter', 'space', 'esc'],
                text: '<span><i class="icon fa fa-check"></i>Confirmar</span>',
                btnClass: 'btn-confirm my-alert-confirm'
            }
        },
        closeIcon: true,
        closeIconClass: 'jconfirm-btn-close fa fa-times',
        boxWidth: '910px',
        useBootstrap: false,
        onOpenBefore: function () {
            //$('body').addClass('jconfirm-overlay');
            $('body').addClass('no-scroll');
            $('.jconfirm-content').css('marginLeft', '0px');
            //$('.master').addClass('blurred');
            $('.jconfirm').css('overflow', 'auto');
            $('.my-alert-confirm').focus();
            $('.jconfirm').overlayScrollbars({
                scrollbars: {
                    autoHide: 'move'
                }
            });
        },
        onClose: function () {
            //$('body').removeClass('jconfirm-overlay');
            $('body').removeClass('no-scroll');
            //$('.master').removeClass('blurred');
            $('.jconfirm').css('overflow', 'hidden');
            $('.my-alert-confirm').blur();
        }
    });
}