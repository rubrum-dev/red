window.path = window.location.origin;

$(function () {
	var family = $('#family').val();
	var product = $('#product').val();
	var guide = $('#guide').val();

	$('#' + guide).css('background-color', '#064B96');
	$('#' + guide).css('color', '#FFFFFF');

	$('#imgSkuTitle').click(function () {
		$.ajax({
			type: 'get',
			url: window.path + '/site/' + family + '/produtos/' + product + '/favorite',
			context: document.body
		}).done(function (data) {
			checkFavorite(data);
		});
	});

	$('#share-link').on('click', function (e) {
		var token = $('#share-link').data('url-token');
		$.ajax({
			type: 'get',
			url: window.path + '/site/token/create/' + $('#share-link').data('id-prod'),
			context: document.body
		}).done(function (data) {
			$('#show-token').val(token + '?token=' + data);
			return false;
		});
	});

	$('.download-archive > a').click(function (e) {
		setTimeout(removeLoader, 500);
	});
});

function shareUrlTemp() {
	$('.share-hidden').animate({
		opacity: 'toggle'
	}, 200);
}

function checkFavorite(data) {
	if (data.favorite) {
		//$('#imgSkuTitle').attr("src", window.path + '/images/icons/frontend/favorite-set-yes.png');
		myAlert(data.prod + ' ' + 'Adicionado aos Favoritos', 'success');
	} else if(!data.favorite) {
		//$('#imgSkuTitle').attr("src", window.path + '/images/icons/frontend/favorite-set-no.png');
		myAlert(data.prod + ' ' + 'Removido dos Favoritos', 'success');
	}
	location.reload();
}