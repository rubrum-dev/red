$(function () {
    $('.table').on('processing.dt', function (e, settings, processing) {
        var loadingPlaceholder = '';

        loadingPlaceholder += '<div class="content-placeholder-container">';
        loadingPlaceholder += '    <div class="dt-loading-indicator">';
        loadingPlaceholder += '        <div class="flexbox-container margin-bottom-20">';
        loadingPlaceholder += '            <div class="flex flex-3-large">';
        loadingPlaceholder += '                <div class="linear-background dt-filter-search"></div>';
        loadingPlaceholder += '            </div>';
        loadingPlaceholder += '            <div class="btn-add-new flex margin-left-auto">';
        loadingPlaceholder += '                <div class="btn-function no-hover"><i class="icon linear-background"></i><span class="text linear-background"></span></div>';
        loadingPlaceholder += '            </div>';
        loadingPlaceholder += '        </div>';
        loadingPlaceholder += '        <div class="dt-thead-container flexbox-container flexbox-group">';
        loadingPlaceholder += '            <div class="flex flex-12-large">';
        loadingPlaceholder += '                <div class="table-thead-tr"><div class="table-thead-th linear-background"></div></div>';
        loadingPlaceholder += '            </div>';
        loadingPlaceholder += '        </div>';
        loadingPlaceholder += '        <div class="dt-tbody-container flexbox-container flexbox-group">';
        loadingPlaceholder += '            <div class="flex flex-12-large">';

        for (var i = 0; i <= 6; i++) {
            loadingPlaceholder += '<div class="table-tbody-tr"><i class="dt-column-ico margin-left-10 linear-background"></i><div class="dt-column-txt linear-background"></div></div>';
        }

        loadingPlaceholder += '            </div>';
        loadingPlaceholder += '        </div>';
        loadingPlaceholder += '        <div class="flexbox-container flex-align-center margin-top-15">';
        loadingPlaceholder += '            <div class="flex flex-2-large margin-right-auto">';
        loadingPlaceholder += '                <div class="dt-pagination-length linear-background"></div>';
        loadingPlaceholder += '            </div>';
        loadingPlaceholder += '            <div class="flex flex-3-large margin-left-auto">';
        loadingPlaceholder += '                <div class="dt-pagination-info-txt linear-background"></div>';
        loadingPlaceholder += '            </div>';
        loadingPlaceholder += '        </div>';
        loadingPlaceholder += '    </div>';
        loadingPlaceholder += '</div>';

        if (processing) {
            $(loadingPlaceholder).appendTo('.dt-content-container');
        } else {
            setTimeout(function () {
                $('div.dt-loading-indicator').remove();
                $('div.dt-content-container').hide().fadeIn(200);
                $('.form-group-add').show();
                $('.dataTable-head').show();
            }, new Date().getMilliseconds());
        }
    }).dataTable();
});