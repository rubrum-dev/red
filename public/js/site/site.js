$(function () {
    function determineDropDownPosition() {
        // require menu height + margin, otherwise convert to drop-up
        var dropUpMarginBottom = 0;
        var dropUp = function () {
            var windowHeight = $(window).height();
            
            $('.dropdown').each(function () {
                var rect = this.getBoundingClientRect();

                // only toggle menu's that are visible on the current page
                if (rect.top > windowHeight) {
                    return;
                }

                // if you know height of menu - set on parent, eg. `data-menu="100"`
                var dropDownMenuHeight = $(this).data('menu');
            
                if (dropDownMenuHeight == null) {
                    var dataMenu = $(this).children('.dropdown-list-auto').innerHeight()
                    dropDownMenuHeight = dataMenu;
                    $(this).attr('data-menu', dataMenu);
                }

                $(this).toggleClass('dropup', ((windowHeight - rect.bottom) < (dropDownMenuHeight + dropUpMarginBottom)) && (rect.top > dropDownMenuHeight));
            });
        }

        $(window).bind('load click resize', dropUp);
    }

    $('body, .collapsible, .os-scrollable-textbox').overlayScrollbars({
        scrollbars: {
            autoHide: 'move'
        },
        callbacks: {
            onScroll: function (eventArgs) {
                var x = -eventArgs.target.scrollLeft,
                    el = $('.box-package-info'),
                    y = eventArgs.target.scrollTop;

                $('.header').css({
                    left: x
                });

                if (y < 90) {
                    el.removeClass('box-package-info-sticky');

                    el.stop().animate({
                        'top': 0
                    }, 0);
                } else {
                    el.stop().animate({
                        'top': 90 + y - 150
                    }, 0);

                    el.addClass('box-package-info-sticky');
                }
            }
        }
    });
    
    /*$('.brand-list .listing li > .box-image > .box-image-thumb-redim img, .product-list .listing li > .box-image > .box-image-thumb-redim img').each(function () {
        var needsUpdate = true;
    
        $(this).on('error', function () {
            if(needsUpdate) {
                $(this).css('visibility', 'hidden');
                $(this).parent('.box-image-thumb-redim').append('<i class="no-image-placeholder fa fa-picture-o"></i>');
            }
        });

    });*/
    
    $('.no-space').on("input", function () {
        $(this).val($(this).val().replace(/ /g, ""));
    });

    $('.input-email-only').on('input', function () {
        $(this).val($(this).val().replace(/ /g, ""));
        $(this).val($(this).val().toLowerCase());    
    });

    $(document).click('.paused-at-btn', function (e) {
        var target = e.target;

        $('.event-dropdown-list').each(function () {
            var $this = $(this),
                dropdown = $this.parents('.event').find('.paused-at-btn');

            if (dropdown[0] === target) {
                $(this).toggle();
            } else {
                $(this).hide();
            }

            if ($(this).is(':visible')) {
                $(this).parents('.event').find('.paused-at-btn').addClass('active');
            } else {
                $(this).parents('.event').find('.paused-at-btn').removeClass('active');
            }
        });
    });

    $('.add-instruction-group select').on('change', textBoxAutoResize);

    $(document).on('click', '.submenu-gerenciar-embalagens', function (event) {
        event.stopPropagation();
        
        $.dialog({
            title: '',
            content: '<h4 class="submenu-title">Gerenciar Embalagens</h4>' +
            '<ul>' + 
                '<li><a href="/admin/sku/embalagens"><i class="icon fa fa-cube"></i>Embalagens</a></li>' +
                '<li><a href="/admin/itens-embalagem"><i class="icon fa fa-files-o"></i>Itens de Embalagem</a></li>' +
                '<li><a href="/admin/dimensions"><i class="icon fa fa-flask"></i>Volumes</a></li>' +
                '<li><a href="/admin/package"><i class="icon fa fa-cubes"></i>Tipos de Embalagens</a></li>' +
                '<li><a href="/admin/product"><i class="icon fa fa-diamond"></i>Produtos</a></li>' +
                '<li><a href="/admin/family"><i class="icon fa fa-certificate"></i>Marcas</a></li>' +
                '<li><a href="/admin/product-category"><i class="icon fa fa-tags"></i>Categorias</a></li>' +
                //'<li><a href="/admin/tipos-instrucao"><i class="icon fa fa-tasks"></i>Tipos de Instrução</a></li>' +
                '<li><a href="/admin/grupos-trabalho"><i class="icon fa fa-handshake-o"></i>Grupos de Trabalho</a></li>' +
            '</ul>',
            animation: 'scale',
            closeAnimation: 'scale',
            animateFromElement: true,
            backgroundDismiss: true,
            closeIcon: false,
            boxWidth: '380px',
            useBootstrap: false,
            offsetTop: 0,
            offsetBottom: 0,
            onContentReady: function () {
                // bind to events
                var jc = this;
                
                $(document).on('click', '.jconfirm-ctl-submenu a', function() {
                    $('.wrapped').removeClass('blurred');
                    $('.header').find('.menu-btn').removeClass('open');
                    $('.header').find('.collapsible').removeClass('collapsed');
                    $('.user-btn').find('.icon').removeClass('fa-times').addClass('fa-user-circle');

                    $('.header').find('.collapsible').animate({
                        top: 'hide',
                        height: 'hide',
                        marginTop: 'hide',
                        opacity: 'hide'
                    }, 150);

                    jc.close();
                });

                $(document).on('click', '.jconfirm', function(event) {
                    //jc.close();
                    return false;
                });
                
            },
            onOpenBefore: function () {
                $('.jconfirm .jconfirm-cell').addClass('on-top');
                $('.jconfirm .jconfirm-box').addClass('jconfirm-ctl-submenu');
                $('body').addClass('no-scroll');
                $('.jconfirm-content').css('marginLeft', '0px');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    });

    $(document).on('click', '.submenu-gerenciar-midias', function (event) {
        event.stopPropagation();
        
        $.dialog({
            title: '',
            content: '<h4 class="submenu-title">Gerenciar Mídias</h4>' + 
            '<ul>' + 
                '<li><a href="/site/adsmart/tipos-midia"><i class="icon fa fa-newspaper-o"></i>Tipos de Mídia</a></li>' +
                '<li><a href="/site/adsmart/aprovadores-midia"><i class="icon fa fa-thumbs-up"></i>Aprovadores de Mídia</a></li>' +
                '<li><a href="/site/adsmart/condicoes-midia"><i class="icon fa fa-quote-right"></i>Critérios de Avaliação</a></li>' +
            '</ul>',
            animation: 'scale',
            closeAnimation: 'scale',
            animateFromElement: true,
            backgroundDismiss: true,
            closeIcon: false,
            boxWidth: '380px',
            useBootstrap: false,
            offsetTop: 0,
            offsetBottom: 0,
            onContentReady: function () {
                // bind to events
                var jc = this;
                
                $(document).on('click', '.jconfirm-ctl-submenu a', function() {
                    $('.wrapped').removeClass('blurred');
                    $('.header').find('.menu-btn').removeClass('open');
                    $('.header').find('.collapsible').removeClass('collapsed');
                    $('.user-btn').find('.icon').removeClass('fa-times').addClass('fa-user-circle');

                    $('.header').find('.collapsible').animate({
                        top: 'hide',
                        height: 'hide',
                        marginTop: 'hide',
                        opacity: 'hide'
                    }, 150);

                    jc.close();
                });

                $(document).on('click', '.jconfirm', function(event) {
                    //jc.close();
                    return false;
                });
            },
            onOpenBefore: function () {
                $('.jconfirm .jconfirm-cell').addClass('on-top');
                $('.jconfirm .jconfirm-box').addClass('jconfirm-ctl-submenu');
                $('body').addClass('no-scroll');
                $('.jconfirm-content').css('marginLeft', '0px');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    });

    $(document).on('click', '.submenu-gerenciar-usuarios', function (event) {
        event.stopPropagation();
        
        $.dialog({
            title: '',
            content: '<h4 class="submenu-title">Gerenciar Usuários</h4>' + 
            '<ul>' + 
                '<li><a href="/site/usuarios/v2"><i class="icon fa fa-user-circle"></i>Usuários</a></li>' +
                '<li><a href="/admin/profile"><i class="icon fa fa-low-vision"></i>Perfis de Acesso</a></li>' +
                '<li><a href="/admin/company"><i class="icon fa fa-building"></i>Empresas</a></li>' +
                '<li><a href="/admin/departamentos"><i class="icon fa fa-id-badge"></i>Departamentos</a></li>' +
                '<li><a href="/admin/cargos"><i class="icon fa fa-id-card"></i>Cargos</a></li>' +
            '</ul>',
            animation: 'scale',
            closeAnimation: 'scale',
            animateFromElement: true,
            backgroundDismiss: true,
            closeIcon: false,
            boxWidth: '380px',
            useBootstrap: false,
            offsetTop: 0,
            offsetBottom: 0,
            onContentReady: function () {
                // bind to events
                var jc = this;
                
                $(document).on('click', '.jconfirm-ctl-submenu a', function() {
                    $('.wrapped').removeClass('blurred');
                    $('.header').find('.menu-btn').removeClass('open');
                    $('.header').find('.collapsible').removeClass('collapsed');
                    $('.user-btn').find('.icon').removeClass('fa-times').addClass('fa-user-circle');

                    $('.header').find('.collapsible').animate({
                        top: 'hide',
                        height: 'hide',
                        marginTop: 'hide',
                        opacity: 'hide'
                    }, 150);

                    jc.close();
                });

                $(document).on('click', '.jconfirm', function(event) {
                    //jc.close();
                    return false;
                });
            },
            onOpenBefore: function () {
                $('.jconfirm .jconfirm-cell').addClass('on-top');
                $('.jconfirm .jconfirm-box').addClass('jconfirm-ctl-submenu');
                $('body').addClass('no-scroll');
                $('.jconfirm-content').css('marginLeft', '0px');
                $('.jconfirm').css('overflow', 'auto');
                $('.jconfirm').overlayScrollbars({
                    scrollbars: {
                        autoHide: 'move'
                    }
                });
            },
            onClose: function () {
                $('body').removeClass('no-scroll');
                $('.jconfirm').css('overflow', 'hidden');
            }
        });
    });

    function textBoxAutoResize() {
        $(document).on('load change input keyup keydown paste cut', '.textbox-auto-resizable', function () {
            $(this).outerHeight(38).outerHeight(this.scrollHeight);
        }).find('.textbox-auto-resizable').change();
    }

    function customFileInputCtrl() {
        $('.files-control .files-control-fake').find('strong').text('Anexar arquivo com no máx. 100 MB');
        $('.files-control.attachPDF .files-control-fake').find('strong').text('Anexar arquivo PDF com no máx. 50 MB');

        $('.files-control input[type="file"]').on('click', function () {
            $(this).each(function () {
                $(this).val('');
                $(this).next('.files-control-fake strong').text('Anexar arquivo com no máx. 100 MB');
            });
        });

        $('.files-control.attachPDF input[type="file"]').on('click', function () {
            $(this).each(function () {
                $(this).val('');
                $(this).next('.files-control-fake strong').text('Anexar arquivo PDF com no máx. 50 MB');
            });
        });

        $('.files-control input[type="file"]').on('change', function () {
            $(this).each(function () {
                var filePlaceholder = $(this).val().split('\\').pop();

                $(this).next('.files-control-fake').find('strong').text(filePlaceholder);

                if (!filePlaceholder) {
                    $(this).val('');
                    $(this).next('.files-control-fake').find('strong').text('Anexar arquivo com no máx. 100 MB');
                }
            });
        });

        $('.files-control.attachPDF input[type="file"]').on('change', function () {
            $(this).each(function () {
                var filePlaceholder = $(this).val().split('\\').pop();

                $(this).next('.files-control-fake').find('strong').text(filePlaceholder);

                if (!filePlaceholder) {
                    $(this).val('');
                    $(this).next('.files-control-fake').find('strong').text('Anexar arquivo PDF com no máx. 50 MB');
                }
            });
        });
    }

    function customMultiFileInputCtrl() {
        $('.files-control-multi .files-control-fake').find('strong').text('Anexar arquivo com no máx. 100 MB');

        $('.files-control-multi input[type="file"]').on('click', function () {
            $(this).each(function () {
                $(this).val('');
                $(this).next('.files-control-fake strong').text('Anexar arquivo com no máx. 100 MB');
            });
        });

        $('.files-control-multi input[type="file"]').on('change', function () {
            $(this).each(function () {
                if ($('.files-control-multi input[type="file"]').val()) {
                    var names = [];

                    for (var i = 0; i < $(this).get(0).files.length; ++i) {
                        names.push($(this).get(0).files[i].name);
                    }

                    $('.files-control-fake').find('strong').text(names.join(', '));
                } else if (!$('.files-control-multi input[type="file"]').val()) {
                    $('body').find('input[type="file"]').val('');
                    $('body').find('.files-control-fake strong').text('Anexar arquivo com no máx. 100 MB');
                }
            });
        });
    }

    function scrollableMenu() {
        $('.menu-nav > .user-menu-body > ul').children('li').on('mouseover', function () {
            var $menuItem = $(this),
                $submenuWrapper = $('> ul', $menuItem);

            var menuItemPos = $menuItem.position();

            $submenuWrapper.css({
                top: menuItemPos.top,
                left: menuItemPos.left + Math.round($menuItem.outerWidth())
            });
        });
    }

    function fileExtValidate() {
        $(document).on('change', '.valid-file-ext', function (e) {
            var val = $('.valid-file-ext').val().toLowerCase(),
                regex = new RegExp("(.*?)\.(docx|doc|pdf|xml|png|jpg|jpeg|bmp|gif|tif|tiff|psd|eps|ai|svg|ppt|pptx|xls|xlsx|zip|rar|7z|txt|dxf|mp3|wav|mp4|mpeg|avi|mov)$");

            if ($(this).val() !== '' && !(regex.test(val))) {
                $(this).val('');
                $(this).siblings('.files-control-fake').find('strong').text('Anexar arquivo com no máx. 100 MB');
                $(this).siblings('.files-control-fake').addClass('has-error');

                myAlert('Formato de arquivo inválido.', 'warning');
            }

            if ($(this).val() === '') {
                $(this).val('');
                $(this).siblings('.files-control-fake').find('strong').text('Anexar arquivo com no máx. 100 MB');

            }
        });
    }


    function selectFocus() {
        $(document).on('focus', '.select-control > select', function () {
            $(this).parent('.select-control').addClass('focus');
        });

        $(document).on('blur', '.select-control > select', function () {
            $(this).parent('.select-control').removeClass('focus');
        });
    }

    function dataTableFilterFocus() {
        $(document).on('focus', '.datatable-filter-container input, .dataTable-searchbox-container input', function () {
            $(this).parent('label').addClass('focus');
        });

        $(document).on('blur', '.datatable-filter-container input, .dataTable-searchbox-container input', function () {
            $(this).parent('label').removeClass('focus');
        });
    }

    function dataTableLengthFocus() {
        $(document).on('focus', '.dataTables_length select', function () {
            $(this).parents('.select-control, label').addClass('focus');
        });

        $(document).on('blur', '.dataTables_length select', function () {
            $(this).parents('.select-control, label').removeClass('focus');
        });
    }

    textBoxAutoResize();
    customFileInputCtrl();
    customMultiFileInputCtrl();
    fileExtValidate();
    scrollableMenu();
    dataTableFilterFocus();
    dataTableLengthFocus();
    selectFocus();
    determineDropDownPosition();
});

var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
var start_time = new Date().getTime();
    
// Start loading
function startLoader() {
    if (!isSafari) {
        $(window).bind('beforeunload', function () {
            $('.lds-css').show();
        });

        $(window).bind('unload', function () {
            setTimeout(removeLoader, new Date().getTime() - start_time);
        });
    } else {
        return;
    }
}

// Remove loading
function removeLoader() {
    $('.lds-css').hide(function () {
        $('.lds-css').remove();
    });
}

function dropdownBtn() {
    $(document).on('click', 'a.dropdown-btn', function (e) {
        var target = e.target;

        $('.dropdown-menu').each(function () {
            var $this = $(this);
            var dropdown = $this.prev('a.dropdown-btn');

            if (dropdown[0] == target) {
                $(this).toggle(200);
                $(dropdown).toggleClass('dropdown-active');
            } else {
                $(this).hide();
                $(dropdown).removeClass('dropdown-active');
            }
        });
    });

    $(document).on('click', 'body', function (event) {
        if (!$(event.target).closest('.dropdown').length) {
            $('a.dropdown-btn').removeClass('dropdown-active');
            $('a.dropdown-btn').parent('.dropdown').find('.dropdown-menu').hide();
        }
    });
}

function dropdownMenu() {
    $(document).on('click', '.dropdown-toggle', function (e) {
        var target = e.target;

        $('.dropdown-list').each(function () {
            var $this = $(this),
                dropdown = $this.parents('.dropdown, .dropdown-user-acount').find('.dropdown-toggle');
            
            if (dropdown[0] === target) {
                $(this).toggle();
                $(this).parents('.dropdown').find('.dropdown-list-sub').hide();
            } else {
                $(this).hide();
            }

            if ($(this).is(':visible')) {
                $(this).parents('.dropdown, .dropdown-user-acount').find('.dropdown-toggle').addClass('active');
            } else {
                $(this).parents('.dropdown, .dropdown-user-acount').find('.dropdown-toggle').removeClass('active');
            }
        });
    });

    $(document).on('click', '.dropdown ul li a, .dropdown-user-acount ul li a', function () {
        $(this).parents('.dropdown, .dropdown-user-acount').find('.dropdown-list').hide();
        $(this).parents('.dropdown, .dropdown-user-acount').find('.dropdown-toggle').removeClass('active');
    });

    $(document).on('click', '.dropdown-list-sub ul li a', function () {
        $(this).parents('.dropdown').find('.dropdown-list').hide();
        $(this).parents('.dropdown').find('.dropdown-list-sub').hide();
    });

    $(document).on('click', '.dropdown-toggle-sub', function (e) {
        $(this).parents('.dropdown-list').show();
        $(this).parents('.dropdown-list').find('.dropdown-list-sub').toggle();
        $(this).parents('.dropdown').find('.dropdown-toggle').addClass('active');
    });

    $(document).on('click', 'body', function (event) {
        if (!$(event.target).closest('.dropdown, .dropdown-user-acount').length) {
            $('.dropdown-list').parents('.dropdown, .dropdown-user-acount').find('.dropdown-list').hide();
            $('.dropdown-list').find('.dropdown-list-sub').hide();
            $('.dropdown-list').parents('.dropdown, .dropdown-user-acount').find('.dropdown-toggle').removeClass('active');
        }
    });

    $(window).on('keyup', function (event) {
        if (event.keyCode == 27) {
            $('.dropdown-toggle').removeClass('active');
            $('.dropdown-toggle').parents('.dropdown').find('.dropdown-list').hide();
        }
    });
}

function fixedNavbar() {
    $(document).on('click', '.header .menu-btn', function (event) {
        var collapse = $(this).parents('.header').find('.collapsible');

        $(collapse).animate({
            top: 'toggle',
            height: 'toggle',
            marginTop: 'toggle',
            opacity: 'toggle'
        }, 150);

        $(collapse).toggleClass('collapsed');

        if ($(collapse).hasClass('collapsed')) {
            $('.menu-btn').addClass('open');
            $('.user-btn').find('.icon').removeClass('fa-times').addClass('fa-user-circle');
            $('.header').find('.user-account-nav').hide();
            $('.wrapped').addClass('blurred');
            $('body').addClass('no-scroll');
        } else {
            $('.menu-btn').removeClass('open');
            $('.wrapped').removeClass('blurred');
            $('body').removeClass('no-scroll');
        }

        event.stopPropagation();
    });

    $(document).on('click', '.user-btn', function (event) {
        $(this).parents('.header').find('.user-account-nav').slideToggle(150, function () {
            if ($('.user-account-nav').is(':visible')) {
                $('.header').find('.collapsible').removeClass('collapsed');
                $('.user-btn').find('.icon').removeClass('fa-user-circle').addClass('fa-times');
                $('.menu-btn').removeClass('open');
                $('.wrapped').addClass('blurred');
                $('body').removeClass('no-scroll');

                $('.header').find('.collapsible').animate({
                    marginTop: 'hide',
                    opacity: 'hide'
                }, 150);
            } else {
                $('.user-btn').find('.icon').removeClass('fa-times').addClass('fa-user-circle');
                $('.wrapped').removeClass('blurred');
            }
        });

        event.stopPropagation();
    });

    $(document).click(function () {
        if ($('.header').find('.collapsible').is(':visible')) {
            $('body').removeClass('no-scroll');
        }

        $('.wrapped').removeClass('blurred');
        $('.header').find('.menu-btn').removeClass('open');
        $('.header').find('.collapsible').removeClass('collapsed');
        $('.user-btn').find('.icon').removeClass('fa-times').addClass('fa-user-circle');

        $('.header').find('.collapsible').animate({
            top: 'hide',
            height: 'hide',
            marginTop: 'hide',
            opacity: 'hide'
        }, 150);

        $('.header').find('.user-account-nav').animate({
            top: 'hide',
            height: 'hide',
            marginTop: 'hide',
            opacity: 'hide'
        }, 150);
    });

    $(document).on('click', '.jconfirm-ctl-submenu', function (event) {
        event.stopPropagation();
    });
}

//var noImgPlaceholderDOM = '<i class="no-image-placeholder fa fa-picture-o"></i>';
//var stopPropagating = false;
    
/*function fixShowCasePanelBrokenImg() {
    $('.showcase-panel .showcase-left').find('img').css('display', 'none');
    $('.showcase-panel .showcase-left').append(noImgPlaceholderDOM);
}

function fixAdminPackShelfBrokenImg() {
    $('img').css('display', 'none');
    $('.box-product-misuse > span').append(noImgPlaceholderDOM);
}

function fixPackShelfProductBrokenImg() {
    if (!stopPropagating) {
        stopPropagating = true;
        
        $('.package-list .listing li > .box-image > .sku-thumb').css('display', 'none');
        $('.package-list .listing li > .box-image').append(noImgPlaceholderDOM);
        $('.package-list .listing li > .box-image').addClass('no-animate');
    }
}

function fixPackShelfFinalArtBrokenImg() {
    $('#form-packshelf-imgs .box-product-misuse > span > img').css('display', 'none');
    $('#form-packshelf-imgs .box-product-misuse > span').append(noImgPlaceholderDOM);
}

function fixModalSkuBrokenImg() {
    $('.modal-sku .imagens-sku-down .unitaria-img-sku .unitaria-img-sku-interno > a .modal-sku-img-redim').css('display', 'none');
    $('.modal-sku .imagens-sku-down .unitaria-img-sku .unitaria-img-sku-interno > a').append(noImgPlaceholderDOM);
}

function fixModalPackShelfBrokenImg() {
    angular.element('.packshelf-sku-listing .flex-img-sku > a > figure').find('img').css('display', 'none');
    angular.element('.packshelf-sku-listing .flex-img-sku > a > figure').append(noImgPlaceholderDOM);    
}*/

fixedNavbar();
startLoader();
dropdownBtn();
dropdownMenu();