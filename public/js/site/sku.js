window.path = window.location.origin;
var now = new Date;

$(function () {
	if (GetURLParameter('sku')) {
		var sPageURL = window.location.pathname;
		var sURLVariables = sPageURL.split('/');
		var family = sURLVariables[2];
		var product = sURLVariables[4];
		
		window.id = GetURLParameter('sku');
		
		$('.sku-modal').trigger('click');
		
		clickSkuModal();
	} else {
		var family = $('#family').val();
		var product = $('#product').val();
	}

	$('.sku-modal').click(function () {
		window.id = $(this).data('id-sku');
		clickSkuModal();
	});
	
	$('#imgSkuTitle').click(function () {
		$.ajax({
			type: 'get',
			url: window.path + '/site/' + family + '/produtos/' + product + '/favorite',
			context: document.body
		}).done(function (data) {
			checkFavorite(data);
		});
	});

	$('#show-hide-itens-expirados').click(function () {
		if ($('.hide-show').is(':visible')) {
			$('.hide-show').hide();
			$('#text-show-hide-expired').text('Mostrar Itens Expirados');
		} else {
			$('.hide-show').show();
			$('#text-show-hide-expired').text('Ocultar Itens Expirados');
		}
	});

	$('.favorite').each(function () {
		var el = '<span></span>';

		$(this).click(function (e) {
			if (!$(this).parents('.top-inline').find('a:first').hasClass('favorite-set-yes')) {
				$(this).parents('.top-inline').find('a:first').addClass('favorite-set-yes');
				$(this).parents('.top-inline').find('a:first').text('Favorito');
				$(this).parents('.top-inline').find('a:first').prepend($(el).addClass('icon icon-star'));
			} else {
				$(this).parents('.top-inline').find('a:first').removeClass('favorite-set-yes');
				$(this).parents('.top-inline').find('a:first').text('Adicionar Favorito');
				$(this).parents('.top-inline').find('a:first').prepend($(el).addClass('icon icon-star'));
			}
		});
	});

	$('.top-packing-logo .logo-down, .top-nav-right .download > a').click(function (e) {
		setTimeout(removeLoader, 500);
	});
});

function GetURLParameter(sParam) {
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	
	for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) {
			return sParameterName[1];
		}
	}
}

function clickSkuModal() {
	$('.modal-sku-title h1 span').empty();
	$('.artes-finais').empty();
	$('.imagens-sku-down').empty();

	$.dialog({
		title: '',
		content: '',
		animateFromElement: false,
		backgroundDismiss: true,
		closeIcon: true,
		closeIconClass: 'jconfirm-btn-close icon-cross',
		boxWidth: '850px',
		useBootstrap: false,
		onContentReady: function () {
			var self = this;

			return $.ajax({
				type: 'get',
				url: window.path + '/site/' + family + '/produtos/' + product + '/sku/modal/' + window.id,
				context: document.body
			}).done(function (data) {
				var modalHTML = '<div id="sku" class="modal-sku">' +
					'	<div class="modal-sku-title clearfix">' +
					'		<h1><span></span></h1>' +
					'	</div>' +
					'	<div class="modal-sku-hide">' +
					'		@if(!Auth::guest())' +
					'			<div id="check-zip-pdf">' +
					'				@if(in_array($user->id_adm_perfil, [1,2]) || (in_array($produto->id_familia, $user->familias) && in_array(2, $user->modulos)))' +
					'					<h4>Artes-Finais</h4>' +
					'					<div class="artes-finais"></div>' +
					'				@endif' +
					'				<br />' +
					'			</div>' +
					'		@endif' +
					'		<div class="imagens-sku-down clearfix"></div>' +
					'	</div>' +
					'</div>';

				self.setContentAppend(modalHTML);

				$('.modal-sku-title h1 span').append((data.modalSku.nome).toUpperCase());
				$('.modal-sku-title .modal-sku-img-redim').attr("src", window.path + data.modalSku.dimensoes.thumb);

				if (data.modalSku.artesFinais.length == 0)
					$('.modal-sku-hide').hide();
				else
					$('.modal-sku-hide').show();

				$(data.modalSku.artesFinais).each(function (index, value) {
					if (value.arte_exibicao == 1) {
						$('.modal-sku-title .modal-sku-img-redim').attr("src", window.path + value.thumb);
					}
					getFinalArts(value, data.user);
					getImagesSku(value, data.user);
				});
				if (!data.modalSku.zip_pdf)
					$('#check-zip-pdf').hide();
				else
					$('#check-zip-pdf').show();
			}).fail(function () {
				myAlert('Ocorreu um erro.', 'error');
				return false;
			});
		},
		onOpenBefore: function () {
			$('.jconfirm-content').css('marginLeft', '0px');
			$('.jconfirm-box').addClass('jconfirm-box-alt');
		},
		onOpen: function () {
			$('body').addClass('overlay');
			$('body').addClass('no-scroll');
			$('.master').addClass('blurred');
			$('.jconfirm').css('overflow', 'auto');
		},
		onClose: function () {
			$('body').removeClass('overlay');
			$('body').removeClass('no-scroll');
			$('.master').removeClass('blurred');
			$('.jconfirm').css('overflow', 'hidden');
		}
	});
}

function checkFavoriteSuccess(message) {
	if ($.alert) {
		$.alert({
			title: '',
			content: '<div class="alert-box-content confirmation clearfix">' +
			'   <span class="icon fas fa-check"></span>' +
			'   <div class="alert-box-body">' +
			'       <div class="heading">' +
			'           <h4>Confirmação</h4>' +
			'       </div>' +    
			'       <p class="text">' + message + '</p>' +
			'   </div>' +
			'</div>',
			boxWidth: '855px',
        	closeIcon: true,
			closeIconClass: 'custom-alert-close',
			buttons: {
				ok: {
					text: '<span><i class="icon icon-check"></i>Entendi</span>',
					btnClass: 'btn-confirm',
					keys: ['enter', 'esc', 'space'],
					action: function () {
						location.reload();
					}
				}
			},
			onOpenBefore: function () {},
			onOpen: function () {},
			onClose: function () {
				location.reload();
			}
		});
	} else {
		alert(message);
	}
}

function checkFavorite(data) {
	if (data.favorite) {
		checkFavoriteSuccess(data.prod + ' adicionado aos Favoritos', 'success');
	} else {
		checkFavoriteSuccess(data.prod + ' removido dos Favoritos', 'success');
	}
}

function getFinalArts(data, user) {
	var val = '';

	$(data).each(function (index, value) {
		if (value.link_pdf != null) {
			// Verifica se é usuário Admin. Caso não seja, verifica se o usuário tem permissão no Módulo SKU (id_modulo = 3)
			if (user && (($.inArray(user.id_adm_perfil, [1, 2]) !== -1) || (($.inArray(3, user.modulos) !== -1) &&
					($.inArray(parseInt(family.value, 10), user.familias) !== -1)))) {
				val += '<div class="modal-sku-artes-finais">';
				if (value.nome_arte)
					val += '<p>' + value.nome_arte + '<br /><small>Adicionado em ' + value.data + ' às ' + value.hora + '</small></p>';
				else
					val += '<p>' + value.sku_nome + '<br /><small>Adicionado em ' + value.data + ' às ' + value.hora + '</small></p>';
				val += '<span>';
				if (value.link_pdf != null) {
					if (value.link_pdf != '') {
						val += '<a href="' + value.link_pdf + '" target="_blank">';
						val += '	<img src="' + window.path + '/images/icons/frontend/short-down.png" alt="down"> PDF';
						val += '</a>';
					}
				}
				val += '</span>';
				val += '</div>';
			}
		}
	});

	$('.artes-finais').append(val);
}

function getImagesSku(data, user) {
	var val = '';

	$(data).each(function (index, value) {
		if (value.thumb) {
			val += '<div class="unitaria-img-sku">';
			val += '<div class="unitaria-img-sku-interno">';
			val += '<img src="' + window.path + value.thumb + '" alt="arquivo" class="modal-sku-img-redim">';
			val += '<div class="nome-sku-box">';
			if (value.nome)
				val += '<p><a href="' + window.path + value.path_cmyk + '?view=1">' + value.nome + '</a></p>';
			else
				val += '<p><a href="' + window.path + value.path_cmyk + '?view=1">' + value.sku_nome + '</a></p>';
			val += '</div>';
			val += '<span class="unitaria-btn-sku btn-view">';
			if (value.path_cmyk != null) {
				if (value.path_cmyk != '') {
					val += '<a href="' + window.path + value.path_cmyk + '?view=1" target="_blank">';
					val += '<strong><i class="icon icon-eye"></i>Vizualizar</strong>';
					val += '</a>';
				}
			}
			val += '</span>';
			val += '</div>';
			val += '<span class="unitaria-btn-sku btn-high-res">';
			if (value.path_rgb != null) {
				if (value.path_rgb != '') {
					val += '<a href="' + window.path + value.path_rgb + '" target="_blank">';
					val += '<i class="icon icon-download"></i>Alta Resolução';
					val += '</a>';
				}
			}
			val += '</span>';
			val += '</div>';
		}
	});

	$('.imagens-sku-down').append(val);
}