jconfirm.defaults = {
    title: '',
    titleClass: '',
    type: 'default',
    typeAnimated: true,
    draggable: false,
    dragWindowGap: 15,
    dragWindowBorder: false,
    animateFromElement: false,
    smoothContent: true,
    content: 'Are you sure to continue?',
    escapeKey: 'cancel',
    defaultButtons: {
        ok: {
            action: function () {
            }
        },
        cancel: {
            action: function () {
            }
        },
    },
    contentLoaded: function(data, status, xhr){
    },
    icon: '',
    lazyOpen: false,
    bgOpacity: null,
    theme: 'light',
    animation: 'scale',
    closeAnimation: 'scale',
    animationSpeed: 300,
    animationBounce: 1,
    rtl: false,
    container: 'body',
    containerFluid: false,
    backgroundDismiss: false,
    backgroundDismissAnimation: 'shake',
    autoClose: false,
    closeIcon: true,
    closeIconClass: 'jconfirm-btn-close fa fa-times',
    watchInterval: 100,
    columnClass: '',
    boxWidth: '910px',
    scrollToPreviousElement: false,
    scrollToPreviousElementAnimate: false,
    useBootstrap: false,
    offsetTop: 40,
    offsetBottom: 40,
    bootstrapClasses: {
        container: 'container',
        containerFluid: 'container-fluid',
        row: 'row',
    },
    onContentReady: function () {},
    onOpenBefore: function () {
        //$('body').addClass('jconfirm-overlay');
        $('body').addClass('no-scroll');
        //$('.master').addClass('blurred');
        $('.jconfirm').css('overflow', 'auto');
        $('.jconfirm').overlayScrollbars({
            scrollbars: {
                autoHide: 'move'
            }
        });
    },
    onClose: function () {
        //$('body').removeClass('jconfirm-overlay');
        $('body').removeClass('no-scroll');
        //$('.master').removeClass('blurred');
        $('.jconfirm').css('overflow', 'hidden');
    },
    onDestroy: function () {},
    onAction: function () {}
};