$(function () {
    var phoneRegex = /^(?:(?:\+|00)?(55)\s?)?(?:\(?([1-9][0-9])\)?\s?)?(?:((?:9\d|[2-9])\d{3})\-?(\d{4}))$/,
        emailRegex = /^([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/;
    
    $('.phone-mask').focusout(function(){
        var phone, element;
        element = $(this);
        element.unmask();
        phone = element.val().replace(/\D/g, '');
        
        if(phone.length > 10) {
            element.mask("(99) 99999-999?9");
        } else {
            element.mask("(99) 9999-9999?9");
        }
    }).trigger('focusout');
    
    $('#addSupplierForm').submit(function () {
        if($('#tfRazaoSocial').val() === '') {
            $('#tfRazaoSocial').focus();
            $('#tfRazaoSocial').addClass('has-error');
            myAlert('Informe a Razão Social.', 'warning');
            
            return false;
        } else {
            $('#tfRazaoSocial').removeClass('has-error');
        }
        
        if($('#sbSupplierStatus').val() === '') {
            $('#sbSupplierStatus').focus();
            $('#sbSupplierStatus').addClass('has-error');
            myAlert('Informe o status.', 'warning');
            
            return false;
        } else {
            $('#sbSupplierStatus').removeClass('has-error');
        }
        
        if($('input[name="nomes[]"]').val() === '' && $('input[name="emails[]"]').val() === '' || $('input[name="nomes[]"]').length < 1 && $('input[name="emails[]"]').length < 1) {
            if($('.col-responsavel input').val() === '') {
                $('.col-responsavel input').focus();
                $('.col-responsavel input').addClass('has-error');
                myAlert('Informe o Nome do Destinatário.', 'warning');
                
                return false;
            } else {
                $('.col-responsavel > input:last').removeClass('has-error');
            }
    
            if($('.col-email-artwork input').val() === '' || !emailRegex.test($('.col-email-artwork input').val())) {
                $('.col-email-artwork input').focus();
                $('.col-email-artwork input').addClass('has-error');
                myAlert('Informe o Email.', 'warning');
                
                return false;
            } else {
                $('.col-email-artwork input').removeClass('has-error');
            }
            
            $('.col-destinatarios-add button').addClass('has-error');

            myAlert('Adicione os dados do fornecedor.', 'warning');
            
            return false;
        }
    });
});