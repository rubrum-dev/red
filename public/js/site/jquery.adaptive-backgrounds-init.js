$(function () {
    $('.box-image>.box-image-thumb-redim').hide();

    $.adaptiveBackground.run({
        exclude: [],
        parent: '.box-image',
        success: function () {
            var eT = 0;

            $('.box-image>.box-image-thumb-redim').each(function () {
                $(this).delay(eT).show();
                eT += 0;
            });
        }
    });
});