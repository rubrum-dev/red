$('.ticket-alt-request-form button[type="submit"]').click(function () {
    var counter = 0;

    $(this).parents('.ticket-alt-request-form').find('.input-required').each(function() {
        if ($(this).val() === '') {
            $(this).addClass('input-error');
            counter++;
        
        } else {
            $(this).removeClass('input-error');
        }
    });
    
    if(counter > 0){
        myAlert('Verefique o preenchimento do campo', 'warning');
    } else {
        $(this).parents('.ticket-alt-request-form').submit();
    }
});