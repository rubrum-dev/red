$(function () {
	$('.footer, .clear-footer').appendTo('.wrapped');
	$('.push').appendTo('.container');

	function scrollingBox() {
		//var el = $('.box-package-info'),
			//elpos = el.offset().top;

		$(window).scroll(function () {
			var y = $(this).scrollTop();

			if (y < elpos) {
				el.removeClass('box-package-info-sticky');
				el.stop().animate({
					'top': 0
				}, 0);
			} else {
				el.stop().animate({
					'top': 90 + y - elpos
				}, 0);
				el.addClass('box-package-info-sticky');
			}
		});
	}
	
	$('.only-numeric').keyup(function(e) {
		if (/\D/g.test(this.value)) {
			// Filter non-digits from input value.
			this.value = this.value.replace(/\D/g, '');
			myAlert('Esse campo aceita apenas números.', 'Atenção', 'warning');
		}
	});

	$('.only-alpha-numeric').bind('keyup', function () {
		var input = $(this).val();
		re = /[!$*()_|\=?;:".<>\{\}\[\]\\\/]/gi;
		var isSplChar = re.test(input);

		if (isSplChar) {
			var noSplChar = input.replace(/[!$*()_|\=?;:",.<>\{\}\[\]\\\/]/gi, '');

			myAlert('Utilize apenas letras, números e alguns caracteres especiais como <br /> @ # % & - + ,.', 'Atenção', 'warning');
			$(this).val(noSplChar);
			$(this).focus();
		}
	});

	scrollingBox();
});